#!/bin/bash
REMOTE_IMAGE_TAG="${GAZELOOK_DOCKER_REPOSITORY}/${GAZELOOK_API_MAIN_NAME}:${GAZELOOK_API_MAIN_IMAGE_VERSION}"

echo "==> Stopping microservice ${GAZELOOK_API_MAIN_NAME}... "
docker stop "${GAZELOOK_API_MAIN_NAME}"
echo "==> Deleting microservice ${GAZELOOK_API_MAIN_NAME}... "
docker rm "${GAZELOOK_API_MAIN_NAME}"
echo "==> Deleting image tag ${REMOTE_IMAGE_TAG}... "
docker rmi "${REMOTE_IMAGE_TAG}"
echo "==> Building docker image ${REMOTE_IMAGE_TAG}... "
docker build "${API_MAIN_PATH_PROJECT}" -f Dockerfile -t "${REMOTE_IMAGE_TAG}"