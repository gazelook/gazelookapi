# BACKEND GAZELOOK

For this technology project we will use the following

Nesjts CLI: 7.4.1

Node: 13.8.0

Nestjs: 7.0.0

Typescript: 3.7.4

Mongo: 4.4.5

AWS S3 / Google Cloud Storage 



# TOOLS

Postman

Robo 3T

Slack

Git

Your favorite editor (Visual Studio Code)∫



## INSTALLATION
To build the project, it is necessary to download and install the following tools:

- NODE JS 13.X (It includes NPM)

  Windows: https://nodejs.org/es/download/

  Linux:
  ```
  curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
  ```
  ```
  sudo apt-get install -y nodejs
  ```

- GIT

  Windows: https://git-scm.com/downloads

  Linux:
  ```
  apt-get install git
  ```

- CLI Nestjs

  windows && Linux:
  ```
  npm i -g @nestjs/cli
  ```

To download the project, from the terminal

  windows && Linux:
  ```
  git clone https://gitlab.com/gazelook/gazelookapi.git
  ```
  Once the repository is cloned, we create our branch starting from master

  Download the dependencies.
  ```
  npm install
  ```

Before running the project

Create database `dbname` and import data from gazelookQa folder

Run the following script
```
mongorestore --uri dbhost -d dbname dbdirectory 

Ejm:
mongorestore --uri mongodb+srv://gazelook:<PASSWORD>@cluster0.89jao.mongodb.net -d gazelookQa C:\repaldoBD\gazelookQa

```



## EXECUTION

From the project folder, we execute the command according to our needs

Before executing, check the connection string in the file in the path configsEnv\qa.env

Just change `MONGO_URI` for your connection string if necessary
  
```bash

# watch mode
$ npm run start:Qa

```

  If we navigate to `http://localhost:4000/api/` we can see our server running.



## API DOCUMENTATION


### GENERATE ELEMENTS

```
$ Nest generate [ name or alias ]

```

    ┌───────────────┬─────────────┐
    │ name          │ alias       │
    │ application   │ application │
    │ class         │ cl          │
    │ configuration │ config      │
    │ controller    │ co          │
    │ decorator     │ d           │
    │ filter        │ f           │
    │ gateway       │ ga          │
    │ guard         │ gu          │
    │ interceptor   │ in          │
    │ interface     │ interface   │
    │ middleware    │ mi          │
    │ module        │ mo          │
    │ pipe          │ pi          │
    │ provider      │ pr          │
    │ resolver      │ r           │
    │ service       │ s           │
    │ library       │ lib         │
    │ sub-app       │ app         │
    └───────────────┴─────────────┘

## STRUCTURE

The project is structured by folders as follows:
      doc/ 
         Documents and prerequisites to execute the project
      src/
        |-- drivers/ 
                    Here must go settings and main elements, which are used throughout the application globally
            |-- [+] logging/ 
                            Config para gestianar logs del sistem
            |-- [+] mongoose/ 
                        Mongo models and schematics
            |-- [+] interfaces/
            |-- [+] modelos/

         |-- Entities/
            |-- [+] entidad (Usuario)/ 
                    |-- [+] controladores/ 
                               Set of controllers that convert data from the most convenient format for use cases and entities, to the most convenient format for some external elements such as the database or the web.
                    |-- [+] caso_de_uso/ 
                                Business rules (services) that define how our system behaves, defining the necessary input data, and what will be its output.
                    |-- [+] entidad/ 
                               Entities encapsulate the business rules of the entire company. An entity can be an object with methods or it can be a set of data and functions.
                    |-- [+] drivers/ 
                                The outermost layer is generally composed of frameworks and tools such as the database, the web framework, etc.



∫

## Git best practice guide

### Introduction

The purpose of this section is to have a common wiki with the good practices that we have to follow to keep the project in order.


## Nomenclature

Directories: my_directory_name (use_cases)
files: file-name.type.ts (my-user.service.ts)
variables: myVariable (userProfile)
classes: MyClass (UserProfile)
functions: myFunction (getUsers)
constants: MY_CONSTANT (PROFILE_TYPE)
API URL: /get-user-profile/id

## API routes documentation

Http verbs describe actions by themselves

GET/users- Returns a list of users
GET/users/12- Returns a specific user
POST/users- Create a new user
PUT/users/12- Update user # 12
PATCH/users/12- Partially update user # 12
DELETE/users/12- Delete user # 12





## Dynamic translations API

Install environment variable on server or local

Open command prompt:

Windows

1. `setx TRANSLATOR_TEXT_SUBSCRIPTION_KEY "7d9df922a6164c61943a9b0e2e4a53b4"`

2. `setx TRANSLATOR_TEXT_ENDPOINT https://api.cognitive.microsofttranslator.com`

Linux o Mac/os:

1. `export TRANSLATOR_TEXT_SUBSCRIPTION_KEY=7d9df922a6164c61943a9b0e2e4a53b4`

2. `export TRANSLATOR_TEXT_ENDPOINT=https://api.cognitive.microsofttranslator.com`

3. `source ~/.bashrc`

To use the translation api, a global method has been created that will receive the language code sent as parameters, for example: `es`,` en`, `fr` etc. and also the text to be translated.

So that it can be called every time a dynamic translation is required, it is found in /src/multiIdioma/traduccion-dinamica.

To import it:
`import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica'`

To use it you can call it for example:
`const textoTraducido= await traducirTexto(idioma, texto);`

The preferred language will be sent by the header.

As a reference of the logic that must be applied every time it is required to use it, they can be referenced in the entity `pensamientos` in the use case `obtener-pensamientos.service`.

It should be noted that the translations api will only be called if the translation is not stored to avoid wasting unnecessary api resources.

Languages allowed by the API

    ┌─────────────────────────┬─────────────────┐
    │ Language                │ Language code   │
    │ Afrikaans               │ af              │
    │ Arabic                  │ ar              │
    │ Bangla                  │ bn              │
    │ Bosnian(Latin)          │ bs              │
    │ Bulgarian               │ bg              │
    │ Cantonese (Traditional) │ yue             │
    │ Catalan                 │ ca              │
    │ Chinese Simplified      │ zh-Hans         │
    │ Chinese Traditional     │ zh-Hant         │
    │ Croatian                │ hr              │
    │ Czech                   │ cs              │
    │ Danish                  │ da              │
    │ Dutch                   │ nl              │
    │ English                 │ en              │
    │ Estonian                │ et              │
    │ Fijian                  │ fj              │
    │ Filipino                │ fil             │
    │ Finnish                 │ fi              │
    │ French                  │ fr              │
    │ German                  │ de              │
    │ Greek                   │ el              │
    │ Gujarati                │ gu              │
    │ Haitian Creole          │ ht              │
    │ Hebrew                  │ he              │
    │ Hindi                   │ hi              │
    │ Hmong Daw               │ mww             │
    │ Hungarian               │ hu              │
    │ Icelandic               │ is              │  
    │ Indonesian              │ id              │
    │ Irish                   │ ga              │
    │ Italian                 │ it              │
    │ Japanese                │ ja              │
    │ Kannada                 │ kn              │
    │ Kazakh                  │ kk              │
    │ Kiswahili               │ sw              │
    │ Klingon                 │ tlh-Latn        │
    │ Klingon (plqaD)         │ tlh-Piqd        │
    │ Korean                  │ ko              │
    │ Latvian                 │ lv              │
    │ Lithuanian              │ lt              │
    │ Malagasy                │ mg              │
    │ Malay                   │ ms              │
    │ Malayalam               │ ml              │
    │ Maltese                 │ mt              │
    │ Maori                   │ mi              │
    │ Marathi                 │ mr              │
    │ Norwegian               │ nb              │
    │ Persian                 │ fa              │
    │ Polish                  │ pl              │
    │ Portuguese (Brazil)     │ pt-br           │
    │ Portuguese (Portugal)   │ pt-pt           │
    │ Punjabi                 │ pa              │
    │ Queretaro Otomi         │ otq             │      
    │ Romanian                │ ro              │
    │ Russian                 │ ru              │
    │ Samoan                  │ sm              │
    │ Serbian (Cyrillic)      │ sr-Cyrl         │
    │ Serbian (Latin)         │ sr-Latn         │
    │ Slovak                  │ sk              │
    │ Slovenian               │ sl              │
    │ Spanish                 │ es              │
    │ Swedish                 │ sv              │
    │ Tahitian                │ ty              │
    │ Tamil                   │ ta              │
    │ Telugu                  │ te              │
    │ Thai                    │ th              │
    │ Tongan                  │ to              │
    │ Turkish                 │ tr              │
    │ Ukrainian               │ uk              │
    │ Urdu                    │ ur              │
    │ Vietnamese              │ vi              │
    │ Welsh                   │ cy              │
    │ Yucatec Maya            │ yua             │
    └─────────────────────────┴─────────────────┘

## Static translations with i18n (internationalization)

For static translations you must import:

`import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller'`

Then call it in the constructor of the controller where the translation is desired, Example:

  `constructor( private readonly traduccionEstaticaController: TraduccionEstaticaController)`

Then it is called as follows, example:

`const FALLO_DEVOLUCION_PENSAMIENTOS= await this.traduccionEstaticaController.traduccionEstatica(headers.idioma, 'FALLO_DEVOLUCION_PENSAMIENTOS')`

Where `headers.idioma` will be the language sent by the header, if no language is sent,`en` will be set as the default language within the `traduccionEstatica` function and THOUGHTS_VOLUTION_FALLO will be the key of the value to be translated, these Key / value are in the path `src/i18n/*`, there will be the files of the translation languages that we need, in this case if new languages are implemented it should be added there with the same format of the previous files .

IMPORTANT: all the routes that are created must be prepared to receive the language through the header with the following syntax example: `key:idioma, value:en`

Example of the en.json file:

    {
      "MANTENIMIENTO_PLATAFORMA": "Sorry, we are doing maintenance on the platform, we will be back in a few minutes",
      "LLAMANDA_NO_CONTESTADA": "User did not answer your call",
      "LLAMADA_TERMINADA": "Call finished",
      "ACTUALIZACION_CORRECTA": "Successfully updated",
      "CREACION_CORRECTA": "It has been successfully created",
      "ELIMINACION_CORRECTA": "It has been correctly removed",
      "PERFIL_NO_EXISTE": "Profile does not exist",
      "PARAMETROS_NO_VALIDOS": "Invalid parameters",
      "ERROR_ELIMINAR": "Cannot be deleted",
      "ERROR_ACTUALIZAR": "Data could not be updated",
      "ERROR_CREACION": "Data could not be registered",
      "ERROR_OBTENER": "Data could not be obtained",
      "COLECCION_VACIA": "Does not contain data",
      "NO_EXISTE_DOCUMENTO": "Document not found",
      "ERROR_VOTO": "Error while voting",
      "GAZELOOK_FUERA_SERVICIO": "We are working to provide you a better service, we will be back soon!",
      "VOTO_CORRECTO": "Vote correctly registered",
      "NO_AUTORIZADO": "Not authorized",
      "EMAIL_ENVIADO": "E-mail successfully sent",
      "EMAIL_NO_PUEDE_CREARSE": "E-mail cannot be sent",
      "EMAIL_VERIFICADO": "E-mail verified",
      "LINK_NO_VALIDO": "Invalid link",
      "CONTRASENIA_ACTUALIZADO": "Password has been successfully updated",
      "ERROR_CONTRASENIAS": "Passwords do not match",
      "ERROR_SOLICITUD": "Sorry, an error occurred while processing your request. Please try again later",
      "EXPERIMENTANDO_INCONVENIENTES": "We are experiencing problems, please try again later",
      "NO_EXISTE_COINCIDENCIAS": "No matches found",
      "SOLICITUD_EN_PROCESO": "Your request is being processed, you will be notified by mail when it is completed",
      "DATOS_ELIMINADOS": "Your information has been removed from the application",
      "EMAIL_REGISTRADO": "E-mail registered",
      "CREDENCIALES_INCORRECTAS": "Incorrect access credentials",
      "ERROR_NOMBRE_CONTACTO": "Invalid contact name",
      "PERFIL_NO_VALIDO": "Invalid profile",
      "USUARIO_NO_REGISTRADO": "Unregistered user",
      "ERROR_TRADUCIR_NOTICIA": "News cannot be translated",
      "PERFIL_HIBERNADO": "Profile disabled",
      "NOMBRE_CONTACTO_DISPONIBLE": "Contact name available",
      "EMAIL_YA_REGISTRADO": "E-mail already registered",
      "EMAIL_DISPONIBLE": "E-mail available",
      "CUENTA_REGISTRADA_NO_PAGO": "Your account is already registered, a new payment has been generated",
      "ERROR_METODO_PAGO": "Invalid payment method",
      "ERROR_TERMINOS_CONDICIONES": "Sorry, you must accept our terms and conditions in order to complete the registration process",
      "USUARIO_YA_REGISTRADO": "User already registered",
      "TRANSACCION_NO_VALIDA": "Invalid transaction",
      "ERROR_REGISTRO_PAGO": "Error while registering payment",
      "LIMITE_ARCHIVO": "File exceeds the allowed size",
      "ERROR_PROCESO_ARCHIVO": "File processing error",
      "ARCHIVO_NO_VALIDO": "Invalid file",
      "ERROR_CONVERSION_MONEDA": "Conversion error",
      "TRANSFERIR_PROYECTO": "Project transferred correctly",
      "EMAIL_NO_VALIDO": "Invalid e-mail",
      "ERROR_TRANSFERIR_PROYECTO": "Project could not be transferred",
      "SOLICITUD_CANCELADA": "Request cancelled",
      "CUENTA_RECHAZADA_RESPONSABLE": "Account rejected by the person in charge",
      "VALIDAR_CUENTA": "To validate your account created, please check your e-mail",
      "INACTIVA_PAGO": "Pay your subscription fee to use our services",
      "NO_PERMISO_ACCION": "You do not have the permission to perform this action",
      "ERROR_SUSCRIPCION": "Error when renewing subscription",
      "SUSCRIPCION_RENOVADA": "Subscription has been renewed",
      "ERROR_SALIR": "Error when exiting the application",
      "SESION_CERRADA": "Closed session"
    }


## Currency converter with (Open Exchange Rates API)

There is a `convertirMoney` function in the path `/gazelookApi/src/money/convertir-money.ts`

It can be called from anywhere that is needed in the application as follows FOR EXAMPLE:

  `const converMoney = await convertirMoney(18, 'EUR')`

Where `18` is the amount (money) we need to convert and` EUR` is the code of the currency that we are going to convert. By default it will convert from dollar `USD` to any currency that we pass to it.

Below is a .json with all available currencies:

    {
      "AED": "United Arab Emirates Dirham",
      "AFN": "Afghan Afghani",
      "ALL": "Albanian Lek",
      "AMD": "Armenian Dram",
      "ANG": "Netherlands Antillean Guilder",
      "AOA": "Angolan Kwanza",
      "ARS": "Argentine Peso",
      "AUD": "Australian Dollar",
      "AWG": "Aruban Florin",
      "AZN": "Azerbaijani Manat",
      "BAM": "Bosnia-Herzegovina Convertible Mark",
      "BBD": "Barbadian Dollar",
      "BDT": "Bangladeshi Taka",
      "BGN": "Bulgarian Lev",
      "BHD": "Bahraini Dinar",
      "BIF": "Burundian Franc",
      "BMD": "Bermudan Dollar",
      "BND": "Brunei Dollar",
      "BOB": "Bolivian Boliviano",
      "BRL": "Brazilian Real",
      "BSD": "Bahamian Dollar",
      "BTC": "Bitcoin",
      "BTN": "Bhutanese Ngultrum",
      "BWP": "Botswanan Pula",
      "BYN": "Belarusian Ruble",
      "BZD": "Belize Dollar",
      "CAD": "Canadian Dollar",
      "CDF": "Congolese Franc",
      "CHF": "Swiss Franc",
      "CLF": "Chilean Unit of Account (UF)",
      "CLP": "Chilean Peso",
      "CNH": "Chinese Yuan (Offshore)",
      "CNY": "Chinese Yuan",
      "COP": "Colombian Peso",
      "CRC": "Costa Rican Colón",
      "CUC": "Cuban Convertible Peso",
      "CUP": "Cuban Peso",
      "CVE": "Cape Verdean Escudo",
      "CZK": "Czech Republic Koruna",
      "DJF": "Djiboutian Franc",
      "DKK": "Danish Krone",
      "DOP": "Dominican Peso",
      "DZD": "Algerian Dinar",
      "EGP": "Egyptian Pound",
      "ERN": "Eritrean Nakfa",
      "ETB": "Ethiopian Birr",
      "EUR": "Euro",
      "FJD": "Fijian Dollar",
      "FKP": "Falkland Islands Pound",
      "GBP": "British Pound Sterling",
      "GEL": "Georgian Lari",
      "GGP": "Guernsey Pound",
      "GHS": "Ghanaian Cedi",
      "GIP": "Gibraltar Pound",
      "GMD": "Gambian Dalasi",
      "GNF": "Guinean Franc",
      "GTQ": "Guatemalan Quetzal",
      "GYD": "Guyanaese Dollar",
      "HKD": "Hong Kong Dollar",
      "HNL": "Honduran Lempira",
      "HRK": "Croatian Kuna",
      "HTG": "Haitian Gourde",
      "HUF": "Hungarian Forint",
      "IDR": "Indonesian Rupiah",
      "ILS": "Israeli New Sheqel",
      "IMP": "Manx pound",
      "INR": "Indian Rupee",
      "IQD": "Iraqi Dinar",
      "IRR": "Iranian Rial",
      "ISK": "Icelandic Króna",
      "JEP": "Jersey Pound",
      "JMD": "Jamaican Dollar",
      "JOD": "Jordanian Dinar",
      "JPY": "Japanese Yen",
      "KES": "Kenyan Shilling",
      "KGS": "Kyrgystani Som",
      "KHR": "Cambodian Riel",
      "KMF": "Comorian Franc",
      "KPW": "North Korean Won",
      "KRW": "South Korean Won",
      "KWD": "Kuwaiti Dinar",
      "KYD": "Cayman Islands Dollar",
      "KZT": "Kazakhstani Tenge",
      "LAK": "Laotian Kip",
      "LBP": "Lebanese Pound",
      "LKR": "Sri Lankan Rupee",
      "LRD": "Liberian Dollar",
      "LSL": "Lesotho Loti",
      "LYD": "Libyan Dinar",
      "MAD": "Moroccan Dirham",
      "MDL": "Moldovan Leu",
      "MGA": "Malagasy Ariary",
      "MKD": "Macedonian Denar",
      "MMK": "Myanma Kyat",
      "MNT": "Mongolian Tugrik",
      "MOP": "Macanese Pataca",
      "MRO": "Mauritanian Ouguiya (pre-2018)",
      "MRU": "Mauritanian Ouguiya",
      "MUR": "Mauritian Rupee",
      "MVR": "Maldivian Rufiyaa",
      "MWK": "Malawian Kwacha",
      "MXN": "Mexican Peso",
      "MYR": "Malaysian Ringgit",
      "MZN": "Mozambican Metical",
      "NAD": "Namibian Dollar",
      "NGN": "Nigerian Naira",
      "NIO": "Nicaraguan Córdoba",
      "NOK": "Norwegian Krone",
      "NPR": "Nepalese Rupee",
      "NZD": "New Zealand Dollar",
      "OMR": "Omani Rial",
      "PAB": "Panamanian Balboa",
      "PEN": "Peruvian Nuevo Sol",
      "PGK": "Papua New Guinean Kina",
      "PHP": "Philippine Peso",
      "PKR": "Pakistani Rupee",
      "PLN": "Polish Zloty",
      "PYG": "Paraguayan Guarani",
      "QAR": "Qatari Rial",
      "RON": "Romanian Leu",
      "RSD": "Serbian Dinar",
      "RUB": "Russian Ruble",
      "RWF": "Rwandan Franc",
      "SAR": "Saudi Riyal",
      "SBD": "Solomon Islands Dollar",
      "SCR": "Seychellois Rupee",
      "SDG": "Sudanese Pound",
      "SEK": "Swedish Krona",
      "SGD": "Singapore Dollar",
      "SHP": "Saint Helena Pound",
      "SLL": "Sierra Leonean Leone",
      "SOS": "Somali Shilling",
      "SRD": "Surinamese Dollar",
      "SSP": "South Sudanese Pound",
      "STD": "São Tomé and Príncipe Dobra (pre-2018)",
      "STN": "São Tomé and Príncipe Dobra",
      "SVC": "Salvadoran Colón",
      "SYP": "Syrian Pound",
      "SZL": "Swazi Lilangeni",
      "THB": "Thai Baht",
      "TJS": "Tajikistani Somoni",
      "TMT": "Turkmenistani Manat",
      "TND": "Tunisian Dinar",
      "TOP": "Tongan Pa'anga",
      "TRY": "Turkish Lira",
      "TTD": "Trinidad and Tobago Dollar",
      "TWD": "New Taiwan Dollar",
      "TZS": "Tanzanian Shilling",
      "UAH": "Ukrainian Hryvnia",
      "UGX": "Ugandan Shilling",
      "USD": "United States Dollar",
      "UYU": "Uruguayan Peso",
      "UZS": "Uzbekistan Som",
      "VEF": "Venezuelan Bolívar Fuerte",
      "VND": "Vietnamese Dong",
      "VUV": "Vanuatu Vatu",
      "WST": "Samoan Tala",
      "XAF": "CFA Franc BEAC",
      "XAG": "Silver Ounce",
      "XAU": "Gold Ounce",
      "XCD": "East Caribbean Dollar",
      "XDR": "Special Drawing Rights",
      "XOF": "CFA Franc BCEAO",
      "XPD": "Palladium Ounce",
      "XPF": "CFP Franc",
      "XPT": "Platinum Ounce",
      "YER": "Yemeni Rial",
      "ZAR": "South African Rand",
      "ZMW": "Zambian Kwacha",
      "ZWL": "Zimbabwean Dollar"
    }


