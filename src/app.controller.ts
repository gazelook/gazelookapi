import { Controller, Get, Inject, Render } from '@nestjs/common';
import { AppService } from './app.service';
import { I18nResolver } from 'nestjs-i18n';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
}
