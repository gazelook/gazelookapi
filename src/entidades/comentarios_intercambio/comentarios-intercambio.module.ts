import { ComentariosIntercambioControllerModule } from './controladores/comentarios-intercambio-controller.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [ComentariosIntercambioControllerModule],
  providers: [],
  controllers: [],
})
export class ComentariosIntercambioModule {}
