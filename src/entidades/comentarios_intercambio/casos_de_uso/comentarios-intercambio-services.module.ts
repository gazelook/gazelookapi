import { RolServiceModule } from '../../rol/casos_de_uso/rol.services.module';
import { ObtenerComentariosIntercambioService } from './obtener-comentarios-intercambio.service';
import { CatalogoTipoComentarioService } from '../../catalogos/casos_de_uso/catalogo-tipo-comentario.service';
import { CrearComentarioIntercambioService } from './crear-comentario.service';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { comentariosProviders } from '../drivers/comentarios-intercambio.provider';
import { TraducirComentarioService } from './traducir-comentario.service';
import { EliminarMiComentarioIntercambioService } from './eliminar-mi-comentario.service';
import { EliminarComentarioIntercambioService } from './eliminar-comentario.service';
import { EliminarComentarioIntercambioCompletoService } from './eliminar-comentario-intercambio-completo.service';
import { AlbumServiceModule } from 'src/entidades/album/casos_de_uso/album.services.module';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { Funcion } from 'src/shared/funcion';
import { MediaServiceModule } from 'src/entidades/media/casos_de_uso/media.services.module';
import { CatalogoProviders } from 'src/entidades/catalogos/drivers/catalogo.provider';
import { CatalogoColoresService } from 'src/entidades/catalogos/casos_de_uso/catalogo-colores.service';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { CatalogoConfiguracionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-configuracion.service';
import { ObtenerComentarioUnicoIntercambioService } from './obtener-comentarios-unico-intercambio.service';
import { ArchivoServicesModule } from 'src/entidades/archivo/casos_de_uso/archivo-services.module';
import { ObtenerComentariosIntercambioByIdService } from './obtener-comentarios-by-id.service';
import { FirebaseModule } from 'src/drivers/firebase/firebase.module';
import { TraducirComentarioSegundoPlanoService } from './traducir-comentario-segundo-plano.service';
import { participanteIntercambioProviders } from 'src/entidades/participante_intercambio/drivers/participante-intercambio.provider';
import { CrearParticipanteIntercambioService } from 'src/entidades/participante_intercambio/casos_de_uso/crear-participante-intercambio.service';
import { ObtenerParticipanteIntercambioervice } from 'src/entidades/participante_intercambio/casos_de_uso/obtener-participante-intercambio.service';
import { ObtenerMediasProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-medias-proyecto.service';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    RolServiceModule,
    forwardRef(() => AlbumServiceModule),
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => MediaServiceModule),
    forwardRef(() => ArchivoServicesModule),
    FirebaseModule,
  ],
  providers: [
    ...comentariosProviders,
    ...participanteIntercambioProviders,
    ...CatalogoProviders,
    CrearComentarioIntercambioService,
    EliminarMiComentarioIntercambioService,
    EliminarComentarioIntercambioService,
    TraducirComentarioService,
    CatalogoTipoComentarioService,
    EliminarComentarioIntercambioCompletoService,
    Funcion,
    CatalogoColoresService,
    CatalogoEstilosService,
    CatalogoConfiguracionService,
    ObtenerComentarioUnicoIntercambioService,
    ObtenerComentariosIntercambioByIdService,
    TraducirComentarioSegundoPlanoService,
    EliminarComentarioIntercambioCompletoService,
    CrearParticipanteIntercambioService,
    ObtenerParticipanteIntercambioervice,
    ObtenerMediasProyectoService,
    ObtenerComentariosIntercambioService,
  ],
  exports: [
    ...comentariosProviders,
    ...participanteIntercambioProviders,
    CrearComentarioIntercambioService,
    EliminarMiComentarioIntercambioService,
    EliminarComentarioIntercambioService,
    TraducirComentarioService,
    CatalogoTipoComentarioService,
    EliminarComentarioIntercambioCompletoService,
    ObtenerComentarioUnicoIntercambioService,
    ObtenerComentariosIntercambioByIdService,
    TraducirComentarioSegundoPlanoService,
    EliminarComentarioIntercambioCompletoService,
    CrearParticipanteIntercambioService,
    ObtenerParticipanteIntercambioervice,
    ObtenerMediasProyectoService,
    ObtenerComentariosIntercambioService,
  ],
  controllers: [],
})
export class ComentariosIntercambioServicesModule {}
