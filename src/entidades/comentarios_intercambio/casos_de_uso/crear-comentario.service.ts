import { ConfiguracionEstilo } from '../../../drivers/mongoose/interfaces/configuracion_estilo/configuracion_estilo.interface';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import { CatalogoTipoComentarioService } from '../../catalogos/casos_de_uso/catalogo-tipo-comentario.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Inject, Injectable } from '@nestjs/common';
import { Model, mongo } from 'mongoose';
import { CrearComentarioDto } from '../entidad/crear-comentario-dto';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import {
  accionNotificacionFirebase,
  codIdiomas,
  codigoEntidades,
  codigosEstadosComentarioIntercambio,
  codigosRol,
  estadoNotificacionesFirebase,
  idiomas,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreCatComentario,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import * as mongoose from 'mongoose';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { ObtenerComentarioUnicoIntercambioService } from './obtener-comentarios-unico-intercambio.service';
import { ActualizarMediaService } from 'src/entidades/media/casos_de_uso/actualizar-media.service';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { TraducirComentarioService } from './traducir-comentario.service';
import { FirebaseNotificacionService } from 'src/drivers/firebase/services/firebase-notificacion.service';
import { TraducirComentarioSegundoPlanoService } from './traducir-comentario-segundo-plano.service';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { ParticipanteIntercambio } from 'src/drivers/mongoose/interfaces/participante_intercambio/participante_intercambio.interface';
import { TraduccionComentarioIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_comentario_intercambio/traduccion-comentario-intercambio.interface';
import { ComentariosIntercambio } from 'src/drivers/mongoose/interfaces/comentarios_intercambio/comentarios-intercambio.interface';
import { ObtenerParticipanteIntercambioervice } from 'src/entidades/participante_intercambio/casos_de_uso/obtener-participante-intercambio.service';
import { CrearParticipanteIntercambioService } from 'src/entidades/participante_intercambio/casos_de_uso/crear-participante-intercambio.service';
import { FBNotificacion } from 'src/drivers/firebase/models/fb-notificacion.interface';
import { ObtenerComentariosIntercambioService } from './obtener-comentarios-intercambio.service';

@Injectable()
export class CrearComentarioIntercambioService {
  constructor(
    @Inject('COMENTARIOS_INTERCAMBIO_MODEL')
    private readonly comentarioIntercambioModel: Model<ComentariosIntercambio>,
    @Inject('TRADUCCION_COMENTARIO_INTERCAMBIO_MODEL')
    private readonly traduccionComentarioIntercambioModel: Model<
      TraduccionComentarioIntercambio
    >,
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    @Inject('PARTICIPANTE_INTERCAMBIO_MODEL')
    private readonly participanteIntercambioModelo: Model<
      ParticipanteIntercambio
    >,
    @Inject('CONFIGURACION_ESTILO_MODEL')
    private readonly configuracionEstiloModelo: Model<ConfiguracionEstilo>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoTipoComentarioService: CatalogoTipoComentarioService,
    private crearParticipanteIntercambioService: CrearParticipanteIntercambioService,
    private obtenerParticipanteIntercambioervice: ObtenerParticipanteIntercambioervice,
    private gestionRolService: GestionRolService,
    private obtenerComentariosService: ObtenerComentariosIntercambioService,
    private obtenerComentarioUnicoIntercambioService: ObtenerComentarioUnicoIntercambioService,
    private actualizarMediaService: ActualizarMediaService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private firebaseNotificacionService: FirebaseNotificacionService,
    private traducirComentarioSegundoPlanoService: TraducirComentarioSegundoPlanoService,
  ) {}

  async comentar(
    comentarioDto: CrearComentarioDto,
    codIdioma: any,
  ): Promise<any> {
    try {
      const saveComentario = await this.procesarComentario(
        comentarioDto,
        codIdioma,
      );

      if (saveComentario) {
        const getComentario = saveComentario.getComentario;
        const perfilPropietario = saveComentario.perfilPropietario;

        this.firebaseNotificacionService.createDataComenatarioIntercambio(
          getComentario,
          comentarioDto.intercambio._id.toString(),
        );

        // setTimeout(() => {
        //_______________ Notificar comentario al propietario del intercambio ____________________

        let formatData = {
          intercambio: {
            _id: comentarioDto.intercambio._id.toString(),
          },
          perfil: {
            _id: comentarioDto.coautor.coautor._id.toString(),
          },
        };

        const dataNotificarComentario: FBNotificacion = {
          idEntidad: getComentario._id.toString(),
          leido: false,
          codEntidad: codigoEntidades.comentarioIntercambio,
          data: formatData,
          estado: estadoNotificacionesFirebase.activa,
          accion: accionNotificacionFirebase.ver,
          query: `${codigoEntidades.comentarioIntercambio}-${false}`,
          queryEntidad: `${comentarioDto.intercambio._id.toString()}`,
        };

        await this.firebaseNotificacionService.createDataNotificacion(
          dataNotificarComentario,
          codigoEntidades.entidadPerfiles,
          perfilPropietario,
        );
        // })
        return getComentario;
      }

      return null;
    } catch (error) {
      throw error;
    }
  }
  async procesarComentario(
    comentarioDto: CrearComentarioDto,
    codIdioma: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();

    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };

    let transaccion;

    //await session.startTransaction(transactionOptions);

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarioIntercambio,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      let idiomaDetectado;
      let conTraduccion;
      if (
        comentarioDto?.traducciones &&
        comentarioDto?.traducciones.length > 0
      ) {
        conTraduccion = true;
        let textoTraducido = await traducirTexto(
          codIdioma,
          comentarioDto.traducciones[0].texto,
        );
        idiomaDetectado = textoTraducido.idiomaDetectado;
      } else {
        conTraduccion = false;
        idiomaDetectado = codIdioma;
      }

      //Obtiene el idioma por el codigo
      // const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(idiomaDetectado)
      // let codIdioma = idioma.codigo;

      const getIntercambio = await this.intercambioModel
        .findOne({ _id: comentarioDto.intercambio._id })
        .populate({
          path: 'comentarios',
          select: 'coautor',
          match: {
            estado: codigosEstadosComentarioIntercambio.activa,
          },
          populate: {
            path: 'coautor',
            select: 'coautor roles',
            populate: {
              path: 'roles',
              select: 'rol',
            },
          },
        });

      let crearComent: any;
      if (getIntercambio) {
        // _________________________________________Inicia transaccion muiti-document_________________________
        transaccion = await session.withTransaction(async () => {
          crearComent = await this.crearComentario(
            comentarioDto,
            idiomaDetectado,
            opts,
          );

          if (!crearComent) {
            await session.abortTransaction();
            return;
          }
        }, transactionOptions);

        if (transaccion) {
          let getComentario = await this.obtenerComentarioUnicoIntercambioService.obtenerComentarioUnicoIntercambio(
            crearComent._id,
            codIdioma,
            conTraduccion,
          );
          return {
            getComentario: getComentario,
            perfilPropietario: getIntercambio.perfil.toString(),
          };
        }
      } else {
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      await session.endSession();
      throw error;
    } finally {
      await session.endSession();
    }
  }

  async crearComentario(
    comentarioDto: CrearComentarioDto,
    codIdioma: any,
    opts: any,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarioIntercambio,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const codigoIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdioma,
      );
      const entidadTraduccion = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionComentarioIntercambio,
      );
      const estadoTradComen = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidadTraduccion.codigo,
      );
      const getEstadoActivaTradComent = estadoTradComen.codigo;

      let tipoComentario: any;

      const entidadParticipanteInter = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.participanteIntercambio,
      );
      const estadoParticipanteInter = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidadParticipanteInter.codigo,
      );

      // Crear nueevo ObjectId para traduccion comentario
      const idComentario = new mongo.ObjectId();

      // crear participante intercambio, con rol tipo coautor
      // validar si es la primera vez que comenta se crea y si es el propietario

      const existeParticipante = await this.obtenerParticipanteIntercambioervice.ObtenerParticipanteIntercambio(
        comentarioDto.coautor.coautor._id,
        comentarioDto.intercambio._id,
      );

      let participanteIntercambio: any;
      if (existeParticipante) {
        participanteIntercambio = existeParticipante;
      } else {
        const rolEntidad = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
          codigosRol.cod_rol_coautor_comentario_intercambio,
          entidad.codigo,
        );
        // random de colores para participante intercambio
        const configEstilo = await this.ObtenerConfiguracionEstilo(
          entidad.codigo,
        );

        const participanteIntercambioDto: any = {
          estado: estadoParticipanteInter.codigo,
          coautor: comentarioDto.coautor.coautor._id,
          roles: [rolEntidad._id],
          configuraciones: [configEstilo], // configuraciones de estilos
          comentarios: [],
          intercambio: comentarioDto.intercambio._id,
          totalComentarios: 0,
        };

        participanteIntercambio = await this.crearParticipanteIntercambioService.crearParticipanteIntercambio(
          participanteIntercambioDto,
          opts,
        );
      }

      tipoComentario = await this.catalogoTipoComentarioService.obtenerTipoComentario(
        nombreCatComentario.normal,
      );

      let fechaCreacionFirebase = new Date();
      // console.log('fechaCreacion.getTime(): ', fechaCreacionFirebase.getTime())
      const nuevoComentario: any = {
        _id: idComentario,
        estado: estado.codigo,
        coautor: participanteIntercambio._id,
        adjuntos: [],
        importante: comentarioDto.importante,
        tipo: tipoComentario.codigo,
        intercambio: comentarioDto.intercambio._id,
        fechaActualizacion: new Date(),
        fechaCreacionFirebase: fechaCreacionFirebase.getTime(),
      };

      let traducciones = [];
      let traduccionComentario;
      //Verifica si tiene traducciones

      if (
        comentarioDto?.traducciones &&
        comentarioDto?.traducciones.length > 0
      ) {
        //Objeto de traduccion comentario
        let objTraduccionComentario = {
          texto: comentarioDto.traducciones[0].texto,
          idioma: codigoIdioma.codigo,
          original: true,
          referencia: idComentario,
          estado: getEstadoActivaTradComent,
        };

        //Guarda la traduccion del comentario
        traduccionComentario = await new this.traduccionComentarioIntercambioModel(
          objTraduccionComentario,
        ).save(opts);

        let historicoTraduccion: any = {
          datos: traduccionComentario,
          usuario: comentarioDto.coautor.coautor._id,
          accion: accionCrear.codigo,
          entidad: entidadTraduccion.codigo,
        };

        //Guarda el historico de la traduccion del comentario
        this.crearHistoricoService.crearHistoricoServer(historicoTraduccion);

        //Lista de traducciones comentarios

        traducciones.push(traduccionComentario._id);
        nuevoComentario.traducciones = traducciones;
      }
      if (comentarioDto.idPerfilRespuesta) {
        nuevoComentario.idPerfilRespuesta = comentarioDto.idPerfilRespuesta._id;
      } else {
        nuevoComentario.idPerfilRespuesta = null;
      }
      const comentario = await new this.comentarioIntercambioModel(
        nuevoComentario,
      ).save(opts);

      if (comentario.traducciones.length > 0) {
        //Si es diferente de idioma español
        if (codigoIdioma.codigo != codIdiomas.espanol) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.espanol,
            opts,
          );
        }
        //Si es diferente de idioma ingles
        if (codigoIdioma.codigo != codIdiomas.ingles) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.ingles,
            opts,
          );
        }
        //Si es diferente de idioma italiano
        if (codigoIdioma.codigo != codIdiomas.italiano) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.italiano,
            opts,
          );
        }
        //Si es diferente de idioma aleman
        if (codigoIdioma.codigo != codIdiomas.aleman) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.aleman,
            opts,
          );
        }
        //Si es diferente de idioma frances
        if (codigoIdioma.codigo != codIdiomas.frances) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.frances,
            opts,
          );
        }
        //Si es diferente de idioma portugues
        if (codigoIdioma.codigo != codIdiomas.portugues) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.portugues,
            opts,
          );
        }
      }

      const getUsuarioPerfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
        comentarioDto.coautor.coautor._id,
      );

      //Verifica si tiene adjuntos
      // _________________medias_____________________
      if (comentarioDto?.adjuntos && comentarioDto?.adjuntos.length > 0) {
        for (const media of comentarioDto.adjuntos) {
          let idMedia = media._id;

          ////cambia de estado sinAsignar a activa
          await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
            idMedia,
            getUsuarioPerfil._id,
            codIdioma,
            null,
            false,
            opts,
          );
          //Actualiza el array de medias
          await this.comentarioIntercambioModel.updateOne(
            { _id: comentario._id },
            { $push: { adjuntos: idMedia } },
            opts,
          );
        }
      }

      //datos para guardar el historico
      const newHistorico: any = {
        datos: comentario,
        usuario: comentarioDto.coautor.coautor._id,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      // actualizar participante y comentarios al intercambio
      let intercambioActualizado: any;

      if (existeParticipante) {
        intercambioActualizado = await this.intercambioModel.findOneAndUpdate(
          { _id: comentarioDto.intercambio._id },
          { $push: { comentarios: comentario._id } },
          opts,
        );
      } else {
        intercambioActualizado = await this.intercambioModel.findOneAndUpdate(
          { _id: comentarioDto.intercambio._id },
          {
            $push: {
              comentarios: comentario._id,
              participantes: participanteIntercambio._id,
            },
          },
          opts,
        );
      }

      const historicoActualizarIntercambio: any = {
        datos: intercambioActualizado,
        usuario: comentarioDto.coautor.coautor._id,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(
        historicoActualizarIntercambio,
      );

      const totalComentarios = await this.obtenerComentariosService.totalComentariosCoautor(
        comentarioDto.intercambio._id,
        comentarioDto.coautor.coautor._id,
        opts,
      );

      const participanteIntercambioActualizado = await this.participanteIntercambioModelo.findOneAndUpdate(
        { _id: participanteIntercambio._id },
        {
          $push: { comentarios: comentario._id },
          $set: { totalComentarios: totalComentarios },
        },
        opts,
      );

      return comentario;
    } catch (error) {
      throw error;
    }
  }

  async ObtenerConfiguracionEstilo(
    codigoEntidadComentario: string,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.configuracionEstilo,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      // random de colores para participante intercambio

      const confiEstilos = await this.configuracionEstiloModelo
        .find({ entidad: codigoEntidadComentario, estado: estado.codigo })
        .populate({
          path: 'estilos',
          //populate: { path: 'tipo', match: { nombre: 'fondo' } }, // fondo o letra
        });

      const arrayEstilos = [];
      for (const config of confiEstilos) {
        arrayEstilos.push(config);
      }

      arrayEstilos.map((conf, index) => {
        arrayEstilos[index].estilos = conf.estilos.filter(
          estilo => estilo.tipo != null,
        );
      });

      const random = Math.floor(Math.random() * arrayEstilos.length);

      return arrayEstilos[random];
    } catch (error) {
      throw error;
    }
  }
}
