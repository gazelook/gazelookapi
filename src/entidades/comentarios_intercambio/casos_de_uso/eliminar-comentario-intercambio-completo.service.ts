import { TraduccionComentario } from 'src/drivers/mongoose/interfaces/traduccion_comentario/traduccion-comentario.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Comentario } from 'src/drivers/mongoose/interfaces/comentario/comentario.interface';
import { Model } from 'mongoose';
import { EliminarAlbumCompletoService } from 'src/entidades/album/casos_de_uso/eliminar-album-completo.service';
import { ComentariosIntercambio } from 'src/drivers/mongoose/interfaces/comentarios_intercambio/comentarios-intercambio.interface';
import { TraduccionComentarioIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_comentario_intercambio/traduccion-comentario-intercambio.interface';

@Injectable()
export class EliminarComentarioIntercambioCompletoService {
  constructor(
    @Inject('COMENTARIOS_INTERCAMBIO_MODEL')
    private readonly comentarioIntercambioModel: Model<ComentariosIntercambio>,
    @Inject('TRADUCCION_COMENTARIO_INTERCAMBIO_MODEL')
    private readonly traduccionComentarioIntercambioModel: Model<
      TraduccionComentarioIntercambio
    >,
    private eliminarAlbumCompletoService: EliminarAlbumCompletoService,
  ) {}

  async eliminarComentarioCompleto(idComentario: any, opts: any): Promise<any> {
    try {
      const comentario = await this.comentarioIntercambioModel.findOne({
        _id: idComentario,
      });

      if (comentario) {
        if (comentario.adjuntos.length > 0) {
          //Elimina todos los adjuntos uno a uno
          for (const getAdjuntos of comentario.adjuntos) {
            await this.eliminarAlbumCompletoService.eliminarAlbumCompleto(
              getAdjuntos,
              opts,
            );
          }
        }
        //Elimina todas las traducciones del comentario
        await this.traduccionComentarioIntercambioModel.deleteMany(
          { referencia: idComentario },
          opts,
        );
        //Elimina el comentario
        await this.comentarioIntercambioModel.deleteOne(
          { _id: idComentario },
          opts,
        );
      }
      return true;
    } catch (error) {
      throw error;
    }
  }
}
