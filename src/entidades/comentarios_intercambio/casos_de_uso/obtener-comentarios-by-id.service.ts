import { TraduccionComentario } from 'src/drivers/mongoose/interfaces/traduccion_comentario/traduccion-comentario.interface';
import { TraducirComentarioService } from './traducir-comentario.service';
import { Inject, Injectable, HttpStatus, Res } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import {
  codIdiomas,
  codigoestadosTraaduccionComentario,
  codigosArchivosSistema,
  codigosCatalogoAlbum,
  codigosCatComentario,
  codigosConfiguracionEstilo,
  codigosEstadosTraduccionComentarioIntercambio,
  estadosProyecto,
  idiomas,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
  perfilFalso,
} from 'src/shared/enum-sistema';
import * as mongoose from 'mongoose';
import { GetFotoPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil-general.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { Response } from 'express';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { CatalogoColoresService } from 'src/entidades/catalogos/casos_de_uso/catalogo-colores.service';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { ObtenerMediasProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-medias-proyecto.service';
import { CatalogoConfiguracionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-configuracion.service';
import { ListaArchivosDefaultTipoService } from 'src/entidades/archivo/casos_de_uso/lista-archivos-default-tipo.service';
import { ConfiguracionEstilo } from 'src/drivers/mongoose/interfaces/configuracion_estilo/configuracion_estilo.interface';
import { ListaComentariosDto } from '../entidad/comentario-dto';
import { ComentariosIntercambio } from 'src/drivers/mongoose/interfaces/comentarios_intercambio/comentarios-intercambio.interface';
import { TraduccionComentarioIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_comentario_intercambio/traduccion-comentario-intercambio.interface';

@Injectable()
export class ObtenerComentariosIntercambioByIdService {
  constructor(
    @Inject('COMENTARIOS_INTERCAMBIO_MODEL')
    private readonly comentarioIntercambioModel: Model<ComentariosIntercambio>,
    @Inject('TRADUCCION_COMENTARIO_INTERCAMBIO_MODEL')
    private readonly traduccionComentarioIntercambioModel: Model<
      TraduccionComentarioIntercambio
    >,
    @Inject('CONFIGURACION_ESTILO_MODEL')
    private readonly configuracionEstiloModelo: Model<ConfiguracionEstilo>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirComentarioService: TraducirComentarioService,
    private getFotoPerfilService: GetFotoPerfilService,
    private traduccionMediaService: TraduccionMediaService,
    private catalogoColoresService: CatalogoColoresService,
    private catalogoEstilosService: CatalogoEstilosService,
    private obtenerMediasProyectoService: ObtenerMediasProyectoService,
    private catalogoConfiguracionService: CatalogoConfiguracionService,
    private listaArchivosDefaultTipoService: ListaArchivosDefaultTipoService,
  ) {}

  async obtenerComentariosById(
    listaComentarios: ListaComentariosDto,
    idioma: string,
    response?: Response,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarioIntercambio,
      );

      //Obtiene el estado activo
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      const codIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad proyectos
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      //Obtiene el codigo de la entidad participante proyecto
      const entidadConfigEstilo = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.configuracionEstilo,
      );
      let codEntidadConfigEstilo = entidadConfigEstilo.codigo;

      //Obtiene el estado activo de la entidad participante proyecto
      const estadoConfigEstilo = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadConfigEstilo,
      );
      let codEstadoConfigEstilo = estadoConfigEstilo.codigo;

      const populateTraduccion = {
        path: 'traducciones',
        select: 'texto idioma original',
        match: {
          idioma: codIdioma.codigo,
          estado: codigosEstadosTraduccionComentarioIntercambio.activa,
        },
      };
      //estado: estadoTraduccion.codigo
      const adjuntos = {
        path: 'adjuntos',
        select:
          'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma.codigo, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
        ],
      };

      const coautor = {
        path: 'coautor',
        select: 'coautor configuraciones',
        populate: [
          {
            path: 'coautor',
            select: 'nombre nombreContacto tipoPerfil album',
          },
          {
            path: 'configuraciones',
            select: 'codigo estilos tonoNotificacion tipo',
            match: { estado: codEstadoConfigEstilo },
            populate: [
              {
                path: 'estilos',
                select: 'codigo media color tipo',
                populate: [
                  {
                    path: 'media',
                    match: { estado: codEstadoMedia },
                    select:
                      'principal enlace miniatura fechaCreacion fechaActualizacion traducciones catalogoMedia',
                    populate: [
                      {
                        path: 'traducciones',
                        select: 'descripcion',
                        match: {
                          idioma: codIdioma.codigo,
                          estado: codEstadoTradMedia,
                        },
                      },
                      {
                        path: 'principal',
                        select:
                          ' url tipo duracion fileDefault fechaActualizacion',
                      },
                      {
                        path: 'miniatura',
                        select:
                          ' url tipo duracion fileDefault fechaActualizacion',
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      };

      const populateIdPerfilRespuesta = {
        path: 'idPerfilRespuesta',
        select: 'nombre nombreContacto tipoPerfil album',
      };

      let getComentarios;
      let dataComentarios = [];
      for (const getComentarioId of listaComentarios.listaComentarios) {
        getComentarios = await this.comentarioIntercambioModel
          .findOne({ _id: getComentarioId._id, estado: estado.codigo })
          .populate([
            populateTraduccion,
            adjuntos,
            coautor,
            populateIdPerfilRespuesta,
          ]);

        if (getComentarios) {
          let getTraduccion = [];
          //Inicio de traducciones

          if (getComentarios.traducciones.length === 0) {
            let tradIdi = await this.traducirComentarioService.traducir(
              getComentarios._id,
              idioma,
              opts,
              true,
            );

            if (tradIdi) {
              getTraduccion.push(tradIdi);
              if (tradIdi.idioma.codigo != codIdiomas.ingles) {
                let getTraduccionIngles = await this.traduccionComentarioIntercambioModel
                  .findOne({
                    referencia: getComentarios._id,
                    idioma: codIdiomas.ingles,
                    estado: codigoestadosTraaduccionComentario.activa,
                  })
                  .select('texto idioma original');

                if (getTraduccionIngles) {
                  let trad = {
                    _id: getTraduccionIngles._id,
                    texto: getTraduccionIngles.texto,
                    idioma: {
                      codigo: getTraduccionIngles.idioma,
                    },
                    original: getTraduccionIngles.original,
                  };
                  getTraduccion.push(trad);
                } else {
                  let tradIdiIngles = await this.traducirComentarioService.traducir(
                    getComentarios._id,
                    idiomas.ingles,
                    opts,
                    true,
                  );
                  if (tradIdiIngles) {
                    getTraduccion.push(tradIdiIngles);
                  }
                }
              }
            }
          } else {
            let tradOri = {
              _id: getComentarios.traducciones[0]._id,
              texto: getComentarios.traducciones[0].texto,
              idioma: {
                codigo: getComentarios.traducciones[0].idioma,
              },
              original: getComentarios.traducciones[0].original,
            };
            getTraduccion.push(tradOri);

            if (getComentarios.traducciones[0].idioma != codIdiomas.ingles) {
              let getTraduccionIngles = await this.traduccionComentarioIntercambioModel
                .findOne({
                  referencia: getComentarios._id,
                  idioma: codIdiomas.ingles,
                  estado: codigoestadosTraaduccionComentario.activa,
                })
                .select('texto idioma original');

              if (getTraduccionIngles) {
                let trad = {
                  _id: getTraduccionIngles._id,
                  texto: getTraduccionIngles.texto,
                  idioma: {
                    codigo: getTraduccionIngles.idioma,
                  },
                  original: getTraduccionIngles.original,
                };
                getTraduccion.push(trad);
              } else {
                let tradIdiIngles = await this.traducirComentarioService.traducir(
                  getComentarios._id,
                  idiomas.ingles,
                  opts,
                  true,
                );
                if (tradIdiIngles) {
                  getTraduccion.push(tradIdiIngles);
                }
              }
            }
          } //Fin de traducciones

          let media = [];
          //Verifica tiene adjuntos (medias)
          if (getComentarios.adjuntos.length > 0) {
            for (const getMedia of getComentarios.adjuntos) {
              let obMiniatura: any;

              //Verifica si la media tiene miniatura
              if (getMedia['miniatura']) {
                //Objeto de tipo miniatura (archivo)
                obMiniatura = {
                  _id: getMedia['miniatura']._id,
                  url: getMedia['miniatura'].url,
                  tipo: {
                    codigo: getMedia['miniatura'].tipo,
                  },
                  fileDefault: getMedia['miniatura'].fileDefault,
                  fechaActualizacion: getMedia['miniatura'].fechaActualizacion,
                };
              }
              //Objeto de tipo media
              let objMedia: any = {
                _id: getMedia['_id'],
                catalogoMedia: {
                  codigo: getMedia['catalogoMedia'],
                },
                principal: {
                  _id: getMedia['principal']._id,
                  url: getMedia['principal'].url,
                  tipo: {
                    codigo: getMedia['principal'].tipo,
                  },
                  fileDefault: getMedia['principal'].fileDefault,
                  fechaActualizacion: getMedia['principal'].fechaActualizacion,
                },
                miniatura: obMiniatura,
              };
              if (getMedia['principal'].duracion) {
                objMedia.principal.duracion = getMedia['principal'].duracion;
              }
              //Verifica si existe la traduccion de la media

              if (getMedia['traducciones'].length === 0) {
                console.log('getMediaaaaa: ', getMedia._id);
                const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                  getMedia['_id'],
                  idioma,
                  opts,
                );
                console.log('traduce: ', traduccionMedia);

                if (traduccionMedia) {
                  let arrayTrad = [];
                  const traducciones = {
                    _id: traduccionMedia._id,
                    descripcion: traduccionMedia.descripcion,
                  };
                  arrayTrad.push(traducciones);
                  objMedia.traducciones = arrayTrad;
                } else {
                  console.log('acaaaaaaaa');
                  objMedia.traducciones = [];
                }
              } else {
                let arrayTraducciones = [];
                arrayTraducciones.push(getMedia['traducciones'][0]);
                objMedia.traducciones = arrayTraducciones;
              }

              media.push(objMedia);
            }
          }

          let dataAlbum = [];

          let objPerfil: any;
          let objPartProyec: any;
          if (getComentarios.coautor['coautor']) {
            let arrayConfigPartic = [];
            //Llama al metodo de obtener foto de perfil
            let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
              getComentarios.coautor['coautor']._id,
            );

            dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);

            //Data para el mapeo de perfil
            objPerfil = {
              _id: getComentarios.coautor['coautor']._id,
              nombreContacto:
                getComentarios.coautor['coautor']['nombreContacto'],
              nombre: getComentarios.coautor['coautor']['nombre'],
              album: dataAlbum,
            };

            //Verifica si el participante tiene configuraciones
            if (getComentarios.coautor['configuraciones'].length > 0) {
              for (const confParticipante of getComentarios.coautor[
                'configuraciones'
              ]) {
                let arrayEstilosConfigPartic = [];
                //Verifica si tiene estilos
                if (confParticipante.estilos.length > 0) {
                  for (const estilosParticipante of confParticipante.estilos) {
                    //Obtiene el color
                    const getColor = await this.catalogoColoresService.obtenerCatalogoColoresByCodigo(
                      estilosParticipante.color,
                    );

                    //Objeto de tipo catalogo estilos
                    //Obtiene el catalogo estilos
                    const getCatEstilos = await this.catalogoEstilosService.obtenerCatalogoEstilosByCodigo(
                      estilosParticipante.tipo,
                    );

                    //Verifica si el estilo tiene media
                    let mediaEstiloPartic: any;
                    if (estilosParticipante.media) {
                      //Data de medias
                      //Llama al metodo del obtener medias
                      mediaEstiloPartic = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(
                        estilosParticipante.media,
                        idioma,
                        opts,
                      );
                    }
                    let objEstiloPartic = {
                      _id: estilosParticipante._id,
                      codigo: estilosParticipante.codigo,
                      color: getColor,
                      tipo: getCatEstilos,
                      media: mediaEstiloPartic,
                    };
                    arrayEstilosConfigPartic.push(objEstiloPartic);
                  }
                }

                //Verifica si la configuracion tiene tono de notificacion
                let objMediaTonoNotificacion: any;
                if (confParticipante.tonoNotificacion) {
                  //Data de medias
                  //Llama al metodo del obtener medias
                  objMediaTonoNotificacion = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(
                    confParticipante.tonoNotificacion,
                    idioma,
                    opts,
                  );
                }

                //Obtiene el catalogo configuracion segun el codigo
                const getCatConfiguracion = await this.catalogoConfiguracionService.obtenerCatalogoConfiguracionByCodigo(
                  confParticipante.tipo,
                );

                let objConfigPartic = {
                  _id: confParticipante._id,
                  codigo: confParticipante.codigo,
                  tonoNotificacion: objMediaTonoNotificacion,
                  estilos: arrayEstilosConfigPartic,
                  tipo: getCatConfiguracion,
                };
                arrayConfigPartic.push(objConfigPartic);
              }
            }
            //Data para el mapeo de participante proyecto
            objPartProyec = {
              _id: getComentarios.coautor['_id'],
              coautor: objPerfil,
              configuraciones: arrayConfigPartic,
            };
          } else {
            let arrayConfigPartic = [];
            const getArchivoFotoPerfil = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
              codigosArchivosSistema.archivo_default_foto_perfil,
            );
            let getUrlPrincipal = getArchivoFotoPerfil.principal.url;
            let getFileDefault = getArchivoFotoPerfil.principal.fileDefault;

            //Data para el mapeo del album solo con foto principal
            let objAlbumTipoPerfil = {
              tipo: {
                codigo: codigosCatalogoAlbum.catAlbumPerfil,
              },
              portada: {
                catalogoMedia: getArchivoFotoPerfil.catalogoMedia,
                principal: {
                  url: getUrlPrincipal,
                  fileDefault: getFileDefault,
                },
              },
            };
            dataAlbum.push(objAlbumTipoPerfil);
            //Data para el mapeo de perfil
            objPerfil = {
              nombreContacto: perfilFalso.no_existe,
              nombre: perfilFalso.no_existe,
              album: dataAlbum,
            };
            let getConfiguracion = this.configuracionEstiloModelo
              .findOne({ codigo: codigosConfiguracionEstilo.CONFEST12 })
              .populate({
                path: 'estilos',
              });

            if (getConfiguracion) {
              let arrayEstilosConfigPartic = [];
              //Verifica si tiene estilos
              if ((await getConfiguracion).estilos.length > 0) {
                for (const estilosParticipante of (await getConfiguracion)
                  .estilos) {
                  //Obtiene el color
                  const getColor = await this.catalogoColoresService.obtenerCatalogoColoresByCodigo(
                    estilosParticipante['color'],
                  );

                  //Objeto de tipo catalogo estilos
                  //Obtiene el catalogo estilos
                  const getCatEstilos = await this.catalogoEstilosService.obtenerCatalogoEstilosByCodigo(
                    estilosParticipante['tipo'],
                  );

                  //Verifica si el estilo tiene media
                  let mediaEstiloPartic: any;
                  if (estilosParticipante['media']) {
                    //Data de medias
                    //Llama al metodo del obtener medias
                    mediaEstiloPartic = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(
                      estilosParticipante['media'],
                      idioma,
                      opts,
                    );
                  }
                  let objEstiloPartic = {
                    _id: estilosParticipante['_id'],
                    codigo: estilosParticipante['codigo'],
                    color: getColor,
                    tipo: getCatEstilos,
                    media: mediaEstiloPartic,
                  };
                  arrayEstilosConfigPartic.push(objEstiloPartic);
                }
              }

              let objConfigPartic = {
                estilos: arrayEstilosConfigPartic,
              };
              arrayConfigPartic.push(objConfigPartic);
            }

            //Data para el mapeo de participante proyecto
            objPartProyec = {
              coautor: objPerfil,
              configuraciones: arrayConfigPartic,
            };
          }

          let objIdPerfilRespuesta: any;
          //Verificca si tiene el comentario un perfil de respuesta
          if (getComentarios.idPerfilRespuesta) {
            //Llama al metodo de obtener foto de perfil
            let getFotoIdPerfilRespuesta = await this.getFotoPerfilService.getFotoPerfil(
              getComentarios.idPerfilRespuesta._id,
            );
            let dataAlbumIdPerfilRespuesta = [];
            dataAlbumIdPerfilRespuesta.push(
              getFotoIdPerfilRespuesta.objAlbumTipoPerfil,
            );

            //Data para el mapeo de perfil respuesta
            objIdPerfilRespuesta = {
              _id: getComentarios.idPerfilRespuesta._id,
              nombreContacto: getComentarios.idPerfilRespuesta.nombreContacto,
              nombre: getComentarios.idPerfilRespuesta.nombre,
              album: dataAlbumIdPerfilRespuesta,
            };
          }

          let objComentarios = {
            _id: getComentarios._id,
            adjuntos: media,
            traducciones: getTraduccion,
            coautor: objPartProyec,
            importante: getComentarios.importante,
            idPerfilRespuesta: objIdPerfilRespuesta || null,
            fechaCreacion: getComentarios.fechaCreacion,
            fechaActualizacion: getComentarios.fechaActualizacion,
          };
          dataComentarios.push(objComentarios);
        }
      }
      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return dataComentarios;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async totalComentariosCoautor(
    proyecto: string,
    coautor: string,
    opts?: any,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarioIntercambio,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const totalComentarios = await this.comentarioIntercambioModel
        .find({ proyecto: proyecto, estado: estado.codigo, coautor: coautor })
        .session(opts.session);

      return totalComentarios.length;
    } catch (error) {
      throw error;
    }
  }

  async obtenerComentariosByIdIntercambio(intercambio: string): Promise<any> {
    try {
      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarioIntercambio,
      );

      //Obtiene el estado activo
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      const comentarios = await this.comentarioIntercambioModel.find({
        $and: [{ intercambio: intercambio }, { estado: estado.codigo }],
      });
      return comentarios;
    } catch (error) {
      throw error;
    }
  }
}
