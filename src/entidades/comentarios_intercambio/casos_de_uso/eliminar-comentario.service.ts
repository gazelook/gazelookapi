import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  codigosEstadosComentarioIntercambio,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ComentariosIntercambio } from 'src/drivers/mongoose/interfaces/comentarios_intercambio/comentarios-intercambio.interface';

@Injectable()
export class EliminarComentarioIntercambioService {
  constructor(
    @Inject('COMENTARIOS_INTERCAMBIO_MODEL')
    private readonly comentarioIntercambioModel: Model<ComentariosIntercambio>,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
  ) {}

  async eliminarComentario(
    idIntercambio: string,
    coautor: string,
    idComentario: string,
    opts?: any,
  ): Promise<any> {
    try {
      const comentario = await this.comentarioIntercambioModel.findOne({
        _id: idComentario,
      });

      if (comentario) {
        // Entitad comentario
        const entidadComentario = await this.catalogoEntidadService.obtenerNombreEntidad(
          nombreEntidades.comentarioIntercambio,
        );

        const accionEliminar = await this.catalogoAccionService.obtenerNombreAccion(
          nombreAcciones.eliminar,
        );

        const comentarioEliminado = await this.comentarioIntercambioModel.updateOne(
          {
            $and: [
              { _id: idComentario },
              { intercambio: idIntercambio },
              { coautor: coautor },
            ],
          },
          { $set: { estado: codigosEstadosComentarioIntercambio.eliminado } },
          opts,
        );

        //datos para guardar el historico
        const newHistorico: any = {
          datos: comentarioEliminado,
          usuario: comentarioEliminado.coautor,
          accion: accionEliminar.codigo,
          entidad: entidadComentario.codigo,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistorico);
        return comentarioEliminado;
      } else {
        return false;
      }
    } catch (error) {
      throw error;
    }
  }
}
