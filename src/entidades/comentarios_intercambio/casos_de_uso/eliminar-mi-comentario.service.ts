import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import * as mongoose from 'mongoose';
import { ComentariosIntercambio } from 'src/drivers/mongoose/interfaces/comentarios_intercambio/comentarios-intercambio.interface';
import { TraduccionComentarioIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_comentario_intercambio/traduccion-comentario-intercambio.interface';

@Injectable()
export class EliminarMiComentarioIntercambioService {
  constructor(
    @Inject('COMENTARIOS_INTERCAMBIO_MODEL')
    private readonly comentarioIntercambioModel: Model<ComentariosIntercambio>,
    @Inject('TRADUCCION_COMENTARIO_INTERCAMBIO_MODEL')
    private readonly traduccionComentarioIntercambioModel: Model<
      TraduccionComentarioIntercambio
    >,

    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
  ) {}

  async eliminarComentario(idComentario: string, idCoautor): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const comentario = await this.comentarioIntercambioModel.findOne({
        _id: idComentario,
      });

      if (comentario) {
        // Entitad comentario
        const entidadComentario = await this.catalogoEntidadService.obtenerNombreEntidad(
          nombreEntidades.comentarioIntercambio,
        );
        const estadoComentario = await this.catalogoEstadoService.obtenerNombreEstado(
          nombrecatalogoEstados.eliminado,
          entidadComentario.codigo,
        );
        const accionEliminar = await this.catalogoAccionService.obtenerNombreAccion(
          nombreAcciones.eliminar,
        );

        const comentarioEliminado = await this.comentarioIntercambioModel.updateOne(
          { $and: [{ _id: idComentario }, { coautor: idCoautor }] },
          { $set: { estado: estadoComentario.codigo } },
          opts,
        );

        //datos para guardar el historico
        const newHistorico: any = {
          datos: comentarioEliminado,
          usuario: comentarioEliminado.coautor,
          accion: accionEliminar.codigo,
          entidad: entidadComentario.codigo,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistorico);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return comentarioEliminado;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
