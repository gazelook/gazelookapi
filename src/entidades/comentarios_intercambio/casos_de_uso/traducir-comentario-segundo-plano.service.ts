import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import {
  codigosEstadosTraduccionComentarioIntercambio,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ComentariosIntercambio } from 'src/drivers/mongoose/interfaces/comentarios_intercambio/comentarios-intercambio.interface';
import { TraduccionComentarioIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_comentario_intercambio/traduccion-comentario-intercambio.interface';

@Injectable()
export class TraducirComentarioSegundoPlanoService {
  constructor(
    @Inject('COMENTARIOS_INTERCAMBIO_MODEL')
    private readonly comentarioIntercambioModel: Model<ComentariosIntercambio>,
    @Inject('TRADUCCION_COMENTARIO_INTERCAMBIO_MODEL')
    private readonly traduccionComentarioIntercambioModel: Model<
      TraduccionComentarioIntercambio
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async traducirComentario(
    idComentario: string,
    perfil: string,
    texto: string,
    idioma: string,
    opts: any,
  ) {
    //Obtiene el codigo del idioma
    const getCodIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      idioma,
    );
    let codIdioma = getCodIdioma.codigo;

    //Obtiene la accion crear
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene la entidad comentarios
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.comentarioIntercambio,
    );

    try {
      //llama al metodo de traduccion dinamica
      const comentarioTraducido = await traducirTexto(idioma, texto);

      //datos para guardar la nueva traduccion
      let newTraduccionComentario = {
        texto: comentarioTraducido.textoTraducido,
        idioma: codIdioma,
        original: false,
        referencia: idComentario,
        estado: codigosEstadosTraduccionComentarioIntercambio.activa,
      };

      //guarda la nueva traduccion
      const traduccion = await new this.traduccionComentarioIntercambioModel(
        newTraduccionComentario,
      ).save(opts);

      //datos para guardar el historico de la traduccion del comentario
      let historicoTraduccion: any = {
        datos: traduccion,
        usuario: perfil,
        accion: getAccion,
        entidad: entidad.codigo,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(historicoTraduccion);

      //Actualiza el array de id de traducciones
      await this.comentarioIntercambioModel.updateOne(
        { _id: idComentario },
        { $push: { traducciones: traduccion._id } },
        opts,
      );
    } catch (error) {
      throw error;
    }
  }
}
