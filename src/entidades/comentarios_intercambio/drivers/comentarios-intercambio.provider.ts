import { CatalogoEstilosModelo } from '../../../drivers/mongoose/modelos/catalogo_estilos/catalogo-estilos.schema';
import { EstiloModelo } from '../../../drivers/mongoose/modelos/estilo/estilo.schema';
import { ConfiguracionEstiloModelo } from '../../../drivers/mongoose/modelos/configuracion_estilo/configuracion-estilo.schema';
import { TipoComentarioModelo } from '../../../drivers/mongoose/modelos/catalogo_tipo_comentario/catalogo-tipo-comentario.schema';
import { Connection } from 'mongoose';
import { IntercambioModelo } from 'src/drivers/mongoose/modelos/intercambio/intercambio.schema';
import { ComentarioIntercambioModelo } from 'src/drivers/mongoose/modelos/comentarioIntercambio/comentarioIntercambio.schema';
import { TraduccionIntercambioComentarioModelo } from 'src/drivers/mongoose/modelos/traduccion_intercambio_comentario/traduccion-intercambio-comentario.schema';

export const comentariosProviders = [
  {
    provide: 'INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('intercambio', IntercambioModelo, 'intercambio'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'COMENTARIOS_INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'comentarios_intercambio',
        ComentarioIntercambioModelo,
        'comentarios_intercambio',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_COMENTARIO_INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_comentario_intercambio',
        TraduccionIntercambioComentarioModelo,
        'traduccion_comentario_intercambio',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_TIPO_COMENTARIO',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_tipo_comentario',
        TipoComentarioModelo,
        'catalogo_tipo_comentario',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_ESTILO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_estilo',
        ConfiguracionEstiloModelo,
        'configuracion_estilo',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ESTILO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('estilos', EstiloModelo, 'estilos'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_ESTILOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_estilos',
        CatalogoEstilosModelo,
        'catalogo_estilos',
      ),
    inject: ['DB_CONNECTION'],
  },
];
