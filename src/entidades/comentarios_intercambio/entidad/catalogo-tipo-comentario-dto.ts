import { ApiProperty } from '@nestjs/swagger';
import { bool } from 'aws-sdk/clients/signer';

export class CatalogoTipoComentarioDto {
  @ApiProperty({
    required: true,
    description: 'Catalogo tipo comentario',
    example: 'Codigo tipo comentario : CATIPCOM_1',
  })
  codigo: string;
}

export class identComentarioDto {
  @ApiProperty()
  _id: string;
}
