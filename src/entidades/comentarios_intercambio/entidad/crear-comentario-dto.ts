import { ApiProperty } from '@nestjs/swagger';
import { mediaId } from 'src/entidades/media/entidad/archivo-resultado.dto';
import { idPerfilDto } from 'src/entidades/perfil/entidad/crear-perfil.dto';
import { ObtenerUsuariosSistemaDto } from 'src/entidades/perfil/entidad/obtenerPerfiles.dto';

export class Coautor {
  @ApiProperty()
  participanteProyecto: { coautor: string };
}

class Perfil {
  @ApiProperty({
    required: true,
    description: 'Identificador del perfil del comentario',
    example: '5f5a4a50794fbb6ee32162b8',
  })
  _id: string;
}
export class Traduccion {
  @ApiProperty()
  texto: string;
}

export class CoautorComentario {
  @ApiProperty({ type: Perfil })
  coautor: Perfil;
}

export class CoautorComentarioCompleto {
  @ApiProperty({ type: ObtenerUsuariosSistemaDto })
  coautor: ObtenerUsuariosSistemaDto;
}

class CrearIntercambio {
  @ApiProperty()
  _id: string;
}

class ParticipanteInteracmbiosDTO {
  @ApiProperty()
  perfil: string;
}

export class CrearComentarioDto {
  // @ApiProperty({type:CatalogoTipoComentarioDto})
  // tipo: CatalogoTipoComentarioDto;

  @ApiProperty({ type: [mediaId] })
  adjuntos: [mediaId];

  @ApiProperty({ required: true, type: CoautorComentario })
  coautor: CoautorComentario;

  @ApiProperty({ description: 'Indica la prioridad del comentario' })
  importante: any;

  @ApiProperty({
    required: true,
    description: 'Traducciones en diferentes idiomas del comentario',
    type: [Traduccion],
  })
  traducciones: Traduccion[];

  @ApiProperty({
    required: true,
    description: 'Id del intercambio al que pertenece el comentario',
  })
  intercambio: CrearIntercambio;

  @ApiProperty({ type: idPerfilDto })
  idPerfilRespuesta: idPerfilDto;
}
