// import { ObtenerComentariosHistoricoControlador } from './obtener-comentarios-historico.controller';
import { EliminarMiComentarioControlador } from './eliminar-mi-comentario.controller';
import { ObtenerComentariosProyectoControlador } from './obtener-comentarios-intercambio.controller';
import { CrearComentarioControlador } from './crear-comentario.controller';
import { Module } from '@nestjs/common';
import { ComentariosIntercambioServicesModule } from '../casos_de_uso/comentarios-intercambio-services.module';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { EliminarComentarioControlador } from './eliminar-comentario.controller';
import { Funcion } from 'src/shared/funcion';
import { ObtenerComentariosByIdControlador } from './obtener-comentarios-by-id.controller';

@Module({
  imports: [ComentariosIntercambioServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    CrearComentarioControlador,
    EliminarMiComentarioControlador,
    EliminarComentarioControlador,
    ObtenerComentariosProyectoControlador,
    // ObtenerComentariosHistoricoControlador,
    ObtenerComentariosByIdControlador,
  ],
})
export class ComentariosIntercambioControllerModule {}
