import {
  ObtencionComentarioDto,
  RespuestaComentarioDto,
} from '../entidad/respuesta-comentario-dto';
import { ApiResponse, ApiTags, ApiHeader, ApiOperation } from '@nestjs/swagger';
import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  HttpException,
  Inject,
  Get,
  Headers,
  Param,
  BadRequestException,
  UseGuards,
  Query,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';

import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { Response } from 'express';
import { AuthGuard } from '@nestjs/passport';

import * as mongoose from 'mongoose';
import { idiomas } from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { ListaComentariosDto } from '../entidad/comentario-dto';
import { ObtenerComentariosIntercambioByIdService } from '../casos_de_uso/obtener-comentarios-by-id.service';

@ApiTags('Comentarios-Intercambio')
@Controller('api/comentario-intercambio')
export class ObtenerComentariosByIdControlador {
  constructor(
    private readonly obtenerComentariosByIdService: ObtenerComentariosIntercambioByIdService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Post('/lista-comentario-id')
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiOperation({
    summary: 'Devuelve una lista de cometarios segun una lista de IDs',
  })
  @ApiResponse({ status: 200, type: ObtencionComentarioDto, description: 'OK' })
  @ApiResponse({ status: 404, description: 'Parámetros no válidos' })
  @UseGuards(AuthGuard('jwt'))
  public async listarComentariosById(
    @Headers() headers,
    @Body() listaComentarios: ListaComentariosDto,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      let idValido = true;
      if (listaComentarios.listaComentarios.length > 0) {
        idValido = true;
        for (const idComentario of listaComentarios.listaComentarios) {
          if (!mongoose.isValidObjectId(idComentario._id)) {
            idValido = false;
            break;
          }
        }
      } else {
        idValido = false;
      }

      if (idValido) {
        const comentarios = await this.obtenerComentariosByIdService.obtenerComentariosById(
          listaComentarios,
          codIdioma,
          response,
        );

        if (comentarios.length > 0) {
          //llama al metodo de traduccion estatica
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: comentarios,
          });
          response.send(respuesta);
          return respuesta;
        } else {
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: [],
          });
          response.send(respuesta);
          return respuesta;
        }
      } else {
        //llama al metodo de traduccion estatica
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
