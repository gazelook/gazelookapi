import { ApiResponse, ApiTags, ApiHeader, ApiOperation } from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  HttpException,
  UseGuards,
  Delete,
  Param,
  Headers,
  Query,
} from '@nestjs/common';

import { AuthGuard } from '@nestjs/passport';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { isValidObjectId } from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { EliminarComentarioIntercambioService } from '../casos_de_uso/eliminar-comentario.service';
import { idiomas } from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';

@ApiTags('Comentarios-Intercambio')
@Controller('api/comentario-intercambio')
export class EliminarComentarioControlador {
  constructor(
    private readonly eliminarComentarioService: EliminarComentarioIntercambioService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}
  @Delete('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary:
      'Elimina un comentario realizado a mi intercambio. Los comentarios eliminados se cambian a un estado historico ',
  })
  @ApiResponse({ status: 200, description: 'Eliminación correcta' })
  @ApiResponse({ status: 406, description: 'El usuario no tiene comentarios' })
  public async crearComentario(
    @Headers() headers,
    @Query('idIntercambio') idIntercambio: string,
    @Query('coautor') coautor: string,
    @Query('idComentario') idComentario: string,
  ) {
    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

      if (
        mongoose.isValidObjectId(idIntercambio) &&
        mongoose.isValidObjectId(idComentario) &&
        mongoose.isValidObjectId(coautor)
      ) {
        const eliminarComentario = await this.eliminarComentarioService.eliminarComentario(
          idIntercambio,
          coautor,
          idComentario,
        );
        if (eliminarComentario) {
          const ELIMINACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ELIMINACION_CORRECTA',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        } else {
          const FALLO_ELIMINAR = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'FALLO_ELIMINAR',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: FALLO_ELIMINAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
    }
  }
}
