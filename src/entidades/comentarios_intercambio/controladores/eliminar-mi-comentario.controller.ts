import { EliminarMiComentarioIntercambioService } from '../casos_de_uso/eliminar-mi-comentario.service';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiQuery,
} from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  HttpException,
  UseGuards,
  Delete,
  Param,
  Headers,
  Query,
} from '@nestjs/common';

import { AuthGuard } from '@nestjs/passport';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { isValidObjectId } from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { idiomas } from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Comentarios-Intercambio')
@Controller('api/comentario-intercambio')
export class EliminarMiComentarioControlador {
  constructor(
    private readonly eliminarComentarioService: EliminarMiComentarioIntercambioService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}
  @Delete('/mi-comentario')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary: 'Elimina mi comentario realizado en un intercambio',
  })
  @ApiResponse({ status: 200, description: 'Eliminación correcta' })
  @ApiResponse({ status: 406, description: 'El usuario no tiene comentarios' })
  // @ApiQuery({ name: 'idComentario', required: false})
  public async crearComentario(
    @Headers() headers,
    @Query('idComentario') idComentario: string,
    @Query('idCoautor') idCoautor?: string,
  ) {
    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

      if (isValidObjectId(idComentario)) {
        const eliminarMiCom = await this.eliminarComentarioService.eliminarComentario(
          idComentario,
          idCoautor,
        );
        if (eliminarMiCom) {
          const ELIMINACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ELIMINACION_CORRECTA',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        } else {
          const ERROR_ELIMINAR = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ERROR_ELIMINAR',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
    }
  }
}
