// import { ObtencionComentarioDto, RespuestaComentarioDto } from '../entidad/respuesta-comentario-dto';
// import { ObtenerComentariosService } from '../casos_de_uso/obtener-comentarios-intercambio.service';
// import { ApiResponse, ApiTags, ApiHeader, ApiOperation } from '@nestjs/swagger';
// import { Controller, Post, Body, Res, HttpStatus, HttpException, Inject, Get, Headers, Param, BadRequestException, UseGuards, Query} from '@nestjs/common';
// import { RespuestaInterface } from 'src/shared/respuesta-interface';

// import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
// import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
// import { Response } from 'express';
// import { AuthGuard } from '@nestjs/passport';

// import * as mongoose from 'mongoose';
// import { ObtenerComentariosHistoricoService } from '../casos_de_uso/obtener-comentarios-historico.service';
// import { idiomas } from 'src/shared/enum-sistema';
// import { Funcion } from 'src/shared/funcion';

// @ApiTags('Comentarios')
// @Controller('api/comentario-historico')
// export class ObtenerComentariosHistoricoControlador {
//   constructor(
//     private readonly obtenerComentariosHistoricoService: ObtenerComentariosHistoricoService,
//     private readonly traduccionEstaticaController: TraduccionEstaticaController,
//     private readonly funcion: Funcion
//   ) {}

//   @Get('/')
//   @ApiHeader({
//     name: 'apiKey',
//     description: 'key para usar las rutas',
//     required: true,
//   })
//   @ApiHeader({
//     name: 'idioma',
//     description: 'codigo del idioma. Ejm: es, en, fr',
//   })
//   @ApiOperation({ summary: 'Devuelve una lista de cometarios de un proyecto según estado' })
//   @ApiResponse({ status: 200, type: ObtencionComentarioDto, description: 'OK' })
//   @ApiResponse({ status: 404, description: 'El proyecto no tiene comentarios' })
//   @UseGuards(AuthGuard('jwt'))
//   public async listarComentariosHistoricoProyecto(
//     @Headers() headers,
//     @Query('idProyecto') idProyecto: string,
//     @Query('codigoEstado') codigoEstado: string,
//     @Query('limite') limite: number,
//     @Query('pagina') pagina: number,
//     @Query('fechaInicial') fechaInicial: Date,
//     @Query('fechaFinal') fechaFinal: Date,
//     @Res() response: Response,
//   ) {

//     const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma)
//     try {
//       if (mongoose.isValidObjectId(idProyecto)) {
//         const comentarios = await this.obtenerComentariosHistoricoService.obtenerComentariosProyecto(
//             idProyecto,
//             codigoEstado,
//             codIdioma,
//             limite,
//             pagina,
//             fechaInicial,
//             fechaFinal,
//             response
//         );

//           const  respuesta= this.funcion.enviarRespuestaOptimizada({codigoEstado : HttpStatus.OK,  datos: comentarios})
//           response.send(respuesta);
//           return respuesta;

//       } else {
//         const PARAMETROS_NO_VALIDOS= await this.traduccionEstaticaController.traduccionEstatica(codIdioma, 'PARAMETROS_NO_VALIDOS');
//         const  respuesta= this.funcion.enviarRespuestaOptimizada({codigoEstado : HttpStatus.NOT_ACCEPTABLE,  mensaje: PARAMETROS_NO_VALIDOS})
//         response.send(respuesta);
//         return respuesta;
//       }
//     } catch (e) {
//         const  respuesta= this.funcion.enviarRespuestaOptimizada({codigoEstado : HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message})
//         response.send(respuesta);
//         return respuesta;
//     }
//   }
// }
