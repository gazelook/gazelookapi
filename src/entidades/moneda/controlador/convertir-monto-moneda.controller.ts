import { Controller, Headers, HttpStatus, Post, Query } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { CatalogoTipoMonedaService } from 'src/entidades/catalogos/casos_de_uso/catalogo-tipo-moneda.service';
import { Funcion } from 'src/shared/funcion';
import { ConvertirMontoMonedaService } from '../casos_de_uso/convertir-monto-moneda.service';
import { ConvertirMontoMonedaDto } from '../entidad/moneda.dto';

@ApiTags('Moneda')
@Controller('api/convertir-monto')
export class ConvertirMontoMonedaControlador {
  constructor(
    private readonly convertirMontoMonedaService: ConvertirMontoMonedaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private readonly catalogoTipoMonedaService: CatalogoTipoMonedaService,
  ) {}

  @Post('/')
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiResponse({
    status: 200,
    type: ConvertirMontoMonedaDto,
    description: 'Monto convertido de una moneda a otra',
  })
  @ApiOperation({
    summary:
      'Devuelve el monto convertido de una moneda a otra, ejemplo: convertirDe: EUR, convertirA: USD',
  })
  @ApiResponse({ status: 404, description: 'Error al convertir la moneda' })
  @ApiResponse({ status: 406, description: 'Parámetros no válidos' })
  public async convertirMontoMoneda(
    @Headers() headers,
    @Query('monto') monto: number,
    @Query('convertirDe') convertirDe: string,
    @Query('convertirA') convertirA: string,
  ) {
    //Verifica si el idioma llega vacio
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      //Verifica si el codido de la moneda que se ingresa esta dentro de los codigos permitidos
      const getConvertirDe = this.catalogoTipoMonedaService.obtenerCatalogoTipoMonedaByCodNombre(
        convertirDe,
      );
      const getConvertirA = this.catalogoTipoMonedaService.obtenerCatalogoTipoMonedaByCodNombre(
        convertirA,
      );

      if (getConvertirA && getConvertirDe) {
        const getConversion = await this.convertirMontoMonedaService.convertirMoney(
          monto,
          convertirDe,
          convertirA,
        );
        if (getConversion) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: getConversion,
          });
        } else {
          const ERROR_CONVERSION_MONEDA = await this.i18n.translate(
            codIdioma.concat('.ERROR_CONVERSION_MONEDA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CONVERSION_MONEDA,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
