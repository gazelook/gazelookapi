import { Module } from '@nestjs/common';
import { MonedaControllerModule } from './controlador/moneda.controller.module';

@Module({
  imports: [MonedaControllerModule],
  providers: [],
  controllers: [],
})
export class MonedaModule {}
