import { Injectable } from '@nestjs/common';
var oxr = require('open-exchange-rates'),
  fx = require('money');
oxr.set({ app_id: 'bd9f0c93e55a43fc87af68f850592ebc' }); //Clave de api

@Injectable()
export class ConvertirMontoMonedaService {
  constructor() {}

  //Metodo para convertir de monto($) (de moneda Dolar (USD) a cualquier otra EJM:(EUR), revisar enum-lista-money)
  async convertirMoney(
    monto: number,
    convertirDe: any,
    convertirA: any,
  ): Promise<number> {
    return new Promise(async function(resolve, reject) {
      oxr.latest(async error => {
        if (error) {
          console.log(
            'ERROR loading data from Open Exchange Rates API! Error was:',
          );
          reject(error.toString());
        } else {
          fx.rates = oxr.rates;
          fx.base = oxr.base;

          // convertor
          const getCambioMoneda = await fx(monto)
            .from(convertirDe)
            .to(convertirA);
          resolve(getCambioMoneda);
        }
      }, 500);
    });
  }
}
