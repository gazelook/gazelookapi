import { Module } from '@nestjs/common';
import { DBModule } from '../../../drivers/db_conection/db.module';
import { MonedaProviders } from '../drivers/moneda.provider';
import { ConvertirMontoMonedaService } from './convertir-monto-moneda.service';

@Module({
  imports: [DBModule],
  providers: [...MonedaProviders, ConvertirMontoMonedaService],
  exports: [ConvertirMontoMonedaService],
  controllers: [],
})
export class MonedaServicesModule {}
