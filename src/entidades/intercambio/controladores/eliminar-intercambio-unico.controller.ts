import {
  Controller,
  Delete,
  Headers,
  HttpStatus,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { EliminarIntercambioUnicoService } from '../casos_de_uso/eliminar-intercambio-unico.service';

@ApiTags('Intercambio')
@Controller('api/intercambio')
export class EliminarEliminarUnicoControlador {
  constructor(
    private readonly eliminarIntercambioUnicoService: EliminarIntercambioUnicoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Delete('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Elimina logicamente un intercambio' })
  @ApiResponse({ status: 200, description: 'Eliminación correcta' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se puede eliminar el documento',
  })
  @UseGuards(AuthGuard('jwt'))
  public async eliminarIntercambioUnico(
    @Headers() headers,
    @Query('perfil') perfil: string,
    @Query('intercambio') intercambio: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(intercambio) &&
      mongoose.isValidObjectId(perfil)
    ) {
      try {
        const getIntercambio = await this.eliminarIntercambioUnicoService.eliminarIntercambioUnico(
          perfil,
          intercambio,
        );

        if (getIntercambio === true) {
          const ELIMINACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ELIMINACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        } else {
          const ERROR_ELIMINAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ELIMINAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
