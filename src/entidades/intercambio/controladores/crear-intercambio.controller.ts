import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CrearIntercambioService } from '../casos_de_uso/crear-intercambio.service';
import { CrearIntercambioDto } from '../entidad/crear-intercambio-dto';
import { IntercambioDto } from '../entidad/intercambio-dto';

@ApiTags('Intercambio')
@Controller('api/intercambio')
export class CrearIntercambioControlador {
  constructor(
    private readonly crearIntercambioService: CrearIntercambioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: CrearIntercambioDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Crea un nuevo intercambio' })
  @ApiResponse({
    status: 201,
    type: IntercambioDto,
    description: 'Creación correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al crear' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async crearIntercambio(
    @Headers() headers,
    @Body() crearIntercambioDto: CrearIntercambioDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(crearIntercambioDto.perfil._id) &&
        crearIntercambioDto.tipo.codigo &&
        crearIntercambioDto.tipoIntercambiar.codigo &&
        crearIntercambioDto.direccion &&
        // && crearIntercambioDto.traducciones[0].descripcion
        crearIntercambioDto.traducciones[0].titulo &&
        crearIntercambioDto.traducciones[0].tituloCorto
      ) {
        const intercambio = await this.crearIntercambioService.crearIntercambio(
          crearIntercambioDto,
          codIdioma,
        );

        if (intercambio) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
            datos: { _id: intercambio._id },
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
