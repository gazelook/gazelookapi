import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ActualizarIntercambioService } from '../casos_de_uso/actualizar-intercambio.service';
import { ActualizarIntercambioDto } from '../entidad/actualizar-intercambio.dto';
import { IntercambioDto } from '../entidad/intercambio-dto';

@ApiTags('Intercambio')
@Controller('api/intercambio')
export class ActualizarIntercambioControlador {
  constructor(
    private readonly actualizarIntercambioService: ActualizarIntercambioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/actualizar-intercambio')
  @ApiSecurity('Authorization')
  @ApiBody({ type: ActualizarIntercambioDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Actualiza la información de un intercambio' })
  @ApiResponse({
    status: 200,
    type: IntercambioDto,
    description: 'Actualización correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarIntercambio(
    @Headers() headers,
    @Body() actualizarIntercambioDto: ActualizarIntercambioDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(actualizarIntercambioDto._id) &&
        mongoose.isValidObjectId(actualizarIntercambioDto.perfil._id)
      ) {
        const intercambio = await this.actualizarIntercambioService.actualizarIntercambio(
          actualizarIntercambioDto,
          actualizarIntercambioDto._id,
          codIdioma,
        );

        if (intercambio) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
            datos: intercambio,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
