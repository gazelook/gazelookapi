import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CambiarEstatusIntercambioService } from '../casos_de_uso/cambiar-estatus-intercambio.service';
import { ActualizarStatusIntercambioDto } from '../entidad/crear-intercambio-dto';

@ApiTags('Intercambio')
@Controller('api/intercambio')
export class CambiarStatusIntercambioControlador {
  constructor(
    private readonly cambiarEstatusIntercambioService: CambiarEstatusIntercambioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/cambia-status-intercambio')
  @ApiSecurity('Authorization')
  @ApiBody({ type: ActualizarStatusIntercambioDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Cambia el status del intercambio segund el id y perfil enviado (solo puede realizarlo si es el dueño de la publicación)',
  })
  @ApiResponse({ status: 200, description: 'Actialización correcta' })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async crearIntercambio(
    @Headers() headers,
    @Body() actualizarStatusDto: ActualizarStatusIntercambioDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(actualizarStatusDto.perfil._id) &&
        actualizarStatusDto.perfil._id
      ) {
        const intercambio = await this.cambiarEstatusIntercambioService.cambiarStatusIntercambio(
          actualizarStatusDto._id,
          actualizarStatusDto.perfil._id,
        );

        if (intercambio) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
