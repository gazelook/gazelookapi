import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { CatalogoTipoIntercambioService } from '../casos_de_uso/obtener-tipo-intercambio.service';
import { CatalogoTipoIntercambioDto } from '../entidad/obtener-tipo-intercambio-dto';

@ApiTags('Intercambio')
@Controller('api/intercambio')
export class ObtenerCatalogoTipoIntercambioControlador {
  constructor(
    private readonly catalogoTipoIntercambioService: CatalogoTipoIntercambioService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Get('/tipos-intercambio')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Obtiene los tipos de intercambios segun el idioma enviado',
  })
  @ApiResponse({
    status: 200,
    type: CatalogoTipoIntercambioDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerCatalogoTipoIntercambio(@Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const catalogoTipoIntercambio = await this.catalogoTipoIntercambioService.obtenerCatalogoTipoIntercambio(
        codIdioma,
      );

      if (!catalogoTipoIntercambio || catalogoTipoIntercambio.length === 0) {
        //llama al metodo de traduccion dinamica
        const ERROR_OBTENER = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_OBTENER',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ERROR_OBTENER,
        });
      }
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: catalogoTipoIntercambio,
      });
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
