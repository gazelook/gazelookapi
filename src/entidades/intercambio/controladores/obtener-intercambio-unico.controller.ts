import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Get,
  Headers,
  UseGuards,
  Query,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import * as mongoose from 'mongoose';
import { AuthGuard } from '@nestjs/passport';
import { Funcion } from 'src/shared/funcion';
import { ObtenerIntercambioUnicoService } from '../casos_de_uso/obtener-intercambio-unico.service';
import { IntercambioCompletoDto } from '../entidad/intercambio-response-dto';

@ApiTags('Intercambio')
@Controller('api/intercambio')
export class ObtenerIntercambioUnicoControlador {
  constructor(
    private readonly obtenerIntercambioUnicoService: ObtenerIntercambioUnicoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Devuelve la información completa de un intercambio',
  })
  @ApiResponse({ status: 200, type: IntercambioCompletoDto, description: 'OK' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerIntercambioUnico(
    @Headers() headers,
    @Query('idIntercambio') idIntercambio: string,
    @Query('idPerfil') idPerfil: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(idIntercambio) &&
        mongoose.isValidObjectId(idPerfil)
      ) {
        const intercambio = await this.obtenerIntercambioUnicoService.obtenerIntercambioUnico(
          idIntercambio,
          idPerfil,
          codIdioma,
        );

        if (intercambio) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: intercambio,
          });
        } else {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_OBTENER,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
