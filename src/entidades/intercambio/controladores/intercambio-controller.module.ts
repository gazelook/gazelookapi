import { Module } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
// import { NotificacionServicesModule } from 'src/entidades/notificacion/casos_de_uso/notificacion-services.module';
import { IntercambioServicesModule } from '../casos_de_uso/intercambio-services.module';
import { ActualizarIntercambioControlador } from './actualizar-intercambio.controller';
import { CambiarStatusIntercambioControlador } from './cambiar_status_intercambio.controller';
import { CrearIntercambioControlador } from './crear-intercambio.controller';
import { EliminarEliminarUnicoControlador } from './eliminar-intercambio-unico.controller';
import { ObtenerCatalogoTipoIntercambioControlador } from './obtener-catalogo-tipo-intercambio.controller';
import { ObtenerIntercambiosRecientesPaginacionControlador } from './obtener-intercambio-reciente-paginacion.controller';
import { ObtenerIntercambioUnicoControlador } from './obtener-intercambio-unico.controller';
import { ObtenerIntercambiosRecientesPerfilControlador } from './obtener-intercambios-reciente-perfil.controller';

@Module({
  imports: [
    IntercambioServicesModule,
    // forwardRef(() => NotificacionServicesModule
    // ),
  ],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    CrearIntercambioControlador,
    CambiarStatusIntercambioControlador,
    ActualizarIntercambioControlador,
    ObtenerCatalogoTipoIntercambioControlador,
    ObtenerIntercambiosRecientesPaginacionControlador,
    EliminarEliminarUnicoControlador,
    ObtenerIntercambioUnicoControlador,
    ObtenerIntercambiosRecientesPerfilControlador,
  ],
})
export class IntercambioControllerModule {}
