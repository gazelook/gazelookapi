import { Response } from 'express';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Res,
  HttpStatus,
  Get,
  Headers,
  Query,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';
import { ObtenerIntercambiosRecientePaginacionService } from '../casos_de_uso/obtener-intercambios-recientes-paginacion.service';
import { IntercambioResumidoPaginacionResponseDto } from '../entidad/intercambio-response-dto';

@ApiTags('Intercambio')
@Controller('api/intercambio')
export class ObtenerIntercambiosRecientesPaginacionControlador {
  constructor(
    private readonly obtenerIntercambiosRecientePaginacionService: ObtenerIntercambiosRecientePaginacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/intercambios-recientes')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary:
      'Obtiene una lista de Intercambios, en un rango de fechas y filtros con paginacion',
  })
  @ApiResponse({
    status: 200,
    type: IntercambioResumidoPaginacionResponseDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerIntercambiosRecientes(
    @Headers() headers,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Query('titulo') titulo: string,
    @Query('tipo') tipo: string,
    @Query('pais') pais: string,
    @Query('localidad') localidad: string,
    @Query('fechaInicial') fechaInicial: Date,
    @Query('fechaFinal') fechaFinal: Date,
    @Query('tipoIntercambiar') tipoIntercambiar: string,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0 &&
        tipo
      ) {
        const intercambios = await this.obtenerIntercambiosRecientePaginacionService.obtenerIntercambiosRecientePaginacion(
          codIdioma,
          limite,
          pagina,
          titulo,
          tipo,
          tipoIntercambiar,
          pais,
          localidad,
          fechaInicial,
          fechaFinal,
          response,
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: intercambios,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
