import { ApiProperty } from '@nestjs/swagger';

export class TraduccionCatalogoTipoIntercambioDto {
  @ApiProperty({
    description: 'Codigo del catalogo tipo intercambio',
    example: 'CATIPOINTER_1',
  })
  catalogoTipoIntercambio: string;
  @ApiProperty({
    description: 'Nombre del catalogo tipo proyecto',
    example: 'Objetos o Productos',
  })
  nombre: string;
  @ApiProperty({
    description: 'Descripción del catalogo tipo proyecto',
    example: 'Objetos o Productos',
  })
  descripcion: string;
  @ApiProperty({
    description: 'codigo del idioma del tipo proyecto',
    example: 'IDI_1',
  })
  idioma: string;
  @ApiProperty({
    description: 'codigo del idioma del tipo proyecto',
    example: true,
  })
  original: boolean;
}

export class IdCatalogoTipoIntercambioDto {
  @ApiProperty({
    description: 'Identificador de la traduccion del catalogo tipo intercambio',
    example: '5f3eaacfd9d7099cad68c632',
  })
  _id: string;
  @ApiProperty({
    description: 'Nombre del catalogo tipo intercambio',
    example: 'Objetos o Productos',
  })
  nombre: string;
  @ApiProperty({
    description: 'Descripción del catalogo tipo intercambio',
    example: 'Objetos o Productos',
  })
  descripcion: string;
  @ApiProperty({
    description: 'codigo del idioma del tipo intercambio',
    example: 'IDI_1',
  })
  idioma: string;
  @ApiProperty({
    description: 'codigo del idioma del tipo intercambio',
    example: true,
  })
  original: boolean;
}
