import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class TraduccionIntercambioDto {
  @ApiProperty({ description: 'titulo resumido', example: 'titulo resumido' })
  @IsString()
  @IsNotEmpty()
  tituloCorto: string;

  @ApiProperty({ description: 'titulo largo', example: 'titulo largo' })
  @IsString()
  @IsNotEmpty()
  titulo: string;

  @ApiProperty({
    description: 'enunciado que describe el intercambio',
    example: 'Esta es la descripción del intercambio',
  })
  @IsString()
  @IsNotEmpty()
  descripcion: string;
}

export class TraduccionResumidaIntercambioDto {
  @ApiProperty({ description: 'Titulo del intercambio', example: 'Titulo' })
  titulo: string;

  @ApiProperty({
    description: 'Titulo corto del intercambio',
    example: 'Titulo corto',
  })
  tituloCorto: string;

  @ApiProperty({
    description: 'Descripción del intercambio',
    example: 'Descripción',
  })
  descripcion: string;
}

export class CrearTraduccionProyectoDto {
  @ApiProperty({ description: 'titulo resumido', example: 'titulo resumido' })
  @IsString()
  @IsNotEmpty()
  tituloCorto: string;

  @ApiProperty({ description: 'titulo largo', example: 'titulo largo' })
  @IsString()
  @IsNotEmpty()
  titulo: string;

  @ApiProperty({
    description: 'enunciado que describe el proyecto en su totalidad',
    example: 'Esta es la descripción del proyecto',
  })
  @IsString()
  @IsNotEmpty()
  descripcion: string;
}
