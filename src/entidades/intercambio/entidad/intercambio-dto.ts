import { ApiProperty } from '@nestjs/swagger';
import { PerfilProyectoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import { IdentAlbumDto } from 'src/entidades/album/entidad/album-dto';
import { TraduccionIntercambioDto } from './traduccion-intercambio-dto';

export class CodCatalogoTipoIntercambioDto {
  @ApiProperty({
    required: false,
    description: 'Codigo del tipo de intercambio',
    example: 'CATIPOINTER_1',
  })
  codigo: string;
}

export class IntercambioDto {
  @ApiProperty({
    description: 'Identificador del perfil',
    example: '5f3173e892e08e19101271f8',
  })
  perfil: PerfilProyectoDto;
  @ApiProperty({
    type: CodCatalogoTipoIntercambioDto,
    description: 'Codigo del Tipo de intercambio',
    example: 'CATIPOINTER_1',
  })
  tipo: CodCatalogoTipoIntercambioDto;
  @ApiProperty({ type: [IdentAlbumDto] })
  adjuntos: [IdentAlbumDto];
  @ApiProperty({
    type: [TraduccionIntercambioDto],
    description: 'Traducciones en diferentes idiomas del intercambio',
    example: '',
  })
  traducciones: [TraduccionIntercambioDto];
  @ApiProperty({ description: 'codigos de estados', example: '' })
  estado: string;
}
