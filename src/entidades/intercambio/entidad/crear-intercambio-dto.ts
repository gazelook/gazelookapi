import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import {
  AlbumNuevo,
  idPerfilDto,
} from 'src/entidades/perfil/entidad/crear-perfil.dto';
import { DireccionIntercambio } from './direccion-intercambio-dto';
import { CodCatalogoTipoIntercambioDto } from './intercambio-dto';
// import { CodCatalogoTipoMonedaDto } from "src/entidades/catalogos/entidad/catalogo-tipo-moneda.dto";
import { TraduccionIntercambioDto } from './traduccion-intercambio-dto';

export class CrearIntercambioDto {
  @ApiProperty({ type: idPerfilDto })
  perfil: idPerfilDto;

  @ApiProperty({ required: true, type: [TraduccionIntercambioDto] })
  traducciones: Array<TraduccionIntercambioDto>;

  @ApiProperty({ type: CodCatalogoTipoIntercambioDto })
  tipo: CodCatalogoTipoIntercambioDto;

  @ApiProperty({ type: CodCatalogoTipoIntercambioDto })
  tipoIntercambiar: CodCatalogoTipoIntercambioDto;

  @ApiProperty({ required: true, type: DireccionIntercambio })
  direccion: DireccionIntercambio;

  @ApiProperty({ type: [AlbumNuevo] })
  @IsOptional()
  adjuntos: AlbumNuevo[];

  @ApiProperty({ type: String })
  @IsOptional()
  email: string;

  // @ApiProperty({type:[mediaId]})
  // @IsOptional()
  // medias: mediaId[];
}

export class ActualizarStatusIntercambioDto {
  @ApiProperty({ type: String })
  _id: string;

  @ApiProperty({ type: idPerfilDto })
  perfil: idPerfilDto;
}
