import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import {
  AlbumNuevo,
  idPerfilDto,
} from 'src/entidades/perfil/entidad/crear-perfil.dto';
import { DireccionIntercambio } from './direccion-intercambio-dto';
import { CodCatalogoTipoIntercambioDto } from './intercambio-dto';
import { TraduccionIntercambioDto } from './traduccion-intercambio-dto';

export class ActualizarIntercambioDto {
  @ApiProperty({ required: true, description: 'identificador del intercambio' })
  @IsString()
  _id: string;

  @ApiProperty({ type: idPerfilDto })
  perfil: idPerfilDto;

  @ApiProperty({ required: true, type: [TraduccionIntercambioDto] })
  traducciones: Array<TraduccionIntercambioDto>;

  @ApiProperty({ type: [AlbumNuevo] })
  @IsOptional()
  adjuntos: AlbumNuevo[];

  @ApiProperty({ type: DireccionIntercambio })
  direccion: DireccionIntercambio;

  @ApiProperty({ type: CodCatalogoTipoIntercambioDto })
  tipoIntercambiar: CodCatalogoTipoIntercambioDto;

  @ApiProperty({ type: String })
  @IsOptional()
  email: string;
}
