import { ApiProperty } from '@nestjs/swagger';
// import { mediaId } from "src/entidades/media/entidad/archivo-resultado.dto";
import {
  Pais,
  traduccionDireccion,
} from 'src/entidades/cuenta/entidad/cuenta.dto';

export class CodigoLocalidadDireccionIntercambio {
  @ApiProperty()
  codigo?: string;
}

export class DireccionIntercambio {
  @ApiProperty()
  latitud?: number;

  @ApiProperty()
  longitud?: number;

  @ApiProperty({ type: [traduccionDireccion], required: true })
  traducciones?: Array<traduccionDireccion>;

  @ApiProperty({ type: CodigoLocalidadDireccionIntercambio })
  localidad: CodigoLocalidadDireccionIntercambio;

  @ApiProperty({ type: Pais })
  pais: Pais;
}
