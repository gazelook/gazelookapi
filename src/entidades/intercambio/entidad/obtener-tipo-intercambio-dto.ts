import { ApiProperty } from '@nestjs/swagger';
import { IdCatalogoTipoIntercambioDto } from './traduccion-catalogo-tipo-intercambio-dto';

export class CatalogoTipoIntercambioDto {
  @ApiProperty({
    description: 'Identificador del catalogo tipo de intercambio',
    example: '5f3173e892e08e19101271f8',
  })
  _id: string;
  @ApiProperty({ type: [IdCatalogoTipoIntercambioDto] })
  traducciones: [IdCatalogoTipoIntercambioDto];
  @ApiProperty({
    description: 'Codigo del catalogo tipo intercambio',
    example: 'CATIPOINTER_1',
  })
  codigo: string;
  @ApiProperty({
    description: 'Fecha de creacion',
    example: '2020-08-20T14:49:48.277Z',
  })
  fechaCreacion: Date;
}

export class CodCatalogTipoIntercambioDto {
  @ApiProperty({
    required: true,
    description: 'Codigo del catalogo tipo intercambio',
    example: 'CATIPOINTER_1',
  })
  codigo: string;
}
