import { ApiProperty } from '@nestjs/swagger';
import {
  RetornoMediaDto,
  IdentAlbumDto,
} from 'src/entidades/album/entidad/album-dto';
import { PerfilProyectoUnicoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import {
  TraduccionIntercambioDto,
  TraduccionResumidaIntercambioDto,
} from './traduccion-intercambio-dto';

export class IntercambioResumidoPaginacionResponseDto {
  @ApiProperty({
    description: 'Identificador del intercambio',
    example: '5f378265075b2a4be839d534',
  })
  _id: string;
  @ApiProperty({ type: [TraduccionResumidaIntercambioDto] })
  traducciones: [TraduccionResumidaIntercambioDto];
  @ApiProperty({
    description: 'Fecha en que se creo el proyecto',
    example: '2020-08-15T06:36:21.436+00:00',
  })
  fechaCreacion: Date;
  @ApiProperty({
    description: 'Fecha en que se actualizo el proyecto',
    example: '2020-08-20T06:36:21.436+00:00',
  })
  fechaActualizacion: Date;
  @ApiProperty({ type: [RetornoMediaDto] })
  adjuntos: [RetornoMediaDto];
}

export class IntercambioCompletoDto {
  @ApiProperty({
    description: 'Identificador del intercambio',
    example: '5f56653564d0cc3bf47893d5',
  })
  _id: string;
  @ApiProperty({ type: [TraduccionIntercambioDto] })
  traducciones: [TraduccionIntercambioDto];
  @ApiProperty({ type: [IdentAlbumDto] })
  adjuntos: [IdentAlbumDto];
  @ApiProperty({ type: PerfilProyectoUnicoDto })
  perfil: PerfilProyectoUnicoDto;
  @ApiProperty({
    description: 'Fecha de creacion del intercambio',
    example: '2020-09-07T16:52:05.229Z',
  })
  fechaCreacion: Date;
  @ApiProperty({
    description: 'Fecha de creacion del intercambio',
    example: '2020-09-07T16:52:05.229Z',
  })
  fechaActualizacion: Date;
}
