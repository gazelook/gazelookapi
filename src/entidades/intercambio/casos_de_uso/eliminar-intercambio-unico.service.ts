import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { TraduccionIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_intercambio/traduccion-intercambio.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { EliminarComentarioIntercambioService } from 'src/entidades/comentarios_intercambio/casos_de_uso/eliminar-comentario.service';
import { ObtenerComentariosIntercambioService } from 'src/entidades/comentarios_intercambio/casos_de_uso/obtener-comentarios-intercambio.service';
import { EliminarDireccionService } from 'src/entidades/direccion/casos_de_uso/eliminar-direccion.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { EliminarAlbumService } from '../../album/casos_de_uso/eliminar-album.service';
const mongoose = require('mongoose');

@Injectable()
export class EliminarIntercambioUnicoService {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    @Inject('TRADUCCION_INTERCAMBIO_MODEL')
    private readonly traduccionIntercambioModel: Model<TraduccionIntercambio>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private eliminarAlbumService: EliminarAlbumService,
    private eliminarDireccionService: EliminarDireccionService,
    private ObtenerComentariosIntercambioService: ObtenerComentariosIntercambioService,
    private eliminarComentarioIntercambioService: EliminarComentarioIntercambioService,
  ) {}

  async eliminarIntercambioUnico(
    perfil: any,
    idIntercambio: string,
    opts?: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    let session;
    let optsLocal;
    if (!opts) {
      session = await mongoose.startSession();
      session.startTransaction();
    }
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      if (!opts) {
        optsLocal = true;
        opts = { session };
      }

      //Obtiene el codigo de la accion eliminar
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      let getAccionEliminar = accion.codigo;

      //Obtiene el codigo de la entidad traduccion intercambio
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionIntercambio,
      );
      let codEntidadTradIntercambio = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion intercambio
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradIntercambio,
      );
      let codEstadoActTradIntercambio = estado.codigo;

      //Obtiene el estado eliminado de la entidad traduccion intercambio
      const estadoTE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadTradIntercambio,
      );
      let codEstadoTIntercambioEli = estadoTE.codigo;

      //Obtiene el codigo de la entidad intercambio
      const entidadIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.intercambio,
      );
      let codEntidadIntercambio = entidadIntercambio.codigo;

      //Obtiene el codigo del estado activa de la entidad intercambio
      const estadointercambio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadIntercambio,
      );
      let codEstadoActIntercambio = estadointercambio.codigo;

      //Obtiene el codigo del estado eliminado de la entidad intercambio
      const estadoElimIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadIntercambio,
      );
      let codEstadoEliIntercambio = estadoElimIntercambio.codigo;

      const intercambio = await this.intercambioModel
        .findOne({
          $and: [
            { _id: idIntercambio },
            { perfil: perfil },
            { estado: codEstadoActIntercambio },
          ],
        })
        .session(session)
        .populate([{ path: 'adjuntos' }]);

      if (intercambio) {
        // eliminar album de adjuntos
        for (const albm of intercambio.adjuntos) {
          const dataAlbum = {
            _id: albm['_id'],
            usuario: perfil,
          };
          await this.eliminarAlbumService.eliminarAlbum(dataAlbum, false, opts);
        }

        // eliminar direccion
        if (intercambio.direccion) {
          let data = {
            _id: intercambio.direccion,
            usuario: perfil,
          };
          await this.eliminarDireccionService.eliminarDireccion(
            data,
            false,
            opts,
          );
        }

        // eliminar comentarios
        const getComentarios = await this.ObtenerComentariosIntercambioService.obtenerComentariosByIdIntercambio(
          intercambio._id,
        );
        for (const comentario of getComentarios) {
          await this.eliminarComentarioIntercambioService.eliminarComentario(
            intercambio._id,
            perfil,
            comentario._id,
            opts,
          );
        }

        //Actualiza el estado a eliminado de las traducciones del intercambio
        await this.traduccionIntercambioModel.updateMany(
          { intercambio: idIntercambio },
          {
            $set: {
              estado: codEstadoTIntercambioEli,
              fechaActualizacion: new Date(),
            },
          },
          opts,
        );

        let datosDelete = {
          perfil: perfil,
          intercambio: idIntercambio,
        };
        let newHistoricoTIntercambio: any = {
          datos: datosDelete,
          usuario: perfil,
          accion: getAccionEliminar,
          entidad: codEntidadTradIntercambio,
        };
        //Crea el historico del borrado de la traduccion intercambio
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoTIntercambio,
        );

        //Actualiza el estado a eliminado del intercambio
        await this.intercambioModel.updateOne(
          { _id: idIntercambio },
          {
            $set: {
              estado: codEstadoEliIntercambio,
              fechaActualizacion: new Date(),
            },
          },
          opts,
        );

        const getProyectElimi = await this.intercambioModel
          .findOne({ _id: idIntercambio, estado: codEstadoEliIntercambio })
          .session(opts.session);

        let newHistoricoIntercambio: any = {
          datos: getProyectElimi,
          usuario: perfil,
          accion: getAccionEliminar,
          entidad: codEntidadIntercambio,
        };
        //Crea el historico del borrado del intercambio
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoIntercambio,
        );

        if (optsLocal) {
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
        }
        return true;
      } else {
        if (optsLocal) {
          await session.abortTransaction();
          await session.endSession();
        }
        return intercambio;
      }
    } catch (error) {
      if (optsLocal) {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }

      throw error;
    }
  }
}
