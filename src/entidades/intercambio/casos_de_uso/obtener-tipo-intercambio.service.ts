import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoTipoIntercambio } from 'src/drivers/mongoose/interfaces/catalogo_tipo_intercambio/catalogo-tipo-intercambio.interface';
import { TraducirCatalogoTipoIntercambioService } from './traducir-tipo-Intercambio.service';
const mongoose = require('mongoose');

@Injectable()
export class CatalogoTipoIntercambioService {
  constructor(
    @Inject('CATALOGO_TIPO_INTERCAMBIO_MODEL')
    private readonly catalogoTipoIntercambioModel: Model<
      CatalogoTipoIntercambio
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private traducirCatalogoTipoIntercambioService: TraducirCatalogoTipoIntercambioService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async obtenerCatalogoTipoIntercambio(codIdiom: string): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la entidad catalogo tipo intercambio
      const entidadTipoIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.catalogoTipoIntercambio,
      );
      let codEntidadTipoIntercambio = entidadTipoIntercambio.codigo;

      //Obtiene el estado activa de la entidad catalogo tipo intercambio
      const estadoIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTipoIntercambio,
      );
      let codEstadoTipoIntercambio = estadoIntercambio.codigo;

      //Obtiene la entidad traduccion catalogo tipo intercambio
      const entidadTTipoIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionCatalogoTipoIntercambio,
      );
      let codEntidadTIntercambio = entidadTTipoIntercambio.codigo;

      //Obtiene el estado  activa de la entidad catalogo tipo intercambio
      const estadoTradIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTIntercambio,
      );
      let codEstadoTradTipoIntercambio = estadoTradIntercambio.codigo;

      //Obtiene el idioma por el codigo
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdiom,
      );
      let codIdioma = idioma.codigo;

      //Obtiene el catalogo tipo intercambio
      const obtenerTipoIntercambio = await this.catalogoTipoIntercambioModel
        .find(
          { estado: codEstadoTipoIntercambio },
          { _id: 1, codigo: 1, traducciones: 1, fechaActualizacion: 1 },
        )
        .populate({
          path: 'traducciones',
          match: {
            idioma: codIdioma,
            estado: codEstadoTradTipoIntercambio,
          },
          select: 'nombre descripcion idioma original fecha_actualizacion',
        });

      let existe: boolean;

      //Variable para identificar si una traduccion es la original
      let identificadorTraduccionTipoIntercambio: string;
      let nombreTipoIntercambio: string;
      let descripcionTipoIntercambio: string;

      if (obtenerTipoIntercambio.length > 0) {
        for (let i = 0; i < obtenerTipoIntercambio.length; i++) {
          let idTipoIntercambio = obtenerTipoIntercambio[i]._id;
          let codigoTipoIntercambio = obtenerTipoIntercambio[i].codigo;
          let traducciones = obtenerTipoIntercambio[i].traducciones.length;

          if (traducciones > 0) {
            existe = true;
          } else {
            existe = false;

            let getTipoIntercambio = await this.catalogoTipoIntercambioModel
              .findOne({
                _id: idTipoIntercambio,
                estado: codEstadoTipoIntercambio,
              })
              .populate({
                path: 'traducciones',
                match: {
                  original: true,
                  estado: codEstadoTradTipoIntercambio,
                },
              });

            const dataTipoIntercambio = JSON.parse(
              JSON.stringify(getTipoIntercambio.traducciones[0]),
            );
            nombreTipoIntercambio = dataTipoIntercambio.nombre;
            descripcionTipoIntercambio = dataTipoIntercambio.descripcion;
            identificadorTraduccionTipoIntercambio = dataTipoIntercambio._id;
          }

          //Si la traduccion no existe la crea
          if (existe === false) {
            await this.traducirCatalogoTipoIntercambioService.TraducirCatalogoTipoIntercambio(
              idTipoIntercambio,
              codigoTipoIntercambio,
              nombreTipoIntercambio,
              descripcionTipoIntercambio,
              codIdiom,
              opts,
            );
          }
        }

        //Obtiene el catalogo tipo intercambio
        const getTipoIntercambio = await this.catalogoTipoIntercambioModel
          .find(
            { estado: codEstadoTipoIntercambio },
            { _id: 1, codigo: 1, traducciones: 1, fechaCreacion: 1 },
          )
          .session(session)
          .populate({
            path: 'traducciones',
            match: {
              idioma: codIdioma,
              estado: codEstadoTradTipoIntercambio,
            },
            select: 'nombre descripcion idioma original fecha_actualizacion',
          });

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return getTipoIntercambio;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async obtenerTipoIntercambioByCodigo(
    codTipoIntercambio: any,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la entidad catalogo tipo intercambio
      const entidadTipoIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.catalogoTipoIntercambio,
      );
      let codEntidadTipoIntercambio = entidadTipoIntercambio.codigo;

      //Obtiene el estado  activa de la entidad catalogo tipo intercambio
      const estadoIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTipoIntercambio,
      );
      let codEstadoTipoIntercambio = estadoIntercambio.codigo;

      //Obtiene la entidad traduccion catalogo tipo intercambio
      const entidadTTipoIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionCatalogoTipoIntercambio,
      );
      let codEntidadTIntercambio = entidadTTipoIntercambio.codigo;

      //Obtiene el estado  activa de la entidad catalogo tipo intercambio
      const estadoTradIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTIntercambio,
      );
      let codEstadoTradTipoIntercambio = estadoTradIntercambio.codigo;

      //Obtiene el idioma por el codigo
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdiom,
      );
      let codIdioma = idioma.codigo;

      console.log('codIdiomaaaaaaaaaaaaaaaaaaaaa: ', codIdioma);
      //Obtiene el catalogo tipo intercambio
      const obtenerTipoIntercambio = await this.catalogoTipoIntercambioModel
        .findOne(
          { codigo: codTipoIntercambio, estado: codEstadoTipoIntercambio },
          { _id: 1, codigo: 1, traducciones: 1, fechaActualizacion: 1 },
        )
        .populate({
          path: 'traducciones',
          match: {
            idioma: codIdioma,
            estado: codEstadoTradTipoIntercambio,
          },
          select: 'nombre descripcion idioma original fecha_actualizacion',
        });

      let existe: boolean;

      //Variable para identificar si una traduccion es la original
      let IdentificadorTraduccionTipoIntercambio: any;
      let nombreTipoIntercambio: string;
      let descripcionTipoIntercambio: string;

      //Verifica si tiene tipo de intercambio
      if (obtenerTipoIntercambio) {
        let idTipoIntercambio = obtenerTipoIntercambio._id;

        let codigoTipoIntercambio = obtenerTipoIntercambio.codigo;

        console.log(
          'obtenerTipoIntercambio.traducciones.length: ',
          obtenerTipoIntercambio.traducciones.length,
        );
        if (obtenerTipoIntercambio.traducciones.length > 0) {
          existe = true;
        } else {
          existe = false;
          //Obtiene los el tipo de proyecto con su traduccion original
          let getTipoIntercambio = await this.catalogoTipoIntercambioModel
            .findOne({
              _id: idTipoIntercambio,
              estado: codEstadoTipoIntercambio,
            })
            .populate({
              path: 'traducciones',
              match: {
                original: true,
                estado: codEstadoTradTipoIntercambio,
              },
            });

          const dataTipoProyecto = JSON.parse(
            JSON.stringify(getTipoIntercambio.traducciones[0]),
          );
          nombreTipoIntercambio = dataTipoProyecto.nombre;
          descripcionTipoIntercambio = dataTipoProyecto.descripcion;
          IdentificadorTraduccionTipoIntercambio = dataTipoProyecto._id;
        }

        //Si la traduccion no existe la crea
        if (existe === false) {
          //await this.traducirPensamientoService.TraducirPensamiento(idPensamiento, IdentificadorTraduccionPensamiento, perfil, headers.idioma)
          await this.traducirCatalogoTipoIntercambioService.TraducirCatalogoTipoIntercambio(
            idTipoIntercambio,
            codigoTipoIntercambio,
            nombreTipoIntercambio,
            descripcionTipoIntercambio,
            codIdiom,
            opts,
          );
        }
        //}

        //Obtiene el catalogo tipo proyecto
        const getTipoProyecto = await this.catalogoTipoIntercambioModel
          .findOne(
            { codigo: codTipoIntercambio, estado: codEstadoTipoIntercambio },
            { _id: 1, codigo: 1, traducciones: 1, fechaCreacion: 1 },
          )
          .session(session)
          .populate({
            path: 'traducciones',
            match: {
              idioma: codIdioma,
              estado: codEstadoTradTipoIntercambio,
            },
            select: 'nombre descripcion idioma original fecha_actualizacion',
          });

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        let retResp = {
          codigo: getTipoProyecto.codigo,
          traducciones: [
            {
              nombre: getTipoProyecto.traducciones[0]['nombre'],
              descripcion: getTipoProyecto.traducciones[0]['descripcion'],
            },
          ],
        };
        return retResp;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
