import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  codigosCatalogoEstatusIntercambio,
  nombreAcciones,
  nombreEntidades,
} from '../../../shared/enum-sistema';
const sw = require('stopword');

@Injectable()
export class CambiarEstatusIntercambioService {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async cambiarStatusIntercambio(
    idIntercambio: string,
    idPerfil: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const accionAc = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    let getAccionModificar = accionAc.codigo;

    //Obtiene la entidad proyecto
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.intercambio,
    );
    let codEntidadIntercambio = entidad.codigo;

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let getIntercambio = await this.intercambioModel.findOne({
        _id: idIntercambio,
        perfil: idPerfil,
      });
      let codStatusActualizado: any;
      if (getIntercambio) {
        if (
          getIntercambio.status == codigosCatalogoEstatusIntercambio.habilitada
        ) {
          //Actualiza todos al intercambio a status de deshabilitada
          codStatusActualizado =
            codigosCatalogoEstatusIntercambio.deshabilitada;
          await this.intercambioModel.updateMany(
            { _id: idIntercambio },
            { status: codStatusActualizado, fechaActualizacion: new Date() },
            opts,
          );
        } else {
          //Actualiza todos al intercambio a status de deshabilitada
          codStatusActualizado = codigosCatalogoEstatusIntercambio.habilitada;
          await this.intercambioModel.updateMany(
            { _id: idIntercambio },
            { status: codStatusActualizado, fechaActualizacion: new Date() },
            opts,
          );
        }

        //Data para guardar en el historico
        let dataHisIntercambio = {
          _id: idIntercambio,
          descripcion: 'Cambio de estado a '.concat(codStatusActualizado),
          estatusAnterior: getIntercambio.status,
          estatusActual: codStatusActualizado,
        };
        let newHistoricoProyectoUpdate: any = {
          datos: dataHisIntercambio,
          usuario: '',
          accion: getAccionModificar,
          entidad: codEntidadIntercambio,
        };
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoProyectoUpdate,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return true;
      }

      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return false;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
