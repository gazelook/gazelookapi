import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { codigosEstadosIntercambio } from 'src/shared/enum-sistema';

@Injectable()
export class ObtenerIntercambioIdService {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
  ) {}

  async obtenerIntercambioById(idIntercambio): Promise<any> {
    try {
      const getIntercambio = await this.intercambioModel.findOne({
        _id: idIntercambio,
      });
      return getIntercambio;
    } catch (error) {
      throw error;
    }
  }

  async obtenerTotalIntercambios(): Promise<any> {
    try {
      const getIntercambio = await this.intercambioModel.find({
        estado: { $ne: codigosEstadosIntercambio.eliminado },
      });
      return getIntercambio.length;
    } catch (error) {
      throw error;
    }
  }
}
