import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { TraduccionIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_intercambio/traduccion-intercambio.interface';
import { CrearAlbumService } from 'src/entidades/album/casos_de_uso/crear-album.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { ActualizarDireccionService } from 'src/entidades/direccion/casos_de_uso/actualizar-direccion.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { ActualizarIntercambioDto } from '../entidad/actualizar-intercambio.dto';
import { TraducirIntercambioServiceSegundoPlano } from './traducir-intercambio-segundo-plano.service';

@Injectable()
export class ActualizarIntercambioService {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    @Inject('TRADUCCION_INTERCAMBIO_MODEL')
    private readonly traduccionIntercambioModel: Model<TraduccionIntercambio>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private crearAlbumService: CrearAlbumService,
    private actualizarDireccionService: ActualizarDireccionService,
    private traducirIntercambioServiceSegundoPlano: TraducirIntercambioServiceSegundoPlano,
  ) {}

  async actualizarIntercambio(
    intercambioActualizar: ActualizarIntercambioDto,
    idIntercambio: string,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const accionAc = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    let getAccionAc = accionAc.codigo;

    //Obtiene la entidad traduccion intercambio
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionIntercambio,
    );
    let codEntidadTraduccionIntercambio = entidad.codigo;

    //Obtiene el estado activo de la traduccion intercambio
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTraduccionIntercambio,
    );
    let codEstadoTIntercambioActiva = estado.codigo;

    //Obtiene el estado eliminado de la traduccion intercambio
    const estadoTE = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.eliminado,
      codEntidadTraduccionIntercambio,
    );
    let codEstadoTIntercambioEli = estadoTE.codigo;

    //Entidad intercambio
    const entidadIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.intercambio,
    );
    let codEntidadIntercambio = entidadIntercambio.codigo;

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let intercambioOriginal = await this.intercambioModel
        .findOne({
          _id: idIntercambio,
          perfil: intercambioActualizar.perfil._id,
        })
        .populate({
          path: 'traducciones',
          select: '-fechaActualizacion  -__v ',
          match: { estado: codEstadoTIntercambioActiva, original: true },
        });

      if (intercambioOriginal) {
        const updateTradu = await this.traduccionIntercambioModel.updateMany(
          { intercambio: idIntercambio },
          { $set: { estado: codEstadoTIntercambioEli } },
          opts,
        );

        let idiomaDetectado: any;

        if (intercambioActualizar.traducciones[0].tituloCorto) {
          let textoTraducido = await traducirTexto(
            codIdiom,
            intercambioActualizar.traducciones[0].tituloCorto,
          );
          idiomaDetectado = textoTraducido.idiomaDetectado;
        } else {
          if (intercambioActualizar.traducciones[0].titulo) {
            let textoTraducido = await traducirTexto(
              codIdiom,
              intercambioActualizar.traducciones[0].titulo,
            );
            idiomaDetectado = textoTraducido.idiomaDetectado;
          } else {
            if (intercambioActualizar.traducciones[0].descripcion) {
              let textoTraducido = await traducirTexto(
                codIdiom,
                intercambioActualizar.traducciones[0].descripcion,
              );
              idiomaDetectado = textoTraducido.idiomaDetectado;
            } else {
              const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
                codIdiom,
              );
              idiomaDetectado = idioma.codigo;
            }
          }
        }

        //Obtiene el idioma por el codigo
        const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
          idiomaDetectado,
        );
        let codIdioma = idioma.codigo;

        let newTraduccionIntercambio = {
          titulo: intercambioActualizar.traducciones[0].titulo || null,
          tituloCorto:
            intercambioActualizar.traducciones[0].tituloCorto || null,
          descripcion:
            intercambioActualizar.traducciones[0].descripcion || null,
          idioma: codIdioma,
          original: true,
          estado: codEstadoTIntercambioActiva,
          intercambio: idIntercambio,
        };
        const crearTraduccionIntercambio = await new this.traduccionIntercambioModel(
          newTraduccionIntercambio,
        ).save(opts);
        const dataTIntercambio = JSON.parse(
          JSON.stringify(crearTraduccionIntercambio),
        );
        delete dataTIntercambio.__v;

        await this.intercambioModel.updateOne(
          { _id: idIntercambio },
          {
            $push: { traducciones: crearTraduccionIntercambio._id },
            fechaActualizacion: new Date(),
          },
          opts,
        );

        //Comprueba si viene direccion para actualizar
        if (intercambioActualizar.direccion !== undefined) {
          //Comprueba si llega la direccion
          let traducciones: any;
          if (
            intercambioActualizar.direccion?.traducciones &&
            intercambioActualizar.direccion.traducciones.length > 0
          ) {
            traducciones = intercambioActualizar.direccion.traducciones;
          } else {
            traducciones = [];
          }
          //Comprueba si llega la localidad
          let localidad: any;
          if (
            intercambioActualizar.direccion?.localidad &&
            intercambioActualizar.direccion?.localidad.codigo
          ) {
            localidad = intercambioActualizar.direccion.localidad.codigo;
          } else {
            localidad = null;
          }
          //Objeto para actualizar la direccion
          const dataDireccion = {
            latitud: intercambioActualizar.direccion?.latitud,
            longitud: intercambioActualizar.direccion?.longitud,
            traducciones: traducciones,
            localidad: localidad,
            pais: intercambioActualizar.direccion?.pais?.codigo,
            _id: intercambioOriginal.direccion,
          };
          const actualizarDireccion = await this.actualizarDireccionService.actualizarDireccion(
            dataDireccion,
            opts,
          );
        }

        // _________________adjuntos_____________________
        let nuevoAlbum;
        let IdUsuario;

        const getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
          intercambioActualizar.perfil._id,
        );
        IdUsuario = getUsuario.usuario._id;

        if (intercambioActualizar.adjuntos.length > 0) {
          for (const album of intercambioActualizar.adjuntos) {
            const dataAlbum: any = {
              nombre: album.nombre,
              tipo: album.tipo.codigo,
              media: album.media,
              usuario: IdUsuario,
              idioma: codIdiom,
            };
            if (album.portada && album.portada._id) {
              dataAlbum.portada = album.portada;
            }

            //// actualiza album
            // if (album._id !== undefined) {
            //   dataAlbum._id = album._id;
            //   await this.actualizarAlbumService.actualizarAlbumconMedia(album._id, dataAlbum);
            // }
            //if (album._id === undefined) {
            //crea el nuevo album
            nuevoAlbum = await this.crearAlbumService.crearAlbum(
              dataAlbum,
              opts,
            );
            //Acatualiza el array de adjuntos
            await this.intercambioModel.updateOne(
              { _id: idIntercambio },
              {
                $push: { adjuntos: nuevoAlbum._id },
                fechaActualizacion: new Date(),
              },
              opts,
            );
            //}
          }
        }

        if (intercambioActualizar.email) {
          await this.intercambioModel.updateOne(
            { _id: idIntercambio },
            {
              $set: {
                email: intercambioActualizar.email,
                fechaActualizacion: new Date(),
              },
            },
            opts,
          );
        }

        if (intercambioActualizar?.tipoIntercambiar) {
          await this.intercambioModel.updateOne(
            { _id: idIntercambio },
            {
              $set: {
                tipoIntercambiar: intercambioActualizar.tipoIntercambiar.codigo,
                fechaActualizacion: new Date(),
              },
            },
            opts,
          );
        }

        let newHistoricoIntercambioUpdate: any = {
          datos: intercambioActualizar,
          usuario: intercambioActualizar.perfil._id,
          accion: getAccionAc,
          entidad: codEntidadIntercambio,
        };
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoIntercambioUpdate,
        );

        const returnIntercambio = await this.intercambioModel
          .findOne({
            $and: [
              { _id: idIntercambio },
              { perfil: intercambioActualizar.perfil._id },
            ],
          })
          .session(session)
          .populate({
            path: 'traducciones ',
            select: '-fechaActualizacion  -__v ',
            match: { estado: codEstadoTIntercambioActiva, original: true },
          });
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        this.traducirIntercambioServiceSegundoPlano.traducirIntercambioSegundoPlano(
          returnIntercambio._id,
          returnIntercambio.perfil,
          codIdiom,
        );

        return returnIntercambio;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
