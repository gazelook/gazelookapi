import { Inject, Injectable } from '@nestjs/common';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { Model } from 'mongoose';
import { codigosEstadosIntercambio } from '../../../shared/enum-sistema';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';

@Injectable()
export class ObtenerIntercambioPerfilService {
  proyecto: any;

  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async obtenerIntercambiosPerfil(idPerfil, codNombredioma): Promise<any> {
    try {
      //Obtiene el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codNombredioma,
      );
      let codIdioma = idioma.codigo;

      const getIntercambio = await this.intercambioModel
        .find({
          $and: [
            { perfil: idPerfil },
            { estado: { $ne: codigosEstadosIntercambio.eliminado } },
          ],
        })
        .populate([
          {
            path: 'traducciones',
          },
          {
            path: 'adjuntos',
          },
          {
            path: 'comentarios',
          },
        ]);
      return getIntercambio;
    } catch (error) {
      throw error;
    }
  }
}
