import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { TraduccionIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_intercambio/traduccion-intercambio.interface';
import { CrearAlbumService } from 'src/entidades/album/casos_de_uso/crear-album.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearComentarioIntercambioService } from 'src/entidades/comentarios_intercambio/casos_de_uso/crear-comentario.service';
import { CrearDireccionService } from 'src/entidades/direccion/casos_de_uso/crear-direccion.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CrearParticipanteIntercambioService } from 'src/entidades/participante_intercambio/casos_de_uso/crear-participante-intercambio.service';
import { CrearParticipanteIntercambioDto } from 'src/entidades/participante_intercambio/entidad/participante-intercambio-dto';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codigoEntidades,
  codigosCatalogoEstatusIntercambio,
  codigosEstadosParticipantentercambio,
  codigosRol,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CrearIntercambioDto } from '../entidad/crear-intercambio-dto';
import { TraducirIntercambioServiceSegundoPlano } from './traducir-intercambio-segundo-plano.service';
const mongoose = require('mongoose');

@Injectable()
export class CrearIntercambioService {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    @Inject('TRADUCCION_INTERCAMBIO_MODEL')
    private readonly traduccionIntercambioModel: Model<TraduccionIntercambio>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private crearAlbumService: CrearAlbumService,
    private crearDireccionService: CrearDireccionService,
    private traducirIntercambioServiceSegundoPlano: TraducirIntercambioServiceSegundoPlano,
    private crearComentarioIntercambioService: CrearComentarioIntercambioService,
    private crearParticipanteIntercambioService: CrearParticipanteIntercambioService,
    private gestionRolService: GestionRolService,
  ) {}

  async crearIntercambio(
    intercambioDto: CrearIntercambioDto,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad intercambio
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.intercambio,
      );
      let getCodEntIntercambio = entidad.codigo;

      //Obtiene el estado activa del intercambio
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        getCodEntIntercambio,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad traduccion intercambio
      const entidadTIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionIntercambio,
      );
      let codEntidadTIntercambio = entidadTIntercambio.codigo;

      //Obtiene el estado  activa de la entidad traduccion intercambio
      const estadoTInter = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTIntercambio,
      );
      let codEstadoTIntercambio = estadoTInter.codigo;

      //detectar el idioma que se envia el texto
      let textoTraducido = await traducirTexto(
        codIdiom,
        intercambioDto.traducciones[0].titulo,
      );
      let idiomaDetectado = textoTraducido.idiomaDetectado;

      //Obtiene el idioma por el codigo
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idiomaDetectado,
      );
      let codIdioma = idioma.codigo;

      //Objeto de traduccion proyectos
      const idIntercambio = new mongoose.mongo.ObjectId();

      let newTraduccionIntercambio = {
        titulo: intercambioDto.traducciones[0].titulo,
        tituloCorto: intercambioDto.traducciones[0].tituloCorto,
        descripcion: intercambioDto.traducciones[0].descripcion,
        idioma: codIdioma,
        original: true,
        estado: codEstadoTIntercambio,
        intercambio: idIntercambio,
      };

      //Crea la traduccion del intercambio
      const crearTraduccionIntercambio = await new this.traduccionIntercambioModel(
        newTraduccionIntercambio,
      ).save(opts);

      const dataTraduccion = JSON.parse(
        JSON.stringify(crearTraduccionIntercambio),
      );
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      let newHistoricoTraduccionIntercambio: any = {
        datos: dataTraduccion,
        usuario: intercambioDto.perfil,
        accion: getAccion,
        entidad: codEntidadTIntercambio,
      };

      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionIntercambio,
      );

      let codTraduccionIntercambio = [];
      codTraduccionIntercambio.push(dataTraduccion._id);

      let dataDireccion;
      let crearDireccion;
      if (intercambioDto.direccion) {
        dataDireccion = {
          latitud: intercambioDto.direccion?.latitud,
          longitud: intercambioDto.direccion?.longitud,
          traducciones: intercambioDto.direccion?.traducciones,
          localidad: intercambioDto.direccion?.localidad?.codigo,
          pais: intercambioDto.direccion?.pais?.codigo,
          usuario: intercambioDto.perfil._id,
        };

        //Objeto de tipo direccion
        crearDireccion = await this.crearDireccionService.crearDireccion(
          dataDireccion,
          opts,
        );
      }

      const getRolEntidad = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
        codigosRol.cod_rol_ent_propietario,
        codigoEntidades.comentarioIntercambio,
      );
      const idRolEntidad = getRolEntidad._id;
      let rolEnt = [];
      rolEnt.push(idRolEntidad);

      //Obtener una configuracion aleatoria para participante proyecto
      const getConfigEstilo = await this.crearComentarioIntercambioService.ObtenerConfiguracionEstilo(
        codigoEntidades.comentarioIntercambio,
      );

      var participanteIntercambioDto: CrearParticipanteIntercambioDto;
      participanteIntercambioDto = {
        coautor: intercambioDto.perfil._id,
        intercambio: idIntercambio,
        roles: rolEnt[0],
        comentarios: [],
        configuraciones: [getConfigEstilo],
        estado: codigosEstadosParticipantentercambio.activa,
        totalComentarios: 0,
      };

      const crearParcipanteInter = await this.crearParticipanteIntercambioService.crearParticipanteIntercambio(
        participanteIntercambioDto,
        opts,
      );
      const getIdePartInter = crearParcipanteInter._id;
      let codParticipantes = [];
      codParticipantes.push(getIdePartInter);

      let nuevoIntercambio = {
        _id: idIntercambio,
        perfil: intercambioDto.perfil,
        tipo: intercambioDto.tipo.codigo,
        tipoIntercambiar: intercambioDto.tipoIntercambiar.codigo,
        direccion: crearDireccion._id || null,
        adjuntos: [],
        traducciones: codTraduccionIntercambio,
        comentarios: [],
        participantes: codParticipantes,
        status: codigosCatalogoEstatusIntercambio.habilitada,
        email: intercambioDto.email || null,
        estado: codEstado,
        fechaActualizacion: new Date(),
      };
      //Guarda el intercambio
      const crearIntercambio = await new this.intercambioModel(
        nuevoIntercambio,
      ).save(opts);

      // _________________adjuntos_____________________
      let nuevoAlbum;
      let idUsuario;

      const getUsuarioPerfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
        intercambioDto.perfil._id,
      );
      idUsuario = getUsuarioPerfil.usuario._id;

      if (intercambioDto.adjuntos.length > 0) {
        for (const album of intercambioDto.adjuntos) {
          const dataAlbum: any = {
            nombre: album.nombre,
            tipo: album.tipo.codigo,
            media: album.media,
            predeterminado: album.predeterminado,
            usuario: getUsuarioPerfil._id,
            idioma: idiomaDetectado,
          };
          if (album.portada && album.portada._id) {
            dataAlbum.portada = album.portada;
          }

          //crea el nuevo album
          nuevoAlbum = await this.crearAlbumService.crearAlbum(dataAlbum, opts);
          //Actualiza el array de adjuntos
          await this.intercambioModel.updateOne(
            { _id: idIntercambio },
            { $push: { adjuntos: nuevoAlbum._id } },
            opts,
          );
        }
      }

      const dataIntercambio = JSON.parse(JSON.stringify(crearIntercambio));
      delete dataIntercambio.__v;

      let newHistoricoIntercambio: any = {
        descripcion: dataIntercambio,
        usuario: intercambioDto.perfil,
        accion: getAccion,
        entidad: getCodEntIntercambio,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistoricoIntercambio);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      this.traducirIntercambioServiceSegundoPlano.traducirIntercambioSegundoPlano(
        crearIntercambio._id,
        intercambioDto.perfil,
        idiomaDetectado,
      );
      return crearIntercambio;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
