import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoTipoIntercambio } from 'src/drivers/mongoose/interfaces/catalogo_tipo_intercambio/catalogo-tipo-intercambio.interface';
import { TraduccionCatalogoTipoIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_intercambio/traduccion-catalogo-tipo-intercambio.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class TraducirCatalogoTipoIntercambioService {
  constructor(
    @Inject('CATALOGO_TIPO_INTERCAMBIO_MODEL')
    private readonly catalogoTipoIntercambioModel: Model<
      CatalogoTipoIntercambio
    >,
    @Inject('TRADUCCION_CATALOGO_TIPO_INTERCAMBIO_MODEL')
    private readonly traduccionCatalogoTipoIntercambioModel: Model<
      TraduccionCatalogoTipoIntercambio
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async TraducirCatalogoTipoIntercambio(
    idCatalogoTipoIntercambio,
    codigoTipoIntercambio,
    nombre,
    descripcion,
    idioma,
    opts,
  ): Promise<any> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene el idioma por el codigo
    const getIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      idioma,
    );
    let codIdioma = getIdioma.codigo;

    //Obtiene la entidad traduccion catalogo tipo intercambio
    const entidadTIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionCatalogoTipoIntercambio,
    );
    let codEntidadTIntercambio = entidadTIntercambio.codigo;

    //Obtiene el estado  activa de la entidad traduccion catalogo tipo intercambio
    const estadoTIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTIntercambio,
    );
    let codEstadoTIntercambio = estadoTIntercambio.codigo;

    try {
      //Obtiene la traduccion en el idioma original
      let textoUnido = nombre + ' [-]' + descripcion;

      //llama al metodo de traduccion dinamica
      const textoTrducido = await traducirTexto(idioma, textoUnido);
      const textS = textoTrducido.textoTraducido.split('[-]');

      console.log(
        'TraducirCatalogoTipoIntercambio******]]]]]]]]]: ',
        codigoTipoIntercambio,
      );

      //datos para guardar la nueva traduccion
      let newTraduccionCatalogoTipoIntercambio = {
        catalogoTipoIntercambio: codigoTipoIntercambio,
        nombre: textS[0].trim(),
        descripcion: textS[1].trim(),
        idioma: codIdioma,
        original: false,
        estado: codEstadoTIntercambio,
      };

      //guarda la nueva traduccion
      const traduccion = new this.traduccionCatalogoTipoIntercambioModel(
        newTraduccionCatalogoTipoIntercambio,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del catalogo tipo intercambio
      let newHistoricoTraduccionTipoProyecto: any = {
        datos: dataTraduccion,
        usuario: '',
        accion: getAccion,
        entidad: codEntidadTIntercambio,
      };
      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionTipoProyecto,
      );

      //Actualiza el array de id de traducciones
      await this.catalogoTipoIntercambioModel.updateOne(
        { _id: idCatalogoTipoIntercambio },
        { $push: { traducciones: crearTraduccion._id } },
        opts,
      );
      return true;
    } catch (error) {
      throw error;
    }
  }
}
