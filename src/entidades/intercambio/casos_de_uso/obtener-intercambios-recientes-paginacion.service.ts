import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { TraduccionIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_intercambio/traduccion-intercambio.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { TraducirDireccionService } from 'src/entidades/direccion/casos_de_uso/traducir-direccion.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from 'src/entidades/proyectos/casos_de_uso/obtener-portada-predeterminada-proyecto-resumen.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import {
  codidosEstadosTraduccionDireccion,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { ObtenerLocalidadIntervaloService } from './obtener-localidad-intervalo.service';
import { CatalogoTipoIntercambioService } from './obtener-tipo-intercambio.service';
import { TraducirIntercambioService } from './traducir-intercambio.service';

const mongoose = require('mongoose');

@Injectable()
export class ObtenerIntercambiosRecientePaginacionService {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: PaginateModel<Intercambio>,
    @Inject('TRADUCCION_INTERCAMBIO_MODEL')
    private readonly traduccionIntercambioModel: PaginateModel<
      TraduccionIntercambio
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirIntercambioService: TraducirIntercambioService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
    private obtenerLocalidadIntervaloService: ObtenerLocalidadIntervaloService,
    private catalogoTipoIntercambioService: CatalogoTipoIntercambioService,
    private traducirDireccionService: TraducirDireccionService,
  ) {}

  async obtenerIntercambiosRecientePaginacion(
    codIdim: string,
    limite: number,
    pagina: number,
    titulo: any,
    tipo: string,
    tipoIntercambiar: string,
    codPais: string,
    localidad: string,
    fechaInicial: any,
    fechaFinal: any,
    response: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const headerNombre = new HadersInterfaceNombres();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtener el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad traduccionIntercambio
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionIntercambio,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion intercambio
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTIntercambio = estado.codigo;

      //Obtiene el codigo de la entidad intercambio
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.intercambio,
      );
      let codEntidadIntercambio = entidadP.codigo;

      //Obtiene el estado activa de la entidad intercambio
      const estadoIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadIntercambio,
      );
      let codEstadoIntercambio = estadoIntercambio.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      let regex1;
      let regex2;
      let regex3;
      let regex4;
      let regex5;
      let regex6;

      if (titulo) {
        //Obtener ultimo digito
        let ultimoCaracter = titulo?.charAt(titulo.length - 1);
        //Obtener ultimo digito
        let penultimoCaracter = titulo?.charAt(titulo.length - 2);

        let ultDosCracteres = penultimoCaracter.concat(ultimoCaracter);
        console.log('ultDosCracteres: ', ultDosCracteres);

        let eliminar_caracteres = [
          'as',
          'es',
          'is',
          'os',
          'us',
          'AS',
          'ES',
          'IS',
          'OS',
          'US',
        ];
        let eliminarS = ['s', 'S'];
        let nuevoTitulo;
        //Elimina la plural de los dos ultimos caracteres de la palabra que se envia
        for (const recorreCaracter of eliminar_caracteres) {
          if (recorreCaracter === ultDosCracteres) {
            nuevoTitulo = titulo.substring(0, titulo.length - 1);
            nuevoTitulo = nuevoTitulo.substring(0, nuevoTitulo.length - 1);
          }
        }

        let nuevoTitulo1;
        //Elimina la s (S) del ultimo caracter de la palabra que se envia
        for (const recorreCaracter of eliminarS) {
          if (recorreCaracter === ultimoCaracter) {
            nuevoTitulo1 = titulo.substring(0, titulo.length - 1);
          }
        }

        console.log('nuevoTitulo: ', nuevoTitulo);
        //Expresion regular de solo busqueda por palabras  completas
        regex1 = '\\b'.concat(titulo).concat('\\b');
        console.log('regex1: ', regex1);

        if (nuevoTitulo) {
          //Expresion regular por fin de palabras
          // regex2 = nuevoTitulo.concat("\\b");
          regex2 = '\\b'.concat(nuevoTitulo);
          console.log('regex2: ', regex2);

          //Expresion regular de solo busqueda por palabras  completas
          regex3 = '\\b'.concat(nuevoTitulo).concat('\\b');
          console.log('regex3: ', regex3);
        } else {
          //Expresion regular por fin de palabras
          regex2 = '\\b'.concat(titulo);
          console.log('regex2: ', regex2);

          //Expresion regular de solo busqueda por palabras  completas
          regex3 = '\\b'.concat(titulo).concat('\\b');
          console.log('regex3: ', regex3);
        }

        if (nuevoTitulo1) {
          //Expresion regular por fin de palabras
          // regex5 = nuevoTitulo1.concat("\\b");
          regex5 = '\\b'.concat(nuevoTitulo1);
          console.log('regex5: ', regex5);

          //Expresion regular de solo busqueda por palabras  completas
          regex6 = '\\b'.concat(nuevoTitulo1).concat('\\b');
          console.log('regex6: ', regex6);
        } else {
          //Expresion regular por fin de palabras
          regex5 = '\\b'.concat(titulo);
          console.log('regex5: ', regex5);

          //Expresion regular de solo busqueda por palabras  completas
          regex6 = '\\b'.concat(titulo).concat('\\b');
          console.log('regex6: ', regex6);
        }

        //Expresion regular por fin de palabras
        regex4 = titulo.concat('\\b');
        console.log('regex4: ', regex4);
      }

      if (!fechaInicial || !fechaFinal) {
        fechaInicial = null;
        fechaFinal = null;
      }

      if (!codPais) {
        codPais = null;
      }

      if (!tipoIntercambiar) {
        tipoIntercambiar = null;
      }

      if (!localidad) {
        localidad = null;
      }

      const populateTraduccion = {
        path: 'traducciones',
        select: '-fechaActualizacion -fechaCreacion -__v -tags',
        match: {
          idioma: codIdioma,
          estado: codEstadoTIntercambio,
        },
      };

      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto',
      };

      const populateDireccion = {
        path: 'direccion',
        select: 'traducciones localidad pais',
        populate: {
          path: 'traducciones',
          select: 'descripcion idioma original estado',
          match: {
            estado: codidosEstadosTraduccionDireccion.activa,
            idioma: codIdioma,
          },
        },
      };

      const populateIntercambio = {
        path: 'intercambio',
        populate: [adjuntos, populatePerfil, populateDireccion],
      };

      let totalIntercambios: any;

      if (
        fechaInicial &&
        fechaFinal &&
        !titulo &&
        !codPais &&
        !tipoIntercambiar &&
        !localidad
      ) {
        console.log('aquiiiii');
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);
        //Obtiene intercambios segun en rango de fecha
        totalIntercambios = await this.intercambioModel.paginate(
          {
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          },
          options,
        );
      }
      if (
        !fechaInicial &&
        !fechaFinal &&
        !titulo &&
        !codPais &&
        !tipoIntercambiar &&
        !localidad
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        totalIntercambios = await this.intercambioModel.paginate(
          { $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] },
          options,
        );
      }

      if (
        fechaInicial &&
        fechaFinal &&
        !titulo &&
        !codPais &&
        tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['localidad'] === localidad) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        !titulo &&
        !codPais &&
        !tipoIntercambiar &&
        !localidad
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        totalIntercambios = await this.intercambioModel.paginate(
          { $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] },
          options,
        );
      }

      if (
        fechaInicial &&
        fechaFinal &&
        titulo &&
        !codPais &&
        !tipoIntercambiar &&
        !localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            arrayIntercambio.push(inter._id);
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        fechaInicial &&
        fechaFinal &&
        titulo &&
        codPais &&
        !tipoIntercambiar &&
        !localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['pais'] === codPais) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        fechaInicial &&
        fechaFinal &&
        titulo &&
        codPais &&
        !tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['pais'] === codPais) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        fechaInicial &&
        fechaFinal &&
        titulo &&
        codPais &&
        tipoIntercambiar &&
        !localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
              { tipoIntercambiar: tipoIntercambiar },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['pais'] === codPais) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        fechaInicial &&
        fechaFinal &&
        titulo &&
        codPais &&
        tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
              { tipoIntercambiar: tipoIntercambiar },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (
                inter.direccion['pais'] === codPais &&
                inter.direccion['localidad'] === localidad
              ) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        titulo &&
        !codPais &&
        !tipoIntercambiar &&
        !localidad
      ) {
        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            arrayIntercambio.push(inter._id);
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        titulo &&
        codPais &&
        !tipoIntercambiar &&
        !localidad
      ) {
        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['pais'] === codPais) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        titulo &&
        codPais &&
        tipoIntercambiar &&
        !localidad
      ) {
        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['pais'] === codPais) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        titulo &&
        codPais &&
        tipoIntercambiar &&
        localidad
      ) {
        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (
                inter.direccion['pais'] === codPais &&
                inter.direccion['localidad'] === localidad
              ) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        titulo &&
        codPais &&
        !tipoIntercambiar &&
        localidad
      ) {
        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (
                inter.direccion['pais'] === codPais &&
                inter.direccion['localidad'] === localidad
              ) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        fechaInicial &&
        fechaFinal &&
        titulo &&
        !codPais &&
        tipoIntercambiar &&
        !localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel.find({
          $and: [
            { tipo: tipo },
            { tipoIntercambiar: tipoIntercambiar },
            { fechaCreacion: { $gt: I } },
            { fechaCreacion: { $lt: F } },
            { estado: codEstadoIntercambio },
          ],
        });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            arrayIntercambio.push(inter._id);
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        fechaInicial &&
        fechaFinal &&
        titulo &&
        !codPais &&
        !tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion['localidad'] === localidad) {
              arrayIntercambio.push(inter._id);
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        titulo &&
        !codPais &&
        tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion['localidad'] === localidad) {
              arrayIntercambio.push(inter._id);
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        !titulo &&
        codPais &&
        !tipoIntercambiar &&
        !localidad
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getIntercambio = await this.intercambioModel
          .find({
            $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['pais'] === codPais) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        !titulo &&
        codPais &&
        tipoIntercambiar &&
        !localidad
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['pais'] === codPais) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        !titulo &&
        codPais &&
        tipoIntercambiar &&
        localidad
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (
                inter.direccion['pais'] === codPais &&
                inter.direccion['localidad'] === localidad
              ) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        !titulo &&
        codPais &&
        !tipoIntercambiar &&
        localidad
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getIntercambio = await this.intercambioModel
          .find({
            $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (
                inter.direccion['pais'] === codPais &&
                inter.direccion['localidad'] === localidad
              ) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        fechaInicial &&
        fechaFinal &&
        !titulo &&
        codPais &&
        !tipoIntercambiar &&
        !localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['pais'] === codPais) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        fechaInicial &&
        fechaFinal &&
        !titulo &&
        codPais &&
        tipoIntercambiar &&
        !localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (inter.direccion['pais'] === codPais) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        fechaInicial &&
        fechaFinal &&
        !titulo &&
        codPais &&
        !tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (
                inter.direccion['pais'] === codPais &&
                inter.direccion['localidad'] === localidad
              ) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        fechaInicial &&
        fechaFinal &&
        !titulo &&
        codPais &&
        tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion) {
              if (
                inter.direccion['pais'] === codPais &&
                inter.direccion['localidad'] === localidad
              ) {
                arrayIntercambio.push(inter._id);
              }
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        !titulo &&
        !codPais &&
        tipoIntercambiar &&
        !localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel.find({
          $and: [
            { tipo: tipo },
            { tipoIntercambiar: tipoIntercambiar },
            { estado: codEstadoIntercambio },
          ],
        });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            arrayIntercambio.push(inter._id);
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        !titulo &&
        !codPais &&
        tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion['localidad'] === localidad) {
              arrayIntercambio.push(inter._id);
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        fechaInicial &&
        fechaFinal &&
        !titulo &&
        !codPais &&
        tipoIntercambiar &&
        !localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            arrayIntercambio.push(inter._id);
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        fechaInicial &&
        fechaFinal &&
        !titulo &&
        !codPais &&
        !tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { fechaCreacion: { $gt: I } },
              { fechaCreacion: { $lt: F } },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion['localidad'] === localidad) {
              arrayIntercambio.push(inter._id);
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        !titulo &&
        !codPais &&
        !tipoIntercambiar &&
        localidad
      ) {
        const I = new Date(fechaInicial);
        const fechaFin = fechaFinal + 'T23:59:59.999Z';
        const F = new Date(fechaFin);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            adjuntos,
            populatePerfil,
            populateDireccion,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion['localidad'] === localidad) {
              arrayIntercambio.push(inter._id);
            }
          }
        }

        totalIntercambios = await this.intercambioModel.paginate(
          { _id: { $in: arrayIntercambio } },
          options,
        );
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        titulo &&
        !codPais &&
        tipoIntercambiar &&
        !localidad
      ) {
        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [
              { tipo: tipo },
              { tipoIntercambiar: tipoIntercambiar },
              { estado: codEstadoIntercambio },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'pais',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            arrayIntercambio.push(inter._id);
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (
        !fechaInicial &&
        !fechaFinal &&
        titulo &&
        !codPais &&
        !tipoIntercambiar &&
        localidad
      ) {
        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { tituloCorto: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateIntercambio],
          page: Number(pagina),
          limit: Number(limite),
        };
        let getIntercambio = await this.intercambioModel
          .find({
            $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }],
          })
          .populate({
            path: 'direccion',
            select: 'pais localidad',
          });

        let arrayIntercambio = [];
        if (getIntercambio.length > 0) {
          for (const inter of getIntercambio) {
            if (inter.direccion['localidad'] === localidad) {
              arrayIntercambio.push(inter._id);
            }
          }
        }

        let totalTraducciones = await this.traduccionIntercambioModel.find({
          $or: [
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex1, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex2, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex3, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex5, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { intercambio: { $in: arrayIntercambio } },
                { tituloCorto: { $regex: regex6, $options: 'i' } },
                { estado: codEstadoTIntercambio },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayIntercambio = [];
        if (totalTraducciones.length > 0) {
          for (const inter of totalTraducciones) {
            const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
              {
                $and: [
                  { intercambio: inter.intercambio },
                  { estado: codEstadoTIntercambio },
                ],
              },
            );

            const find = getIdiomaIntercambio.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayIntercambio.push(find._id);
            }
          }
        }

        totalIntercambios = await this.traduccionIntercambioModel.paginate(
          {
            _id: { $in: arrayIntercambio },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataIntercambio = await this.obtenerTraduccionIntercambios(
          totalIntercambios,
          codIdim,
          codIdioma,
          codEstadoTIntercambio,
          opts,
        );

        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataIntercambio;
      }

      if (totalIntercambios.docs.length > 0) {
        const arrayIntercambios = [];
        for (const intercambio of totalIntercambios.docs) {
          let objDireccion: any;

          let traduccionDireccion = [];
          if (intercambio.direccion) {
            //Objeto de tipo direccion
            objDireccion = {
              _id: intercambio.direccion._id,
            };
            if (intercambio.direccion.localidad) {
              //Obtiene la localidad segun el codigo enviado
              const getLocalidad = await this.obtenerLocalidadIntervaloService.obtenerLocalidadIntervalo(
                intercambio.direccion.localidad,
                codIdioma,
              );
              objDireccion.localidad = getLocalidad;
            }

            if (intercambio.direccion.traducciones.length > 0) {
              let obtTraduccionDireccion = {
                descripcion: intercambio.direccion.traducciones[0].descripcion,
                idioma: intercambio.direccion.traducciones[0].idioma,
                original: intercambio.direccion.traducciones[0].original,
              };
              traduccionDireccion.push(obtTraduccionDireccion);

              objDireccion.traducciones = traduccionDireccion;
            } else {
              //traduce la direccion
              let getTradDir = await this.traducirDireccionService.traducirDireccion(
                intercambio.direccion._id,
                codIdim,
                opts,
              );
              if (getTradDir.length > 0) {
                objDireccion.traducciones = getTradDir;
              }
            }

            objDireccion.pais = intercambio.direccion.pais
              ? await this.obtenerLocalidadIntervaloService.obtenerPaisIntercambio(
                  intercambio.direccion.pais,
                  codIdim,
                )
              : null;
          }

          let getAdjuntos: any;
          // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
          getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
            intercambio.adjuntos,
            codIdim,
            opts,
          );

          //Traduce el intercambio en caso de no existir en el idioma enviado
          let getTraducirIntercambio: any;

          const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
            {
              $and: [
                { intercambio: intercambio._id },
                { estado: codEstadoTIntercambio },
              ],
            },
          );

          const find = getIdiomaIntercambio.find(
            element => element.idioma === codIdioma,
          );
          if (!find) {
            getTraducirIntercambio = await this.traducirIntercambioService.traducirIntercambio(
              intercambio._id,
              intercambio.perfil,
              codIdim,
              opts,
            );
          } else {
            getTraducirIntercambio = [
              {
                tituloCorto: find.tituloCorto,
                titulo: find.titulo,
                descripcion: find.descripcion,
              },
            ];
          }

          let objPerfil = {
            _id: intercambio.perfil._id,
            // nombre: intercambio.perfil.nombre,
            nombreContacto: intercambio.perfil.nombreContacto,
          };

          //Busca el tipo de intercambio
          // let getTipoIntercambio = await this.catalogoTipoIntercambioService.obtenerTipoIntercambioByCodigo(intercambio['tipo'], codIdim);

          let datos = {
            _id: intercambio._id,
            traducciones: getTraducirIntercambio,
            adjuntos: getAdjuntos,
            perfil: objPerfil,
            // direccion: objDireccion,
            status: {
              codigo: intercambio.status,
            },
            // tipo: getTipoIntercambio,
            estado: {
              codigo: intercambio.estado,
            },
            fechaCreacion: intercambio.fechaCreacion,
            fechaActualizacion: intercambio.fechaActualizacion,
          };
          arrayIntercambios.push(datos);
        }
        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return arrayIntercambios;
      } else {
        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Finaliza la transaccion
        await session.endSession();

        return totalIntercambios.docs;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async obtenerTraduccionIntercambios(
    intercambio: any,
    codIdim,
    codIdioma,
    codEstadoTIntercambio,
    opts,
  ): Promise<any> {
    let arrayIntercambio = [];
    if (intercambio.docs.length > 0) {
      for (const getIntercambio of intercambio.docs) {
        //Busca el tipo de intercambio
        let getTipoIntercambio = await this.catalogoTipoIntercambioService.obtenerTipoIntercambioByCodigo(
          getIntercambio.intercambio['tipo'],
          codIdim,
        );

        //Traduce el intercambio en caso de no existir en el idioma enviado
        let getTraducirIntercambio: any;

        const getIdiomaIntercambio = await this.traduccionIntercambioModel.find(
          {
            $and: [
              { intercambio: getIntercambio.intercambio['_id'] },
              { estado: codEstadoTIntercambio },
            ],
          },
        );

        const find = getIdiomaIntercambio.find(
          element => element.idioma === codIdioma,
        );
        if (!find) {
          getTraducirIntercambio = await this.traducirIntercambioService.traducirIntercambio(
            getIntercambio.intercambio['_id'],
            getIntercambio.intercambio['perfil']['_id'],
            codIdim,
            opts,
          );
        } else {
          getTraducirIntercambio = [
            {
              tituloCorto: find.tituloCorto,
              titulo: find.titulo,
              descripcion: find.descripcion,
            },
          ];
        }

        let getAdjuntos: any;
        // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
        getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
          getIntercambio.intercambio['adjuntos'],
          codIdim,
          opts,
        );

        let objPerfil = {
          _id: getIntercambio.intercambio['perfil']['_id'],
          // nombre: getIntercambio.intercambio['perfil']['nombre'],
          nombreContacto:
            getIntercambio.intercambio['perfil']['nombreContacto'],
        };

        let objDireccion: any;

        let traduccionDireccion = [];
        if (getIntercambio.intercambio['direccion']) {
          //Objeto de tipo direccion
          objDireccion = {
            _id: getIntercambio.intercambio['direccion']['_id'],
          };
          if (getIntercambio.intercambio['direccion']['localidad']) {
            //Obtiene la localidad segun el codigo enviado
            const getLocalidad = await this.obtenerLocalidadIntervaloService.obtenerLocalidadIntervalo(
              getIntercambio.intercambio['direccion']['localidad'],
              codIdioma,
            );
            objDireccion.localidad = getLocalidad;
          }

          if (
            getIntercambio.intercambio['direccion']['traducciones'].length > 0
          ) {
            let obtTraduccionDireccion = {
              descripcion:
                getIntercambio.intercambio['direccion']['traducciones'][0]
                  .descripcion,
              idioma:
                getIntercambio.intercambio['direccion']['traducciones'][0]
                  .idioma,
              original:
                getIntercambio.intercambio['direccion']['traducciones'][0]
                  .original,
            };
            traduccionDireccion.push(obtTraduccionDireccion);

            objDireccion.traducciones = traduccionDireccion;
          } else {
            //traduce la direccion
            let getTradDir = await this.traducirDireccionService.traducirDireccion(
              getIntercambio.intercambio['direccion']['_id'],
              codIdim,
              opts,
            );
            if (getTradDir.length > 0) {
              objDireccion.traducciones = getTradDir;
            }
          }

          objDireccion.pais = getIntercambio.intercambio['direccion']['pais']
            ? await this.obtenerLocalidadIntervaloService.obtenerPaisIntercambio(
                getIntercambio.intercambio['direccion']['pais'],
                codIdim,
              )
            : null;
        }

        let datos = {
          _id: getIntercambio.intercambio['_id'],
          traducciones: getTraducirIntercambio,
          adjuntos: getAdjuntos,
          perfil: objPerfil,
          // direccion: objDireccion,
          status: {
            codigo: getIntercambio.intercambio['status'],
          },
          // tipo: getTipoIntercambio,
          estado: {
            codigo: getIntercambio.intercambio['estado'],
          },
          fechaCreacion: getIntercambio.intercambio['fechaCreacion'],
          fechaActualizacion: getIntercambio.intercambio['fechaActualizacion'],
        };

        arrayIntercambio.push(datos);
      }
    }
    return arrayIntercambio;
  }
}
