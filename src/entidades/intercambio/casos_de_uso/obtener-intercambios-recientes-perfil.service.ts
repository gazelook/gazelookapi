import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { TraduccionIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_intercambio/traduccion-intercambio.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from 'src/entidades/proyectos/casos_de_uso/obtener-portada-predeterminada-proyecto-resumen.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
const mongoose = require('mongoose');

@Injectable()
export class ObtenerIntercambiosRecientesPerfilService {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: PaginateModel<Intercambio>,
    @Inject('TRADUCCION_INTERCAMBIO_MODEL')
    private readonly traduccionIntercambioModel: PaginateModel<
      TraduccionIntercambio
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
  ) {}

  async obtenerIntercambiosRecientePerfil(
    idPerfil: string,
    codIdim: string,
    limite: number,
    pagina: number,
    tipo: string,
    response: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const headerNombre = new HadersInterfaceNombres();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtener el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad traduccionIntercambio
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionIntercambio,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion intercambio
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTIntercambio = estado.codigo;

      //Obtiene el codigo de la entidad intercambio
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.intercambio,
      );
      let codEntidadIntercambio = entidadP.codigo;

      //Obtiene el estado activa de la entidad intercambio
      const estadoIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadIntercambio,
      );
      let codEstadoIntercambio = estadoIntercambio.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      const populateTraduccion = {
        path: 'traducciones',
        select: '-fechaActualizacion -fechaCreacion -__v -tags',
        match: {
          original: true,
          estado: codEstadoTIntercambio,
        },
      };

      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto ',
      };

      const options = {
        session: session,
        lean: true,
        sort: '-fechaCreacion',
        select: ' -__v ',
        populate: [populateTraduccion, adjuntos, populatePerfil],
        page: Number(pagina),
        limit: Number(limite),
      };

      let totalIntercambios = await this.intercambioModel.paginate(
        {
          $and: [
            { perfil: idPerfil },
            { tipo: tipo },
            { estado: codEstadoIntercambio },
          ],
        },
        options,
      );

      if (totalIntercambios.docs.length > 0) {
        const arrayIntercambios = [];
        for (const intercambio of totalIntercambios.docs) {
          let getAdjuntos: any;
          // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
          getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
            intercambio.adjuntos,
            codIdim,
            opts,
          );

          let getTraducirIntercambio = [
            {
              tituloCorto: intercambio.traducciones[0]['tituloCorto'],
              titulo: intercambio.traducciones[0]['titulo'],
            },
          ];

          // let getAlbumPerfil = await this.obtenerAlbumPerfilService.obtenerAlbumPerfil(intercambio.perfil['_id']);
          let objPerfil = {
            _id: intercambio.perfil['_id'],
            // nombre: intercambio.perfil['nombre'],
            nombreContacto: intercambio.perfil['nombreContacto'],
            // album:getAlbumPerfil
          };

          let datos = {
            _id: intercambio._id,
            traducciones: getTraducirIntercambio,
            adjuntos: getAdjuntos,
            perfil: objPerfil,
            status: {
              codigo: intercambio.status,
            },
            estado: {
              codigo: intercambio.estado,
            },
            fechaCreacion: intercambio.fechaCreacion,
            fechaActualizacion: intercambio.fechaActualizacion,
          };
          arrayIntercambios.push(datos);
        }
        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return arrayIntercambios;
      } else {
        response.set(headerNombre.totalDatos, totalIntercambios.totalDocs);
        response.set(headerNombre.totalPaginas, totalIntercambios.totalPages);
        response.set(headerNombre.proximaPagina, totalIntercambios.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          totalIntercambios.hasPrevPage,
        );

        //Finaliza la transaccion
        await session.endSession();

        return totalIntercambios.docs;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
