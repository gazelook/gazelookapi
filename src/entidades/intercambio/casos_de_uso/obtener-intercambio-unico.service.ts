import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { CatalogoColoresService } from 'src/entidades/catalogos/casos_de_uso/catalogo-colores.service';
import { CatalogoConfiguracionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-configuracion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerDireccionService } from 'src/entidades/direccion/casos_de_uso/obtener-direccion.service';
import { TraducirDireccionService } from 'src/entidades/direccion/casos_de_uso/traducir-direccion.service';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { ObtenerAlbumPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil.service';
import {
  codidosEstadosTraduccionDireccion,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ObtenerLocalidadIntervaloService } from './obtener-localidad-intervalo.service';
import { ObtenerMediasIntercambioService } from './obtener-medias-intercambio.service';
import { TraducirIntercambioService } from './traducir-intercambio.service';

const mongoose = require('mongoose');

@Injectable()
export class ObtenerIntercambioUnicoService {
  intercambio: any;

  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirIntercambioService: TraducirIntercambioService,
    private traduccionMediaService: TraduccionMediaService,
    private obtenerLocalidadIntervaloService: ObtenerLocalidadIntervaloService,
    private obtenerDireccionService: ObtenerDireccionService,
    private traducirDireccionService: TraducirDireccionService,
    private obtenerAlbumPerfilService: ObtenerAlbumPerfilService,
    private obtenerMediasIntercambioService: ObtenerMediasIntercambioService,
    private catalogoColoresService: CatalogoColoresService,
    private catalogoEstilosService: CatalogoEstilosService,
    private catalogoConfiguracionService: CatalogoConfiguracionService,
  ) {}

  async obtenerIntercambioUnico(
    idIntercambio: string,
    idPerfil: string,
    codIdioma: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    //Obtiene el codigo del idioma
    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codIdioma,
    );
    let codIdi = idioma.codigo;

    //Obtiene la entidad traduccion intercambio
    const entidadTraduccionIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionIntercambio,
    );
    let codEntidadTradIntercambio = entidadTraduccionIntercambio.codigo;

    //Obtiene el estado activa de la entidad traduccion intercambio
    const estadoTradIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradIntercambio,
    );
    let codEstadoTradIntercambio = estadoTradIntercambio.codigo;

    //Obtiene el codigo de la entidad intercambio
    const entidadIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.intercambio,
    );
    let codEntidadIntercambio = entidadIntercambio.codigo;

    //Obtiene el estado activo de la entidad proyectos
    const estadoIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.eliminado,
      codEntidadIntercambio,
    );
    let codEstadoIntercambio = estadoIntercambio.codigo;

    //Obtiene el estado activo de la entidad proyectos
    const estadoIntercambioActiva = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadIntercambio,
    );
    let codEstadoIntercambioActiva = estadoIntercambioActiva.codigo;

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let perfilPropietario = false;
      let getProyecto = await this.intercambioModel.findOne({
        _id: idIntercambio,
        estado: codEstadoIntercambioActiva,
        perfil: idPerfil,
      });
      if (getProyecto) {
        perfilPropietario = true;
      }
      const intercambio = await this.verificarTraduccion(
        idIntercambio,
        codIdioma,
        codIdi,
        codEstadoTradIntercambio,
        codEstadoIntercambio,
        opts,
        session,
        perfilPropietario,
        false,
      );

      if (intercambio) {
        let adjun = [];
        let objAdjun: any;
        let getAdjuntos: any;

        //Verifica si el intercambio tiene adjuntos
        if (intercambio.adjuntos.length > 0) {
          for (const album of intercambio.adjuntos) {
            let media = [];
            let objPortada: any;
            //Verifica si el album tiene portada
            if (album.portada) {
              //Verifica si la media tiene miniatura
              let objMiniatura: any;
              if (album.portada.miniatura) {
                //Objeto de tipo miniatura (archivo)
                objMiniatura = {
                  _id: album.portada.miniatura._id,
                  url: album.portada.miniatura.url,
                  tipo: {
                    codigo: album.portada.miniatura.tipo,
                  },
                  fileDefault: album.portada.miniatura.fileDefault,
                  fechaActualizacion:
                    album.portada.miniatura.fechaActualizacion,
                };
                if (album.portada.miniatura.duracion) {
                  objMiniatura.duracion = album.portada.miniatura.duracion;
                }
              }
              //Objeto de tipo portada(media)
              objPortada = {
                _id: album.portada._id,
                catalogoMedia: {
                  codigo: album.portada.catalogoMedia,
                },
                principal: {
                  _id: album.portada.principal._id,
                  url: album.portada.principal.url,
                  tipo: {
                    codigo: album.portada.principal.tipo,
                  },
                  fileDefault: album.portada.principal.fileDefault,
                  fechaCreacion: album.portada.principal.fechaCreacion,
                  fechaActualizacion:
                    album.portada.principal.fechaActualizacion,
                },
                miniatura: objMiniatura,
                fechaCreacion: album.portada.fechaCreacion,
                fechaActualizacion: album.portada.fechaActualizacion,
              };
              if (album.portada.principal.duracion) {
                objPortada.principal.duracion =
                  album.portada.principal.duracion;
              }

              if (album.portada.traducciones.length === 0) {
                const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                  album.portada._id,
                  codIdioma,
                  opts,
                );
                if (traduccionMedia) {
                  let arrayTrad = [];
                  const traducciones = {
                    _id: traduccionMedia._id,
                    descripcion: traduccionMedia.descripcion,
                  };
                  arrayTrad.push(traducciones);
                  objPortada.traducciones = arrayTrad;
                } else {
                  objPortada.traducciones = [];
                }
              } else {
                let arrayTraduccionesPort = [];
                arrayTraduccionesPort.push(album.portada.traducciones[0]);
                objPortada.traducciones = arrayTraduccionesPort;
              }
            }

            //Verifica si el album tiene medias
            if (album.media.length > 0) {
              for (const getMedia of album.media) {
                let obMiniatura: any;

                //Verifica si la media tiene miniatura
                if (getMedia.miniatura) {
                  //Objeto de tipo miniatura (archivo)
                  obMiniatura = {
                    _id: getMedia.miniatura._id,
                    url: getMedia.miniatura.url,
                    tipo: {
                      codigo: getMedia.miniatura.tipo,
                    },
                    fileDefault: getMedia.miniatura.fileDefault,
                    fechaActualizacion: getMedia.miniatura.fechaActualizacion,
                  };
                  if (getMedia.miniatura.duracion) {
                    obMiniatura.duracion = getMedia.miniatura.duracion;
                  }
                }
                //Objeto de tipo media
                let objMedia: any = {
                  _id: getMedia._id,
                  catalogoMedia: {
                    codigo: getMedia.catalogoMedia,
                  },
                  principal: {
                    _id: getMedia.principal._id,
                    url: getMedia.principal.url,
                    tipo: {
                      codigo: getMedia.principal.tipo,
                    },
                    fileDefault: getMedia.principal.fileDefault,
                    fechaCreacion: getMedia.principal.fechaCreacion,
                    fechaActualizacion: getMedia.principal.fechaActualizacion,
                  },
                  miniatura: obMiniatura,
                  fechaCreacion: getMedia.fechaCreacion,
                  fechaActualizacion: getMedia.fechaActualizacion,
                };
                if (getMedia.principal.duracion) {
                  objMedia.principal.duracion = getMedia.principal.duracion;
                }
                //Verifica si existe la traduccion de la media

                if (getMedia.traducciones[0] === undefined) {
                  const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                    getMedia._id,
                    codIdioma,
                    opts,
                  );
                  if (traduccionMedia) {
                    let arrayTrad = [];
                    const traducciones = {
                      _id: traduccionMedia._id,
                      descripcion: traduccionMedia.descripcion,
                    };
                    arrayTrad.push(traducciones);
                    objMedia.traducciones = arrayTrad;
                  } else {
                    objMedia.traducciones = [];
                  }
                } else {
                  let arrayTraducciones = [];
                  arrayTraducciones.push(getMedia.traducciones[0]);
                  objMedia.traducciones = arrayTraducciones;
                }

                media.push(objMedia);
              }
            }

            //Objeto de tipo adjunto (album)
            objAdjun = {
              _id: album._id,
              tipo: {
                codigo: album.tipo,
              },
              traducciones: album.traducciones,
              predeterminado: album.predeterminado,
              portada: objPortada,
              media: media,
            };
            adjun.push(objAdjun);
          }
        }
        getAdjuntos = adjun;

        //Data de traducciones del intercambio
        let traduccionProyecto = [];
        if (intercambio.traducciones.length > 0) {
          for (const tradInter of intercambio.traducciones) {
            //Objeto de tipo traducccion intercambio
            let trad = {
              tituloCorto: tradInter.tituloCorto,
              titulo: tradInter.titulo,
              descripcion: tradInter.descripcion,
              idioma: {
                codigo: tradInter.idioma,
              },
              original: tradInter.original,
            };
            traduccionProyecto.push(trad);
          }
        }

        let objDireccion: any;
        let traduccionDireccion = [];
        if (intercambio.direccion) {
          //Objeto de tipo direccion
          objDireccion = {
            _id: intercambio.direccion._id,
          };
          if (intercambio.direccion.localidad) {
            console.log(
              '**********************************************************************',
            );
            //Obtiene la localidad segun el codigo enviado
            const getLocalidad = await this.obtenerLocalidadIntervaloService.obtenerLocalidadIntervalo(
              intercambio.direccion.localidad,
              codIdi,
            );
            objDireccion.localidad = getLocalidad;
            console.log(
              '**********************************************************************',
            );
          }

          // if (proyecto.direccion.traducciones) {

          console.log(
            'TRADUCCION DIRECCION: ',
            intercambio.direccion.traducciones.length,
          );
          if (intercambio.direccion.traducciones.length > 0) {
            //Si la traduccion no es la original
            if (!intercambio.direccion.traducciones[0].original) {
              console.log('direccion NO es original');
              let obtTraduccionDireccion = {
                descripcion: intercambio.direccion.traducciones[0].descripcion,
                idioma: intercambio.direccion.traducciones[0].idioma,
                original: intercambio.direccion.traducciones[0].original,
              };
              traduccionDireccion.push(obtTraduccionDireccion);

              //Obtiene traduccion original
              let getTraDirecOriginal = await this.obtenerDireccionService.obtenerDireccionTraduccionOriginal(
                intercambio.direccion._id,
                opts,
              );
              traduccionDireccion.push(getTraDirecOriginal);

              objDireccion.traducciones = traduccionDireccion;
            } else {
              console.log('direccion SI es original');
              let obtTraduccionDireccion = {
                descripcion: intercambio.direccion.traducciones[0].descripcion,
                idioma: intercambio.direccion.traducciones[0].idioma,
                original: intercambio.direccion.traducciones[0].original,
              };
              traduccionDireccion.push(obtTraduccionDireccion);

              objDireccion.traducciones = traduccionDireccion;
            }
          } else {
            //traduce la direccion
            let getTradDir = await this.traducirDireccionService.traducirDireccion(
              intercambio.direccion._id,
              codIdioma,
              opts,
            );
            if (getTradDir.length > 0) {
              objDireccion.traducciones = getTradDir;
              //Obtiene traduccion original
              let getTraDirecOriginal = await this.obtenerDireccionService.obtenerDireccionTraduccionOriginal(
                intercambio.direccion._id,
                opts,
              );

              objDireccion.traducciones.push(getTraDirecOriginal);
            }
          }
          // }

          objDireccion.pais = intercambio.direccion.pais
            ? await this.obtenerLocalidadIntervaloService.obtenerPaisIntercambio(
                intercambio.direccion.pais,
                codIdioma,
              )
            : null;
          objDireccion.latitud = intercambio.direccion.latitud;
          objDireccion.longitud = intercambio.direccion.longitud;
        }

        console.log(
          '******************************------------------------**************************',
        );
        let getAlbumPerfil = await this.obtenerAlbumPerfilService.obtenerAlbumPerfil(
          intercambio.perfil['_id'],
        );
        // getAlbumPerfil._id = intercambio.perfil['_id']
        let dataPerfil = {
          _id: intercambio.perfil['_id'],
          nombreContacto: intercambio.perfil['nombreContacto'],
          nombreContactoTraducido:
            intercambio.perfil['nombreContactoTraducido'],
          album: [getAlbumPerfil.album],
        };

        //Data Participantes
        let arrayParticipantes = [];
        //Verifica si el proyecto tiene coemtnarios
        if (intercambio.participantes.length > 0) {
          for (const participantes of intercambio.participantes) {
            console.log('PARTICIOANTE INTERCAMBIO: ', participantes._id);
            if (participantes.coautor) {
              let arrayRolParticipante = [];
              if (participantes.roles.length > 0) {
                for (const rolParticipante of participantes.roles) {
                  let arrayAccParticipante = [];
                  if (rolParticipante.acciones.length > 0) {
                    for (const getAcciones of rolParticipante.acciones) {
                      let objAccionesPart = {
                        _id: getAcciones._id,
                        codigo: getAcciones.codigo,
                        nombre: getAcciones.nombre,
                      };
                      arrayAccParticipante.push(objAccionesPart);
                    }
                  }

                  let objRolesPartic = {
                    _id: rolParticipante._id,
                    nombre: rolParticipante.nombre,
                    rol: {
                      codigo: rolParticipante.rol,
                    },
                    acciones: arrayAccParticipante,
                  };
                  arrayRolParticipante.push(objRolesPartic);
                }
              }
              let arrayComentPartic = [];

              if (participantes.comentarios.length > 0) {
                for (const comentParticipante of participantes.comentarios) {
                  let ObjCoautorParticProyecto: any;
                  if (comentParticipante.coautor.coautor) {
                    ObjCoautorParticProyecto = {
                      coautor: {
                        _id: comentParticipante.coautor.coautor._id,
                        nombreContacto:
                          comentParticipante.coautor.coautor.nombreContacto,
                        nombreContactoTraducido:
                          comentParticipante.coautor.coautor
                            .nombreContactoTraducido,
                        nombre: comentParticipante.coautor.coautor.nombre,
                      },
                    };
                  }

                  //Verifica si el comentario del paarticipante proyecto tiene adjuntos
                  let arrayAdjComentPartic: any;

                  if (comentParticipante.adjuntos.length > 0) {
                    // llamamos al servicio para obtener medias
                    arrayAdjComentPartic = await this.obtenerMediasIntercambioService.obtenerMediasIntercambio(
                      comentParticipante.adjuntos,
                      codIdioma,
                      opts,
                    );
                  }

                  let objComentPartic: any = {
                    _id: comentParticipante._id,
                    // traducciones: arrayTraducComentarioParti,
                    coautor: ObjCoautorParticProyecto,
                    importante: comentParticipante.importante,
                    tipo: {
                      codigo: comentParticipante.tipo,
                    },
                    adjuntos: arrayAdjComentPartic,
                  };
                  let arrayTraducComentarioParti = [];
                  //Verifica si el comentario del participante tiene traducciones
                  if (comentParticipante.traducciones.length > 0) {
                    let objTradComentPartic = {
                      _id: comentParticipante.traducciones[0]._id,
                      texto: comentParticipante.traducciones[0].texto,
                      idioma: {
                        codigo: comentParticipante.traducciones[0].idioma,
                      },
                      original: comentParticipante.traducciones[0].original,
                    };
                    arrayTraducComentarioParti.push(objTradComentPartic);
                    objComentPartic.traducciones = arrayTraducComentarioParti;
                  }
                  arrayComentPartic.push(objComentPartic);
                }
              }

              let arrayConfigPartic = [];
              //Verifica si el participante tiene configuraciones
              if (participantes.configuraciones.length > 0) {
                for (const confParticipante of participantes.configuraciones) {
                  let arrayEstilosConfigPartic = [];
                  //Verifica si tiene estilos
                  if (confParticipante.estilos.length > 0) {
                    for (const estilosParticipante of confParticipante.estilos) {
                      //Obtiene el color
                      console.log(
                        'acaaaaaaaaaa se caeeeeeeeeeeeee: ',
                        estilosParticipante,
                      );
                      const getColor = await this.catalogoColoresService.obtenerCatalogoColoresByCodigo(
                        estilosParticipante.color,
                      );

                      //Objeto de tipo catalogo estilos
                      //Obtiene el catalogo estilos
                      const getCatEstilos = await this.catalogoEstilosService.obtenerCatalogoEstilosByCodigo(
                        estilosParticipante.tipo,
                      );

                      //Verifica si el estilo tiene media
                      let mediaEstiloPartic: any;
                      if (estilosParticipante.media) {
                        //Data de medias
                        //Llama al metodo del obtener medias
                        mediaEstiloPartic = await this.obtenerMediasIntercambioService.obtenerMediaUnicaIntercambio(
                          estilosParticipante.media,
                          codIdioma,
                          opts,
                        );
                      }
                      let objEstiloPartic = {
                        _id: estilosParticipante._id,
                        codigo: estilosParticipante.codigo,
                        color: getColor,
                        tipo: getCatEstilos,
                        media: mediaEstiloPartic,
                      };
                      arrayEstilosConfigPartic.push(objEstiloPartic);
                    }
                  }

                  //Verifica si la configuracion tiene tono de notificacion
                  let objMediaTonoNotificacion: any;
                  if (confParticipante.tonoNotificacion) {
                    //Data de medias
                    //Llama al metodo del obtener medias
                    objMediaTonoNotificacion = await this.obtenerMediasIntercambioService.obtenerMediaUnicaIntercambio(
                      confParticipante.tonoNotificacion,
                      codIdioma,
                      opts,
                    );
                  }

                  //Obtiene el catalogo configuracion segun el codigo
                  const getCatConfiguracion = await this.catalogoConfiguracionService.obtenerCatalogoConfiguracionByCodigo(
                    confParticipante.tipo,
                  );

                  let objConfigPartic = {
                    _id: confParticipante._id,
                    codigo: confParticipante.codigo,
                    tonoNotificacion: objMediaTonoNotificacion,
                    estilos: arrayEstilosConfigPartic,
                    tipo: getCatConfiguracion,
                  };
                  arrayConfigPartic.push(objConfigPartic);
                }
              }
              let objCoautorPartic = {
                _id: participantes.coautor._id,
                nombreContacto: participantes.coautor.nombreContacto,
                nombreContactoTraducido:
                  participantes.coautor.nombreContactoTraducido,
                nombre: participantes.coautor.nombre,
              };
              //objeto de tipo comentario
              let objParticipantes = {
                _id: participantes._id,
                roles: arrayRolParticipante,
                comentarios: arrayComentPartic,
                configuraciones: arrayConfigPartic,
                coautor: objCoautorPartic,
              };
              arrayParticipantes.push(objParticipantes);
            }
          }
        }

        //Objeto de intercambio
        let datos = {
          _id: intercambio._id,
          traducciones: traduccionProyecto,
          adjuntos: getAdjuntos,
          perfil: dataPerfil,
          direccion: objDireccion || null,
          tipo: {
            codigo: intercambio.tipo,
          },
          tipoIntercambiar: {
            codigo: intercambio.tipoIntercambiar,
          },
          participantes: arrayParticipantes,
          // comentarios:,
          status: {
            codigo: intercambio.status,
          },
          email: intercambio.email,
          estado: {
            codigo: intercambio.estado,
          },
          fechaCreacion: intercambio.fechaCreacion,
          fechaActualizacion: intercambio.fechaActualizacion,
        };

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return datos;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async verificarTraduccion(
    idIntercambio,
    codIdioma,
    codIdim,
    codEstadoTradIntercambio,
    codEstadoIntercambio,
    opts,
    session,
    perfilPropietario,
    original,
  ) {
    //Obtiene el codigo de la entidad media
    const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    let codEntidadMedia = entidadmedia.codigo;

    //Obtiene el estado activo de la entidad intercambio
    const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadMedia,
    );
    let codEstadoMedia = estadoMedia.codigo;

    //Obtiene l codigo de la entidad album
    const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.album,
    );
    let codEntidadAlbum = entidadAlbum.codigo;

    //Obtiene el estado activo de la entidad album
    const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadAlbum,
    );
    let codEstadoAlbum = estadoAlbum.codigo;

    //Obtiene el codigo de la entidad traduccion media
    const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    let codEntidadTradMedia = entidadTradMedia.codigo;

    //Obtiene el estado activo de la entidad traduccion media
    const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradMedia,
    );
    let codEstadoTradMedia = estadoTradMedia.codigo;

    //Obtiene el codigo de la entidad comentarios
    const entidaComentarios = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.comentarios,
    );
    let codEntidadComentarios = entidaComentarios.codigo;

    //Obtiene el estado activo de la entidad coemntarios
    const estadoComentarios = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadComentarios,
    );
    let codEstadoComentarios = estadoComentarios.codigo;

    //Obtiene el codigo de la entidad traduccion comentarios
    const entidaTradComentarios = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionComentario,
    );
    let codEntidadTradComentarios = entidaTradComentarios.codigo;

    //Obtiene el estado activo de la entidad coemntarios
    const estadoTradComentarios = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradComentarios,
    );
    let codEstadoTradComentarios = estadoTradComentarios.codigo;

    //Obtiene el codigo de la entidad participante intercambio
    const entidadPartiIntercambio = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.participanteIntercambio,
    );
    let codEntidadPartIntercambio = entidadPartiIntercambio.codigo;

    //Obtiene el estado activo de la entidad participante intercambio
    const estadoPartiIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadPartIntercambio,
    );
    let codEstadoPartiIntercambio = estadoPartiIntercambio.codigo;

    //Obtiene el codigo de la entidad participante proyecto
    const entidadConfigEstilo = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.configuracionEstilo,
    );
    let codEntidadConfigEstilo = entidadConfigEstilo.codigo;

    //Obtiene el estado activo de la entidad participante proyecto
    const estadoConfigEstilo = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadConfigEstilo,
    );
    let codEstadoConfigEstilo = estadoConfigEstilo.codigo;

    let intercambioTraduccionOriginal;

    console.log('original: ', original);
    console.log('perfilPropietario: ', perfilPropietario);
    console.log('codEstadoTradIntercambio: ', codEstadoTradIntercambio);
    if (!original) {
      if (perfilPropietario) {
        this.intercambio = await this.intercambioModel
          .findOne({
            $and: [
              { _id: idIntercambio },
              { estado: { $ne: codEstadoIntercambio } },
            ],
          })
          .session(session)
          .select('-__v')
          .populate({
            path: 'traducciones',
            select: '-fechaActualizacion -__v ',
            match: {
              original: true,
              estado: codEstadoTradIntercambio,
            },
          })
          .populate({
            path: 'perfil',
            select: 'nombre nombreContacto nombreContactoTraducido _id',
          })
          .populate({
            path: 'adjuntos',
            select: '-fechaActualizacion -fechaCreacion -__v -nombre',
            match: { estado: codEstadoAlbum },
            populate: [
              {
                path: 'media',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: ' url tipo duracion fileDefault fechaActualizacion',
                  },
                  {
                    path: 'miniatura',
                    select: ' url tipo duracion fileDefault fechaActualizacion',
                  },
                ],
              },
              {
                path: 'portada',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: 'url tipo duracion fileDefault fechaActualizacion',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fileDefault fechaActualizacion',
                  },
                ],
              },
            ],
          })
          .populate({
            path: 'comentarios',
            select: 'adjuntos traducciones coautor importante tipo',
            match: { estado: codEstadoComentarios },
            populate: [
              {
                path: 'adjuntos',
                match: { estado: codEstadoMedia },
                select:
                  'principal traducciones enlace miniatura fechaCreacion fechaActualizacion catalogoMedia traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select:
                      'url tipo duracion  fileDefault fechaActualizacion ',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fileDefault fechaActualizacion ',
                  },
                ],
              },
              {
                path: 'traducciones',
                select: 'texto idioma original _id',
                match: { estado: codEstadoTradComentarios },
              },
              {
                path: 'coautor',
                select: '-_id coautor',
                populate: {
                  path: 'coautor',
                  select: '_id nombre nombreContacto nombreContactoTraducido',
                },
              },
            ],
          })
          .populate({
            path: 'participantes',
            select: 'configuraciones coautor comentarios roles',
            match: { estado: codEstadoPartiIntercambio },
            populate: [
              {
                path: 'configuraciones',
                select: 'codigo estilos tonoNotificacion tipo',
                match: { estado: codEstadoConfigEstilo },
                populate: [
                  {
                    path: 'estilos',
                    select: 'codigo media color tipo',
                    populate: [
                      {
                        path: 'media',
                        match: { estado: codEstadoMedia },
                        select:
                          'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion',
                        populate: [
                          {
                            path: 'traducciones',
                            select: 'descripcion',
                            match: {
                              idioma: codIdim,
                              estado: codEstadoTradMedia,
                            },
                          },
                          {
                            path: 'principal',
                            select:
                              ' url tipo duracion fileDefault fechaActualizacion',
                          },
                          {
                            path: 'miniatura',
                            select:
                              ' url tipo duracion fileDefault fechaActualizacion',
                          },
                        ],
                      },
                    ],
                  },
                ],
              },
              {
                path: 'coautor',
                select: '_id nombre nombreContacto nombreContactoTraducido',
              },
              {
                path: 'comentarios',
                select: 'adjuntos traducciones coautor importante tipo',
                match: { estado: codEstadoComentarios },
                populate: [
                  {
                    path: 'adjuntos',
                    match: { estado: codEstadoMedia },
                    select:
                      'principal traducciones enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
                    populate: [
                      {
                        path: 'traducciones',
                        select: 'descripcion',
                        match: { idioma: codIdim, estado: codEstadoTradMedia },
                      },
                      {
                        path: 'principal',
                        select:
                          'url tipo duracion  fileDefault fechaActualizacion ',
                      },
                      {
                        path: 'miniatura',
                        select:
                          'url tipo duracion fileDefault fechaActualizacion ',
                      },
                    ],
                  },
                  {
                    path: 'traducciones',
                    select: 'texto idioma original _id',
                    match: { estado: codEstadoTradComentarios },
                  },
                  {
                    path: 'coautor',
                    select: '-_id coautor',
                    populate: {
                      path: 'coautor',
                      select:
                        '_id nombre nombreContacto nombreContactoTraducido',
                    },
                  },
                ],
              },
              {
                path: 'roles',
                select: 'nombre rol acciones',
                populate: {
                  path: 'acciones',
                  select: 'codigo nombre',
                },
              },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'traducciones localidad pais latitud longitud',
            populate: {
              path: 'traducciones',
              select: 'descripcion idioma original estado',
              match: {
                estado: codidosEstadosTraduccionDireccion.activa,
                original: true,
              },
            },
          });
      } else {
        this.intercambio = await this.intercambioModel
          .findOne({
            $and: [
              { _id: idIntercambio },
              { estado: { $ne: codEstadoIntercambio } },
            ],
          })
          .session(session)
          .select('-__v')
          .populate({
            path: 'traducciones',
            select: '-fechaActualizacion -__v ',
            match: {
              idioma: codIdim,
              estado: codEstadoTradIntercambio,
            },
          })
          .populate({
            path: 'perfil',
            select: 'nombre nombreContacto nombreContactoTraducido _id',
          })
          .populate({
            path: 'adjuntos',
            select: '-fechaActualizacion -fechaCreacion -__v -nombre',
            match: { estado: codEstadoAlbum },
            populate: [
              {
                path: 'media',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: ' url tipo duracion fileDefault fechaActualizacion',
                  },
                  {
                    path: 'miniatura',
                    select: ' url tipo duracion fileDefault fechaActualizacion',
                  },
                ],
              },
              {
                path: 'portada',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: 'url tipo duracion fileDefault fechaActualizacion',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fileDefault fechaActualizacion',
                  },
                ],
              },
            ],
          })
          .populate({
            path: 'comentarios',
            select: 'adjuntos traducciones coautor importante tipo',
            match: { estado: codEstadoComentarios },
            populate: [
              {
                path: 'adjuntos',
                match: { estado: codEstadoMedia },
                select:
                  'principal traducciones enlace miniatura fechaCreacion fechaActualizacion catalogoMedia traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select:
                      'url tipo duracion  fileDefault fechaActualizacion ',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fileDefault fechaActualizacion ',
                  },
                ],
              },
              {
                path: 'traducciones',
                select: 'texto idioma original _id',
                match: { estado: codEstadoTradComentarios },
              },
              {
                path: 'coautor',
                select: '-_id coautor',
                populate: {
                  path: 'coautor',
                  select: '_id nombre nombreContacto nombreContactoTraducido',
                },
              },
            ],
          })
          .populate({
            path: 'participantes',
            select: 'configuraciones coautor comentarios roles',
            match: { estado: codEstadoPartiIntercambio },
            populate: [
              {
                path: 'configuraciones',
                select: 'codigo estilos tonoNotificacion tipo',
                match: { estado: codEstadoConfigEstilo },
                populate: [
                  {
                    path: 'estilos',
                    select: 'codigo media color tipo',
                    populate: [
                      {
                        path: 'media',
                        match: { estado: codEstadoMedia },
                        select:
                          'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion',
                        populate: [
                          {
                            path: 'traducciones',
                            select: 'descripcion',
                            match: {
                              idioma: codIdim,
                              estado: codEstadoTradMedia,
                            },
                          },
                          {
                            path: 'principal',
                            select:
                              ' url tipo duracion fileDefault fechaActualizacion',
                          },
                          {
                            path: 'miniatura',
                            select:
                              ' url tipo duracion fileDefault fechaActualizacion',
                          },
                        ],
                      },
                    ],
                  },
                ],
              },
              {
                path: 'coautor',
                select: '_id nombre nombreContacto nombreContactoTraducido',
              },
              {
                path: 'comentarios',
                select: 'adjuntos traducciones coautor importante tipo',
                match: { estado: codEstadoComentarios },
                populate: [
                  {
                    path: 'adjuntos',
                    match: { estado: codEstadoMedia },
                    select:
                      'principal traducciones enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
                    populate: [
                      {
                        path: 'traducciones',
                        select: 'descripcion',
                        match: { idioma: codIdim, estado: codEstadoTradMedia },
                      },
                      {
                        path: 'principal',
                        select:
                          'url tipo duracion  fileDefault fechaActualizacion ',
                      },
                      {
                        path: 'miniatura',
                        select:
                          'url tipo duracion fileDefault fechaActualizacion ',
                      },
                    ],
                  },
                  {
                    path: 'traducciones',
                    select: 'texto idioma original _id',
                    match: { estado: codEstadoTradComentarios },
                  },
                  {
                    path: 'coautor',
                    select: '-_id coautor',
                    populate: {
                      path: 'coautor',
                      select:
                        '_id nombre nombreContacto nombreContactoTraducido',
                    },
                  },
                ],
              },
              {
                path: 'roles',
                select: 'nombre rol acciones',
                populate: {
                  path: 'acciones',
                  select: 'codigo nombre',
                },
              },
            ],
          })
          .populate({
            path: 'direccion',
            select: 'traducciones localidad pais latitud longitud',
            populate: {
              path: 'traducciones',
              select: 'descripcion idioma original estado',
              match: {
                estado: codidosEstadosTraduccionDireccion.activa,
                idioma: codIdim,
              },
            },
          });
      }
    } else {
      //Obtiene el intercambio en su traduccion original
      intercambioTraduccionOriginal = await this.intercambioModel
        .findOne({
          $and: [
            { _id: idIntercambio },
            { estado: { $ne: codEstadoIntercambio } },
          ],
        })
        .session(session)
        .select('-__v')
        .populate({
          path: 'traducciones',
          select: '-fechaActualizacion -__v ',
          match: {
            original: true,
            estado: codEstadoTradIntercambio,
          },
        })
        .populate({
          path: 'direccion',
          select: 'traducciones localidad pais latitud longitud',
          populate: {
            path: 'traducciones',
            select: 'descripcion idioma original estado',
            match: {
              estado: codidosEstadosTraduccionDireccion.activa,
              original: true,
            },
          },
        });
    }

    if (intercambioTraduccionOriginal) {
      console.log('acaaaaaaaaaaaaaaaaaaaaaaa');
      await this.intercambio.traducciones.push(
        intercambioTraduccionOriginal.traducciones[0],
      );
      return this.intercambio;
    }

    if (this.intercambio) {
      if (this.intercambio.traducciones.length === 0) {
        const idPerfil = this.intercambio.perfil;

        await this.traducirIntercambioService.traducirIntercambio(
          idIntercambio,
          idPerfil,
          codIdioma,
          opts,
        );

        return await this.verificarTraduccion(
          idIntercambio,
          codIdioma,
          codIdim,
          codEstadoTradIntercambio,
          codEstadoIntercambio,
          opts,
          session,
          perfilPropietario,
          false,
        );
      } else {
        if (this.intercambio.traducciones[0].original) {
          return this.intercambio;
        } else {
          return await this.verificarTraduccion(
            idIntercambio,
            codIdioma,
            codIdim,
            codEstadoTradIntercambio,
            codEstadoIntercambio,
            opts,
            session,
            perfilPropietario,
            true,
          );
        }
      }
    } else {
      return null;
    }
  }
}
