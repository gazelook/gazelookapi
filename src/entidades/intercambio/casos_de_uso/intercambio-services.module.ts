import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { AlbumServiceModule } from 'src/entidades/album/casos_de_uso/album.services.module';
import { ArchivoServicesModule } from 'src/entidades/archivo/casos_de_uso/archivo-services.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { CatalogoProviders } from 'src/entidades/catalogos/drivers/catalogo.provider';
import { ComentariosIntercambioServicesModule } from 'src/entidades/comentarios_intercambio/casos_de_uso/comentarios-intercambio-services.module';
import { DireccionServiceModule } from 'src/entidades/direccion/casos_de_uso/direccion.services.module';
import { EmailServicesModule } from 'src/entidades/emails/casos_de_uso/email-services.module';
import { MediaServiceModule } from 'src/entidades/media/casos_de_uso/media.services.module';
import { CrearLocalidadService } from 'src/entidades/pais/casos_de_uso/crear-localidad.service';
import { CrearParticipanteIntercambioService } from 'src/entidades/participante_intercambio/casos_de_uso/crear-participante-intercambio.service';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from 'src/entidades/proyectos/casos_de_uso/obtener-portada-predeterminada-proyecto-resumen.service';
import { RolServiceModule } from 'src/entidades/rol/casos_de_uso/rol.services.module';
import { UsuarioServicesModule } from 'src/entidades/usuario/casos_de_uso/usuario-services.module';
import { intercambioProviders } from '../drivers/intercambio.provider';
import { ActualizarIntercambioService } from './actualizar-intercambio.service';
import { CambiarEstatusIntercambioService } from './cambiar-estatus-intercambio.service';
import { CrearIntercambioService } from './crear-intercambio.service';
import { EliminarIntercambioUnicoService } from './eliminar-intercambio-unico.service';
import { ObtenerIntercambioIdService } from './obtener-intercambio-id.service';
import { ObtenerIntercambioPerfilService } from './obtener-intercambio-perfil.service';
import { ObtenerIntercambioUnicoService } from './obtener-intercambio-unico.service';
import { ObtenerIntercambiosRecientePaginacionService } from './obtener-intercambios-recientes-paginacion.service';
import { ObtenerIntercambiosRecientesPerfilService } from './obtener-intercambios-recientes-perfil.service';
import { ObtenerLocalidadIntervaloService } from './obtener-localidad-intervalo.service';
import { ObtenerMediasIntercambioService } from './obtener-medias-intercambio.service';
import { CatalogoTipoIntercambioService } from './obtener-tipo-intercambio.service';
import { TraducirIntercambioServiceSegundoPlano } from './traducir-intercambio-segundo-plano.service';
import { TraducirIntercambioService } from './traducir-intercambio.service';
import { TraducirCatalogoTipoIntercambioService } from './traducir-tipo-Intercambio.service';

@Module({
  imports: [
    DBModule,
    forwardRef(() => UsuarioServicesModule),
    forwardRef(() => EmailServicesModule),
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => AlbumServiceModule),
    forwardRef(() => DireccionServiceModule),
    forwardRef(() => ArchivoServicesModule),
    forwardRef(() => CatalogosServiceModule),
    forwardRef(() => AlbumServiceModule),
    forwardRef(() => MediaServiceModule),
    forwardRef(() => DireccionServiceModule),
    forwardRef(() => ArchivoServicesModule),
    ComentariosIntercambioServicesModule,
    RolServiceModule,
  ],
  providers: [
    ...intercambioProviders,
    ...CatalogoProviders,
    CrearIntercambioService,
    TraducirIntercambioServiceSegundoPlano,
    CambiarEstatusIntercambioService,
    ActualizarIntercambioService,
    CatalogoTipoIntercambioService,
    TraducirCatalogoTipoIntercambioService,
    TraducirIntercambioService,
    ObtenerIntercambiosRecientePaginacionService,
    ObtenerPortadaPredeterminadaProyectoResumenService,
    ObtenerLocalidadIntervaloService,
    EliminarIntercambioUnicoService,
    ObtenerIntercambioUnicoService,
    CrearLocalidadService,
    ObtenerIntercambiosRecientesPerfilService,
    ObtenerIntercambioIdService,
    CrearParticipanteIntercambioService,
    ObtenerMediasIntercambioService,
    ObtenerIntercambioPerfilService,
  ],
  exports: [
    CrearIntercambioService,
    TraducirIntercambioServiceSegundoPlano,
    CambiarEstatusIntercambioService,
    ActualizarIntercambioService,
    CatalogoTipoIntercambioService,
    TraducirIntercambioService,
    ObtenerIntercambiosRecientePaginacionService,
    ObtenerPortadaPredeterminadaProyectoResumenService,
    ObtenerLocalidadIntervaloService,
    EliminarIntercambioUnicoService,
    ObtenerIntercambioUnicoService,
    ObtenerIntercambiosRecientesPerfilService,
    ObtenerIntercambioIdService,
    CrearParticipanteIntercambioService,
    ObtenerMediasIntercambioService,
    ObtenerIntercambioPerfilService,
  ],
  controllers: [],
})
export class IntercambioServicesModule {}
