import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { TraduccionIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_intercambio/traduccion-intercambio.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codigoEntidades,
  codigosCatalogoAcciones,
  codigosEstadosTraduccionIntercambio,
} from 'src/shared/enum-sistema';

@Injectable()
export class TraducirIntercambioService {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    @Inject('TRADUCCION_INTERCAMBIO_MODEL')
    private readonly traduccionIntercambioModel: Model<TraduccionIntercambio>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async traducirIntercambio(idIntercambio, perfil, idioma, opts): Promise<any> {
    try {
      //Obtener el codigo del idioma
      const getIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      let codIdioma = getIdioma.codigo;

      //Obtiene la traduccion en el idioma original
      const getTraductorOriginal = await this.traduccionIntercambioModel.findOne(
        {
          intercambio: idIntercambio,
          original: true,
          estado: codigosEstadosTraduccionIntercambio.activa,
        },
      );

      let textoTraducidoTitulo = await traducirTexto(
        idioma,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTraducidoTitulo.textoTraducido;

      let textoTraducidoTituloCorto = await traducirTexto(
        idioma,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTraducidoTituloCorto.textoTraducido;

      let descripcion;
      if (getTraductorOriginal.descripcion) {
        let textoTraducidoDescripcion = await traducirTexto(
          idioma,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTraducidoDescripcion.textoTraducido;
      }

      //datos para guardar la nueva traduccion
      let newTraduccionIntercambio = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        idioma: codIdioma,
        original: false,
        estado: codigosEstadosTraduccionIntercambio.activa,
        intercambio: idIntercambio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionIntercambioModel(
        newTraduccionIntercambio,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del intercambio
      let newHistoricoTraduccionIntercambio: any = {
        datos: dataTraduccion,
        usuario: perfil.toString(),
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionIntercambio,
      };

      //guarda el historico de la nueva traduccion de intercambio
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionIntercambio,
      );

      //Actualiza el array de id de traducciones
      await this.intercambioModel.updateOne(
        { _id: idIntercambio },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      let returnTraduccion = [
        {
          titulo: dataTraduccion.titulo,
          tituloCorto: dataTraduccion.tituloCorto,
          descripcion: dataTraduccion.descripcion,
        },
      ];

      return returnTraduccion;
    } catch (error) {
      throw error;
    }
  }
}
