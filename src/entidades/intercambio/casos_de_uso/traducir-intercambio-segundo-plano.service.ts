import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { TraduccionIntercambio } from 'src/drivers/mongoose/interfaces/traduccion_intercambio/traduccion-intercambio.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codIdiomas,
  codigoEntidades,
  codigosCatalogoAcciones,
  codigosEstadosTraduccionIntercambio,
  idiomas,
} from 'src/shared/enum-sistema';

const mongoose = require('mongoose');

@Injectable()
export class TraducirIntercambioServiceSegundoPlano {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    @Inject('TRADUCCION_INTERCAMBIO_MODEL')
    private readonly traduccionIntercambioModel: Model<TraduccionIntercambio>,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async traducirIntercambioSegundoPlano(idIntercambio, perfil, idioma) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la traduccion en el idioma original
      const getTraductorOriginal = await this.traduccionIntercambioModel
        .findOne({
          intercambio: idIntercambio,
          original: true,
          estado: codigosEstadosTraduccionIntercambio.activa,
        })
        .session(opts.session);

      console.log('getTraductorOriginal: ', getTraductorOriginal);
      if (getTraductorOriginal) {
        if (idioma != idiomas.ingles) {
          await this.traducirIntercambioIngles(
            idIntercambio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.espanol) {
          await this.traducirIntercambioEspaniol(
            idIntercambio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.italiano) {
          await this.traducirIntercambioItaliano(
            idIntercambio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.frances) {
          await this.traducirIntercambioFrances(
            idIntercambio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.aleman) {
          await this.traducirIntercambioAleman(
            idIntercambio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.portugues) {
          await this.traducirIntercambioPortugues(
            idIntercambio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return true;
      }

      //Finaliza la transaccion
      await session.endSession();
      return true;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  //TRADUCCION DE PROYECTO A ESPAÑOL
  async traducirIntercambioEspaniol(
    idIntercambio,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.espanol,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.espanol,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let descripcion;
      if (getTraductorOriginal.descripcion) {
        let textoTrducidoDescripcion = await traducirTexto(
          idiomas.espanol,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTrducidoDescripcion.textoTraducido;
      }

      // let textoUnido = getTraductorOriginal.titulo + ' [-]' + getTraductorOriginal.tituloCorto +
      //     ' [-]' + getTraductorOriginal.descripcion;

      // let textoTrducido = await traducirTexto(idiomas.ingles, textoUnido);

      // const textS = textoTrducido.textoTraducido.split('[-]');

      // const T = this.gestionProyectoService.detectarIdioma(idiomas.ingles);
      // const titu = textS[0].trim().split(' ');
      // const tagsTitulo = sw.removeStopwords(titu, T);

      // const tituCort = textS[1].trim().split(' ');

      // const tagsTituloCort = sw.removeStopwords(tituCort, T);

      // const tagsUnidos = tagsTitulo.concat(tagsTituloCort);

      // const tags = tagsUnidos.filter(function (item, index, array) {
      //     return array.indexOf(item) === index;
      // })

      //datos para guardar la nueva traduccion

      let newTraduccionIntercambio = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.espanol,
        original: false,
        estado: codigosEstadosTraduccionIntercambio.activa,
        intercambio: idIntercambio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionIntercambioModel(
        newTraduccionIntercambio,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionIntercambio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.intercambioModel.updateOne(
        { _id: idIntercambio },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE PROYECTO A ITALIANO
  async traducirIntercambioItaliano(
    idIntercambio,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.italiano,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.italiano,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let descripcion;
      if (getTraductorOriginal.descripcion) {
        let textoTrducidoDescripcion = await traducirTexto(
          idiomas.italiano,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTrducidoDescripcion.textoTraducido;
      }

      //datos para guardar la nueva traduccion

      let newTraduccionIntercambio = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.italiano,
        original: false,
        estado: codigosEstadosTraduccionIntercambio.activa,
        intercambio: idIntercambio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionIntercambioModel(
        newTraduccionIntercambio,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del intercambio
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionIntercambio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.intercambioModel.updateOne(
        { _id: idIntercambio },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE INTERCAMBIO A FRANCES
  async traducirIntercambioFrances(
    idIntercambio,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.frances,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.frances,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let descripcion;
      if (getTraductorOriginal.descripcion) {
        let textoTrducidoDescripcion = await traducirTexto(
          idiomas.frances,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTrducidoDescripcion.textoTraducido;
      }

      //datos para guardar la nueva traduccion

      let newTraduccionProyecto = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.frances,
        original: false,
        estado: codigosEstadosTraduccionIntercambio.activa,
        intercambio: idIntercambio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionIntercambioModel(
        newTraduccionProyecto,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionIntercambio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.intercambioModel.updateOne(
        { _id: idIntercambio },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE INTERCAMBIO A ALEMAN
  async traducirIntercambioAleman(
    idIntercambio,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.aleman,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.aleman,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let descripcion;
      if (getTraductorOriginal.descripcion) {
        let textoTrducidoDescripcion = await traducirTexto(
          idiomas.aleman,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTrducidoDescripcion.textoTraducido;
      }

      //datos para guardar la nueva traduccion

      let newTraduccionIntercambio = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.aleman,
        original: false,
        estado: codigosEstadosTraduccionIntercambio.activa,
        intercambio: idIntercambio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionIntercambioModel(
        newTraduccionIntercambio,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del intercambio
      let newHistoricoTraduccionIntercambio: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionIntercambio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionIntercambio,
      );

      //Actualiza el array de id de traducciones
      await this.intercambioModel.updateOne(
        { _id: idIntercambio },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE INTERCAMBIO A PORTUGUES
  async traducirIntercambioPortugues(
    idIntercambio,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.portugues,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.portugues,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let descripcion;
      if (getTraductorOriginal.descripcion) {
        let textoTrducidoDescripcion = await traducirTexto(
          idiomas.portugues,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTrducidoDescripcion.textoTraducido;
      }

      //datos para guardar la nueva traduccion

      let newTraduccionIntercambio = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.portugues,
        original: false,
        estado: codigosEstadosTraduccionIntercambio.activa,
        intercambio: idIntercambio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionIntercambioModel(
        newTraduccionIntercambio,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del intercambio
      let newHistoricoTraduccionIntercambio: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionIntercambio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionIntercambio,
      );

      //Actualiza el array de id de traducciones
      await this.intercambioModel.updateOne(
        { _id: idIntercambio },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE INTERCAMBIO A INGLES
  async traducirIntercambioIngles(
    idIntercambio,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.ingles,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.ingles,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let descripcion;
      if (getTraductorOriginal.descripcion) {
        let textoTrducidoDescripcion = await traducirTexto(
          idiomas.ingles,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTrducidoDescripcion.textoTraducido;
      }

      //datos para guardar la nueva traduccion

      let newTraduccionIntercambio = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.ingles,
        original: false,
        estado: codigosEstadosTraduccionIntercambio.activa,
        intercambio: idIntercambio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionIntercambioModel(
        newTraduccionIntercambio,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del intercambio
      let newHistoricoTraduccionIntercambio: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionIntercambio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionIntercambio,
      );

      //Actualiza el array de id de traducciones
      await this.intercambioModel.updateOne(
        { _id: idIntercambio },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
