import { Connection } from 'mongoose';
import { CatalogoTipoIntercambioModelo } from 'src/drivers/mongoose/modelos/catalogo_tipo_intercambio/catalogo-tipo-intercambio.schema';
import { ComentarioIntercambioModelo } from 'src/drivers/mongoose/modelos/comentarioIntercambio/comentarioIntercambio.schema';
import { ConfiguracionEstiloModelo } from 'src/drivers/mongoose/modelos/configuracion_estilo/configuracion-estilo.schema';
import { EventoModelo } from 'src/drivers/mongoose/modelos/evento/evento.schema';
import { HistoricoModelo } from 'src/drivers/mongoose/modelos/historico/historico.schema';
import { IntercambioModelo } from 'src/drivers/mongoose/modelos/intercambio/intercambio.schema';
import { perfilModelo } from 'src/drivers/mongoose/modelos/perfil/perfil.schema';
import { TraduccionCatalogoTipoIntercambioModelo } from 'src/drivers/mongoose/modelos/traduccion_catalogo_tipo_intercambio/traduccion-catalogo-tipo-intercambio.schema';
import { TraduccionIntercambioModelo } from 'src/drivers/mongoose/modelos/traduccion_intercambio/traduccion-intercambio.schema';

export const intercambioProviders = [
  {
    provide: 'INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('intercambio', IntercambioModelo, 'intercambio'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_intercambio',
        TraduccionIntercambioModelo,
        'traduccion_intercambio',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_TIPO_INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_tipo_intercambio',
        CatalogoTipoIntercambioModelo,
        'catalogo_tipo_intercambio',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_CATALOGO_TIPO_INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_catalogo_tipo_intercambio',
        TraduccionCatalogoTipoIntercambioModelo,
        'traduccion_catalogo_tipo_intercambio',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'HISTORICO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('historico', HistoricoModelo, 'historico'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'EVENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('evento', EventoModelo, 'evento'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_ESTILO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_estilo',
        ConfiguracionEstiloModelo,
        'configuracion_estilo',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'COMENTARIOS_INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'comentarios_intercambio',
        ComentarioIntercambioModelo,
        'comentarios_intercambio',
      ),
    inject: ['DB_CONNECTION'],
  },
];
