import { Module } from '@nestjs/common';
import { IntercambioServicesModule } from './casos_de_uso/intercambio-services.module';
import { IntercambioControllerModule } from './controladores/intercambio-controller.module';

@Module({
  imports: [IntercambioControllerModule],
  providers: [IntercambioServicesModule],
  controllers: [],
})
export class IntercambioModule {}
