import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { LogSistemaServicesModule } from '../casos_de_uso/log-sistema-services.module';
import { TraduccionEstaticaController } from './../../../multiIdioma/controladores/traduccion-estatica-controller';

@Module({
  imports: [LogSistemaServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [],
})
export class LogsistemaControllerModule {}
