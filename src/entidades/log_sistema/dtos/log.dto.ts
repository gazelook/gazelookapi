export class LogSistemaDto {
  method: string;
  date: Date;
  status: number;
  url: string;
  ip: string;
  browser: string;
  content: any;
  timeRequest: number;
}
