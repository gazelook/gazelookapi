import { Connection } from 'mongoose';
import { LogSistema } from '../../../drivers/mongoose/modelos/log_sistema/log_sistema.schema';

export const logSistemaProviders = [
  {
    provide: 'LOG_SISTEMA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('log_sistema', LogSistema, 'log_sistema'),
    inject: ['DB_CONNECTION'],
  },
];
