import { Module } from '@nestjs/common';
import { LogsistemaControllerModule } from './controladores/log-sistema-controller.module';

@Module({
  imports: [LogsistemaControllerModule],
  providers: [],
})
export class LogSistemaModule {}
