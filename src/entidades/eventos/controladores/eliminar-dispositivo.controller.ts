import {
  Controller,
  Delete,
  Headers,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { Funcion } from 'src/shared/funcion';
import { EjecutarCronServiceEliminarDispositivo } from '../casos_de_uso/ejecutar-cron-eliminar-dispositivo.service';
// import {CrearSuscripcionAdaptador}from '../adaptador/v1/adapterCrearSuscripcion'

// @ApiTags('Suscripciones')
// @ApiTags('EJECUTAR-CRON')
@Controller('api/eliminar-dispositivos')
export class CronEliminarDispositivoControlador {
  constructor(
    private readonly funcion: Funcion,
    private ejecutarCronServiceEliminarDispositivo: EjecutarCronServiceEliminarDispositivo,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Delete('/')
  @ApiOperation({
    summary: 'Este método elimina dispositivos cuando si el token ya expirado',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiResponse({ status: 201, description: 'Creado' })
  @ApiResponse({ status: 404, description: 'Datos inválidos' })
  @UseGuards(AuthGuard('jwt'))
  public async eliminarDispositivo(@Headers() headers) {
    // const respuesta = new RespuestaInterface();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const eliminarDispositivos = await this.ejecutarCronServiceEliminarDispositivo.ejecutarCronEliminarDispositivo();

      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: eliminarDispositivos,
      });
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
