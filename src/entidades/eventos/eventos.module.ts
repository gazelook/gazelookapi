import { ScheduleModule } from '@nestjs/schedule';
import { Module } from '@nestjs/common';
import { EventosControllerModule } from './controladores/eventos-controller.module';

@Module({
  imports: [EventosControllerModule, ScheduleModule.forRoot()],
  providers: [],
  controllers: [],
})
export class EventosModule {}
