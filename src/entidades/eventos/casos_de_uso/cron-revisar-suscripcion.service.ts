import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { ActualizarSuscripcionService } from '../../suscripcion/casos_de_uso/actualizar-suscripcion.service';
import { ObtenerSuscripcionService } from '../../suscripcion/casos_de_uso/obtener-suscripcion.service';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import {
  codigosEstadoSuscripcion,
  estadosUsuario,
} from './../../../shared/enum-sistema';

@Injectable()
export class CronRevisarSuscripcionService {
  intervalo;
  configEvento;

  constructor(
    private obtenerSuscripcionService: ObtenerSuscripcionService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private actualizarSuscripcionService: ActualizarSuscripcionService,
  ) {}

  //@Cron(`30000 * * * * *`)
  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  async cronBloquearCuentaResponsable() {
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      const suscripciones = await this.obtenerSuscripcionService.getSucripcionesFechaVencida();

      if (suscripciones.length > 0) {
        for (const suscripcion of suscripciones) {
          const formatFechaActual = moment().format('YYYY-MM-DD HH:mm:ss');
          const fechaActual = new Date(formatFechaActual);

          if (
            moment(fechaActual) > moment(suscripcion.fechaFinalizacion) &&
            suscripcion?.referencia
          ) {
            // actualizar estado suscripcion
            await this.actualizarSuscripcionService.actualizarSuscripcionEstado(
              suscripcion._id,
              codigosEstadoSuscripcion.finalizada,
              opts,
            );
          } else {
            // si no tiene mas suscripciones
            // actualizar estado usuario
            await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
              suscripcion.usuario.toString(),
              estadosUsuario.inactivaPago,
              opts,
            );
            // actualizar estado suscripcion
            await this.actualizarSuscripcionService.actualizarSuscripcionEstado(
              suscripcion._id,
              codigosEstadoSuscripcion.finalizada,
              opts,
            );
          }
        }
      }

      await session.commitTransaction();
      await session.endSession();
    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      throw error;
    }
  }
}
