import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { EliminarUsuarioSinPagoService } from 'src/entidades/usuario/casos_de_uso/eliminar-usuario-sin-pago.service';
import { ObtenerUsuariosSinPagoervice } from 'src/entidades/usuario/casos_de_uso/obtener-usuarios-sin-pago.service';

@Injectable()
export class EjecutarCronServiceEliminarUsuarioSinPago {
  constructor(
    private obtenerUsuariosSinPagoervice: ObtenerUsuariosSinPagoervice,
    private eliminarUsuarioSinPagoService: EliminarUsuarioSinPagoService,
  ) {}

  // Se ejecuta cada 1er dia del mes a media noche
  @Cron(CronExpression.EVERY_1ST_DAY_OF_MONTH_AT_MIDNIGHT)
  //Se crea el evento de gastos operativos
  async ejecutarCronEliminarUsuarioSinPago() {
    try {
      //Obtiene todos los usarios sin pago que no tiene ningun rol
      const usuario = await this.obtenerUsuariosSinPagoervice.obtenerUsuariosSinPago();

      if (usuario.length > 0) {
        //Recorre todos los usuarios
        for (const getUsuario of usuario) {
          //Elimina los usuarios
          await this.eliminarUsuarioSinPagoService.eliminarCuentaUsuario(
            getUsuario._id,
          );
        }
        return true;
      }
      return false;
    } catch (error) {
      throw error;
    }
  }
}
