import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression, SchedulerRegistry } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { ConfiguracionEvento } from 'src/drivers/mongoose/interfaces/configuracion_evento/configuracion-evento.interface';
import { ActualizarEmailResponsableService } from '../../emails/casos_de_uso/actualizar-email-responsable.service';
import { ObtenerEmailService } from '../../emails/casos_de_uso/obtener-email.service';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import {
  codigosConfiguracionEvento,
  estadosUsuario,
} from './../../../shared/enum-sistema';

@Injectable()
export class CronBloquearCuentaResponsableService {
  intervalo;
  configEvento;

  constructor(
    @Inject('CONFIGURACION_EVENTO_MODEL')
    private readonly configEventoModel: Model<ConfiguracionEvento>,
    private obtenerEmailService: ObtenerEmailService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private actualizarEmailResponsableService: ActualizarEmailResponsableService,
    private schedulerRegistry: SchedulerRegistry,
  ) {
    this.configEvento = this.configEventoModel.findOne({
      codigo: codigosConfiguracionEvento.eventoBloquearResponsableEmail,
    });
  }

  //@Cron(CronExpression.EVERY_12_HOURS)
  //@Cron(`${intervaloEventos.intervalo} * * * *`)
  // 86400000 ms = 24H
  // 172800000 ms = 48H
  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  async cronBloquearCuentaResponsable(intervalo) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const configEvento = await this.configEventoModel.findOne({
        codigo: codigosConfiguracionEvento.eventoBloquearResponsableEmail,
      });

      //const intervalo = configEvento.intervalo / (60 * 60 * 24 * 1000); //  30 dias finaliza el evento
      //this.addCronJob('BloquearCuentaResponsable', intervalo);

      //Obtiene fecha actual
      const fechaActual = moment().format('YYYY-MM-DD');
      let formatFechaActual = new Date(fechaActual);

      const emails = await this.obtenerEmailService.obtenerEmailsResponsable();
      let i = 0;
      for (const email of emails) {
        if (moment(formatFechaActual) > moment(email.fechaValidacion)) {
          console.log('bloqueado:', email);
          await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
            email.usuario.toString(),
            estadosUsuario.bloqueadoSistema,
            opts,
          );
          await this.actualizarEmailResponsableService.bloquearEmailResponsable(
            email._id,
            opts,
          );
        }
      }
      //Crear evento
      /* const evento = {
        estado: estadoEvento.pendiente,
        fechaInicio: configEvento.fechaInicio,
        fechaFin: null,
        configuracionEvento: configEvento.codigo,
      };
      await new this.eventoModel(evento).save(opts); */

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  addCronJob(name: string, seconds: string) {
    const job = Cron(`${seconds} * * * * *`);

    this.schedulerRegistry.addCronJob(name, job);
    console.warn(`Intervalo ${name} agregado cada ${seconds} segundos!`);
  }
}
