import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { FirebaseModule } from 'src/drivers/firebase/firebase.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { CuentaServiceModule } from 'src/entidades/cuenta/casos_de_uso/cuenta.services.module';
import { DispositivoServicesModule } from 'src/entidades/dispositivos/casos_de_uso/dispositivo-services.module';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { ProyectosServicesModule } from 'src/entidades/proyectos/casos_de_uso/proyectos-services.module';
import { EmailServicesModule } from '../../emails/casos_de_uso/email-services.module';
import { SuscripcionServicesModule } from '../../suscripcion/casos_de_uso/suscripcion-services.module';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { eventosProviders } from '../drivers/eventos.provider';
import { CronBloquearCuentaResponsableService } from './cron-bloquear-cuenta-responsable.service';
import { CronInicioForoProyectosService } from './cron-eventos-foro.service';
import { CronMesnajeTransferenciaProyectoService } from './cron-mensajes-transferencia-proyecto.service';
import { CronRevisarSuscripcionService } from './cron-revisar-suscripcion.service';
import { EjecutarCronServiceEliminarDispositivo } from './ejecutar-cron-eliminar-dispositivo.service';
import { EjecutarCronServiceEliminarUsuarioSinPago } from './ejecutar-cron-eliminar-usuario-sin-pago.service';
import { EjecutarCronService } from './ejecutar-cron-eventos.service';
import { EjecutarCronVerificarUsuarioCriptoService } from './ejecutar-cron-verificar-usuario-cripto.service';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    PerfilServiceModule,
    forwardRef(() => ProyectosServicesModule),
    UsuarioServicesModule,
    EmailServicesModule,
    SuscripcionServicesModule,
    forwardRef(() => DispositivoServicesModule),
    FirebaseModule,
    CuentaServiceModule
  ],
  providers: [
    ...eventosProviders,
    CronInicioForoProyectosService,
    EjecutarCronService,
    CronBloquearCuentaResponsableService,
    CronRevisarSuscripcionService,
    EjecutarCronServiceEliminarDispositivo,
    CronMesnajeTransferenciaProyectoService,
    EjecutarCronServiceEliminarUsuarioSinPago,
    EjecutarCronVerificarUsuarioCriptoService
  ],
  exports: [
    ...eventosProviders,
    CronInicioForoProyectosService,
    CronBloquearCuentaResponsableService,
    CronRevisarSuscripcionService,
    EjecutarCronServiceEliminarDispositivo,
    CronMesnajeTransferenciaProyectoService,
    EjecutarCronServiceEliminarUsuarioSinPago,
    EjecutarCronVerificarUsuarioCriptoService
  ],
  controllers: [],
})
export class EventosServicesModule {}
