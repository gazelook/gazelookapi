import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { EliminarDispositivoService } from 'src/entidades/dispositivos/casos_de_uso/eliminar-dispositivo.service';
import { ObtenerDispositivoUsuarioService } from 'src/entidades/dispositivos/casos_de_uso/obtener-dispositivo-usuario.service';
import { CambiarEstadoProyectoService } from 'src/entidades/proyectos/casos_de_uso/cambiar-estado-proyecto.service';
import { Evento } from './../../../drivers/mongoose/interfaces/evento/evento.interface';
import {
  codigosConfiguracionEvento,
  estadoEvento,
  expiracionTokenDispositivo,
  nombrecatalogoEstados,
} from './../../../shared/enum-sistema';

@Injectable()
export class EjecutarCronServiceEliminarDispositivo {
  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
    private cambiarEstadoProyectoService: CambiarEstadoProyectoService,
    private obtenerDispositivoUsuarioService: ObtenerDispositivoUsuarioService,
    private eliminarDispositivoService: EliminarDispositivoService,
  ) {}

  // Se ejecuta cada semana
  @Cron(CronExpression.EVERY_WEEKEND)
  //Se crea el evento de gastos operativos
  async ejecutarCronEliminarDispositivo() {
    try {
      //Obtiene todos los dispositivos activos
      const dispositivo = await this.obtenerDispositivoUsuarioService.getDispositivos();

      if (dispositivo.length > 0) {
        //Recorre todos los dispositivos
        for (const getDispositivo of dispositivo) {
          if (getDispositivo.tokenUsuario) {
            if (getDispositivo.tokenUsuario['ultimoAcceso']) {
              const fechaCaducidadDispositivo =
                getDispositivo.tokenUsuario['ultimoAcceso'];
              console.log(
                '************************************************************',
              );
              console.log('getDispositivo: ', getDispositivo._id);
              console.log('TOKEN: ', getDispositivo.tokenUsuario['_id']);
              console.log(
                'fechaCaducidadDispositivo: ',
                fechaCaducidadDispositivo,
              );
              //Obtener fecha de caducidad
              let fechaCaducidad = new Date(fechaCaducidadDispositivo);
              //Le sumo 3 dias que es el tiempo que dura el token
              fechaCaducidad.setDate(
                new Date(fechaCaducidadDispositivo).getDate() +
                  expiracionTokenDispositivo.dias,
              );

              //Obtiene la fecha actual
              let fechaActual = new Date();
              let fecha =
                fechaActual.getFullYear() +
                '-' +
                (fechaActual.getMonth() + 1) +
                '-' +
                fechaActual.getDate();
              let hora =
                fechaActual.getHours() +
                ':' +
                fechaActual.getMinutes() +
                ':' +
                fechaActual.getSeconds();
              let formatFechaAct = fecha + ' ' + hora;
              let formatFechaActual = new Date(formatFechaAct);

              console.log('fechaActual: ', formatFechaActual);
              console.log('fechaCaducidad: ', fechaCaducidad);

              //Si la fecha actual es mayor a la fecha de caducidad elimiina el dispositivo
              if (formatFechaActual > fechaCaducidad) {
                console.log('DISPOSITIVO A ELIMINAR:', getDispositivo._id);
                const eliminaDispositivo = await this.eliminarDispositivoService.eliminarDispositivo(
                  getDispositivo._id,
                  null,
                );
              }
            }
          }
        }
        return true;
      }
      return false;
    } catch (error) {
      throw error;
    }
  }

}
