import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { CambiarEstadoProyectoService } from 'src/entidades/proyectos/casos_de_uso/cambiar-estado-proyecto.service';
import { Evento } from './../../../drivers/mongoose/interfaces/evento/evento.interface';
import {
  codigosConfiguracionEvento,
  estadoEvento,
  nombrecatalogoEstados,
} from './../../../shared/enum-sistema';

@Injectable()
export class EjecutarCronService {
  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
    private cambiarEstadoProyectoService: CambiarEstadoProyectoService,
  ) { }

  // Se ejecuta cada 24 horas todos los dias a las 2am
  // @Cron(CronExpression.EVERY_DAY_AT_2AM)
  //Se crea el evento de gastos operativos
  async ejecutarCronRangoForoProyectos() {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };
      let localTime = moment().format('YYYY-MM-DD'); // store localTime
      let fechaFormat = localTime + 'T00:00:00.000Z';
      let formatFecha = new Date(fechaFormat);
      formatFecha.setDate(new Date(fechaFormat).getDate());

      const getEvento = await this.eventoModel.findOne({
        estado: estadoEvento.pendiente,
        configuracionEvento:
          codigosConfiguracionEvento.eventoForoProyectosCONFEVT_3,
        fechaInicio: formatFecha,
      });

      if (getEvento) {
        // actualizar evento como ejecutado
        await this.eventoModel.updateOne(
          { _id: getEvento._id },
          { estado: estadoEvento.ejecutado, fechaActualizacion: new Date() },
          opts,
        );
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  // Se ejecuta cada 24 horas
  // @Cron(CronExpression.EVERY_DAY_AT_1AM)
  // @Cron(CronExpression.EVERY_30_SECONDS)
  //Se crea el evento de gastos operativos
  async ejecutarCronInicioForoProyectos() {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };
      let localTime = moment().format('YYYY-MM-DD'); // store localTime
      let fechaFormat = localTime + 'T00:00:00.000Z';
      let formatFecha = new Date(fechaFormat);
      formatFecha.setDate(new Date(fechaFormat).getDate());
      const getEvento = await this.eventoModel.findOne({
        estado: estadoEvento.pendiente,
        configuracionEvento:
          codigosConfiguracionEvento.eventoForoProyectosCONFEVT_4,
        fechaInicio: formatFecha,
      });

      if (getEvento) {
        // actualizar evento como ejecutado
        await this.eventoModel.updateOne(
          { _id: getEvento._id },
          { fechaActualizacion: new Date() },
          opts,
        );
        await this.cambiarEstadoProyectoService.cambiarEstadoProyectos(
          nombrecatalogoEstados.foro,
          opts,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
