import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { FirebaseMensajesService } from 'src/drivers/firebase/services/firebase-mensajes.service';

@Injectable()
export class CronMesnajeTransferenciaProyectoService {
  constructor(private firebaseMensajesService: FirebaseMensajesService) {}

  // Se ejecuta cada 24 horas
  @Cron(CronExpression.EVERY_DAY_AT_3AM)
  //Se crea el evento de gastos operativos
  async cronMensajeTransferenciaProyecto() {
    try {
      let formatFecha = new Date();
      // formatFecha.setDate(new Date(fechaFormat).getDate());

      await this.firebaseMensajesService.obtenerMensajeTipo(formatFecha);
    } catch (error) {
      throw error;
    }
  }
}
