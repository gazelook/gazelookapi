import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { ConfiguracionEvento } from 'src/drivers/mongoose/interfaces/configuracion_evento/configuracion-evento.interface';
import { ConfiguracionForo } from 'src/drivers/mongoose/interfaces/configuracion_foro/configuracion-foro.interface';
import { ConfiguracionPreestrategia } from 'src/drivers/mongoose/interfaces/configuracion_preestrategia/configuracion-preestrategia.interface';
import { FormulaEventoService } from 'src/entidades/catalogos/casos_de_uso/formula_evento.service';
import { Evento } from '../../../drivers/mongoose/interfaces/evento/evento.interface';
import {
  codigosConfiguracionEvento,
  codigosConfiguracionForo,
  codigosConfiguracionPreestrategia,
  codigosFormulasEvento,
  estadoEvento,
  estadosConfiguracionForo,
  estadosConfiguracionPreestrategia,
} from '../../../shared/enum-sistema';

@Injectable()
export class CronInicioForoProyectosService {
  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
    @Inject('CONFIGURACION_EVENTO_MODEL')
    private readonly configEventoModel: Model<ConfiguracionEvento>,
    @Inject('CONFIGURACION_PREESTRATEGIA_MODEL')
    private readonly configPreestrategiaModel: Model<ConfiguracionPreestrategia>,
    @Inject('CONFIGURACION_FORO_MODEL')
    private readonly configForoModel: Model<ConfiguracionForo>,
    private readonly formulaEventoService: FormulaEventoService
  ) { }

  // Se ejecuta cada dia a las 5 de la mañana
  // @Cron(CronExpression.EVERY_MINUTE)

  //Se crea el evento FORO PROYECTOS CADA CUANTO SE EJECUTA EL FORO (cada 5 meses)
  async cronInicioForoProyectos() {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const configEvento = await this.configEventoModel.findOne({
        codigo: codigosConfiguracionEvento.eventoForoProyectosCONFEVT_3,
      });

      let codigoFormulaEvento
      if (configEvento) {
        codigoFormulaEvento = configEvento.formulas[0]
      }

      const configPreestrategia = await this.configPreestrategiaModel.find({}).sort({ orden: 1 });
      
      let fechaFin: any;
      let fechaProxima: any;
      let fechaIniForo: any;
      let fechaInicio: any;

      // si es la primera vez que se crea el evento
      if (!configEvento.fechaProximo && configEvento.ciclico) {
        let semanasPreestrategia
        if (configPreestrategia.length > 0) {
          for (const preestrategia of configPreestrategia) {
            if (preestrategia.orden === 1 && preestrategia.estado === estadosConfiguracionPreestrategia.activa) {
              semanasPreestrategia = preestrategia.semanas
              // actualizar estado de la configuracion CONFPRE_1
              await this.configPreestrategiaModel.updateOne(
                { codigo: codigosConfiguracionPreestrategia.CONFPRE_1 },
                { estado: estadosConfiguracionPreestrategia.inactivo, fechaActualizacion: new Date() },
                opts,
              );
              // actualizar estado de la configuracion CONFPRE_2
              await this.configPreestrategiaModel.updateOne(
                { codigo: codigosConfiguracionPreestrategia.CONFPRE_2 },
                { estado: estadosConfiguracionPreestrategia.activa, fechaActualizacion: new Date() },
                opts,
              );
            }

            if (preestrategia.orden === 2 && preestrategia.estado === estadosConfiguracionPreestrategia.activa) {
              semanasPreestrategia = preestrategia.semanas
              // actualizar estado de la configuracion CONFPRE_2
              await this.configPreestrategiaModel.updateOne(
                { codigo: codigosConfiguracionPreestrategia.CONFPRE_2 },
                { estado: estadosConfiguracionPreestrategia.inactivo, fechaActualizacion: new Date() },
                opts,
              );
              // actualizar estado de la configuracion CONFPRE_3
              await this.configPreestrategiaModel.updateOne(
                { codigo: codigosConfiguracionPreestrategia.CONFPRE_3 },
                { estado: estadosConfiguracionPreestrategia.activa, fechaActualizacion: new Date() },
                opts,
              );
            }


            if (preestrategia.orden === 3 && preestrategia.estado === estadosConfiguracionPreestrategia.activa) {
              semanasPreestrategia = preestrategia.semanas
              // actualizar estado de la configuracion CONFPRE_3
              await this.configPreestrategiaModel.updateOne(
                { codigo: codigosConfiguracionPreestrategia.CONFPRE_3 },
                { estado: estadosConfiguracionPreestrategia.inactivo, fechaActualizacion: new Date() },
                opts,
              );
              // actualizar estado de la configuracion CONFPRE_1
              await this.configPreestrategiaModel.updateOne(
                { codigo: codigosConfiguracionPreestrategia.CONFPRE_1 },
                { estado: estadosConfiguracionPreestrategia.activa, fechaActualizacion: new Date() },
                opts,
              );
            }

          }
        }

        //valor obtenido de la FORMULA SEMANAS X DIAS
        const getForm = await this.formulaEventoService.obtenerFormulaEvento(
          codigosFormulasEvento.semanasxdias,
        );
        //Remmplaza las SEMANAS
        let formulaSemanasxDias = getForm.replace(
          'SEMANAS',
          semanasPreestrategia,
        );
        //Numero de dias
        const getSemanasxDias = eval(formulaSemanasxDias);


        // const intervalo = configEvento.intervalo / (60 * 60 * 24 * 1000); // cada 150 dias (5 meses) se creara un nuevo evento de conteo de foro
        const intervalo = getSemanasxDias


        fechaProxima = new Date(configEvento.fechaInicio);
        fechaFin = new Date(configEvento.fechaInicio);
        fechaProxima.setDate(fechaProxima.getDate() + intervalo);
        fechaFin.setDate(configEvento.fechaInicio.getDate() + intervalo);

        //Crear evento para el foro (cada 4 meses se inicia)
        const evento = {
          estado: estadoEvento.pendiente,
          fechaInicio: configEvento.fechaInicio,
          fechaFin: fechaFin,
          configuracionEvento: configEvento.codigo,
          proyectos: [],
        };
        await new this.eventoModel(evento).save(opts);

        // actualizar fechaProximo
        await this.configEventoModel.updateOne(
          { codigo: codigosConfiguracionEvento.eventoForoProyectosCONFEVT_3 },
          { fechaProximo: fechaProxima },
          opts,
        );

        //Se crea el evento de inicio de foro dura un mes(30 dias)
        const configEventoForo = await this.configEventoModel.findOne({
          codigo: codigosConfiguracionEvento.eventoForoProyectosCONFEVT_4,
        });
        const configForo = await this.configForoModel.find({}).sort({ orden: 1 });
        let semanasForo
        if (configForo.length > 0) {
          for (const foro of configForo) {
            if (foro.orden === 1 && foro.estado === estadosConfiguracionForo.activa) {
              semanasForo = foro.semanas
              // actualizar estado de la configuracion CONFORO_1
              await this.configForoModel.updateOne(
                { codigo: codigosConfiguracionForo.CONFORO_1 },
                { estado: estadosConfiguracionForo.inactivo, fechaActualizacion: new Date() },
                opts,
              );
              // actualizar estado de la configuracion CONFORO_2
              await this.configForoModel.updateOne(
                { codigo: codigosConfiguracionForo.CONFORO_2 },
                { estado: estadosConfiguracionForo.activa, fechaActualizacion: new Date() },
                opts,
              );
            }

            if (foro.orden === 2 && foro.estado === estadosConfiguracionForo.activa) {
              semanasForo = foro.semanas
              // actualizar estado de la configuracion CONFORO_2
              await this.configForoModel.updateOne(
                { codigo: codigosConfiguracionForo.CONFORO_2 },
                { estado: estadosConfiguracionForo.inactivo, fechaActualizacion: new Date() },
                opts,
              );
              // actualizar estado de la configuracion CONFORO_3
              await this.configForoModel.updateOne(
                { codigo: codigosConfiguracionForo.CONFORO_3 },
                { estado: estadosConfiguracionForo.activa, fechaActualizacion: new Date() },
                opts,
              );
            }


            if (foro.orden === 3 && foro.estado === estadosConfiguracionForo.activa) {
              semanasForo = foro.semanas
              // actualizar estado de la configuracion CONFPRE_3
              await this.configForoModel.updateOne(
                { codigo: codigosConfiguracionForo.CONFORO_3 },
                { estado: estadosConfiguracionForo.inactivo, fechaActualizacion: new Date() },
                opts,
              );
              // actualizar estado de la configuracion CONFORO_1
              await this.configForoModel.updateOne(
                { codigo: codigosConfiguracionForo.CONFORO_1 },
                { estado: estadosConfiguracionForo.activa, fechaActualizacion: new Date() },
                opts,
              );
            }

          }
        }

        //valor obtenido de la FORMULA SEMANAS X DIAS
        const getFormForo = await this.formulaEventoService.obtenerFormulaEvento(
          codigosFormulasEvento.semanasxdias,
        );
        //Remmplaza las SEMANAS
        let formulaSemanasxDiasForo = getFormForo.replace(
          'SEMANAS',
          semanasForo,
        );
        //Numero de dias
        const getSemanasxDiasForo = eval(formulaSemanasxDiasForo);


        // const duracionForo = configEventoForo.duracion / (60 * 60 * 24 * 1000); //  30 dias finaliza el evento
        const duracionForo = getSemanasxDiasForo
        fechaIniForo = new Date(configEvento.fechaInicio)
        fechaIniForo.setDate(fechaIniForo.getDate() + duracionForo)
        const eventoIniForo = {
          estado: estadoEvento.pendiente,
          fechaInicio: fechaIniForo,
          fechaFin: fechaFin,
          configuracionEvento: configEventoForo.codigo,
          proyectos: [],
        };
        await new this.eventoModel(eventoIniForo).save(opts);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }

      // si existe un fecha proxima del evento, comprueba si le toca actulizar el evento para la proxima fecha
      if (configEvento.fechaProximo) {

        let fechaSiguiente = moment(configEvento.fechaProximo).format(
          'YYYY-MM-DD',
        );
        //Obtiene la fecha proxima menos cinco (5) dias para que se cree un nuevo evento
        //Los 5 dias es para anticipar la creacion del evento
        let formatFechaSiguiente = new Date(fechaSiguiente);
        formatFechaSiguiente.setDate(new Date(fechaSiguiente).getDate() - 5);

        //Obtiene la fecha actual
        let fechaActual = moment().format('YYYY-MM-DD');
        let formatFechaActual = new Date(fechaActual);
        console.log('formatFechaActual: ', formatFechaActual)
        console.log('formatFechaSiguiente: ', formatFechaSiguiente)
        // console.log('formatFechaActual: ', formatFechaActual.getTime())
        // console.log('formatFechaSiguiente: ', formatFechaSiguiente.getTime())
        if (formatFechaActual.getTime() === formatFechaSiguiente.getTime()) {

          let semanasPreestrategia
          if (configPreestrategia.length > 0) {
            for (const preestrategia of configPreestrategia) {
              if (preestrategia.orden === 1 && preestrategia.estado === estadosConfiguracionPreestrategia.activa) {
                semanasPreestrategia = preestrategia.semanas
                // actualizar estado de la configuracion CONFPRE_1
                await this.configPreestrategiaModel.updateOne(
                  { codigo: codigosConfiguracionPreestrategia.CONFPRE_1 },
                  { estado: estadosConfiguracionPreestrategia.inactivo, fechaActualizacion: new Date() },
                  opts,
                );
                // actualizar estado de la configuracion CONFPRE_2
                await this.configPreestrategiaModel.updateOne(
                  { codigo: codigosConfiguracionPreestrategia.CONFPRE_2 },
                  { estado: estadosConfiguracionPreestrategia.activa, fechaActualizacion: new Date() },
                  opts,
                );
              }

              if (preestrategia.orden === 2 && preestrategia.estado === estadosConfiguracionPreestrategia.activa) {
                semanasPreestrategia = preestrategia.semanas
                // actualizar estado de la configuracion CONFPRE_2
                await this.configPreestrategiaModel.updateOne(
                  { codigo: codigosConfiguracionPreestrategia.CONFPRE_2 },
                  { estado: estadosConfiguracionPreestrategia.inactivo, fechaActualizacion: new Date() },
                  opts,
                );
                // actualizar estado de la configuracion CONFPRE_3
                await this.configPreestrategiaModel.updateOne(
                  { codigo: codigosConfiguracionPreestrategia.CONFPRE_3 },
                  { estado: estadosConfiguracionPreestrategia.activa, fechaActualizacion: new Date() },
                  opts,
                );
              }


              if (preestrategia.orden === 3 && preestrategia.estado === estadosConfiguracionPreestrategia.activa) {
                semanasPreestrategia = preestrategia.semanas
                // actualizar estado de la configuracion CONFPRE_3
                await this.configPreestrategiaModel.updateOne(
                  { codigo: codigosConfiguracionPreestrategia.CONFPRE_3 },
                  { estado: estadosConfiguracionPreestrategia.inactivo, fechaActualizacion: new Date() },
                  opts,
                );
                // actualizar estado de la configuracion CONFPRE_1
                await this.configPreestrategiaModel.updateOne(
                  { codigo: codigosConfiguracionPreestrategia.CONFPRE_1 },
                  { estado: estadosConfiguracionPreestrategia.activa, fechaActualizacion: new Date() },
                  opts,
                );
              }

            }
          }

          //valor obtenido de la FORMULA SEMANAS X DIAS
          const getForm = await this.formulaEventoService.obtenerFormulaEvento(
            codigosFormulasEvento.semanasxdias,
          );
          //Remmplaza las SEMANAS
          let formulaSemanasxDias = getForm.replace(
            'SEMANAS',
            semanasPreestrategia,
          );
          //Numero de dias
          const getSemanasxDias = eval(formulaSemanasxDias);


          // const intervalo = configEvento.intervalo / (60 * 60 * 24 * 1000); // cada 150 dias (5 meses) se creara un nuevo evento de conteo de foro
          const intervalo = getSemanasxDias


          fechaInicio = new Date(configEvento.fechaProximo);
          fechaProxima = new Date(configEvento.fechaProximo);
          fechaFin = new Date(configEvento.fechaProximo);
          fechaProxima.setDate(fechaProxima.getDate() + intervalo);
          fechaFin.setDate(configEvento.fechaProximo.getDate() + intervalo);

          //Crear evento para el foro (cada 4 meses se crea)
          const evento = {
            estado: estadoEvento.pendiente,
            fechaInicio: fechaInicio,
            fechaFin: fechaFin,
            configuracionEvento: configEvento.codigo,
            proyectos: [],
          };
          await new this.eventoModel(evento).save(opts);

          await this.configEventoModel.updateOne(
            { codigo: codigosConfiguracionEvento.eventoForoProyectosCONFEVT_3 },
            { fechaProximo: fechaProxima },
            opts,
          );

          //Se crea el evento de inicio de foro dura un mes(30 dias)
          const configEventoForo = await this.configEventoModel.findOne({
            codigo: codigosConfiguracionEvento.eventoForoProyectosCONFEVT_4,
          });

          const configForo = await this.configForoModel.find({}).sort({ orden: 1 });
          let semanasForo
          if (configForo.length > 0) {
            for (const foro of configForo) {
              if (foro.orden === 1 && foro.estado === estadosConfiguracionForo.activa) {
                semanasForo = foro.semanas
                // actualizar estado de la configuracion CONFORO_1
                await this.configForoModel.updateOne(
                  { codigo: codigosConfiguracionForo.CONFORO_1 },
                  { estado: estadosConfiguracionForo.inactivo, fechaActualizacion: new Date() },
                  opts,
                );
                // actualizar estado de la configuracion CONFORO_2
                await this.configForoModel.updateOne(
                  { codigo: codigosConfiguracionForo.CONFORO_2 },
                  { estado: estadosConfiguracionForo.activa, fechaActualizacion: new Date() },
                  opts,
                );
              }

              if (foro.orden === 2 && foro.estado === estadosConfiguracionForo.activa) {
                semanasForo = foro.semanas
                // actualizar estado de la configuracion CONFORO_2
                await this.configForoModel.updateOne(
                  { codigo: codigosConfiguracionForo.CONFORO_2 },
                  { estado: estadosConfiguracionForo.inactivo, fechaActualizacion: new Date() },
                  opts,
                );
                // actualizar estado de la configuracion CONFORO_3
                await this.configForoModel.updateOne(
                  { codigo: codigosConfiguracionForo.CONFORO_3 },
                  { estado: estadosConfiguracionForo.activa, fechaActualizacion: new Date() },
                  opts,
                );
              }


              if (foro.orden === 3 && foro.estado === estadosConfiguracionForo.activa) {
                semanasForo = foro.semanas
                // actualizar estado de la configuracion CONFPRE_3
                await this.configForoModel.updateOne(
                  { codigo: codigosConfiguracionForo.CONFORO_3 },
                  { estado: estadosConfiguracionForo.inactivo, fechaActualizacion: new Date() },
                  opts,
                );
                // actualizar estado de la configuracion CONFORO_1
                await this.configForoModel.updateOne(
                  { codigo: codigosConfiguracionForo.CONFORO_1 },
                  { estado: estadosConfiguracionForo.activa, fechaActualizacion: new Date() },
                  opts,
                );
              }

            }
          }

          //valor obtenido de la FORMULA SEMANAS X DIAS
          const getFormForo = await this.formulaEventoService.obtenerFormulaEvento(
            codigosFormulasEvento.semanasxdias,
          );
          //Remmplaza las SEMANAS
          let formulaSemanasxDiasForo = getFormForo.replace(
            'SEMANAS',
            semanasForo,
          );
          //Numero de dias
          const getSemanasxDiasForo = eval(formulaSemanasxDiasForo);

          // const duracionForo =
          //   configEventoForo.duracion / (60 * 60 * 24 * 1000); //  30 dias finaliza el evento
          const duracionForo = getSemanasxDiasForo
          fechaIniForo = new Date(configEvento.fechaProximo);
          fechaIniForo.setDate(fechaIniForo.getDate() + duracionForo);
          const eventoIniForo = {
            estado: estadoEvento.pendiente,
            fechaInicio: fechaIniForo,
            fechaFin: fechaFin,
            configuracionEvento: configEventoForo.codigo,
            proyectos: [],
          };
          await new this.eventoModel(eventoIniForo).save(opts);

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
        } else {
          //Finaliza la transaccion
          await session.endSession();
        }

      }
    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
