import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import { RollbackCreacionCuentaCoinpaymentsService } from 'src/entidades/cuenta/casos_de_uso/rollback-creacion-cuenta-coinpayments.service';
import { ValidarCuentaCoinpaymentsService } from 'src/entidades/cuenta/casos_de_uso/validar-cuenta-coinpayments.service';
import { ObtenerUsuariosCriptoService } from 'src/entidades/usuario/casos_de_uso/obtener-usuarios-cripto.service';

@Injectable()
export class EjecutarCronVerificarUsuarioCriptoService {
  constructor(
    private obtenerUsuariosCriptoService: ObtenerUsuariosCriptoService,
    private validarCuentaCoinpaymentsService: ValidarCuentaCoinpaymentsService,
    private rollbackCreacionCuentaCoinpaymentsService: RollbackCreacionCuentaCoinpaymentsService
  ) { }

  // Se ejecuta cada HORA
  @Cron(CronExpression.EVERY_HOUR)
  //Se crea el evento de gastos operativos
  async ejecutarCronEliminarUsuarioSinPago() {
    try {
      //Obtiene todos los usarios sin pago que no tiene ningun rol
      const usuario = await this.obtenerUsuariosCriptoService.obtenerUsuariosCripto();


      // console.log('usuario.length: ', usuario.length)
      if (usuario.length > 0) {

        //fecha actual
        const formatFechaActual = moment().format('YYYY-MM-DD HH:mm:ss');
        const fechaActual = new Date(formatFechaActual);

        //Recorre todos los usuarios
        for (const getUsuario of usuario) {
          // console.log('fecha antes: ', getUsuario.fechaCreacion)

          //Se le agrega 12 horas a la fecha de creacion del usuario
          getUsuario.fechaCreacion.setHours(getUsuario.fechaCreacion.getHours() + 12);

          console.log('getUsuario.fechaCreacion: ', getUsuario.fechaCreacion)
          console.log('fechaActual: ', fechaActual)

          //si la fecha de creacion del usuario es menor a 12 horas de creacion
          if (fechaActual <= getUsuario.fechaCreacion) {
            console.log('DEBE VALIDAR LA CUENTA DE USUARIO CRIPTO')
            await this.validarCuentaCoinpaymentsService.validarCuenta(getUsuario.transacciones[0], getUsuario.email)
          } else {
            console.log('DEBE BORRAR LA CUENTA HACER ROLLBACK DE USUARIOS CRIPTO')
            await this.rollbackCreacionCuentaCoinpaymentsService.rollBackCrearCuenta(getUsuario.email)
          }

        }
      }

      return true;

    } catch (error) {
      throw error;
    }
  }
}
