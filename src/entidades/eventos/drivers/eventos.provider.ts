import { Connection } from 'mongoose';
import { ConfiguracionEventoModelo } from 'src/drivers/mongoose/modelos/configuracion_evento/configuracion-evento.schema';
import { ConfiguracionForoModelo } from 'src/drivers/mongoose/modelos/configuracion_foro/configuracion-foro.schema';
import { ConfiguracionPreestrategiaModelo } from 'src/drivers/mongoose/modelos/configuracion_preestrategia/configuracion-preestrategia.schema';
import { TransaccionModelo } from 'src/drivers/mongoose/modelos/transaccion/transaccion.schema';
import { entidadesModelo } from './../../../drivers/mongoose/modelos/catalogoEntidades/catalogoEntidadesModelo';
import { EventoModelo } from './../../../drivers/mongoose/modelos/evento/evento.schema';

export const eventosProviders = [
  {
    provide: 'TRANSACCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('transaccion', TransaccionModelo, 'transaccion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ESTADOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('catalogo_estados', entidadesModelo, 'catalogo_estados'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'EVENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('evento', EventoModelo, 'evento'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_EVENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_evento',
        ConfiguracionEventoModelo,
        'configuracion_evento',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_PREESTRATEGIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_preestrategia',
        ConfiguracionPreestrategiaModelo,
        'configuracion_preestrategia',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_FORO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_foro',
        ConfiguracionForoModelo,
        'configuracion_foro',
      ),
    inject: ['DB_CONNECTION'],
  },
];
