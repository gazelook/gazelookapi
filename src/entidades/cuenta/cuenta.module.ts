import { HttpModule, Module } from '@nestjs/common';
import { CatalogosServiceModule } from '../catalogos/casos_de_uso/catalogos-services.module';
import { CuentaServiceModule } from './casos_de_uso/cuenta.services.module';
import { CuentaControllerModule } from './controladores/cuenta.controller.module';

@Module({
  imports: [CuentaControllerModule, HttpModule],
  providers: [CuentaServiceModule, CatalogosServiceModule],
  controllers: [],
  exports: [],
})
export class CuentaModule {}
