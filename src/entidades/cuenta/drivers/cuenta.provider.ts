import { Connection } from 'mongoose';
import { albumModelo } from '../../../drivers/mongoose/modelos/album/album.schema';
import { archivoModelo } from '../../../drivers/mongoose/modelos/archivo/archivo.schema';
import { DireccionModelo } from '../../../drivers/mongoose/modelos/direcciones/direccion.schema';
import { DocumentoUsuario } from '../../../drivers/mongoose/modelos/documento_usuario/documento-usuario.schema';
import { informacionPago } from '../../../drivers/mongoose/modelos/informacion_pago/informacion-pago.schema';
import { MediaModelo } from '../../../drivers/mongoose/modelos/media/mediaModelo';
import { perfilModelo } from '../../../drivers/mongoose/modelos/perfil/perfil.schema';
import { SuscripcionModelo } from '../../../drivers/mongoose/modelos/suscripcion/suscripcion.schema';
import { TelefonoModelo } from '../../../drivers/mongoose/modelos/telefonos/telefono.schema';
import { TraduccionMediaModelo } from '../../../drivers/mongoose/modelos/traducion_media/traduccion-media.schema';
import { TransaccionModelo } from '../../../drivers/mongoose/modelos/transaccion/transaccion.schema';
import { usuarioModelo } from '../../../drivers/mongoose/modelos/usuarios/usuario.schema';

export const CuentaProviders = [
  {
    provide: 'USUARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('usuario', usuarioModelo, 'usuario'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ALBUM_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('album', albumModelo, 'album'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'MEDIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('media', MediaModelo, 'media'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_MEDIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_media',
        TraduccionMediaModelo,
        'traduccion_media',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRANSACCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('transaccion', TransaccionModelo, 'transaccion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'INFORMACION_PAGO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('informacion_pago', informacionPago, 'informacion_pago'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'SUSCRIPCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('suscripcion', SuscripcionModelo, 'suscripcion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ARCHIVO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('archivo', archivoModelo, 'archivo'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_MEDIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_media',
        TraduccionMediaModelo,
        'traduccion_media',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'DIRECCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('direccion', DireccionModelo, 'direccion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TELEFONO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('telefono', TelefonoModelo, 'telefono'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'DOCUMENTO_USUARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'documento_usuario',
        DocumentoUsuario,
        'documento_usuario',
      ),
    inject: ['DB_CONNECTION'],
  },
];
