import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
} from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Headers,
  UseGuards,
} from '@nestjs/common';
import { Funcion } from '../../../shared/funcion';
import { I18nService } from 'nestjs-i18n';
import { RenovarSuscripcionService } from '../casos_de_uso/renovar-suscripcion.service';
import { RenovarSuscripcionDto } from '../entidad/renovar-suscripcion.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Cuenta')
@Controller('api/cuenta/renovar-suscripcion')
export class RenovarSuscripcion {
  constructor(
    private readonly renovarSuscripcionService: RenovarSuscripcionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'Suscripción activa con éxito' })
  @ApiResponse({
    status: 400,
    description: 'Error al crear la suscripcion',
  })
  @ApiOperation({
    summary: 'Renovara o creara una nueva suscripcion',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async renovarCuenta(
    @Body() dataSuscripcion: RenovarSuscripcionDto,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    console.log('dataRenovacion', dataSuscripcion);

    try {
      const suscripcion = await this.renovarSuscripcionService.renovarSuscripcion(
        dataSuscripcion,
        codIdioma,
      );

      // proceso normal
      if (suscripcion) {
        const CREACION_CORRECTA = await this.i18n.translate(
          codIdioma.concat('.CREACION_CORRECTA'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: CREACION_CORRECTA,
          datos: suscripcion,
        });
      } else {
        const ERROR_REGISTRO_PAGO = await this.i18n.translate(
          codIdioma.concat('.ERROR_REGISTRO_PAGO'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ERROR_REGISTRO_PAGO,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
