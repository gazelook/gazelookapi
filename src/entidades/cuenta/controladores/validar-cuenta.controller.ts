import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  Request,
} from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { GestionCuentaService } from '../casos_de_uso/gestion-cuenta.service';
import { RollbackCreacionCuentaService } from '../casos_de_uso/rollback-creacion-cuenta.service';
import { RetornoValidarCuentaDto } from '../entidad/retorno-validar-cuenta.dto';
import { ValidarCuentaDto } from '../entidad/validar-cuenta.dto';

@ApiTags('Cuenta')
@Controller('api/cuenta/validar')
export class ValidarCuentaControlador {
  constructor(
    private readonly gestionCuentaService: GestionCuentaService,
    private readonly rollbackCreacionCuentaService: RollbackCreacionCuentaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Post('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: RetornoValidarCuentaDto,
    description: 'Cuenta Verificada',
  })
  @ApiResponse({
    status: 404,
    description: 'Error al validar la cuenta del usuario',
  })
  @ApiOperation({
    summary:
      'Esta función validara la cuenta del usuario. (cambiara el estado de la cuenta a activo y se enviara un email de verificacion de cuenta al usuario)',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async validarCuenta(
    @Body() validar: ValidarCuentaDto,
    @Headers() headers,
    @Request() req,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    console.log('dataValidar', validar);

    try {
      const dispositivo = {
        ip: req.ip,
        userAgent: headers['user-agent'],
      };

      if (
        mongoose.isValidObjectId(validar.idTransaccion) &&
        !validar.pagoNoConfirmado
      ) {
        const cuentaVerificada = await this.gestionCuentaService.validarCuenta(
          validar.idTransaccion,
          codIdioma,
          dispositivo,
        );
        if (cuentaVerificada) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
            datos: cuentaVerificada,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_CREACION,
          });
        }
      } else if (
        mongoose.isValidObjectId(validar.idTransaccion) &&
        validar.pagoNoConfirmado
      ) {
        //_______________rolback________________________
        await this.rollbackCreacionCuentaService.rollBackCrearCuenta(
          validar.idTransaccion,
        );
        const ELIMINACION_CORRECTA = await this.i18n.translate(
          codIdioma.concat('.ELIMINACION_CORRECTA'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.CONFLICT,
          mensaje: ELIMINACION_CORRECTA,
        });
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
