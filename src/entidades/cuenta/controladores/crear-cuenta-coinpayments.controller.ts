import {
  Body, Controller, Headers, HttpStatus, Post, Req
} from '@nestjs/common';
import {
  ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { CrearCuentaCoinpaymentsService } from '../casos_de_uso/crear-cuenta-coinpayments.service';
import { CrearCuentaCoinpaymentsDto } from '../entidad/cuenta-coinpayments.dto';


@ApiTags('Cuenta')
@Controller('api/cuenta-coinpayments')
export class CrearCuentaCoinpaymentsControlador {
  constructor(
    private readonly crearCuentaCoinpaymentsService: CrearCuentaCoinpaymentsService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: CrearCuentaCoinpaymentsDto,
    description: 'Session de pago y cuenta de usuario pendiente de activación',
  })
  @ApiResponse({
    status: 400,
    description: 'Error al crear la cuenta del usuario',
  })
  @ApiOperation({
    summary:
      'Esta función guardara los datos de los perfiles de usuario, albunes, archivos y retornara el token de session de pago',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async crearCuenta(
    @Body() cuentaDto: CrearCuentaCoinpaymentsDto,
    @Req() req,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    //const dataToken = req.user;
    //console.log("dispositivoToken", dataToken);

    const dispositivo = {
      ip: req.ip,
      userAgent: headers['user-agent'],
    };

    try {
      const cuenta = await this.crearCuentaCoinpaymentsService.crearCuenta(
        cuentaDto,
        codIdioma,
        dispositivo
      );
      // respuesta en el caso que el pago falle, se genera un nuevo pago si
      // intenta crear de nuevo la cuenta con los mismos datos
      if (cuenta.usuario) {
        const CUENTA_REGISTRADA_NO_PAGO = await this.i18n.translate(
          codIdioma.concat('.CUENTA_REGISTRADA_NO_PAGO'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: CUENTA_REGISTRADA_NO_PAGO,
          datos: cuenta,
        });
      }

      // proceso normal
      if (cuenta) {
        const CREACION_CORRECTA = await this.i18n.translate(
          codIdioma.concat('.CREACION_CORRECTA'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: CREACION_CORRECTA,
          datos: cuenta,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
