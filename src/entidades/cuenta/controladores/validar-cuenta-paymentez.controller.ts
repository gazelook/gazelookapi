import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  Request,
} from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { RollbackCreacionCuentaPaymentezService } from '../casos_de_uso/rollback-creacion-cuenta-paymentez.service';
import { ValidarCuentaPaymentezService } from '../casos_de_uso/validar-cuenta-paymentez.service';
import { ValidarCuentaPaymentezDto } from '../entidad/validar-cuenta-paymentez.dto';

@ApiTags('Cuenta')
@Controller('api/cuenta/validar-paymentez')
export class ValidarCuentaPaymentezControlador {
  constructor(
    private readonly validarCuentaPaymentezService: ValidarCuentaPaymentezService,
    private readonly rollbackCreacionCuentaPaymentezService: RollbackCreacionCuentaPaymentezService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Post('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: ValidarCuentaPaymentezDto,
    description: 'Cuenta Verificada',
  })
  @ApiResponse({
    status: 404,
    description: 'Error al validar la cuenta del usuario',
  })
  @ApiOperation({
    summary:
      'Esta función validara la cuenta del usuario. (cambiara el estado de la cuenta a activo y se enviara un email de verificacion de cuenta al usuario)',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async validarCuenta(
    @Body() validar: ValidarCuentaPaymentezDto,
    @Headers() headers,
    @Request() req,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    // console.log('dataValidar', validar);

    try {
      const dispositivo = {
        ip: req.ip,
        userAgent: headers['user-agent'],
      };

      console.log('dispositivo: ', dispositivo)
      if (
        validar.datosFacturacion &&
        validar.metodoPago.codigo &&
        validar.monedaRegistro.codNombre &&
        validar.transacciones.length > 0 &&
        !validar.pagoNoConfirmado &&
        validar.autorizacionCodePaymentez
      ) {
        const cuentaVerificada = await this.validarCuentaPaymentezService.validarCuenta(
          validar,
          codIdioma,
          dispositivo,
        );
        if (cuentaVerificada) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
            datos: cuentaVerificada,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_CREACION,
          });
        }
      } else if (validar.pagoNoConfirmado) {
        //_______________rolback________________________
        await this.rollbackCreacionCuentaPaymentezService.rollBackCrearCuenta(
          validar.email,
        );
        const ELIMINACION_CORRECTA = await this.i18n.translate(
          codIdioma.concat('.ELIMINACION_CORRECTA'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.CONFLICT,
          mensaje: ELIMINACION_CORRECTA,
        });
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
