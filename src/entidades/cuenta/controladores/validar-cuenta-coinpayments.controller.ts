import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  Request,
} from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { RollbackCreacionCuentaCoinpaymentsService } from '../casos_de_uso/rollback-creacion-cuenta-coinpayments.service';
import { RollbackCreacionCuentaPaymentezService } from '../casos_de_uso/rollback-creacion-cuenta-paymentez.service';
import { ValidarCuentaCoinpaymentsService } from '../casos_de_uso/validar-cuenta-coinpayments.service';
import { ValidarCuentaPaymentezService } from '../casos_de_uso/validar-cuenta-paymentez.service';
import { ValidarCuentaCoinpaymentsDto } from '../entidad/validar-cuenta-coinpayments.dto';
import { ValidarCuentaPaymentezDto } from '../entidad/validar-cuenta-paymentez.dto';

@ApiTags('Cuenta')
@Controller('api/cuenta/validar-coinpayments')
export class ValidarCuentaCoinPaymentsControlador {
  constructor(
    private readonly validarCuentaCoinpaymentsService: ValidarCuentaCoinpaymentsService,
    private readonly rollbackCreacionCuentaCoinpaymentsService: RollbackCreacionCuentaCoinpaymentsService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) { }
  @Post('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: ValidarCuentaCoinpaymentsDto,
    description: 'Cuenta Verificada',
  })
  @ApiResponse({
    status: 404,
    description: 'Error al validar la cuenta del usuario',
  })
  @ApiOperation({
    summary:
      'Esta función validara la cuenta del usuario. (cambiara el estado de la cuenta a activo y se enviara un email de verificacion de cuenta al usuario)',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async validarCuenta(
    @Body() validar: ValidarCuentaCoinpaymentsDto,
    @Headers() headers,
    @Request() req,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    // console.log('dataValidar', validar);

    try {
      const dispositivo = {
        ip: req.ip,
        userAgent: headers['user-agent'],
      };

      // console.log('dispositivo: ', dispositivo)
      if (
        validar.idTransaccion &&
        validar.email
      ) {
        const cuentaVerificada = await this.validarCuentaCoinpaymentsService.validarCuenta(
          validar.idTransaccion,
          validar.email
        );
        if (cuentaVerificada) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
            datos: cuentaVerificada,
          });
        } 
        else {
          //_______________rolback________________________
          // await this.rollbackCreacionCuentaCoinpaymentsService.rollBackCrearCuenta(validar.email)

          const ERROR_REGISTRO_PAGO = await this.i18n.translate(
            codIdioma.concat('.ERROR_REGISTRO_PAGO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_REGISTRO_PAGO,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
