import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { ValidarRenovacionCuentaService } from '../casos_de_uso/validar-renovacion.service';
import { ValidarCuentaDto } from '../entidad/validar-cuenta.dto';

@ApiTags('Cuenta')
@Controller('api/cuenta/validar-suscripcion')
export class ValidarRenovacionController {
  constructor(
    private readonly validarRenovacionCuentaService: ValidarRenovacionCuentaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Post('/')
  @UseGuards(AuthGuard('jwt'))
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'Se ha renovado la suscripción' })
  @ApiResponse({
    status: 404,
    description: 'Error al renovar la suscripcion',
  })
  @ApiOperation({
    summary: 'Esta función validara la renovacion de la suscripcion',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async VerificarCuenta(
    @Body() validar: ValidarCuentaDto,
    @Headers() headers,
    @Request() req,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    console.log('dataValidar', validar);

    try {
      if (mongoose.isValidObjectId(validar.idTransaccion)) {
        const cuentaVerificada = await this.validarRenovacionCuentaService.validarRenovacionCuenta(
          validar.idTransaccion,
        );
        if (cuentaVerificada) {
          const SUSCRIPCION_RENOVADA = await this.i18n.translate(
            codIdioma.concat('.SUSCRIPCION_RENOVADA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: SUSCRIPCION_RENOVADA,
            datos: cuentaVerificada,
          });
        } else {
          const ERROR_SUSCRIPCION = await this.i18n.translate(
            codIdioma.concat('.ERROR_SUSCRIPCION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_SUSCRIPCION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
