import { HttpModule, Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { HttpErrorFilter } from '../../../shared/filters/http-error.filter';
import { CuentaServiceModule } from '../casos_de_uso/cuenta.services.module';
import { CrearCuentaCoinpaymentsControlador } from './crear-cuenta-coinpayments.controller';
import { CrearCuentaPaymentezControlador } from './crear-cuenta-paymentez.controller';
import { CrearCuentaControlador } from './crear-cuenta.controller';
import { RenovarSuscripcion } from './renovar-suscripcion.controller';
import { ValidarCuentaCoinPaymentsControlador } from './validar-cuenta-coinpayments.controller';
import { ValidarCuentaPaymentezControlador } from './validar-cuenta-paymentez.controller';
import { ValidarCuentaControlador } from './validar-cuenta.controller';
import { ValidarRenovacionController } from './validar-renovacion.controller';

@Module({
  imports: [CuentaServiceModule, HttpModule],
  providers: [HttpErrorFilter, Funcion],
  exports: [CuentaServiceModule],
  controllers: [
    CrearCuentaControlador,
    ValidarCuentaControlador,
    RenovarSuscripcion,
    ValidarRenovacionController,
    ValidarCuentaPaymentezControlador,
    CrearCuentaPaymentezControlador,
    CrearCuentaCoinpaymentsControlador,
    ValidarCuentaCoinPaymentsControlador
  ],
})
export class CuentaControllerModule {}
