import { HttpStatus, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { openStdin } from 'process';
import { CoinpaymentezInfoTransactionService } from 'src/drivers/coinpaymentez/services/coinpayments-info-transaction.service';
import { CrearEmailBienvenidaService } from 'src/entidades/emails/casos_de_uso/crear-email-bienvenida.service';
import { CrearPagoPaymentezService } from 'src/entidades/transaccion/casos_de_uso/crear-pago-paymentez.service';
import { GestionPagoCoinpaymentsService } from 'src/entidades/transaccion/casos_de_uso/gestion-pago-coinpayments.service';
import { GestionPagoPaymentezService } from 'src/entidades/transaccion/casos_de_uso/gestion-pago-paymentez.service';
import { erroresCuenta } from '../../../shared/enum-errores';
import {
  catalogoOrigen,
  codigosCatalogoSuscripcion,
  codigosMetodosPago,
  estadosUsuario,
  porcentajeComisionCoinpaiments,
  porcentajeComisionPaymentez,
} from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoTipoEmailService } from '../../catalogos/casos_de_uso/catalogo-tipo-email.service';
import { CrearEmailMenorEdadService } from '../../emails/casos_de_uso/crear-email-menor-edad.service';
import { CrearEmailReciboPagoService } from '../../emails/casos_de_uso/crear-email-recibo-pago.service';
import { CrearEmailService } from '../../emails/casos_de_uso/crear-email.service';
import { LoginService } from '../../login/casos_de_uso/login.service';
import { ActualizarSuscripcionService } from '../../suscripcion/casos_de_uso/actualizar-suscripcion.service';
import { ActualizarConversionTransaccionService } from '../../transaccion/casos_de_uso/actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from '../../transaccion/casos_de_uso/actualizar-transaccion.service';
import { GestionPagoPaypalService } from '../../transaccion/casos_de_uso/gestion-pago-paypal.service';
import { GestionPagoStripeService } from '../../transaccion/casos_de_uso/gestion-pago-stripe.service';
import { ObtenerTransaccionService } from '../../transaccion/casos_de_uso/obtener-transaccion.service';
import { UpdateTransaccionPagoCriptoDto, UpdateTransaccionPagoDto } from '../../transaccion/entidad/transaccion.dto';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { RetornoCuentaDto } from '../entidad/retorno-cuenta.dto';
import { ValidarCuentaPaymentezDto } from '../entidad/validar-cuenta-paymentez.dto';

@Injectable()
export class ValidarCuentaCoinpaymentsService {
  inactivaPago = estadosUsuario.inactivaPago;
  activaNoVerificado = estadosUsuario.activaNoVerificado;
  activo = estadosUsuario.activa;
  codigoMetodoPagoStripe = codigosMetodosPago.stripe;
  // codigoMetodoPagoPaypal = codigosMetodosPago.paypal;

  constructor(
    private crearEmail: CrearEmailService,
    private nombreTipoEmail: CatalogoTipoEmailService,
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private gestionPagoStripeService: GestionPagoStripeService,
    private gestionPagoPaypalService: GestionPagoPaypalService,
    private loginService: LoginService,
    private crearEmailMenorEdadService: CrearEmailMenorEdadService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private actualizarSuscripcionService: ActualizarSuscripcionService,
    private actualizarConversionTransaccionService: ActualizarConversionTransaccionService,
    private crearEmailReciboPagoService: CrearEmailReciboPagoService,
    private crearPagoPaymentezService: CrearPagoPaymentezService,
    private crearEmailBienvenidaService: CrearEmailBienvenidaService,
    private gestionPagoPaymentezService: GestionPagoPaymentezService,
    private coinpaymentezInfoTransactionService: CoinpaymentezInfoTransactionService,
    private gestionPagoCoinpaymentsService: GestionPagoCoinpaymentsService
  ) { }

  async validarCuenta(
    idTransaccion,
    email
  ): Promise<any> {
    const session = await mongoose.startSession();

    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };

    let transaccionDatabase;
    const opts = { session };

    try {
      const usuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithTransaccionByEmail(
        email,
      );

      //_____________________cuenta: proceso normal __________________
      if (!usuario)
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresCuenta.ERROR_REGISTRO_PAGO,
        };

      const idUsuario = usuario._id;

      let cuentaOk: RetornoCuentaDto = {};

      const transaccionQuery: any = await this.obtenerTransaccionService.obtenerTransaccionById(
        idTransaccion,
        opts,
      );
      if (!transaccionQuery) {
        throw {
          codigo: HttpStatus.EXPECTATION_FAILED,
          codigoNombre: erroresCuenta.TRANSACCION_NO_VALIDA,
        };
      }

      const informacionPago = transaccionQuery.informacionPago;
      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithID(
        transaccionQuery.usuario._id,
        opts,
      );

      //data para consultar transaccion en coinpayments
      let objTransaccion = {
        txid: informacionPago.idPago,
        full: 1
      }

      //VALIDA TRAMSACCION EN COINPAYMENTS QUE ESTE OK
      const getTransaccion = await this.coinpaymentezInfoTransactionService.obtenerTransaction(objTransaccion)


      if (getTransaccion.status === 100) {
        // ___________________________________________ Verificar Pago OK____________________________________________

        // actualizar usuario
        transaccionDatabase = await session.withTransaction(async () => {
          const updateUser = await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
            getUsuario._id,
            this.activaNoVerificado,
            opts,
          );
          if (!updateUser) {
            await session.abortTransaction();
            return;
          }
        }, transactionOptions);

        // ______________________________ actualizar transaccion (dos origenes) ________________________________
        const transaccionUser: any = await this.obtenerTransaccionService.obtenerTransaccionByUsuarioByPago(
          idUsuario,
          informacionPago._id,
          opts,
        );
        for (const transaccion of transaccionUser.transacciones) {

          if (transaccion.origen === null) {
            const montoRecibidoCripto = getTransaccion.receivedf

            const dataUpdate: UpdateTransaccionPagoCriptoDto = {
              monto: montoRecibidoCripto, // valor neto recibido
            };

            // if (transaccion.informacionPago.idPago === transaccionQuery.informacionPago.idPago) {
            transaccionDatabase = await session.withTransaction(async () => {
              const updatetransaccion = await this.actualizarTransaccionService.actualizarTransaccionCripto(
                transaccion._id,
                opts,
                dataUpdate,
              );
              if (!updatetransaccion) {
                await session.abortTransaction();
                return;
              }
            }, transactionOptions);

          } else {

            //__________________Calcular valor total por transaccion________________
            const getNewValor = this.gestionPagoCoinpaymentsService.calcularValorTotalComisionCoinpayments(
              // detallePagoStripe.amount,
              transaccion.monto,
              porcentajeComisionCoinpaiments.porcentaje,
            );

            const dataUpdate: UpdateTransaccionPagoDto = {
              comisionTransferencia: getNewValor.newFee, // valor de la comision
              totalRecibido: getNewValor.newAmountReceived, // total recibido
              origenPais: null,
            };

            // if (transaccion.informacionPago.idPago === transaccionQuery.informacionPago.idPago) {
            transaccionDatabase = await session.withTransaction(async () => {
              const updatetransaccion = await this.actualizarTransaccionService.actualizarTransaccion(
                transaccion._id,
                opts,
                dataUpdate,
              );
              if (!updatetransaccion) {
                await session.abortTransaction();
                return;
              }
            }, transactionOptions);
            // }

            //__________________ Actualizar conversion transaccion ________________
            transaccionDatabase = await session.withTransaction(async () => {
              const updateConversion = await this.actualizarConversionTransaccionService.actualizarConversionStripe(
                transaccion._id,
                dataUpdate,
                opts,
              );
              if (!updateConversion) {
                await session.abortTransaction();
                return;
              }
            }, transactionOptions);

            //___________________________________ actualizar suscripcion__________________________
            if (transaccion.origen === catalogoOrigen.suscripciones) {
              transaccionDatabase = await session.withTransaction(async () => {
                const updateSuscripcion = await this.actualizarSuscripcionService.actualizarSuscripcion(
                  transaccion._id,
                  getUsuario._id,
                  opts,
                );
                if (!updateSuscripcion) {
                  await session.abortTransaction();
                  return;
                }
              }, transactionOptions);
            }

          }

        }

        const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(
          getUsuario.idioma,
        );

        const login = await this.loginService.login(
          getUsuario,
          null,
          null,
          opts,
        );

        //________________________________enviar email________________________________________________________

        const dataEmail: any = {
          usuario: getUsuario._id,
          menorEdad: getUsuario.menorEdad,
          idioma: idiomaUsuario.codNombre,
          nombre: getUsuario.perfiles[0].nombre,
        };

        if (getUsuario.menorEdad) {
          const codTipEmaR = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
            'validacionResponsable',
          );
          let codEstadoValiRes = codTipEmaR.codigo;
          dataEmail.codigo = codEstadoValiRes;
          dataEmail.emailDestinatario = getUsuario.emailResponsable;

          await this.crearEmail.crearEmail(dataEmail);

          await this.crearEmailMenorEdadService.crearEmailMenorEdad(getUsuario);
        } else {
          const codTipEma = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
            'validacion',
          );
          let codTipoEmailValid = codTipEma.codigo;
          dataEmail.menorEdad = false;
          dataEmail.emailDestinatario = getUsuario.email;
          dataEmail.codigo = codTipoEmailValid;

          await this.crearEmail.crearEmail(dataEmail);
        }

        //_______________________________ enviar recibo pago ___________________________________
        const dataEmailPago = {
          idInformacionPago: informacionPago._id as string,
          usuario: getUsuario._id as string,
          nombre: getUsuario.perfiles[0].nombre as string,
          emailDestinatario: getUsuario.email as string,
          idioma: idiomaUsuario.codNombre as string,
        };
        await this.crearEmailReciboPagoService.crearEmailReciboPago(
          dataEmailPago,
        );

        //_______________________________email bienvenida ___________________________________
        transaccionDatabase = await session.withTransaction(async () => {
          await this.crearEmailBienvenidaService.crearEmailBienvenida(
            getUsuario._id,
            opts,
          );
        }, transactionOptions);

        const result = {
          usuario: login.usuario,
          tokenAccess: login.accessToken,
          tokenRefresh: login.tokenRefresh,
        };

        // proceso exitoso
        return result;
      } else {
        await session.endSession();
        return null;
      }
    } catch (error) {
      throw error;
    }
  }

  validarCodigosMetodosPago(codigo): Boolean {
    const codigos = [
      codigosMetodosPago.stripe,
      codigosMetodosPago.payments1,
      codigosMetodosPago.payments2,
      codigosMetodosPago.panama,
    ];
    if (codigos.indexOf(codigo) !== -1) {
      return true;
    } else {
      return false;
    }
  }
}
