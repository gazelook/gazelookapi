import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { StorageDocumentosUsuarioService } from '../../../drivers/amazon_s3/services/storage-documentos-usuario.service';
import { StorageService } from '../../../drivers/amazon_s3/services/storage.service';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';
import { Direccion } from '../../../drivers/mongoose/interfaces/direccion/direccion.interface';
import { DocumentoUsuario } from '../../../drivers/mongoose/interfaces/documento_usuario/documento-usuario.interface';
import { Media } from '../../../drivers/mongoose/interfaces/media/media.interface';
import { Perfil } from '../../../drivers/mongoose/interfaces/perfil/perfil.interface';
import { Suscripcion } from '../../../drivers/mongoose/interfaces/suscripcion/suscripcion.interface';
import { TraduccionMedia } from '../../../drivers/mongoose/interfaces/traduccion_media/traduccion-media.interface';
import { Transaccion } from '../../../drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { Usuario } from '../../../drivers/mongoose/interfaces/usuarios/usuario.interface';
const mongoose = require('mongoose');

@Injectable()
export class RollbackCreacionCuentaCoinpaymentsService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    @Inject('DIRECCION_MODEL')
    private readonly direccionModel: Model<Direccion>,
    @Inject('MEDIA_MODEL') private readonly mediaModel: Model<Media>,
    @Inject('TRADUCCION_MEDIA_MODEL')
    private readonly traduccionMediaModel: Model<TraduccionMedia>,
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    @Inject('SUSCRIPCION_MODEL')
    private readonly suscripcionModel: Model<Suscripcion>,
    @Inject('DOCUMENTO_USUARIO_MODEL')
    private readonly documentoUserModel: Model<DocumentoUsuario>,
    private storageAws: StorageService,
    private storageDocumentos: StorageDocumentosUsuarioService,
  ) { }

  async rollBackCrearCuenta(email: string): Promise<any> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      const opts = { session };

      // const transaccion = await this.transaccionModel.findById(idTransaccion);
      const usuario = await this.userModel.findOne({ email: email });
      // await this.transaccionModel.deleteMany({ usuario: usuario._id });
      // await this.suscripcionModel.deleteMany({ transaccion: transaccion._id });

      const perfiles = await this.perfilModel.find({ usuario: usuario._id });
      await this.direccionModel.findOne({ _id: usuario.direccion.toString() });
      for (const perfil of perfiles) {
        //eliminar direccion
        for (const direccion of perfil.direcciones) {
          await this.direccionModel.deleteOne({ _id: direccion.toString() });
        }
        if (perfil.telefonos) {
          for (const telefono of perfil.telefonos) {
            await this.direccionModel.deleteOne({ _id: telefono.toString() });
          }
        }
        // eliminar album
        for (const album of perfil.album) {
          const getAlbum = await this.albumModel.findOne({
            _id: album.toString(),
          });
          // eliminar medias
          console.log('getAlbum', getAlbum);
          for (const media of getAlbum.media) {
            const getMedia = await this.mediaModel.findOne({
              _id: media.toString(),
            });

            //eliminar archivos
            if (getMedia) {
              if (getMedia.principal) {
                const getArchivo = await this.archivoModel.findOne({
                  _id: getMedia.principal.toString(),
                });
                console.log('getArchivo', getArchivo);
                this.storageAws.eliminarArchivo(getArchivo.filenameStorage);
                await this.archivoModel.deleteOne({ _id: getArchivo._id });
              }
              if (getMedia.miniatura) {
                const getArchivo = await this.archivoModel.findOne({
                  _id: getMedia.miniatura.toString(),
                });
                this.storageAws.eliminarArchivo(getArchivo.filenameStorage);
                await this.archivoModel.deleteOne({ _id: getArchivo._id });
              }
              await this.mediaModel.deleteOne({ _id: getMedia._id });
              await this.traduccionMediaModel.deleteMany({
                media: getMedia._id,
              });
            }
          }
          // documentos del usuario
          if (usuario.documentosUsuario) {
            for (const documento of usuario.documentosUsuario) {
              const getDocumentoUsuario = await this.documentoUserModel.findById(
                documento.toString(),
              );
              const getArchivo = await this.archivoModel.findOne({
                _id: getDocumentoUsuario.archivo.toString(),
              });
              console.log('getDocumentoUsuario', getArchivo);
              this.storageDocumentos.eliminarArchivo(
                getArchivo.filenameStorage,
              );
              await this.archivoModel.deleteOne({ _id: getArchivo._id });
            }
            await this.documentoUserModel.deleteMany({ usuario: usuario._id });
          }
          await this.albumModel.deleteOne({ _id: album.toString() });
        }
        if (usuario.transacciones.length) {
          for (const trans of usuario.transacciones) {
            await this.transaccionModel.deleteOne({ _id: trans })
          }

        }
        if (usuario.suscripciones.length) {
          for (const suscrip of usuario.suscripciones) {
            await this.suscripcionModel.deleteOne({ _id: suscrip })
          }

        }

        await this.perfilModel.deleteOne({ _id: perfil._id });
        await this.userModel.deleteOne({ _id: usuario._id });
      }
      // proceso exitoso
      await session.commitTransaction();
      session.endSession();

      return true;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }

  // validar y generar nuevo pago
}
