import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from '../../../drivers/mongoose/interfaces/usuarios/usuario.interface';
import { erroresCuenta } from '../../../shared/enum-errores';
import { codigosMetodosPago } from '../../../shared/enum-sistema';
import { CrearPagoPaypalService } from '../../transaccion/casos_de_uso/crear-pago-paypal.service';
import { CrearPagoStripeService } from '../../transaccion/casos_de_uso/crear-pago-stripe.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { RenovarSuscripcionDto } from '../entidad/renovar-suscripcion.dto';
import { GestionCuentaService } from './gestion-cuenta.service';

const mongoose = require('mongoose');

@Injectable()
export class RenovarSuscripcionService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private crearPagoStripeService: CrearPagoStripeService,
    private crearPagoPaypalService: CrearPagoPaypalService,
    private gestionCuentaService: GestionCuentaService,
  ) {}

  async renovarSuscripcion(
    dataRenovacion: RenovarSuscripcionDto,
    idioma,
  ): Promise<any> {
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      const opts = { session };

      // validar codigos metodos pago
      if (
        !this.gestionCuentaService.validarCodigosMetodosPago(
          dataRenovacion.metodoPago.codigo,
        )
      ) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresCuenta.ERROR_METODO_PAGO,
        };
      }

      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
        dataRenovacion._id,
      );

      let datosPago: any = {
        email: getUsuario.email,
        idUsuario: getUsuario._id,
        metodoPago: dataRenovacion.metodoPago.codigo,
      };

      if (dataRenovacion.datosFacturacion) {
        let total = 0;
        for (let transaccion of dataRenovacion.transacciones) {
          total += transaccion.monto;
        }

        datosPago.nombres = dataRenovacion.datosFacturacion.nombres;
        datosPago.telefono = dataRenovacion.datosFacturacion.telefono;
        datosPago.direccion = dataRenovacion.datosFacturacion?.direccion;
        datosPago.monto = total;
        datosPago.transacciones = dataRenovacion.transacciones;
        datosPago.tipoSuscripcion = dataRenovacion.suscripcion.tipo.codigo;
        datosPago.renovacionCuenta = true;
      }

      // ___________________________________________Crear Pago____________________________________________
      let pagoOk: any = {};
      // if (codigosMetodosPago.paypal === dataRenovacion.metodoPago.codigo) {
      //   const pagoPaypal: any = await this.crearPagoPaypalService.crearPagoPaypal(datosPago, opts);
      //   pagoOk.idPago = pagoPaypal.idPaypal;
      //   pagoOk.idTransaccion = pagoPaypal.idTransaccion;
      //   pagoOk.idSuscripcion = pagoPaypal.idSuscripcion;
      // }
      if (codigosMetodosPago.stripe === dataRenovacion.metodoPago.codigo) {
        const pagoStripe: any = await this.crearPagoStripeService.crearPagoStripe(
          datosPago,
          opts,
        );

        pagoOk.idPago = pagoStripe.stripe.client_secret;
        pagoOk.idTransaccion = pagoStripe.idTransaccion;
        pagoOk.idSuscripcion = pagoStripe.idSuscripcion;
      }

      await session.commitTransaction();
      session.endSession();

      return pagoOk;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
