import { HttpStatus, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { openStdin } from 'process';
import { CrearEmailBienvenidaService } from 'src/entidades/emails/casos_de_uso/crear-email-bienvenida.service';
import { CrearPagoPaymentezService } from 'src/entidades/transaccion/casos_de_uso/crear-pago-paymentez.service';
import { GestionPagoPaymentezService } from 'src/entidades/transaccion/casos_de_uso/gestion-pago-paymentez.service';
import { erroresCuenta } from '../../../shared/enum-errores';
import {
  catalogoOrigen,
  codigosCatalogoSuscripcion,
  codigosMetodosPago,
  estadosUsuario,
  porcentajeComisionPaymentez,
} from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoTipoEmailService } from '../../catalogos/casos_de_uso/catalogo-tipo-email.service';
import { CrearEmailMenorEdadService } from '../../emails/casos_de_uso/crear-email-menor-edad.service';
import { CrearEmailReciboPagoService } from '../../emails/casos_de_uso/crear-email-recibo-pago.service';
import { CrearEmailService } from '../../emails/casos_de_uso/crear-email.service';
import { LoginService } from '../../login/casos_de_uso/login.service';
import { ActualizarSuscripcionService } from '../../suscripcion/casos_de_uso/actualizar-suscripcion.service';
import { ActualizarConversionTransaccionService } from '../../transaccion/casos_de_uso/actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from '../../transaccion/casos_de_uso/actualizar-transaccion.service';
import { GestionPagoPaypalService } from '../../transaccion/casos_de_uso/gestion-pago-paypal.service';
import { GestionPagoStripeService } from '../../transaccion/casos_de_uso/gestion-pago-stripe.service';
import { ObtenerTransaccionService } from '../../transaccion/casos_de_uso/obtener-transaccion.service';
import { UpdateTransaccionPagoDto } from '../../transaccion/entidad/transaccion.dto';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { RetornoCuentaDto } from '../entidad/retorno-cuenta.dto';
import { ValidarCuentaPaymentezDto } from '../entidad/validar-cuenta-paymentez.dto';

@Injectable()
export class ValidarCuentaPaymentezService {
  inactivaPagoPaymentez = estadosUsuario.inactivaPagoPaymentez;
  activaNoVerificado = estadosUsuario.activaNoVerificado;
  codigoMetodoPagoStripe = codigosMetodosPago.stripe;
  // codigoMetodoPagoPaypal = codigosMetodosPago.paypal;

  constructor(
    private crearEmail: CrearEmailService,
    private nombreTipoEmail: CatalogoTipoEmailService,
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private gestionPagoStripeService: GestionPagoStripeService,
    private gestionPagoPaypalService: GestionPagoPaypalService,
    private loginService: LoginService,
    private crearEmailMenorEdadService: CrearEmailMenorEdadService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private actualizarSuscripcionService: ActualizarSuscripcionService,
    private actualizarConversionTransaccionService: ActualizarConversionTransaccionService,
    private crearEmailReciboPagoService: CrearEmailReciboPagoService,
    private crearPagoPaymentezService: CrearPagoPaymentezService,
    private crearEmailBienvenidaService: CrearEmailBienvenidaService,
    private gestionPagoPaymentezService: GestionPagoPaymentezService,
  ) {}

  async validarCuenta(
    validar: ValidarCuentaPaymentezDto,
    idioma,
    dispositivo,
  ): Promise<any> {
    const session = await mongoose.startSession();

    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };

    let transaccionDatabase;
    const opts = { session };

    try {
      const usuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithTransaccionByEmail(
        validar.email,
      );

      
      //_____________________cuenta: proceso normal __________________
      if (!usuario)
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresCuenta.ERROR_REGISTRO_PAGO,
        };

      const idUsuario = usuario._id;

      let cuentaOk: RetornoCuentaDto = {};
      // _____________________ datos de pago _________________
      const tipoMonedaRegistro = validar.monedaRegistro.codNombre;

      let datosPago: any = {
        email: validar.email,
        idUsuario: idUsuario,
        metodoPago: validar.metodoPago.codigo,
        tipoMonedaRegistro: tipoMonedaRegistro,
      };

      let total = 0;
      for (let transaccion of validar.transacciones) {
        total += transaccion.monto;
      }

      datosPago.nombres = validar.datosFacturacion.nombres;
      datosPago.telefono = validar.datosFacturacion.telefono;
      datosPago.direccion =
        validar.datosFacturacion?.direccion ||
        usuario.direccion.traducciones[0].descripcion;
      datosPago.monto = total;
      datosPago.transacciones = validar.transacciones;
      datosPago.tipoSuscripcion = codigosCatalogoSuscripcion.anual;
      datosPago.creacionCuenta = true;
      datosPago.idPago = validar.datosFacturacion.idPago;
      // ___________________________________________ Crear Pago ____________________________________________

      const pagoPaymentez = await this.crearPagoPaymentezService.crearPagoPaymentez(
        datosPago,
        opts,
      );

      cuentaOk.idPago = validar.datosFacturacion.idPago;
      cuentaOk.idTransaccion = pagoPaymentez.idTransaccion;

      const transaccionQuery: any = await this.obtenerTransaccionService.obtenerTransaccionById(
        cuentaOk.idTransaccion,
        opts,
      );

      

      if (!transaccionQuery) {
        throw {
          codigo: HttpStatus.EXPECTATION_FAILED,
          codigoNombre: erroresCuenta.TRANSACCION_NO_VALIDA,
        };
      }

      const informacionPago = transaccionQuery.informacionPago;
      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithID(
        transaccionQuery.usuario._id,
        opts,
      );

      if (getUsuario.estado === this.inactivaPagoPaymentez) {
        // ___________________________________________ Verificar Pago OK____________________________________________

        // const detallePagoStripe = await this.gestionPagoStripeService.obtenerDetallePagoStripe(informacionPago.idPago);
        // detallePagoStripe.amount = transaccionQuery.monto / 100;
        // detallePagoStripe.fee = detallePagoStripe.fee / 100;
        // detallePagoStripe.net = detallePagoStripe.net / 100;

        // actualizar usuario
        transaccionDatabase = await session.withTransaction(async () => {
          const updateUser = await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
            getUsuario._id,
            this.activaNoVerificado,
            opts,
          );
          if (!updateUser) {
            await session.abortTransaction();
            return;
          }
        }, transactionOptions);

        
        
        // ______________________________actualizar transaccion (dos origenes)________________________________
        const transaccionUser: any = await this.obtenerTransaccionService.obtenerTransaccionByUsuarioByPago(
          idUsuario,
          informacionPago._id,
          opts,
        );
        for (const transaccion of transaccionUser) {
          //__________________Calcular valor total por transaccion________________
          const getNewValor = this.gestionPagoPaymentezService.calcularValorTotalComisionPaymentez(
            // detallePagoStripe.amount,
            transaccion.monto,
            porcentajeComisionPaymentez.porcentaje,
          );

          const dataUpdate: UpdateTransaccionPagoDto = {
            comisionTransferencia: getNewValor.newFee, // valor de la comision
            totalRecibido: getNewValor.newAmountReceived, // total recibido
            origenPais: null,
          };

          // if (transaccion.informacionPago.idPago === transaccionQuery.informacionPago.idPago) {
          transaccionDatabase = await session.withTransaction(async () => {
            const updatetransaccion = await this.actualizarTransaccionService.actualizarTransaccion(
              transaccion._id,
              opts,
              dataUpdate,
            );
            if (!updatetransaccion) {
              await session.abortTransaction();
              return;
            }
          }, transactionOptions);
          // }

          //__________________Actualizar conversion transaccion________________
          transaccionDatabase = await session.withTransaction(async () => {
            const updateConversion = await this.actualizarConversionTransaccionService.actualizarConversionStripe(
              transaccion._id,
              dataUpdate,
              opts,
            );
            if (!updateConversion) {
              await session.abortTransaction();
              return;
            }
          }, transactionOptions);

          //___________________________________ actualizar suscripcion__________________________
          if (transaccion.origen === catalogoOrigen.suscripciones) {
            transaccionDatabase = await session.withTransaction(async () => {
              const updateSuscripcion = await this.actualizarSuscripcionService.actualizarSuscripcion(
                transaccion._id,
                getUsuario._id,
                opts,
              );
              if (!updateSuscripcion) {
                await session.abortTransaction();
                return;
              }
            }, transactionOptions);
          }
        }

        const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(
          getUsuario.idioma,
        );

        const getUser = await this.obtenerIdUsuarioService.obtenerPerfilesUsuarioSessionById(
          transaccionQuery.usuario._id,
          opts,
        );

        const login = await this.loginService.login(
          getUser,
          idioma,
          dispositivo,
          opts,
        );

        console.log('idUsuario------------->: ', idUsuario)
        //________________________________enviar email________________________________________________________

        const dataEmail: any = {
          usuario: getUsuario._id,
          menorEdad: getUsuario.menorEdad,
          idioma: idiomaUsuario.codNombre,
          nombre: getUsuario.perfiles[0].nombre,
        };

        if (getUsuario.menorEdad) {
          const codTipEmaR = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
            'validacionResponsable',
          );
          let codEstadoValiRes = codTipEmaR.codigo;
          dataEmail.codigo = codEstadoValiRes;
          dataEmail.emailDestinatario = getUsuario.emailResponsable;

          await this.crearEmail.crearEmail(dataEmail);

          await this.crearEmailMenorEdadService.crearEmailMenorEdad(getUsuario);
        } else {
          const codTipEma = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
            'validacion',
          );
          let codTipoEmailValid = codTipEma.codigo;
          dataEmail.menorEdad = false;
          dataEmail.emailDestinatario = getUsuario.email;
          dataEmail.codigo = codTipoEmailValid;

          
          transaccionDatabase = await session.withTransaction(async () => {
            await this.crearEmail.crearEmail(dataEmail, opts);
          }, transactionOptions);

        }

        //_______________________________enviar recibo pago___________________________________
        const dataEmailPago = {
          idInformacionPago: informacionPago._id as string,
          usuario: getUsuario._id as string,
          nombre: getUsuario.perfiles[0].nombre as string,
          emailDestinatario: getUsuario.email as string,
          idioma: idiomaUsuario.codNombre as string,
          autorizacionCodePaymentez: validar.autorizacionCodePaymentez as string
        };
        // await this.crearEmailReciboPagoService.crearEmailReciboPago(
        //   dataEmailPago,
        // );

        transaccionDatabase = await session.withTransaction(async () => {
          await this.crearEmailReciboPagoService.crearEmailReciboPago(
            dataEmailPago,
            opts
          );
        }, transactionOptions);

        //_______________________________email bienvenida ___________________________________
        transaccionDatabase = await session.withTransaction(async () => {
          await this.crearEmailBienvenidaService.crearEmailBienvenida(
            getUsuario._id,
            opts,
          );
        }, transactionOptions);

        const result = {
          usuario: login.usuario,
          tokenAccess: login.accessToken,
          tokenRefresh: login.tokenRefresh,
        };

        // proceso exitoso
        return result;
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }

  validarCodigosMetodosPago(codigo): Boolean {
    const codigos = [
      codigosMetodosPago.stripe,
      codigosMetodosPago.payments1,
      codigosMetodosPago.payments2,
      codigosMetodosPago.panama,
    ];
    if (codigos.indexOf(codigo) !== -1) {
      return true;
    } else {
      return false;
    }
  }
}
