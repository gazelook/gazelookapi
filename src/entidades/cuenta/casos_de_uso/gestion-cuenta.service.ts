import { HttpStatus, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { CrearEmailBienvenidaService } from 'src/entidades/emails/casos_de_uso/crear-email-bienvenida.service';
import { erroresCuenta } from '../../../shared/enum-errores';
import {
  catalogoOrigen,
  codigosMetodosPago,
  estadosUsuario,
} from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoTipoEmailService } from '../../catalogos/casos_de_uso/catalogo-tipo-email.service';
import { CrearEmailMenorEdadService } from '../../emails/casos_de_uso/crear-email-menor-edad.service';
import { CrearEmailReciboPagoService } from '../../emails/casos_de_uso/crear-email-recibo-pago.service';
import { CrearEmailService } from '../../emails/casos_de_uso/crear-email.service';
import { LoginService } from '../../login/casos_de_uso/login.service';
import { ActualizarSuscripcionService } from '../../suscripcion/casos_de_uso/actualizar-suscripcion.service';
import { ActualizarConversionTransaccionService } from '../../transaccion/casos_de_uso/actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from '../../transaccion/casos_de_uso/actualizar-transaccion.service';
import { GestionPagoPaypalService } from '../../transaccion/casos_de_uso/gestion-pago-paypal.service';
import { GestionPagoStripeService } from '../../transaccion/casos_de_uso/gestion-pago-stripe.service';
import { ObtenerTransaccionService } from '../../transaccion/casos_de_uso/obtener-transaccion.service';
import { UpdateTransaccionPagoDto } from '../../transaccion/entidad/transaccion.dto';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';

@Injectable()
export class GestionCuentaService {
  inactivaPago = estadosUsuario.inactivaPago;
  activaNoVerificado = estadosUsuario.activaNoVerificado;
  codigoMetodoPagoStripe = codigosMetodosPago.stripe;
  // codigoMetodoPagoPaypal = codigosMetodosPago.paypal;

  constructor(
    private crearEmail: CrearEmailService,
    private nombreTipoEmail: CatalogoTipoEmailService,
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private gestionPagoStripeService: GestionPagoStripeService,
    private gestionPagoPaypalService: GestionPagoPaypalService,
    private loginService: LoginService,
    private crearEmailMenorEdadService: CrearEmailMenorEdadService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private actualizarSuscripcionService: ActualizarSuscripcionService,
    private actualizarConversionTransaccionService: ActualizarConversionTransaccionService,
    private crearEmailReciboPagoService: CrearEmailReciboPagoService,
    private crearEmailBienvenidaService: CrearEmailBienvenidaService
  ) {}

  async validarCuenta(idTransaccion, idioma, dispositivo): Promise<any> {

    console.log('idTransaccion: ', idTransaccion)
    console.log('idioma: ', idioma)
    console.log('dispositivo: ', dispositivo)
    const session = await mongoose.startSession();

    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };

    let transaccionDatabase;
    const opts = { session };

    try {
      const transaccionQuery: any = await this.obtenerTransaccionService.obtenerTransaccionById(
        idTransaccion,
      );
      if (!transaccionQuery) {
        throw {
          codigo: HttpStatus.EXPECTATION_FAILED,
          codigoNombre: erroresCuenta.TRANSACCION_NO_VALIDA,
        };
      }

      const informacionPago = transaccionQuery.informacionPago;
      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithID(
        transaccionQuery.usuario._id,
      );

      if (getUsuario.estado === this.inactivaPago) {
        // ___________________________________________ Verificar Pago OK____________________________________________
        // pago Stripe
        let verificarCuentaStripe;
        if (transaccionQuery.metodoPago === this.codigoMetodoPagoStripe) {
          verificarCuentaStripe = await this.gestionPagoStripeService.obtenerEstadoPagoStripe(
            informacionPago.idPago,
          );
          if (!verificarCuentaStripe) {
            throw {
              codigo: HttpStatus.EXPECTATION_FAILED,
              codigoNombre: erroresCuenta.ERROR_REGISTRO_PAGO,
            };
          }
        }

        if (verificarCuentaStripe) {
          const detallePagoStripe = await this.gestionPagoStripeService.obtenerDetallePagoStripe(
            informacionPago.idPago,
          );
          detallePagoStripe.amount = detallePagoStripe.amount / 100;
          detallePagoStripe.fee = detallePagoStripe.fee / 100;
          detallePagoStripe.net = detallePagoStripe.net / 100;

          // actualizar usuario
          transaccionDatabase = await session.withTransaction(async () => {
            const updateUser = await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
              getUsuario._id,
              this.activaNoVerificado,
              opts,
            );
            if (!updateUser) {
              await session.abortTransaction();
              return;
            }
          }, transactionOptions);

          // ______________________________actualizar transaccion (dos origenes)________________________________
          for (const transaccion of getUsuario.transacciones) {
            //__________________Calcular valor total por transaccion________________
            const getNewValor = this.gestionPagoStripeService.calcularValorTotalComisionStripe(
              detallePagoStripe.amount,
              transaccion.monto,
              detallePagoStripe.fee,
            );

            const dataUpdate: UpdateTransaccionPagoDto = {
              comisionTransferencia: getNewValor.newFee,
              totalRecibido: getNewValor.newAmountReceived,
              origenPais: detallePagoStripe.country,
            };

            if (
              transaccion.informacionPago.idPago ===
              transaccionQuery.informacionPago.idPago
            ) {
              transaccionDatabase = await session.withTransaction(async () => {
                const updatetransaccion = await this.actualizarTransaccionService.actualizarTransaccion(
                  transaccion._id,
                  opts,
                  dataUpdate,
                );
                if (!updatetransaccion) {
                  await session.abortTransaction();
                  return;
                }
              }, transactionOptions);
            }

            //__________________Actualizar conversion transaccion________________
            transaccionDatabase = await session.withTransaction(async () => {
              const updateConversion = await this.actualizarConversionTransaccionService.actualizarConversionStripe(
                transaccion._id,
                dataUpdate,
                opts,
              );
              if (!updateConversion) {
                await session.abortTransaction();
                return;
              }
            }, transactionOptions);

            //___________________________________ actualizar suscripcion__________________________
            if (transaccion.origen === catalogoOrigen.suscripciones) {
              transaccionDatabase = await session.withTransaction(async () => {
                const updateSuscripcion = await this.actualizarSuscripcionService.actualizarSuscripcion(
                  transaccion._id,
                  getUsuario._id,
                  opts,
                );
                if (!updateSuscripcion) {
                  await session.abortTransaction();
                  return;
                }
              }, transactionOptions);
            }
          }

          const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(
            getUsuario.idioma,
          );

          const login = await this.loginService.login(
            getUsuario,
            idioma,
            dispositivo,
            opts,
          );

          //________________________________enviar email________________________________________________________

          const dataEmail: any = {
            usuario: getUsuario._id,
            menorEdad: getUsuario.menorEdad,
            idioma: idiomaUsuario.codNombre,
            nombre: getUsuario.perfiles[0].nombre,
          };

          if (getUsuario.menorEdad) {
            const codTipEmaR = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
              'validacionResponsable',
            );
            let codEstadoValiRes = codTipEmaR.codigo;
            dataEmail.codigo = codEstadoValiRes;
            dataEmail.emailDestinatario = getUsuario.emailResponsable;

            await this.crearEmail.crearEmail(dataEmail);

            await this.crearEmailMenorEdadService.crearEmailMenorEdad(
              getUsuario,
            );
          } else {
            const codTipEma = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
              'validacion',
            );
            let codTipoEmailValid = codTipEma.codigo;
            dataEmail.menorEdad = false;
            dataEmail.emailDestinatario = getUsuario.email;
            dataEmail.codigo = codTipoEmailValid;

            await this.crearEmail.crearEmail(dataEmail);
          }

          //_______________________________enviar recibo pago___________________________________
          const dataEmailPago = {
            idInformacionPago: informacionPago._id as string,
            usuario: getUsuario._id as string,
            nombre: getUsuario.perfiles[0].nombre as string,
            emailDestinatario: getUsuario.email as string,
            idioma: idiomaUsuario.codNombre as string,
          };
          await this.crearEmailReciboPagoService.crearEmailReciboPago(
            dataEmailPago,
          );


          await this.crearEmailBienvenidaService.crearEmailBienvenida(
            getUsuario._id,
            opts,
          );

          const result = {
            usuario: login.usuario,
            tokenAccess: login.accessToken,
            tokenRefresh: login.tokenRefresh,
          };

          // proceso exitoso
          return result;
        }
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }

  validarCodigosMetodosPago(codigo): Boolean {
    const codigos = [
      codigosMetodosPago.stripe,
      codigosMetodosPago.payments1,
      codigosMetodosPago.payments2,
      codigosMetodosPago.panama,
      codigosMetodosPago.cripto,
    ];
    if (codigos.indexOf(codigo) !== -1) {
      return true;
    } else {
      return false;
    }
  }
}
