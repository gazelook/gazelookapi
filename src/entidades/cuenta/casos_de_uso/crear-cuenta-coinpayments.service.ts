import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { CrearEmailBienvenidaService } from 'src/entidades/emails/casos_de_uso/crear-email-bienvenida.service';
import { LoginService } from 'src/entidades/login/casos_de_uso/login.service';
import { CrearPagoCoinpaymentezService } from 'src/entidades/transaccion/casos_de_uso/crear-pago-coinpaymentez.service';
import { listaMonedasCripto } from 'src/money/enum-lista-money';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { Direccion } from '../../../drivers/mongoose/interfaces/direccion/direccion.interface';
import { Perfil } from '../../../drivers/mongoose/interfaces/perfil/perfil.interface';
import { Telefono } from '../../../drivers/mongoose/interfaces/telefonos/telefono.interface';
import { Usuario } from '../../../drivers/mongoose/interfaces/usuarios/usuario.interface';
import { erroresCuenta, erroresPerfil } from '../../../shared/enum-errores';
import {
  codigosCatalogoSuscripcion,
  codigosMetodosPago,
  estadosTransaccion,
  estadosUsuario
} from '../../../shared/enum-sistema';
import { CrearAlbumService } from '../../album/casos_de_uso/crear-album.service';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearDireccionService } from '../../direccion/casos_de_uso/crear-direccion.service';
import { CrearTelefonoService } from '../../direccion/casos_de_uso/crear-telefono.service';
import { CrearDireccionDto } from '../../direccion/dtos/crear-direccion.dto';
import { CrearDocumentoUsuarioService } from '../../documentos_usuario/casos_de_uso/crear-documento-usuario.service';
import { ContactnameUnicoPerfilService } from '../../perfil/casos_de_uso/contactname-unico-perfil.service';
import { CrearPerfilService } from '../../perfil/casos_de_uso/crear-perfil.service';
import { GestionRolService } from '../../rol/casos_de_uso/gestion-rol.service';
import { GenerarNuevoPagoService } from '../../transaccion/casos_de_uso/generar-nuevo-pago.service';
import { CrearUsuarioService } from '../../usuario/casos_de_uso/crear-usuario.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { CrearCuentaCoinpaymentsDto } from '../entidad/cuenta-coinpayments.dto';
import { CrearCuentaDto } from '../entidad/cuenta.dto';
import { RetornoCuentaCoinpaymentsDto, RetornoCuentaDto } from '../entidad/retorno-cuenta.dto';
import { GestionCuentaService } from './gestion-cuenta.service';

@Injectable()
export class CrearCuentaCoinpaymentsService {
  constructor(
    private crearUsuarioService: CrearUsuarioService,
    private crearPerfilService: CrearPerfilService,
    private crearAlbumService: CrearAlbumService,
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private crearDireccionService: CrearDireccionService,
    private crearTelefonoService: CrearTelefonoService,
    private gestionRolService: GestionRolService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private contactnameUnicoPerfilService: ContactnameUnicoPerfilService,
    private generarNuevoPagoService: GenerarNuevoPagoService,
    private crearPagoCoinpaymentezService: CrearPagoCoinpaymentezService,
    private gestionCuentaService: GestionCuentaService,
    private crearDocumentoUsuarioService: CrearDocumentoUsuarioService,
    private crearEmailBienvenidaService: CrearEmailBienvenidaService,
    private loginService: LoginService
  ) { }

  async crearCuenta(
    dataCuenta: CrearCuentaCoinpaymentsDto,
    idioma,
    dispositivo
  ): Promise<RetornoCuentaDto> {
    const obtenerIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      idioma,
    );
    const usuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithTransaccionByEmail(
      dataCuenta.email,
    );

    

    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      const opts = { session };

      if (usuario)
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresCuenta.USUARIO_YA_REGISTRADO,
        };
        
      //_____________________generar nuevo pago__________________
      // si el usuario existe y tiene un pago pendiente,
      // verificamos en stripe ó paypal si el pago se ha hecho efectivo
      // si no se ha hecho efectivo el pago
      // se borra la cuenta o le redirigimos a la ventana de realizar pago

      const nuevoPago = await this.verifivarPagoUsuario(
        usuario,
        dataCuenta,
        opts,
      );
      if (nuevoPago) {
        const datosNuevoPago: RetornoCuentaDto = {
          idPago: nuevoPago.idPago,
          idTransaccion: nuevoPago.idTransaccion,
          usuario: {
            _id: usuario._id,
            email: usuario.email,
            estado: {
              codigo: usuario.estado,
            },
          },
        };
        // actualizar el nuevo pago
        await session.commitTransaction();
        session.endSession();

        return datosNuevoPago;
      }
      //_____________________cuenta: proceso normal __________________
      

      const idUsuario = new mongoose.mongo.ObjectId();
      let saveDireccionUsuario: Direccion;

      // direccion usuario
      const dataDireccUsuario = dataCuenta.direccion;

      const direccionUsuario: CrearDireccionDto = {
        traducciones: dataDireccUsuario.traducciones,
        //localidad: dataDireccUsuario.localidad.codigo,
        pais: dataDireccUsuario.pais.codigo,
        latitud: null,
        longitud: null,
      };
      saveDireccionUsuario = await this.crearDireccionService.crearDireccion(
        direccionUsuario,
        opts,
      );

      let dataUsuario: any = {
        _id: idUsuario,
        email: dataCuenta.email,
        contrasena: dataCuenta.contrasena,
        fechaNacimiento: dataCuenta.fechaNacimiento,
        aceptoTerminosCondiciones: dataCuenta.aceptoTerminosCondiciones,
        perfilGrupo: dataCuenta.perfilGrupo,
        emailVerificado: false,
        menorEdad: dataCuenta.menorEdad,
        idioma: obtenerIdioma.codigo,
        direccion: saveDireccionUsuario._id,
        direccionDomiciliaria: dataCuenta.direccionDomiciliaria,
        documentoIdentidad: dataCuenta.documentoIdentidad,
        estado: estadosUsuario.cripto
      };

      if (dataUsuario.menorEdad) {
        dataUsuario.emailResponsable = dataCuenta.emailResponsable;
        dataUsuario.nombreResponsable = dataCuenta.nombreResponsable;
        dataUsuario.responsableVerificado = false;
      }

      //verificar que haya aceptado los terminos y condiciones
      if (!dataUsuario.aceptoTerminosCondiciones) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresCuenta.ERROR_TERMINOS_CONDICIONES,
        };
      }
      //verificar perfil
      const listaTipoPerfil: Array<string> = [];
      dataCuenta.perfiles.forEach(element => {
        listaTipoPerfil.push(element.tipoPerfil.codigo);
      });
      const perfilUnico = await this.crearPerfilService.verificarPerfilUnico(
        listaTipoPerfil,
      );
      if (!perfilUnico) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresPerfil.PERFIL_NO_VALIDO,
        };
      }
      // verificar nombre de contacto de perfil
      for (const perfil of dataCuenta.perfiles) {
        const nombreContactoUnico = await this.contactnameUnicoPerfilService.nombreContactoUnico(
          perfil.nombreContacto,
          'false',
        );
        if (!nombreContactoUnico) {
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresPerfil.ERROR_NOMBRE_CONTACTO,
          };
        }
      }
      // validar codigos metodos pago
      if (
        !this.gestionCuentaService.validarCodigosMetodosPago(
          dataCuenta.metodoPago.codigo,
        )
      ) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresCuenta.ERROR_METODO_PAGO,
        };
      }

      //_____________guardar usuario_______________
      const saveUser = await this.crearUsuarioService.crearUsuario(
        dataUsuario,
        opts,
      );

      const perfiles = dataCuenta.perfiles;
      const idPerfiles: Array<string> = [];
      let idAlbunes: Array<string> = [];
      const idDireccion: Array<string> = [];
      const idTelefono: Array<string> = [];
      // _______________perfil____________________
      let savePerfil: Perfil;
      let saveAlbum: Album;
      let saveDireccion: Direccion;
      let saveTelefono: Telefono;

      for (const perfil of perfiles) {
        idAlbunes = [];
        const dataPerfil = {
          tipoPerfil: perfil.tipoPerfil.codigo,
          usuario: saveUser._id,
          nombreContacto: perfil.nombreContacto,
          nombreContactoTraducido: perfil.nombreContactoTraducido,
          nombre: perfil.nombre,
        };
        //guardar perfil
        savePerfil = await this.crearPerfilService.crearPerfil(
          dataPerfil,
          null,
          opts,
        );

        idPerfiles.push(savePerfil._id);
        //______________album______________________
        if (perfil?.album && perfil?.album.length > 0) {
          const listaAlbum = perfil.album;
          for (const album of listaAlbum) {
            const dataAlbum = {
              nombre: album.nombre, // para guardar en la traduccion del album
              tipo: album.tipo.codigo,
              media: album.media,
              portada: album.portada,
              usuario: saveUser._id,
              idioma: obtenerIdioma.codNombre,
            };

            // guardar album
            saveAlbum = await this.crearAlbumService.crearAlbum(
              dataAlbum,
              opts,
            );
            idAlbunes.push(saveAlbum._id);
          }
        }
        //guardar referencia idAlbunes en Perfil
        await this.perfilModel.findOneAndUpdate(
          { _id: savePerfil._id },
          { $set: { album: idAlbunes } },
          opts,
        );

        // _________________direccion_____________________
        if (perfil.direcciones && perfil.direcciones.length > 0) {
          const listaDireccion = perfil.direcciones;
          for (const direccionPerfil of listaDireccion) {
            console.log('direccionPerfil-------->: ', direccionPerfil);
            console.log(
              'direccionPerfil.pais.codigo: ',
              direccionPerfil.pais.codigo,
            );
            const dataDireccionPerfil = {
              latitud: null,
              longitud: null,
              traducciones: direccionPerfil.traducciones,
              //localidad: direccionPerfil.localidad.codigo,
              pais: direccionPerfil.pais.codigo,
              usuario: savePerfil._id,
            };

            saveDireccion = await this.crearDireccionService.crearDireccion(
              dataDireccionPerfil,
              opts,
            );
            idDireccion.push(saveDireccion._id);
          }
        }
        //guardar referencia idDirecciones en Perfil
        await this.perfilModel.findOneAndUpdate(
          { _id: savePerfil._id },
          { $set: { direcciones: idDireccion } },
          opts,
        );

        // ____________telefono_________________
        const listaTelefono = perfil.telefonos;
        if (listaTelefono && listaTelefono.length > 0) {
          for (let telefono of listaTelefono) {
            console.log(
              'telefono.pais.codigo--------------->: ',
              telefono.pais.codigo,
            );
            const dataTelefono = {
              numero: telefono.numero,
              pais: telefono.pais.codigo,
              usuario: savePerfil._id,
            };

            saveTelefono = await this.crearTelefonoService.crearTelefono(
              dataTelefono,
              opts,
            );
            idTelefono.push(saveTelefono._id);
          }
          //guardar referencia idTelefonos en Perfil
          await this.perfilModel.findOneAndUpdate(
            { _id: savePerfil._id },
            { $set: { telefonos: idTelefono } },
            opts,
          );
        }
      }

      //guardar referencia idPerfiles en Usuario
      const userOk = await this.userModel.findOneAndUpdate(
        { _id: saveUser._id },
        { $set: { perfiles: idPerfiles } },
        opts,
      );

      //asignar rol de usuario normal
      const rolUsuarioNormal = this.gestionRolService
        .CODIGO_ROL_USUARIO_SISTEMA;
      const getRol = await this.gestionRolService.obtenerRolSistemaByCodRol(
        rolUsuarioNormal,
      );
      const addRolUser = await this.userModel.findOneAndUpdate(
        { _id: saveUser._id },
        { $set: { rolSistema: [getRol[0]._id] } },
        opts,
      );

      // _____________________ datos de pago _________________
      
      // dataCuenta.pagoCripto.simboloCripto;
      const currency2 = dataCuenta.pagoCripto.simboloCripto;
      console.log('currency2: ', currency2)

      let datosPago: any = {
        email: dataCuenta.email,
        idUsuario: saveUser._id,
        metodoPago: dataCuenta.metodoPago.codigo,
        tipoMonedaRegistro: dataCuenta.monedaRegistro.codNombre,
        currency2: currency2,
        currency1: listaMonedasCripto.BTC
      };

      if (dataCuenta.menorEdad) {
        datosPago.email = dataCuenta.emailResponsable;
      }

      if (dataCuenta.datosFacturacion) {
        let total = 0;
        for (let transaccion of dataCuenta.transacciones) {

          total += transaccion.monto;
        }

        datosPago.nombres = dataCuenta.datosFacturacion.nombres;
        datosPago.telefono = dataCuenta.datosFacturacion.telefono;
        datosPago.direccion =
          dataCuenta.datosFacturacion?.direccion ||
          saveDireccionUsuario.traducciones[0].descripcion;
        datosPago.monto = total;
        datosPago.transacciones = dataCuenta.transacciones;
        datosPago.tipoSuscripcion = codigosCatalogoSuscripcion.anual;
        datosPago.creacionCuenta = true;
        datosPago.pagoCripto = dataCuenta.pagoCripto
      }

      // ___________________________________________Crear Pago____________________________________________
      let cuentaOk: RetornoCuentaCoinpaymentsDto = {};

      if (codigosMetodosPago.cripto === dataCuenta.metodoPago.codigo) {
        const pagoCoinpayments = await this.crearPagoCoinpaymentezService.crearPagoCoinpaymentez(
          datosPago,
          opts,
        );


        cuentaOk.coinpayments = pagoCoinpayments.coinpayments;
        cuentaOk.idTransaccion = pagoCoinpayments.idTransaccion;
      }
      console.log('TERMINAAAAAAAAAA PAGOOOOOOOOOOOOOOOOOOO CRIPTO');

      //_______________________________________Crear documentos Usuario__________________________________

      if (dataCuenta?.documentosUsuario) {
        for (const documento of dataCuenta.documentosUsuario) {
          const docUser = {
            usuario: saveUser._id,
            tipo: documento.tipo.codigo,
            archivo: documento.archivo._id,
          };
          const docSaveUser = await this.crearDocumentoUsuarioService.crearDocumentoUsuario(
            docUser,
            opts,
          );
          await this.userModel.updateOne(
            { _id: saveUser._id },
            { $push: { documentosUsuario: docSaveUser._id } },
            opts,
          );
        }
      }

      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithID(
        saveUser._id,
        opts,
      );

      const login = await this.loginService.login(
        getUsuario,
        null,
        dispositivo,
        opts,
      );

      // const result = {
      //   usuario: login.usuario,
      //   tokenAccess: login.accessToken,
      //   tokenRefresh: login.tokenRefresh,
      // };
      cuentaOk.usuario = login.usuario
      cuentaOk.tokenAccess = login.accessToken
      cuentaOk.tokenRefresh = login.tokenRefresh
      // await this.crearEmailBienvenidaService.crearEmailBienvenida(
      //   saveUser._id,
      //   opts,
      // );

      // proceso exitoso
      await session.commitTransaction();
      session.endSession();

      return cuentaOk;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    } finally {
      session.endSession();
    }
  }

  // validar y generar nuevo pago
  async verifivarPagoUsuario(
    usuarioRegistrado: Usuario,
    dataCuenta: CrearCuentaDto,
    opts: any,
  ): Promise<RetornoCuentaDto> {
    try {
      let nuevoPago;
      if (
        usuarioRegistrado?.estado === estadosUsuario.inactivaPago &&
        usuarioRegistrado.transacciones[0]?.estado ===
        estadosTransaccion.pendiente
      ) {
        // if (dataCuenta.metodoPago.codigo === codigosMetodosPago.paypal) {
        //   nuevoPago = await this.generarNuevoPagoService.nuevoPagoPaypal(usuarioRegistrado.transacciones, dataCuenta, opts);
        // }
        if (dataCuenta.metodoPago.codigo === codigosMetodosPago.stripe) {
          nuevoPago = await this.generarNuevoPagoService.nuevoPagoStripe(
            usuarioRegistrado,
            dataCuenta,
            opts,
          );
        }
        return nuevoPago;
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }
}
