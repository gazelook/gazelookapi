import { LoginServicesModule } from './../../login/casos_de_uso/login-services.module';
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CrearCuentaService } from './crear-cuenta.service';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { PerfilServiceModule } from '../../perfil/casos_de_uso/perfil.services.module';
import { CuentaProviders } from '../drivers/cuenta.provider';
import { EmailServicesModule } from '../../emails/casos_de_uso/email-services.module';
import { TransaccionServicesModule } from '../../transaccion/casos_de_uso/transaccion-services.module';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { DireccionServiceModule } from '../../direccion/casos_de_uso/direccion.services.module';
import { AlbumServiceModule } from '../../album/casos_de_uso/album.services.module';
import { GestionCuentaService } from './gestion-cuenta.service';
import { LoginModule } from '../../login/login.module';
import { RolServiceModule } from '../../rol/casos_de_uso/rol.services.module';
import { RollbackCreacionCuentaService } from './rollback-creacion-cuenta.service';
import { StorageModule } from '../../../drivers/amazon_s3/storage.module';
import { RenovarSuscripcionService } from './renovar-suscripcion.service';
import { SuscripcionServicesModule } from '../../suscripcion/casos_de_uso/suscripcion-services.module';
import { ValidarRenovacionCuentaService } from './validar-renovacion.service';
import { PaisServicesModule } from '../../pais/casos_de_uso/pais.services.module';
import { DocumentoUsuarioServicesModule } from '../../documentos_usuario/casos_de_uso/documento-usuario.services.module';
import { ValidarCuentaPaymentezService } from './validar-cuenta-paymentez.service';
import { RollbackCreacionCuentaPaymentezService } from './rollback-creacion-cuenta-paymentez.service';
import { CrearCuentaPaymentezService } from './crear-cuenta-paymentez.service';
import { CrearCuentaCoinpaymentsService } from './crear-cuenta-coinpayments.service';
import { ValidarCuentaCoinpaymentsService } from './validar-cuenta-coinpayments.service';
import { CoinpaymentezModule } from 'src/drivers/coinpaymentez/coinpaymentez.module';
import { RollbackCreacionCuentaCoinpaymentsService } from './rollback-creacion-cuenta-coinpayments.service';

@Module({
  imports: [
    DBModule,
    HttpModule,
    forwardRef(() => UsuarioServicesModule),
    PerfilServiceModule,
    EmailServicesModule,
    forwardRef(() => TransaccionServicesModule),
    CatalogosServiceModule,
    DireccionServiceModule,
    AlbumServiceModule,
    LoginModule,
    RolServiceModule,
    LoginServicesModule,
    StorageModule,
    SuscripcionServicesModule,
    PaisServicesModule,
    DocumentoUsuarioServicesModule,
    CoinpaymentezModule
  ],
  providers: [
    ...CuentaProviders,
    CrearCuentaService,
    GestionCuentaService,
    RollbackCreacionCuentaService,
    RenovarSuscripcionService,
    ValidarRenovacionCuentaService,
    ValidarCuentaPaymentezService,
    RollbackCreacionCuentaPaymentezService,
    CrearCuentaPaymentezService,
    CrearCuentaCoinpaymentsService,
    ValidarCuentaCoinpaymentsService,
    RollbackCreacionCuentaCoinpaymentsService
  ],
  exports: [
    CrearCuentaService,
    GestionCuentaService,
    RollbackCreacionCuentaService,
    RenovarSuscripcionService,
    ValidarRenovacionCuentaService,
    ValidarCuentaPaymentezService,
    RollbackCreacionCuentaPaymentezService,
    CrearCuentaPaymentezService,
    CrearCuentaCoinpaymentsService,
    ValidarCuentaCoinpaymentsService,
    RollbackCreacionCuentaCoinpaymentsService
  ],
  controllers: [],
})
export class CuentaServiceModule {}
