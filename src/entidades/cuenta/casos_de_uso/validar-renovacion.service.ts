import { HttpStatus, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { erroresCuenta } from '../../../shared/enum-errores';
import {
  catalogoOrigen,
  codigosMetodosPago,
  estadosUsuario,
} from '../../../shared/enum-sistema';
import { ActualizarSuscripcionService } from '../../suscripcion/casos_de_uso/actualizar-suscripcion.service';
import { ActualizarTransaccionService } from '../../transaccion/casos_de_uso/actualizar-transaccion.service';
import { GestionPagoPaypalService } from '../../transaccion/casos_de_uso/gestion-pago-paypal.service';
import { GestionPagoStripeService } from '../../transaccion/casos_de_uso/gestion-pago-stripe.service';
import { ObtenerTransaccionService } from '../../transaccion/casos_de_uso/obtener-transaccion.service';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';

@Injectable()
export class ValidarRenovacionCuentaService {
  inactivaPago = estadosUsuario.inactivaPago;
  activaNoVerificado = estadosUsuario.activaNoVerificado;
  codigoMetodoPagoStripe = codigosMetodosPago.stripe;
  // codigoMetodoPagoPaypal = codigosMetodosPago.paypal;

  constructor(
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private gestionPagoStripeService: GestionPagoStripeService,
    private gestionPagoPaypalService: GestionPagoPaypalService,
    private actualizarSuscripcionService: ActualizarSuscripcionService,
  ) {}

  async validarRenovacionCuenta(idTransaccion): Promise<any> {
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      const transaccionQuery: any = await this.obtenerTransaccionService.obtenerTransaccionById(
        idTransaccion,
      );
      if (!transaccionQuery) {
        throw {
          codigo: HttpStatus.EXPECTATION_FAILED,
          codigoNombre: erroresCuenta.TRANSACCION_NO_VALIDA,
        };
      }

      const informacionPago = transaccionQuery.informacionPago;
      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithTransaccion(
        transaccionQuery.usuario._id,
      );

      // ___________________________________________ Verificar Pago OK____________________________________________
      // pago Stripe
      let verificarCuentaStripe;
      if (transaccionQuery.metodoPago === this.codigoMetodoPagoStripe) {
        verificarCuentaStripe = await this.gestionPagoStripeService.obtenerEstadoPagoStripe(
          informacionPago.idPago,
        );
        if (!verificarCuentaStripe) {
          throw {
            codigo: HttpStatus.EXPECTATION_FAILED,
            codigoNombre: erroresCuenta.ERROR_REGISTRO_PAGO,
          };
        }
      }
      // pago Paypal
      // let verificarCuentaPaypal;
      // if (transaccionQuery.metodoPago === this.codigoMetodoPagoPaypal) {

      //   verificarCuentaPaypal = await this.gestionPagoPaypalService.obtenerEstadoPagoPaypal(informacionPago.idPago)
      //   if (!verificarCuentaPaypal) {
      //     throw { codigo: HttpStatus.EXPECTATION_FAILED, codigoNombre: erroresCuenta.ERROR_REGISTRO_PAGO };
      //   }
      // }
      if (
        verificarCuentaStripe
        // || verificarCuentaPaypal
      ) {
        // actualizar usuario
        if (getUsuario.estado === this.inactivaPago) {
          await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
            getUsuario._id,
            estadosUsuario.activa,
            opts,
          );
        }

        // actualizar transaccion (origenes)
        for (const transaccion of getUsuario.transacciones) {
          // verificar el mismo id de pago
          if (
            transaccion.informacionPago.idPago ===
            transaccionQuery.informacionPago.idPago
          ) {
            await this.actualizarTransaccionService.actualizarTransaccion(
              transaccion._id,
              opts,
            );

            // actualizar suscripcion
            if (transaccion.origen === catalogoOrigen.suscripciones) {
              await this.actualizarSuscripcionService.actualizarSuscripcion(
                transaccion._id,
                getUsuario._id,
                opts,
              );
            }
          }
        }

        await session.commitTransaction();
        session.endSession();

        // proceso exitoso
        return true;
      }
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
