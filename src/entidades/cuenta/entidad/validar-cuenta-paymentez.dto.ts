import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsMongoId,
  IsNotEmpty,
  IsNotEmptyObject,
  IsObject,
  IsOptional,
  ValidateNested,
} from 'class-validator';
import { TransaccionCuentaDto } from 'src/entidades/transaccion/entidad/transaccion.dto';
import { DatosFacturacion, MetodoPago, MonedaRegistro } from './cuenta.dto';

export class ValidarCuentaPaymentezDto {
  //metodo de pago
  @ApiProperty({ type: MetodoPago })
  @IsNotEmpty()
  metodoPago: MetodoPago;

  //monto de pago en diferentes monedas
  @ApiProperty({ type: [TransaccionCuentaDto] })
  @IsNotEmpty()
  @ValidateNested({ each: true })
  transacciones: TransaccionCuentaDto[];

  //pago
  @ApiProperty({ type: DatosFacturacion })
  @IsOptional()
  @IsObject() //@IsInstance(DatosPago)
  datosFacturacion: DatosFacturacion;

  @ApiProperty({ type: MonedaRegistro })
  @IsObject()
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => MonedaRegistro)
  monedaRegistro: MonedaRegistro;

  //email
  @ApiProperty({ type: String })
  email: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  pagoNoConfirmado: boolean;

  //autorizacionCodePaymentez
  @ApiProperty({ type: String })
  autorizacionCodePaymentez: string;
  
}
