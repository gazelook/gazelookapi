import { ApiProperty } from '@nestjs/swagger';

export class EstadoRetornoCuentaDto {
  codigo: string;
}

export class UsuarioRetornoCuentaDto {
  _id: string;
  email: string;
  estado: EstadoRetornoCuentaDto;
}

export class RetornoCuentaDto {
  @ApiProperty()
  idPago?: string;
  @ApiProperty()
  idTransaccion?: string;
  @ApiProperty()
  usuario?: UsuarioRetornoCuentaDto;
}

export class RetornoCuentaCoinpaymentsDto {
  @ApiProperty()
  coinpayments?: any;
  @ApiProperty()
  idTransaccion?: string;
  @ApiProperty()
  usuario?: any;
  @ApiProperty()
  tokenAccess?: any;
  @ApiProperty()
  tokenRefresh?: any;
}
