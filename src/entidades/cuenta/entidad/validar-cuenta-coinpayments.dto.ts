import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty
} from 'class-validator';

export class ValidarCuentaCoinpaymentsDto {
  //metodo de pago
  @ApiProperty({ type: String })
  @IsNotEmpty()
  idTransaccion: string;

  //monto de pago en diferentes monedas
  @ApiProperty({ type: String })
  @IsNotEmpty()
  email: string;
  
}
