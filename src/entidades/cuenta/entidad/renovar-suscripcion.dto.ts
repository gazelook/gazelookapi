import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty, IsObject, IsOptional } from 'class-validator';
import { TransaccionCuentaDto } from '../../transaccion/entidad/transaccion.dto';
import { DatosFacturacion, MetodoPago } from './cuenta.dto';

export class TipoSuscripcionDto {
  @ApiProperty({ description: 'codigo tipo suscripcion: anual, mensual' })
  @IsNotEmpty()
  codigo: string;
}

export class SuscripcionDto {
  @ApiProperty({ type: TipoSuscripcionDto })
  @IsNotEmpty()
  tipo: TipoSuscripcionDto;
}

export class RenovarSuscripcionDto {
  @ApiProperty({ description: 'Id del usuario' })
  @IsNotEmpty()
  @IsMongoId()
  _id: string;

  @ApiProperty({ type: MetodoPago })
  @IsNotEmpty()
  metodoPago: MetodoPago;

  @ApiProperty({
    type: Number,
    description: 'tiempo en años: 1 || 2 || 3 (aplica solo cuando es anual)',
  })
  @IsOptional()
  tiempo: number;

  @ApiProperty({ type: SuscripcionDto })
  @IsNotEmpty()
  suscripcion: SuscripcionDto;

  //pago
  @ApiProperty({ type: DatosFacturacion })
  @IsOptional()
  @IsObject() //@IsInstance(DatosPago)
  datosFacturacion: DatosFacturacion;

  //monto de pago en diferentes monedas
  @ApiProperty({ type: [TransaccionCuentaDto] })
  @IsNotEmpty()
  transacciones: TransaccionCuentaDto[];
}
