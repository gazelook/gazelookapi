import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  ArrayNotEmpty,
  IsArray,
  IsBoolean,
  IsEmail,
  IsMongoId,
  IsNotEmpty,
  IsNotEmptyObject,
  IsObject,
  IsOptional,
  IsString,
  Matches,
  ValidateNested,
} from 'class-validator';
import { TransaccionCuentaDto } from 'src/entidades/transaccion/entidad/transaccion.dto';
import { CatalogoLocalidadDto } from '../../pais/entidad/catalogo-localidad.dto';

export class CatalogoTipoDocumentoUsuario {
  @ApiProperty({
    description: 'Código catálogo tipo documento',
    required: true,
    example: 'TIPDOCUSER_01: usuario | TIPDOCUSER_02: representante ',
  })
  @IsString()
  codigo: string;
}

export class ArchivoDocumento {
  @ApiProperty()
  @IsMongoId()
  _id: string;
}

export class DocumentoUsuarioCtaDto {
  @ApiProperty()
  @IsNotEmptyObject()
  @IsObject()
  @ValidateNested()
  @Type(() => CatalogoTipoDocumentoUsuario)
  tipo!: CatalogoTipoDocumentoUsuario;

  @ApiProperty()
  @IsNotEmptyObject()
  @IsObject()
  @ValidateNested()
  @Type(() => ArchivoDocumento)
  archivo: ArchivoDocumento;
}

export class TraduccionesMedia {
  @ApiProperty()
  @IsOptional()
  descripcion: string;
}

export class Media {
  @ApiProperty({ description: 'id de la media' })
  @IsNotEmpty()
  @IsMongoId()
  _id?: string;
  @ApiProperty({
    type: TraduccionesMedia,
    description: 'descripcion de la media',
  })
  @IsOptional()
  traducciones?: [TraduccionesMedia];
}
export class TipoPerfil {
  @ApiProperty({
    description: 'Código catálogo tipo perfil',
    required: true,
    example: 'TIPERFIL_2 | TIPERFIL_1 ',
  })
  @IsNotEmpty()
  codigo: string;
}
export class Idioma {
  @ApiProperty({
    description: 'Código catálogo idiomas',
    required: true,
    example: 'IDI_1 español | IDI_2 ingles ',
  })
  @IsNotEmpty()
  codigo: string;
}
export class MetodoPago {
  @ApiProperty({
    description: 'Código catálogo método de pago',
    required: true,
    example: 'METPAG_1:stripe | METPAG_3:paypal ',
  })
  @IsNotEmpty()
  codigo: string;
}
export class Localidad {
  @ApiProperty({
    description: 'Código catálogo Localidad',
    required: true,
    example: 'LOC_ebc4898c-ca34-49b4-95f3-7be466747372',
  })
  @IsNotEmpty()
  codigo: string;
}
export class Pais {
  @ApiProperty({
    description: 'Código catálgo país',
    required: true,
    example: 'PAI_31',
  })
  @IsNotEmpty()
  codigo: string;
}
export class TipoAlbum {
  @ApiProperty({
    description: 'Código catálogo tipo album',
    required: true,
    example: 'CATALB_1 | CATALB_2',
  })
  @IsNotEmpty()
  codigo?: string;
}

export class traduccionDireccion {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  descripcion: string;
}

export class Direccion {
  @ApiProperty()
  _id?: string;

  @ApiProperty()
  latitud?: number;

  @ApiProperty()
  longitud?: number;

  @ApiProperty({ type: [traduccionDireccion], required: true })
  @IsNotEmpty()
  traducciones?: Array<traduccionDireccion>;

  @ApiProperty({ type: CatalogoLocalidadDto })
  @IsOptional()
  localidad?: CatalogoLocalidadDto;
  @ApiProperty({ type: Pais })
  @IsOptional()
  pais?: Pais;
}
export class Telefono {
  @ApiProperty()
  @IsNotEmpty()
  numero: string;
  @ApiProperty({ type: Pais })
  @IsNotEmpty()
  pais: Pais;
}

export class Album {
  @ApiProperty({ required: false })
  nombre?: string;
  @ApiProperty({ type: TipoAlbum })
  @IsNotEmpty()
  tipo?: TipoAlbum;
  @ApiProperty({ type: [Media] })
  @IsNotEmpty()
  media?: Media[];
  @ApiProperty({ type: Media })
  portada?: Media;
  @ApiProperty()
  predeterminado?: boolean;
}

export class PerfilCuenta {
  @ApiProperty()
  @IsNotEmpty()
  nombreContacto: string;
  @ApiProperty()
  nombreContactoTraducido: string;
  @ApiProperty()
  @IsNotEmpty()
  nombre: string;
  @ApiProperty({ type: TipoPerfil })
  @IsNotEmpty()
  tipoPerfil: TipoPerfil;
  @ApiProperty({ type: [Album] })
  @IsNotEmpty()
  @ArrayNotEmpty()
  album: Album[];
  @ApiProperty({ type: [Telefono] })
  @IsOptional()
  @ArrayNotEmpty()
  telefonos: Telefono[];
  @ApiProperty({ type: [Direccion] })
  @IsNotEmpty()
  @ArrayNotEmpty()
  direcciones: Direccion[];
}

export class DatosFacturacion {
  @ApiProperty()
  @IsNotEmpty()
  nombres: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;
  @ApiProperty()
  telefono: string;
  @ApiProperty()
  idPago: string;
  @ApiProperty()
  @IsNotEmpty()
  direccion: string;
  @ApiProperty({ description: 'Monto a pagar de la suscripción' })
  @IsNotEmpty()
  monto: number;
}

export class MonedaRegistro {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  codNombre: string;
}

export class CrearCuentaDto {
  //usuario
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail() //@Matches(/^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,4})+$/, { message: 'No valid Email!!' })
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  contrasena: string;
  @ApiProperty()
  @IsOptional()
  fechaNacimiento: Date;
  @ApiProperty()
  @IsNotEmpty()
  aceptoTerminosCondiciones: boolean;
  @ApiProperty()
  perfilGrupo: boolean;
  @ApiProperty()
  @IsNotEmpty()
  @IsBoolean()
  menorEdad: boolean;
  @ApiProperty()
  @IsOptional()
  @IsEmail()
  @Matches(/^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,4})+$/, {
    message: 'No valid Email!!',
  })
  emailResponsable: string;
  @IsOptional()
  @IsNotEmpty()
  nombreResponsable: string;

  //perfil
  @ApiProperty({ type: [PerfilCuenta] })
  @ArrayNotEmpty()
  perfiles: PerfilCuenta[];

  //metodo de pago
  @ApiProperty({ type: MetodoPago })
  @IsNotEmpty()
  metodoPago: MetodoPago;

  //monto de pago en diferentes monedas
  @ApiProperty({ type: [TransaccionCuentaDto] })
  @IsNotEmpty()
  @ValidateNested({ each: true })
  transacciones: TransaccionCuentaDto[];

  //pago
  @ApiProperty({ type: DatosFacturacion })
  @IsOptional()
  @IsObject() //@IsInstance(DatosPago)
  datosFacturacion: DatosFacturacion;

  // direccion
  @ApiProperty({ type: Direccion })
  @IsNotEmptyObject()
  direccion: Direccion;

  // documentos usuario
  @ApiProperty({ type: [DocumentoUsuarioCtaDto] })
  @IsOptional()
  @ValidateNested({ each: true })
  @IsArray()
  @ArrayNotEmpty()
  @Type(() => DocumentoUsuarioCtaDto)
  documentosUsuario: DocumentoUsuarioCtaDto[];

  @ApiProperty({ type: MonedaRegistro })
  @IsObject()
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => MonedaRegistro)
  monedaRegistro: MonedaRegistro;

  // Direccion temporal
  @ApiProperty({})
  @IsOptional()
  direccionDomiciliaria: string;

  // Documento de identidad temporal
  @ApiProperty({})
  @IsOptional()
  documentoIdentidad: string;
}


