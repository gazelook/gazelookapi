import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsMongoId, IsNotEmpty, IsOptional } from 'class-validator';

export class ValidarCuentaDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  idTransaccion: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  pagoNoConfirmado: boolean;
}
