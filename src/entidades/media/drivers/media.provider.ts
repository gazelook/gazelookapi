import { Connection } from 'mongoose';
import { MediaModelo } from '../../../drivers/mongoose/modelos/media/mediaModelo';
import { TraduccionMediaModelo } from '../../../drivers/mongoose/modelos/traducion_media/traduccion-media.schema';

export const MediaProviders = [
  {
    provide: 'MEDIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('media', MediaModelo, 'media'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_MEDIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_media',
        TraduccionMediaModelo,
        'traduccion_media',
      ),
    inject: ['DB_CONNECTION'],
  },
];
