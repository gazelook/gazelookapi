import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBody,
  ApiConsumes,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { SubirArchivoService } from '../casos_de_uso/subir-archivo.service';
import { RetornoArchivoDto } from '../entidad/archivo-resultado.dto';
import { SubirArchivoDto } from '../entidad/subir-archivo.dto';

export const ApiFile = (fileName: string = 'file'): MethodDecorator => (
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor,
) => {
  ApiBody({
    required: true,
    schema: {
      type: 'object',
      properties: {
        [fileName]: {
          type: 'string',
          format: 'binary',
        },
        fileDefault: {
          type: 'Boolean',
          description:
            'Este campo controla si la imagen es del usuario(false por defecto-> es controlado en el api, no es obligatorio) o si es del sistema es true (Obligatorio)',
        },
        catalogoArchivoDefault: {
          type: 'string',
          description:
            'Este campo es la referencia al tipo de archivo por defecto(Ejemplo: CAT_ARC_DEFAULT_01 es de tipo album_general | CAT_ARC_DEFAULT_02 es de tipo album_perfil',
        },
        relacionAspecto: {
          type: 'string',
        },
        catalogoMedia: {
          type: 'string',
          description:
            'Obligatorio: referencia al codigo de catalogo media, Ejemplo: CATMED_1 es de tipo link | CATMED_2 es de tipo compuesto | CATMED_3 es de tipo simple',
        },
        descripcion: {
          type: 'string',
          description: 'descripcion de la imagen',
        },
        enlace: {
          type: 'string',
          description:
            'url o link. Este campo debe de guardarse junto con el codigo catalogoMedia: CATMED_1 que es de tipo link',
        },
        duracion: {
          type: 'string',
          description: 'Duración del audio',
        },
        estado: {
          type: 'string',
          description: 'codigo del estado de la media: EST_24 (activa)',
        },
      },
    },
  })(target, propertyKey, descriptor);
};

@ApiTags('Archivo')
@Controller('api/archivo')
export class SubirArchivoControlador {
  constructor(
    private readonly subirArchivoService: SubirArchivoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Post('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 201,
    type: RetornoArchivoDto,
    description: 'Archivo creado',
  })
  @ApiResponse({ status: 404, description: 'Error al guardar el archivo' })
  @ApiOperation({
    summary:
      'Esta función guarda el archivo y retorna las caracteristicas del mismo',
  })
  @ApiConsumes('multipart/form-data')
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: true,
  })
  @ApiFile('archivo')
  @UseInterceptors(FileInterceptor('archivo'))
  public async crearArchivo(
    @UploadedFile() file: Express.Multer.File,
    @Body() archivoDto: SubirArchivoDto,
    @Headers() headers,
  ) {
    const respuesta = new RespuestaInterface();
    archivoDto.archivo = file;
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const result = await this.subirArchivoService.subirArchivo(
        archivoDto,
        codIdioma,
      );
      if (result) {
        const CREACION_CORRECTA = await this.i18n.translate(
          codIdioma.concat('.CREACION_CORRECTA'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.CREATED,
          mensaje: CREACION_CORRECTA,
          datos: result,
        });
      } else {
        const ERROR_CREACION = await this.i18n.translate(
          codIdioma.concat('.ERROR_CREACION'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: ERROR_CREACION,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
