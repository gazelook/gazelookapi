import { Module } from '@nestjs/common';
import { SubirArchivoControlador } from './subir-archivo.controller';
import { MediaServiceModule } from '../casos_de_uso/media.services.module';
import { Funcion } from '../../../shared/funcion';

@Module({
  imports: [MediaServiceModule],
  providers: [Funcion],
  exports: [],
  controllers: [SubirArchivoControlador],
})
export class MediaControllerModule {}
