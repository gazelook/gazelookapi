import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { Media } from '../../../drivers/mongoose/interfaces/media/media.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

@Injectable()
export class CrearMediaService {
  constructor(
    @Inject('MEDIA_MODEL') private readonly mediaModel: Model<Media>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async crearMedia(dataMedia, opts): Promise<Media> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.sinAsignar,
      codEntidad,
    );
    const codEstado = estado.codigo;

    try {
      // verificar si viene en la data el estado
      if (!dataMedia?.estado) {
        dataMedia.estado = codEstado;
      }
      //crea media

      const media = new this.mediaModel(dataMedia);
      await media.save(opts);

      const datosMedia = JSON.parse(JSON.stringify(media));
      //eliminar parametro no necesarios
      delete datosMedia.fechaCreacion;
      delete datosMedia.fechaActualizacion;
      delete datosMedia.__v;

      const dataHistoricoMedia: any = {
        datos: datosMedia,
        usuario: null,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);
      return media;
    } catch (error) {
      throw error;
    }
  }
}
