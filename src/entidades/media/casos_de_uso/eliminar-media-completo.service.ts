import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { Media } from '../../../drivers/mongoose/interfaces/media/media.interface';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

@Injectable()
export class EliminarMediaCompletoService {
  constructor(
    @Inject('MEDIA_MODEL') private readonly mediaModel: Model<Media>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
  ) {}

  async eliminarMediaCompleto(idMedia: string, opts): Promise<Media> {
    //Obtiene el codigo de la accion a realizarse
    // const accion = await this.catalogoAccionService.obtenerNombreAccion(nombreAcciones.eliminar);

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.eliminado,
      codEntidad,
    );
    const codEstado = estado.codigo;

    // let dataMedia: any = {}

    try {
      // estado de la media eliminado (desactivado)
      // dataMedia.estado = codEstado;
      const media = await this.mediaModel.findOne({ _id: idMedia });
      // .populate([
      //   { path: 'principal' },
      //   { path: 'miniatura' }
      // ]);
      if (!media) {
        //throw { message: "No existe la media" };
        console.log('no existe la media');
        return null;
      }

      // ________________________ eliminar completamente (exepcion: media storage rollback)______________________________
      /* if (media?.principal !== undefined) {
        await this.eliminarArchivoCompletoService.eliminarArchivoCompleto(media.principal._id, opts);
      }
      if (media?.miniatura !== undefined) {
        await this.eliminarArchivoCompletoService.eliminarArchivoCompleto(media.miniatura._id, opts);
      }
      // eliminar traducciones de la media
      await this.traduccionMediaModel.deleteMany({ media: idMedia }, opts);

      // eliminar media
      await this.mediaModel.deleteOne({ _id: idMedia }, opts) */

      //________ eliminar logicamente luego se limpia las medias en estado => noAsignado o eliminado______________________________

      const dataActualizarMed = {
        estado: codEstado,
        fechaActualizacion: new Date(),
      };

      const parAso = await this.mediaModel.findByIdAndUpdate(
        idMedia,
        dataActualizarMed,
        opts,
      );

      return media;
    } catch (error) {
      throw error;
    }
  }
}
