import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { Media } from '../../../drivers/mongoose/interfaces/media/media.interface';
import { EliminarArchivoService } from '../../archivo/casos_de_uso/eliminar-archivo.service';
import { TraduccionMedia } from '../../../drivers/mongoose/interfaces/traduccion_media/traduccion-media.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

@Injectable()
export class EliminarMediaService {
  constructor(
    @Inject('MEDIA_MODEL') private readonly mediaModel: Model<Media>,
    @Inject('TRADUCCION_MEDIA_MODEL')
    private readonly traduccionMediaModel: Model<TraduccionMedia>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private eliminarArchivoService: EliminarArchivoService,
  ) {}

  async eliminarMedia(
    idMedia: string,
    idUsuario: string,
    hibernado: boolean,
    opts,
  ): Promise<Media> {
    let accion = '';

    //Obtiene el codigo de la accion a realizarse
    const accionEli = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.eliminar,
    );
    accion = accionEli.codigo;

    //Obtiene el codigo de la accion a realizarse
    const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.eliminado,
      codEntidad,
    );
    const codEstado = estado.codigo;

    // estado hibernado
    const estadoHibernado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.hibernado,
      codEntidad,
    );
    const codEstadoHibernado = estadoHibernado.codigo;

    //Obtiene el codigo de la entidad traduccionMedia
    const entidadTradMed = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    const codEntTradMed = entidadTradMed.codigo;

    //Obtiene el estado de eliminar la traduccion
    const estadoEliTrad = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.eliminado,
      codEntTradMed,
    );

    let dataMedia: any = {};

    try {
      // estado de la media eliminado (desactivado)
      dataMedia.estado = codEstado;
      // estado hibernado
      if (hibernado) {
        dataMedia.estado = codEstadoHibernado;
        accion = accionModificar.codigo;
      }
      const media = await this.mediaModel.findByIdAndUpdate(
        idMedia,
        dataMedia,
        opts,
      );
      if (!media) {
        return null;
      }

      if (!hibernado) {
        // eliminar traduccion media
        await this.traduccionMediaModel.updateMany(
          { media: idMedia },
          { $set: { estado: estadoEliTrad.codigo } },
          opts,
        );
        // eliminar archivos
        if (media.principal) {
          await this.eliminarArchivoService.eliminarArchivo(
            media.principal,
            idUsuario,
            opts,
          );
        }
        if (media.miniatura) {
          await this.eliminarArchivoService.eliminarArchivo(
            media.miniatura,
            idUsuario,
            opts,
          );
        }
      }

      const datosMedia = JSON.parse(JSON.stringify(media));
      //eliminar parametro no necesarios
      delete datosMedia.fechaCreacion;
      delete datosMedia.fechaActualizacion;
      delete datosMedia.__v;

      const dataHistoricoMedia: any = {
        datos: datosMedia,
        usuario: idUsuario,
        accion: accion,
        entidad: codEntidad,
      };
      // crear el historico
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

      return media;
    } catch (error) {
      throw error;
    }
  }
}
