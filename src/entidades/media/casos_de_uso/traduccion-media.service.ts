import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { Media } from '../../../drivers/mongoose/interfaces/media/media.interface';
import { TraduccionMedia } from '../../../drivers/mongoose/interfaces/traduccion_media/traduccion-media.interface';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

@Injectable()
export class TraduccionMediaService {
  constructor(
    @Inject('TRADUCCION_MEDIA_MODEL')
    private readonly traduccionMediaModel: Model<TraduccionMedia>,
    @Inject('MEDIA_MODEL') private readonly mediaModel: Model<Media>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async crearTraduccionMedia(
    dataTraduccionMedia,
    opts?,
  ): Promise<TraduccionMedia> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    try {
      const obtenerIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        dataTraduccionMedia.idioma,
      );

      //llama al metodo de traduccion dinamica
      /* const traduceDescripcion = await traducirTexto(
        dataTraduccionMedia.idioma,
        dataTraduccionMedia.descripcion
      );
      dataTraduccionMedia.descripcion = traduceDescripcion */

      //crea traduccion de la descripcion
      dataTraduccionMedia.estado = codEstado;
      dataTraduccionMedia.idioma = obtenerIdioma.codigo;
      const traduccionMedia = new this.traduccionMediaModel(
        dataTraduccionMedia,
      );
      await traduccionMedia.save(opts);
      const datosTradMedia = JSON.parse(JSON.stringify(traduccionMedia));
      delete datosTradMedia.fechaCreacion;
      delete datosTradMedia.fechaActualizacion;
      delete datosTradMedia.__v;

      const dataHistoricoMedia: any = {
        datos: datosTradMedia,
        usuario: null,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de la traduccion
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

      return traduccionMedia;
    } catch (error) {
      throw error;
    }
  }

  async traducirDescripcionMedia(
    idMedia,
    codigoNombreIdioma,
    opts: any,
  ): Promise<any> {
    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    try {
      console.log('codigoNombreIdioma: ', codigoNombreIdioma);
      const obtenerIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codigoNombreIdioma,
      );
      console.log('***¨¨¨¨¨¨¨¨¨¨¨¨¨¨');
      // obtener traduccion original activa
      const mediaOriginal = await this.mediaModel.findById(idMedia).populate({
        path: 'traducciones',
        select: 'idioma descripcion',
        match: { original: true, estado: codEstado },
      });

      const media = await this.mediaModel.findById(idMedia).populate({
        path: 'traducciones',
        select: '_id idioma descripcion',
        match: { idioma: obtenerIdioma.codigo, estado: codEstado },
      });
      console.log(
        'accca en traduccion se carrrrrrrrrrrrr: ',
        mediaOriginal.traducciones[0],
      );
      if (!mediaOriginal.traducciones[0]) {
        // console.log('accca en traduccion se carrrrrrrrrrrrr')
        return;
      }

      let original: any = mediaOriginal.traducciones;

      let mediaTraducir: any = media.traducciones;
      //___________si no existe la traduccion, se realiza la traduccion____________________
      if (mediaTraducir.length === 0) {
        //llama al metodo de traduccion dinamica
        const traduceDescripcion = await traducirTexto(
          obtenerIdioma.codNombre,
          original[0].descripcion,
        );
        const dataTraduccionMedia = {
          idioma: obtenerIdioma.codigo,
          descripcion: traduceDescripcion.textoTraducido,
          original: false,
          estado: codEstado,
          media: idMedia,
        };
        //crear nueva traduccion
        const traduccionMedia = await new this.traduccionMediaModel(
          dataTraduccionMedia,
        ).save(opts);
        console.log('traduccionMedia: ', traduccionMedia);
        //agregar a la referencia de media
        const addReferenciaMedia = await this.mediaModel.findOneAndUpdate(
          { _id: idMedia },
          { $push: { traducciones: traduccionMedia._id } },
          opts,
        );
        return traduccionMedia;
      }
      // ya existe la traduccion
      return media.traducciones[0];
    } catch (error) {
      throw error;
    }
  }
}
