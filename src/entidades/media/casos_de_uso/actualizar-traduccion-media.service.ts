import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { TraduccionMedia } from '../../../drivers/mongoose/interfaces/traduccion_media/traduccion-media.interface';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

@Injectable()
export class ActualizarTraduccionMediaService {
  constructor(
    @Inject('TRADUCCION_MEDIA_MODEL')
    private readonly traduccionMediaModel: Model<TraduccionMedia>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async actualizarDescripcion(
    idMedia: string,
    descripcion: string,
    idiomaCodNombre,
    opts,
  ): Promise<TraduccionMedia> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado de activa de la traduccion
    const estadoActiva = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstadoActiva = estadoActiva.codigo;

    try {
      const obtenerIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idiomaCodNombre,
      );

      //llama al metodo de traduccion dinamica
      /* const traduceDescripcion = await traducirTexto(
        dataTraduccionMedia.idioma,
        dataTraduccionMedia.descripcion
      );
      dataTraduccionMedia.descripcion = traduceDescripcion */

      //eliminar la traduccion
      await this.eliminarTraduccionesMedia(idMedia, opts);

      // data crear nueva traduccion
      const dataNuevaDescripcionMedia = {
        estado: codEstadoActiva,
        idioma: obtenerIdioma.codigo,
        original: true,
        descripcion: descripcion,
        media: idMedia,
      };
      const traduccionMedia = await new this.traduccionMediaModel(
        dataNuevaDescripcionMedia,
      );
      await traduccionMedia.save(opts);
      const datosTradMedia = JSON.parse(JSON.stringify(traduccionMedia));
      delete datosTradMedia.fechaCreacion;
      delete datosTradMedia.fechaActualizacion;
      delete datosTradMedia.__v;

      const dataHistoricoMedia: any = {
        datos: datosTradMedia,
        usuario: null,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de la traduccion
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

      return traduccionMedia;
    } catch (error) {
      throw error;
    }
  }

  async eliminarTraduccionesMedia(idMedia, opts: any): Promise<any> {
    try {
      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      const codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidad,
      );
      const codEstado = estado.codigo;

      const update = await this.traduccionMediaModel.updateMany(
        { media: idMedia },
        { $set: { estado: codEstado, fechaActualizacion: new Date() } },
        opts,
      );
      return update;
    } catch (error) {
      throw error;
    }
  }
}
