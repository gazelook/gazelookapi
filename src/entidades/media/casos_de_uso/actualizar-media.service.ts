import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { Media } from '../../../drivers/mongoose/interfaces/media/media.interface';
import { TraduccionMediaService } from './traduccion-media.service';
import { ActualizarTraduccionMediaService } from './actualizar-traduccion-media.service';
import {
  codigosHibernadoEntidades,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

@Injectable()
export class ActualizarMediaService {
  constructor(
    @Inject('MEDIA_MODEL') private readonly mediaModel: Model<Media>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private traduccionMediaService: TraduccionMediaService,
    private actualizarTraduccionMediaService: ActualizarTraduccionMediaService,
  ) {}

  // agregar referencia y cambiar estado asignado a la media
  // si tiene decripcion la agrega
  // el idioma: es, en
  async actualizarMediaAsignadoDescripcion(
    idMedia: string,
    idUsuario: string,
    idioma: string,
    descripcion: string,
    actualizar: boolean,
    opts: any,
  ): Promise<Media> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    try {
      // verificar principal y miniatura
      const getMedia = await this.mediaModel.findOne({ _id: idMedia });
      if (!getMedia) {
        //throw { message: "La media no existe" };
        return null;
      }
      let media;
      //_________________________Cuando se crear el Album______actualizar=false____________________
      if (!actualizar) {
        let dataTraduccionMedia: any = {};
        if (idioma && descripcion && getMedia.traducciones.length === 0) {
          dataTraduccionMedia.idioma = idioma;
          dataTraduccionMedia.descripcion = descripcion;
          dataTraduccionMedia.original = true;
          dataTraduccionMedia.media = getMedia._id;
          const saveTraduccionMedia = await this.traduccionMediaService.crearTraduccionMedia(
            dataTraduccionMedia,
            opts,
          );

          // agregar referencia de la traduccion a la media
          await this.mediaModel.updateOne(
            { _id: idMedia },
            { $push: { traducciones: saveTraduccionMedia._id } },
            opts,
          );
        }
        let date = new Date();
        media = await this.mediaModel.findByIdAndUpdate(
          idMedia,
          { estado: codEstado, fechaActualizacion: date },
          opts,
        );
      } else {
        //_________________________Cuando se Actualiza el Album______actualizar=true____________________
        if (idioma && descripcion) {
          const saveNuevaTraduccionMedia = await this.actualizarTraduccionMediaService.actualizarDescripcion(
            getMedia._id,
            descripcion,
            idioma,
            opts,
          );

          // agregar referencia de la traduccion a la media
          await this.mediaModel.updateOne(
            { _id: idMedia },
            { $push: { traducciones: saveNuevaTraduccionMedia._id } },
            opts,
          );
        }
        let date = new Date();
        // actualiza a estado asignado
        media = await this.mediaModel.updateOne(
          { _id: idMedia },
          { estado: codEstado, fechaActualizacion: date },
          opts,
        );
      }

      const datosMedia = JSON.parse(JSON.stringify(media));
      //eliminar parametro no necesarios
      delete datosMedia.fechaCreacion;
      delete datosMedia.fechaActualizacion;
      delete datosMedia.__v;

      const dataHistoricoMedia: any = {
        datos: datosMedia,
        usuario: idUsuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

      return media;
    } catch (error) {
      throw error;
    }
  }

  async activarMedia(
    idMedia: string,
    idPerfil: string,
    opts: any,
  ): Promise<Media> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    let dataMedia: any = {};

    try {
      // obtener media hibernado
      const mediaHibernado = await this.mediaModel.findOne({
        $and: [
          { _id: idMedia },
          { estado: codigosHibernadoEntidades.mediaHibernado },
        ],
      });
      if (!mediaHibernado) {
        return null;
      }

      dataMedia.estado = codEstado;
      const media = await this.mediaModel.findByIdAndUpdate(
        idMedia,
        dataMedia,
        opts,
      );

      const datosMedia = JSON.parse(JSON.stringify(media));
      //eliminar parametro no necesarios
      delete datosMedia.fechaCreacion;
      delete datosMedia.fechaActualizacion;
      delete datosMedia.__v;

      const dataHistoricoMedia: any = {
        datos: datosMedia,
        usuario: idPerfil,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

      return media;
    } catch (error) {
      throw error;
    }
  }

  //____________cambia de estado sinAsignar a activa(es parte de un usuario o perfil)__________
  async actualizarEstadoAsignado(idMedia, idUsuario, opts): Promise<Media> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    try {
      let date = new Date();
      const media = await this.mediaModel.updateOne(
        { _id: idMedia },
        { estado: codEstado, fechaActualizacion: date },
        opts,
      );

      const datosMedia = JSON.parse(JSON.stringify(media));
      //eliminar parametro no necesarios
      delete datosMedia.fechaCreacion;
      delete datosMedia.fechaActualizacion;
      delete datosMedia.__v;

      const dataHistoricoMedia: any = {
        datos: datosMedia,
        usuario: idUsuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

      return media;
    } catch (error) {
      throw error;
    }
  }
}
