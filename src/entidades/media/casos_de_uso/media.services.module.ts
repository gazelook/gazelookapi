import { Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from './../../catalogos/casos_de_uso/catalogos-services.module';
import { MediaProviders } from '../drivers/media.provider';
import { CrearMediaService } from './crear-media.service';
import { TraduccionMediaService } from './traduccion-media.service';
import { ArchivoServicesModule } from '../../archivo/casos_de_uso/archivo-services.module';
import { SubirArchivoService } from './subir-archivo.service';
import { EliminarMediaService } from './eliminar-media.service';
import { ActualizarMediaService } from './actualizar-media.service';
import { EliminarMediaCompletoService } from './eliminar-media-completo.service';
import { ActualizarTraduccionMediaService } from './actualizar-traduccion-media.service';

@Module({
  imports: [DBModule, CatalogosServiceModule, ArchivoServicesModule],
  providers: [
    ...MediaProviders,
    CrearMediaService,
    TraduccionMediaService,
    SubirArchivoService,
    EliminarMediaService,
    ActualizarMediaService,
    EliminarMediaCompletoService,
    ActualizarTraduccionMediaService,
  ],
  exports: [
    CrearMediaService,
    TraduccionMediaService,
    SubirArchivoService,
    EliminarMediaService,
    ActualizarMediaService,
    EliminarMediaCompletoService,
    ActualizarTraduccionMediaService,
  ],
  controllers: [],
})
export class MediaServiceModule {}
