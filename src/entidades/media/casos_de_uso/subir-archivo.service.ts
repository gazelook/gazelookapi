import { HttpStatus, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { resolve } from 'path';
import {
  codigosCatalogoMedia,
  codigosEstadosMedia,
  dimensionesImagen,
} from 'src/shared/enum-sistema';
import { v4 as uuidv4 } from 'uuid';
import { erroresGeneral, erroresMedia } from '../../../shared/enum-errores';
import { codigoCatalogoTipoMedia } from '../../../shared/enum-sistema';
import { CrearArchivoService } from '../../archivo/casos_de_uso/crear-archivo.service';
import { CrearMiniaturaService } from '../../archivo/casos_de_uso/crear-miniatura.service';
import { GestionArchivoService } from '../../archivo/casos_de_uso/gestion-archivo.service';
import { crearArchivoDto } from '../../archivo/entidad/crear-archivo.dto';
import { CatalogoMediaService } from '../../catalogos/casos_de_uso/catalogo-media.service';
import { SubirArchivoDto } from '../entidad/subir-archivo.dto';
import { CrearMediaService } from './crear-media.service';
import { TraduccionMediaService } from './traduccion-media.service';

@Injectable()
export class SubirArchivoService {
  COMPUESTO = codigosCatalogoMedia.catMediaCompuesto; // principal y miniatura
  SIMPLE = codigosCatalogoMedia.catMediaSimple; // miniatura
  LINK = codigosCatalogoMedia.catMediaLink; // solo enlace, si es de youtube guardar miniatura
  ONE_MEGA_BYTE = 1048576; // 1MB en bytes

  constructor(
    private traduccionMediaService: TraduccionMediaService,
    private crearArchivoService: CrearArchivoService,
    private crearMediaService: CrearMediaService,
    private crearMiniaturaService: CrearMiniaturaService,
    private catalogoMediaService: CatalogoMediaService,
    private gestionArchivoService: GestionArchivoService,
  ) {}

  async subirArchivoTest(data): Promise<any> {
    // guarda la imagen en la raiz del proyecto
    try {
      console.log(data.archivo);

      //const result = await this.resizeAudio(data.archivo)
      //const result = await this.resizeVideo(data.archivo)

      const dimensiones = await this.resizeImagen(data.archivo, false);

      return { message: 'ok', datos: dimensiones };
    } catch (error) {
      throw error;
    }
  }

  async resizeAudio(archivo): Promise<any> {
    try {
      let result: any = {};
      const folder = resolve(__dirname, `../temp_file/`);
      const extension = this.gestionArchivoService.obtenerExtensionArchivo(
        archivo.originalname,
      );
      const originalname = `${uuidv4().toString()}.${extension}`;
      const path = resolve(__dirname, `../temp_file/${originalname}`);

      const newPath = resolve(
        __dirname,
        `../temp_file/${uuidv4().toString()}.${extension}`,
      );
      await this.gestionArchivoService
        .crearArchivo(path, archivo.buffer)
        .catch(data => {
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
          };
        });

      await this.gestionArchivoService
        .redimensionarAudio(path, '128k', newPath)
        .catch(data => {
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
          };
        });
      //datos del audio redimensionado
      const getPropiedadesAudio: any = await this.gestionArchivoService.getPropertyFile(
        newPath,
      );
      const bufferAudio = await this.gestionArchivoService.getBufferFile(
        newPath,
      );

      //valor a retornar
      result.audio = {
        size: getPropiedadesAudio.size,
        buffer: bufferAudio,
      };

      // eliminar archivo fisico
      await this.gestionArchivoService.eliminarArchivo(path).catch(data => {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
        };
      });
      await this.gestionArchivoService.eliminarArchivo(newPath).catch(data => {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
        };
      });

      return result;
    } catch (error) {
      throw error;
    }
  }

  async resizeVideo(archivo): Promise<any> {
    try {
      let result: any = {};
      const folder = resolve(__dirname, `../temp_file/`);
      const extension = this.gestionArchivoService.obtenerExtensionArchivo(
        archivo.originalname,
      );
      const originalname = `${uuidv4().toString()}.${extension}`;
      const path = resolve(__dirname, `../temp_file/${originalname}`);

      const newPath = resolve(
        __dirname,
        `../temp_file/${uuidv4().toString()}.${extension}`,
      );

      await this.gestionArchivoService
        .crearArchivo(path, archivo.buffer)
        .catch(data => {
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
          };
        });

      await this.gestionArchivoService
        .redimensionarVideo(path, '80%', newPath)
        .catch(data => {
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
          };
        });
      //datos del video redimensionado
      const getPropiedadesVideo: any = await this.gestionArchivoService.getPropertyFile(
        newPath,
      );
      const bufferVideo = await this.gestionArchivoService.getBufferFile(
        newPath,
      );
      //datos miniatura
      // retorna el nombre de la imagen
      const miniaturaVideo = await this.gestionArchivoService
        .obtenerMiniaturaVideo(path, folder)
        .catch(data => {
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
          };
        });
      const pathMiniatura = resolve(
        __dirname,
        `../temp_file/${miniaturaVideo}`,
      );
      const getPropiedadesMiniatura: any = await this.gestionArchivoService.getPropertyFile(
        pathMiniatura,
      );
      const bufferMiniatura = await this.gestionArchivoService.getBufferFile(
        pathMiniatura,
      );
      //valor a retornar
      result.video = {
        size: getPropiedadesVideo.size,
        buffer: bufferVideo,
      };
      result.miniatura = {
        size: getPropiedadesMiniatura.size,
        buffer: bufferMiniatura,
        imagen: miniaturaVideo,
      };

      // eliminar archivo fisico
      await this.gestionArchivoService.eliminarArchivo(path);
      await this.gestionArchivoService.eliminarArchivo(newPath);
      await this.gestionArchivoService.eliminarArchivo(pathMiniatura);

      return result;
    } catch (error) {
      throw error;
    }
  }

  async resizeImagen(archivo, crearMiniatura: boolean): Promise<any> {
    try {
      let result: any = {};
      const folder = resolve(__dirname, `../temp_file/`);
      const extension = this.gestionArchivoService.obtenerExtensionArchivo(
        archivo.originalname,
      );
      const originalname = `${uuidv4().toString()}.${extension}`;

      // guarda la imagen
      const dirPath = resolve(__dirname, `../temp_file/${originalname}`);
      const newPath = resolve(
        __dirname,
        `../temp_file/${uuidv4().toString()}.${extension}`,
      );

      await this.gestionArchivoService
        .crearArchivo(dirPath, archivo.buffer)
        .catch(data => {
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
          };
        });

      //redimensionar imagen
      const porcentajeConfigurable = this.gestionArchivoService.obtenerPorcentajeBySizeFile(
        archivo.size,
      );
      const getDimensiones: any = await this.gestionArchivoService.getDimensionesImage(
        dirPath,
      );

      const nuevasDimensiones = this.gestionArchivoService.nuevasDimensionesImagen(
        getDimensiones.width,
        getDimensiones.height,
        porcentajeConfigurable,
      );

      await this.gestionArchivoService
        .redimensionarImagenByDimensiones(
          dirPath,
          `${nuevasDimensiones.width}x${nuevasDimensiones.height}`,
          newPath,
        )
        .catch(data => {
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
          };
        });

      const getPropiedadesImagen: any = await this.gestionArchivoService.getPropertyFile(
        newPath,
      );
      const bufferImagen = await this.gestionArchivoService.getBufferFile(
        newPath,
      );
      // crear miniatura imagen
      let getPropiedadesMiniatura, bufferMiniatura;
      if (crearMiniatura) {
        const extension = this.gestionArchivoService.obtenerExtensionArchivo(
          originalname,
        );
        const pathMiniatura = resolve(
          __dirname,
          `../temp_file/${uuidv4().toString()}.${extension}`,
        );
        const miniatura = await this.gestionArchivoService.crearArchivo(
          pathMiniatura,
          archivo.buffer,
        );
        // sacar la miniatura a 650px
        const newPathMiniatura = resolve(
          __dirname,
          `../temp_file/${uuidv4().toString()}.${extension}`,
        );

        await this.gestionArchivoService
          .redimensionarImagenByDimensiones(
            pathMiniatura,
            dimensionesImagen.miniatura,
            newPathMiniatura,
          )
          .catch(data => {
            throw {
              codigo: HttpStatus.NOT_ACCEPTABLE,
              codigoNombre: erroresMedia.ERROR_PROCESO_ARCHIVO,
            };
          });

        const getPropiedadesMiniatura: any = await this.gestionArchivoService.getPropertyFile(
          newPathMiniatura,
        );
        const bufferMiniatura = await this.gestionArchivoService.getBufferFile(
          newPathMiniatura,
        );

        //valor a retornar de la miniatura
        result.miniatura = {
          size: getPropiedadesMiniatura.size,
          buffer: bufferMiniatura,
        };
        //eliminar miniatura
        await this.gestionArchivoService.eliminarArchivo(pathMiniatura);
        await this.gestionArchivoService.eliminarArchivo(newPathMiniatura);
      }
      //valor a retornar
      result.imagen = {
        size: getPropiedadesImagen.size,
        buffer: bufferImagen,
      };
      // eliminar archivo fisico
      await this.gestionArchivoService.eliminarArchivo(dirPath);
      await this.gestionArchivoService.eliminarArchivo(newPath);

      return result;
    } catch (error) {
      throw error;
    }
  }

  //In wait
  async getImagePdf(archivo) {
    const dirPath = resolve(__dirname, `../temp_file/${archivo.originalname}`);
    const crearArchivo = await this.gestionArchivoService.crearArchivo(
      dirPath,
      archivo.buffer,
    );
    const bufferPdf = await this.gestionArchivoService.getBufferFile(dirPath);
  }

  async subirArchivo(data: SubirArchivoDto, idioma: string): Promise<any> {
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      let saveTraduccionMedia;
      let saveArchivo;
      let saveMiniatura;
      let saveMedia;
      let referenciaTraduccionMedia = [];
      let bufferMiniatura;
      const archivo = data.archivo;
      let dataArchivo: crearArchivoDto = {
        fileDefault: data.fileDefault,
      };
      let validateCodigos;
      //traduccion
      let dataTraduccionMedia: any = {};
      // id de la media generado
      const idMedia = new mongoose.mongo.ObjectId();
      //media
      let dataMedia: any = {
        _id: idMedia,
      };

      // Opcional: verificar la propiedad estado en la data
      if (data?.estado) {
        if (data.estado !== codigosEstadosMedia.activa) {
          console.error('Error: el estado de la media no es valido');
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
          };
        } else {
          dataMedia.estado = data.estado;
        }
      }

      // verificar que solo venga el archivo
      if (archivo && data.enlace) {
        console.error('Error: archivo y enlace son diferentes tipos');
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }
      // verificar si existe el codigo de catalogo media
      const catalogoMedia = await this.catalogoMediaService.obtenerCatalogoMediaByCodigo(
        data.catalogoMedia,
      );
      if (!catalogoMedia) {
        console.error('Error: tipo de la media no existe');
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }

      //crear traduccion de los archivos (se crea sin referencia de la media )
      if (idioma && data?.descripcion) {
        dataTraduccionMedia.idioma = idioma;
        dataTraduccionMedia.descripcion = data.descripcion;
        dataTraduccionMedia.original = true;
        dataTraduccionMedia.media = idMedia;
        saveTraduccionMedia = await this.traduccionMediaService.crearTraduccionMedia(
          dataTraduccionMedia,
          opts,
        );
        referenciaTraduccionMedia.push(saveTraduccionMedia._id);
      }
      // referencia a la traduccion de la descripcion
      if (saveTraduccionMedia) {
        dataMedia.traducciones = referenciaTraduccionMedia;
      }
      // si hay  un archivo
      if (archivo) {
        //Validar archivos por defecto
        if (
          (!data.fileDefault && data.catalogoArchivoDefault) ||
          (data.fileDefault && !data.catalogoArchivoDefault)
        ) {
          console.error('El archivo por defecto acepta dos parametros');
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
          };
        }
        if (data.fileDefault && data.catalogoArchivoDefault) {
          validateCodigos = this.gestionArchivoService.validarArchivosDefault(
            data.fileDefault,
            data.catalogoArchivoDefault,
          );

          if (!validateCodigos) {
            console.error('Error: codigo no valido del archivo por defecto');
            throw {
              codigo: HttpStatus.NOT_ACCEPTABLE,
              codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
            };
          }
        }
        // validar archivo
        const validarArchivo = this.gestionArchivoService.validarArchivo(
          data.archivo,
        );
        if (validarArchivo.validate && data.catalogoMedia !== this.LINK) {
          // data archivo general
          dataArchivo.archivo = archivo;

          // archivos del sistema
          if (validateCodigos) {
            dataArchivo.catalogoArchivoDefault = data.catalogoArchivoDefault;
            dataArchivo.fileDefault = data.fileDefault;
          }

          // guardar segun la media
          // si es simple y compuesto
          //______________IMAGEN__________________
          if (validarArchivo.tipo === codigoCatalogoTipoMedia.codImage) {
            // codigo de catalogoTipoMedia
            dataArchivo.tipo = codigoCatalogoTipoMedia.codImage;

            if (data.catalogoMedia === this.COMPUESTO) {
              // crear compuesto
              const resizeImgPrincipal = await this.resizeImagen(
                dataArchivo.archivo,
                true,
              );

              // guardar archivo principal
              dataArchivo.archivo.buffer = resizeImgPrincipal.imagen.buffer;
              dataArchivo.archivo.size = resizeImgPrincipal.imagen.size;
              saveArchivo = await this.crearArchivoService.crearArchivo(
                dataArchivo,
                opts,
              );

              // crear miniatura
              dataArchivo.archivo.buffer = resizeImgPrincipal.miniatura.buffer;
              dataArchivo.archivo.size = resizeImgPrincipal.miniatura.size;
              saveMiniatura = await this.crearArchivoService.crearArchivo(
                dataArchivo,
                opts,
              );

              // guardar media
              //dataMedia
              dataMedia.catalogoMedia = data.catalogoMedia;
              dataMedia.principal = saveArchivo._id;
              dataMedia.miniatura = saveMiniatura._id;
              saveMedia = await this.crearMediaService.crearMedia(
                dataMedia,
                opts,
              );
            }
            if (data.catalogoMedia === this.SIMPLE) {
              let resizeImgPrincipalSimple;
              if (data.fileDefault !== 'true') {
                resizeImgPrincipalSimple = await this.resizeImagen(
                  dataArchivo.archivo,
                  false,
                );
                // guardar archivo principal
                dataArchivo.archivo.buffer =
                  resizeImgPrincipalSimple.imagen.buffer;
                dataArchivo.archivo.size = resizeImgPrincipalSimple.imagen.size;
              }

              saveArchivo = await this.crearArchivoService.crearArchivo(
                dataArchivo,
                opts,
              );

              // data media
              dataMedia.catalogoMedia = data.catalogoMedia;
              dataMedia.principal = saveArchivo._id;

              // guardar media
              saveMedia = await this.crearMediaService.crearMedia(
                dataMedia,
                opts,
              );
            }
          }
          //______________VIDEO__________________
          // el video es compuesto: se almacena el video y la miniatura
          if (validarArchivo.tipo === codigoCatalogoTipoMedia.codVideo) {
            // codigo de catalogoTipoMedia
            dataArchivo.tipo = codigoCatalogoTipoMedia.codVideo;

            if (data.catalogoMedia === this.COMPUESTO) {
              const resizeImgPrincipal = await this.resizeVideo(
                dataArchivo.archivo,
              );

              // guardar archivo principal
              dataArchivo.archivo.buffer = resizeImgPrincipal.video.buffer;
              dataArchivo.archivo.size = resizeImgPrincipal.video.size;
              saveArchivo = await this.crearArchivoService.crearArchivo(
                dataArchivo,
                opts,
              );

              // crear miniatura

              dataArchivo.archivo.buffer = resizeImgPrincipal.miniatura.buffer;
              dataArchivo.archivo.size = resizeImgPrincipal.miniatura.size;
              dataArchivo.archivo.originalname =
                resizeImgPrincipal.miniatura.imagen;
              dataArchivo.archivo.mimetype = 'image/png';
              saveMiniatura = await this.crearArchivoService.crearArchivo(
                dataArchivo,
                opts,
              );

              // guardar media
              //dataMedia
              dataMedia.catalogoMedia = data.catalogoMedia;
              dataMedia.principal = saveArchivo._id;
              dataMedia.miniatura = saveMiniatura._id;
              saveMedia = await this.crearMediaService.crearMedia(
                dataMedia,
                opts,
              );
            }
          }
          //______________AUDIO__________________
          if (validarArchivo.tipo === codigoCatalogoTipoMedia.codAudio) {
            // codigo de catalogoTipoMedia
            dataArchivo.tipo = codigoCatalogoTipoMedia.codAudio;

            if (data.catalogoMedia === this.SIMPLE) {
              // redimensionar audio
              /* const resizeAudio = await this.resizeAudio(dataArchivo.archivo);

                            // guardar archivo principal
                            dataArchivo.archivo.buffer = resizeAudio.audio.buffer;
                            dataArchivo.archivo.size = resizeAudio.audio.size;
                            dataArchivo.duracion = data.duracion;
                            saveArchivo = await this.crearArchivoService.crearArchivo(dataArchivo, opts); */
              //__________________________
              dataArchivo.duracion = data.duracion;
              saveArchivo = await this.crearArchivoService.crearArchivo(
                dataArchivo,
                opts,
              );
              dataMedia.catalogoMedia = data.catalogoMedia;
              dataMedia.principal = saveArchivo._id;

              // guardar media
              saveMedia = await this.crearMediaService.crearMedia(
                dataMedia,
                opts,
              );
            }
          }
          //______________FILES__________________
          if (validarArchivo.tipo === codigoCatalogoTipoMedia.codFiles) {
            // codigo de catalogoTipoMedia
            dataArchivo.tipo = codigoCatalogoTipoMedia.codFiles;

            if (data.catalogoMedia === this.SIMPLE) {
              saveArchivo = await this.crearArchivoService.crearArchivo(
                dataArchivo,
                opts,
              );

              dataMedia.catalogoMedia = data.catalogoMedia;
              dataMedia.principal = saveArchivo._id;

              // guardar media
              saveMedia = await this.crearMediaService.crearMedia(
                dataMedia,
                opts,
              );
            }
          }
        }
      }
      //____________________________ENLACE__________________________
      let saveMiniaturaEnlace;
      let saveArchivoPrincipalEnlace;
      if (data.enlace && data.catalogoMedia === this.LINK) {
        dataMedia.catalogoMedia = data.catalogoMedia;

        //_____________________
        // guardar archivo principal enlace
        const dataArchivoPrincipalEnlace = {
          enlace: data.enlace,
          tipoCatTipMedia: codigoCatalogoTipoMedia.codEnlace,
        };
        saveArchivoPrincipalEnlace = await this.crearArchivoService.crearArchivoTipoLink(
          dataArchivoPrincipalEnlace,
          opts,
        );

        // crear miniatura enlace
        if (this.gestionArchivoService.validarEnlaceYoutube(data.enlace)) {
          const enlaceMiniatura = await this.crearMiniaturaService.obtenerMiniaturaUrlYoutube(
            data.enlace,
          );
          const dataArchivoMiniaturaEnlace = {
            enlace: enlaceMiniatura,
            tipoCatTipMedia: codigoCatalogoTipoMedia.codEnlace,
          };
          saveMiniaturaEnlace = await this.crearArchivoService.crearArchivoTipoLink(
            dataArchivoMiniaturaEnlace,
            opts,
          );
          // referencia media miniatura
          dataMedia.miniatura = saveMiniaturaEnlace._id;
        }
        // miniatura enlace ted
        if (this.gestionArchivoService.validarEnlaceTed(data.enlace)) {
          const enlaceMiniatura = await this.crearMiniaturaService
            .obtenerMiniaturaUrlTed(data.enlace)
            .toPromise();
          const dataArchivoMiniaturaEnlace = {
            enlace: enlaceMiniatura.data.thumbnail_url,
            tipoCatTipMedia: codigoCatalogoTipoMedia.codEnlace,
          };
          saveMiniaturaEnlace = await this.crearArchivoService.crearArchivoTipoLink(
            dataArchivoMiniaturaEnlace,
            opts,
          );
          // referencia media miniatura
          dataMedia.miniatura = saveMiniaturaEnlace._id;
        }

        // guardar media
        //dataMedia
        dataMedia.catalogoMedia = data.catalogoMedia;
        dataMedia.principal = saveArchivoPrincipalEnlace._id;

        saveMedia = await this.crearMediaService.crearMedia(dataMedia, opts);
        //_______________________
      }

      if (!saveMedia) {
        console.error('Error al crear la media');
        return null;
      }

      // ______________resultado____________________
      const result: any = {
        _id: saveMedia._id,
        fechaActualizacion: saveMedia.fechaActualizacion,
      };
      if (data.catalogoMedia === this.LINK) {
        result.principal = {
          _id: saveArchivoPrincipalEnlace._id,
          url: saveArchivoPrincipalEnlace.url,
          estado: saveArchivoPrincipalEnlace.estado,
          tipo: {
            codigo: saveArchivoPrincipalEnlace.tipo,
          },
        };
        if (saveMedia.miniatura) {
          // consultar la miniatura
          result.miniatura = {
            _id: saveMiniaturaEnlace._id,
            url: saveMiniaturaEnlace.url,
            estado: saveMiniaturaEnlace.estado,
            tipo: {
              codigo: saveMiniaturaEnlace.tipo,
            },
          };
        }
        if (saveTraduccionMedia) {
          const dataTraduccion = {
            _id: saveTraduccionMedia._id,
            descripcion: saveTraduccionMedia.descripcion,
          };
          result.traducciones = [dataTraduccion];
        }
      } else {
        result.principal = {
          _id: saveArchivo._id,
          url: saveArchivo.url,
          duracion: saveArchivo?.duracion,
          filenameStorage: saveArchivo.filenameStorage,
          filename: saveArchivo.filename,
          peso: saveArchivo.peso,
          tipo: {
            codigo: saveArchivo.tipo,
          },
          path: saveArchivo.path,
          fileDefault: saveArchivo.fileDefault,
          catalogoArchivoDefault: saveArchivo?.catalogoArchivoDefault,
        };
        if (saveMedia.miniatura) {
          // consultar la miniatura
          result.miniatura = {
            _id: saveMiniatura._id,
            url: saveMiniatura.url,
            filenameStorage: saveArchivo.filenameStorage,
            filename: saveArchivo.filename,
            peso: saveArchivo.peso,
            tipo: {
              codigo: saveArchivo.tipo,
            },
            path: saveArchivo.path,
            fileDefault: saveArchivo.fileDefault,
            catalogoArchivoDefault: saveArchivo?.catalogoArchivoDefault,
          };
        }
        if (saveTraduccionMedia) {
          const dataTraduccion = {
            _id: saveTraduccionMedia._id,
            descripcion: saveTraduccionMedia.descripcion,
          };
          result.traducciones = [dataTraduccion];
        }
      }
      // proceso exitoso
      await session.commitTransaction();
      session.endSession();

      return result;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
