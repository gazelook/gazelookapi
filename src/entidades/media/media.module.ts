import { Module } from '@nestjs/common';
import { MediaControllerModule } from './controladores/media-controller.module';
@Module({
  imports: [MediaControllerModule],
})
export class MediaModule {}
