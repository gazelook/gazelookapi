import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsUrl } from 'class-validator';

export class SubirArchivoDto {
  @ApiProperty({ type: 'file' })
  archivo: Express.Multer.File;
  @ApiProperty()
  relacionAspecto: string;
  @ApiProperty({
    description:
      'Este campo controla si la imagen es del usuario(false por defecto) o del sistema(true)',
    required: true,
  })
  fileDefault?: string;
  @ApiProperty({
    description:
      'Este campo es la referencia al tipo de archivo por defecto(Ejemplo: CAT_ARC_DEFAULT_01 es de tipo album_general | CAT_ARC_DEFAULT_02 es de tipo album_perfil',
    required: true,
  })
  catalogoArchivoDefault?: string;
  @ApiProperty()
  codigoTipoCatalogoArchivo: string;
  @ApiProperty()
  descripcion: string;

  @ApiProperty()
  catalogoMedia;

  @ApiProperty()
  @IsOptional()
  @IsUrl()
  enlace: string;

  @ApiProperty()
  duracion: string;

  @ApiProperty()
  estado: string;
}
