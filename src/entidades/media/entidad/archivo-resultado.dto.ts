import { ApiProperty } from '@nestjs/swagger';
import { ArchivoUrlDto } from 'src/entidades/archivo/entidad/archivo-dto';
export class Archivo {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  url: string;
}
export class RetornoArchivoDto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  principal: Archivo;
  @ApiProperty({
    description:
      'Este campo se visualizara en el caso que la media sea de tipo link: CATMED_1',
    example: 'string | ejemplo: https://www.youtube.com/watch?v=B2a2rNgQPDM',
  })
  enlace: string;
  @ApiProperty({
    description:
      'Este campo se visualizara en el caso que la media sea de tipo compuesto: CATMED_2 || Sí es de tipo link, se visualizara un enlace (ejemplo: enlace miniatura de youtube) ',
  })
  miniatura: Archivo;
  /* @ApiProperty()
    fileDefault?: Boolean; */
}

export class RetornoPrincipalMediaDto {
  @ApiProperty({ type: ArchivoUrlDto })
  principal: ArchivoUrlDto;
}

export class mediaId {
  @ApiProperty()
  _id: string;
}

export class urlFotoPerfil {
  @ApiProperty({
    description: 'url del archivo',
    example:
      'https://gazelook-storage.s3.amazonaws.com/general/1599230128870.jpeg',
  })
  url: string;

  @ApiProperty()
  fileDefault: boolean;
}

export class codigoCatalogoMediaDto {
  @ApiProperty()
  codigo: string;
}
