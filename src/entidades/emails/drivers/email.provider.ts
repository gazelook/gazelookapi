import { Connection } from 'mongoose';
import { emailSchema } from './../../../drivers/mongoose/modelos/emails/email.modelo';

export const emailProviders = [
  {
    provide: 'EMAIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('emails', emailSchema, 'emails'),
    inject: ['DB_CONNECTION'],
  },
];
