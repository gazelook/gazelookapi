import { ApiProperty } from '@nestjs/swagger';

export class EmailDto {
  codigoTipo: string;

  emailDestinatario: string;

  usuario: string;

  estado: string;

  fechaCreacion: Date;

  fechaValidacion: Date;

  tokenEmail: String;
}

export class ActualizarEmailResponsableDto {
  @ApiProperty({ description: 'id del usuario' })
  _id: string;
  @ApiProperty()
  emailResponsable: string;
}
export class EmailPagoDto {
  idInformacionPago: string;
  emailDestinatario: string;
  nombre: string;
  idioma: string;
  usuario: string;
  autorizacionCodePaymentez?:string;
}
