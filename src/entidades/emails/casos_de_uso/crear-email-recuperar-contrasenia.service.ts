import { HttpService, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ConfigService } from '../../../config/config.service';
import { pathServerM } from '../../../shared/enum-servidor-mantenimiento';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';

@Injectable()
export class CrearEmailRecuperarContraseniaService {
  constructor(
    @Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private httpService: HttpService,
    private config: ConfigService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
  ) {}

  async crearEmailRecuperarContrasenia(
    crearEmailDto: any,
    opts: any,
  ): Promise<any> {
    //Obtiene el codigo de la accion crear
    const accion = await this.nombreAccion.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene el codigo de la entidad Email
    const entidad = await this.nombreEntidad.obtenerNombreEntidad(
      nombreEntidades.email,
    );
    let codEntidad = entidad.codigo;

    //Obtiene el estado activa de la entidad email
    const estado = await this.nombreEstado.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    let codEstado = estado.codigo;

    const fechaCreacion = new Date();
    const fechaValid = this.sumarDias(new Date());
    const token = this.random() + this.random() + this.random();

    try {
      const usuario = await this.obtenerIdUsuarioService.obtenerUsuarioByEmail(
        crearEmailDto.emailDestinatario,
      );

      const nuevoEmail = {
        usuario: usuario._id,
        emailDestinatario: crearEmailDto.emailDestinatario,
        estado: codEstado,
        fechaCreacion: fechaCreacion,
        fechaValidacion: fechaValid,
        tokenEmail: token,
        codigo: crearEmailDto.codigo,
        idioma: crearEmailDto.idioma,
        enviado: false,
        descripcionEnvio: '',
      };
      const getEmail = await this.envioEmail(nuevoEmail).toPromise();
      if (getEmail.data.respuesta.datos?.response) {
        nuevoEmail['enviado'] = true;
        nuevoEmail['descripcionEnvio'] = getEmail.data.respuesta.datos.response;
      } else {
        nuevoEmail['enviado'] = false;
        nuevoEmail['descripcionEnvio'] = getEmail.data.respuesta.datos.code;
      }
      const email = await new this.emailModel(nuevoEmail).save(opts);

      //datos para guardar el historico
      const newHistorico: any = {
        datos: email,
        usuario: nuevoEmail.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      return true;
    } catch (error) {
      throw error;
    }
  }
  envioEmail(crearEmailDto): Observable<any> {
    try {
      const apiKey = this.config.get('API_KEY');
      const URL_SERVER_MANTENIMIENTO = this.config.get(
        'URL_SERVER_MANTENIMIENTO',
      );
      // const URL_SERVER_MANTENIMIENTO = 'http://localhost:5000'
      const headersRequest: any = {
        'Content-Type': 'application/json',
        apiKey: apiKey,
      };
      return this.httpService.post(
        `${URL_SERVER_MANTENIMIENTO}${pathServerM.RECUPERAR_CONTRASENA}`,
        crearEmailDto,
        { headers: headersRequest },
      );
    } catch (error) {
      throw error;
    }
  }
  sumarDias(fecha) {
    fecha.setDate(fecha.getDate() + 15);
    return fecha;
  }
  random() {
    return Math.random()
      .toString(36)
      .substr(2);
  }
}
