import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { erroresGeneral } from '../../../shared/enum-errores';
import {
  codigoCatalogosTipoEmail,
  estadosUsuario,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoTipoEmailService } from '../../catalogos/casos_de_uso/catalogo-tipo-email.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { CrearEmailMenorEdadService } from './crear-email-menor-edad.service';
import { CrearEmailService } from './crear-email.service';
const mongoose = require('mongoose');

@Injectable()
export class NuevoEmailVerificacionCuentaService {
  constructor(
    @Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreEstado: CatalogoEstadoService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoTipoEmailService: CatalogoTipoEmailService,
    private crearEmailService: CrearEmailService,
    private crearEmailMenorEdadService: CrearEmailMenorEdadService,
  ) {}

  async nuevoEmailVerificacionCuenta(idUsuario: any): Promise<any> {
    const session = await mongoose.startSession();
    await session.startTransaction();

    //Obtiene el codigo de la entidad Email
    const entidad = await this.nombreEntidad.obtenerNombreEntidad(
      nombreEntidades.email,
    );
    let codEntidad = entidad.codigo;

    //Obtiene el estado de la entidad email
    const estado = await this.nombreEstado.obtenerNombreEstado(
      nombrecatalogoEstados.bloqueado,
      codEntidad,
    );
    let codEstado = estado.codigo;

    try {
      const opts = { session };

      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
        idUsuario,
      );

      if (getUsuario.estado === estadosUsuario.activaNoVerificado
        || getUsuario.estado === estadosUsuario.activa) {
        //______________ actualizar estado email anterior___________________
        getUsuario.menorEdad
          ? await this.emailModel.updateMany(
              {
                usuario: idUsuario,
                codigo: codigoCatalogosTipoEmail.validacion_correo_responsable,
              },
              { estado: codEstado },
            )
          : await this.emailModel.updateMany(
              {
                usuario: idUsuario,
                codigo: codigoCatalogosTipoEmail.validacion_correo_normal,
              },
              { estado: codEstado },
            );

        //____________________________data nuevo email_____________________________

        const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(
          getUsuario.idioma,
        );
        const dataEmail: any = {
          usuario: getUsuario._id,
          emailDestinatario: '',
          menorEdad: getUsuario.menorEdad,
          codigo: '',
          idioma: idiomaUsuario.codNombre,
          nombre: getUsuario.perfiles[0].nombre,
        };

        console.log('dataEmail', dataEmail);

        if (getUsuario.menorEdad) {
          const codTipEmaR = await this.catalogoTipoEmailService.obtenerCodigoTipoEmail(
            'validacionResponsable',
          );
          let codEstadoValiRes = codTipEmaR.codigo;
          dataEmail.codigo = codEstadoValiRes;
          dataEmail.emailDestinatario = getUsuario.emailResponsable;

          await this.crearEmailService.crearEmail(dataEmail, opts);

          await this.crearEmailMenorEdadService.crearEmailMenorEdad(
            getUsuario,
            opts,
          );
        } else {
          const codTipEma = await this.catalogoTipoEmailService.obtenerCodigoTipoEmail(
            'validacion',
          );
          let codTipoEmailValid = codTipEma.codigo;
          dataEmail.menorEdad = false;
          dataEmail.emailDestinatario = getUsuario.email;
          dataEmail.codigo = codTipoEmailValid;

          await this.crearEmailService.crearEmail(dataEmail, opts);
        }
        await session.commitTransaction();
        await session.endSession();

        return true;
      } else {
        throw {
          codigo: HttpStatus.NOT_FOUND,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }
    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      throw error;
    }
  }
}
