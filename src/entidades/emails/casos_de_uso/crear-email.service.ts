import { HttpService, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { v4 as uuidv4 } from 'uuid';
import { ConfigService } from '../../../config/config.service';
import { pathServerM } from '../../../shared/enum-servidor-mantenimiento';
const mongoose = require('mongoose');

@Injectable()
export class CrearEmailService {
  constructor(
    @Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private httpService: HttpService,
    private config: ConfigService,
  ) {}

  async crearEmail(crearEmailDto: any, opts?: any): Promise<any> {
    const accion = await this.nombreAccion.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    const entidad = await this.nombreEntidad.obtenerNombreEntidad(
      nombreEntidades.email,
    );
    let codEntidad = entidad.codigo;

    const estado = await this.nombreEstado.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    let codEstado = estado.codigo;

    const fechaCreacion = new Date();
    const fechaValid = this.sumarDias(new Date());
    //const token = this.random() + this.random() + this.random();
    const token = uuidv4().toString();

    let session;
    let optsLocal;
    if (!opts) {
      session = await mongoose.startSession();
      session.startTransaction();
    }

    try {
      if (!opts) {
        optsLocal = true;
        opts = { session };
      }

      const nuevoEmail = {
        usuario: crearEmailDto.usuario,
        emailDestinatario: crearEmailDto.emailDestinatario,
        estado: codEstado,
        fechaCreacion: fechaCreacion,
        fechaValidacion: fechaValid,
        tokenEmail: token,
        codigo: crearEmailDto.codigo,
      };

      const dataEmail = {
        tokenEmail: token,
        emailDestinatario: crearEmailDto.emailDestinatario,
        idioma: crearEmailDto.idioma,
        usuario: crearEmailDto.usuario,
        codigo: crearEmailDto.codigo,
        nombre: crearEmailDto.nombre,
      };
      const getEmail = await this.envioEmail(dataEmail).toPromise();

      if (getEmail.data.respuesta.datos?.response) {
        nuevoEmail['enviado'] = true;
        nuevoEmail['descripcionEnvio'] = getEmail.data.respuesta.datos.response;
      } else {
        nuevoEmail['enviado'] = false;
        nuevoEmail['descripcionEnvio'] =
          getEmail.data.respuesta.datos?.code ||
          getEmail.data.respuesta.mensaje;
      }
      const email = await new this.emailModel(nuevoEmail).save(opts);

      //datos para guardar el historico
      const newHistorico: any = {
        datos: email,
        usuario: nuevoEmail.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      if (optsLocal) {
        await session.commitTransaction();
        await session.endSession();
      }
      return true;
    } catch (error) {
      if (optsLocal) {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      throw error;
    }
  }
  envioEmail(nuevoEmail): Observable<any> {
    try {
      const apiKey = this.config.get('API_KEY');
      const URL_SERVER_MANTENIMIENTO = this.config.get(
        'URL_SERVER_MANTENIMIENTO',
      );
      // const URL_SERVER_MANTENIMIENTO = 'http://localhost:5000'
      const headersRequest: any = {
        'Content-Type': 'application/json',
        apiKey: apiKey,
      };
      return this.httpService.post(
        `${URL_SERVER_MANTENIMIENTO}${pathServerM.CONFIRMACION_EMAIL}`,
        nuevoEmail,
        { headers: headersRequest },
      );
    } catch (error) {
      throw error;
    }
  }
  sumarDias(fecha) {
    fecha.setDate(fecha.getDate() + 15);
    return fecha;
  }
  random() {
    return Math.random()
      .toString(36)
      .substr(2);
  }
}
