import { HttpModule, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { emailProviders } from '../drivers/email.provider';
import { CatalogosServiceModule } from './../../catalogos/casos_de_uso/catalogos-services.module';
import { ActualizarEmailResponsableService } from './actualizar-email-responsable.service';
import { ConfirmarEmailService } from './confirmar-email.service';
import { CrearEmailActualizarDataEmailService } from './crear-email-actualizar-data-email.service';
import { CrearEmailBienvenidaService } from './crear-email-bienvenida.service';
import { ConfirmacionRecuperarContraseniaService } from './crear-email-confirmacion-recuperar-contrasenia.service';
import { CrearEmailMenorEdadService } from './crear-email-menor-edad.service';
import { CrearEmailPeticionDatosService } from './crear-email-peticion-datos.service';
import { CrearEmailPeticionEliminacionDatosService } from './crear-email-peticion-eliminacion-datos.service';
import { CrearEmailReciboPagoService } from './crear-email-recibo-pago.service';
import { CrearEmailRecuperarContraseniaService } from './crear-email-recuperar-contrasenia.service';
import { CrearEmailTransferirProyectoService } from './crear-email-transferir-proyecto.service';
import { CrearEmailService } from './crear-email.service';
import { NuevoEmailVerificacionCuentaService } from './nuevo-email-verificacion.service';
import { ObtenerEmailService } from './obtener-email.service';

@Module({
  imports: [
    DBModule,
    HttpModule,
    CatalogosServiceModule,
    UsuarioServicesModule,
  ],
  providers: [
    ...emailProviders,
    CrearEmailService,
    ConfirmarEmailService,
    ActualizarEmailResponsableService,
    CrearEmailTransferirProyectoService,
    CrearEmailRecuperarContraseniaService,
    ConfirmacionRecuperarContraseniaService,
    CrearEmailPeticionDatosService,
    CrearEmailPeticionEliminacionDatosService,
    CrearEmailMenorEdadService,
    ObtenerEmailService,
    CrearEmailActualizarDataEmailService,
    NuevoEmailVerificacionCuentaService,
    CrearEmailReciboPagoService,
    CrearEmailBienvenidaService,
  ],
  exports: [
    CrearEmailService,
    ConfirmarEmailService,
    ActualizarEmailResponsableService,
    CrearEmailTransferirProyectoService,
    CrearEmailRecuperarContraseniaService,
    ConfirmacionRecuperarContraseniaService,
    CrearEmailPeticionDatosService,
    CrearEmailPeticionEliminacionDatosService,
    CrearEmailMenorEdadService,
    ObtenerEmailService,
    CrearEmailActualizarDataEmailService,
    NuevoEmailVerificacionCuentaService,
    CrearEmailReciboPagoService,
    CrearEmailBienvenidaService,
  ],
  controllers: [],
})
export class EmailServicesModule {}
