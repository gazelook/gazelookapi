import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoTipoEmailService } from 'src/entidades/catalogos/casos_de_uso/catalogo-tipo-email.service';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreCatalogoTipoEmail,
  nombreEntidades,
  estadosUsuario,
} from '../../../shared/enum-sistema';
import * as mongoose from 'mongoose';

@Injectable()
export class ConfirmarEmailService {
  constructor(
    @Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private nombreTipoEmail: CatalogoTipoEmailService,
    private actualizarUsuario: ActualizarUsuarioService,
  ) {}

  async confirmarEmail(tokenVa: string): Promise<any> {
    const accion = await this.nombreAccion.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    let getAccion = accion.codigo;

    const entidad = await this.nombreEntidad.obtenerNombreEntidad(
      nombreEntidades.email,
    );
    let codEntidad = entidad.codigo;

    const estadoAc = await this.nombreEstado.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    let codEstadoAc = estadoAc.codigo;

    const estadoBl = await this.nombreEstado.obtenerNombreEstado(
      nombrecatalogoEstados.bloqueado,
      codEntidad,
    );
    let codEstadoBl = estadoBl.codigo;

    const codTipEma = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
      nombreCatalogoTipoEmail.validacion,
    );
    let codTipoEmailValid = codTipEma.codigo;

    const codTipEmaR = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
      nombreCatalogoTipoEmail.validacionResponsable,
    );
    let codEstadoValiRes = codTipEmaR.codigo;

    const codTipTransProy = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
      nombreCatalogoTipoEmail.trasferirProyecto,
    );
    let codEstadoTransProyecto = codTipTransProy.codigo;

    const codTipRecContras = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
      nombreCatalogoTipoEmail.recuperarContrasenia,
    );
    let codEstadoRecContrasenia = codTipRecContras.codigo;

    const codTipPeticionDatos = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
      nombreCatalogoTipoEmail.peticionDatos,
    );
    let codPeticionDatos = codTipPeticionDatos.codigo;

    const codTipEliminacionDatos = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
      nombreCatalogoTipoEmail.eliminacionDatos,
    );
    let codPeticionEliminacionDatos = codTipEliminacionDatos.codigo;

    console.log('tokenVa: ', tokenVa);
    const fechaActual = new Date();

    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      const opts = { session };
      let verificacion = false;

      const verificarLink = await this.emailModel
        .findOne({ tokenEmail: tokenVa, estado: codEstadoAc })
        .session(opts.session);
      if (verificarLink) {
        if (verificarLink.codigo === codTipoEmailValid) {
          const datoActua = {
            estado: codEstadoBl,
          };
          const emailVerifica = {
            emailVerificado: true,
            estado: estadosUsuario.activa,
          };
          const actualizarEmail = await this.emailModel.findByIdAndUpdate(
            verificarLink._id,
            datoActua,
            opts,
          );

          const actualizarUsuario = await this.actualizarUsuario.actualizarEstadoEmailUsuario(
            verificarLink.usuario,
            emailVerifica,
            opts,
          );
          //return true;
          verificacion = true;
        } else if (verificarLink.codigo === codEstadoValiRes) {
          const fechaAct = this.getDateTime(fechaActual);
          const fechaLink = this.getDateTime(verificarLink.fechaValidacion);

          if (fechaAct <= fechaLink) {
            const datoActua = {
              estado: codEstadoBl,
            };
            const actualizarEmail = await this.emailModel.findByIdAndUpdate(
              verificarLink._id,
              datoActua,
              opts,
            );
            //return true;
            verificacion = true;
          } else {
            //return false;
            verificacion = false;
          }
        } else if (verificarLink.codigo === codEstadoTransProyecto) {
          const fechaAct = this.getDateTime(fechaActual);
          const fechaLink = this.getDateTime(verificarLink.fechaValidacion);

          if (fechaAct <= fechaLink) {
            const datoActua = {
              estado: codEstadoBl,
            };
            const actualizarEmail = await this.emailModel.findByIdAndUpdate(
              verificarLink._id,
              datoActua,
              opts,
            );
            //return true;
            verificacion = true;
          } else {
            //return false;
            verificacion = false;
          }
        } else if (verificarLink.codigo === codEstadoRecContrasenia) {
          const fechaAct = this.getDateTime(fechaActual);
          const fechaLink = this.getDateTime(verificarLink.fechaValidacion);

          if (fechaAct <= fechaLink) {
            const datoActua = {
              estado: codEstadoBl,
            };
            const actualizarEmail = await this.emailModel.findByIdAndUpdate(
              verificarLink._id,
              datoActua,
              opts,
            );
            //return true;
            verificacion = true;
          } else {
            //return false;
            verificacion = false;
          }
        } else if (verificarLink.codigo === codPeticionDatos) {
          const fechaAct = this.getDateTime(fechaActual);
          const fechaLink = this.getDateTime(verificarLink.fechaValidacion);

          if (fechaAct <= fechaLink) {
            const datoActua = {
              estado: codEstadoBl,
            };
            const actualizarEmail = await this.emailModel.findByIdAndUpdate(
              verificarLink._id,
              datoActua,
              opts,
            );
            //return true;
            verificacion = true;
          } else {
            //return false;
            verificacion = false;
          }
        } else if (verificarLink.codigo === codPeticionEliminacionDatos) {
          const fechaAct = this.getDateTime(fechaActual);
          const fechaLink = this.getDateTime(verificarLink.fechaValidacion);

          if (fechaAct <= fechaLink) {
            const datoActua = {
              estado: codEstadoBl,
            };
            const actualizarEmail = await this.emailModel.findByIdAndUpdate(
              verificarLink._id,
              datoActua,
              opts,
            );
            //return true;
            verificacion = true;
          } else {
            //return false;
            verificacion = false;
          }
        }

        if (verificacion) {
          const getEmail = await this.emailModel
            .findOne({ tokenEmail: tokenVa })
            .session(opts.session);
          let newConfEmailHistorico = {
            _id: getEmail._id,
            emailDestinatario: getEmail.emailDestinatario,
            enviado: getEmail.enviado,
            fechaValidacion: getEmail.fechaValidacion,
          };

          const newHistorico: any = {
            datos: newConfEmailHistorico,
            usuario: getEmail.usuario,
            accion: getAccion,
            entidad: codEntidad,
          };
          //Llama a clase generica para almacenar historico
          this.crearHistoricoService.crearHistoricoServer(newHistorico);

          await session.commitTransaction();
          session.endSession();

          return verificacion;
        } else {
          await session.abortTransaction();
          session.endSession();
          return verificacion;
        }
      } else {
        await session.abortTransaction();
        session.endSession();
        return false;
      }
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }

  getDateTime(date) {
    let year;
    let month;
    let day;

    year = date.getFullYear();
    month = date.getMonth() + 1;
    month = (month < 10 ? '0' : '') + month;
    day = date.getDate();
    day = (day < 10 ? '0' : '') + day;

    return year + '-' + month + '-' + day;
  }
}
