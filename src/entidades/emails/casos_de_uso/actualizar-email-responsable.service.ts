import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class ActualizarEmailResponsableService {
  constructor(
    @Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async actualizarEmailResponsable(usuario: string, opts: any): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccion = accion.codigo;

      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.email,
      );
      let codEntidad = entidad.codigo;

      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstado = estado.codigo;

      const estadoBlo = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.bloqueado,
        codEntidad,
      );
      let codEstadoBlo = estadoBlo.codigo;

      const emailActualizar = await this.emailModel.find({
        $and: [{ usuario: usuario }, { estado: codEstado }],
      });
      const datoActua = {
        estado: codEstadoBlo,
      };
      const actualizarEmail = await this.emailModel.findByIdAndUpdate(
        emailActualizar[0]._id,
        datoActua,
        opts,
      );

      const newHistorico: any = {
        datos: actualizarEmail,
        usuario: emailActualizar[0].usuario,
        accion: getAccion,
        entidad: codEntidad,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return actualizarEmail;
    } catch (error) {
      throw error;
    }
  }

  async bloquearEmailResponsable(idEmail: string, opts: any): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccion = accion.codigo;

      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.email,
      );
      let codEntidad = entidad.codigo;

      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstado = estado.codigo;

      const estadoBlo = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.bloqueado,
        codEntidad,
      );
      let codEstadoBlo = estadoBlo.codigo;

      const email = await this.emailModel.findOne({
        $and: [{ _id: idEmail }, { estado: codEstado }],
      });
      const datoActua = {
        estado: codEstadoBlo,
      };
      const actualizarEmail = await this.emailModel.findByIdAndUpdate(
        email._id,
        datoActua,
        opts,
      );

      const newHistorico: any = {
        datos: actualizarEmail,
        usuario: email.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return actualizarEmail;
    } catch (error) {
      throw error;
    }
  }
}
