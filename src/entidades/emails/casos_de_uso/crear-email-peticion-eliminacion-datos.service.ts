import { HttpService, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { v4 as uuidv4 } from 'uuid';
import { ConfigService } from '../../../config/config.service';
import { pathServerM } from '../../../shared/enum-servidor-mantenimiento';
import {
  codigoCatalogosTipoEmail,
  codigosCatalogoHistorico,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
const mongoose = require('mongoose');

@Injectable()
export class CrearEmailPeticionEliminacionDatosService {
  constructor(
    @Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private httpService: HttpService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private readonly catalogoIdiomas: CatalogoIdiomasService,
    private config: ConfigService,
  ) {}

  async crearEmailPeticionEliminacionDatos(idUsuario: any): Promise<any> {
    //Obtiene el codigo de la accion crear
    const accion = await this.nombreAccion.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene el codigo de la entidad Email
    const entidad = await this.nombreEntidad.obtenerNombreEntidad(
      nombreEntidades.email,
    );
    let codEntidad = entidad.codigo;

    //Obtiene el estado activa de la entidad email
    const estado = await this.nombreEstado.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    let codEstado = estado.codigo;

    const fechaCreacion = new Date();
    const fechaValid = this.sumarDias(new Date());
    //const token = this.random() + this.random() + this.random();
    const token = uuidv4().toString();

    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      const getUsuario: any = await this.obtenerIdUsuarioService.obtenerUsuarioById(
        idUsuario,
      );
      const idiomaUsuario = await this.catalogoIdiomas.obtenerIdiomaByCodigo(
        getUsuario.idioma,
      );
      const nuevoEmail = {
        usuario: getUsuario._id,
        emailDestinatario: getUsuario.email,
        estado: codEstado,
        fechaCreacion: fechaCreacion,
        fechaValidacion: fechaValid,
        tokenEmail: token,
        codigo: codigoCatalogosTipoEmail.eliminacion_datos,
        datosProceso: {
          nombre: getUsuario.perfiles[0].nombre,
          idioma: idiomaUsuario.codNombre,
        },
      };
      if (getUsuario.menorEdad) {
        nuevoEmail.emailDestinatario = getUsuario.emailResponsable;
      }
      const dataEmail = {
        tokenEmail: token,
        emailDestinatario: nuevoEmail.emailDestinatario,
        nombre: getUsuario.perfiles[0].nombre,
        idioma: idiomaUsuario.codNombre,
      };
      const getEmail = await this.envioEmail(dataEmail).toPromise();

      if (getEmail.data.respuesta.datos?.response) {
        nuevoEmail['enviado'] = true;
        nuevoEmail['descripcionEnvio'] = getEmail.data.respuesta.datos.response;
      } else {
        nuevoEmail['enviado'] = false;
        nuevoEmail['descripcionEnvio'] = getEmail.data.respuesta.datos.code;
      }
      const email = await new this.emailModel(nuevoEmail).save(opts);
      // historico de email
      let newHistorico: any = {
        datos: nuevoEmail,
        usuario: getUsuario._id,
        accion: getAccion,
        entidad: codEntidad,
      };

      // ________________historico solicitud eliminacion datos__________
      const dataUsuario = {
        _id: getUsuario._id,
        email: getUsuario.email,
        emailResponsable: getUsuario?.emailResponsable,
        nombre: getUsuario.perfiles[0]?.nombre,
        nombreContacto: getUsuario.perfiles[0]?.nombreContacto,
      };

      let historicoSolElimDatos: any = {
        datos: dataUsuario,
        usuario: getUsuario._id,
        accion: getAccion,
        entidad: codEntidad,
        tipo: codigosCatalogoHistorico.solicitudRetiro,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      this.crearHistoricoService.crearHistoricoServer(historicoSolElimDatos);

      await session.commitTransaction();
      await session.endSession();

      return true;
    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      throw error;
    }
  }
  envioEmail(dataEmail): Observable<any> {
    try {
      const apiKey = this.config.get('API_KEY');
      const URL_SERVER_MANTENIMIENTO = this.config.get(
        'URL_SERVER_MANTENIMIENTO',
      );
      // const URL_SERVER_MANTENIMIENTO = 'http://localhost:5000'
      const headersRequest: any = {
        'Content-Type': 'application/json',
        apiKey: apiKey,
      };
      return this.httpService.post(
        `${URL_SERVER_MANTENIMIENTO}${pathServerM.ELIMINACION_DATOS}`,
        dataEmail,
        { headers: headersRequest },
      );
    } catch (error) {
      throw error;
    }
  }
  sumarDias(fecha) {
    fecha.setDate(fecha.getDate() + 1);
    return fecha;
  }
  random() {
    return Math.random()
      .toString(36)
      .substr(2);
  }
}
