import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import {
  codigoCatalogosTipoEmail,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class ObtenerEmailService {
  constructor(
    @Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
  ) {}

  // obtiene todos los emails que el usuario responsable no ha confirmado
  // para bloquear la cuenta
  async obtenerEmailsResponsable(): Promise<any> {
    try {
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.email,
      );
      let codEntidad = entidad.codigo;

      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );

      const getEmails = await this.emailModel.find({
        $and: [
          { fechaValidacion: { $lt: new Date() } },
          { estado: estado.codigo },
          { codigo: codigoCatalogosTipoEmail.validacion_correo_responsable },
        ],
      });
      return getEmails;
    } catch (error) {
      throw error;
    }
  }

  async obtenerEmailByToken(tokenEmail: string): Promise<Email> {
    const getEmails = await this.emailModel.findOne({ tokenEmail: tokenEmail });
    return getEmails;
  }
}
