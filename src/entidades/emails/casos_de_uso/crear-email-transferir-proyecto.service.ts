import { HttpService, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { v4 as uuidv4 } from 'uuid';
import { ConfigService } from '../../../config/config.service';
import { pathServerM } from '../../../shared/enum-servidor-mantenimiento';

@Injectable()
export class CrearEmailTransferirProyectoService {
  constructor(
    @Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private httpService: HttpService,
    private config: ConfigService,
  ) {}

  async crearEmailTrasferirProyecto(
    crearEmailDto: any,
    opts: any,
  ): Promise<any> {
    //Obtiene el codigo de la accion crear
    const accion = await this.nombreAccion.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene el codigo de la entidad Email
    const entidad = await this.nombreEntidad.obtenerNombreEntidad(
      nombreEntidades.email,
    );
    let codEntidad = entidad.codigo;

    //Obtiene el estado activa de la entidad email
    const estado = await this.nombreEstado.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    let codEstado = estado.codigo;

    const fechaCreacion = new Date();
    const fechaValid = this.sumarDias(new Date());
    //const token = this.random() + this.random() + this.random();
    const token = uuidv4().toString();

    try {
      const nuevoEmail = {
        perfil: crearEmailDto.perfilPropietario,
        emailDestinatario: crearEmailDto.emailDestinatario,
        estado: codEstado,
        fechaCreacion: fechaCreacion,
        fechaValidacion: fechaValid,
        tokenEmail: token,
        codigo: crearEmailDto.codigo,
        datosProceso: {
          perfilPropietario: crearEmailDto.perfilPropietario,
          nombrePropietario: crearEmailDto.nombrePropietario,
          nombrePerfilNuevo: crearEmailDto.nombrePerfilNuevo,
          perfilNuevo: crearEmailDto.perfilNuevo,
          idProyecto: crearEmailDto.idProyecto,
          titulo: crearEmailDto.titulo,
          tituloCorto: crearEmailDto.tituloCorto,
          descripcion: crearEmailDto.descripcion,
          idioma: crearEmailDto.idioma,
        },
      };
      crearEmailDto.tokenEmail = token;
      const dataEmail = {
        tokenEmail: token,
        emailDestinatario: nuevoEmail.emailDestinatario,
        datosProceso: nuevoEmail.datosProceso,
      };
      const getEmail = await this.envioEmail(dataEmail).toPromise();

      if (getEmail.data.respuesta.datos?.response) {
        nuevoEmail['enviado'] = true;
        nuevoEmail['descripcionEnvio'] = getEmail.data.respuesta.datos.response;
      } else {
        console.log('fallo email: ', getEmail.data.respuesta);
        nuevoEmail['enviado'] = false;
        nuevoEmail['descripcionEnvio'] = getEmail.data.respuesta.datos.code;
      }
      const email = await new this.emailModel(nuevoEmail).save(opts);

      let newHistorico: any = {
        datos: email,
        usuario: crearEmailDto.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };

      return this.crearHistoricoService.crearHistoricoServer(newHistorico);
    } catch (error) {
      throw error;
    }
  }
  envioEmail(crearEmailDto): Observable<any> {
    try {
      const apiKey = this.config.get('API_KEY');
      const URL_SERVER_MANTENIMIENTO = this.config.get(
        'URL_SERVER_MANTENIMIENTO',
      );
      const headersRequest: any = {
        'Content-Type': 'application/json',
        apiKey: apiKey,
      };
      return this.httpService.post(
        `${URL_SERVER_MANTENIMIENTO}${pathServerM.TRANSFERIR_PROYECTO}`,
        crearEmailDto,
        { headers: headersRequest },
      );
    } catch (error) {
      throw error;
    }
  }
  sumarDias(fecha) {
    fecha.setDate(fecha.getDate() + 15);
    return fecha;
  }
  random() {
    return Math.random()
      .toString(36)
      .substr(2);
  }
}
