import { Module, HttpModule } from '@nestjs/common';
import { EmailControllerModule } from './controladores/email-controller.module';

@Module({
  imports: [EmailControllerModule, HttpModule],
  providers: [],
  controllers: [],
  exports: [],
})
export class EmailModule {}
