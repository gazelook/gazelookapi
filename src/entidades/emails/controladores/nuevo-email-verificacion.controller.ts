import {
  Controller,
  Headers,
  HttpStatus,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from '../../../shared/funcion';
import { NuevoEmailVerificacionCuentaService } from '../casos_de_uso/nuevo-email-verificacion.service';

@Controller('api/email/nuevo-email-verificacion')
export class NuevoEmailVerificacionControlador {
  constructor(
    private readonly nuevoEmailVerificacionCuentaService: NuevoEmailVerificacionCuentaService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Post('/:idUsuario')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: 'Ok',
    description: 'El email ha sido enviado',
  })
  @ApiResponse({ status: 404, description: 'Error al crear el email' })
  @ApiOperation({
    summary: 'Envia un nuevo email para la verificación de la cuenta.',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  public async crearEmail(@Headers() headers, @Param('idUsuario') idUsuario) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const emailCreado = await this.nuevoEmailVerificacionCuentaService.nuevoEmailVerificacionCuenta(
        idUsuario,
      );

      if (!emailCreado) {
        //llama al metodo de traduccion estatica
        const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_CREACION',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.CONFLICT,
          mensaje: ERROR_CREACION,
        });
      } else {
        const EMAIL_REGISTRADO = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'EMAIL_REGISTRADO',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.CREATED,
          mensaje: EMAIL_REGISTRADO,
          datos: emailCreado,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
