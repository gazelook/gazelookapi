import { Controller, HttpStatus, Get, Param, Render } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { I18nService } from 'nestjs-i18n';
import { EliminarInformacionCompleto } from '../../usuario/casos_de_uso/eliminar-informacion-completo.service';
import { Funcion } from '../../../shared/funcion';

@Controller('api/email')
export class ConfirmarEmailPeticionEliminacionDatosControlador {
  constructor(
    private readonly confirmarEmailService: ConfirmarEmailService,
    private readonly eliminarInformacionCompleto: EliminarInformacionCompleto,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get(
    '/confirmar-peticion-eliminacion-datos/:token/:idUsuario/:idioma/:validar',
  )
  @Render('eliminacionDatos')
  public async confirmarEmailPeticionEliminacionDatos(
    @Param('token') token,
    @Param('idUsuario') idUsuario,
    @Param('idioma') idioma,
    @Param('validar') validar,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(idioma);
    try {
      const verificaEmail = await this.confirmarEmailService.confirmarEmail(
        token,
      );

      if (verificaEmail && validar === 'true') {
        const peticionEliminacionDatos = await this.eliminarInformacionCompleto.eliminarInformacionCompletoUsuario(
          idUsuario,
          token,
        );
        if (peticionEliminacionDatos) {
          const DATOS_ELIMINADOS = await this.i18n.translate(
            codIdioma.concat('.DATOS_ELIMINADOS'),
            {
              lang: codIdioma,
            },
          );
          return { eliminacionOk: DATOS_ELIMINADOS };
        }
      } else {
        const LINK_NO_VALIDO = await this.i18n.translate(
          codIdioma.concat('.LINK_NO_VALIDO'),
          {
            lang: codIdioma,
          },
        );
        return { eliminacionOk: LINK_NO_VALIDO };
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
