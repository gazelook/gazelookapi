import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Put,
  Res,
} from '@nestjs/common';
import * as mongoose from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from '../../../shared/funcion';
import { CrearEmailService } from '../casos_de_uso/crear-email.service';
import { ActualizarEmailResponsableDto } from '../entidad/email.dto';
import { ActualizarEmailResponsableService } from './../casos_de_uso/actualizar-email-responsable.service';

@Controller('api/email')
export class ActualizarEmailResponsableControlador {
  constructor(
    private readonly actualizarEmailService: ActualizarEmailResponsableService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly crearEmailService: CrearEmailService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/actualizar-email')
  public async actualizarEmailResponsable(
    @Res() res,
    @Body() actualizarEmailDto: ActualizarEmailResponsableDto,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const opts = { session };
      const updateEmail = await this.actualizarEmailService.actualizarEmailResponsable(
        actualizarEmailDto._id,
        opts,
      );
      const emailCreado = await this.crearEmailService.crearEmail(
        actualizarEmailDto,
        opts,
      );

      if (!emailCreado) {
        await session.abortTransaction();
        session.endSession();
        const ERROR_ACTUALIZAR = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_ACTUALIZAR',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_ACTUALIZAR,
        });
      } else {
        await session.commitTransaction();
        session.endSession();
        const ACTUALIZACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ACTUALIZACION_CORRECTA',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ACTUALIZACION_CORRECTA,
        });
      }
    } catch (e) {
      await session.abortTransaction();
      session.endSession();
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
