import { ActualizarEmailResponsableControlador } from './actualizar-email-responsable.controller';
import { EmailServicesModule } from '../casos_de_uso/email-services.module';
import { Module, forwardRef } from '@nestjs/common';
import { CrearEmailControlador } from './crear-email.controller';
import { ConfirmarEmailControlador } from './confirmar-email.controller';
import { ProyectosServicesModule } from 'src/entidades/proyectos/casos_de_uso/proyectos-services.module';
import { ConfirmarEmailRecuperarContraseniaControlador } from './confirmar-email-recuperar-contrasenia.controller';
import { CrearEmailPeticionDatosControlador } from './crear-email-peticion-datos.controller';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { CrearEmailEliminacionDatosControlador } from './crear-email-eliminacion-datos.controller';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from '../../../shared/funcion';
import { NuevoEmailVerificacionControlador } from './nuevo-email-verificacion.controller';

@Module({
  imports: [
    EmailServicesModule,
    forwardRef(() => ProyectosServicesModule),
    forwardRef(() => UsuarioServicesModule),
  ],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    CrearEmailControlador,
    ConfirmarEmailControlador,
    //ConfirmarEmailTranferirProyectoControlador,
    ActualizarEmailResponsableControlador,
    ConfirmarEmailRecuperarContraseniaControlador,
    //ConfirmarEmailPeticionDatosControlador,
    CrearEmailPeticionDatosControlador,
    //ConfirmarEmailPeticionEliminacionDatosControlador,
    CrearEmailEliminacionDatosControlador,
    NuevoEmailVerificacionControlador,
  ],
})
export class EmailControllerModule {}
