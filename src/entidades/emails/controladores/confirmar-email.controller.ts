import {
  Controller,
  HttpStatus,
  Get,
  Param,
  Render,
  Headers,
} from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';

@Controller('api/email')
export class ConfirmarEmailControlador {
  constructor(
    private readonly confirmarEmailService: ConfirmarEmailService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/confirmar-email/:token')
  @Render('index')
  public async crearDispositivo(@Param('token') token, @Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const verificaEmail = await this.confirmarEmailService.confirmarEmail(
        token,
      );

      if (verificaEmail) {
        const EMAIL_VERIFICADO = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'EMAIL_VERIFICADO',
        );
        return { message: EMAIL_VERIFICADO };
      } else {
        const LINK_NO_VALIDO = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'LINK_NO_VALIDO',
        );
        return { message: LINK_NO_VALIDO };
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
