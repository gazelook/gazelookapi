import { Body, Controller, Headers, HttpStatus, Post } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from '../../../shared/funcion';
import { CrearEmailService } from '../casos_de_uso/crear-email.service';
import { EmailDto } from '../entidad/email.dto';

@Controller('api/email')
export class CrearEmailControlador {
  constructor(
    private readonly crearEmailService: CrearEmailService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}
  @Post('/crear-email')
  public async crearEmail(
    @Body() createEmailDTO: EmailDto,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const emailCreado = await this.crearEmailService.crearEmail(
        createEmailDTO,
      );

      if (!emailCreado) {
        //llama al metodo de traduccion estatica
        const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_CREACION',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_CREACION,
        });
      } else {
        const EMAIL_REGISTRADO = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'EMAIL_REGISTRADO',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.CREATED,
          mensaje: EMAIL_REGISTRADO,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
