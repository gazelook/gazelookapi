import {
  Controller,
  Post,
  HttpStatus,
  Param,
  Headers,
  UseGuards,
} from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { CrearEmailPeticionEliminacionDatosService } from '../casos_de_uso/crear-email-peticion-eliminacion-datos.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import * as mongoose from 'mongoose';
import { ApiSecurity } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

@Controller('api/email')
@ApiSecurity('Authorization')
@UseGuards(AuthGuard('jwt'))
export class CrearEmailEliminacionDatosControlador {
  constructor(
    private readonly crearEmailPeticionEliminacionDatosService: CrearEmailPeticionEliminacionDatosService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Post('/crear-email-peticion-eliminacion-datos/:idusuario')
  public async crearEmailPeticionDatos(
    @Param('idusuario') idUsuario: string,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (mongoose.isValidObjectId(idUsuario)) {
        const emailCreado = await this.crearEmailPeticionEliminacionDatosService.crearEmailPeticionEliminacionDatos(
          idUsuario,
        );

        if (!emailCreado) {
          //llama al metodo de traduccion estatica
          const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ERROR_CREACION',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        } else {
          const EMAIL_REGISTRADO = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'EMAIL_REGISTRADO',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: EMAIL_REGISTRADO,
            datos: true,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
