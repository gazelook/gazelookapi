import { Controller, HttpStatus, Get, Param, Render } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';

@Controller('api/email')
export class ConfirmarEmailRecuperarContraseniaControlador {
  constructor(
    private readonly confirmarEmailService: ConfirmarEmailService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/confirmar-email-recuperar-contrasenia/:token/:email/:idioma')
  @Render('indexNewPass')
  public async confirmarEmailRecuperarContrasenia(
    @Param('token') token,
    @Param('email') email,
    @Param('idioma') idioma,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(idioma);

    try {
      const verificaEmail = await this.confirmarEmailService.confirmarEmail(
        token,
      );
      if (verificaEmail) {
        return { email: email };
      } else {
        const LINK_NO_VALIDO = await this.i18n.translate(
          codIdioma.concat('.LINK_NO_VALIDO'),
          {
            lang: codIdioma,
          },
        );
        return { message: LINK_NO_VALIDO };
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
