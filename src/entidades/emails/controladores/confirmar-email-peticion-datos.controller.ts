import { Controller, Get, HttpStatus, Param, Render } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { InformacioAplicacionService } from '../../usuario/casos_de_uso/informacion-aplicacion.service';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';

@Controller('api/email')
export class ConfirmarEmailPeticionDatosControlador {
  constructor(
    private readonly confirmarEmailService: ConfirmarEmailService,
    private readonly informacioAplicacionService: InformacioAplicacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/confirmar-peticion-datos/:token/:idUsuario/:idioma/:validar')
  @Render('solicitudInformacion')
  public async confirmarEmailPeticionDatos(
    @Param('token') token,
    @Param('idUsuario') idUsuario,
    @Param('idioma') idioma,
    @Param('validar') validar,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(idioma);
    try {
      const verificaEmail = await this.confirmarEmailService.confirmarEmail(
        token,
      );

      if (verificaEmail && validar === 'true') {
        this.informacioAplicacionService
          .allInformacionUsuarioAplicacion(idUsuario, codIdioma)
          .then(data => {
            console.log('data');
          });

        const SOLICITUD_ACEPTADA = await this.i18n.translate(
          codIdioma.concat('.SOLICITUD_ACEPTADA'),
          {
            lang: codIdioma,
          },
        );
        return { peticionOk: SOLICITUD_ACEPTADA };
      } else {
        const LINK_NO_VALIDO = await this.i18n.translate(
          codIdioma.concat('.LINK_NO_VALIDO'),
          {
            lang: codIdioma,
          },
        );
        return { peticionOk: LINK_NO_VALIDO };
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
