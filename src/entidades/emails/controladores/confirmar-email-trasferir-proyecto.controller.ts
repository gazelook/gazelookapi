import { Controller, HttpStatus, Get, Param, Render } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { TransferirProyectoService } from 'src/entidades/proyectos/casos_de_uso/trasferir-proyecto.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import * as mongoose from 'mongoose';

@Controller('api/email')
export class ConfirmarEmailTranferirProyectoControlador {
  constructor(
    private readonly confirmarEmailService: ConfirmarEmailService,
    private readonly transferirProyectoService: TransferirProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get(
    '/confirmar-email-trasferir-proyecto/:idProyecto/:idPerfilNuevo/:idPerfilPropietario/:idioma',
  )
  @Render('transferirProyecto')
  public async confirmarEmailTrasferirProyecto(
    @Param('token') token,
    @Param('idProyecto') idProyecto,
    @Param('idPerfilNuevo') idPerfilNuevo,
    @Param('idPerfilPropietario') idPerfilPropietario,
    @Param('idioma') idioma,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(idioma);
    try {
      if (
        mongoose.isValidObjectId(idProyecto) &&
        mongoose.isValidObjectId(idPerfilNuevo) &&
        mongoose.isValidObjectId(idPerfilPropietario)
      ) {
        const verificaEmail = await this.confirmarEmailService.confirmarEmail(
          token,
        );

        if (verificaEmail) {
          const transProyecto = await this.transferirProyectoService.trasferirProyecto(
            idProyecto,
            idPerfilPropietario,
            idPerfilNuevo,
          );
          if (transProyecto) {
            const TRANSFERIR_PROYECTO = await this.i18n.translate(
              codIdioma.concat('.TRANSFERIR_PROYECTO'),
              {
                lang: codIdioma,
              },
            );
            return { transferenciaProyecto: TRANSFERIR_PROYECTO };
          }
        } else {
          const LINK_NO_VALIDO = await this.i18n.translate(
            codIdioma.concat('.LINK_NO_VALIDO'),
            {
              lang: codIdioma,
            },
          );
          return { transferenciaProyecto: LINK_NO_VALIDO };
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return { transferenciaProyecto: PARAMETROS_NO_VALIDOS };
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
