import { Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { Funcion } from 'src/shared/funcion';
import { PerfilProviders } from 'src/entidades/perfil/drivers/perfil.provider';
import { notificacionProviders } from '../drivers/notificacion.provider';
import { CrearNotificacionService } from './crear-notificacion.service';
import { ObtenerEntidadNotificacionService } from './obtener-entidad-notificacion.service';
import { ActualizarNotificacionService } from './actualizar-notificacion.service';

@Module({
  imports: [DBModule, CatalogosServiceModule],
  providers: [
    ...notificacionProviders,
    ...PerfilProviders,
    CrearNotificacionService,
    Funcion,
    ObtenerEntidadNotificacionService,
    ActualizarNotificacionService,
  ],
  exports: [
    CrearNotificacionService,
    Funcion,
    ObtenerEntidadNotificacionService,
    ActualizarNotificacionService,
  ],
  controllers: [],
})
export class NotificacionServicesModule {}
