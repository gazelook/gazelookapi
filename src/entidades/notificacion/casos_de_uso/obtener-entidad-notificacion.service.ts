import { CatalogoAccionService } from './../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from './../../catalogos/casos_de_uso/catalogo-entidad.service';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  codigosCatalogoStatusNotificacion,
  nombreAcciones,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Notificacion } from 'src/drivers/mongoose/interfaces/notificacion/notificacion.interface';

@Injectable()
export class ObtenerEntidadNotificacionService {
  constructor(
    @Inject('NOTIFICACION_MODEL')
    private readonly notificacionModel: Model<Notificacion>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
  ) {}

  // funcion = new Funcion();

  async obtenerIdEntidadNotificacion(idEntidad: string): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      const entidadNot = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.notificacion,
      );
      let codEntidadNotificacion = entidadNot.codigo;

      let getNotificacion = await this.notificacionModel.find({
        idEntidad: idEntidad,
        status: codigosCatalogoStatusNotificacion.entregada,
      });

      let arrayNotificaciones = [];
      if (getNotificacion.length > 0) {
        for (const notif of getNotificacion) {
          arrayNotificaciones.push(notif._id);
        }
      }
      return arrayNotificaciones;
    } catch (error) {
      throw error;
    }
  }
}
