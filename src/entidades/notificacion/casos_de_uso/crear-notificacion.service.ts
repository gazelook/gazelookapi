import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Notificacion } from 'src/drivers/mongoose/interfaces/notificacion/notificacion.interface';
import { nombreAcciones, nombreEntidades } from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { CrearNotificacionDto } from '../entidad/notificacion-dto.service';
import { CatalogoAccionService } from './../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from './../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class CrearNotificacionService {
  constructor(
    @Inject('NOTIFICACION_MODEL')
    private readonly notificacionModel: Model<Notificacion>,
    private crearHistoricoService: CrearHistoricoService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private funcion: Funcion,
  ) {}

  // funcion = new Funcion();

  async crearNotificacion(data: CrearNotificacionDto): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    let session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const options = { session };

      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      const entidadNot = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.notificacion,
      );
      let codEntidadNotificacion = entidadNot.codigo;

      let objNotificacion: any = {
        entidad: data.entidad,
        evento: data.evento,
        data: data.data,
        accion: data.accion,
        estado: data.estado,
        status: data.status,
        idEntidad: data.idEntidad,
        listaPerfiles: data.listaPerfiles,
      };

      let crear = await new this.notificacionModel(objNotificacion).save(
        options,
      );
      let notificacion = await this.notificacionModel
        .findById(crear._id)
        .session(options.session)
        .lean();
      //formato de notificacion
      this.funcion.formatoNotificacion(notificacion);

      if (!data.perfil) {
        data.perfil = null;
      }

      let newHistoricoCrearPartiAso: any = {
        datos: notificacion,
        usuario: data.perfil,
        accion: getAccion,
        entidad: codEntidadNotificacion,
      };

      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoCrearPartiAso,
      );

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return notificacion;
    } catch (error) {
      console.log(error);
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
