import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Notificacion } from 'src/drivers/mongoose/interfaces/notificacion/notificacion.interface';
import { codigosCatalogoStatusNotificacion } from 'src/shared/enum-sistema';
import { notificacionIdDto } from '../entidad/notificacion-dto.service';

@Injectable()
export class ActualizarNotificacionService {
  constructor(
    @Inject('NOTIFICACION_MODEL')
    private readonly notificacionModel: Model<Notificacion>,
  ) {}

  // funcion = new Funcion();

  async actualizarNotificacion(data: notificacionIdDto): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    let session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const options = { session };

      await this.notificacionModel.updateOne(
        { _id: data._id },
        {
          $set: { status: codigosCatalogoStatusNotificacion.leida },
          fechaActualizacion: new Date(),
        },
        options,
      );

      // //Actualiza el estado a eliminado de las traducciones proyecto
      // await this.notificacionModel.updateMany({ idEntidad: idProyecto }, { $set: { estado: codEstadoTProyectoEli, fechaActualizacion: new Date() } }, opts);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return true;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
