import { Module } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { FirebaseModule } from '../../../drivers/firebase/firebase.module';
import { NotificacionServicesModule } from '../casos_de_uso/notificacion-services.module';
import { NotificacionControlador } from './enviar-notificacion.controller';

@Module({
  imports: [NotificacionServicesModule, FirebaseModule],
  providers: [Funcion, TraduccionEstaticaController],
  exports: [],
  controllers: [NotificacionControlador],
})
export class NotificacionControllerModule {}
