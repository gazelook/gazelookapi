import { Body, Controller, Headers, HttpStatus, Post } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { FirebaseNotificacionService } from '../../../drivers/firebase/services/firebase-notificacion.service';
import { EnviarNotificacionDto } from '../entidad/notificacion-dto.service';

@ApiTags('Notificaciones')
@Controller('api/notificacion')
export class NotificacionControlador {
  constructor(
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
    private firebaseNotificacionService: FirebaseNotificacionService,
  ) {}

  @Post('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Envia notificacion a los usuarios cuando llega una nueva solicitud de contacto',
  })
  @ApiResponse({
    status: 200,
    description: 'Notificación enviada correctamente',
  })
  @ApiResponse({ status: 404, description: 'Fallo envío de notificación' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  // @UseGuards(AuthGuard('jwt'))
  public async enviarNotificacion(
    @Headers() headers,
    @Body() enviarNotificacionDto: EnviarNotificacionDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        enviarNotificacionDto.registrationToken.length > 0 &&
        enviarNotificacionDto.message
      ) {
        const registrationToken = enviarNotificacionDto.registrationToken;
        const message = enviarNotificacionDto.message;
        const option = enviarNotificacionDto.options;

        // let enviarNotificacion = this.pushNotificacionService.pushNotificacion();
        // console.log('enviarNotificacion: ', enviarNotificacion)
        let enviarNotificacion = this.firebaseNotificacionService.enviarNotificacion(
          registrationToken,
          message,
          option,
        );

        if (enviarNotificacion) {
          //llama al metodo de traduccion dinamica
          // const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(codIdioma, 'ERROR_CREACION');
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: 'Notificación enviada correctamente',
          });
        }

        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: (await enviarNotificacion).results[0].error,
        });
      } else {
        const PARAMETROS_NO_VALIDOS = await await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      console.log(e);
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
