import { Connection } from 'mongoose';
import { NotificacionModelo } from 'src/drivers/mongoose/modelos/notificacion/notificacion.schema';

export const notificacionProviders = [
  {
    provide: 'NOTIFICACION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('notificacion', NotificacionModelo, 'notificacion'),
    inject: ['DB_CONNECTION'],
  },
];
