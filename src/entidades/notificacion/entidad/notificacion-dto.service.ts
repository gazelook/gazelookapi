import { ApiProperty } from '@nestjs/swagger';
import { CatalogoAccionNotificacionDto } from 'src/entidades/catalogos/entidad/catalogo-accion-notificacion.dto';
import { CatalogoEntidadDto } from 'src/entidades/catalogos/entidad/catalogo-entidad.dto';
import { CatalogoEventoNotificacionDto } from 'src/entidades/catalogos/entidad/catalogo-evento-notificacion.dto';
import { idPerfilDto } from 'src/entidades/perfil/entidad/crear-perfil.dto';

export class CrearNotificacionDto {
  @ApiProperty()
  entidad: any;
  @ApiProperty()
  evento: any;
  @ApiProperty()
  accion: any;
  @ApiProperty()
  data: string;
  @ApiProperty()
  estado: string;
  @ApiProperty()
  status: string;
  @ApiProperty()
  idEntidad: string;
  @ApiProperty()
  perfil: string;
  @ApiProperty()
  listaPerfiles: Array<string>;
}

export class notificacionDto {
  @ApiProperty({ type: CatalogoEntidadDto })
  entidad: CatalogoEntidadDto;
  @ApiProperty({ type: CatalogoEventoNotificacionDto })
  evento: CatalogoEventoNotificacionDto;
  @ApiProperty({ type: CatalogoAccionNotificacionDto })
  accion: CatalogoAccionNotificacionDto;
  @ApiProperty()
  data: any;
  @ApiProperty({ type: [idPerfilDto] })
  listaPerfiles: Array<idPerfilDto>;
  // @ApiProperty({type: EstadoNotificacionDto})
  // estado: EstadoNotificacionDto;
  // @ApiProperty({type: StatusNotificacionDto})
  // status: StatusNotificacionDto;
  @ApiProperty({ type: String })
  idEntidad: string;
}

export class notificacionIdDto {
  @ApiProperty({ type: String })
  _id: string;
}

export class listaNotificacionDto {
  @ApiProperty({ type: [notificacionIdDto] })
  listaNotificaciones: Array<notificacionIdDto>;
}

export class EnviarNotificacionDto {
  @ApiProperty({ type: [String] })
  registrationToken: Array<string>;
  @ApiProperty({})
  message: any;
  @ApiProperty({ type: Object })
  options: Object;
}
