import { ApiProperty } from '@nestjs/swagger';

export class StatusNotificacionDto {
  @ApiProperty()
  codigo: string;
}
