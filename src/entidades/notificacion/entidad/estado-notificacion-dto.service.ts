import { ApiProperty } from '@nestjs/swagger';

export class EstadoNotificacionDto {
  @ApiProperty()
  codigo: string;
}
