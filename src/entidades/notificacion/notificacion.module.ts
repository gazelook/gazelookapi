import { Module } from '@nestjs/common';
import { NotificacionControllerModule } from './controladores/notificacion-controller.module';

@Module({
  imports: [NotificacionControllerModule],
  providers: [],
  controllers: [],
  exports: [],
})
export class NotificacionModule {}
