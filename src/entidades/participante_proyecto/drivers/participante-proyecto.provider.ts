import { ParticipanteProyectoModelo } from './../../../drivers/mongoose/modelos/participante_proyecto/participante_proyecto.schema';
import { Connection } from 'mongoose';

export const participanteProyectoProviders = [
  {
    provide: 'PARTICIPANTE_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'participante_proyecto',
        ParticipanteProyectoModelo,
        'participante_proyecto',
      ),
    inject: ['DB_CONNECTION'],
  },
];
