import { ParticipanteProyectoControllerModule } from './controladores/participante-proyecto-controller.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [ParticipanteProyectoControllerModule],
  providers: [],
  controllers: [],
  exports: [],
})
export class ParticipanteProyectoModule {}
