import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CrearParticipanteProyectoService } from '../casos_de_uso/crear-participante-proyecto.service';
import { CrearParticipanteProyectoDto } from './../entidad/participante-proyecto-dto';

@ApiTags('Proyecto')
@Controller('api/participante-proyecto')
export class CrearParticipanteProyectoControlador {
  constructor(
    private readonly crearParticiProyectoService: CrearParticipanteProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  //@ApiBody({ type: any })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Crea una particpante proyecto' })
  //@ApiResponse({ status: 201, type: ProyectoDto, description: 'Noticia creado' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al crear la asociacion' })
  @UseGuards(AuthGuard('jwt'))
  public async crearParticipanteProyecto(
    @Headers() headers,
    @Body() crearParticipanteProyectoDTO: CrearParticipanteProyectoDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(crearParticipanteProyectoDTO.coautor) &&
        mongoose.isValidObjectId(crearParticipanteProyectoDTO.proyecto)
      ) {
        const partProyecto = await this.crearParticiProyectoService.crearParticipanteProyecto(
          crearParticipanteProyectoDTO,
        );

        if (partProyecto) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            datos: partProyecto,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
