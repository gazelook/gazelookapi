import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { ParticipanteProyectoServicesModule } from '../casos_de_uso/participante-proyecto-services.module';
import { CrearParticipanteProyectoControlador } from './crear-participante-proyecto.controller';

@Module({
  imports: [ParticipanteProyectoServicesModule],
  providers: [Funcion],
  exports: [],
  controllers: [CrearParticipanteProyectoControlador],
})
export class ParticipanteProyectoControllerModule {}
