import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { ComentariosServicesModule } from 'src/entidades/comentarios/casos_de_uso/comentarios-services.module';
import { RolServiceModule } from 'src/entidades/rol/casos_de_uso/rol.services.module';
import { participanteProyectoProviders } from './../drivers/participante-proyecto.provider';
import { CrearParticipanteProyectoRolesService } from './crear-participante-proyecto-roles.service';
import { CrearParticipanteProyectoService } from './crear-participante-proyecto.service';
import { EliminarParticipanteProyectoCompletoService } from './eliminar-participante-proyecto-completo.service';
import { ObtenerParticipanteProyectoService } from './obtener-participante-proyecto.service';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    forwardRef(() => ComentariosServicesModule),
    forwardRef(() => RolServiceModule),
  ],
  providers: [
    ...participanteProyectoProviders,
    CrearParticipanteProyectoService,
    ObtenerParticipanteProyectoService,
    EliminarParticipanteProyectoCompletoService,
    CrearParticipanteProyectoRolesService,
  ],
  exports: [
    ...participanteProyectoProviders,
    CrearParticipanteProyectoService,
    ObtenerParticipanteProyectoService,
    EliminarParticipanteProyectoCompletoService,
    CrearParticipanteProyectoRolesService,
  ],
  controllers: [],
})
export class ParticipanteProyectoServicesModule {}
