import { ParticipanteProyecto } from 'src/drivers/mongoose/interfaces/participante_proyecto/participante_proyecto.interface';
import { Inject } from '@nestjs/common';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class ObtenerParticipanteProyectoService {
  constructor(
    @Inject('PARTICIPANTE_PROYECTO_MODEL')
    private readonly participanteProyectoModelo: Model<ParticipanteProyecto>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreEstado: CatalogoEstadoService,
  ) {}
  async ObtenerParticipanteProyecto(
    coautor: string,
    proyecto: string,
  ): Promise<any> {
    try {
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteProyecto,
      );
      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const busqueda = await this.participanteProyectoModelo.findOne({
        coautor: coautor,
        proyecto: proyecto,
        estado: estado.codigo,
      });
      return busqueda;
    } catch (error) {
      throw error;
    }
  }
}
