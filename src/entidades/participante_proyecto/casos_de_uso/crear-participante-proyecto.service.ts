import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { ParticipanteProyecto } from 'src/drivers/mongoose/interfaces/participante_proyecto/participante_proyecto.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoAccionService } from './../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from './../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';
import { CrearParticipanteProyectoDto } from './../entidad/participante-proyecto-dto';

@Injectable()
export class CrearParticipanteProyectoService {
  constructor(
    @Inject('PARTICIPANTE_PROYECTO_MODEL')
    private readonly participanteProyectoModelo: Model<ParticipanteProyecto>,
    private crearHistoricoService: CrearHistoricoService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
  ) {}

  async crearParticipanteProyecto(
    participanteProyecto: CrearParticipanteProyectoDto,
    opts?: any,
  ): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteProyecto,
      );
      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      const crear = await new this.participanteProyectoModelo(
        participanteProyecto,
      ).save(opts);

      let newHistoricoCrearPartiAso: any = {
        datos: crear,
        usuario: crear.coautor,
        accion: accion.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoCrearPartiAso,
      );

      const returnPartProyecto = await this.participanteProyectoModelo
        .findOne({ _id: crear._id })
        .session(opts.session);
      return returnPartProyecto;
    } catch (error) {
      throw error;
    }
  }
}
