import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { ParticipanteProyecto } from 'src/drivers/mongoose/interfaces/participante_proyecto/participante_proyecto.interface';
import { EliminarComentarioCompletoService } from 'src/entidades/comentarios/casos_de_uso/eliminar-comentario-completo.service';

@Injectable()
export class EliminarParticipanteProyectoCompletoService {
  noticia: any;

  constructor(
    @Inject('PARTICIPANTE_PROYECTO_MODEL')
    private readonly participanteProyectoModel: Model<ParticipanteProyecto>,
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private eliminarComentarioCompletoService: EliminarComentarioCompletoService,
  ) {}

  async eliminarParticipanteProyectoCompleto(
    idProyecto: string,
    opts,
  ): Promise<any> {
    try {
      // const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.participanteProyecto);
      // const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, entidad.codigo);
      const participanteProyecto = await this.participanteProyectoModel
        .find({ proyecto: idProyecto })
        .populate({
          path: 'comentarios',
          select: '_id adjuntos traducciones',
          populate: {
            path: 'traducciones',
          },
        });
      // .session(opts.session)

      for (const getParticipanteProyecto of participanteProyecto) {
        console.log('getParticipanteProyecto: ', getParticipanteProyecto._id);
        if (getParticipanteProyecto.comentarios.length > 0) {
          for (const getComentarios of getParticipanteProyecto.comentarios) {
            if (getComentarios) {
              console.log('getComentarios: ', getComentarios['_id']);
              await this.eliminarComentarioCompletoService.eliminarComentarioCompleto(
                getComentarios,
                opts,
              );
              //await this.comentarioModel.deleteOne({_id: getComentarios}, opts)
              console.log('elimino comentario: ');
            }
          }
        }
      }
      await this.participanteProyectoModel.deleteMany(
        { proyecto: idProyecto },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  // elimina la referencia del participante(perfil-coautor) en otros proyectos y
  // retorna el participanteProyecto para eliminarlo despues de este proceso
  async eliminarExisteParticipanteProyectoCompleto(
    idPerfil: any,
    opts?: any,
  ): Promise<any> {
    try {
      const participanteProyecto = await this.participanteProyectoModel
        .find({ coautor: idPerfil })
        .session(opts.session);

      if (participanteProyecto.length > 0) {
        for (const getParticipanteproyecto of participanteProyecto) {
          const getIdProyecto = getParticipanteproyecto.proyecto;
          if (getIdProyecto) {
            console.log('PROYECTO PARTICIPANTE: ', getIdProyecto);
            const idParticipanteProyecto = getParticipanteproyecto._id;
            const getProyecto = await this.proyectoModel
              .findOne({ _id: getIdProyecto })
              .session(opts.session);
            // .session(opts.session)
            let listaParticipantes = [];
            if (getProyecto) {
              if (getProyecto.participantes.length > 0) {
                for (const getParticipantes of getProyecto.participantes) {
                  listaParticipantes.push(getParticipantes['_id'].toString());
                }
                let index = listaParticipantes.indexOf(idParticipanteProyecto);
                //Eliminamos el elemento del array
                listaParticipantes.splice(index, 1);
              }
            }

            // ______ datos participantes _____
            let dataParticipantes: any = {
              participantes: listaParticipantes,
              fechaActualizacion: new Date(),
            };

            //Se le quita los participantes
            await this.proyectoModel.findByIdAndUpdate(
              getIdProyecto,
              dataParticipantes,
              opts,
            );
          }
        }
      }

      return participanteProyecto;
    } catch (error) {
      throw error;
    }
  }

  async eliminarParticipanteProyectoCompletoById(
    idParticipanteProyecto: string,
    opts,
  ): Promise<any> {
    try {
      // const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.participanteProyecto);
      // const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, entidad.codigo);
      const participanteProyecto = await this.participanteProyectoModel
        .findById(idParticipanteProyecto)
        .populate({
          path: 'comentarios',
          select: '_id adjuntos traducciones',
          populate: {
            path: 'traducciones',
          },
        })
        .session(opts.session);

      if (participanteProyecto) {
        for (const getComentarios of participanteProyecto.comentarios) {
          if (getComentarios) {
            await this.eliminarComentarioCompletoService.eliminarComentarioCompleto(
              getComentarios,
              opts,
            );
          }

          //await this.comentarioModel.deleteOne({_id: getComentarios}, opts)
        }
      }
      await this.participanteProyectoModel.deleteOne(
        { _id: participanteProyecto._id },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
