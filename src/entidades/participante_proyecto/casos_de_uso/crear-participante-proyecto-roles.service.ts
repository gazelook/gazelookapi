import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { CrearComentarioService } from 'src/entidades/comentarios/casos_de_uso/crear-comentario.service';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import {
  codigoEntidades,
  codigosCatalogoAcciones,
  nombrecatalogoEstados,
} from 'src/shared/enum-sistema';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';
import { CrearParticipanteProyectoDto } from './../entidad/participante-proyecto-dto';
import { CrearParticipanteProyectoService } from './crear-participante-proyecto.service';

@Injectable()
export class CrearParticipanteProyectoRolesService {
  constructor(
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    private crearHistoricoService: CrearHistoricoService,
    private gestionRolService: GestionRolService,
    private crearComentarioService: CrearComentarioService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearParticipanteProyectoService: CrearParticipanteProyectoService,
  ) {}

  async crearParticipanteProyectoByRoles(
    idPerfil: string,
    idProyecto: string,
    rolEntidad: string,
    codEntidad: string,
    idPerfilRol: string,
    opts?: any,
  ): Promise<any> {
    try {
      //Obtiene el estado  activa de la entidad participante proyecto
      const estadoPartProyec = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codigoEntidades.entidadParticipanteProyecto,
      );
      let codEstadoPartProyecto = estadoPartProyec.codigo;

      //Obtener una configuracion aleatoria para participante proyecto
      const getConfigEstilo = await this.crearComentarioService.ObtenerConfiguracionEstilo(
        codigoEntidades.entidadComentarios,
      );

      //Obtiene el rol entidad
      const getRolEntidad = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
        rolEntidad,
        codEntidad,
      );
      const idRolEntidad = getRolEntidad._id;

      let rolEnt = [];
      rolEnt.push(idRolEntidad);

      var participanteProyectoDto: CrearParticipanteProyectoDto;
      participanteProyectoDto = {
        coautor: idPerfilRol,
        proyecto: idProyecto,
        roles: rolEnt[0],
        comentarios: [],
        configuraciones: [getConfigEstilo],
        estado: codEstadoPartProyecto,
        totalComentarios: 0,
      };

      const crearParcipanteProyec = await this.crearParticipanteProyectoService.crearParticipanteProyecto(
        participanteProyectoDto,
        opts,
      );
      const getIdePartProy = crearParcipanteProyec._id;

      await this.proyectoModel.updateOne(
        { _id: idProyecto },
        { $push: { participantes: getIdePartProy } },
        opts,
      );

      let newHistoricoCrearPartiProy: any = {
        datos: crearParcipanteProyec,
        usuario: idPerfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadParticipanteProyecto,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoCrearPartiProy,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
