import { ApiProperty } from '@nestjs/swagger';

export class EstadoDocumentoUsuarioDto {
  @ApiProperty()
  codigo: string;
}

export class RetornoDocumentoUsuarioDto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  estado: EstadoDocumentoUsuarioDto;
}
