export class DocumentoUsuarioDto {
  usuario: string;
  estado?: string;
  tipo: string;
  archivo: string;
}

export class SubirDocumentoUsuarioDto {
  archivo: Express.Multer.File;
}
