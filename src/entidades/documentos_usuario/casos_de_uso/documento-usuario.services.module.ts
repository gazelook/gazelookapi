import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { UsuarioServicesModule } from 'src/entidades/usuario/casos_de_uso/usuario-services.module';
import { ArchivoServicesModule } from 'src/entidades/archivo/casos_de_uso/archivo-services.module';
import { CrearDocumentoUsuarioService } from './crear-documento-usuario.service';
import { documentoUsuarioProviders } from '../drivers/documento-usuario.provider';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { SubirDocumentoUsuarioService } from './subir-documento-usuario.service';

@Module({
  imports: [
    DBModule,
    forwardRef(() => UsuarioServicesModule),
    forwardRef(() => ArchivoServicesModule),
    CatalogosServiceModule,
  ],
  providers: [
    ...documentoUsuarioProviders,
    CrearDocumentoUsuarioService,
    SubirDocumentoUsuarioService,
  ],
  exports: [CrearDocumentoUsuarioService, SubirDocumentoUsuarioService],
  controllers: [],
})
export class DocumentoUsuarioServicesModule {}
