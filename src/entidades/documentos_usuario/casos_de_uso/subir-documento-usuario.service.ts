import { Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { CrearArchivoService } from '../../archivo/casos_de_uso/crear-archivo.service';
import { GestionArchivoService } from '../../archivo/casos_de_uso/gestion-archivo.service';
import { crearArchivoDocumentoDto } from '../../archivo/entidad/crear-archivo-documento.dto';
import { SubirDocumentoUsuarioDto } from '../entidad/documentos-usuario.dto';
import { RetornoDocumentoUsuarioDto } from '../entidad/retorno-documento-usuario.dto';

@Injectable()
export class SubirDocumentoUsuarioService {
  constructor(
    private crearArchivoService: CrearArchivoService,
    private gestionArchivoService: GestionArchivoService,
  ) {}

  async subirDocUsuario(
    data: SubirDocumentoUsuarioDto,
    idioma: string,
  ): Promise<RetornoDocumentoUsuarioDto> {
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };
      console.log(data);

      let dataArchivo: crearArchivoDocumentoDto = {
        fileDefault: false,
      };

      const validarArchivo = this.gestionArchivoService.validarArchivo(
        data.archivo,
      );

      if (validarArchivo.validate) {
        dataArchivo.archivo = data.archivo;
        dataArchivo.tipo = validarArchivo.tipo;

        const archivo = await this.crearArchivoService.crearArchivoDocumento(
          dataArchivo,
          opts,
        );

        // proceso exitoso
        await session.commitTransaction();
        session.endSession();

        const result = {
          _id: archivo._id,
          estado: {
            codigo: archivo.estado,
          },
        };

        return result;
      }

      return null;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
