import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { DocumentoUsuario } from '../../../drivers/mongoose/interfaces/documento_usuario/documento-usuario.interface';
import { DocumentoUsuarioDto } from '../entidad/documentos-usuario.dto';
import { ActualizarArchivoService } from '../../archivo/casos_de_uso/actualizar-archivo.service';

@Injectable()
export class CrearDocumentoUsuarioService {
  constructor(
    @Inject('DOCUMENTO_USUARIO_MODEL')
    private readonly documentoUsuarioModel: Model<DocumentoUsuario>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private actualizarArchivoService: ActualizarArchivoService,
  ) {}

  async crearDocumentoUsuario(
    documentoUsuario: DocumentoUsuarioDto,
    opts,
  ): Promise<DocumentoUsuario> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene la entidad documentoUsuario
    const entidadDocUsuario = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.documentoUsuario,
    );
    let codEntidadDocUsuario = entidadDocUsuario.codigo;

    //Obtiene el estado  activa de la entidad documentoUsuario
    const estadoDocUsuario = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadDocUsuario,
    );
    let codEstadoDocUsuario = estadoDocUsuario.codigo;

    try {
      // cambiar estado archivo
      await this.actualizarArchivoService.activarArchivo(
        documentoUsuario.archivo,
        documentoUsuario.usuario,
        opts,
      );

      documentoUsuario.estado = codEstadoDocUsuario;

      const saveDocUsuario = new this.documentoUsuarioModel(documentoUsuario);
      await saveDocUsuario.save(opts);

      delete saveDocUsuario.__v;

      let newHistoricoDocUser: any = {
        descripcion: saveDocUsuario,
        usuario: saveDocUsuario.usuario,
        accion: getAccion,
        entidad: codEntidadDocUsuario,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistoricoDocUser);

      return saveDocUsuario;
    } catch (error) {
      throw error;
    }
  }
}
