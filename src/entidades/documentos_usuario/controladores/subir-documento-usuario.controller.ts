import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBody,
  ApiConsumes,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { SubirDocumentoUsuarioService } from '../casos_de_uso/subir-documento-usuario.service';
import { RetornoDocumentoUsuarioDto } from '../entidad/retorno-documento-usuario.dto';

export const ApiFile = (fileName: string = 'file'): MethodDecorator => (
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor,
) => {
  ApiBody({
    required: true,
    schema: {
      type: 'object',
      properties: {
        [fileName]: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })(target, propertyKey, descriptor);
};

@ApiTags('Usuario')
@Controller('api/usuario/subir-documento')
export class SubirDocumentoUsuarioController {
  constructor(
    private readonly subirDocumentoUsuarioService: SubirDocumentoUsuarioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 201,
    type: RetornoDocumentoUsuarioDto,
    description: 'Archivo creado',
  })
  @ApiResponse({ status: 404, description: 'Error al guardar el archivo' })
  @ApiOperation({
    summary:
      'Esta función guarda el documento del usuario y retorna las caracteristicas del mismo',
  })
  @ApiConsumes('multipart/form-data')
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiFile('archivo')
  @UseInterceptors(FileInterceptor('archivo'))
  public async crearDocUsuario(
    @UploadedFile() file: Express.Multer.File,
    @Body() archivoDto,
    @Headers() headers,
  ) {
    archivoDto.archivo = file;
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const result = await this.subirDocumentoUsuarioService.subirDocUsuario(
        archivoDto,
        codIdioma,
      );
      if (result) {
        const CREACION_CORRECTA = await this.i18n.translate(
          codIdioma.concat('.CREACION_CORRECTA'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.CREATED,
          mensaje: CREACION_CORRECTA,
          datos: result,
        });
      } else {
        const ERROR_CREACION = await this.i18n.translate(
          codIdioma.concat('.ERROR_CREACION'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: ERROR_CREACION,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
