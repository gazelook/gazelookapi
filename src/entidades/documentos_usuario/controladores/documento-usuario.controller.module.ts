import { Module } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { DocumentoUsuarioServicesModule } from '../casos_de_uso/documento-usuario.services.module';
import { SubirDocumentoUsuarioController } from './subir-documento-usuario.controller';

@Module({
  imports: [DocumentoUsuarioServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [SubirDocumentoUsuarioController],
})
export class DocumentoUsuarioControllerModule {}
