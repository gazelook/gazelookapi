import { Connection } from 'mongoose';
import { DocumentoUsuario } from 'src/drivers/mongoose/modelos/documento_usuario/documento-usuario.schema';

export const documentoUsuarioProviders = [
  {
    provide: 'DOCUMENTO_USUARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'documento_usuario',
        DocumentoUsuario,
        'documento_usuario',
      ),
    inject: ['DB_CONNECTION'],
  },
];
