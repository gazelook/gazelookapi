import { Module } from '@nestjs/common';
import { DocumentoUsuarioServicesModule } from './casos_de_uso/documento-usuario.services.module';
import { DocumentoUsuarioControllerModule } from './controladores/documento-usuario.controller.module';

@Module({
  imports: [DocumentoUsuarioControllerModule],
  providers: [DocumentoUsuarioServicesModule],
  controllers: [],
})
export class DocumentoUsuarioModule {}
