import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';
import { CatalogoEstadoLocalidadDto } from './catalogo-estado-localidad.dto';

export class Pais {
  @ApiProperty()
  codigo: string;
}

export class CatalogoLocalidadDto {
  @ApiProperty()
  nombre?: string;
  @ApiProperty()
  codigo?: string;
  @ApiProperty({ type: Pais })
  catalogoPais?: Pais;
  @ApiProperty()
  codigoPostal?: string;
  @ApiProperty({ type: CatalogoEstadoLocalidadDto })
  estado?: CatalogoEstadoLocalidadDto;
}

export class CatalogoLocalidadNoticiaDto {
  @ApiProperty()
  codigo: string;
}

export class CatalogoLocalidadProyectoDto {
  @ApiProperty({
    description: 'Codigo de la localidad del proyecto',
    example: 'LOC_735',
  })
  @IsString()
  @IsNotEmpty()
  codigo: string;
}
