import { ApiProperty } from '@nestjs/swagger';

export class Traducciones {
  @ApiProperty()
  nombre: string;
}

export class CatalogoPaisDto {
  @ApiProperty()
  codigo: string;
  @ApiProperty()
  codigoNombre: string;
  @ApiProperty({ type: Array })
  codigoTelefono: [];
  @ApiProperty({ type: [Traducciones] })
  traducciones: Traducciones[];
}
