import { ApiProperty } from '@nestjs/swagger';

export class CatalogoEstadoLocalidadDto {
  @ApiProperty({ description: 'EST_235 -> enRevision | EST_57-> activa' })
  codigo: string;
}
