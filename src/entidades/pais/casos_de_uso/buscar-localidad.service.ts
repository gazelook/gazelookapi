import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Funcion } from 'src/shared/funcion';
import { CatalogoLocalidad } from '../../../drivers/mongoose/interfaces/catalogo_localidad/catalogo-localidad.interface';
import { estadoLocalidad } from '../../../shared/enum-sistema';
import { HadersInterfaceNombres } from '../../../shared/header-response-interface';

@Injectable()
export class BuscarLocalidadService {
  constructor(
    @Inject('CATALOGO_LOCALIDAD_MODEL')
    private readonly catalogoLocalidad: PaginateModel<CatalogoLocalidad>,
    private readonly funcion: Funcion,
  ) {}

  async bucarLocalidad(
    busqueda: string,
    codigoPais: string,
    limite,
    pagina,
    response,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    try {
      //const name = nombre.charAt(0).toUpperCase() + nombre.slice(1);

      const options = {
        lean: true,
        sort: { nombre: 1 },
        select:
          '-traducciones -fechaActualizacion -fechaCreacion  -estado -_id -__v',
        page: Number(pagina),
        limit: Number(limite),
      };

      const localidades: any = await this.catalogoLocalidad.paginate(
        {
          $or: [
            {
              $and: [
                {
                  nombre: {
                    $regex: this.funcion.diacriticSensitiveRegex(busqueda),
                    $options: 'i',
                  },
                },
                // { 'nombre': { $regex: new RegExp(".*" + busqueda + ".*", "i")} },
                // {$text: { $search: busqueda }},
                { catalogoPais: codigoPais },
                { estado: estadoLocalidad.activa },
              ],
            },
          ],
        },
        options,
      );

      let listResult = [];
      if (localidades.docs.length > 0) {
        for (const localidad of localidades.docs) {
          const data = {
            nombre: localidad.nombre,
            codigo: localidad.codigo,
            catalogoPais: { codigo: localidad.catalogoPais },
            codigoPostal: localidad.codigoPostal,
          };
          listResult.push(data);
        }
      }
      response.set(headerNombre.totalDatos, localidades.totalDocs);
      response.set(headerNombre.totalPaginas, localidades.totalPages);
      response.set(headerNombre.proximaPagina, localidades.hasNextPage);
      response.set(headerNombre.anteriorPagina, localidades.hasPrevPage);

      return listResult;
    } catch (error) {
      throw error;
    }
  }
}
