import { Module } from '@nestjs/common';
import { DBModule } from '../../../drivers/db_conection/db.module';
import { paisProviders } from '../drivers/pais.provider';
import { ObtenerCatalogoPaisService } from './obtener-catalogo-pais.service';
import { ObtenerCatalogoLocalidadService } from './obtener-catalogo-localidad.service';
import { BuscarLocalidadService } from './buscar-localidad.service';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { CrearLocalidadService } from './crear-localidad.service';
import { Funcion } from 'src/shared/funcion';

@Module({
  imports: [DBModule, CatalogosServiceModule],
  providers: [
    ...paisProviders,
    ObtenerCatalogoPaisService,
    ObtenerCatalogoLocalidadService,
    BuscarLocalidadService,
    CrearLocalidadService,
    Funcion,
  ],
  exports: [
    ObtenerCatalogoPaisService,
    ObtenerCatalogoLocalidadService,
    BuscarLocalidadService,
    CrearLocalidadService,
  ],
  controllers: [],
})
export class PaisServicesModule {}
