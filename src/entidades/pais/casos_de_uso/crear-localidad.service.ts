import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { CatalogoLocalidad } from '../../../drivers/mongoose/interfaces/catalogo_localidad/catalogo-localidad.interface';
import {
  codigoEntidades,
  codigosCatalogoAcciones,
  estadoLocalidad,
} from '../../../shared/enum-sistema';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class CrearLocalidadService {
  constructor(
    @Inject('CATALOGO_LOCALIDAD_MODEL')
    private readonly catalogoLocalidad: Model<CatalogoLocalidad>,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async crearLocalidad(nombreLocalidad, pais, perfil, opts): Promise<any> {
    try {
      const dataLocalidad = {
        nombre: nombreLocalidad,
        codigo: `LOC_USER${uuidv4()}`,
        catalogoPais: pais,
        estado: estadoLocalidad.enRevision,
      };
      const saveLocalidad = await new this.catalogoLocalidad(
        dataLocalidad,
      ).save(opts);

      const dataLocalidadHistorico = JSON.parse(JSON.stringify(saveLocalidad));
      //eliminar parametro no necesarios
      delete dataLocalidadHistorico.fechaCreacion;
      delete dataLocalidadHistorico.fechaActualizacion;
      delete dataLocalidadHistorico.__v;

      const dataHistorico: any = {
        datos: dataLocalidadHistorico,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadCatalogoLocalidad,
      };
      this.crearHistoricoService.crearHistoricoServer(dataHistorico);
      return saveLocalidad;
    } catch (error) {
      throw error;
    }
  }
}
