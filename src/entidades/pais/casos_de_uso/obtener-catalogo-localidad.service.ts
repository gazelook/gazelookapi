import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoLocalidad } from '../../../drivers/mongoose/interfaces/catalogo_localidad/catalogo-localidad.interface';

@Injectable()
export class ObtenerCatalogoLocalidadService {
  constructor(
    @Inject('CATALOGO_LOCALIDAD_MODEL')
    private readonly catalogoLocalidad: Model<CatalogoLocalidad>,
  ) {}

  async obtenerCatalogoLocalidad(
    codigoPais: string,
  ): Promise<CatalogoLocalidad> {
    try {
      const catalogos: any = await this.catalogoLocalidad
        .find({ catalogoPais: codigoPais.toString() }, {})
        .select('-fechaActualizacion -fechaCreacion  -estado -_id -__v')
        .sort({ nombre: 1 });
      let listResult: any = [];
      if (catalogos.length > 0) {
        for (let element of catalogos) {
          const data = {
            nombre: element.nombre,
            codigo: element.codigo,
            catalogoPais: { codigo: element.catalogoPais },
            codigoPostal: element.codigoPostal,
          };
          listResult.push(data);
        }
      }
      return listResult;
    } catch (error) {
      throw error;
    }
  }

  async obtenerLocalidad(codigoLocalidad: string): Promise<CatalogoLocalidad> {
    try {
      const localidad: any = await this.catalogoLocalidad
        .findOne({ codigo: codigoLocalidad.toString() }, {})
        .select('-fechaActualizacion -fechaCreacion  -estado -_id -__v');

      return localidad;
    } catch (error) {
      throw error;
    }
  }
}
