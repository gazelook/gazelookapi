import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoPais } from '../../../drivers/mongoose/interfaces/catalogo_pais/catalogo-pais.interface';
import { idiomas } from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';

@Injectable()
export class ObtenerCatalogoPaisService {
  constructor(
    @Inject('CATALOGO_PAIS_MODEL')
    private readonly catalogoPais: Model<CatalogoPais>,
    private readonly catalogoIdiomasModel: CatalogoIdiomasService,
  ) {}

  // todos los paises por idioma
  async obtenerCatalogoPais(idiomaPais): Promise<any> {
    try {
      const idioma = await this.catalogoIdiomasModel.obtenerIdiomaByCodigoNombre(
        idiomaPais,
      );

      const catalogoPais = await this.catalogoPais
        .find({})
        .select(
          '-fechaActualizacion -fechaCreacion  -estado -_id -__v -localidades',
        )
        .populate({
          path: 'traducciones',
          select: 'nombre -_id',
          match: {
            idioma: idioma.codigo,
          },
        });
      // .sort({ traducciones: 1 });

      catalogoPais.sort(function(o1, o2) {
        if (o1.traducciones[0]['nombre'] > o2.traducciones[0]['nombre']) {
          //comparación lexicogŕafica
          return 1;
        } else if (
          o1.traducciones[0]['nombre'] < o2.traducciones[0]['nombre']
        ) {
          return -1;
        }
        return 0;
      });

      return catalogoPais;
    } catch (error) {
      throw error;
    }
  }

  async obtenerPaisByCodigo(codigoPais, idioma): Promise<any> {
    try {
      let idiomaPais = idioma;
      if (!idiomaPais) {
        idiomaPais = idiomas.ingles;
      }
      const catalogoIdioma = await this.catalogoIdiomasModel.obtenerIdiomaByCodigoNombre(
        idiomaPais,
      );

      const pais = await this.catalogoPais
        .findOne({ codigo: codigoPais })
        .select(
          '-fechaActualizacion -fechaCreacion  -estado -_id -__v -localidades',
        )
        .populate({
          path: 'traducciones',
          select: 'nombre -_id',
          match: {
            idioma: catalogoIdioma.codigo,
          },
        });

      return pais;
    } catch (error) {
      throw error;
    }
  }
}
