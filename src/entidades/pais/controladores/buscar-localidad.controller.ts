import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  Res,
} from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { I18nService } from 'nestjs-i18n';
import { TraduccionEstaticaController } from '../../../multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from '../../../shared/funcion';
import { BuscarLocalidadService } from '../casos_de_uso/buscar-localidad.service';
import { CatalogoLocalidadDto } from '../entidad/catalogo-localidad.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos/buscar-localidad')
export class BuscarLocalidadControlador {
  constructor(
    private readonly buscarLocalidadService: BuscarLocalidadService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
  ) {}
  @Get('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: CatalogoLocalidadDto,
    description: 'Localidad',
  })
  @ApiResponse({ status: 404, description: 'Error al encontrar la localidad' })
  @ApiOperation({
    summary:
      'Este metodo retorna las localidades con el codigo postal, parametros: nombre de la localidad o codigo postal, y codigo de pais(ejemplo nombre:loja | codigoPais:PAI_31)',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  public async buscarLocalidad(
    @Query('codigoPais') codigoPais: string,
    @Query('busqueda') buscar: string,
    @Headers() headers,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (!isNaN(limite) && !isNaN(pagina) && limite > 0 && pagina > 0) {
        const busqueda = await this.buscarLocalidadService.bucarLocalidad(
          buscar,
          codigoPais,
          limite,
          pagina,
          response,
        );
        if (!busqueda) {
          const COLECCION_VACIA = await this.i18n.translate(
            codIdioma.concat('.COLECCION_VACIA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: COLECCION_VACIA,
          });
        } else {
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: busqueda,
          });
          response.send(respuesta);
          return respuesta;
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
