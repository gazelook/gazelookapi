import { Module } from '@nestjs/common';
import { ObtenerCatalogoPaisControlador } from './obtener-catalogo-pais.controller';
import { PaisServicesModule } from '../casos_de_uso/pais.services.module';
import { ObtenerCatalogoLocalidadControlador } from './obtener-catalogo-localidad.controller';
import { BuscarLocalidadControlador } from './buscar-localidad.controller';
import { Funcion } from '../../../shared/funcion';
import { TraduccionEstaticaController } from '../../../multiIdioma/controladores/traduccion-estatica-controller';

@Module({
  imports: [PaisServicesModule],
  providers: [Funcion, TraduccionEstaticaController],
  exports: [],
  controllers: [
    ObtenerCatalogoPaisControlador,
    ObtenerCatalogoLocalidadControlador,
    BuscarLocalidadControlador,
  ],
})
export class PaisControllerModule {}
