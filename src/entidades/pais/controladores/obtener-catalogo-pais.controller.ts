import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { ObtenerCatalogoPaisService } from '../casos_de_uso/obtener-catalogo-pais.service';
import { CatalogoPaisDto } from '../entidad/catalogo-pais.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos/catalogo-pais')
export class ObtenerCatalogoPaisControlador {
  constructor(
    private readonly obtenerCatalogoPaisService: ObtenerCatalogoPaisService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Get('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: CatalogoPaisDto,
    description: 'Lista de catalogos de paises',
  })
  @ApiOperation({
    summary: 'Este metodo retorna la lista de catalogo de paises',
  })
  @ApiResponse({
    status: 404,
    description: 'Error al obtener los catalogos de paises',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  public async obtenerCatalogoPais(@Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const catalogo = await this.obtenerCatalogoPaisService.obtenerCatalogoPais(
        codIdioma,
      );

      if (!catalogo) {
        const COLECCION_VACIA = await this.i18n.translate(
          codIdioma.concat('.COLECCION_VACIA'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: COLECCION_VACIA,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: catalogo,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
