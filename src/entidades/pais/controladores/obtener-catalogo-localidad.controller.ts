import { Controller, Get, Headers, HttpStatus, Param } from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { ObtenerCatalogoLocalidadService } from '../casos_de_uso/obtener-catalogo-localidad.service';
import { CatalogoLocalidadDto } from '../entidad/catalogo-localidad.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos/pais-localidad')
export class ObtenerCatalogoLocalidadControlador {
  constructor(
    private readonly obtenerCatalogoLocalidadService: ObtenerCatalogoLocalidadService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Get('/:codigoPais')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: CatalogoLocalidadDto,
    description: 'Lista de catalogos postales',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener los catalogos' })
  @ApiOperation({
    summary:
      'Este metodo retorna la lista de codigos postales, que le pasan como parametro el codigo del pais(ejemplo. codigoPais:PAI_31)',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  public async obtenerCatalogoLocalidad(
    @Param('codigoPais') codigoPais: string,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const catalogo = await this.obtenerCatalogoLocalidadService.obtenerCatalogoLocalidad(
        codigoPais,
      );

      if (!catalogo) {
        const COLECCION_VACIA = await this.i18n.translate(
          codIdioma.concat('.COLECCION_VACIA'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: COLECCION_VACIA,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: catalogo,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
