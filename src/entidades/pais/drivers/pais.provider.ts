import { Connection } from 'mongoose';
import { CatalogoPaisModelo } from '../../../drivers/mongoose/modelos/catalogo_pais/catalogo-pais.schema';
import { CatalogoLocalidadModelo } from '../../../drivers/mongoose/modelos/catalogo_localidad/catalogo-localidad.schema';
import { TraduccionLocalidadModelo } from '../../../drivers/mongoose/modelos/traducciones/traduccion_localidad/traduccion-localidad.schema';
import { TraduccionPaisModelo } from '../../../drivers/mongoose/modelos/traducciones/traducion_pais/traduccion-pais.schema';

export const paisProviders = [
  {
    provide: 'CATALOGO_PAIS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('catalogo_pais', CatalogoPaisModelo, 'catalogo_pais'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_LOCALIDAD_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_localidad',
        CatalogoLocalidadModelo,
        'catalogo_localidad',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_PAIS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_catalogo_pais',
        TraduccionPaisModelo,
        'traduccion_catalogo_pais',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_LOCALIDAD_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_catalogo_localidad',
        TraduccionLocalidadModelo,
        'traduccion_catalogo_localidad',
      ),
    inject: ['DB_CONNECTION'],
  },
];
