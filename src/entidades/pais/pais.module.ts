import { Module } from '@nestjs/common';
import { PaisControllerModule } from './controladores/pais.controller.module';

@Module({
  imports: [PaisControllerModule],
})
export class PaisModule {}
