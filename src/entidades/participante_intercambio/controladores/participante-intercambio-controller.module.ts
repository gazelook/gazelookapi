import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { ParticipanteIntercambioServicesModule } from '../casos_de_uso/participante-intercambio-services.module';
import { CrearParticipanteIntercambioControlador } from './crear-participante-intercambio.controller';

@Module({
  imports: [ParticipanteIntercambioServicesModule],
  providers: [Funcion],
  exports: [],
  controllers: [CrearParticipanteIntercambioControlador],
})
export class ParticipanteIntercambioControllerModule {}
