import { Connection } from 'mongoose';
import { ParticipanteIntercambioModelo } from 'src/drivers/mongoose/modelos/participante_intercambio/participante_intercambio.schema';

export const participanteIntercambioProviders = [
  {
    provide: 'PARTICIPANTE_INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'participante_intercambio',
        ParticipanteIntercambioModelo,
        'participante_intercambio',
      ),
    inject: ['DB_CONNECTION'],
  },
];
