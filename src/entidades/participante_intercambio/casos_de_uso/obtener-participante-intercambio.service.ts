import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { ParticipanteIntercambio } from 'src/drivers/mongoose/interfaces/participante_intercambio/participante_intercambio.interface';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';

@Injectable()
export class ObtenerParticipanteIntercambioervice {
  constructor(
    @Inject('PARTICIPANTE_INTERCAMBIO_MODEL')
    private readonly participanteIntercambioModelo: Model<
      ParticipanteIntercambio
    >,
    private nombreEntidad: CatalogoEntidadService,
    private nombreEstado: CatalogoEstadoService,
  ) {}
  async ObtenerParticipanteIntercambio(
    coautor: string,
    intercambio: string,
  ): Promise<any> {
    try {
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteIntercambio,
      );
      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const busqueda = await this.participanteIntercambioModelo.findOne({
        coautor: coautor,
        intercambio: intercambio,
        estado: estado.codigo,
      });
      return busqueda;
    } catch (error) {
      throw error;
    }
  }
}
