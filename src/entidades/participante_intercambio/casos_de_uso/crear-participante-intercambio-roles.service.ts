import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { CrearComentarioIntercambioService } from 'src/entidades/comentarios_intercambio/casos_de_uso/crear-comentario.service';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import {
  codigoEntidades,
  codigosCatalogoAcciones,
  nombrecatalogoEstados,
} from 'src/shared/enum-sistema';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CrearParticipanteIntercambioDto } from '../entidad/participante-intercambio-dto';
import { CrearParticipanteIntercambioService } from './crear-participante-intercambio.service';

@Injectable()
export class CrearParticipanteIntercambioRolesService {
  constructor(
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    private crearHistoricoService: CrearHistoricoService,
    private gestionRolService: GestionRolService,
    private crearComentarioIntercambioService: CrearComentarioIntercambioService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearParticipanteIntercambioService: CrearParticipanteIntercambioService,
  ) {}

  async crearParticipanteIntercambioByRoles(
    idPerfil: string,
    idIntercambio: string,
    rolEntidad: string,
    codEntidad: string,
    idPerfilRol: string,
    opts?: any,
  ): Promise<any> {
    try {
      //Obtiene el estado  activa de la entidad participante intercambio
      const estadoPartIntercambio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codigoEntidades.participanteIntercambio,
      );
      let codEstadoPartInter = estadoPartIntercambio.codigo;

      //Obtener una configuracion aleatoria para participante intercambio
      const getConfigEstilo = await this.crearComentarioIntercambioService.ObtenerConfiguracionEstilo(
        codigoEntidades.comentarioIntercambio,
      );

      //Obtiene el rol entidad
      const getRolEntidad = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
        rolEntidad,
        codEntidad,
      );
      const idRolEntidad = getRolEntidad._id;

      let rolEnt = [];
      rolEnt.push(idRolEntidad);

      var participanteIntercambioDto: CrearParticipanteIntercambioDto;
      participanteIntercambioDto = {
        coautor: idPerfilRol,
        intercambio: idIntercambio,
        roles: rolEnt[0],
        comentarios: [],
        configuraciones: [getConfigEstilo],
        estado: codEstadoPartInter,
        totalComentarios: 0,
      };

      const crearParcipanteInter = await this.crearParticipanteIntercambioService.crearParticipanteIntercambio(
        participanteIntercambioDto,
        opts,
      );
      const getIdePartInter = crearParcipanteInter._id;

      await this.intercambioModel.updateOne(
        { _id: idIntercambio },
        { $push: { participantes: getIdePartInter } },
        opts,
      );

      let newHistoricoCrearPartiInter: any = {
        datos: crearParcipanteInter,
        usuario: idPerfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.participanteIntercambio,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoCrearPartiInter,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
