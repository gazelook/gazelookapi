import { ObtenerParticipanteIntercambioervice } from './obtener-participante-intercambio.service';
import { participanteIntercambioProviders } from '../drivers/participante-intercambio.provider';
import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { CrearParticipanteIntercambioService } from './crear-participante-intercambio.service';
import { EliminarParticipanteIntercambioCompletoService } from './eliminar-participante-intercambio-completo.service';
import { CrearParticipanteIntercambioRolesService } from './crear-participante-intercambio-roles.service';
import { RolServiceModule } from 'src/entidades/rol/casos_de_uso/rol.services.module';
import { ComentariosIntercambioServicesModule } from 'src/entidades/comentarios_intercambio/casos_de_uso/comentarios-intercambio-services.module';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    forwardRef(() => ComentariosIntercambioServicesModule),
    forwardRef(() => RolServiceModule),
  ],
  providers: [
    ...participanteIntercambioProviders,
    CrearParticipanteIntercambioService,
    ObtenerParticipanteIntercambioervice,
    EliminarParticipanteIntercambioCompletoService,
    CrearParticipanteIntercambioRolesService,
  ],
  exports: [
    ...participanteIntercambioProviders,
    CrearParticipanteIntercambioService,
    ObtenerParticipanteIntercambioervice,
    EliminarParticipanteIntercambioCompletoService,
    CrearParticipanteIntercambioRolesService,
  ],
  controllers: [],
})
export class ParticipanteIntercambioServicesModule {}
