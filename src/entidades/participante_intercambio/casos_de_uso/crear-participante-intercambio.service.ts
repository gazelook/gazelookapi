import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { ParticipanteIntercambio } from 'src/drivers/mongoose/interfaces/participante_intercambio/participante_intercambio.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CrearParticipanteIntercambioDto } from '../entidad/participante-intercambio-dto';

@Injectable()
export class CrearParticipanteIntercambioService {
  constructor(
    @Inject('PARTICIPANTE_INTERCAMBIO_MODEL')
    private readonly participanteIntercambioModelo: Model<
      ParticipanteIntercambio
    >,
    private crearHistoricoService: CrearHistoricoService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
  ) {}

  async crearParticipanteIntercambio(
    participanteIntercambio: CrearParticipanteIntercambioDto,
    opts?: any,
  ): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteIntercambio,
      );
      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      const crear = await new this.participanteIntercambioModelo(
        participanteIntercambio,
      ).save(opts);

      let newHistoricoCrearPartiInter: any = {
        datos: crear,
        usuario: crear.coautor,
        accion: accion.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoCrearPartiInter,
      );

      const returnPartIntercambio = await this.participanteIntercambioModelo
        .findOne({ _id: crear._id })
        .session(opts.session);
      return returnPartIntercambio;
    } catch (error) {
      throw error;
    }
  }
}
