import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { PaginateModel } from 'mongoose';
import { ParticipanteIntercambio } from 'src/drivers/mongoose/interfaces/participante_intercambio/participante_intercambio.interface';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { EliminarComentarioIntercambioCompletoService } from 'src/entidades/comentarios_intercambio/casos_de_uso/eliminar-comentario-intercambio-completo.service';

@Injectable()
export class EliminarParticipanteIntercambioCompletoService {
  constructor(
    @Inject('PARTICIPANTE_INTERCAMBIO_MODEL')
    private readonly participanteIntercambioModel: Model<
      ParticipanteIntercambio
    >,
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: PaginateModel<Intercambio>,
    private eliminarComentarioIntercambioCompletoService: EliminarComentarioIntercambioCompletoService,
  ) {}

  async eliminarParticipanteIntercambioCompleto(
    idIntercambio: any,
    opts: any,
  ): Promise<any> {
    try {
      // const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.participanteProyecto);
      // const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, entidad.codigo);
      const participanteIntercambio = await this.participanteIntercambioModel
        .find({ intercambio: idIntercambio })
        .session(opts.session);

      for (const getParticipanteIntercambio of participanteIntercambio) {
        if (getParticipanteIntercambio.comentarios.length > 0) {
          for (const getComentarios of getParticipanteIntercambio.comentarios) {
            await this.eliminarComentarioIntercambioCompletoService.eliminarComentarioCompleto(
              getComentarios,
              opts,
            );
            //await this.comentarioModel.deleteOne({_id: getComentarios}, opts)
          }
        }
      }
      await this.participanteIntercambioModel.deleteMany(
        { intercambio: idIntercambio },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  async eliminarExisteParticipanteIntercambioCompleto(
    idPerfil: any,
    opts: any,
  ): Promise<any> {
    try {
      const participanteIntercambio = await this.participanteIntercambioModel
        .find({ coautor: idPerfil })
        .session(opts.session);

      if (participanteIntercambio.length > 0) {
        for (const getParticipanteIntercambio of participanteIntercambio) {
          const getIdIntercambio = getParticipanteIntercambio.intercambio;
          const idParticipanteIntercambio = getParticipanteIntercambio._id;
          const getIntercambio = await this.intercambioModel
            .findOne({ _id: getIdIntercambio })
            .session(opts.session);
          let listaParticipantes = [];
          if (getIntercambio.participantes.length > 0) {
            for (const getParticipantes of getIntercambio.participantes) {
              listaParticipantes.push(getParticipantes['_id'].toString());
            }
            let index = listaParticipantes.indexOf(idParticipanteIntercambio);
            //Eliminamos el elemento del array
            listaParticipantes.splice(index, 1);
          }
          // ______ datos participantes _____
          let dataParticipantes: any = {
            participantes: listaParticipantes,
            fechaActualizacion: new Date(),
          };

          //Se le quita los participantes
          await this.intercambioModel.findByIdAndUpdate(
            getIdIntercambio,
            dataParticipantes,
            opts,
          );
        }
      }

      return participanteIntercambio;
    } catch (error) {
      throw error;
    }
  }

  async eliminarParticipanteIntercambioCompletoById(
    idParticipanteIntercambio: any,
    opts: any,
  ): Promise<any> {
    try {
      // const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.participanteProyecto);
      // const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, entidad.codigo);
      const participanteIntercambio = await this.participanteIntercambioModel
        .findById(idParticipanteIntercambio)
        .session(opts.session);

      if (participanteIntercambio) {
        for (const getComentarios of participanteIntercambio.comentarios) {
          await this.eliminarComentarioIntercambioCompletoService.eliminarComentarioCompleto(
            getComentarios,
            opts,
          );
          //await this.comentarioModel.deleteOne({_id: getComentarios}, opts)
        }
      }
      await this.participanteIntercambioModel.deleteOne(
        { _id: participanteIntercambio._id },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
