import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { ProyectoUnicoDto } from 'src/entidades/proyectos/entidad/proyecto-dto';
import { IdentRolesDto } from 'src/entidades/rol/entidad/roles-dto';
import { PerfilProyectoDto } from '../../usuario/dtos/perfil.dto';

export class ParticipanteProyectoDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  coautor: PerfilProyectoDto;

  @ApiProperty({ type: ProyectoUnicoDto })
  @IsOptional()
  proyecto: ProyectoUnicoDto;

  @ApiProperty({ type: [IdentRolesDto] })
  @IsNotEmpty()
  roles: [IdentRolesDto];

  @ApiProperty({ type: [] })
  @IsOptional()
  comentarios: [];

  @ApiProperty({ type: [] })
  @IsOptional()
  configuraciones: [];

  @ApiProperty()
  @IsOptional()
  estado: string;

  @ApiProperty()
  @IsOptional()
  totalComentarios: number;
}

export class CrearParticipanteIntercambioDto {
  @ApiProperty()
  @IsNotEmpty()
  coautor: string;

  @ApiProperty({ type: ProyectoUnicoDto })
  @IsOptional()
  intercambio: string;

  @ApiProperty({ type: [] })
  @IsNotEmpty()
  roles: [];

  @ApiProperty({ type: [] })
  @IsOptional()
  comentarios: [];

  @ApiProperty({ type: [] })
  @IsOptional()
  configuraciones: [any];

  @ApiProperty()
  @IsOptional()
  estado: string;

  @ApiProperty()
  @IsOptional()
  totalComentarios: number;
}
