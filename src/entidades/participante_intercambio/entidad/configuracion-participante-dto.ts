import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class IdentConfiguracionParticipanteDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  _id: string;
}
