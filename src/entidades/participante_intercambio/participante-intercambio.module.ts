import { ParticipanteIntercambioControllerModule } from './controladores/participante-intercambio-controller.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [ParticipanteIntercambioControllerModule],
  providers: [],
  controllers: [],
  exports: [],
})
export class ParticipanteIntercambioModule {}
