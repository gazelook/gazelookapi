import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiSecurity,
  ApiHeader,
  ApiParam,
} from '@nestjs/swagger';

import {
  Controller,
  Get,
  HttpStatus,
  Headers,
  Param,
  UseGuards,
} from '@nestjs/common';
import { RetornoMediaAlbumDto } from '../entidad/retorno-media-album.dto';
import { AuthGuard } from '@nestjs/passport';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';
import { ObtenerAlbumService } from '../casos_de_uso/obtener-album.service';

@ApiTags('Album')
@Controller('api/album')
export class ConsultarAlbumController {
  constructor(
    private readonly obtenerAlbumService: ObtenerAlbumService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Get('/:idAlbum')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: RetornoMediaAlbumDto,
    description: 'Datos del Album',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener los datos' })
  @ApiOperation({
    summary: 'Esta función retorna un album con sus medias y descripciones',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: true,
  })
  @ApiParam({ name: 'idAlbum', description: 'id del album' })
  @UseGuards(AuthGuard('jwt'))
  public async agregarMedia(
    @Headers() headers,
    @Param('idAlbum') idAlbum: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (mongoose.isValidObjectId(idAlbum)) {
        const result = await this.obtenerAlbumService.consultarAlbum(
          idAlbum,
          codIdioma,
        );

        if (result) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: result,
          });
        } else {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_OBTENER,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
