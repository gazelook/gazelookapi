import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AgregarMediaAlbumService } from '../casos_de_uso/agregar-media-album.service';
import { AlbumEntidad } from '../entidad/agregar-media-album.dto';
import { RetornoMediaAlbumDto } from '../entidad/retorno-media-album.dto';

@ApiTags('Album')
@Controller('api/album/agregar-media')
export class AgregarMediaAlbumControlador {
  constructor(
    private readonly agregarMediaAlbumService: AgregarMediaAlbumService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Post('/:idEntidad/:codigoEntidad')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 201,
    type: RetornoMediaAlbumDto,
    description: 'La media se agrego al album',
  })
  @ApiResponse({
    status: 404,
    description: 'Error al registrar la nueva media',
  })
  @ApiOperation({
    summary:
      'Esta función agrega medias(archivos) al album, y actualiza la portada del album',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: true,
  })
  @ApiParam({
    name: 'idEntidad',
    description: 'id del proyecto | noticia | perfil',
  })
  @ApiParam({
    name: 'codigoEntidad',
    description:
      'codigo de la entidad. Ejemplo: ENT_2 => Proyectos; ENT_3 => Noticias; ENT_8 => Perfiles ',
  })
  @UseGuards(AuthGuard('jwt'))
  public async agregarMedia(
    @Body() dataAlbum: AlbumEntidad,
    @Headers() headers,
    @Param('idEntidad') idEntidad: string,
    @Param('codigoEntidad') codigoEntidad: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (mongoose.isValidObjectId(idEntidad) && codigoEntidad) {
        const result = await this.agregarMediaAlbumService.agregarMediaAlbum(
          dataAlbum,
          idEntidad,
          codigoEntidad,
          codIdioma,
        );

        if (result) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
