import { Module } from '@nestjs/common';
import { AlbumServiceModule } from '../casos_de_uso/album.services.module';
import { TraduccionEstaticaController } from '../../../multiIdioma/controladores/traduccion-estatica-controller';
import { AgregarMediaAlbumControlador } from './agregar-media-album.controller';
import { EliminarMediaAlbumControlador } from './eliminar-media-album.controller';
import { ActualizarMediaAlbumControlador } from './actualizar-media-album.controller';
import { Funcion } from '../../../shared/funcion';
import { CrearAlbumEntidadController } from './crear-album-entidad.controller';
import { ConsultarAlbumController } from './consultar-album.controller';
import { ActualizarPropiedadesAlbunController } from './actualizar-propiedades-album.controller';

@Module({
  imports: [AlbumServiceModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    AgregarMediaAlbumControlador,
    EliminarMediaAlbumControlador,
    ActualizarMediaAlbumControlador,
    CrearAlbumEntidadController,
    ConsultarAlbumController,
    ActualizarPropiedadesAlbunController,
  ],
})
export class AlbumControllerModule {}
