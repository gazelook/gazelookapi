import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Param,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ActualizarPropiedadesAlbumService } from '../casos_de_uso/actualizar-propiedades-album.service';
import { AlbumActPropiedades } from '../entidad/actualizar-propiedades-album.dto';

@ApiTags('Album')
@Controller('api/album/actualizar-propiedades')
export class ActualizarPropiedadesAlbunController {
  constructor(
    private readonly actualizarPropiedadesAlbumService: ActualizarPropiedadesAlbumService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Put('/:idEntidad/:codigoEntidad')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'Actualizacion correcta' })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiOperation({
    summary:
      'Actualiza las propiedades del album: predeterminado, estado y tipo',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: true,
  })
  @ApiParam({
    name: 'idEntidad',
    description: 'id del proyecto | noticia | perfil',
  })
  @ApiParam({
    name: 'codigoEntidad',
    description:
      'codigo de la entidad. Ejemplo: ENT_2 => Proyectos; ENT_3 => Noticias; ENT_8 => Perfiles ',
  })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarPropiedadesAlbum(
    @Body() dataAlbum: AlbumActPropiedades,
    @Headers() headers,
    @Param('idEntidad') idEntidad: string,
    @Param('codigoEntidad') codigoEntidad: string,
  ) {
    const respuesta = new RespuestaInterface();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (mongoose.isValidObjectId(idEntidad) && codigoEntidad) {
        const result = await this.actualizarPropiedadesAlbumService.actualizarPropiedadesAlbum(
          dataAlbum,
          idEntidad,
          codigoEntidad,
          codIdioma,
        );

        if (result) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
