import {
  Body,
  Controller,
  Delete,
  Headers,
  HttpStatus,
  Param,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { EliminarMediaAlbumService } from '../casos_de_uso/eliminar-media-album.service';
import { AlbumEntidad } from '../entidad/agregar-media-album.dto';

@ApiTags('Album')
@Controller('api/album/eliminar-media')
export class EliminarMediaAlbumControlador {
  constructor(
    private readonly eliminarMediaAlbumService: EliminarMediaAlbumService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Delete('/:idEntidad/:codigoEntidad')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'La media ha sido eliminado' })
  @ApiResponse({ status: 404, description: 'Error al eliminar la media' })
  @ApiOperation({ summary: 'Esta función las medias de un album' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiParam({
    name: 'idEntidad',
    description: 'id del proyecto | noticia | perfil',
  })
  @ApiParam({
    name: 'codigoEntidad',
    description:
      'codigo de la entidad. Ejemplo: ENT_2 => Proyectos; ENT_3 => Noticias; ENT_8 => Perfiles ',
  })
  @UseGuards(AuthGuard('jwt'))
  public async agregarMedia(
    @Body() dataMedia: AlbumEntidad,
    @Headers() headers,
    @Param('idEntidad') idEntidad: string,
    @Param('codigoEntidad') codigoEntidad: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (mongoose.isValidObjectId(idEntidad) && codigoEntidad) {
        const result = await this.eliminarMediaAlbumService.eliminarMediaAlbum(
          dataMedia,
          idEntidad,
          codigoEntidad,
        );
        if (result) {
          const ELIMINACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ELIMINACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        } else {
          const ERROR_ELIMINAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ELIMINAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
