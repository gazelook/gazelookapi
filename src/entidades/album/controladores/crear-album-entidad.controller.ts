import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiSecurity,
  ApiHeader,
  ApiParam,
} from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Headers,
  Param,
  UseGuards,
} from '@nestjs/common';

import { IdentAlbumDto } from '../entidad/album-dto';
import { AuthGuard } from '@nestjs/passport';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';
import { CrearAlbumEntidadService } from '../casos_de_uso/crear-album-entidad.service';
import { Album } from '../../cuenta/entidad/cuenta.dto';

@ApiTags('Album')
@Controller('api/album/crear-album-entidad')
export class CrearAlbumEntidadController {
  constructor(
    private readonly crearAlbumEntidadService: CrearAlbumEntidadService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Post('/:idEntidad/:codigoEntidad')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 201,
    type: IdentAlbumDto,
    description: 'Se agrego un nuevo album a la entidad',
  })
  @ApiResponse({ status: 404, description: 'Error al registrar el album' })
  @ApiOperation({
    summary: 'Crea un nuevo album y lo agrega a la entidad correspondiente',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: true,
  })
  @ApiParam({
    name: 'idEntidad',
    description: 'id del proyecto | noticia | perfil',
  })
  @ApiParam({
    name: 'codigoEntidad',
    description:
      'codigo de la entidad. Ejemplo: ENT_2 => Proyectos; ENT_3 => Noticias; ENT_8 => Perfiles ',
  })
  @UseGuards(AuthGuard('jwt'))
  public async agregarAlbum(
    @Body() dataAlbum: Album,
    @Headers() headers,
    @Param('idEntidad') idEntidad: string,
    @Param('codigoEntidad') codigoEntidad: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (mongoose.isValidObjectId(idEntidad) && codigoEntidad) {
        const result = await this.crearAlbumEntidadService.crearAlbumEntidad(
          dataAlbum,
          idEntidad,
          codigoEntidad,
          codIdioma,
        );

        if (result) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
            datos: result,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
