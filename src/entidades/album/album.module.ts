import { Module } from '@nestjs/common';
import { AlbumControllerModule } from './controladores/album-controller.module';

@Module({
  imports: [AlbumControllerModule],
})
export class AlbumModule {}
