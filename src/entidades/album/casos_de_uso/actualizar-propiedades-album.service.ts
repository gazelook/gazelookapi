import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import * as mongoose from 'mongoose';
import { ObtenerAlbumService } from './obtener-album.service';
import { ObtenerPerfilUsuarioService } from '../../perfil/casos_de_uso/obtener-perfil-usuario.service';
import { ObtenerProyetoIdService } from '../../proyectos/casos_de_uso/obtener-proyecto-id.service';
import {
  codigoEntidades,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { ObtenerNoticiaIdService } from '../../noticia/casos_de_uso/obtener-noticia-id.service';
import { erroresGeneral } from '../../../shared/enum-errores';
import { GestionAlbumService } from './gestion-album.service';

@Injectable()
export class ActualizarPropiedadesAlbumService {
  constructor(
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerAlbumService: ObtenerAlbumService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private obtenerProyetoIdService: ObtenerProyetoIdService,
    private obtenerNoticiaIdService: ObtenerNoticiaIdService,
    private gestionAlbumService: GestionAlbumService,
  ) {}

  async actualizarPropiedadesAlbum(
    data: any,
    idEntidad: string,
    codigoEntidad: string,
    idioma: string,
  ): Promise<any> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();

    const opts = { session };

    try {
      let idPerfil;
      let proyecto;
      let noticia;
      let perfil;
      let datosAlbumHis;
      const idAlbum = data._id;
      let dataAlbum: any = {};

      data.tipo?.codigo ? (dataAlbum.tipo = data.tipo.codigo) : false;
      data.estado?.codigo ? (dataAlbum.estado = data.tipo.estado) : false;
      data?.predeterminado
        ? (dataAlbum.predeterminado = data.predeterminado)
        : false;

      if (
        !mongoose.isValidObjectId(idAlbum) ||
        !mongoose.isValidObjectId(idEntidad)
      ) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }
      if (!this.gestionAlbumService.verificarCodigosEntidad(codigoEntidad)) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }

      const albumQuery: any = await this.obtenerAlbumService.obtenerAlbumById(
        idAlbum,
      );
      if (!albumQuery) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresGeneral.COLECCION_VACIA,
        };
      }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      //const perfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(idPerfil);
      if (codigoEntidades.entidadProyectos === codigoEntidad) {
        proyecto = await this.obtenerProyetoIdService.obtenerProyectoById(
          idEntidad,
        );
        idPerfil = proyecto.perfil.toString();
      }

      if (codigoEntidades.entidadNoticias === codigoEntidad) {
        noticia = await this.obtenerNoticiaIdService.obtenerNoticiaById(
          idEntidad,
        );
        idPerfil = noticia.perfil.toString();
      }

      if (codigoEntidades.entidadPerfiles === codigoEntidad) {
        perfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
          idEntidad,
        );
        idPerfil = perfil._id.toString();
      }

      if (!proyecto && !noticia && !perfil) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresGeneral.COLECCION_VACIA,
        };
      }

      const albumUpdated = await this.albumModel.findByIdAndUpdate(
        idAlbum,
        dataAlbum,
        opts,
      );
      const dataAlbumHistorico = JSON.parse(JSON.stringify(albumUpdated));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      datosAlbumHis = {
        _id: idEntidad,
        datos: dataAlbumHistorico,
      };

      const dataHistoricoAlbum: any = {
        datos: datosAlbumHis,
        usuario: idPerfil,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de album
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);
      await session.commitTransaction();
      session.endSession();

      //const result: any = await this.obtenerAlbumService.obtenerAlbumById(idAlbum);

      return albumUpdated;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
