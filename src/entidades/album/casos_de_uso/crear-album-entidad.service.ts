import { Injectable, HttpStatus } from '@nestjs/common';
import { CrearAlbumService } from './crear-album.service';
import * as mongoose from 'mongoose';
import { erroresGeneral } from '../../../shared/enum-errores';
import { GestionAlbumService } from './gestion-album.service';

@Injectable()
export class CrearAlbumEntidadService {
  constructor(
    private crearAlbumService: CrearAlbumService,
    private gestionAlbumService: GestionAlbumService,
  ) {}

  async crearAlbumEntidad(
    data: any,
    idEntidad: string,
    codigoEntidad: string,
    idioma: string,
  ): Promise<any> {
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      const opts = { session };

      if (!mongoose.isValidObjectId(idEntidad)) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }
      if (!this.gestionAlbumService.verificarCodigosEntidad(codigoEntidad)) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }

      const getEntidad = await this.gestionAlbumService.getEntidad(
        codigoEntidad,
        idEntidad,
      );
      if (!getEntidad)
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };

      const dataAlbum = {
        nombre: data.nombre,
        tipo: data.tipo.codigo,
        media: data.media,
        portada: data?.portada,
        usuario: getEntidad.perfil,
        idioma: idioma,
        codigoEntidad: codigoEntidad,
        predeterminado: data.predeterminado,
      };

      const album = await this.crearAlbumService.crearAlbum(
        dataAlbum,
        opts,
        idEntidad,
      );

      const updateEntidad = await this.gestionAlbumService.agregarAlbumEntidad(
        codigoEntidad,
        idEntidad,
        album._id,
        opts,
      );

      if (!updateEntidad) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresGeneral.ERROR_ACTUALIZAR,
        };
      }

      await session.commitTransaction();
      session.endSession();

      const resultado = {
        _id: album._id,
      };

      return resultado;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
