import { CatalogoIdiomasService } from './../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { TraduccionMediaService } from '../../media/casos_de_uso/traduccion-media.service';
import {
  populateGetMediaTraduciones,
  populateGetPortadaTraduccion,
} from '../../../shared/enum-query-populate';
import {
  codigosEstadosMedia,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';

@Injectable()
export class ObtenerAlbumService {
  constructor(
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    private traduccionMediaService: TraduccionMediaService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private readonly catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async obtenerAlbumById(idAlbum: string): Promise<any> {
    const album = await this.albumModel
      .findOne({ _id: idAlbum })
      .select('_id tipo portada estado')
      .populate({
        path: 'media',
        select: '_id',
        populate: [
          {
            path: 'principal',
            select: 'url fileDefault duracion fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: 'url fileDefault duracion fechaActualizacion',
          },
        ],
      });
    return album;
  }

  async obtenerAlbumByIdEstado(idAlbum, estado: string): Promise<any> {
    const album = await this.albumModel
      .findOne({ $and: [{ _id: idAlbum }, { estado: estado }] })
      .select('_id tipo')
      .populate({
        path: 'media',
        select: '_id',
        populate: [
          {
            path: 'principal',
            select: 'url fileDefault duracion fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: 'url url fileDefault duracion fechaActualizacion',
          },
        ],
      });
    return album;
  }

  async getArchivoRandomAlbum(listaMedias): Promise<any> {
    let count = listaMedias.length - 1;
    let alet = Math.round(Math.random() * count);

    let media = [];
    for (let index = 0; index < listaMedias.length; index++) {
      if (listaMedias[index].principal) {
        let dataMedia = {
          _id: listaMedias[index]._id,
          principal: {
            url: listaMedias[index].principal['url'],
            fileDefault: listaMedias[index].principal['fileDefault'],
            duracion: listaMedias[index].principal?.duracion,
            fechaActualizacion:
              listaMedias[index].principal['fechaActualizacion'],
          },
        };
        media.push(dataMedia);
      }
    }
    return media[alet];
  }

  // obtener album con medias
  async consultarAlbum(idAlbum: string, idioma) {
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      //_____________________estado traduccion media___________________________
      //Obtiene el codigo de la entidad
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      const codEntidadTradMedia = entidadTradMedia['codigo'];
      //Obtiene el estado
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      const codEstadoTradMedia = estadoTradMedia['codigo'];

      //Obtiene el estado activo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      const codEstadoAlbum = estadoAlbum.codigo;

      // obtener idioma
      const catalogoIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );

      const opts = { session };
      const getAlbum = await this.albumModel
        .findOne({ _id: idAlbum, estado: codEstadoAlbum })
        .select('tipo portada estado')
        .populate([
          populateGetMediaTraduciones(
            codigosEstadosMedia.activa,
            codEstadoTradMedia,
            catalogoIdioma.codigo,
          ),
          populateGetPortadaTraduccion(
            codEstadoTradMedia,
            catalogoIdioma.codigo,
          ),
        ]);

      if (!getAlbum) {
        return null;
      }

      let dataAlbum: any = {
        _id: getAlbum._id,
        tipo: { codigo: getAlbum.tipo },
      };

      if (getAlbum['portada']) {
        dataAlbum.portada = {
          _id: getAlbum.portada['_id'],
          principal: {
            _id: getAlbum.portada['principal']._id,
            url: getAlbum.portada['principal'].url,
            duracion: getAlbum.portada['principal']?.duracion,
            fechaActualizacion:
              getAlbum.portada['principal']?.fechaActualizacion,
          },
        };

        // verificar si hay traduccion
        if (getAlbum.portada['traducciones'][0]) {
          const traducciones = [
            {
              _id: getAlbum.portada['traducciones']._id,
              descripcion: getAlbum.portada['traducciones'].descripcion,
            },
          ];
          dataAlbum.portada.traducciones = traducciones;
        }

        //traducir descripcion de la media
        if (getAlbum.portada['traducciones'][0] === undefined) {
          const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
            getAlbum.portada['_id'],
            idioma,
            opts,
          );
          if (traduccionMedia) {
            const traducciones = [
              {
                _id: traduccionMedia._id,
                descripcion: traduccionMedia.descripcion,
              },
            ];
            dataAlbum.portada.traducciones = traducciones;
          }
        }
      }
      //datos media
      if (getAlbum['media']) {
        let listaMedia = [];
        for (const media of getAlbum['media']) {
          let dataMedia: any = {
            _id: media['_id'],
          };
          if (media['principal']) {
            dataMedia.principal = {
              _id: media['principal']._id,
              url: media['principal'].url,
              duracion: media['principal']?.duracion,
              fechaActualizacion: media['principal']?.fechaActualizacion,
            };
          }
          if (media['miniatura']) {
            dataMedia.miniatura = {
              _id: media['miniatura']._id,
              url: media['miniatura'].url,
              duracion: media['principal']?.duracion,
            };
          }

          if (media['traducciones'][0]) {
            dataMedia.traducciones = [
              {
                _id: media['traducciones'][0]._id,
                descripcion: media['traducciones'][0].descripcion,
              },
            ];
          }

          //traducir descripcion de la media
          if (media['traducciones'][0] === undefined) {
            const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
              media['_id'],
              idioma,
              opts,
            );
            if (traduccionMedia) {
              dataMedia.traducciones = [
                {
                  _id: traduccionMedia._id,
                  descripcion: traduccionMedia.descripcion,
                },
              ];
            }
          }
          listaMedia.push(dataMedia);
        }
        dataAlbum.media = listaMedia;
      }
      return dataAlbum;
    } catch (error) {
      throw error;
    }
  }
}
