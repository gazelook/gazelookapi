import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import * as mongoose from 'mongoose';
import { ObtenerAlbumService } from './obtener-album.service';
import { EliminarMediaService } from '../../media/casos_de_uso/eliminar-media.service';
import { ActualizarAlbumService } from './actualizar-album.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { erroresGeneral } from '../../../shared/enum-errores';
import { GestionAlbumService } from './gestion-album.service';
import { EliminarAlbumService } from './eliminar-album.service';

@Injectable()
export class EliminarMediaAlbumService {
  constructor(
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerAlbumService: ObtenerAlbumService,
    private eliminarMediaService: EliminarMediaService,
    private actualizarAlbumService: ActualizarAlbumService,
    private gestionAlbumService: GestionAlbumService,
    private eliminarAlbumService: EliminarAlbumService,
  ) {}

  async eliminarMediaAlbum(data: any, idEntidad, codigoEntidad): Promise<any> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };
      let idPerfil;
      let datosAlbumHis: any = {};
      const idAlbum = data._id;
      const listaMedias = data.media;
      //const perfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(idPerfil);
      if (
        !mongoose.isValidObjectId(idAlbum) ||
        !mongoose.isValidObjectId(idEntidad)
      ) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }
      if (!this.gestionAlbumService.verificarCodigosEntidad(codigoEntidad)) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }

      const albumQuery: any = await this.obtenerAlbumService.obtenerAlbumById(
        idAlbum,
      );
      if (!albumQuery) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresGeneral.COLECCION_VACIA,
        };
      }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      const getEntidad = await this.gestionAlbumService.getEntidad(
        codigoEntidad,
        idEntidad,
      );

      if (!getEntidad) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresGeneral.COLECCION_VACIA,
        };
      }
      idPerfil = getEntidad.perfil;

      // eliminar portada
      if (data?.portada) {
        await this.albumModel.findByIdAndUpdate(
          idAlbum,
          { portada: null },
          opts,
        );
      }

      //eliminar medias
      if (listaMedias) {
        for (const media of listaMedias) {
          // si la media esta como portada del album se desvincula
          if (albumQuery.portada?.toString() === media._id) {
            // portada null
            await this.actualizarAlbumService.actualizarPortadaAlbum(
              idAlbum,
              null,
              idPerfil,
              opts,
            );
          }
          await this.eliminarMediaService.eliminarMedia(
            media._id,
            idPerfil,
            false,
            opts,
          );
          // eliminar referencia del album
          await this.actualizarAlbumService.eliminarReferenciaMediaToAlbum(
            idAlbum,
            media._id,
            opts,
          );
        }
      }

      const albumUpdated = await this.albumModel
        .findOne({ _id: idAlbum })
        .session(opts.session);
      const dataAlbumHistorico = JSON.parse(JSON.stringify(albumUpdated));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      datosAlbumHis = {
        _id: idEntidad,
        datos: dataAlbumHistorico,
      };
      const dataHistoricoAlbum: any = {
        datos: datosAlbumHis,
        usuario: idPerfil,
        accion: getAccion,
        entidad: codEntidad,
      };

      // crear el historico de album
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      // verificar si el album tiene medias, NO.- eliminar album
      let albumEliminado = false;
      if (albumUpdated.media?.length === 0) {
        const dataAlbum = {
          _id: albumUpdated['_id'],
          usuario: idPerfil,
        };
        await this.eliminarAlbumService.eliminarAlbum(dataAlbum, false, opts);
        albumEliminado = true;
      }

      await session.commitTransaction();
      session.endSession();

      return albumEliminado ? true : albumUpdated;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
