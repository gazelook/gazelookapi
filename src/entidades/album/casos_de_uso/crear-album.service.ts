import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { CatalogoAlbum } from '../../../drivers/mongoose/interfaces/catalogo_album/catalogo-album.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { ActualizarMediaService } from '../../media/casos_de_uso/actualizar-media.service';

@Injectable()
export class CrearAlbumService {
  constructor(
    //private obtenerNombreEntidadService: ObtenerNombreEntidadService,
    //9private obtenerNombreEstadoService: ObtenerNombreEstadoService,
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    @Inject('CATALOGO_ALBUM_MODEL')
    private readonly catalogoAlbumModel: Model<CatalogoAlbum>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private actualizarMediaService: ActualizarMediaService,
  ) {}

  async crearAlbum(data: any, opts: any, idEntidad?: string): Promise<Album> {
    try {
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      // datos album
      const listaIdMedia = [];

      // actualizar el estado de la media, si tiene descripcion la agrega
      if (data.media) {
        for (const media of data.media) {
          let descripcionMedia = null;
          if (media.traducciones && media.traducciones?.length > 0) {
            descripcionMedia = media.traducciones[0].descripcion;
          }
          listaIdMedia.push(media._id); // lista de ids de medias a guardar en el album
          // actualizacion true, se agrega la nueva descripcion
          await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
            media._id,
            data.usuario,
            data.idioma,
            descripcionMedia,
            true,
            opts,
          );
        }
      }
      let dataAlbum = {
        nombre: data.nombre,
        predeterminado: data.predeterminado || false,
        tipo: data.tipo,
        media: listaIdMedia,
        portada: data.portada?._id,
        estado: codEstado,
      };

      //crea album
      const album = await new this.albumModel(dataAlbum).save(opts);

      const dataAlbumHistorico = JSON.parse(JSON.stringify(album));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const datosAlbumHis = {
        _id: idEntidad,
        codigoEntidad: data.codigoEntidad,
        datos: dataAlbumHistorico,
      };

      const dataHistoricoAlbum: any = {
        datos: datosAlbumHis,
        usuario: data.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de album
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      return album;
    } catch (error) {
      throw error;
    }
  }
}
