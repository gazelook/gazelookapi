import { Module, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from './../../catalogos/casos_de_uso/catalogos-services.module';
import { AlbumProviders } from '../drivers/album.provider';
import { CrearAlbumService } from '../../album/casos_de_uso/crear-album.service';
import { ActualizarAlbumService } from './actualizar-album.service';
import { AgregarMediaAlbumService } from './agregar-media-album.service';
import { ObtenerAlbumService } from './obtener-album.service';
import { PerfilServiceModule } from '../../perfil/casos_de_uso/perfil.services.module';
import { EliminarMediaAlbumService } from './eliminar-media-album.service';
import { MediaServiceModule } from '../../media/casos_de_uso/media.services.module';
import { EliminarAlbumService } from './eliminar-album.service';
import { GestionAlbumService } from './gestion-album.service';
import { EliminarAlbumCompletoService } from './eliminar-album-completo.service';
import { ProyectosServicesModule } from '../../proyectos/casos_de_uso/proyectos-services.module';
import { NoticiaServicesModule } from '../../noticia/casos_de_uso/noticia-services.module';
import { ActualizarMediaAlbumService } from './actualizar-media-album.service';
import { CrearAlbumEntidadService } from './crear-album-entidad.service';
import { ActualizarPropiedadesAlbumService } from './actualizar-propiedades-album.service';
import { IntercambioServicesModule } from 'src/entidades/intercambio/casos_de_uso/intercambio-services.module';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    forwardRef(() => PerfilServiceModule),
    MediaServiceModule,
    ProyectosServicesModule,
    NoticiaServicesModule,
    IntercambioServicesModule,
  ],
  providers: [
    ...AlbumProviders,
    CrearAlbumService,
    ActualizarAlbumService,
    AgregarMediaAlbumService,
    ObtenerAlbumService,
    EliminarMediaAlbumService,
    EliminarAlbumService,
    GestionAlbumService,
    EliminarAlbumCompletoService,
    ActualizarMediaAlbumService,
    CrearAlbumEntidadService,
    ActualizarPropiedadesAlbumService,
  ],
  exports: [
    CrearAlbumService,
    ActualizarAlbumService,
    AgregarMediaAlbumService,
    ObtenerAlbumService,
    EliminarMediaAlbumService,
    EliminarAlbumService,
    GestionAlbumService,
    EliminarAlbumCompletoService,
    ActualizarMediaAlbumService,
    CrearAlbumEntidadService,
    ActualizarPropiedadesAlbumService,
  ],
  controllers: [],
})
export class AlbumServiceModule {}
