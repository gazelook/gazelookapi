import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Intercambio } from 'src/drivers/mongoose/interfaces/intercambio/intercambio.interface';
import { ObtenerIntercambioIdService } from 'src/entidades/intercambio/casos_de_uso/obtener-intercambio-id.service';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { CatalogoAlbum } from '../../../drivers/mongoose/interfaces/catalogo_album/catalogo-album.interface';
import { Noticia } from '../../../drivers/mongoose/interfaces/noticia/noticia.interface';
import { Perfil } from '../../../drivers/mongoose/interfaces/perfil/perfil.interface';
import { Proyecto } from '../../../drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { codigoEntidades } from '../../../shared/enum-sistema';
import { ObtenerNoticiaIdService } from '../../noticia/casos_de_uso/obtener-noticia-id.service';
import { ObtenerPerfilUsuarioService } from '../../perfil/casos_de_uso/obtener-perfil-usuario.service';
import { ObtenerProyetoIdService } from '../../proyectos/casos_de_uso/obtener-proyecto-id.service';

@Injectable()
export class GestionAlbumService {
  constructor(
    //private obtenerNombreEntidadService: ObtenerNombreEntidadService,
    //9private obtenerNombreEstadoService: ObtenerNombreEstadoService,
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    @Inject('CATALOGO_ALBUM_MODEL')
    private readonly catalogoAlbumModel: Model<CatalogoAlbum>,
    @Inject('NOTICIA_MODEL') private readonly noticiaModel: Model<Noticia>,
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('INTERCAMBIO_MODEL')
    private readonly intercambioModel: Model<Intercambio>,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private obtenerProyetoIdService: ObtenerProyetoIdService,
    private obtenerNoticiaIdService: ObtenerNoticiaIdService,
    private obtenerIntercambioIdService: ObtenerIntercambioIdService,
  ) {}

  async verificarAlbumUnico(listaAlbum: any[]): Promise<Boolean> {
    let bandera = true;
    if (await this.existeAlbum(listaAlbum)) {
      if (await this.elementosRepetidos(listaAlbum)) {
        bandera = false;
      }
    } else {
      bandera = false;
    }
    return bandera;
  }

  async elementosRepetidos(lista: any[]): Promise<Boolean> {
    return lista.some(function(v, i) {
      return lista.indexOf(v, i + 1) > -1;
    });
  }

  async existeAlbum(listaAlbum: any[]): Promise<Boolean> {
    const tipoAlbunes = await this.catalogoAlbumModel.find();
    for (let i = 0; i < listaAlbum.length; i++) {
      const result = tipoAlbunes.findIndex(
        element => element.codigo === listaAlbum[i],
      );
      if (result === -1) {
        return false;
      }
    }
    return true;
  }

  verificarCodigosEntidad(codigoEntidad) {
    let codigos = [
      codigoEntidades.entidadProyectos,
      codigoEntidades.entidadNoticias,
      codigoEntidades.entidadPerfiles,
      codigoEntidades.intercambio,
    ];
    if (codigos.indexOf(codigoEntidad) !== -1) {
      return true;
    } else {
      return false;
    }
  }

  async getEntidad(codigoEntidad, idEntidad) {
    switch (codigoEntidad) {
      case codigoEntidades.entidadProyectos:
        const proyecto = await this.obtenerProyetoIdService.obtenerProyectoById(
          idEntidad,
        );
        return proyecto ? { perfil: proyecto.perfil.toString() } : null;
      case codigoEntidades.entidadNoticias:
        const noticia = await this.obtenerNoticiaIdService.obtenerNoticiaById(
          idEntidad,
        );
        return noticia ? { perfil: noticia.perfil.toString() } : null;
      case codigoEntidades.entidadPerfiles:
        const perfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
          idEntidad,
        );
        return perfil ? { perfil: perfil._id.toString() } : null;

      case codigoEntidades.intercambio:
        const intercambio = await this.obtenerIntercambioIdService.obtenerIntercambioById(
          idEntidad,
        );
        return intercambio
          ? { perfil: intercambio.perfil._id.toString() }
          : null;
      default:
        return;
    }
  }

  async agregarAlbumEntidad(codigoEntidad, idEntidad, idAlbum, opts) {
    switch (codigoEntidad) {
      case codigoEntidades.entidadProyectos:
        return await this.proyectoModel.updateOne(
          { _id: idEntidad },
          { $push: { adjuntos: idAlbum } },
          opts,
        );
      case codigoEntidades.entidadNoticias:
        return await this.noticiaModel.updateOne(
          { _id: idEntidad },
          { $push: { adjuntos: idAlbum } },
          opts,
        );
      case codigoEntidades.entidadPerfiles:
        return await this.perfilModel.updateOne(
          { _id: idEntidad },
          { $push: { album: idAlbum } },
          opts,
        );

      case codigoEntidades.intercambio:
        return await this.intercambioModel.updateOne(
          { _id: idEntidad },
          { $push: { adjuntos: idAlbum } },
          opts,
        );
      default:
        return;
    }
  }
}
