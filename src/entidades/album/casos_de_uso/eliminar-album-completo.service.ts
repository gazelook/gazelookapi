import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { EliminarMediaCompletoService } from '../../media/casos_de_uso/eliminar-media-completo.service';

@Injectable()
export class EliminarAlbumCompletoService {
  constructor(
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    private eliminarMediaCompletoService: EliminarMediaCompletoService,
  ) {}

  async eliminarAlbumCompleto(idAlbum: string, opts): Promise<any> {
    try {
      const album = await this.albumModel
        .findOne({ _id: idAlbum })
        .populate([{ path: 'media' }, { path: 'portada' }]);
      if (!album) {
        return { message: 'no existe el album' };
      }
      console.log('_____________________________________________________');
      console.log('ID ALBUM....', album._id);
      console.log('album....', album);

      //eliminar medias del album
      if (album?.media[0] !== undefined) {
        for (const media of album.media) {
          console.log('media.elim..', media);
          await this.eliminarMediaCompletoService.eliminarMediaCompleto(
            media['_id'],
            opts,
          );
          console.log('elimino media..');
        }
      }
      //verificar si tiene portada, eliminar
      if (album?.portada) {
        console.log('album?.portada...: ', album?.portada);
        await this.eliminarMediaCompletoService.eliminarMediaCompleto(
          album.portada['_id'],
          opts,
        );
        console.log('elimino portada de album');
      }
      // eliminar album
      await this.albumModel.deleteOne({ _id: idAlbum }, opts);

      return true;
    } catch (error) {
      throw error;
    }
  }
}
