import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { ObtenerAlbumService } from './obtener-album.service';
import { EliminarMediaService } from '../../media/casos_de_uso/eliminar-media.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

@Injectable()
export class EliminarAlbumService {
  constructor(
    //private obtenerNombreEntidadService: ObtenerNombreEntidadService,
    //9private obtenerNombreEstadoService: ObtenerNombreEstadoService,
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerAlbumService: ObtenerAlbumService,
    private eliminarMediaService: EliminarMediaService,
  ) {}

  async eliminarAlbum(data: any, hibernado: boolean, opts?): Promise<any> {
    try {
      const idAlbum = data._id;
      const idUsuario = data.usuario;

      const albumQuery: any = await this.obtenerAlbumService.obtenerAlbumById(
        idAlbum,
      );
      if (!albumQuery) {
        return { message: 'no existe el album' };
      }

      let accion = '';

      //Obtiene el codigo de la accion a realizarse
      const accionEli = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      accion = accionEli.codigo;

      //Obtiene el codigo de la accion a realizarse
      const accionMod = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      // estado hibernado
      const estadoHibernado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.hibernado,
        codEntidad,
      );
      const codEstadoHib = estadoHibernado['codigo'];

      //actualizar estado eliminar
      const dataAlbum = {
        estado: codEstado,
      };
      // si el perfil esta hibernado
      if (hibernado) {
        dataAlbum.estado = codEstadoHib;
        accion = accionMod.codigo;
      }

      const album = await this.albumModel.findByIdAndUpdate(
        idAlbum,
        dataAlbum,
        opts,
      );

      //eliminar medias del album
      if (albumQuery.media[0] !== null) {
        for (const media of albumQuery.media) {
          await this.eliminarMediaService.eliminarMedia(
            media._id,
            idUsuario,
            hibernado,
            opts,
          );
        }
      }

      const albumUpdated = await this.albumModel
        .findOne({ _id: idAlbum })
        .session(opts.session);
      const dataAlbumHistorico = JSON.parse(JSON.stringify(albumUpdated));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const dataHistoricoAlbum: any = {
        datos: dataAlbumHistorico,
        usuario: idUsuario,
        accion: accion,
        entidad: codEntidad,
      };
      // crear el historico de album
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      //const result: any = await this.obtenerAlbumService.obtenerAlbumById(idAlbum);

      return albumUpdated;
    } catch (error) {
      throw error;
    }
  }
}
