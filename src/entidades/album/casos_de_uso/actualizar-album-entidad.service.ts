import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { erroresGeneral } from '../../../shared/enum-errores';
import { codigosEstadosMedia } from '../../../shared/enum-sistema';
import { ActualizarMediaService } from '../../media/casos_de_uso/actualizar-media.service';
import { GestionAlbumService } from './gestion-album.service';

//TODO "hasta proximo aviso"
@Injectable()
export class ActualizarAlbumEntidadService {
  constructor(
    private gestionAlbumService: GestionAlbumService,
    private actualizarMediaService: ActualizarMediaService,
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
  ) {}

  async actualizarAlbumEntidad(
    idEntidad: string,
    codigoEntidad: string,
  ): Promise<any> {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      if (!mongoose.isValidObjectId(idEntidad)) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }
      if (!this.gestionAlbumService.verificarCodigosEntidad(codigoEntidad)) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }

      const getEntidad = await this.gestionAlbumService.getEntidad(
        codigoEntidad,
        idEntidad,
      );
      if (!getEntidad)
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };

      await session.commitTransaction();
      session.endSession();

      const resultado = {
        _id: '',
      };
      return resultado;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }

  async accionesMediaAlbum(idPerfil, idAlbum, media, idioma, opts) {
    switch (media.estado.codigo) {
      case codigosEstadosMedia.activa:
        let descripcionMediaActiva = null;
        if (media.traducciones && media.traducciones?.length > 0) {
          descripcionMediaActiva = media.traducciones[0].descripcion;
        }
        await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
          media._id,
          idPerfil,
          idioma,
          descripcionMediaActiva,
          true,
          opts,
        );
      case codigosEstadosMedia.eliminado:

      case codigosEstadosMedia.sinAsignar:
        // verificar traduccion media
        let descripcionSinAsignar;
        if (media.traducciones && media.traducciones?.length > 0) {
          descripcionSinAsignar = media.traducciones[0].descripcion;
        }
        //agregar una media al album
        await this.albumModel.updateOne(
          { _id: idAlbum },
          { $push: { media: media._id } },
          opts,
        );
        //await this.actualizarMediaService.actualizarEstadoAsignado(data.album.media._id, perfil.usuario._id);
        await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
          media._id,
          idPerfil,
          idioma,
          descripcionSinAsignar,
          false,
          opts,
        );
      default:
        return;
    }
  }
}
