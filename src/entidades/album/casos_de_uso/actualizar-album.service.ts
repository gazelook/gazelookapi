import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import {
  codigosHibernadoEntidades,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { ActualizarMediaService } from '../../media/casos_de_uso/actualizar-media.service';
import { ObtenerAlbumService } from './obtener-album.service';

@Injectable()
export class ActualizarAlbumService {
  constructor(
    //private obtenerNombreEntidadService: ObtenerNombreEntidadService,
    //9private obtenerNombreEstadoService: ObtenerNombreEstadoService,
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerAlbumService: ObtenerAlbumService,
    private actualizarMediaService: ActualizarMediaService,
  ) {}

  async activarAlbum(data: any, opts: any): Promise<any> {
    try {
      const idAlbum = data._id;
      const idUsuario = data.perfil;

      const albumHibernado: any = await this.obtenerAlbumService.obtenerAlbumByIdEstado(
        idAlbum,
        codigosHibernadoEntidades.albumHibernado,
      );
      if (!albumHibernado) {
        return { message: 'no existe el album' };
      }
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      // datos album
      //actualizar estado
      const dataAlbum = {
        estado: codEstado,
      };

      const album = await this.albumModel.findByIdAndUpdate(
        idAlbum,
        dataAlbum,
        opts,
      );

      //modificar medias del album
      if (albumHibernado.media[0] != undefined) {
        for (const media of albumHibernado.media) {
          await this.actualizarMediaService.activarMedia(
            media._id,
            idUsuario,
            opts,
          );
        }
      }

      const dataAlbumHistorico = JSON.parse(JSON.stringify(album));
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const dataHistoricoAlbum: any = {
        datos: dataAlbumHistorico,
        usuario: idUsuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de album
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      return album;
    } catch (error) {
      throw error;
    }
  }
  // se lo utiliza en actualizar perfil
  async actualizarAlbumconMedia(idAlbum, data: any, opts: any): Promise<any> {
    try {
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      // ______ datos album______
      const listaIdMedia = [];
      //verificar si hay medias
      if (data.media) {
        for (const media of data.media) {
          let descripcionMedia = null;
          if (media.traducciones && media.traducciones?.length > 0) {
            descripcionMedia = media.traducciones[0].descripcion;
          }
          listaIdMedia.push(media._id); // lista de ids de medias a guardar en el album
          await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
            media._id,
            data.usuario,
            data.idioma,
            descripcionMedia,
            true,
            opts,
          );
        }
      }
      let dataAlbum: any = {
        nombre: data.nombre,
        tipo: data.tipo,
      };
      if (listaIdMedia) {
        dataAlbum.media = listaIdMedia;
      }
      if (data.portada) {
        dataAlbum.portada = data.portada;
      }

      //actualizar album
      const album = await this.albumModel.findByIdAndUpdate(
        idAlbum,
        dataAlbum,
        opts,
      );
      const dataAlbumHistorico = JSON.parse(JSON.stringify(album));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const dataHistoricoAlbum: any = {
        datos: dataAlbumHistorico,
        usuario: data.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de album
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      return album;
    } catch (error) {
      throw error;
    }
  }

  async actualizarPortadaAlbum(idAlbum, idMedia, perfil, opts): Promise<any> {
    try {
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      // ______ datos album______

      let dataAlbum: any = {
        portada: idMedia,
      };

      // update album
      const album = await this.albumModel.findByIdAndUpdate(
        idAlbum,
        dataAlbum,
        opts,
      );
      const dataAlbumHistorico = JSON.parse(JSON.stringify(album));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const dataHistoricoAlbum: any = {
        datos: dataAlbumHistorico,
        usuario: perfil,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de album
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      return album;
    } catch (error) {
      throw error;
    }
  }

  // retorna una nueva lista
  async eliminarReferenciaMediaToAlbum(
    idAlbum,
    idMedia,
    opts: any,
  ): Promise<any> {
    let listaMediasString = [];
    try {
      const getAlbum = await this.albumModel.findOne({ _id: idAlbum });
      for (const media of getAlbum.media) {
        listaMediasString.push(media.toString());
      }
      let index = listaMediasString.indexOf(idMedia);
      //Eliminamos el elemento del array
      listaMediasString.splice(index, 1);

      // ______ datos album______
      let dataAlbum: any = {
        media: listaMediasString,
      };

      // update album
      const album = await this.albumModel.findByIdAndUpdate(
        idAlbum,
        dataAlbum,
        opts,
      );
      return album;
    } catch (error) {
      throw error;
    }
  }

  // without use
  /* async actualizarAlbum(idAlbum, data: any, idEntidad?: string): Promise<any> {

        // Inicia proceso de transaccion
        const session = await startSession();
        session.startTransaction();

        try {
            //Obtiene el codigo de la accion a realizarse
            const accion = await this.catalogoAccionService.obtenerNombreAccion('modificar')
            const getAccion = accion['codigo'];

            //Obtiene el codigo de la entidad
            const entidad = await this.catalogoEntidadService.obtenerNombreEntidad('album')
            const codEntidad = entidad['codigo'];

            //Obtiene el estado
            const estado = await this.catalogoEstadoService.obtenerNombreEstado('activa', codEntidad)
            const codEstado = estado['codigo'];

            // datos album
            const listaIdMedia = [];
            const lista = data.media
            for (let j = 0; j < lista.length; j++) {
                listaIdMedia.push(lista[j]._id);
                await this.actualizarMediaService.actualizarEstadoAsignado(lista[j]._id, data.usuario);
            }
            let dataAlbum = {
                nombre: data.nombre,
                tipo: data.tipo,
                media: listaIdMedia,
                portada: data.portada._id,
            }

            //crea album
            const album = await this.albumModel.findByIdAndUpdate(
                idAlbum,
                dataAlbum,
                { new: true }
            );
            const dataAlbumHistorico = JSON.parse(JSON.stringify(album));
            //eliminar parametro no necesarios
            delete dataAlbumHistorico.fechaCreacion
            delete dataAlbumHistorico.fechaActualizacion
            delete dataAlbumHistorico.__v
            let dataEntidad = {
                _id: idEntidad || null,
                datos: dataAlbumHistorico
            }
            const dataHistoricoAlbum: any = {
                datos: dataEntidad,
                usuario: data.usuario,
                accion: getAccion,
                entidad: codEntidad
            }
            // crear el historico de album
            await this.crearHistoricoService.crearHistorico(dataHistoricoAlbum);
            await session.commitTransaction();
            session.endSession();

            return album;
        } catch (error) {
            await session.abortTransaction();
            session.endSession();
            throw error;
        }
    } */
}
