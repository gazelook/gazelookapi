import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import * as mongoose from 'mongoose';
import { ObtenerAlbumService } from './obtener-album.service';
import { ObtenerPerfilUsuarioService } from '../../perfil/casos_de_uso/obtener-perfil-usuario.service';
import { ActualizarMediaService } from '../../media/casos_de_uso/actualizar-media.service';
import { ObtenerProyetoIdService } from '../../proyectos/casos_de_uso/obtener-proyecto-id.service';
import {
  codigoEntidades,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { ObtenerNoticiaIdService } from '../../noticia/casos_de_uso/obtener-noticia-id.service';
import { erroresGeneral } from '../../../shared/enum-errores';
import { GestionAlbumService } from './gestion-album.service';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';

@Injectable()
export class ActualizarMediaAlbumService {
  constructor(
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    @Inject('NOTICIA_MODEL') private readonly noticiaModel: Model<Noticia>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerAlbumService: ObtenerAlbumService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private actualizarMediaService: ActualizarMediaService,
    private obtenerProyetoIdService: ObtenerProyetoIdService,
    private obtenerNoticiaIdService: ObtenerNoticiaIdService,
    private gestionAlbumService: GestionAlbumService,
  ) {}

  async actualizarMediaAlbum(
    data: any,
    idEntidad: string,
    codigoEntidad: string,
    idioma: string,
  ): Promise<any> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();

    const opts = { session };

    try {
      let idPerfil;
      let proyecto;
      let noticia;
      let perfil;
      let datosAlbumHis: any = {};
      const idAlbum = data._id;
      const listaMedias = data.media;

      if (
        !mongoose.isValidObjectId(idAlbum) ||
        !mongoose.isValidObjectId(idEntidad)
      ) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }
      if (!this.gestionAlbumService.verificarCodigosEntidad(codigoEntidad)) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
        };
      }

      const albumQuery: any = await this.obtenerAlbumService.obtenerAlbumById(
        idAlbum,
      );
      if (!albumQuery) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresGeneral.COLECCION_VACIA,
        };
      }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      //const perfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(idPerfil);
      if (codigoEntidades.entidadProyectos === codigoEntidad) {
        proyecto = await this.obtenerProyetoIdService.obtenerProyectoById(
          idEntidad,
        );
        idPerfil = proyecto.perfil.toString();
      }

      if (codigoEntidades.entidadNoticias === codigoEntidad) {
        noticia = await this.obtenerNoticiaIdService.obtenerNoticiaById(
          idEntidad,
        );
        idPerfil = noticia.perfil.toString();
      }

      if (codigoEntidades.entidadPerfiles === codigoEntidad) {
        perfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
          idEntidad,
        );
        idPerfil = perfil._id.toString();
      }

      if (!proyecto && !noticia && !perfil) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresGeneral.COLECCION_VACIA,
        };
      }

      if (listaMedias) {
        for (const media of listaMedias) {
          // verificar traduccion media
          let descripcionMedia = null;
          if (media.traducciones && media.traducciones?.length > 0) {
            descripcionMedia = media.traducciones[0].descripcion;
          }

          await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
            media._id,
            idPerfil,
            idioma,
            descripcionMedia,
            true,
            opts,
          );
        }

        //Actualiza la noticia
        if (codigoEntidades.entidadNoticias === codigoEntidad) {
          await this.noticiaModel.updateOne(
            { _id: idEntidad },
            { fechaActualizacion: new Date() },
            opts,
          );
        }

        //Actualiza el proyecto
        if (codigoEntidades.entidadProyectos === codigoEntidad) {
          await this.proyectoModel.updateOne(
            { _id: idEntidad },
            { fechaActualizacion: new Date() },
            opts,
          );
        }
      }

      const albumUpdated = await this.albumModel
        .findOne({ _id: idAlbum })
        .session(opts.session);
      const dataAlbumHistorico = JSON.parse(JSON.stringify(albumUpdated));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      datosAlbumHis = {
        _id: idEntidad,
        datos: dataAlbumHistorico,
      };

      const dataHistoricoAlbum: any = {
        datos: datosAlbumHis,
        usuario: idPerfil,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de album
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);
      await session.commitTransaction();
      session.endSession();

      //const result: any = await this.obtenerAlbumService.obtenerAlbumById(idAlbum);

      return albumUpdated;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
