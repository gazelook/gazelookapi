import { Connection } from 'mongoose';
import { IntercambioModelo } from 'src/drivers/mongoose/modelos/intercambio/intercambio.schema';
import { albumModelo } from '../../../drivers/mongoose/modelos/album/album.schema';
import { catalogoAlbumModelo } from '../../../drivers/mongoose/modelos/catalogo_album/catalogo-album.schema';
import { noticiaModelo } from '../../../drivers/mongoose/modelos/noticias/noticia-schema';
import { perfilModelo } from '../../../drivers/mongoose/modelos/perfil/perfil.schema';
import { ProyectoModelo } from '../../../drivers/mongoose/modelos/proyectos/proyecto.schema';

export const AlbumProviders = [
  {
    provide: 'ALBUM_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('album', albumModelo, 'album'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_ALBUM_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('catalogo_album', catalogoAlbumModelo, 'catalogo_album'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('proyecto', ProyectoModelo, 'proyecto'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'NOTICIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('noticia', noticiaModelo, 'noticia'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('intercambio', IntercambioModelo, 'intercambio'),
    inject: ['DB_CONNECTION'],
  },
];
