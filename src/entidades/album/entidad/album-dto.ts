import { ApiProperty } from '@nestjs/swagger';
import {
  RetornoPrincipalMediaDto,
  urlFotoPerfil,
} from 'src/entidades/media/entidad/archivo-resultado.dto';
import { TipoAlbum } from 'src/entidades/perfil/entidad/crear-perfil.dto';
import { MediaAdjuntos } from './retorno-media-adjuntos.dto';

export class IdentAlbumDto {
  @ApiProperty()
  _id: string;
}

export class RetornoMediaDto {
  @ApiProperty({ type: [RetornoPrincipalMediaDto] })
  media: [RetornoPrincipalMediaDto];
}

export class albumPrincipalPerfilSistemaDto {
  @ApiProperty({ type: urlFotoPerfil })
  principal: urlFotoPerfil;
}
export class albumPerfilSistemaDto {
  @ApiProperty()
  tipo: TipoAlbum;

  @ApiProperty({ type: albumPrincipalPerfilSistemaDto })
  portada: albumPrincipalPerfilSistemaDto;
}

export class adjuntosNoticiasPerfilDto {
  @ApiProperty()
  tipo: TipoAlbum;

  @ApiProperty({})
  predeterminado: boolean;

  @ApiProperty({ type: MediaAdjuntos })
  portada: MediaAdjuntos;

  @ApiProperty({ type: [MediaAdjuntos] })
  media: [MediaAdjuntos];
}
