import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayNotEmpty,
  IsArray,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
} from 'class-validator';

class TraduccionMedia {
  @ApiProperty()
  @IsOptional()
  descripcion: string;
}

class MediaActAlbum {
  @ApiProperty({ description: 'id de la media' })
  @IsMongoId()
  _id: string;
  @ApiProperty({
    description: 'descripcion de la media',
    type: [TraduccionMedia],
  })
  @IsOptional()
  traducciones: TraduccionMedia[];
}

export class AlbumActEntidad {
  @ApiProperty({ description: 'id del album' })
  @IsMongoId()
  _id: string;
  @ApiProperty({ type: [MediaActAlbum] })
  @IsNotEmpty()
  @IsArray()
  @ArrayNotEmpty()
  media: MediaActAlbum[];
}
