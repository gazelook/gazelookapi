import { ApiProperty } from '@nestjs/swagger';

export class ArchivoAlbum {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  url: string;
}
export class MediaAlbum {
  @ApiProperty()
  _id: string;
  @ApiProperty({ type: ArchivoAlbum })
  principal: ArchivoAlbum;
  @ApiProperty({ type: ArchivoAlbum })
  miniatura: ArchivoAlbum;
}

export class RetornoMediaAlbumDto {
  @ApiProperty({ type: MediaAlbum })
  media: MediaAlbum;
}
