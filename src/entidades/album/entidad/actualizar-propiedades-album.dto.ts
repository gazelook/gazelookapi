import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsMongoId,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator';

export class EstadoAlbum {
  @ApiProperty()
  @IsOptional()
  @IsString()
  codigo: string;
}
export class TipoAlbum {
  @ApiProperty()
  @IsOptional()
  @IsString()
  codigo: string;
}

export class AlbumActPropiedades {
  @ApiProperty({ description: 'id del album' })
  @IsMongoId()
  _id: string;

  @ApiProperty({ type: Boolean })
  @IsOptional()
  @IsBoolean()
  predeterminado: boolean;

  @ApiProperty({ type: EstadoAlbum })
  @IsOptional()
  @IsObject()
  estado: EstadoAlbum;

  @ApiProperty({ type: TipoAlbum })
  @IsOptional()
  @IsObject()
  tipo: TipoAlbum;
}
