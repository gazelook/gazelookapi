import { ApiProperty } from '@nestjs/swagger';
import { codigoCatalogoMediaDto } from 'src/entidades/media/entidad/archivo-resultado.dto';
import { ArchivoAlbum } from './retorno-media-album.dto';

export class MediaAdjuntos {
  @ApiProperty({ type: codigoCatalogoMediaDto })
  catalogoMedia: codigoCatalogoMediaDto;
  @ApiProperty({ type: ArchivoAlbum })
  principal: ArchivoAlbum;
  @ApiProperty({ type: ArchivoAlbum })
  miniatura: ArchivoAlbum;
}
