import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayNotEmpty,
  IsArray,
  IsMongoId,
  IsObject,
  IsOptional,
} from 'class-validator';

class TraduccionMedia {
  @ApiProperty()
  @IsOptional()
  descripcion: string;
}

class MediaAlbumEntidad {
  @ApiProperty({ description: 'id de la media' })
  @IsMongoId()
  _id: string;
  @ApiProperty({
    description: 'descripcion de la media',
    type: [TraduccionMedia],
  })
  @IsOptional()
  traducciones: TraduccionMedia[];
}

class PortadaMedia {
  @ApiProperty({ description: 'id de la lista de media' })
  @IsMongoId()
  _id: string;
}

export class AlbumEntidad {
  @ApiProperty({ description: 'id del album' })
  @IsMongoId()
  _id: string;
  @ApiProperty({ type: PortadaMedia })
  @IsOptional()
  @IsObject()
  portada: PortadaMedia;
  @ApiProperty({ type: MediaAlbumEntidad })
  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  media: MediaAlbumEntidad;
}
