import { Module } from '@nestjs/common';
import { HistoricoControllerModule } from './controlador/v1/historico-controller.module';

@Module({
  imports: [HistoricoControllerModule],
  providers: [],
  controllers: [],
})
export class HistoricoModule {}
