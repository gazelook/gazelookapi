import { HttpService, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { ConfigService } from 'src/config/config.service';
import { Historico } from 'src/drivers/mongoose/interfaces/historico/historico.interface';
import { pathServerHistorico } from 'src/shared/enum-servidor-historico';
import { HistoricoDto } from '../entidad/historicoEntidad';

@Injectable()
export class CrearHistoricoService {
  constructor(
    @Inject('HISTORICO_MODEL')
    private readonly historicoModel: Model<Historico>,
    private config: ConfigService,
    private httpService: HttpService,
  ) {}

  async crearHistorico(crearHistoricoDto: HistoricoDto): Promise<Historico> {
    //Objeto de datos
    let newHistorico = {
      datos: crearHistoricoDto.datos,
      usuario: crearHistoricoDto.usuario,
      accion: crearHistoricoDto.accion,
      entidad: crearHistoricoDto.entidad,
      //registro: crearHistoricoDto.registro,
      tipo: crearHistoricoDto.tipo,
    };

    try {
      //Almacenamiento de historico en a base de datos
      const historico = new this.historicoModel(newHistorico);
      const crearHistorico = await historico.save();
      return crearHistorico;
    } catch (error) {
      throw error;
    }
  }

  async crearHistoricoTransaccion(
    crearHistoricoDto: HistoricoDto,
    opts,
  ): Promise<Historico> {
    //Objeto de datos
    let newHistorico = {
      datos: crearHistoricoDto.datos,
      usuario: crearHistoricoDto.usuario,
      accion: crearHistoricoDto.accion,
      entidad: crearHistoricoDto.entidad,
      tipo: crearHistoricoDto.tipo,
    };

    try {
      //Almacenamiento de historico en a base de datos
      const historico = new this.historicoModel(newHistorico);
      const crearHistorico = await historico.save(opts);
      return crearHistorico;
    } catch (error) {
      throw error;
    }
  }
  crearHistoricoServer(data: HistoricoDto) {
    try {
      this.enviarHistoricoServer(data).subscribe(
        response => {
          console.log('OK: ', response.data);
        },
        error => {
          console.log('Error____________: ');
        },
      );
    } catch (error) {
      throw error;
    }
  }

  enviarHistoricoServer(data: HistoricoDto): Observable<any> {
    try {
      const apiKey = this.config.get('API_KEY');
      const URL_SERVER_HISTORICO = this.config.get('URL_SERVER_HISTORICO');
      const headersRequest: any = {
        'Content-Type': 'application/json',
        apiKey: apiKey,
      };
      return this.httpService.post(
        `${URL_SERVER_HISTORICO}${pathServerHistorico.CREAR_HISTORICO}`,
        data,
        { headers: headersRequest },
      );
    } catch (error) {
      throw error;
    }
  }
}
