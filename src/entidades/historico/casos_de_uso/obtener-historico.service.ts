import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Historico } from 'src/drivers/mongoose/interfaces/historico/historico.interface';

@Injectable()
export class ObtenerHistoricoService {
  constructor(
    @Inject('HISTORICO_MODEL')
    private readonly historicoModel: Model<Historico>,
  ) {}

  async obtenerHistorico(
    codEntidad: any,
    codAccion: any,
    idEntidad: any,
  ): Promise<any> {
    try {
      //Obtener historico en la base de datos
      const historico = await this.historicoModel.find({
        entidad: codEntidad,
        accion: codAccion,
      });
      if (historico.length > 0) {
        let arrayHistorico = [];
        for (const getHistorico of historico) {
          if (getHistorico.datos['_id']) {
            if ((getHistorico.datos['_id'] = idEntidad)) {
              let objHistorico = {
                datos: getHistorico.datos,
                fechaActualizacion: getHistorico.fechaCreacion,
              };
              arrayHistorico.push(objHistorico);
            }
          }
        }
        return arrayHistorico;
      }
    } catch (error) {
      throw error;
    }
  }

  async obtenerHistorialSolicitudes(tipo: string): Promise<any> {
    try {
      let listaHistorico = [];
      const getHistorico = await this.historicoModel.find({ tipo: tipo });
      if (getHistorico.length > 0) {
        for (const historico of getHistorico) {
          let objHistorico = {
            datos: historico.datos,
            fecha: historico.fechaCreacion,
          };
          listaHistorico.push(objHistorico);
        }
      }
      return listaHistorico;
    } catch (error) {
      throw error;
    }
  }
}
