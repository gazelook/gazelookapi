import { Controller, Get, Headers, HttpStatus, Param } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { idiomas } from '../../../../shared/enum-sistema';
import { ObtenerHistoricoService } from '../../casos_de_uso/obtener-historico.service';

@ApiTags('Historico')
@Controller('api/historico/historial-solicitudes')
export class ObtenerHistorialSolicitudesControlador {
  constructor(
    private obtenerHistoricoService: ObtenerHistoricoService,
    private readonly i18n: I18nService,
  ) {}

  @Get('/:tipo')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    required: true,
    description: 'key para usar las rutas',
  })
  @ApiOperation({
    summary:
      'Recibe como parametro el tipo de historico, ejemplo: CATHIS_03 .Retorna la informacion de los usuarios que han solicitado su retiro de la aplicación',
  })
  @ApiResponse({ status: 200, description: 'Historial de solicitudes' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  //@UseGuards(AuthGuard('jwt'))
  public async informacionAplicacion(
    @Headers() headers,
    @Param('tipo') tipo: string,
  ) {
    const respuesta = new RespuestaInterface();
    let codIdioma = '';
    if (headers.idioma !== undefined) {
      codIdioma = headers.idioma;
    } else {
      codIdioma = idiomas.ingles;
    }
    try {
      const informacion = await this.obtenerHistoricoService.obtenerHistorialSolicitudes(
        tipo,
      );

      if (informacion.length > 0) {
        respuesta.codigoEstado = HttpStatus.OK;
        respuesta.respuesta = {
          datos: informacion,
          mensaje: 'Historial de solicitudes',
        };
      } else {
        respuesta.codigoEstado = HttpStatus.NOT_FOUND;
        respuesta.respuesta = {
          mensaje: 'Datos no encontrados',
        };
      }
      return respuesta;
    } catch (e) {
      respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
      respuesta.respuesta = {
        mensaje: e.message,
      };
      return respuesta;
    }
  }
}
