import { Module } from '@nestjs/common';
import { HistoricoServiceModule } from '../../casos_de_uso/historico-services.module';
import { ObtenerHistorialSolicitudesControlador } from './historial-solicitudes.controller';
import { HistoricoControlador } from './historico.controller';

@Module({
  imports: [HistoricoServiceModule],
  providers: [],
  exports: [],
  controllers: [HistoricoControlador, ObtenerHistorialSolicitudesControlador],
})
export class HistoricoControllerModule {}
