import { ApiProperty } from '@nestjs/swagger';

export class HistoricoDto {
  @ApiProperty()
  datos: object;
  @ApiProperty()
  usuario: string;
  @ApiProperty()
  accion: string;
  @ApiProperty()
  entidad: string;
  // @ApiProperty()
  // registro: string;
  @ApiProperty()
  tipo?: string;
}
