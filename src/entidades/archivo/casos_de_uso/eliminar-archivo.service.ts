import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

@Injectable()
export class EliminarArchivoService {
  constructor(
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async eliminarArchivo(idArchivo: any, idUsuario, opts?): Promise<any> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.eliminar,
    );
    const getAccion = accion['codigo'];

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.archivo,
    );
    const codEntidad = entidad['codigo'];

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.eliminado,
      codEntidad,
    );
    const codEstado = estado['codigo'];

    let dataArchivo: any = {};
    try {
      // estado de la archivo eliminado (desactivado)
      dataArchivo.estado = codEstado;
      const archivo = await this.archivoModel.findByIdAndUpdate(
        idArchivo,
        dataArchivo,
        opts,
      );

      const datosArchivo = JSON.parse(JSON.stringify(archivo));
      //eliminar parametro no necesarios
      delete datosArchivo.fechaCreacion;
      delete datosArchivo.fechaActualizacion;
      delete datosArchivo.__v;

      const dataHistoricoMedia: any = {
        datos: datosArchivo,
        usuario: idUsuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

      return archivo;
    } catch (error) {
      throw error;
    }
  }
}
