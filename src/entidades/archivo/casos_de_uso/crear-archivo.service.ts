import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { StorageService } from '../../../drivers/amazon_s3/services/storage.service';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';
import { TipoMedia } from '../../../drivers/mongoose/interfaces/catalogo_tipo_media/catalogo-tipo-media.interface';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { GestionArchivoService } from './gestion-archivo.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { StorageDocumentosUsuarioService } from '../../../drivers/amazon_s3/services/storage-documentos-usuario.service';
import { crearArchivoDocumentoDto } from '../entidad/crear-archivo-documento.dto';
import { crearArchivoDto } from '../entidad/crear-archivo.dto';
import { StorageArchivoDto } from '../../../drivers/amazon_s3/dtos/storage-archivo.dto';

@Injectable()
export class CrearArchivoService {
  constructor(
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    @Inject('TIPO_MEDIA_MODEL')
    private readonly catalogoTipoMedia: Model<TipoMedia>,
    private storageService: StorageService,
    private storageDocumentosUsuarioService: StorageDocumentosUsuarioService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private gestionArchivoService: GestionArchivoService,
  ) {}

  // crear los archivos fisicos y los sube a la nube
  async crearArchivo(
    dataArchivo: crearArchivoDto,
    opts: any,
  ): Promise<Archivo> {
    console.log('dataArchivo.11...', dataArchivo.archivo);

    try {
      const validar = await this.gestionArchivoService.validarArchivo(
        dataArchivo.archivo,
      );
      if (!dataArchivo.archivo) {
        throw { message: 'No hay archivo' };
      }
      if (!validar.validate) {
        throw { message: 'El sistema no admite este tipo de archivo' };
      }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const getAccion = accion['codigo'];
      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.archivo,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];
      console.log('antes', dataArchivo.archivo);

      let dataStorage: StorageArchivoDto;
      if (dataArchivo.fileDefault && dataArchivo.fileDefault === 'true') {
        dataStorage = await this.storageService.cargarArchivo(
          dataArchivo.archivo,
          true,
        );
      } else {
        dataStorage = await this.storageService.cargarArchivo(
          dataArchivo.archivo,
        );
      }

      if (dataStorage) {
        let data = {
          estado: codEstado,
          url: dataStorage.url,
          filename: dataArchivo.archivo.originalname,
          filenameStorage: dataStorage.filename,
          peso: dataStorage.size,
          tipo: dataArchivo.tipo,
          relacionAspecto: dataArchivo.relacionAspecto,
          path: dataStorage.path,
          fileDefault: dataArchivo.fileDefault,
          catalogoArchivoDefault: dataArchivo.catalogoArchivoDefault,
          duracion: dataArchivo?.duracion,
        };

        const archivo = await new this.archivoModel(data);
        await archivo.save(opts);

        const datosHisArch = JSON.parse(JSON.stringify(archivo));
        //eliminar parametro no necesarios
        delete datosHisArch.fechaCreacion;
        delete datosHisArch.fechaActualizacion;
        delete datosHisArch.__v;

        const dataHistoricoMedia: any = {
          datos: datosHisArch,
          usuario: null,
          accion: getAccion,
          entidad: codEntidad,
        };
        // crear el historico del archivo
        this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

        return archivo;
      }
    } catch (error) {
      throw error;
    }
  }

  // crear los archivos tipo link
  async crearArchivoTipoLink(dataEnlace: any, opts): Promise<any> {
    console.log('dataEnlace....', dataEnlace);
    try {
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const getAccion = accion['codigo'];
      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.archivo,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      let data = {
        estado: codEstado,
        url: dataEnlace.enlace,
        tipo: dataEnlace.tipoCatTipMedia,
      };

      const archivo = await new this.archivoModel(data);
      await archivo.save(opts);

      const datosHisArch = JSON.parse(JSON.stringify(archivo));
      //eliminar parametro no necesarios
      delete datosHisArch.fechaCreacion;
      delete datosHisArch.fechaActualizacion;
      delete datosHisArch.__v;

      const dataHistoricoMedia: any = {
        datos: datosHisArch,
        usuario: null,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico del archivo
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

      return archivo;
    } catch (error) {
      throw error;
    }
  }

  // crear documentos usuarios
  async crearArchivoDocumento(
    dataArchivo: crearArchivoDocumentoDto,
    opts: any,
  ): Promise<Archivo> {
    try {
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const getAccion = accion['codigo'];
      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.archivo,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.sinAsignar,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      const dataStorage = await this.storageDocumentosUsuarioService.cargarArchivo(
        dataArchivo.archivo,
      );

      // dataStorage.filename

      if (dataStorage) {
        let data = {
          estado: codEstado,
          url: dataStorage.url,
          filename: dataArchivo.archivo.originalname,
          filenameStorage: dataStorage.filename,
          peso: dataStorage.size,
          tipo: dataArchivo.tipo,
          relacionAspecto: '',
          path: dataStorage.path,
          fileDefault: dataArchivo.fileDefault || false,
        };

        const archivo = await new this.archivoModel(data);
        await archivo.save(opts);

        const datosHisArch = JSON.parse(JSON.stringify(archivo));
        //eliminar parametro no necesarios
        delete datosHisArch.fechaCreacion;
        delete datosHisArch.fechaActualizacion;
        delete datosHisArch.__v;

        const dataHistoricoMedia: any = {
          datos: datosHisArch,
          usuario: null,
          accion: getAccion,
          entidad: codEntidad,
        };
        // crear el historico del archivo
        this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

        return archivo;
      }
    } catch (error) {
      throw error;
    }
  }
}
