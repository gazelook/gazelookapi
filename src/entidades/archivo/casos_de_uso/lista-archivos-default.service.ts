import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { StorageService } from '../../../drivers/amazon_s3/services/storage.service';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';

@Injectable()
export class ListaArchivosDefaultService {
  constructor(
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    private storageService: StorageService,
  ) {}

  async listaArchivosDefault(): Promise<any> {
    try {
      let archivos = await this.archivoModel.find(
        { fileDefault: true },
        { _id: 1, url: 1, catalogoArchivoDefault: 1 },
      );
      return archivos;
    } catch (error) {
      throw error;
    }
  }
}
