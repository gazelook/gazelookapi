import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Media } from 'src/drivers/mongoose/interfaces/media/media.interface';
import { codigosCatalogoMedia } from 'src/shared/enum-sistema';
import { StorageService } from '../../../drivers/amazon_s3/services/storage.service';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';

@Injectable()
export class ListaArchivosDefaultTipoService {
  constructor(
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    @Inject('MEDIA_MODEL') private readonly mediaModel: Model<Media>,
    private storageService: StorageService,
  ) {}

  async listaArchivosDefaultTipo(tipo): Promise<any> {
    try {
      let archivos = await this.archivoModel.find(
        { fileDefault: true, catalogoArchivoDefault: tipo },
        { _id: 1, url: 1, catalogoArchivoDefault: 1 },
      );
      return archivos;
    } catch (error) {
      throw error;
    }
  }

  async getArchivoDefaultTipoAleatorio(tipo): Promise<any> {
    try {
      let getArchivos = await this.archivoModel.find({
        fileDefault: true,
        catalogoArchivoDefault: tipo,
      });
      let count = getArchivos.length - 1;
      let alet = Math.round(Math.random() * count);
      if (alet === 1) {
        alet = 0;
      }
      let archivos = await this.mediaModel
        .find({})
        .populate({
          path: 'principal',
          select:
            'url fileDefault duracion fechaCreacion fechaActualizacion tipo',
          match: {
            fileDefault: true,
            catalogoArchivoDefault: tipo,
          },
        })
        .populate({ path: 'traducciones', select: 'descripcion' });

      let media = [];
      for (let index = 0; index < archivos.length; index++) {
        if (archivos[index].principal) {
          let dataMedia = {
            _id: archivos[index]._id,
            catalogoMedia: codigosCatalogoMedia.catMediaSimple,
            traducciones: archivos[index].traducciones,
            principal: {
              _id: archivos[index].principal['_id'],
              url: archivos[index].principal['url'],
              fileDefault: archivos[index].principal['fileDefault'],
              tipo: {
                codigo: archivos[index].principal['tipo'],
              },
              duracion: archivos[index].principal['duracion'],
              fechaActualizacion:
                archivos[index].principal['fechaActualizacion'],
            },
            fechaCreacion: archivos[index].fechaCreacion,
            fechaActualizacion: archivos[index].fechaActualizacion,
          };
          //console.log('dataMedia: ', dataMedia)
          media.push(dataMedia);
        }
      }
      //console.log('media.length: ', media.length)
      let returnMedia = {
        _id: media[alet]._id,
        catalogoMedia: {
          codigo: media[alet].catalogoMedia,
        },
        traducciones: media[alet].traducciones,
        principal: media[alet].principal,
        fechaCreacion: media[alet].fechaCreacion,
        fechaActualizacion: media[alet].fechaActualizacion,
      };
      return returnMedia;
    } catch (error) {
      throw error;
    }
  }

  async getArchivoDefaultTipoFotoPerfil(tipo): Promise<any> {
    try {
      // let getArchivos = await this.archivoModel.find({ fileDefault: true, catalogoArchivoDefault: tipo });
      // let count = getArchivos.length
      // let alet = Math.round(Math.random() * count);
      let archivos = await this.mediaModel.find({}).populate({
        path: 'principal',
        select: 'url duracion fechaActualizacion',
        match: {
          fileDefault: true,
          catalogoArchivoDefault: tipo,
        },
      });
      let media = [];
      for (let index = 0; index < archivos.length; index++) {
        if (archivos[index].principal) {
          let dataMedia = {
            tipo: archivos[index].catalogoMedia,
            principal: {
              url: archivos[index].principal['url'],
              duracion: archivos[index].principal['duracion'],
              fechaActualizacion:
                archivos[index].principal['fechaActualizacion'],
            },
          };
          media.push(dataMedia);
        }
      }
      return media[0].principal;
    } catch (error) {
      throw error;
    }
  }
}
