import { Injectable, Inject, HttpService } from '@nestjs/common';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';

//const sharp = require('sharp');

@Injectable()
export class CrearMiniaturaService {
  constructor(
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    private httpService: HttpService,
  ) {}

  /* async crearMiniaturaImagen(archivo: any): Promise<any> {
        try {
            const result = await this.redimensionarImagen(archivo)
            // retorna el buffer del archivo
            return result;

        } catch (error) {
            error.message
            throw error;
        }
    } */

  /* async redimensionarImagenBySize(archivo, porcentaje): Promise<any> {
        try {
            const bufferArchivo = await sharp(archivo.buffer)
                .resize(porcentaje)
                .toBuffer()
                .then(data => {
                    // appendFile(archivo.originalname, data, (error) => {
                    //    if (error) {
                    //        throw error;
                   //     }
                    //}) 
                    const buffer = data;
                    return buffer;
                })
                .catch(err => {
                    
                });
                return bufferArchivo
        } catch (error) {
            console.error(error)
            throw error
        }
    } */

  /* async redimensionarImagen(archivo): Promise<any> {
        try {
            //const dirPath = resolve(__dirname, `./tmp/`);
            const bufferArchivo = await sharp(archivo.buffer)
                .resize(650)
                .toBuffer()
                .then(data => {

                    const buffer = data;
                    return buffer;
                })
                .catch(err => {

                });
                return bufferArchivo
        } catch (error) {
            console.error(error)
            throw error
        }
    } */

  async obtenerMiniaturaUrlYoutube(enlace): Promise<any> {
    let results, video;
    if (enlace === null) {
      return '';
    }
    results = enlace.match('(/|%3D|v=)([0-9A-z-_]{11})([%#?&]|$)');
    video = results === null ? enlace : results[2];
    const miniatura =
      'http://img.youtube.com/vi/' + video + '/maxresdefault.jpg';
    return miniatura;
  }

  obtenerMiniaturaUrlTed(enlace): Observable<any> {
    try {
      return this.httpService.get(
        `http://www.ted.com/talks/oembed.json?url=${enlace}`,
      );
    } catch (error) {
      throw error;
    }
  }
}
