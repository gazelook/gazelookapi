import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class ActualizarArchivoService {
  constructor(
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async activarArchivo(
    idArchivo: string,
    idUsuario: string,
    opts,
  ): Promise<Archivo> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion['codigo'];

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.archivo,
    );
    const codEntidad = entidad['codigo'];

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado['codigo'];

    try {
      let dataArchivo = {
        estado: codEstado,
      };

      const archivo = await this.archivoModel.findByIdAndUpdate(
        idArchivo,
        dataArchivo,
        opts,
      );

      if (!archivo) {
        return null;
      }

      const datosArchivo = JSON.parse(JSON.stringify(archivo));
      //eliminar parametro no necesarios
      delete datosArchivo.fechaCreacion;
      delete datosArchivo.fechaActualizacion;
      delete datosArchivo.__v;

      const dataHistoricoMedia: any = {
        datos: datosArchivo,
        usuario: idUsuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);

      return archivo;
    } catch (error) {
      error.message;
      throw error;
    }
  }
}
