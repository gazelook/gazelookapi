import { HttpStatus, Injectable } from '@nestjs/common';
import { appendFile, readFile, stat, unlink } from 'fs';
import * as imageSize from 'image-size';
import { erroresMedia } from '../../../shared/enum-errores';
import {
  codigoCatalogoTipoMedia,
  codigosArchivosSistema,
  dimensionesImagen,
  limiteArchivo,
} from '../../../shared/enum-sistema';

const ffmpeg = require('fluent-ffmpeg');

@Injectable()
export class GestionArchivoService {
  VALIDATE_IMAGE = /\.(jpe?g|png)$/i;
  VALIDATE_VIDEO = /\.(mp4|mkv|mov|avi)$/i;
  VALIDATE_AUDIO = /\.(acc|mp3|mp4|ogg|webm|wav)$/i;
  VALIDATE_DOC = /\.(pdf|docx|doc|xls|xlsx|pptx|pptm|ppt|xml|json|zip|txt|csv)$/i;
  ONE_MEGA_BYTE = 1048576; // 1MB en bytes

  COD_ARCHIVO_DEFAULT_ALBUM_GENERAL =
    codigosArchivosSistema.archivo_default_album_general; // codigo del archivo del album general
  COD_ARCHIVO_DEFAULT_ALBUM_PERFIL =
    codigosArchivosSistema.archivo_default_album_perfil; // codigo del archivo del album perfil
  COD_ARCHIVO_DEFAULT_PROYECTOS =
    codigosArchivosSistema.archivo_default_proyectos;
  COD_ARCHIVO_DEFAULT_NOTICIAS =
    codigosArchivosSistema.archivo_default_noticias;
  COD_ARCHIVO_DEFAULT_CONTACTOS =
    codigosArchivosSistema.archivo_default_contactos;
  COD_ARCHIVO_DEFAULT_FOTO_PERFIL =
    codigosArchivosSistema.archivo_default_foto_perfil;
  COD_ARCHIVO_DEFAULT_RECURSOS_DONE =
    codigosArchivosSistema.archivo_default_recursos_done;
  COD_ARCHIVO_DEFAULT_DEMO_LIST_CONTACTOS =
    codigosArchivosSistema.archivo_default_demo_lista_contactos;
  COD_ARCHIVO_DEFAULT_PORTADA_ANUNCIOS =
    codigosArchivosSistema.archivo_default_portada_anuncios;
  COD_ARCHIVO_DEFAULT_TERCIOS = codigosArchivosSistema.archivo_default_tercios;

  //VALIDATE_ENLACE_YOUTUBE = /^(?:https?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/watch\?v=([^&]+)/m;
  VALIDATE_ENLACE_YOUTUBE = /(?:https?:\/\/|www\.|m\.|^)youtu(?:be\.com\/watch\?(?:.*?&(?:amp;)?)?v=|\.be\/)([\w‌​\-]+)(?:&(?:amp;)?[\w\?=]*)?/;
  VALIDATE_ENLACE_TED = /^(?:https?:\/\/)?(?:www\.)?(?:ted\.com)\/talks\/+/m;

  constructor() {}

  validarArchivo(archivo: Express.Multer.File) {
    try {
      let result = { tipo: '', validate: false };
      const mimetype = archivo.mimetype;
      const tipoArchivoMetadata = mimetype.split('/');
      // validar tamaño
      if (!this.sizeLimitFile(tipoArchivoMetadata[0], archivo)) {
        console.error('El archivo supera el limite');
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresMedia.LIMITE_ARCHIVO,
        };
      }
      // validar tipos
      if (tipoArchivoMetadata[0] === 'image') {
        result.validate = this.VALIDATE_IMAGE.test(archivo.originalname);
        result.tipo = codigoCatalogoTipoMedia.codImage;
      }
      if (tipoArchivoMetadata[0] === 'audio') {
        result.validate = this.VALIDATE_AUDIO.test(archivo.originalname);
        result.tipo = codigoCatalogoTipoMedia.codAudio;
      }
      if (tipoArchivoMetadata[0] === 'video') {
        result.validate = this.VALIDATE_VIDEO.test(archivo.originalname);
        result.tipo = codigoCatalogoTipoMedia.codVideo;
      }
      // text => archivos de texto plano (txt, csv)
      if (
        tipoArchivoMetadata[0] === 'application' ||
        tipoArchivoMetadata[0] === 'text'
      ) {
        result.validate = this.VALIDATE_DOC.test(archivo.originalname);
        result.tipo = codigoCatalogoTipoMedia.codFiles;
      }
      // false, si no admite el tipo de archivo
      if (!result.validate) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresMedia.ARCHIVO_NO_VALIDO,
        };
      }
      return result;
    } catch (error) {
      throw error;
    }
  }

  sizeLimitFile(tipoArchivo, archivo) {
    switch (tipoArchivo) {
      case 'image':
        if (archivo.size > limiteArchivo.general) {
          return false;
        }
        return true;
      case 'audio':
        if (archivo.size > limiteArchivo.audio) {
          return false;
        }
        return true;
      case 'video':
        if (archivo.size > limiteArchivo.general) {
          return false;
        }
        return true;
      case 'application' || 'text':
        if (archivo.size > limiteArchivo.general) {
          return false;
        }
        return true;
    }
  }

  validarEnlaceYoutube(enlace) {
    return this.VALIDATE_ENLACE_YOUTUBE.test(enlace) ? true : false;
  }

  validarEnlaceTed(enlace) {
    return this.VALIDATE_ENLACE_TED.test(enlace) ? true : false;
  }

  validarArchivosDefault(archivoDefault, catalogoArchivosDefault) {
    let codigos = [
      this.COD_ARCHIVO_DEFAULT_ALBUM_GENERAL,
      this.COD_ARCHIVO_DEFAULT_ALBUM_PERFIL,
      this.COD_ARCHIVO_DEFAULT_PROYECTOS,
      this.COD_ARCHIVO_DEFAULT_NOTICIAS,
      this.COD_ARCHIVO_DEFAULT_CONTACTOS,
      this.COD_ARCHIVO_DEFAULT_FOTO_PERFIL,
      this.COD_ARCHIVO_DEFAULT_RECURSOS_DONE,
      this.COD_ARCHIVO_DEFAULT_DEMO_LIST_CONTACTOS,
      this.COD_ARCHIVO_DEFAULT_PORTADA_ANUNCIOS,
      this.COD_ARCHIVO_DEFAULT_TERCIOS,
      'true',
    ];
    if (
      codigos.indexOf(catalogoArchivosDefault) !== -1 &&
      codigos.indexOf(archivoDefault) !== -1
    ) {
      return true;
    } else {
      return false;
    }
  }

  async redimensionarImagen(path, porcentaje, newPath) {
    return new Promise(function(resolve, reject) {
      setTimeout(async function() {
        /* const result = ffmpeg(path).size(porcentaje).autopad().save(path);
                resolve(result); */

        ffmpeg(path)
          .size(porcentaje)
          .autopad()
          .on('error', function(err) {
            unlink(path, async err => {
              if (err) reject(err);
            });
            unlink(newPath, async err => {
              if (err) reject(err);
            });
            reject(err);
          })
          .on('end', function() {
            resolve(true);
          })
          .save(newPath);
      }, 500);
    });
  }

  async redimensionarImagenByDimensiones(path, dimensiones, newPath) {
    return new Promise(function(resolve, reject) {
      setTimeout(async function() {
        /* const result = ffmpeg(path).size(porcentaje).autopad().save(path);
                resolve(result); */

        ffmpeg(path)
          .size(dimensiones)
          .autopad()
          .on('error', function(err) {
            unlink(path, async err => {
              if (err) reject(err);
            });
            unlink(newPath, async err => {
              if (err) reject(err);
            });
            reject(err);
          })
          .on('end', function() {
            resolve(true);
          })
          .save(newPath);
      }, 500);
    });
  }

  async redimensionarVideo(path, porcentaje, newPath) {
    //const stream = createWriteStream(newPath);
    return new Promise(function(resolve, reject) {
      setTimeout(async function() {
        //const result = await ffmpeg(path).withSize(porcentaje).autopad().saveToFile(newPath);
        ffmpeg(path)
          .videoCodec('libx264')
          .audioCodec('libmp3lame')
          .size(porcentaje)
          .autopad() // reduce el tamaño en base a la calidad del video
          .on('error', function(err) {
            unlink(path, async err => {
              if (err) reject(err);
            });
            unlink(newPath, async err => {
              if (err) reject(err);
            });
            reject(err);
          })
          .on('end', function() {
            resolve(true);
          })
          .save(newPath);
      }, 500);
    });
  }

  async redimensionarAudio(path, bitrate, newPath) {
    //const stream = createWriteStream(newPath);
    return new Promise(function(resolve, reject) {
      setTimeout(async function() {
        //const result = await ffmpeg(path).withSize(porcentaje).autopad().saveToFile(newPath);
        ffmpeg(path)
          .audioCodec('libmp3lame')
          .audioBitrate(bitrate) //ejemplo 128k
          .on('error', async function(err) {
            unlink(path, async err => {
              if (err) reject(err);
            });
            unlink(newPath, async err => {
              if (err) reject(err);
            });
            reject(err);
          })
          .on('end', function() {
            resolve(true);
          })
          .save(newPath);
      }, 500);
    });
  }

  // retorna el nombre de la imagen
  async obtenerMiniaturaVideo(pathVideo, pathFolderDestinoImagen) {
    const filename = `${Date.now()}.png`;
    return new Promise(function(resolve, reject) {
      setTimeout(async function() {
        /* ffmpeg(pathVideo)
                    .takeScreenshots({
                        count: 1,
                        timemarks: ['6'], // numbero de segundos,
                        size: '650x650' //tamaño pixeles || porcentaje "38.53%"

                    }, pathFolderDestinoImagen, function (err) {

                    }); */
        ffmpeg(pathVideo)
          .takeScreenshots(
            {
              count: 1, //numero de capturas
              filename: filename,
              timemarks: ['0.5'],
              size: dimensionesImagen.miniatura,
            },
            pathFolderDestinoImagen,
          )
          .on('error', function(err) {
            unlink(pathVideo, async err => {
              if (err) reject(err);
            });
            reject(err);
          })
          .on('end', function() {
            resolve(filename);
          });
      }, 500);
    });
  }

  //obtiene las propiedades del archivo(tamaño...)
  async getPropertyFile(path) {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        stat(path, async (err, data) => {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      }, 80);
    });
  }

  async getBufferFile(path) {
    return new Promise(async function(resolve, reject) {
      setTimeout(function() {
        readFile(path, async (err, data) => {
          if (err) {
            reject(err);
          } else {
            resolve(data);
          }
        });
      }, 80);
    });
  }
  async eliminarArchivo(path) {
    return new Promise(async function(resolve, reject) {
      setTimeout(function() {
        unlink(path, async err => {
          if (err) reject(err);
          resolve(true);
        });
      }, 50);
    });
  }

  async crearArchivo(path, data) {
    return new Promise(async function(resolve, reject) {
      setTimeout(function() {
        appendFile(path, data, error => {
          if (error) {
            reject(error);
          }
        });
        resolve(true);
      }, 50);
    });
  }
  // si el archivo tiene extension pero no un nombre, el metodo retornara vacio.
  // el archivo debe tener un nombre
  obtenerExtensionArchivo(filename) {
    return filename.slice(((filename.lastIndexOf('.') - 1) >>> 0) + 2);
  }

  nuevasDimensionesImagen(width: number, height: number, porcentaje: number) {
    const newWidth = Math.round((width * porcentaje) / 100);
    const newHeight = Math.round((height * porcentaje) / 100);
    return { width: newWidth, height: newHeight };
  }

  obtenerPorcentajeBySizeFile(size: number) {
    let porcentaje = 0;

    // si esta entre 0 a 1 MB
    if (size > 0 && size <= this.ONE_MEGA_BYTE) porcentaje = 95;
    // si esta entre 1 a 2 MB
    if (size > this.ONE_MEGA_BYTE && size <= this.ONE_MEGA_BYTE * 2)
      porcentaje = 85;
    // si esta entre 2 a 3 MB
    if (size > this.ONE_MEGA_BYTE * 2 && size <= this.ONE_MEGA_BYTE * 3)
      porcentaje = 75;
    // si esta entre 3 a 4 MB
    if (size > this.ONE_MEGA_BYTE * 3 && size <= this.ONE_MEGA_BYTE * 4)
      porcentaje = 65;
    // si esta entre 4 a 5 MB
    if (size > this.ONE_MEGA_BYTE * 4 && size <= this.ONE_MEGA_BYTE * 5)
      porcentaje = 55;
    // si es mayor a 5 MB
    if (size > this.ONE_MEGA_BYTE * 5) porcentaje = 45;

    return porcentaje;
  }

  async getDimensionesImage(path) {
    return new Promise(async function(resolve, reject) {
      setTimeout(function() {
        imageSize.imageSize(path, function(err, dimensiones) {
          resolve({ width: dimensiones.width, height: dimensiones.height });
        });
      }, 50);
    });
  }
}
