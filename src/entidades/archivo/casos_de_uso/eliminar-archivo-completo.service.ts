import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { StorageService } from '../../../drivers/amazon_s3/services/storage.service';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';

@Injectable()
export class EliminarArchivoCompletoService {
  constructor(
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    private storageService: StorageService,
  ) {}

  async eliminarArchivoCompleto(idArchivo: string, opts?: any): Promise<any> {
    try {
      const getArchivo = await this.archivoModel.findOne({ _id: idArchivo });
      if (!getArchivo) {
        throw { message: 'No existe el archivo' };
      }
      // eliminar archivo de amazon s3
      await this.storageService.eliminarArchivo(getArchivo.filename);

      const archivo = await this.archivoModel.deleteOne(
        { _id: idArchivo },
        opts,
      );

      return true;
    } catch (error) {
      error.message;
      throw error;
    }
  }
}
