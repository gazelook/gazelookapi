import { HttpModule, Module } from '@nestjs/common';
import { DBModule } from '../../../drivers/db_conection/db.module';
import { StorageModule } from '../../../drivers/amazon_s3/storage.module';
import { archivoProviders } from '../drivers/archivo.provider';
import { CrearArchivoService } from './crear-archivo.service';
import { ListaArchivosDefaultService } from './lista-archivos-default.service';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { CrearMiniaturaService } from './crear-miniatura.service';
import { ListaArchivosDefaultTipoService } from './lista-archivos-default-tipo.service';
import { GestionArchivoService } from './gestion-archivo.service';
import { EliminarArchivoService } from './eliminar-archivo.service';
import { ActualizarArchivoService } from './actualizar-archivo.service';
import { EliminarArchivoCompletoService } from './eliminar-archivo-completo.service';
@Module({
  imports: [DBModule, StorageModule, CatalogosServiceModule, HttpModule],
  providers: [
    ...archivoProviders,
    CrearArchivoService,
    ListaArchivosDefaultService,
    CrearMiniaturaService,
    ListaArchivosDefaultTipoService,
    GestionArchivoService,
    EliminarArchivoService,
    ActualizarArchivoService,
    EliminarArchivoCompletoService,
  ],
  exports: [
    CrearArchivoService,
    ListaArchivosDefaultService,
    CrearMiniaturaService,
    ListaArchivosDefaultTipoService,
    GestionArchivoService,
    EliminarArchivoService,
    ActualizarArchivoService,
    EliminarArchivoCompletoService,
  ],
  controllers: [],
})
export class ArchivoServicesModule {}
