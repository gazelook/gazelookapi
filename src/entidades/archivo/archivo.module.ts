import { Module } from '@nestjs/common';
import { ArchivoControllerModule } from './controladores/archivo-controller.module';

@Module({
  imports: [ArchivoControllerModule],
})
export class ArchivoModule {}
