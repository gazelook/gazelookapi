import { Connection } from 'mongoose';
import { archivoModelo } from '../../../drivers/mongoose/modelos/archivo/archivo.schema';
import { CatalogoTipoMediaModelo } from '../../../drivers/mongoose/modelos/catalogoTipoMedia/catalogoTipoMediaModelo';
import { CatalogoArchivoDefaultModelo } from 'src/drivers/mongoose/modelos/catalogo_archivo_default/catalogo-archivo-default.schema';
import { MediaModelo } from 'src/drivers/mongoose/modelos/media/mediaModelo';

export const archivoProviders = [
  {
    provide: 'ARCHIVO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('archivo', archivoModelo, 'archivo'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TIPO_MEDIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_tipo_media',
        CatalogoTipoMediaModelo,
        'catalogo_tipo_media',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_ARCHIVO_DEFAULT_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_archivo_default',
        CatalogoArchivoDefaultModelo,
        'catalogo_archivo_default',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'MEDIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('media', MediaModelo, 'media'),
    inject: ['DB_CONNECTION'],
  },
];
