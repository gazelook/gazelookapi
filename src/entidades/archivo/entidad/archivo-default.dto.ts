import { ApiProperty } from '@nestjs/swagger';

export class ArchivoDefaultDto {
  @ApiProperty({
    description: 'Identificador autogenerado del archivo',
    example: '5f3c329bf6ee0139d8133723',
  })
  _id: string;
  @ApiProperty({
    description: 'Ruta del archvio',
    example:
      'https://gazelook-storage.s3.amazonaws.com/general/1597780634337.jpg',
  })
  url: string;
  @ApiProperty({
    description: 'Codigos de archivos del catalogo archivos default',
    example: 'CAT_ARC_DEFAULT_01',
  })
  catalogoArchivoDefault: string;
}
