export class crearArchivoDto {
  archivo?: Express.Multer.File;
  tipo?: string;
  fileDefault?: string;
  relacionAspecto?: string;
  duracion?: string;
  catalogoArchivoDefault?: string;
}
