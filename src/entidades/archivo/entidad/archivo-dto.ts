import { ApiProperty } from '@nestjs/swagger';

export class ArchivoDto {
  @ApiProperty()
  relacionAspecto: string;
  @ApiProperty({ type: 'file' })
  archivo: any;
  @ApiProperty({
    description:
      'Este campo controla si la imagen es del usuario(false por defecto) o del sistema(true)',
    required: true,
  })
  fileDefault?: Boolean;
}

export class ArchivoUrlDto {
  @ApiProperty({
    description: 'Ruta del archvio',
    example:
      'https://gazelook-storage.s3.amazonaws.com/general/1597780634337.jpg',
  })
  url: string;
}
