import { ApiProperty } from '@nestjs/swagger';

export class RetornoArchivoDto {
  @ApiProperty()
  idArchivo: string;
  @ApiProperty()
  url: string;
  @ApiProperty()
  filename: string;
  @ApiProperty()
  fileDefault?: Boolean;
}
