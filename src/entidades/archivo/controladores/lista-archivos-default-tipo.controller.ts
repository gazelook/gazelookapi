import { Controller, Get, Headers, HttpStatus, Param } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ListaArchivosDefaultTipoService } from '../casos_de_uso/lista-archivos-default-tipo.service';
import { ArchivoDefaultDto } from '../entidad/archivo-default.dto';

@ApiTags('Archivo')
@Controller('api/lista-archivos-default')
export class ListaArchivosDefaultTipoControlador {
  constructor(
    private readonly listaArchivosDefaultTipoService: ListaArchivosDefaultTipoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
  ) {}
  @Get('/:codigoDefault')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Retorna la lista de imagenes segun su tipo Ejemplo album_general: CAT_ARC_DEFAULT_01, album_perfil: CAT_ARC_DEFAULT_02, proyectos: CAT_ARC_DEFAULT_03, noticias:CAT_ARC_DEFAULT_04, contactos:CAT_ARC_DEFAULT_05) que son por defecto del sistema.',
  })
  @ApiResponse({
    status: 200,
    type: ArchivoDefaultDto,
    description: 'Lista de Archivos del sistema',
  })
  @ApiResponse({
    status: 406,
    description: 'Error al obtener la lista de archivos',
  })
  public async listaArchivosDefaultTipo(
    @Param('codigoDefault') tipo: string,
    @Headers() headers,
  ) {
    const respuesta = new RespuestaInterface();
    try {
      const listaArchivos = await this.listaArchivosDefaultTipoService.listaArchivosDefaultTipo(
        tipo,
      );
      if (!listaArchivos || listaArchivos.length === 0) {
        //llama al metodo de traduccion dinamica
        const FALLO_DEVOLUCION_ARCHIVOS = await this.traduccionEstaticaController.traduccionEstatica(
          headers.idioma,
          'FALLO_DEVOLUCION_ARCHIVOS',
        );

        respuesta.codigoEstado = HttpStatus.NOT_ACCEPTABLE;
        respuesta.respuesta = {
          mensaje: FALLO_DEVOLUCION_ARCHIVOS,
        };
        return respuesta;
      }

      respuesta.codigoEstado = HttpStatus.OK;
      respuesta.respuesta = {
        datos: listaArchivos,
      };
      return respuesta;
    } catch (e) {
      respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
      respuesta.respuesta = {
        mensaje: e.message,
      };
      return respuesta;
    }
  }
}
