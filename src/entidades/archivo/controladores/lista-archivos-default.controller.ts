import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ListaArchivosDefaultService } from '../casos_de_uso/lista-archivos-default.service';
import { ArchivoDefaultDto } from '../entidad/archivo-default.dto';

@ApiTags('Archivo')
@Controller('api/lista-archivos-default')
export class ListaArchivosDefaultControlador {
  constructor(
    private readonly listaArchivosDefaultService: ListaArchivosDefaultService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
  ) {}
  @Get('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Retorna la lista de imagenes que son por defecto del sistema. (codigo, url)',
  })
  @ApiResponse({
    status: 200,
    type: ArchivoDefaultDto,
    description: 'Lista de Archivos del sistema',
  })
  @ApiResponse({
    status: 406,
    description: 'Error al obtener la lista de archivos',
  })
  public async listaArchivosDefault(@Headers() headers) {
    const respuesta = new RespuestaInterface();
    try {
      const listaArchivos = await this.listaArchivosDefaultService.listaArchivosDefault();
      if (!listaArchivos && listaArchivos.length === 0) {
        //llama al metodo de traduccion dinamica
        const FALLO_DEVOLUCION_ARCHIVOS = await this.traduccionEstaticaController.traduccionEstatica(
          headers.idioma,
          'FALLO_DEVOLUCION_ARCHIVOS',
        );

        respuesta.codigoEstado = HttpStatus.NOT_ACCEPTABLE;
        respuesta.respuesta = {
          mensaje: FALLO_DEVOLUCION_ARCHIVOS,
        };
        return respuesta;
      }

      respuesta.codigoEstado = HttpStatus.OK;
      respuesta.respuesta = {
        datos: listaArchivos,
      };
      return respuesta;
    } catch (e) {
      respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
      respuesta.respuesta = {
        mensaje: e.message,
      };
      return respuesta;
    }
  }
}
