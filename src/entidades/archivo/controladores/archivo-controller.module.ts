import { Module } from '@nestjs/common';
import { ArchivoServicesModule } from '../casos_de_uso/archivo-services.module';
import { ListaArchivosDefaultControlador } from './lista-archivos-default.controller';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { ListaArchivosDefaultTipoControlador } from './lista-archivos-default-tipo.controller';

@Module({
  imports: [ArchivoServicesModule],
  providers: [TraduccionEstaticaController],
  exports: [],
  controllers: [
    ListaArchivosDefaultControlador,
    ListaArchivosDefaultTipoControlador,
  ],
})
export class ArchivoControllerModule {}
