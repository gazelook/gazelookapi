import { ApiProperty } from '@nestjs/swagger';

export class CatalogoConfiguracionDto {
  @ApiProperty()
  codigo: string;
}
