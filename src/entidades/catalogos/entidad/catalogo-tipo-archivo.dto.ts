import { ApiProperty } from '@nestjs/swagger';

export class CatalogoTipoArchivoDto {
  @ApiProperty()
  codigo: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  descripcion: string;
}
