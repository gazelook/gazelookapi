import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CatalogoOrigenDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  codigo: string;
}
