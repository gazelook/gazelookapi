import { ApiProperty } from '@nestjs/swagger';

export class CatalogoAccionNotificacionDto {
  @ApiProperty()
  codigo: string;
}
