import { ApiProperty } from '@nestjs/swagger';

export class CatalogoMetodoPagoDto {
  @ApiProperty({
    description: 'Código catálogo generado automaticamente',
    required: false,
    example: 'METPAG_1 | METPAG_2',
  })
  codigo: string;

  @ApiProperty({
    description: 'Lista Traduciones disponibles',
    required: true,
    example: '[METPAG_ES1 | METPAG_EN1 | METPAG_IT1]',
  })
  traduciones: string[];
  // @ApiProperty({description: 'Nombre catálogo', required: true, example: 'tajeta de credito | debito | tranferecia | trueque'})
  // nombre: string;
  // @ApiProperty({description: 'Descripción del método de pago', required: false})
  // descripcion: string;
  @ApiProperty({
    description: 'Código estado catálogo',
    required: true,
    example: 'EST_13(Activa) | EST_14(eliminado)',
  })
  estado: string;
  @ApiProperty({
    description: 'Refencia a la media del método de pago',
    required: true,
  })
  icono: string;
}
