import { ApiProperty } from '@nestjs/swagger';

export class CatalogoIdiomasDto {
  @ApiProperty()
  codigo: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  codNombre: string;
  @ApiProperty()
  idiomaSistema: boolean;
}

export class CodCatalogoIdiomasDto {
  @ApiProperty()
  codigo: string;
}
