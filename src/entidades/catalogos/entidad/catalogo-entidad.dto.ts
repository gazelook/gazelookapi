import { ApiProperty } from '@nestjs/swagger';

export class CatalogoEntidadDto {
  @ApiProperty()
  codigo: string;
}
