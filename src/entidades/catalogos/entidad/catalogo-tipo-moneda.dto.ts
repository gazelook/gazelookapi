import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CatalogoTipoMonedaDto {
  @ApiProperty()
  codigo: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  codNombre: string;
  @ApiProperty()
  predeterminado: boolean;
}

export class CatalogoTipoMonedaCodigoDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  codNombre: string;
}

export class CodCatalogoTipoMonedaDto {
  @ApiProperty({
    required: true,
    description: 'Codigo nombre del catalogo tipo moneda',
    example: 'USD',
  })
  codNombre: string;
}
