import { ApiProperty } from '@nestjs/swagger';

export class CatalogoEventoNotificacionDto {
  @ApiProperty()
  codigo: string;
}
