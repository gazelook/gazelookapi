import { ApiProperty } from '@nestjs/swagger';

export class CatalogoArchivoDefaultDto {
  @ApiProperty({
    description: 'Identificador autogenerado del catalogo archivo default',
    required: true,
    example: '5f3d4b226440b5baa60b876b',
  })
  _id: string;
  @ApiProperty({
    description: 'Codigo del catalogo archivo default',
    required: true,
    example: 'CAT_ARC_DEFAULT_01',
  })
  codigo: string;
  @ApiProperty({
    description: 'Nombre del catalogo archivo default',
    required: true,
    example: 'album_general',
  })
  nombre: string;
  // @ApiProperty({description: 'Codigo del Estado del catalogo archivo default', required: true, example: "CAT_ARC_DEFAULT_01"})
  // estado: string;
}
