import { ApiProperty } from '@nestjs/swagger';

export class CatalogoEstilosDto {
  @ApiProperty()
  codigo: string;
}
