import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoRepartirFondos } from 'src/drivers/mongoose/interfaces/catalogo_repartir_fondos/catalogo-repartir-fondos.interface';
import { codigosEstadosCatalogoRepartirFondos } from 'src/shared/enum-sistema';

@Injectable()
export class CatalogoRepartirFondosService {
  constructor(
    @Inject('CATALOGO_REPARTIR_FONDOS_MODEL')
    private readonly catalogoRepartirFondosModel: Model<CatalogoRepartirFondos>,
  ) {}

  async obtenerCatalogoPorcentajeEsperaFondos(): Promise<any> {
    try {
      const catalogoRepartirFondos = await this.catalogoRepartirFondosModel.findOne(
        { estado: codigosEstadosCatalogoRepartirFondos.activa },
      );

      return catalogoRepartirFondos;
    } catch (error) {
      throw error;
    }
  }
}
