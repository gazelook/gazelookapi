import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { nombreEntidades } from 'src/shared/enum-sistema';
import { CatalogoMetodoPago } from '../../../drivers/mongoose/interfaces/catalogo_metodo_pago/catalogo-metodo-pago.interface';
import { CatalogoEntidadService } from './catalogo-entidad.service';
import { CatalogoEstadoService } from './catalogo-estado.service';

@Injectable()
export class ObtenerCatalogoMetodoPagoService {
  constructor(
    @Inject('CATALOGO_METODO_PAGO')
    private readonly catalogoMetodoPagoModel: Model<CatalogoMetodoPago>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async obtenerListaMetodoPago(): Promise<any> {
    //Obtiene la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.metodoPago,
    );
    try {
      let catalogoMetodoPago = await this.catalogoMetodoPagoModel
        .find({})
        .select('-fechaActualizacion -fechaCreacion')
        .populate({
          path: 'traducciones',
          select: 'nombre descripcion idioma ',
        })
        .populate({
          path: 'icono',
          select: 'principal -_id',
          populate: { path: 'principal', select: 'url -_id' },
        });

      let result: any = [];
      for (let index = 0; index < catalogoMetodoPago.length; index++) {
        result.push(catalogoMetodoPago[index]);
        console.log('CODIGO: ' + catalogoMetodoPago[index].estado);
        let catMetodoPago = await this.catalogoEstadoService.obtenerEstadoByCodigo(
          catalogoMetodoPago[index].estado,
        );
        result[index].estado = catMetodoPago;
      }
      return result;
    } catch (error) {
      throw error;
    }
  }
}
