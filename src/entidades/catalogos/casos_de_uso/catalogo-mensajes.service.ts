import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoMensaje } from 'src/drivers/mongoose/interfaces/catalogo_mensaje/catalogo-mensaje.interface';

@Injectable()
export class CatalogoMensajesService {
  constructor(
    @Inject('TIPO_MENSAJE_MODEL')
    private readonly catalogMsjModel: Model<CatalogoMensaje>,
  ) {}

  async obtenerCodigoCatalogoMensj(nombre: string): Promise<any> {
    try {
      const nombreEnt = await this.catalogMsjModel.findOne({ nombre: nombre });
      return nombreEnt;
    } catch (error) {
      throw error;
    }
  }
}
