import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoTipoArchivo } from 'src/drivers/mongoose/interfaces/catalogo_tipo_archivo/catalogo-tipo-archivo.interface';

@Injectable()
export class CatalogoTipoArchivoService {
  constructor(
    @Inject('CATALOGO_TIPO_ARCHIVO')
    private readonly catalogoTipoArchivo: Model<CatalogoTipoArchivo>,
  ) {}

  async obtenerCatalogoTipoArchivo(): Promise<any> {
    try {
      const catalogoTipoArchivo = await this.catalogoTipoArchivo.find(
        {},
        { codigo: 1, nombre: 1, descripcion: 1 },
      );

      return catalogoTipoArchivo;
    } catch (error) {
      throw error;
    }
  }
}
