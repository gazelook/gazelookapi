import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoColores } from 'src/drivers/mongoose/interfaces/catalogo_colores/catalogo-colores.interface';
import { CatalogoTipoColores } from 'src/drivers/mongoose/interfaces/catalogo_tipo_colores/catalogo-tipo-colores.interface';

@Injectable()
export class CatalogoColoresService {
  constructor(
    @Inject('CATALOGO_COLORES_MODEL')
    private readonly catalogoColoresModel: Model<CatalogoColores>,
    @Inject('CATALOGO_TIPO_COLORES_MODEL')
    private readonly catalogoTipoColorModel: Model<CatalogoTipoColores>,
  ) {}

  async obtenerCatalogoColoresByCodigo(codigo: string): Promise<any> {
    try {
      //Obtiene los tipos de colores por el codigo
      const catalogoColores = await this.catalogoColoresModel.findOne(
        { codigo: codigo },
        { _id: 1, codigo: 1, tipo: 1 },
      );

      //Obtiene el catalogo Tipo colores por el codigo
      const catalogoTipoColores = await this.catalogoTipoColorModel.findOne(
        { codigo: catalogoColores.tipo },
        { codigo: 1, nombre: 1, formula: 1 },
      );

      //Objeto de tipo catalogo tipo color
      let objCatTipoColor = {
        _id: catalogoTipoColores._id.toString(),
        codigo: catalogoTipoColores.codigo,
        nombre: catalogoTipoColores.nombre,
        formula: catalogoTipoColores.formula,
      };
      //Objeto de tipo catalogo colores
      let objColor = {
        _id: catalogoColores._id.toString(),
        codigo: catalogoColores.codigo,
        tipo: objCatTipoColor,
      };
      return objColor;
    } catch (error) {
      throw error;
    }
  }
}
