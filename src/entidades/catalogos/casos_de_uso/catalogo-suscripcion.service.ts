import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

@Injectable()
export class CatalogoSuscripcionService {
  constructor(
    @Inject('CATALOGO_SUSCRIPCION')
    private readonly catalogoSuscripcionModel: Model<any>,
  ) {}

  async obtenerTipoSuscripcionByNombre(nombre: string): Promise<any> {
    try {
      return await this.catalogoSuscripcionModel.findOne({ nombre: nombre });
    } catch (error) {
      throw error;
    }
  }

  async obtenerTipoSuscripcionByCodigo(codigo: string): Promise<any> {
    try {
      return await this.catalogoSuscripcionModel.findOne({ codigo: codigo });
    } catch (error) {
      throw error;
    }
  }
}
