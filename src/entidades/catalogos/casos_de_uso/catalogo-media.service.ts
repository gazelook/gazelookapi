import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoMedia } from '../../../drivers/mongoose/interfaces/catalogo_media/catalogo-media.interface';

@Injectable()
export class CatalogoMediaService {
  constructor(
    @Inject('CATALOGO_MEDIA')
    private readonly catalogoMedia: Model<CatalogoMedia>,
  ) {}

  async obtenerCatalogoMedia(): Promise<any> {
    try {
      const catalogo = await this.catalogoMedia.find(
        {},
        { codigo: 1, nombre: 1 },
      );
      return catalogo;
    } catch (error) {
      throw error;
    }
  }

  async obtenerCatalogoMediaByCodigo(codigo): Promise<any> {
    try {
      const catalogo = await this.catalogoMedia.findOne(
        { codigo: codigo },
        { codigo: 1, nombre: 1 },
      );

      return catalogo;
    } catch (error) {
      throw error;
    }
  }

  async obtenerCatalogoMediaByNombre(nombre): Promise<any> {
    try {
      const catalogo = await this.catalogoMedia.findOne(
        { nombre: nombre },
        { codigo: 1, nombre: 1 },
      );

      return catalogo;
    } catch (error) {
      throw error;
    }
  }
}
