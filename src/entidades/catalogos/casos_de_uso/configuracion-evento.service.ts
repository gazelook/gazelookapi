import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { ConfiguracionEvento } from 'src/drivers/mongoose/interfaces/configuracion_evento/configuracion-evento.interface';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoEntidadService } from './catalogo-entidad.service';
import { CatalogoEstadoService } from './catalogo-estado.service';

@Injectable()
export class ConfiguracionEventoService {
  constructor(
    @Inject('CONFIGURACION_EVENTO_MODEL')
    private readonly configuracionEvento: Model<ConfiguracionEvento>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async obtenerConfiguracionEvento(codigoConfEvent: string): Promise<any> {
    try {
      //Obtiene la entidad configuracion evento
      const entidadConfiguracionEvento = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.configuracionEvento,
      );
      let codEntidadConfiguracionEvento = entidadConfiguracionEvento.codigo;

      //Obtiene el estado activa de la configuracion evento
      const estadoConfiguracionEvento = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadConfiguracionEvento,
      );
      let codEstadoConfiguracionEvento = estadoConfiguracionEvento.codigo;

      const getConfiguracionEvento = await this.configuracionEvento.findOne(
        { codigo: codigoConfEvent, estado: codEstadoConfiguracionEvento },
        { fechaCreacion: 0, fechaActualizacion: 0, estado: 0 },
      );
      // const getPensamientos = await this.configuracionEvento.findOne({codigo: codigoConfEvent, estado:codEstadoConfiguracionEvento},{fechaCreacion:0, fechaActualizacion:0, estado:0})
      //                                                   .populate({path: 'formulas', select: 'texto',
      //                                                   match: {
      //                                                     estado: codEstadoTra,
      //                                                     idioma:codIdioma
      //                                                   }}).limit(3).sort({ fechaCreacion: -1 });
      return getConfiguracionEvento;
    } catch (error) {
      throw error;
    }
  }
}
