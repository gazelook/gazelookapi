import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoEstado } from 'src/drivers/mongoose/interfaces/estado/estado.interface';

@Injectable()
export class CatalogoEstadoService {
  constructor(
    @Inject('ESTADOS_MODEL')
    private readonly estadoModel: Model<CatalogoEstado>,
  ) {}

  async obtenerNombreEstado(
    nombreEstado: string,
    entidad: string,
  ): Promise<any> {
    const nombreEst = await this.estadoModel.findOne({
      $and: [{ nombre: nombreEstado }, { entidad: entidad }],
    });
    return nombreEst;
  }

  async obtenerEstadoByCodigo(codigo: any): Promise<any> {
    const estado = await this.estadoModel.findOne(
      { codigo: codigo },
      { nombre: 1, codigo: 1 },
    );
    return estado;
  }

  async obtenerEstadoByCodigoWithCodEntidad(
    codigo: any,
    codEntidad: any,
  ): Promise<any> {
    const estado = await this.estadoModel.findOne({
      $and: [{ codigo: codigo }, { entidad: codEntidad }],
    });
    return estado;
  }
}
