import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoPorcentajeEsperaFondos } from 'src/drivers/mongoose/interfaces/catalogo_porcentaje_espera_fondos/catalogo-porcentaje-espera-fondos.interface';
import { codigosEstadosCatalogoPorcentajeEsperaFondos } from 'src/shared/enum-sistema';

@Injectable()
export class CatalogoPorcentajeEsperaFondosService {
  constructor(
    @Inject('CATALOGO_PORCENTAJE_ESPERA_FONDOS_MODEL')
    private readonly catalogoPorcentajeEsperaFondosModel: Model<
      CatalogoPorcentajeEsperaFondos
    >,
  ) {}

  async obtenerCatalogoPorcentajeEsperaFondos(): Promise<any> {
    try {
      const catalogoPorcentajeProyecto = await this.catalogoPorcentajeEsperaFondosModel.findOne(
        { estado: codigosEstadosCatalogoPorcentajeEsperaFondos.activa },
      );

      return catalogoPorcentajeProyecto;
    } catch (error) {
      throw error;
    }
  }
}
