import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoPorcentajeProyecto } from 'src/drivers/mongoose/interfaces/catalogo_porcentaje_proyecto/catalogo-porcentaje-proyecto.interface';
import { codigosEstadosCatalogoPorcentajeProyecto } from 'src/shared/enum-sistema';

@Injectable()
export class CatalogoPorcentajeProyectoService {
  constructor(
    @Inject('CATALOGO_PORCENTAJE_PROYECTO_MODEL')
    private readonly catalogoPorcentajeProyectoModel: Model<
      CatalogoPorcentajeProyecto
    >,
  ) {}

  async obtenerCatalogoPorcentajeProyecto(): Promise<any> {
    try {
      const catalogoPorcentajeProyecto = await this.catalogoPorcentajeProyectoModel
        .find({ estado: codigosEstadosCatalogoPorcentajeProyecto.activa })
        .sort('prioridad');

      return catalogoPorcentajeProyecto;
    } catch (error) {
      throw error;
    }
  }
}
