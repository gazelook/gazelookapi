import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoPorcentajeFinanciacion } from 'src/drivers/mongoose/interfaces/catalogo_porcentaje_financiacion/catalogo-porcentaje-financiacion.interface';
import { codigosEstadosCatalogoPorcentajeFinanciacion } from 'src/shared/enum-sistema';

@Injectable()
export class CatalogoPorcentajeFinanciacionService {
  constructor(
    @Inject('CATALOGO_PORCENTAJE_FINANCIACION_MODEL')
    private readonly catalogoPorcentajeFinanciacionModel: Model<
      CatalogoPorcentajeFinanciacion
    >,
  ) {}

  async obtenerCatalogoPorcentajeFinanciacion(prioridad): Promise<any> {
    try {
      //Obtiene los tipos de colores por el codigo
      const catalogoPorcentajeFinanciacion = await this.catalogoPorcentajeFinanciacionModel.findOne(
        {
          prioridad: prioridad,
          estado: codigosEstadosCatalogoPorcentajeFinanciacion.activa,
        },
      );

      return catalogoPorcentajeFinanciacion;
    } catch (error) {
      throw error;
    }
  }
}
