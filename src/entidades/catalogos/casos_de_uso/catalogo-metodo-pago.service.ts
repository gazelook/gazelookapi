import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  estadosTraduccionMetodosPago,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoMetodoPago } from '../../../drivers/mongoose/interfaces/catalogo_metodo_pago/catalogo-metodo-pago.interface';
import { CatalogoEntidadService } from './catalogo-entidad.service';
import { CatalogoEstadoService } from './catalogo-estado.service';

@Injectable()
export class CatalogoMetodoPagoService {
  constructor(
    @Inject('CATALOGO_METODO_PAGO')
    private readonly catalogoMetodoPagoModel: Model<CatalogoMetodoPago>,
    @Inject('CATALOGO_TRADUCCION_METODO_PAGO')
    private readonly catalogoTraduccionMetodoPagoModel: Model<any>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async obtenerListaMetodoPago(codigoIdioma: string): Promise<any> {
    //Obtiene la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.metodoPago,
    );

    const estadoActivo: any = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      entidad.codigo,
    );
    // const estadoPendiente: any = await this.catalogoEstadoService.obtenerNombreEstado(
    //   nombrecatalogoEstados.desactivado,
    //   entidad.codigo,
    // );

    try {
      console.log('codigoIdioma: ', codigoIdioma);
      let catalogoMetodoPago = await this.catalogoMetodoPagoModel
        .find({ estado: { $in: [estadoActivo.codigo] } })
        .select('-fechaActualizacion -fechaCreacion')
        .populate({
          path: 'traducciones',
          select: 'nombre descripcion idioma ',
          match: {
            idioma: codigoIdioma,
            estado: estadosTraduccionMetodosPago.activa,
          },
        })
        .populate({
          path: 'icono',
          select: 'principal -_id',
          populate: { path: 'principal', select: 'url -_id' },
        })
        .sort({
          orden: 1,
        });

      let result: any = [];
      for (let index = 0; index < catalogoMetodoPago.length; index++) {
        result.push(catalogoMetodoPago[index]);
        let catMetodoPago = await this.catalogoEstadoService.obtenerEstadoByCodigo(
          catalogoMetodoPago[index].estado,
        );
        result[index].estado = catMetodoPago;
      }
      return result;
    } catch (error) {
      throw error;
    }
  }

  async existeTraduccionMetodoPago(codigoIdioma: string): Promise<any> {
    try {
      const existeTraducion = await this.catalogoTraduccionMetodoPagoModel.findOne(
        { idioma: codigoIdioma },
      );
      return existeTraducion ? true : false;
    } catch (error) {
      throw error;
    }
  }
}
