import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoTipoComentario } from 'src/drivers/mongoose/interfaces/catalogo_tipo_comentario/catalogo-tipo-comentario.interface';

@Injectable()
export class CatalogoTipoComentarioService {
  constructor(
    @Inject('CATALOGO_TIPO_COMENTARIO')
    private readonly catalogoTipoComentario: Model<CatalogoTipoComentario>,
  ) {}

  async obtenerTiposComentarios(): Promise<any> {
    try {
      const tiposComentario = await this.catalogoTipoComentario.find({});
      return tiposComentario;
    } catch (error) {
      throw error;
    }
  }

  async obtenerTipoComentario(nombre: string): Promise<any> {
    try {
      const tipoComentario = await this.catalogoTipoComentario.findOne({
        nombre: nombre,
      });
      return tipoComentario;
    } catch (error) {
      throw error;
    }
  }
}
