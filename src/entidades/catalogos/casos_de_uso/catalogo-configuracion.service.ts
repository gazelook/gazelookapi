import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoConfiguracion } from 'src/drivers/mongoose/interfaces/catalogo_configuracion/catalogo-configuracion.interface';

@Injectable()
export class CatalogoConfiguracionService {
  constructor(
    @Inject('CATALOGO_CONFIGURACION_MODEL')
    private readonly catalogoConfiguracionModel: Model<CatalogoConfiguracion>,
  ) {}

  async obtenerCatalogoConfiguracionByCodigo(codigo: string): Promise<any> {
    try {
      //Obtiene los tipos de colores por el codigo
      const catalogoConfiguracion = await this.catalogoConfiguracionModel.findOne(
        { codigo: codigo },
        { _id: 1, codigo: 1, nombre: 1 },
      );
      console.log('catalogoConfiguracion: ', catalogoConfiguracion);
      //Objeto de tipo catalogo estilos
      //Obtiene el catalogo estilos
      let objCatalConfiguracion = {
        _id: catalogoConfiguracion._id.toString(),
        codigo: catalogoConfiguracion.codigo,
        nombre: catalogoConfiguracion.nombre,
      };

      return objCatalConfiguracion;
    } catch (error) {
      throw error;
    }
  }
}
