import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoIdiomas } from 'src/drivers/mongoose/interfaces/catalogo_idiomas/catalogo-idiomas.interface';

@Injectable()
export class CatalogoIdiomasService {
  constructor(
    @Inject('CATALOGO_IDIOMAS')
    private readonly catalogoIdiomasModel: Model<CatalogoIdiomas>,
  ) {}

  async obtenerCatalogoIdiomas(): Promise<CatalogoIdiomas[]> {
    try {
      //Obtiene todos los idiomas
      const catalogoidiomas = await this.catalogoIdiomasModel.find(
        {},
        { _id: 0, codigo: 1, nombre: 1, codNombre: 1, idiomaSistema: 1 },
      );
      return catalogoidiomas;
    } catch (error) {
      throw error;
    }
  }

  async obtenerIdiomaByCodigoNombre(codNombre: string): Promise<any> {
    try {
      //Obtiene todos los idiomas
      const getIdioma = await this.catalogoIdiomasModel.findOne({
        codNombre: codNombre,
      });
      if (!getIdioma) {
        throw {
          message: `El idioma ${codNombre} no ésta registrado en los catálogos`,
        };
      } else {
        return getIdioma;
      }
    } catch (error) {
      throw error;
    }
  }
  async obtenerIdiomaByCodigo(codigo: string): Promise<any> {
    try {
      //Obtiene todos los idiomas
      const getIdioma = await this.catalogoIdiomasModel.findOne({
        codigo: codigo,
      });
      if (!getIdioma) {
        throw {
          message: `El idioma ${codigo} no ésta registrado en los catálogos`,
        };
      } else {
        return getIdioma;
      }
    } catch (error) {
      throw error;
    }
  }
}
