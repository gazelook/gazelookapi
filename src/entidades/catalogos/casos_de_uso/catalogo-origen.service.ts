import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoOrigen } from 'src/drivers/mongoose/interfaces/catalogo_origen/catalogo-origen.interface';
import { catalogoOrigen } from 'src/shared/enum-sistema';

@Injectable()
export class CatalogoOrigenService {
  constructor(
    @Inject('CATALOGO_ORIGEN_MODEL')
    private readonly catalogoOrigen: Model<CatalogoOrigen>,
  ) {}

  async obtenerCatalogoOrigenIngreso(codigoOrigen: string): Promise<any> {
    try {
      if (codigoOrigen === catalogoOrigen.valor_extra) {
        return null;
      }
      //Considera todas las transacciones de tipo ingreso
      // const catalogo = await this.catalogoOrigen.findOne({ $and: [{ codigo: codigoOrigen }, { ingreso: true }] });

      //Considera todas las transacciones de tipo ingreso tipo suscripciones
      if (codigoOrigen === catalogoOrigen.suscripciones) {
        const getCatalogo = await this.catalogoOrigen.findOne({
          $and: [{ codigo: codigoOrigen }, { ingreso: true }],
        });
        return getCatalogo;
      }

      //Considera todas las transacciones de tipo ingreso tipo monto sobrante
      if (codigoOrigen === catalogoOrigen.monto_sobrante) {
        const getCatalogo = await this.catalogoOrigen.findOne({
          $and: [{ codigo: codigoOrigen }, { ingreso: true }],
        });
        return getCatalogo;
      }

      return null;
    } catch (error) {
      throw error;
    }
  }

  async obtenerCatalogoOrigenValorExtra(codigoOrigen: string): Promise<any> {
    try {
      const catalogo = await this.catalogoOrigen.findOne({
        $and: [{ codigo: codigoOrigen }, { ingreso: true }],
      });
      return catalogo;
    } catch (error) {
      throw error;
    }
  }

  async obtenerCatalogoOrigenEgreso(codigoOrigen: string): Promise<any> {
    try {
      if (codigoOrigen === catalogoOrigen.valor_extra) {
        return null;
      }
      const catalogo = await this.catalogoOrigen.findOne({
        $and: [{ codigo: codigoOrigen }, { ingreso: false }],
      });
      return catalogo;
    } catch (error) {
      throw error;
    }
  }
}
