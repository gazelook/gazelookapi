import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoEstilos } from 'src/drivers/mongoose/interfaces/catalogo_estilos/catalogo-estilos.interface';

@Injectable()
export class CatalogoEstilosService {
  constructor(
    @Inject('CATALOGO_ESTILOS_MODEL')
    private readonly catalogoEstilosModel: Model<CatalogoEstilos>,
  ) {}

  async obtenerCatalogoEstilosByCodigo(codigo: string): Promise<any> {
    try {
      //Obtiene los tipos de colores por el codigo
      const catalogoEstilos = await this.catalogoEstilosModel.findOne(
        { codigo: codigo },
        { _id: 1, codigo: 1, nombre: 1, descripcion: 1 },
      );
      //Objeto de tipo catalogo estilos
      //Obtiene el catalogo estilos
      let objCatalEstilos = {
        _id: catalogoEstilos._id.toString(),
        codigo: catalogoEstilos.codigo,
        nombre: catalogoEstilos.nombre,
        descripcion: catalogoEstilos.descripcion,
      };

      return objCatalEstilos;
    } catch (error) {
      throw error;
    }
  }
}
