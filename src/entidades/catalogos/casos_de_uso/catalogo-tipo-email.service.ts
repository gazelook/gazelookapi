import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoTipoEmail } from './../../../drivers/mongoose/interfaces/catalogo_tipo_email/catalogo-tipo-email.interface';

@Injectable()
export class CatalogoTipoEmailService {
  constructor(
    @Inject('TIPO_EMAIL_MODEL')
    private readonly catalogoTipoEmail: Model<CatalogoTipoEmail>,
  ) {}

  async obtenerCodigoTipoEmail(nombreEmail: string): Promise<any> {
    try {
      const catalogoTipoEmail = await this.catalogoTipoEmail.findOne({
        nombre: nombreEmail,
      });
      return catalogoTipoEmail;
    } catch (error) {
      throw error;
    }
  }
}
