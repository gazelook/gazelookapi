import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { CatalogoIdiomasService } from './catalogo-idiomas.service';
import * as mongoose from 'mongoose';
import {
  CatalogoIdioma,
  CatalogoIdiomas,
} from '../../../drivers/mongoose/interfaces/catalogo_idiomas/catalogo-idiomas.interface';
import { catalogoIdiomasModelo } from '../../../drivers/mongoose/modelos/catalogoIdiomas/catalogoIdiomasModelo';

class Idioma {
  static getIdiomas(): CatalogoIdioma {
    const idiomaModel = new CatalogoIdioma();
    idiomaModel.codigo = 'IDI_1';
    idiomaModel.nombre = 'Español';
    idiomaModel.codNombre = 'es';
    idiomaModel.idiomaSistema = true;
    idiomaModel.estado = 'EST_84';

    return idiomaModel;
  }
}

class mockRepositoryIdiomas {
  constructor(private data) {}
  save = jest.fn().mockResolvedValue(this.data);
  static find = jest.fn().mockResolvedValue([CatalogoIdioma]);
  static findOne = jest.fn().mockResolvedValue(CatalogoIdioma);
  static findOneAndUpdate = jest.fn().mockResolvedValue(CatalogoIdioma);
  static deleteOne = jest.fn().mockResolvedValue(true);
}

const CATALOGO_IDIOMAS: mongoose.Model<CatalogoIdiomas> = mongoose.model(
  'catalogo_idiomas',
  catalogoIdiomasModelo,
);

const mockingIdiomasModel = () => {
  find: jest.fn();
};

describe('CatalogoIdiomasService', () => {
  let idiomaService: CatalogoIdiomasService;
  let catalogoIdiomasModel;
  beforeAll(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers: [
        CatalogoIdiomasService,
        {
          provide: CatalogoIdiomasService,
          useValue: jest.fn(),
        },
        {
          provide: getModelToken('CATALOGO_IDIOMAS'),
          useValue: mockRepositoryIdiomas,
        },
        { provide: CATALOGO_IDIOMAS, useFactory: mockingIdiomasModel },
      ],
    }).compile();
    idiomaService = moduleRef.get<CatalogoIdiomasService>(
      CatalogoIdiomasService,
    );
    catalogoIdiomasModel = moduleRef.get(getModelToken('CATALOGO_IDIOMAS'));
  });

  beforeEach(() => {
    mockRepositoryIdiomas.find.mockReset();
    mockRepositoryIdiomas.findOne.mockReset();
    mockRepositoryIdiomas.deleteOne.mockReset();
  });

  it('Servicio Idiomas', () => {
    expect(idiomaService).toBeDefined();
  });

  describe('getIdiomas', () => {
    it('obtener Catalogo Idiomas', async () => {
      catalogoIdiomasModel.find.mockResolvedValue([Idioma.getIdiomas()]);
      /* const idiomas: CatalogoIdioma[] = await idiomaService.obtenerCatalogoIdiomas();
            expect(mockRepositoryIdiomas).toHaveBeenCalled()
            expect(idiomas).toHaveLength(2);
            expect(mockRepositoryIdiomas.find).toHaveBeenCalledTimes(2); */
    });
  });
});
