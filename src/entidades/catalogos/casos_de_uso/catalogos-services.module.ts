import { CatalogoMensajesService } from './catalogo-mensajes.service';
import { CatalogoTipoComentarioService } from './catalogo-tipo-comentario.service';
import { TraduccionMetodoPagoService } from './traducion-metodo-pago.service';
import { CatalogoTipoEmailService } from './catalogo-tipo-email.service';
import { CatalogoSuscripcionService } from './catalogo-suscripcion.service';
import { CatalogoIdiomasService } from './catalogo-idiomas.service';
import { Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogoProviders } from '../drivers/catalogo.provider';
import { CatalogoTipoMediaService } from './catalogo-tipo-media.service';
import { CatalogoMetodoPagoService } from './catalogo-metodo-pago.service';
import { ObtenerCatalogoMetodoPagoService } from './obtener-catalogo-metodo-pago.service';
import { CatalogoAccionService } from './catalogo-accion.service';
import { CatalogoEntidadService } from './catalogo-entidad.service';
import { CatalogoEstadoService } from './catalogo-estado.service';
import { CatalogoTipoArchivoService } from './catalogo-tipo-archivo.service';
import { CatalogoMediaService } from './catalogo-media.service';
import { CatalogoArchivoDefaultService } from './catalogo-archivo-default.service';
import { CatalogoTipoMonedaService } from './catalogo-tipo-moneda.service';
import { FormulaEventoService } from './formula_evento.service';
import { ConfiguracionEventoService } from './configuracion-evento.service';
import { CatalogoColoresService } from './catalogo-colores.service';
import { CatalogoEstilosService } from './catalogo-estilos.service';
import { CatalogoConfiguracionService } from './catalogo-configuracion.service';
import { CatalogoOrigenService } from './catalogo-origen.service';
import { CatalogoPorcentajeProyectoService } from './catalogo-porcentaje-proyecto.service';
import { CatalogoPorcentajeFinanciacionService } from './catalogo-porcentaje-financiacion.service';
import { CatalogoPorcentajeEsperaFondosService } from './catalogo-porcentaje-espera-fondos.service';
import { CatalogoRepartirFondosService } from './catalogo-repartir-fondos.service';

@Module({
  imports: [DBModule],
  providers: [
    ...CatalogoProviders,
    CatalogoIdiomasService,
    CatalogoTipoMediaService,
    CatalogoMetodoPagoService,
    ObtenerCatalogoMetodoPagoService,
    CatalogoAccionService,
    CatalogoEntidadService,
    CatalogoEstadoService,
    CatalogoSuscripcionService,
    CatalogoTipoArchivoService,
    CatalogoTipoEmailService,
    TraduccionMetodoPagoService,
    CatalogoMediaService,
    CatalogoArchivoDefaultService,
    CatalogoTipoMonedaService,
    CatalogoTipoComentarioService,
    FormulaEventoService,
    ConfiguracionEventoService,
    CatalogoMensajesService,
    CatalogoColoresService,
    CatalogoEstilosService,
    CatalogoConfiguracionService,
    CatalogoOrigenService,
    CatalogoPorcentajeProyectoService,
    CatalogoPorcentajeFinanciacionService,
    CatalogoPorcentajeEsperaFondosService,
    CatalogoRepartirFondosService,
  ],
  exports: [
    CatalogoIdiomasService,
    CatalogoTipoMediaService,
    CatalogoMetodoPagoService,
    ObtenerCatalogoMetodoPagoService,
    CatalogoAccionService,
    CatalogoEntidadService,
    CatalogoEstadoService,
    CatalogoSuscripcionService,
    CatalogoTipoArchivoService,
    CatalogoTipoEmailService,
    TraduccionMetodoPagoService,
    CatalogoMediaService,
    CatalogoArchivoDefaultService,
    CatalogoTipoMonedaService,
    CatalogoTipoComentarioService,
    FormulaEventoService,
    ConfiguracionEventoService,
    CatalogoMensajesService,
    CatalogoColoresService,
    CatalogoEstilosService,
    CatalogoConfiguracionService,
    CatalogoOrigenService,
    CatalogoPorcentajeProyectoService,
    CatalogoPorcentajeFinanciacionService,
    CatalogoPorcentajeEsperaFondosService,
    CatalogoRepartirFondosService,
  ],
  controllers: [],
})
export class CatalogosServiceModule {}
