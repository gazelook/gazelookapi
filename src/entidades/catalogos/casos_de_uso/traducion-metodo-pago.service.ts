import { CatalogoAccionService } from './catalogo-accion.service';
import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoMetodoPago } from 'src/drivers/mongoose/interfaces/catalogo_metodo_pago/catalogo-metodo-pago.interface';
import { CatalogoEntidadService } from './catalogo-entidad.service';
import { nombreAcciones, nombreEntidades } from 'src/shared/enum-sistema';

@Injectable()
export class TraduccionMetodoPagoService {
  constructor(
    @Inject('CATALOGO_METODO_PAGO')
    private readonly catalogoMetodoPagoModel: Model<CatalogoMetodoPago>,
    @Inject('CATALOGO_TRADUCCION_METODO_PAGO')
    private readonly catalogoTraduccionMetodoPagoModel: Model<any>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async traduccirMetodoPago(idioma: any, usuario: string): Promise<any> {
    try {
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.metodoPago,
      );

      //Obtiene las traducciones en el idioma original,
      const traduccionesOriginales = await this.catalogoTraduccionMetodoPagoModel.find(
        { original: true },
      );

      // traduccionesOriginales.forEach(async (traduccionCatMetodoPago: any) => {

      for (const traduccionCatMetodoPago of traduccionesOriginales) {
        //llama al metodo de traduccion dinamica
        const traducNombre = await traducirTexto(
          idioma.codNombre,
          traduccionCatMetodoPago.nombre,
        );
        const traducDescripcion = await traducirTexto(
          idioma.codNombre,
          traduccionCatMetodoPago.descripcion,
        );

        //datos para guardar la nueva traduccion
        let objtraduccionCatMetodoPago = {
          codigo: traduccionCatMetodoPago.codigo,
          nombre: traducNombre.textoTraducido,
          descripcion: traducDescripcion.textoTraducido,
          idioma: idioma.codigo,
          original: false,
        };

        //guarda la nueva traducción
        const traduccion = await new this.catalogoTraduccionMetodoPagoModel(
          objtraduccionCatMetodoPago,
        ).save();

        //  guardar historico de traduccion
        let historicoTraduccionCatMetodoPago: any = {
          datos: traduccion,
          usuario: usuario,
          accion: accion.codigo,
          entidad: entidad.codigo,
        };
        //guarda el historico de la nueva traduccion
        this.crearHistoricoService.crearHistoricoServer(
          historicoTraduccionCatMetodoPago,
        );

        // Obtiene todos los metodos de pago para ser actualizados
        const metodosPago = await this.catalogoMetodoPagoModel.find();
        for (const metodo of metodosPago) {
          // Actualiza colección metodos de pago con las nuevas traducciones
          if (metodo.codigo === traduccion.codigo) {
            let metodoPago: any = await this.catalogoMetodoPagoModel.findOneAndUpdate(
              { _id: metodo._id },
              { $push: { traducciones: traduccion._id } },
            );
          }
        }
      }
    } catch (error) {
      throw error;
    }
  }
}
