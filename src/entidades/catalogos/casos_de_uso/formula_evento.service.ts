import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { FormulaEvento } from 'src/drivers/mongoose/interfaces/formula_evento/formula-evento.interface';
import { CatalogoEstadoService } from './catalogo-estado.service';
import { CatalogoEntidadService } from './catalogo-entidad.service';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class FormulaEventoService {
  constructor(
    @Inject('FORMULA_EVENTO_MODEL')
    private readonly formulaEvento: Model<FormulaEvento>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async obtenerFormulaEvento(codigoFormEvent: string): Promise<any> {
    try {
      //Obtiene la entidad configuracion evento
      const entidadConfiguracionEvento = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.formulaEvento,
      );
      let codEntidadConfiguracionEvento = entidadConfiguracionEvento.codigo;

      //Obtiene el estado activa de la configuracion evento
      const estadoConfiguracionEvento = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadConfiguracionEvento,
      );
      let codEstadoConfiguracionEvento = estadoConfiguracionEvento.codigo;

      const getFormulaEvento = await this.formulaEvento.findOne(
        { codigo: codigoFormEvent, estado: codEstadoConfiguracionEvento },
        { fechaCreacion: 0, fechaActualizacion: 0, estado: 0 },
      );
      const returnFormulaEvento = getFormulaEvento.formula;

      return returnFormulaEvento;
    } catch (error) {
      throw error;
    }
  }
}
