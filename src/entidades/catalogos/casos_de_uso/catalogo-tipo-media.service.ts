import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { TipoMedia } from 'src/drivers/mongoose/interfaces/catalogo_tipo_media/catalogo-tipo-media.interface';

@Injectable()
export class CatalogoTipoMediaService {
  constructor(
    @Inject('CATALOGO_TIPO_MEDIA')
    private readonly catalogoTipoMedia: Model<TipoMedia>,
  ) {}

  async obtenerCatalogoTipoMedia(): Promise<any> {
    try {
      const catalogo = await this.catalogoTipoMedia.find(
        {},
        { codigo: 1, nombre: 1 },
      );

      return catalogo;
    } catch (error) {
      throw error;
    }
  }

  async obtenerCatalogoTipoMediaByCodigo(codigo): Promise<any> {
    try {
      const catalogo = await this.catalogoTipoMedia.findOne(
        { codigo: codigo },
        { codigo: 1, nombre: 1 },
      );

      return catalogo;
    } catch (error) {
      throw error;
    }
  }

  async obtenerCatalogoTipoMediaByNombre(nombre): Promise<any> {
    try {
      const catalogo = await this.catalogoTipoMedia.findOne(
        { nombre: nombre },
        { codigo: 1, nombre: 1 },
      );

      return catalogo;
    } catch (error) {
      throw error;
    }
  }
}
