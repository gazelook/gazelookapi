import { CatalogoEntidad } from './../../../drivers/mongoose/interfaces/entidad/entidad.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

@Injectable()
export class CatalogoEntidadService {
  constructor(
    @Inject('ENTIDADES_MODEL')
    private readonly entidadModel: Model<CatalogoEntidad>,
  ) {}

  async obtenerNombreEntidad(nombreEntidad: string): Promise<any> {
    const nombreEnt = await this.entidadModel.findOne({
      nombre: nombreEntidad,
    });
    return nombreEnt;
  }

  async obtenerEntidadByCodigo(codigoEntidad: string): Promise<any> {
    const entidad = await this.entidadModel.findOne({ codigo: codigoEntidad });
    return entidad;
  }
}
