import { Module } from '@nestjs/common';
import { CatalogosControllerModule } from './controlador/catalogos.controller.module';

@Module({
  imports: [CatalogosControllerModule],
  providers: [],
  controllers: [],
})
export class CatalogosModule {}
