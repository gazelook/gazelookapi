import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CatalogoIdiomasService } from '../casos_de_uso/catalogo-idiomas.service';
import { CatalogoIdiomasDto } from '../entidad/catalogo-idiomas.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoIdiomasControlador {
  constructor(
    private readonly catalogoIdiomasService: CatalogoIdiomasService,
    private readonly i18n: I18nService,
  ) {}
  funcion = new Funcion(this.i18n);

  @Get('/catalogo-idiomas')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Devuelve todos los idiomas (codigo, nombre, codNombre, idiomaSistema)',
  })
  @ApiResponse({ status: 200, type: CatalogoIdiomasDto, description: 'OK' })
  public async obtenerCatalogoIdiomas(@Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const catalogoIdiomas = await this.catalogoIdiomasService.obtenerCatalogoIdiomas();

      if (!catalogoIdiomas && catalogoIdiomas.length === 0) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'ERROR_OBTENER',
          ),
        });
      }
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: catalogoIdiomas,
      });
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }

  // @Get('/catalogo-idiomas:idioma')
  // @ApiHeader({name:'idioma', description: 'codigo del idioma. Ejm: es, en, fr'})
  // @ApiOperation({ summary: 'Devuelve todos los idiomas (codigo, nombre, codNombre).' })
  // @ApiResponse({ status: 200, type: CatalogoIdiomasDto , description: 'OK' })
  // @ApiResponse({ status: 406, description: 'Error al obtener la lista de imagenes' })
  // public async obtenerIdiomaByCodigoNombre(@Param('idioma') codNombre: string){

  //     try {
  //         const getIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(codNombre);

  //     //    if(!getIdioma && getIdioma.length === 0){
  //     //         //utiliza traduccion estatica
  //     //         // const FALLO_DEVOLUCION_IDIOMAS =await this.i18n.translate(headers.idioma.concat('.FALLO_DEVOLUCION_IDIOMAS'), {
  //     //         //     lang: headers.idioma
  //     //         // });
  //     //         return ({
  //     //             message: FALLO_DEVOLUCION_IDIOMAS,
  //     //             statusCode: HttpStatus.NOT_ACCEPTABLE,
  //     //             timestamp: new Date().toISOString()

  //     //         })
  //     //    }
  //         return ({
  //             statusCode: HttpStatus.OK,
  //             timestamp: new Date().toISOString(),
  //             body: getIdioma
  //         })

  //     } catch (e) {
  //         return new BadRequestException(e);
  //     }
  // }
}
