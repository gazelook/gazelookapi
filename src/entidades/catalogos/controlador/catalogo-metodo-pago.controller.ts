import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { CatalogoIdiomasService } from '../casos_de_uso/catalogo-idiomas.service';
import { CatalogoMetodoPagoService } from '../casos_de_uso/catalogo-metodo-pago.service';
import { TraduccionMetodoPagoService } from './../casos_de_uso/traducion-metodo-pago.service';
import { CatalogoMetodoPagoDto } from './../entidad/catalogo-metodo-pago.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoMetodoPagoControlador {
  constructor(
    private readonly catalogoMetodoPagoService: CatalogoMetodoPagoService,
    private readonly catalogoIdiomasModel: CatalogoIdiomasService,
    private readonly traduccionMetodoPagoService: TraduccionMetodoPagoService,
    private readonly i18n: I18nService,
  ) {}
  funcion = new Funcion(this.i18n);
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/catalogo-metodo-pago')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: CatalogoMetodoPagoDto,
    description: 'Lista de catalogos metodos de pago',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener los catalogos' })
  @ApiOperation({
    summary:
      'Este metodo devuelve todos los catalogos de los metodos de pago existentes',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async obtenerCatalogoMetodoPago(@Headers() headers) {
    const respuesta = new RespuestaInterface();

    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      // obtiene el código del idioma recibido
      const idioma = await this.catalogoIdiomasModel.obtenerIdiomaByCodigoNombre(
        codIdioma,
      );
      if (idioma) {
        // Consultar si existe la traducción
        const existeTraducion = await this.catalogoMetodoPagoService.existeTraduccionMetodoPago(
          idioma.codigo,
        );

        // si no exíste, guardar tradución en el idioma recibido
        if (!existeTraducion) {
          await this.traduccionMetodoPagoService.traduccirMetodoPago(
            idioma,
            null,
          );
        } else {
        }
      }
      const catalogo = await this.catalogoMetodoPagoService.obtenerListaMetodoPago(
        idioma.codigo,
      );
      if (catalogo) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: catalogo,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'ERROR_OBTENER',
          ),
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
