import { Module } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { CatalogosServiceModule } from '../casos_de_uso/catalogos-services.module';
import { CatalogoArchivoDefaultControlador } from './catalogo-archivo-default.controller';
import { CatalogoIdiomasControlador } from './catalogo-idiomas.controller';
import { CatalogoMetodoPagoControlador } from './catalogo-metodo-pago.controller';
import { ObtenerCatalogoMetodoPagoControlador } from './obtener-catalogo-metodo-pago.controller';
import { CatalogoTipoMediaControlador } from './catalogo-tipo-media.controller';
import { CatalogoTipoMonedaControlador } from './catalogo-tipo-moneda.controller';

@Module({
  imports: [CatalogosServiceModule],
  providers: [TraduccionEstaticaController],
  exports: [],
  controllers: [
    CatalogoIdiomasControlador,
    CatalogoTipoMediaControlador,
    CatalogoMetodoPagoControlador,
    ObtenerCatalogoMetodoPagoControlador,
    CatalogoArchivoDefaultControlador,
    CatalogoTipoMonedaControlador,
    //CatalogoTipoArchivoControlador
  ],
})
export class CatalogosControllerModule {}
