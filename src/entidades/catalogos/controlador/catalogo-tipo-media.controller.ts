import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CatalogoTipoMediaService } from '../casos_de_uso/catalogo-tipo-media.service';
import { CatalogoTipoMediaDto } from '../entidad/catalogo-tipo-media.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoTipoMediaControlador {
  constructor(
    private readonly catalogoTipoMediaService: CatalogoTipoMediaService,
    private readonly i18n: I18nService,
  ) {}
  funcion = new Funcion(this.i18n);

  @Get('/catalogo-tipo-media')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: CatalogoTipoMediaDto,
    description: 'Lista de catalogos tipo media',
  })
  @ApiOperation({
    summary:
      'Este metodo devuelve todos los catalogos de tipo media, que contiene los codigos y nombre de los tipos de archivo (image/audio/video)',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener los catalogos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  public async obtenerCatalogoTipoMedia(@Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const catalogoTipoMedia = await this.catalogoTipoMediaService.obtenerCatalogoTipoMedia();
      if (catalogoTipoMedia && catalogoTipoMedia.length > 0) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: catalogoTipoMedia,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'ERROR_OBTENER',
          ),
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
