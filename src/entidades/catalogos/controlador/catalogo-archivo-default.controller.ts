import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CatalogoArchivoDefaultService } from '../casos_de_uso/catalogo-archivo-default.service';
import { CatalogoArchivoDefaultDto } from '../entidad/catalogo-archivo-default.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoArchivoDefaultControlador {
  constructor(
    private readonly catalogoArchivoDefaultService: CatalogoArchivoDefaultService,
    private readonly i18n: I18nService,
  ) {}
  funcion = new Funcion(this.i18n);

  @Get('/catalogo-archivo-default')
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiResponse({
    status: 200,
    type: CatalogoArchivoDefaultDto,
    description: 'Lista de catalogo archivo default',
  })
  @ApiOperation({
    summary:
      'Este metodo devuelve el catalogo de archivos default tales como: album_general, album_perfil, proyectos, noticias, contactos',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener el catalogo' })
  public async obtenerCatalogoArchivoDefault(@Headers() headers) {
    //Verifica si el idioma llega vacio
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const catalogoTipoArchivo = await this.catalogoArchivoDefaultService.obtenerCatalogoArchivoDefault();
      if (!catalogoTipoArchivo || catalogoTipoArchivo.length === 0) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'ERROR_OBTENER',
          ),
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: catalogoTipoArchivo,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
