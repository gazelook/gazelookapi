import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ObtenerCatalogoMetodoPagoService } from '../casos_de_uso/obtener-catalogo-metodo-pago.service';
import { CatalogoMetodoPagoDto } from './../entidad/catalogo-metodo-pago.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class ObtenerCatalogoMetodoPagoControlador {
  constructor(
    private readonly obtenerCatalogoMetodoPagoService: ObtenerCatalogoMetodoPagoService,
    private readonly i18n: I18nService,
  ) {}
  funcion = new Funcion(this.i18n);
  @Get('/obtener-catalogo-metodo-pago')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: CatalogoMetodoPagoDto,
    description: 'Lista de todos los catalogos metodos de pago',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener los catalogos' })
  @ApiOperation({
    summary:
      'Este metodo devuelve todos los catalogos de los metodos de pago existentes con todas las traducciones sin importar su estado o idioma',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async obtenerCatalogoMetodoPago(@Headers() headers) {
    const respuesta = new RespuestaInterface();
    try {
      const catalogo = await this.obtenerCatalogoMetodoPagoService.obtenerListaMetodoPago();
      if (catalogo) {
        //return catalogo;
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: catalogo,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      //return e;
    }
  }
}
