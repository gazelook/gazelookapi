import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CatalogoTipoArchivoService } from '../casos_de_uso/catalogo-tipo-archivo.service';
import { CatalogoTipoArchivoDto } from '../entidad/catalogo-tipo-archivo.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoTipoArchivoControlador {
  constructor(
    private readonly catalogoTipoArchivoService: CatalogoTipoArchivoService,
    private readonly i18n: I18nService,
  ) {}
  funcion = new Funcion(this.i18n);

  @Get('/catalogo-tipo-archivo')
  @ApiResponse({
    status: 200,
    type: CatalogoTipoArchivoDto,
    description: 'Lista de catalogos tipo archivo',
  })
  @ApiOperation({
    summary:
      'Este metodo devuelve todos los catalogos de tipo archivo tales como: principal, portada, miniatura, link',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener los catalogos' })
  public async obtenerCatalogoTipoArchivo(@Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const catalogoTipoArchivo = await this.catalogoTipoArchivoService.obtenerCatalogoTipoArchivo();
      if (catalogoTipoArchivo && catalogoTipoArchivo.length > 0) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: catalogoTipoArchivo,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'ERROR_OBTENER',
          ),
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
