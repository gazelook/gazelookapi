import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Direccion } from 'src/drivers/mongoose/interfaces/direccion/direccion.interface';
import { TraduccionDireccion } from 'src/drivers/mongoose/interfaces/traduccion_direccion/traduccion-direccion.interface';
import {
  codidosEstadosTraduccionDireccion,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';

@Injectable()
export class ObtenerDireccionService {
  constructor(
    @Inject('DIRECCION_MODEL')
    private readonly direccionModel: Model<Direccion>,
    @Inject('TRADUCCION_DIRECCION_MODEL')
    private readonly traduccionDireccionModel: Model<TraduccionDireccion>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
  ) {}

  async obtenerDireccionTraduccionOriginal(
    idDireccion,
    opts?: any,
  ): Promise<any> {
    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.direccion,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    try {
      const direccion = await this.direccionModel
        .findOne({ _id: idDireccion, estado: codEstado })
        .session(opts.session)
        .populate({
          path: 'traducciones',
          match: {
            original: true,
            estado: codidosEstadosTraduccionDireccion.activa,
          },
        });

      if (direccion && direccion.traducciones) {
        if (direccion.traducciones.length > 0) {
          let obTraduccionDireccion = {
            descripcion: direccion.traducciones[0]['descripcion'],
            idioma: direccion.traducciones[0]['idioma'],
            original: direccion.traducciones[0]['original'],
          };
          return obTraduccionDireccion;
        }
      }
      return false;
    } catch (error) {
      return error;
    }
  }
}
