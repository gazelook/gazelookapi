import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { Direccion } from '../../../drivers/mongoose/interfaces/direccion/direccion.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class EliminarDireccionService {
  constructor(
    //private obtenerNombreEntidadService: ObtenerNombreEntidadService,
    //9private obtenerNombreEstadoService: ObtenerNombreEstadoService,
    @Inject('DIRECCION_MODEL')
    private readonly direccionModel: Model<Direccion>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async eliminarDireccion(
    data: any,
    hibernado: boolean,
    opts: any,
  ): Promise<any> {
    try {
      const idDireccion = data._id;
      const idUsuario = data.usuario;

      const getDireccion = await this.direccionModel.findOne({
        _id: idDireccion,
      });
      if (!getDireccion) {
        return null;
      }

      let accionHis = '';
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      accionHis = accion.codigo;

      // accion modificar
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.direccion,
      );
      const codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidad,
      );
      const codEstado = estado.codigo;

      // estado hibernado
      const estadoHibernado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.hibernado,
        codEntidad,
      );
      const codEstadoHib = estadoHibernado.codigo;

      //actualizar estado eliminar
      const dataDireccion = {
        estado: codEstado,
      };
      if (hibernado) {
        dataDireccion.estado = codEstadoHib;
        accionHis = accionModificar.codigo;
      }
      const direccionUpdated = await this.direccionModel.findByIdAndUpdate(
        idDireccion,
        dataDireccion,
        opts,
      );

      const dataAlbumHistorico = JSON.parse(JSON.stringify(direccionUpdated));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const dataHistoricoAlbum: any = {
        datos: dataAlbumHistorico,
        usuario: idUsuario,
        accion: accionHis,
        entidad: codEntidad,
      };
      // crear el historico de direccion
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      return direccionUpdated;
    } catch (error) {
      throw error;
    }
  }
}
