import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Telefono } from 'src/drivers/mongoose/interfaces/telefonos/telefono.interface';
import {
  codigosHibernadoEntidades,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class ActualizarTelefonoService {
  constructor(
    @Inject('TELEFONO_MODEL') private readonly telefonoModel: Model<Telefono>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async actualizarTelefono(dataTelefono, opts: any): Promise<any> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion.codigosHibernadoEntidades;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.telefono,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;
    const dataUpdate = {
      numero: dataTelefono.numero,
      pais: dataTelefono.pais,
      estado: codEstado,
    };

    try {
      const telefono = await this.telefonoModel.findByIdAndUpdate(
        dataTelefono._id,
        dataUpdate,
        opts,
      );

      const datosDirHis = JSON.parse(JSON.stringify(telefono));
      //eliminar parametro no necesarios
      delete datosDirHis.fechaCreacion;
      delete datosDirHis.fechaActualizacion;
      delete datosDirHis.__v;

      const dataHistorico: any = {
        datos: datosDirHis,
        usuario: dataTelefono.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };

      this.crearHistoricoService.crearHistoricoServer(dataHistorico);
      return telefono;
    } catch (error) {
      return error;
    }
  }

  async activarTelefono(data: any, opts: any): Promise<any> {
    try {
      const idTelefono = data._id;
      const idUsuario = data.usuario;

      const getTelefono = await this.telefonoModel.findOne({
        $and: [
          { _id: idTelefono },
          { estado: codigosHibernadoEntidades.telefonoHibernado },
        ],
      });
      if (!getTelefono) {
        return null;
      }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion.codigo;

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.telefono,
      );
      const codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado.codigo;

      //actualizar estado eliminar
      const dataTelefono = {
        estado: codEstado,
      };
      const telefonoUpdated = await this.telefonoModel.findByIdAndUpdate(
        idTelefono,
        dataTelefono,
        opts,
      );

      const dataAlbumHistorico = JSON.parse(JSON.stringify(telefonoUpdated));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const dataHistoricoAlbum: any = {
        datos: dataAlbumHistorico,
        usuario: idUsuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      return telefonoUpdated;
    } catch (error) {
      throw error;
    }
  }
}
