import { Module, HttpModule } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { CrearDireccionService } from './crear-direccion.service';
import { DireccionProviders } from '../drivers/direccion.provider';
import { CrearTelefonoService } from './crear-telefono.service';
import { ActualizarDireccionService } from './actualizar-direccion.service';
import { EliminarDireccionService } from './eliminar-direccion.service';
import { EliminarDireccionCompletoService } from './eliminar-direccion-completo.service';
import { ActualizarTelefonoService } from './actualizar-telefono.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { TraducirDireccionService } from './traducir-direccion.service';
import { ObtenerDireccionService } from './obtener-direccion.service';
import { EliminarTelefonoCompletoService } from './eliminar-telefono-completo.service';
import { EliminarTelefonoService } from './eliminar-telefono.service';

@Module({
  imports: [DBModule, HttpModule, CatalogosServiceModule],
  providers: [
    ...DireccionProviders,
    CrearDireccionService,
    CrearTelefonoService,
    ActualizarDireccionService,
    EliminarDireccionService,
    EliminarDireccionCompletoService,
    ActualizarTelefonoService,
    CatalogoIdiomasService,
    TraducirDireccionService,
    ObtenerDireccionService,
    EliminarTelefonoCompletoService,
    EliminarTelefonoService,
  ],
  exports: [
    CrearDireccionService,
    CrearTelefonoService,
    ActualizarDireccionService,
    EliminarDireccionService,
    EliminarDireccionCompletoService,
    ActualizarTelefonoService,
    CatalogoIdiomasService,
    TraducirDireccionService,
    ObtenerDireccionService,
    EliminarTelefonoCompletoService,
    EliminarTelefonoService,
  ],
  controllers: [],
})
export class DireccionServiceModule {}
