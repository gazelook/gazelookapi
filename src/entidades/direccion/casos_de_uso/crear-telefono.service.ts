import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Telefono } from '../../../drivers/mongoose/interfaces/telefonos/telefono.interface';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class CrearTelefonoService {
  constructor(
    @Inject('TELEFONO_MODEL') private readonly telefonoModel: Model<Telefono>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async crearTelefono(dataTelefono, opts: any): Promise<Telefono> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.telefono,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;
    const dataSave = {
      numero: dataTelefono.numero,
      pais: dataTelefono.pais,
      estado: codEstado,
    };

    try {
      const telefono = await new this.telefonoModel(dataSave).save(opts);

      const dataTelefonoHistorico = JSON.parse(JSON.stringify(telefono));
      //eliminar parametro no necesarios
      delete dataTelefonoHistorico.fechaCreacion;
      delete dataTelefonoHistorico.fechaActualizacion;
      delete dataTelefonoHistorico.__v;

      const dataHistoricoTelefono: any = {
        datos: dataTelefonoHistorico,
        usuario: dataTelefono.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de telefono
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoTelefono);
      return telefono;
    } catch (error) {
      return error;
    }
  }
}
