import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Direccion } from '../../../drivers/mongoose/interfaces/direccion/direccion.interface';
import { TraduccionDireccion } from '../../../drivers/mongoose/interfaces/traduccion_direccion/traduccion-direccion.interface';

@Injectable()
export class EliminarDireccionCompletoService {
  constructor(
    @Inject('DIRECCION_MODEL')
    private readonly direccionModel: Model<Direccion>,
    @Inject('TRADUCCION_DIRECCION_MODEL')
    private readonly traduccionDireccionModel: Model<TraduccionDireccion>,
  ) {}

  async eliminarDireccionCompleto(idDireccion, opts?: any): Promise<any> {
    try {
      const getDireccion = await this.direccionModel.findOne({
        _id: idDireccion,
      });
      if (!getDireccion) {
        return null;
      }
      await this.traduccionDireccionModel.deleteMany(
        { direccion: getDireccion._id },
        opts,
      );
      await this.direccionModel.deleteOne({ _id: getDireccion._id }, opts);

      return true;
    } catch (error) {
      throw error;
    }
  }
}
