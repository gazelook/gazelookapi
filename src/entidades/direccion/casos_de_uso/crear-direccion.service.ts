import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Direccion } from 'src/drivers/mongoose/interfaces/direccion/direccion.interface';
import { TraduccionDireccion } from 'src/drivers/mongoose/interfaces/traduccion_direccion/traduccion-direccion.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codidosEstadosTraduccionDireccion,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CrearDireccionDto } from '../dtos/crear-direccion.dto';

@Injectable()
export class CrearDireccionService {
  constructor(
    @Inject('DIRECCION_MODEL')
    private readonly direccionModel: Model<Direccion>,
    @Inject('TRADUCCION_DIRECCION_MODEL')
    private readonly traduccionDireccionModel: Model<TraduccionDireccion>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async crearDireccion(
    dataDireccion: CrearDireccionDto,
    opts?: any,
  ): Promise<Direccion> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.direccion,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado: string = estado.codigo;

    try {
      const idDireccion = new mongoose.mongo.ObjectId();

      let dataSaveDreccion: any = {
        _id: idDireccion,
        latitud: dataDireccion.latitud,
        longitud: dataDireccion.longitud,
        localidad: dataDireccion.localidad,
        pais: dataDireccion.pais,
        estado: codEstado,
      };

      let traduccionDireccion = null;
      if (
        dataDireccion?.traducciones &&
        dataDireccion.traducciones.length > 0
      ) {
        let textoTraducido = await traducirTexto(
          null,
          dataDireccion.traducciones[0].descripcion,
        );
        let idiomaDetectado = textoTraducido.idiomaDetectado;
        //Obtiene el idioma por el codigo
        const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
          idiomaDetectado,
        );
        let codIdioma = idioma.codigo;

        //data para guardar la trauduccion de la direccion
        const dataTraduccion = {
          descripcion: dataDireccion.traducciones[0].descripcion,
          idioma: codIdioma,
          original: true,
          direccion: idDireccion,
          estado: codidosEstadosTraduccionDireccion.activa,
        };
        traduccionDireccion = await new this.traduccionDireccionModel(
          dataTraduccion,
        ).save(opts);

        dataSaveDreccion.traducciones = [traduccionDireccion._id];
      }

      const direccion = await new this.direccionModel(dataSaveDreccion).save(
        opts,
      );
      const dataDireccionHistorico = JSON.parse(JSON.stringify(direccion));

      //eliminar parametro no necesarios
      delete dataDireccionHistorico.__v;

      const dataHistoricoDirreccion = {
        datos: dataDireccionHistorico,
        usuario: dataDireccion.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };

      // crear el historico de direccion
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoDirreccion);
      return direccion;
    } catch (error) {
      return error;
    }
  }
}
