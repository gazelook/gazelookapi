import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Telefono } from '../../../drivers/mongoose/interfaces/telefonos/telefono.interface';

@Injectable()
export class EliminarTelefonoCompletoService {
  constructor(
    @Inject('TELEFONO_MODEL') private readonly telefonoModel: Model<Telefono>,
  ) {}

  async eliminarTelefonoCompleto(idTelefono: string, opts?: any): Promise<any> {
    try {
      const getDireccion = await this.telefonoModel.findOne({
        _id: idTelefono,
      });
      if (!getDireccion) {
        return null;
      }
      await this.telefonoModel.deleteOne({ _id: getDireccion._id }, opts);

      return true;
    } catch (error) {
      throw error;
    }
  }
}
