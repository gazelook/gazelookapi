import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Direccion } from 'src/drivers/mongoose/interfaces/direccion/direccion.interface';
import { TraduccionDireccion } from 'src/drivers/mongoose/interfaces/traduccion_direccion/traduccion-direccion.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codidosEstadosTraduccionDireccion,
  codigosHibernadoEntidades,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class ActualizarDireccionService {
  constructor(
    @Inject('DIRECCION_MODEL')
    private readonly direccionModel: Model<Direccion>,
    @Inject('TRADUCCION_DIRECCION_MODEL')
    private readonly traduccionDireccionModel: Model<TraduccionDireccion>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async actualizarDireccion(dataDireccion, opts: any): Promise<any> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion.codigosHibernadoEntidades;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.direccion,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    try {
      let traduccionDireccion = null;
      if (
        dataDireccion?.traducciones &&
        dataDireccion.traducciones.length > 0
      ) {
        let textoTraducido = await traducirTexto(
          null,
          dataDireccion.traducciones[0].descripcion,
        );
        let idiomaDetectado = textoTraducido.idiomaDetectado;
        //Obtiene el idioma por el codigo
        const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
          idiomaDetectado,
        );
        let codIdioma = idioma.codigo;

        let getTraduccion = null;
        if (dataDireccion.traducciones[0]?._id) {
          getTraduccion = await this.traduccionDireccionModel.findOne({
            _id: dataDireccion.traducciones[0]?._id,
            original: true,
          });
        }
        if (!getTraduccion) {
          await this.traduccionDireccionModel.updateMany(
            { direccion: dataDireccion._id },
            { $set: { estado: codidosEstadosTraduccionDireccion.eliminado } },
            opts,
          );

          //data para guardar la trauduccion de la direccion
          const dataTraduccion = {
            descripcion: dataDireccion.traducciones[0].descripcion,
            idioma: codIdioma,
            original: true,
            direccion: dataDireccion._id,
            estado: codidosEstadosTraduccionDireccion.activa,
          };
          traduccionDireccion = await new this.traduccionDireccionModel(
            dataTraduccion,
          ).save(opts);
        } else {
          const direccion = await this.direccionModel
            .findOne({ _id: dataDireccion._id })
            .session(opts.session);
          return direccion;
        }
      }
      let traduc = [];
      if (traduccionDireccion) {
        traduc.push(traduccionDireccion._id);
      }
      const dataUpdate = {
        latitud: dataDireccion?.latitud,
        longitud: dataDireccion?.longitud,
        traducciones: traduc,
        localidad: dataDireccion?.localidad,
        pais: dataDireccion?.pais,
        estado: codEstado,
      };
      const direccion = await this.direccionModel.findByIdAndUpdate(
        dataDireccion._id,
        dataUpdate,
        opts,
      );

      const dataDireccionHistorico = JSON.parse(JSON.stringify(direccion));
      //eliminar parametro no necesarios
      delete dataDireccionHistorico.fechaCreacion;
      delete dataDireccionHistorico.fechaActualizacion;
      delete dataDireccionHistorico.__v;

      const dataHistoricoDirreccion: any = {
        datos: dataDireccionHistorico,
        usuario: dataDireccion.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de direccion
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoDirreccion);
      return direccion;
    } catch (error) {
      return error;
    }
  }

  async activarDireccion(data: any, opts: any): Promise<any> {
    try {
      const idDireccion = data._id;
      const idUsuario = data.usuario;

      const getDireccion = await this.direccionModel.findOne({
        $and: [
          { _id: idDireccion },
          { estado: codigosHibernadoEntidades.direccionHibernado },
        ],
      });
      if (!getDireccion) {
        return null;
      }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion.codigo;

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.direccion,
      );
      const codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado.codigo;

      //actualizar estado eliminar
      const dataDireccion = {
        estado: codEstado,
      };
      const direccionUpdated = await this.direccionModel.findByIdAndUpdate(
        idDireccion,
        dataDireccion,
        opts,
      );

      const dataAlbumHistorico = JSON.parse(JSON.stringify(direccionUpdated));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const dataHistoricoAlbum: any = {
        datos: dataAlbumHistorico,
        usuario: idUsuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de direccion
      await this.crearHistoricoService.crearHistoricoTransaccion(
        dataHistoricoAlbum,
        opts,
      );

      return direccionUpdated;
    } catch (error) {
      throw error;
    }
  }
}
