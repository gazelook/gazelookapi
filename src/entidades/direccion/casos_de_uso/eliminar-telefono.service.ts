import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { Telefono } from '../../../drivers/mongoose/interfaces/telefonos/telefono.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class EliminarTelefonoService {
  constructor(
    //private obtenerNombreEntidadService: ObtenerNombreEntidadService,
    //9private obtenerNombreEstadoService: ObtenerNombreEstadoService,
    @Inject('TELEFONO_MODEL') private readonly TelefonoModel: Model<Telefono>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async eliminarTelefono(
    data: any,
    hibernado: boolean,
    opts: any,
  ): Promise<any> {
    try {
      const idTelefono = data._id;
      const idUsuario = data.usuario;

      const getTelefono = await this.TelefonoModel.findOne({ _id: idTelefono });
      if (!getTelefono) {
        return null;
      }

      let accionHis = '';
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      accionHis = accion.codigo;

      // accion modificar
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.telefono,
      );
      const codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidad,
      );
      const codEstado = estado.codigo;

      // estado hibernado
      const estadoHibernado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.hibernado,
        codEntidad,
      );
      const codEstadoHib = estadoHibernado.codigo;

      //actualizar estado eliminar
      const dataTelefono = {
        estado: codEstado,
      };
      if (hibernado) {
        dataTelefono.estado = codEstadoHib;
        accionHis = accionModificar.codigo;
      }
      const TelefonoUpdated = await this.TelefonoModel.findByIdAndUpdate(
        idTelefono,
        dataTelefono,
        opts,
      );

      const dataAlbumHistorico = JSON.parse(JSON.stringify(TelefonoUpdated));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const dataHistoricoAlbum: any = {
        datos: dataAlbumHistorico,
        usuario: idUsuario,
        accion: accionHis,
        entidad: codEntidad,
      };
      // crear el historico de Telefono
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      return TelefonoUpdated;
    } catch (error) {
      throw error;
    }
  }
}
