import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Direccion } from 'src/drivers/mongoose/interfaces/direccion/direccion.interface';
import { TraduccionDireccion } from 'src/drivers/mongoose/interfaces/traduccion_direccion/traduccion-direccion.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codidosEstadosTraduccionDireccion,
  nombreAcciones,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class TraducirDireccionService {
  constructor(
    @Inject('DIRECCION_MODEL')
    private readonly direccionModel: Model<Direccion>,
    @Inject('TRADUCCION_DIRECCION_MODEL')
    private readonly traduccionDireccionModel: Model<TraduccionDireccion>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async traducirDireccion(idDireccion, codNombreIdioma, opts): Promise<any> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.direccion,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el idioma por el codigo
    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codNombreIdioma,
    );
    let codIdioma = idioma.codigo;

    try {
      //Obtengo la traduccion original
      const getTradDireccion = await this.traduccionDireccionModel.findOne({
        direccion: idDireccion,
        original: true,
        estado: codidosEstadosTraduccionDireccion.activa,
      });

      if (getTradDireccion) {
        //Manda a traducir el texto
        let textoTraducido = await traducirTexto(
          codNombreIdioma,
          getTradDireccion.descripcion,
        );
        let descripcion = textoTraducido.textoTraducido;

        //data para guardar la trauduccion de la direccion
        const dataTraduccion = {
          descripcion: descripcion,
          idioma: codIdioma,
          original: false,
          direccion: idDireccion,
          estado: codidosEstadosTraduccionDireccion.activa,
        };
        const traduccionDireccion = await new this.traduccionDireccionModel(
          dataTraduccion,
        ).save(opts);

        const updateDireccionTraduccion = await this.direccionModel.findByIdAndUpdate(
          idDireccion,
          { $push: { traducciones: traduccionDireccion._id } },
        );

        const dataTradDireccionHistorico = JSON.parse(
          JSON.stringify(traduccionDireccion),
        );
        //eliminar parametro no necesarios
        delete dataTradDireccionHistorico.__v;

        const dataHistoricoDirreccion: any = {
          datos: dataTradDireccionHistorico,
          usuario: '',
          accion: getAccion,
          entidad: codEntidad,
        };
        // crear el historico de direccion
        await this.crearHistoricoService.crearHistoricoTransaccion(
          dataHistoricoDirreccion,
          opts,
        );

        let returnTraduccionDireccion = [
          {
            descripcion: traduccionDireccion.descripcion,
            idioma: traduccionDireccion.idioma,
            original: traduccionDireccion.original,
          },
        ];

        return returnTraduccionDireccion;
      }
      return [];
    } catch (error) {
      return error;
    }
  }
}
