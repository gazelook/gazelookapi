export class TraducionDireccionDto {
  descripcion: string;
}

export class CrearDireccionDto {
  latitud: number;
  longitud: number;
  traducciones: Array<TraducionDireccionDto>;
  pais?: string;
  estado?: string;
  localidad?: string;
  usuario?: string;
}
