import { Module } from '@nestjs/common';
import { DireccionServiceModule } from './casos_de_uso/direccion.services.module';

@Module({
  imports: [],
  providers: [DireccionServiceModule],
  controllers: [],
  exports: [],
})
export class DireccionModule {}
