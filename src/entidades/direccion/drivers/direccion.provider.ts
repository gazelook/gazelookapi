import { Connection } from 'mongoose';
import { catalogoIdiomasModelo } from 'src/drivers/mongoose/modelos/catalogoIdiomas/catalogoIdiomasModelo';
import { TraduccionDireccionModelo } from 'src/drivers/mongoose/modelos/traduccion_direccion/traduccion_direccion.schema';
import { DireccionModelo } from '../../../drivers/mongoose/modelos/direcciones/direccion.schema';
import { TelefonoModelo } from '../../../drivers/mongoose/modelos/telefonos/telefono.schema';

export const DireccionProviders = [
  {
    provide: 'DIRECCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('direccion', DireccionModelo, 'direccion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TELEFONO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('telefono', TelefonoModelo, 'telefono'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_DIRECCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_direccion',
        TraduccionDireccionModelo,
        'traduccion_direccion',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_IDIOMAS',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_idiomas',
        catalogoIdiomasModelo,
        'catalogo_idiomas',
      ),
    inject: ['DB_CONNECTION'],
  },
];
