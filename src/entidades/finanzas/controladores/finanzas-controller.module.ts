import { CrearGastoOperacionControlador } from './crear-gasto-operacion.controller';
import { ObtenerGastosOperacionesControlador } from './obtener-gastos-operaciones.controller';
import { FinanzasServicesModule } from './../casos_de_uso/finanzas-services.module';
import { Module } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { CrearBalanceControlador } from './crear-balance.controller';
import { RepartirFondosControlador } from './repartir-fondos-proyectos.controller';

@Module({
  imports: [FinanzasServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    CrearGastoOperacionControlador,
    ObtenerGastosOperacionesControlador,
    CrearBalanceControlador,
    RepartirFondosControlador,
  ],
})
export class FinanzasControllerModule {}
