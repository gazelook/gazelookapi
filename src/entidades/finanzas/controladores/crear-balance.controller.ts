import {
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { CrearBalanceService } from '../casos_de_uso/crear-balance.service';

// import {CrearSuscripcionAdaptador}from '../adaptador/v1/adapterCrearSuscripcion'

// @ApiTags('Suscripciones')
@ApiTags('Finanzas')
@Controller('api/generar-balance')
export class CrearBalanceControlador {
  constructor(
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
    private crearBalanceService: CrearBalanceService,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Post('/')
  @ApiOperation({
    summary: 'Este método crea genera un balance mensual',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Crea gasto operacional' })
  @ApiResponse({ status: 201, description: 'Creado' })
  @ApiResponse({ status: 404, description: 'Datos inválidos' })
  // @UseGuards(AuthGuard('jwt'))
  public async crearbalance(@Headers() headers) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    // const respuesta = new RespuestaInterface();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };
      const balance = await this.crearBalanceService.crearBalance(opts);

      if (balance) {
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: balance,
        });
      } else {
        //Confirma los cambios de la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        //llama al metodo de traduccion dinamica
        const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_CREACION',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_CREACION,
        });
      }
    } catch (err) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
