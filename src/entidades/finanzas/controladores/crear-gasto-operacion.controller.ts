import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { GastoOperacionService } from './../casos_de_uso/crear-gasto-operacion.service';
import { GastoOperacionDto } from './../entidad/gasto-operacional-dto';

// import {CrearSuscripcionAdaptador}from '../adaptador/v1/adapterCrearSuscripcion'

// @ApiTags('Suscripciones')
@ApiTags('Finanzas')
@Controller('api/gasto-operacion')
export class CrearGastoOperacionControlador {
  constructor(
    private readonly gastoOperacionService: GastoOperacionService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Post('/')
  @ApiOperation({
    summary: 'Este método crea un gasto operaconal',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Crea gasto operacional' })
  @ApiResponse({ status: 201, description: 'Creado' })
  @ApiResponse({ status: 404, description: 'Datos inválidos' })
  @UseGuards(AuthGuard('jwt'))
  public async crearGasto(
    @Headers() headers,
    @Body() gastoOperacionDto: GastoOperacionDto,
    // @Query('idUsuario') idUsuario: string,
    // @Query('monto') monto: number,
  ) {
    const respuesta = new RespuestaInterface();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const gasto = await this.gastoOperacionService.crearGasto(
        gastoOperacionDto.usuario._id,
        gastoOperacionDto.monto,
      );

      if (gasto) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: gasto,
        });
      } else {
        //llama al metodo de traduccion dinamica
        const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_CREACION',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_CREACION,
        });
      }
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
