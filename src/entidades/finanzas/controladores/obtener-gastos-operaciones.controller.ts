import { RespuestaGastoOperacionDto } from './../entidad/gasto-operacional-dto';
import { ObtenerGastosOperacionesService } from './../casos_de_uso/obtener-gastos-operaciones.service';

import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';

import {
  Controller,
  HttpStatus,
  Query,
  Headers,
  UseGuards,
  Get,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';

// import {CrearSuscripcionAdaptador}from '../adaptador/v1/adapterCrearSuscripcion'

// @ApiTags('Suscripciones')
@ApiTags('Finanzas')
@Controller('api/gastos-operaciones')
export class ObtenerGastosOperacionesControlador {
  constructor(
    private readonly obtenerGastosOperacionesService: ObtenerGastosOperacionesService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/')
  @ApiOperation({
    summary: 'Este método obtiene los gastos operaconales que gazelook genera',
  })
  @ApiResponse({ status: 201, description: 'Creado' })
  @ApiResponse({
    status: 400,
    type: RespuestaGastoOperacionDto,
    description: 'Datos inválidos',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerGastoOperacional(
    @Headers() headers,
    @Query('idUsuario') idUsuario: string,
    @Query('fechaInicial') fechaInicial: Date,
    @Query('fechaFinal') fechaFinal: Date,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const gastos = await this.obtenerGastosOperacionesService.listarRango(
        idUsuario,
        fechaInicial,
        fechaFinal,
      );

      if (gastos) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: gastos,
        });
      } else {
        //llama al metodo de traduccion dinamica
        const ERROR_OBTENER = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_OBTENER',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ERROR_OBTENER,
        });
      }
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
