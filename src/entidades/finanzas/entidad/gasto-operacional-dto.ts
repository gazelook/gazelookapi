import { ApiProperty } from '@nestjs/swagger';

export class IdUsuarioDto {
  @ApiProperty()
  _id: string;
}
export class GastoOperacionDto {
  @ApiProperty()
  usuario: IdUsuarioDto;
  @ApiProperty()
  monto: number;
}

export class RespuestaGastoOperacionDto {
  @ApiProperty()
  gastos_operacionales: number;
  @ApiProperty()
  fechaInicio: Date;
  @ApiProperty()
  fechaFin: Date;
}
