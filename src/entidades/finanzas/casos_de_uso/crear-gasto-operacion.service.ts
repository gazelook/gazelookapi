import {
  catalogoOrigen,
  beneficiario,
  nombreEntidades,
  nombrecatalogoEstados,
  nombreAcciones,
  descripcionTransPagosGazelook,
} from './../../../shared/enum-sistema';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import * as mongoose from 'mongoose';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';

@Injectable()
export class GastoOperacionService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async crearGasto(usuario: string, monto: number): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );

      // guardar informacion de pago

      const transaccionDto = {
        estado: estado.codigo,
        monto: monto,
        moneda: listaCodigosMonedas.USD,
        descripcion: descripcionTransPagosGazelook.gastoOperacional,
        origen: catalogoOrigen.gastos_operativos,
        balance: [],
        beneficiario: beneficiario.usuario,
        metodoPago: null,
        informacionPago: null,
        usuario: usuario,
      };

      const transaccion = new this.transaccionModel(transaccionDto);
      const crearTransaccion = await transaccion.save(opts);

      //datos para guardar el historico
      const newHistorico: any = {
        datos: crearTransaccion,
        usuario: usuario,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return crearTransaccion;
    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
