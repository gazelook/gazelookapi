import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import {
  catalogoOrigen,
  codigosMetodosPago,
  nombrecatalogoEstados,
  nombreEntidades,
} from './../../../shared/enum-sistema';

@Injectable()
export class ObtenerGastosOperacionesService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
  ) {}
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types

  async listarRango(
    usuario: string,
    fechaInicial: any,
    fechaFinal: any,
  ): Promise<any> {
    try {
      const perfil = await this.obtenerPerfilUsuarioService.obtenerPerfilesUsuario(
        usuario,
      );

      if (perfil) {
        const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
          nombreEntidades.transaccion,
        );

        const estado = await this.catalogoEstadoService.obtenerNombreEstado(
          nombrecatalogoEstados.activa,
          entidad.codigo,
        );

        let transacciones: any;

        if (fechaInicial && fechaFinal) {
          transacciones = await this.transaccionModel.find({
            $and: [
              {
                fechaActualizacion: {
                  $gte: new Date(
                    new Date(fechaInicial).setUTCHours(0, 0, 0, 0),
                  ),
                  $lt: new Date(
                    new Date(fechaFinal).setUTCHours(23, 59, 59, 999),
                  ),
                },
              },
              { estado: estado.codigo },
              {
                $or: [
                  { metodoPago: codigosMetodosPago.payments1 },
                  { metodoPago: codigosMetodosPago.payments2 },
                ],
              },
              { destino: catalogoOrigen.gastos_operativos },
            ],
          });
        } else {
          const fechaActual = new Date();
          const fechaIni = new Date(new Date().setMonth(0)).setDate(1);

          console.log(
            new Date(new Date(fechaIni).setUTCHours(0, 0, 0, 0)),
            new Date(new Date(fechaActual).setUTCHours(23, 59, 59, 999)),
          );

          transacciones = await this.transaccionModel.find({
            $and: [
              {
                fechaActualizacion: {
                  $gte: new Date(new Date(fechaIni).setUTCHours(0, 0, 0, 0)),
                  $lt: new Date(
                    new Date(fechaActual).setUTCHours(23, 59, 59, 999),
                  ),
                },
              },
              { estado: estado.codigo },
              {
                $or: [
                  { metodoPago: codigosMetodosPago.payments1 },
                  { metodoPago: codigosMetodosPago.payments2 },
                ],
              },
              { destino: catalogoOrigen.gastos_operativos },
            ],
          });
        }

        let gastos = 0;

        for (const transaccion of transacciones) {
          if (transaccion.totalRecibido) {
            gastos += transaccion.totalRecibido;
          }
        }

        return {
          gastos_operacionales: gastos,
          fechaInicio: fechaInicial,
          fechaFinal: fechaFinal,
        };
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }
}
