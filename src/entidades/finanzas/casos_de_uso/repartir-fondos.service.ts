import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';
import { Balance } from 'src/drivers/mongoose/interfaces/balance/balance.interface';
import { Beneficiario } from 'src/drivers/mongoose/interfaces/beneficiario/beneficiario.interface';
import { Bonificacion } from 'src/drivers/mongoose/interfaces/bonificacion/bonificacion.interface';
import { CatalogoPorcentajeFondosReservados } from 'src/drivers/mongoose/interfaces/catalogo_porcentaje_fondos_reservados/catalogo-porcentaje_fondos_reservados.interface';
import { Evento } from 'src/drivers/mongoose/interfaces/evento/evento.interface';
import { FinanciamientoEsperaFondos } from 'src/drivers/mongoose/interfaces/financiamiento_espera_fondos/financiamiento-espera-fondos.interface';
import { FondosFinanciamiento } from 'src/drivers/mongoose/interfaces/fondos_financiamiento/fondos_financiamiento.interface';
import { FondosTipoProyecto } from 'src/drivers/mongoose/interfaces/fondos_tipo_proyecto/fondos_tipo_proyecto.interface';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { CatalogoPorcentajeEsperaFondosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-porcentaje-espera-fondos.service';
import { CatalogoPorcentajeFinanciacionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-porcentaje-financiacion.service';
import { CatalogoPorcentajeProyectoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-porcentaje-proyecto.service';
import { ConfiguracionEventoService } from 'src/entidades/catalogos/casos_de_uso/configuracion-evento.service';
import { FormulaEventoService } from 'src/entidades/catalogos/casos_de_uso/formula_evento.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { ActualizaEstadoProyectoService } from 'src/entidades/proyectos/casos_de_uso/actualiza-estado-proyecto.service';
import { FondosFinanciamientoProyectoService } from 'src/entidades/proyectos/casos_de_uso/fondos-financiamiento-proyecto.service';
import { FondosTipoProyectoService } from 'src/entidades/proyectos/casos_de_uso/fondos-tipo-proyecto.service';
import { ProyectosMasVotadosService } from 'src/entidades/proyectos/casos_de_uso/proyectos-mas-votados.service';
import { CrearConversionTransaccionService } from 'src/entidades/transaccion/casos_de_uso/crear-conversion-transaccion.service';
import { CrearTransaccionFondosProyectoService } from 'src/entidades/transaccion/casos_de_uso/crear-transaccion-fondos-proyectos.service';
import { ConversionTransaccionDto } from 'src/entidades/transaccion/entidad/conversion-transaccion.dto';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
import {
  beneficiario,
  catalogoOrigen,
  catalogoTipoBalance,
  codigoEntidades,
  codigosCatalogo,
  codigosCatalogoAcciones,
  codigosCatalogoPorcentajeFinanciacion,
  codigosConfiguracionEvento,
  codigosEstadosBeneficiario,
  codigosEstadosBonificacion,
  codigosEstadosFinanciamientoEsperaFondos,
  codigosEstadosFondosFinanciacion,
  codigosEstadosFondosTipoProyecto,
  codigosFormulasEvento,
  descripcionTransPagosGazelook,
  estadoEvento,
  estadosCatalogoPorcentajeFondosReservados,
  estadosProyecto,
  estadosTransaccion,
} from '../../../shared/enum-sistema';

@Injectable()
export class RepartirFondosService {
  numeroTotalEjecucionReparticionFondos = 0;
  arrayTipoProyectosRecorridos = [];

  constructor(
    @Inject('BALANCE_MODEL')
    private readonly balanceModel: Model<Balance>,
    @Inject('BONIFICACION_MODEL')
    private readonly bonificacionModel: Model<Bonificacion>,
    @Inject('BENEFICIARIO_MODEL')
    private readonly beneficiarioModel: Model<Beneficiario>,
    @Inject('FONDOS_TIPO_PROYECTO_MODEL')
    private readonly fondosTipoProyectoModel: Model<FondosTipoProyecto>,
    @Inject('FONDOS_FINANCIAMIENTO_MODEL')
    private readonly fondosFinanciamientoModel: Model<FondosFinanciamiento>,
    @Inject('FINANCIAMIENTO_ESPERA_FONDOS_MODEL')
    private readonly financiamientoEsperaFondosModel: Model<
      FinanciamientoEsperaFondos
    >,
    @Inject('EVENTO_MODEL')
    private readonly eventoModel: Model<Evento>,
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: Model<Proyecto>,
    @Inject('CATALOGO_PORCENTAJE_FONDOS_RESERVADOS_MODEL')
    private readonly catalogoPorcentajeFondosReservadosModel: Model<
      CatalogoPorcentajeFondosReservados
    >,
    private crearHistoricoService: CrearHistoricoService,
    private configuracionEventoService: ConfiguracionEventoService,
    private formulaEventoService: FormulaEventoService,
    private proyectosMasVotadosService: ProyectosMasVotadosService,
    private catalogoPorcentajeProyectoService: CatalogoPorcentajeProyectoService,
    private catalogoPorcentajeFinanciacionService: CatalogoPorcentajeFinanciacionService,
    private actualizaEstadoProyectoService: ActualizaEstadoProyectoService,
    private crearTransaccionFondosProyectoService: CrearTransaccionFondosProyectoService,
    private catalogoPorcentajeEsperaFondosService: CatalogoPorcentajeEsperaFondosService,
    private fondosTipoProyectoService: FondosTipoProyectoService,
    private fondosFinanciamientoProyectoService: FondosFinanciamientoProyectoService,
    private crearConversionTransaccionService : CrearConversionTransaccionService
  ) { }

  //reparte los fondos a los proyectos mas votados por el tipo de proyecto
  async repartirFondosProyectos(idEvento, opts): Promise<any> {
    try {
      let getValorActualUltimoBalance = 0;
      let getUltimoBalanceId: any;

      //Obtener valor actual del ultimo balance de usuarios
      let getUltimoBalanceUsuarios = await this.balanceModel
        .find({
          tipo: catalogoTipoBalance.fondos_usuario,
          reparticionFondos: false,
        })
        .sort('-fechaCreacion')
        .limit(1);

      if (getUltimoBalanceUsuarios.length > 0) {
        getValorActualUltimoBalance = getUltimoBalanceUsuarios[0].valorActual;
        getUltimoBalanceId = getUltimoBalanceUsuarios[0]._id;
        await this.balanceModel.updateOne(
          { _id: getUltimoBalanceId },
          {
            $set: { reparticionFondos: true },
            fechaActualizacion: new Date()
          },
          opts
        );
      } else {
        return false;
      }

      let getCatalogoPorcentajeProyecto: any;

      //Obtiene el catalogo de porcentajes segun los tipos de proyecto en orden de prioridad
      getCatalogoPorcentajeProyecto = await this.catalogoPorcentajeProyectoService.obtenerCatalogoPorcentajeProyecto();

      console.log(
        'Separa el 7% para fondos de que se utilizaran en proyectos como fiscalizador etc.',
      );
      const getPorcentajeFondosReservados = await this.catalogoPorcentajeFondosReservadosModel.find(
        { estado: estadosCatalogoPorcentajeFondosReservados.activa },
      );
      console.log('getPorcentajeFondosReservados----------------->', getPorcentajeFondosReservados[0])
      const porcentajeFondosReservados = getPorcentajeFondosReservados[0].porcentaje;

      //valor obtenido de la FORMULA x porcentaje
      const getForm = await this.formulaEventoService.obtenerFormulaEvento(
        codigosFormulasEvento.monto_por_porcentaje,
      );
      //Remmplaza el MONTO Y EL PROCENTAJE
      let formulaMontoPorc = getForm.replace(
        'MONTO',
        getValorActualUltimoBalance,
      );
      formulaMontoPorc = formulaMontoPorc.replace(
        'PORCENTAJE',
        porcentajeFondosReservados,
      );
      //Fondos segun el porcentaje de fondos reservados
      const getMontoFondosReserva = eval(formulaMontoPorc);

      //Objeto de beneficiario
      const beneficiarioDto = {
        tipo: beneficiario.proyecto,
        usuario: null,
        proyecto: null,
        estado: codigosEstadosBeneficiario.activa,
      };

      //Se guarda el beneficiario
      let crearBeneficiario = await new this.beneficiarioModel(
        beneficiarioDto,
      ).save(opts);

      //datos para guardar el historico
      const newHistoricoBeneficiario: any = {
        datos: crearBeneficiario,
        usuario: '',
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadBeneficiario,
        tipo: codigosCatalogo.reparticionFondos,
      };

      //Crear historico del beneficiario
      this.crearHistoricoService.crearHistoricoServer(newHistoricoBeneficiario);

      //Objeto de la transaccion
      const transaccionDto = {
        estado: estadosTransaccion.activa,
        monto: getMontoFondosReserva,
        moneda: listaCodigosMonedas.USD,
        descripcion:
          descripcionTransPagosGazelook.fondosReservadosFiscalizacionProyectos,
        origen: catalogoOrigen.fondos_reservados_fiscalizacion_proyectos,
        balance: [],
        beneficiario: crearBeneficiario._id,
        metodoPago: null,
        informacionPago: null,
        usuario: null,
        conversionTransaccion: [],
        destino: catalogoOrigen.fondos_reservados_fiscalizacion_proyectos,
        numeroRecibo: null,
        comisionTransferencia: null,
        totalRecibido: null,
        origenPais: null,
      };

      //Crea la transaccion de tipo fondos reservados
      let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
        transaccionDto,
        opts,
      );

      const porcentajeReparticionFodosProyectos =
        1 - porcentajeFondosReservados;
      //valor obtenido del monto x porcentaje
      const getFormReparticionFondos = await this.formulaEventoService.obtenerFormulaEvento(
        codigosFormulasEvento.monto_por_porcentaje,
      );
      //Remmplaza el MONTO Y EL PROCENTAJE
      let formulaRepartFondos = getFormReparticionFondos.replace(
        'MONTO',
        getValorActualUltimoBalance,
      );
      formulaRepartFondos = formulaRepartFondos.replace(
        'PORCENTAJE',
        porcentajeReparticionFodosProyectos,
      );
      //Fondos segun el porcentaje la reparticion de fondos de proyectos
      const valorReparticionFondosProyectos = eval(formulaRepartFondos);

      console.log('REPARTE FONDOS');
      let reparteFondos = await this.fondosTipoProyecto(
        getCatalogoPorcentajeProyecto,
        valorReparticionFondosProyectos,
        getUltimoBalanceId,
        idEvento,
        opts,
      );
      console.log('retornaaa y finaliza finallll!');
      return reparteFondos;
    } catch (error) {
      throw error;
    }
  }

  //reparte los fondos a los proyectos mas votados por el tipo de proyecto
  async fondosTipoProyecto(
    getCatalogoPorcentajeProyecto,
    getValorActualUltimoBalance,
    getUltimoBalanceId,
    idEvento,
    opts,
  ): Promise<any> {
    try {
      let getCodigoCatalogoPorcentajeProyecto: any;
      let getTipoProyecto: any;
      let getFormulaEvento: any;
      let getFondosPreEstrategia: any;
      let getFondosEsperaFondos: any;
      let crearFondosFinanciacion: any;
      let crearFondosFinanciacion2: any;
      let getCodFormula: any;
      let getFondosTipoProyecto: any;
      let prioridadTipoProyecto: any;

      if (getCatalogoPorcentajeProyecto.length > 0) {
        let numTipoProyectos = getCatalogoPorcentajeProyecto.length;

        for (const catalogoPorcentajeProyecto of getCatalogoPorcentajeProyecto) {
          getTipoProyecto = catalogoPorcentajeProyecto.tipo

          getCodigoCatalogoPorcentajeProyecto = catalogoPorcentajeProyecto.codigo
          prioridadTipoProyecto = catalogoPorcentajeProyecto.prioridad

          //********************************************************/
          console.log('TIPO DE PROYECTO -> ', getTipoProyecto);
          let crearFondosTipoProyecto: any;

          //Obtengo la configuracion del evento
          const getConfEvento = await this.configuracionEventoService.obtenerConfiguracionEvento(
            codigosConfiguracionEvento.montoPorPorcentaje,
          );

          if (getConfEvento) {
            if (getConfEvento.formulas.length > 0) {
              for (const getFormula of getConfEvento.formulas) {
                getCodFormula = getFormula;

                //FORMULA1. Obteniendo Fórmula para obtener Ultimo balance para el numero de tipos de proyectos
                if (
                  getCodFormula === codigosFormulasEvento.fondos_disponibles_tipo_proyecto
                ) {
                  //Obtengo la formula del evento
                  getFormulaEvento = await this.formulaEventoService.obtenerFormulaEvento(
                    getCodFormula,
                  );
                  let formula;

                  //Reemplaza el VALOR DEL ULTIMO BALANCE Y EL NUMERO DE TIPO DE PROYECTOS
                  formula = getFormulaEvento.replace(
                    'VALOR',
                    getValorActualUltimoBalance,
                  );
                  formula = formula.replace(
                    'NUM_TIPO_PROYECTOS',
                    numTipoProyectos,
                  );
                  //Fondos segun el tipo de proyecto
                  getFondosTipoProyecto = eval(formula);

                  //Objecto de fondos tipo proyecto
                  const fondosTipoProyectoDto = {
                    catalogoPorcentajeProyecto: getCodigoCatalogoPorcentajeProyecto,
                    monto: getFondosTipoProyecto,
                    balance: getUltimoBalanceId || null,
                    estado: codigosEstadosFondosTipoProyecto.activa,
                  };
                  console.log('fondosTipoProyectoDto: ', fondosTipoProyectoDto);
                  //Se guarda los fondos tipo proyecto
                  crearFondosTipoProyecto = await new this.fondosTipoProyectoModel(
                    fondosTipoProyectoDto,
                  ).save(opts);

                  //datos para guardar el historico de los fondos tipo proyecto
                  const newHistoricoFondosTipoProyecto: any = {
                    datos: crearFondosTipoProyecto,
                    usuario: '',
                    accion: codigosCatalogoAcciones.crear,
                    entidad: codigoEntidades.fondosTipoProyecto,
                    tipo: codigosCatalogo.reparticionFondos,
                  };

                  //Crear historico para los fondos tipo proyecto
                  this.crearHistoricoService.crearHistoricoServer(
                    newHistoricoFondosTipoProyecto,
                  );
                }

                //FORMULA1. Obteniendo Fórmula para obtener monto por porcentaje
                if (
                  getCodFormula === codigosFormulasEvento.monto_por_porcentaje
                ) {
                  //Obtiene el catalogo porcentaje financiacion segun la prioridad que se envie
                  let getporcentajeFinanciacionPreEstrategia = await this.catalogoPorcentajeFinanciacionService.obtenerCatalogoPorcentajeFinanciacion(
                    1,
                  );
                  let porcentajePreEstrategia: any;
                  let codigoCatalogoPorcentajeFinanciacion: any;
                  // let porcentajeEsperaDeFondos:any;
                  if (getporcentajeFinanciacionPreEstrategia) {
                    porcentajePreEstrategia =
                      getporcentajeFinanciacionPreEstrategia.porcentaje;
                    codigoCatalogoPorcentajeFinanciacion =
                      getporcentajeFinanciacionPreEstrategia.codigo;
                  }
                  //Obtengo la formula del evento
                  //FORMULA2. valor obtenido de la FORMULA1 x porcentaje
                  getFormulaEvento = await this.formulaEventoService.obtenerFormulaEvento(
                    getCodFormula,
                  );
                  //Remmplaza el MONTO Y EL PROCENTAJE
                  let formulaPorcentaje1 = getFormulaEvento.replace(
                    'MONTO',
                    getFondosTipoProyecto,
                  );
                  formulaPorcentaje1 = formulaPorcentaje1.replace(
                    'PORCENTAJE',
                    porcentajePreEstrategia,
                  );
                  //Fondos segun el porcentaje de preEstrategia
                  getFondosPreEstrategia = eval(formulaPorcentaje1);

                  //Objeto de fondos financiacion
                  const fondosFinanciacion1Dto = {
                    catalogoPorcentajeFinanciacion: codigoCatalogoPorcentajeFinanciacion,
                    monto: getFondosPreEstrategia,
                    fondosTipoProyecto: crearFondosTipoProyecto._id,
                    estado: codigosEstadosFondosFinanciacion.activa,
                  };
                  console.log('fondosPreEstrategia: ', fondosFinanciacion1Dto);
                  //Se guarda los fondos de financiacion
                  crearFondosFinanciacion = await new this.fondosFinanciamientoModel(
                    fondosFinanciacion1Dto,
                  ).save(opts);

                  //datos para guardar el historico de los fondos financiacion
                  const newHistoricoFondosFinanciacion: any = {
                    datos: crearFondosFinanciacion,
                    usuario: '',
                    accion: codigosCatalogoAcciones.crear,
                    entidad: codigoEntidades.fondosFinanciamiento,
                    tipo: codigosCatalogo.reparticionFondos,
                  };

                  //Crear historico para los fondos financiacion de pre-estrategia
                  this.crearHistoricoService.crearHistoricoServer(
                    newHistoricoFondosFinanciacion,
                  );

                  //Obtiene el catalogo porcentaje financiacion segun la prioridad que se envie
                  let getporcentajeFinanciacionEsperaFondos = await this.catalogoPorcentajeFinanciacionService.obtenerCatalogoPorcentajeFinanciacion(
                    2,
                  );
                  let porcentajeEsperaFondos: any;
                  let codigoCatalogoPorcentajeFinanciacionEsperaFondo: any;
                  // let porcentajeEsperaDeFondos:any;
                  if (getporcentajeFinanciacionEsperaFondos) {
                    porcentajeEsperaFondos =
                      getporcentajeFinanciacionEsperaFondos.porcentaje;
                    codigoCatalogoPorcentajeFinanciacionEsperaFondo =
                      getporcentajeFinanciacionEsperaFondos.codigo;
                  }
                  //Obtengo la formula del evento
                  //FORMULA3. valor obtenido de la FORMULA1 x porcentaje
                  getFormulaEvento = await this.formulaEventoService.obtenerFormulaEvento(
                    getCodFormula,
                  );

                  //Reemplaza el MONTO Y EL PORCENTAJE
                  let formulaPorcentaje2 = getFormulaEvento.replace(
                    'MONTO',
                    getFondosTipoProyecto,
                  );
                  formulaPorcentaje2 = formulaPorcentaje2.replace(
                    'PORCENTAJE',
                    porcentajeEsperaFondos,
                  );
                  //Fondos segun el porcentaje de espera de fondos
                  getFondosEsperaFondos = eval(formulaPorcentaje2);

                  //Objeto de fondos financiacion
                  const fondosFinanciacion2Dto = {
                    catalogoPorcentajeFinanciacion: codigoCatalogoPorcentajeFinanciacionEsperaFondo,
                    monto: getFondosEsperaFondos,
                    fondosTipoProyecto: crearFondosTipoProyecto._id,
                    estado: codigosEstadosFondosFinanciacion.activa,
                  };
                  console.log('fondosEsperaFondos: ', fondosFinanciacion2Dto);
                  //Se guarda los fondos de financiacion
                  crearFondosFinanciacion2 = await new this.fondosFinanciamientoModel(
                    fondosFinanciacion2Dto,
                  ).save(opts);

                  //datos para guardar el historico de los fondos financiacion
                  const newHistoricoFondosFinanciacionEsperaFondos: any = {
                    datos: crearFondosFinanciacion2,
                    usuario: '',
                    accion: codigosCatalogoAcciones.crear,
                    entidad: codigoEntidades.fondosFinanciamiento,
                    tipo: codigosCatalogo.reparticionFondos,
                  };

                  //Crear historico para los fondos financiacion de espera de fondos
                  this.crearHistoricoService.crearHistoricoServer(
                    newHistoricoFondosFinanciacionEsperaFondos,
                  );
                }
              }
            }
          }
        }
        let reparteFondos = await this.repartirSegunTipoProyecto(
          getCatalogoPorcentajeProyecto,
          idEvento,
          opts,
        );
        console.log('retornaaa y finaliza!');
        return reparteFondos;
      }
    } catch (error) {
      throw error;
    }
  }

  //reparte los fondos a los proyectos mas votados por el tipo de proyecto
  async repartirSegunTipoProyecto(
    getCatalogoPorcentajeProyecto,
    idEvento,
    opts,
  ): Promise<any> {
    try {
      let getTipoProyecto: any;
      let prioridadTipoProyecto: any;
      let getCodigoCatalogoPorcentajeProyecto: any;
      let montoSobrante = 0
      //////////////////////////////////////////////////////////////////////////////
      if (getCatalogoPorcentajeProyecto.length > 0) {
        for (const catalogoPorcentajeProyecto of getCatalogoPorcentajeProyecto) {
          getTipoProyecto = catalogoPorcentajeProyecto.tipo
          getCodigoCatalogoPorcentajeProyecto = catalogoPorcentajeProyecto.codigo
          prioridadTipoProyecto = catalogoPorcentajeProyecto.prioridad

          let getProyectosMasVotados: any;

          let getFondosTipoProyecto = await this.fondosTipoProyectoService.obtenerFondosTipoProyecto(
            getCodigoCatalogoPorcentajeProyecto,
            opts,
          );
          let idFondosTipoProyecto = getFondosTipoProyecto._id;

          //Fondos finaciamiento preEstrategia
          let getFondosFinanciamientoPreEstrategia = await this.fondosFinanciamientoProyectoService.obtenerFondosFinanciamientoProyecto(
            idFondosTipoProyecto,
            codigosCatalogoPorcentajeFinanciacion.preEstrategia,
            opts,
          );

          let get80PorcMontoSobrante = 0
          let get20PorcMontoSobrante = 0
          //Si el monto sobrante es mayor a cero
          if (montoSobrante > 0) {

            //******** OBTIENE EL 80% DEL MONTO SOBRANTE **************

            //Obtiene el catalogo porcentaje financiacion segun la prioridad que se envie
            let getporcentajeFinanciacionPreEstrategia = await this.catalogoPorcentajeFinanciacionService.obtenerCatalogoPorcentajeFinanciacion(
              1,
            );
            let porcentajePreEstrategia: any;
            let codigoCatalogoPorcentajeFinanciacion: any;
            // let porcentajeEsperaDeFondos:any;
            if (getporcentajeFinanciacionPreEstrategia) {
              porcentajePreEstrategia =
                getporcentajeFinanciacionPreEstrategia.porcentaje;
              codigoCatalogoPorcentajeFinanciacion =
                getporcentajeFinanciacionPreEstrategia.codigo;
            }
            //Obtengo la formula del evento
            //FORMULA2. valor obtenido de la FORMULA1 x porcentaje
            let getFormulaEvento = await this.formulaEventoService.obtenerFormulaEvento(
              codigosFormulasEvento.monto_por_porcentaje,
            );
            //Remmplaza el MONTO Y EL PROCENTAJE
            let formulaPorcentaje1 = getFormulaEvento.replace(
              'MONTO',
              montoSobrante,
            );
            formulaPorcentaje1 = formulaPorcentaje1.replace(
              'PORCENTAJE',
              porcentajePreEstrategia,
            );
            //Fondos segun el porcentaje de preEstrategia
            get80PorcMontoSobrante = eval(formulaPorcentaje1);

            //******** OBTIENE EL 20% DEL MONTO SOBRANTE **************

            //Obtiene el catalogo porcentaje financiacion segun la prioridad que se envie
            let getporcentajeFinanciacionEsperaFondos = await this.catalogoPorcentajeFinanciacionService.obtenerCatalogoPorcentajeFinanciacion(
              2,
            );
            let porcentajeEsperaFondos: any;
            let codigoCatalogoPorcentajeFinanciacionEsperaFondo: any;
            // let porcentajeEsperaDeFondos:any;
            if (getporcentajeFinanciacionEsperaFondos) {
              porcentajeEsperaFondos =
                getporcentajeFinanciacionEsperaFondos.porcentaje;
              codigoCatalogoPorcentajeFinanciacionEsperaFondo =
                getporcentajeFinanciacionEsperaFondos.codigo;
            }
            //Obtengo la formula del evento
            //FORMULA3. valor obtenido de la FORMULA1 x porcentaje
            getFormulaEvento = await this.formulaEventoService.obtenerFormulaEvento(
              codigosFormulasEvento.monto_por_porcentaje,
            );

            //Reemplaza el MONTO Y EL PORCENTAJE
            let formulaPorcentaje2 = getFormulaEvento.replace(
              'MONTO',
              montoSobrante,
            );
            formulaPorcentaje2 = formulaPorcentaje2.replace(
              'PORCENTAJE',
              porcentajeEsperaFondos,
            );
            //Fondos segun el porcentaje de espera de fondos
            get20PorcMontoSobrante = eval(formulaPorcentaje2);

          }
          let getFondosPreEstrategia =
            getFondosFinanciamientoPreEstrategia.monto + get80PorcMontoSobrante;
          let idFondosFinanciamientoPreEstrategia =
            getFondosFinanciamientoPreEstrategia._id;

          console.log(
            '*******************************************************************',
          );
          console.log('TIPO DE PROYECTO: ', getTipoProyecto);
          console.log(
            'getFondosPreEstrategia: ',
            getFondosPreEstrategia,
          );
          //Fondos finaciamiento Espera de fondos
          let getFondosFinanciamientoEsperaFondos = await this.fondosFinanciamientoProyectoService.obtenerFondosFinanciamientoProyecto(
            idFondosTipoProyecto,
            codigosCatalogoPorcentajeFinanciacion.esperaFondos,
            opts,
          );
          let getFondosEsperaFondos = getFondosFinanciamientoEsperaFondos.monto + get20PorcMontoSobrante;
          let idFondosFinanciamientoEsperaFondos =
            getFondosFinanciamientoEsperaFondos._id;
          console.log(
            '*******************************************************************',
          );
          console.log(
            'getFondosEsperaFondos: ',
            getFondosEsperaFondos,
          );

          //Obtiene los proyectos mas votados segun el tipo de proyecto
          getProyectosMasVotados = await this.proyectosMasVotadosService.obtenerProyectosMasVotados(
            getTipoProyecto,
            estadosProyecto.proyectoForo,
          );
          let numProyectos = getProyectosMasVotados.length;
          console.log('NUMERO DE PROYECTOS: ', numProyectos);

          //Verifica si existen proyectos
          if (getProyectosMasVotados.length > 0) {
            let proyectoRecorridos = 0
            let proyectoAnteriorFinanciado = true
            let ciclo = 1
            const limiteProyectos = 8

            for (const proyectosMasVotados of getProyectosMasVotados) {
              proyectoRecorridos = proyectoRecorridos + 1;
              let idProyecto = proyectosMasVotados._id;
              let valorEstimadoProyecto = proyectosMasVotados.valorEstimadoFinal;
              let estadoProyecto = proyectosMasVotados.estado;
              let getMonedaProyecto = proyectosMasVotados.moneda;

              //obtiene el 25% del valor estimado del proyecto
              let porct25ValorEstimado = valorEstimadoProyecto * 0.25


              if (getFondosPreEstrategia > 0) {
                if (proyectoAnteriorFinanciado) {
                  //Cubre el 25% de valor estimado del proyecto
                  if (getFondosPreEstrategia >= porct25ValorEstimado) {

                    //si cubre el 100% del valor estimado del proyecto
                    if (getFondosPreEstrategia >= valorEstimadoProyecto) {

                      //si se financia este proyecto
                      proyectoAnteriorFinanciado = true

                      //Obtengo la configuracion del evento de pre-estrategia
                      const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                        codigosConfiguracionEvento.preEstrategia,
                      );
                      if (getConfEventoPreEstrategia) {
                        if (getConfEventoPreEstrategia.formulas.length > 0) {
                          for (const getFormula of getConfEventoPreEstrategia.formulas) {
                            let getFormulaEventoPreEstrategia: any;

                            const getCodFormulaPreEstrategia = getFormula;
                            //Obtengo la formula del evento de pre-estrategia
                            getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                              getCodFormulaPreEstrategia,
                            );
                            let formulaPreEstrategia;

                            //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                            if (
                              getCodFormulaPreEstrategia === codigosFormulasEvento.pre_estrategia
                            ) {
                              //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                              formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                'TOTAL_MONTO',
                                getFondosPreEstrategia,
                              );
                              formulaPreEstrategia = formulaPreEstrategia.replace(
                                'VALOR_ESTIMADO',
                                valorEstimadoProyecto,
                              );
                              //Fondos segun el tipo de proyecto
                              getFondosPreEstrategia = eval(formulaPreEstrategia);

                              //Actualiza el estado del proyecto a pre estrategia
                              let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                idProyecto,
                                estadoProyecto,
                                estadosProyecto.proyectoPreEstrategia,
                                null,
                                null,
                                opts,
                              );
                              if (actualizaProyecto) {
                                //Objeto de beneficiario
                                const beneficiarioDto = {
                                  tipo: beneficiario.proyecto,
                                  usuario: null,
                                  proyecto: idProyecto,
                                  estado: codigosEstadosBeneficiario.activa,
                                };

                                //Se guarda el beneficiario
                                let crearBeneficiario = await new this.beneficiarioModel(
                                  beneficiarioDto,
                                ).save(opts);

                                //datos para guardar el historico
                                const newHistoricoBeneficiario: any = {
                                  datos: crearBeneficiario,
                                  usuario: '',
                                  accion: codigosCatalogoAcciones.crear,
                                  entidad: codigoEntidades.entidadBeneficiario,
                                  tipo: codigosCatalogo.reparticionFondos,
                                };

                                //Crear historico del beneficiario
                                this.crearHistoricoService.crearHistoricoServer(
                                  newHistoricoBeneficiario,
                                );

                                //Objeto de la transaccion
                                const transaccionDto = {
                                  estado: estadosTransaccion.activa,
                                  monto: valorEstimadoProyecto,
                                  moneda: listaCodigosMonedas.USD,
                                  descripcion:
                                    descripcionTransPagosGazelook.fondosReservados,
                                  origen: catalogoOrigen.fondos_reservados,
                                  destino: catalogoOrigen.fondos_reservados,
                                  balance: [],
                                  beneficiario: crearBeneficiario._id,
                                  metodoPago: null,
                                  informacionPago: null,
                                  usuario: null,
                                  conversionTransaccion: [],
                                  numeroRecibo: null,
                                  comisionTransferencia: null,
                                  totalRecibido: null,
                                  origenPais: null,
                                };
                                //Crea la transaccion de tipo fondos reservados
                                let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                  transaccionDto,
                                  opts,
                                );

                                //Objeto de la bonificacion
                                let objBonificacion: any = {
                                  proyecto: idProyecto,
                                  montoCubierto: null,
                                  montoFaltante: null,
                                  fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                  fondosReservados: crearTransaccion._id,
                                  transacciones: [],
                                  estado: codigosEstadosBonificacion.activa,
                                };

                                //Se guarda la bonificacion
                                let bonificacion = await new this.bonificacionModel(
                                  objBonificacion,
                                ).save(opts);

                                //datos para guardar el historico
                                const newHistoricoBonificacion: any = {
                                  datos: bonificacion,
                                  usuario: '',
                                  accion: codigosCatalogoAcciones.crear,
                                  entidad: codigoEntidades.entidadBonificacion,
                                  tipo: codigosCatalogo.reparticionFondos,
                                };

                                //Crear historico de la bonificacion
                                this.crearHistoricoService.crearHistoricoServer(
                                  newHistoricoBonificacion,
                                );

                                //Actualiza el array de proyectos en el evento
                                await this.eventoModel.updateOne(
                                  { _id: idEvento },
                                  {
                                    $push: { proyectos: idProyecto },
                                    estado: estadoEvento.ejecutado,
                                    fechaActualizacion: new Date(),
                                  },
                                  opts,
                                );

                                //Actualiza el evento en proyectos
                                await this.proyectoModel.updateOne(
                                  { _id: idProyecto },
                                  {
                                    evento: idEvento,
                                    fechaActualizacion: new Date(),
                                  },
                                  opts,
                                );
                              }
                            }
                          }
                        }
                      }
                    } else {

                      //si se financia este proyecto
                      proyectoAnteriorFinanciado = true


                      //proyectos en espera de fondos
                      let objProyectosEsperaFondos = {
                        _id: idProyecto,
                        valorEstimado: valorEstimadoProyecto,
                        moneda: getMonedaProyecto,
                        estado: estadoProyecto,
                      };

                      //Reparte los fondos de espera de fondos a los proyectos
                      let repartirFondos = await this.repartirEsperaFondos(
                        getFondosPreEstrategia,
                        codigosFormulasEvento.monto_por_porcentaje,
                        getFondosFinanciamientoEsperaFondos,
                        objProyectosEsperaFondos,
                        idEvento,
                        getTipoProyecto,
                        opts,
                        ciclo
                      );

                      getFondosPreEstrategia = repartirFondos

                    }
                  } else {


                    //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                    if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                      //se financia este proyecto
                      proyectoAnteriorFinanciado = false

                      //suma de monto de preestrategia mas la espera de fondos
                      // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                      //El sistema ubica al proyecto en estado de preseleccionado cuando 
                      //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                      //lugar de los más votados dentro de cada tipo de proyecto. 
                      let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                        idProyecto,
                        estadoProyecto,
                        estadosProyecto.preseleccionado,
                        null,
                        null,
                        opts
                      );

                    } else {
                      getFondosEsperaFondos = getFondosEsperaFondos + getFondosPreEstrategia
                      getFondosPreEstrategia = 0
                      //Si se existen fondos en el monto de espera de fondos
                      if (getFondosEsperaFondos > 0) {
                        if (proyectoAnteriorFinanciado) {
                          //Cubre el 25% de valor estimado del proyecto
                          if (getFondosEsperaFondos >= porct25ValorEstimado) {

                            //si cubre el 100% del valor estimado del proyecto
                            if (getFondosEsperaFondos >= valorEstimadoProyecto) {

                              //si se financia este proyecto
                              proyectoAnteriorFinanciado = true

                              //Obtengo la configuracion del evento de pre-estrategia
                              const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                                codigosConfiguracionEvento.preEstrategia,
                              );
                              if (getConfEventoPreEstrategia) {
                                if (getConfEventoPreEstrategia.formulas.length > 0) {
                                  for (const getFormula of getConfEventoPreEstrategia.formulas) {
                                    let getFormulaEventoPreEstrategia: any;

                                    const getCodFormulaPreEstrategia = getFormula;
                                    //Obtengo la formula del evento de pre-estrategia
                                    getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                                      getCodFormulaPreEstrategia,
                                    );
                                    let formulaPreEstrategia;

                                    //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                                    if (
                                      getCodFormulaPreEstrategia ===
                                      codigosFormulasEvento.pre_estrategia
                                    ) {
                                      //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                                      formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                        'TOTAL_MONTO',
                                        getFondosEsperaFondos,
                                      );
                                      formulaPreEstrategia = formulaPreEstrategia.replace(
                                        'VALOR_ESTIMADO',
                                        valorEstimadoProyecto,
                                      );
                                      //Fondos segun el tipo de proyecto
                                      getFondosEsperaFondos = eval(formulaPreEstrategia);

                                      //Actualiza el estado del proyecto a pre estrategia
                                      let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                        idProyecto,
                                        estadoProyecto,
                                        estadosProyecto.proyectoPreEstrategia,
                                        null,
                                        null,
                                        opts,
                                      );
                                      if (actualizaProyecto) {
                                        //Objeto de beneficiario
                                        const beneficiarioDto = {
                                          tipo: beneficiario.proyecto,
                                          usuario: null,
                                          proyecto: idProyecto,
                                          estado: codigosEstadosBeneficiario.activa,
                                        };

                                        //Se guarda el beneficiario
                                        let crearBeneficiario = await new this.beneficiarioModel(
                                          beneficiarioDto,
                                        ).save(opts);

                                        //datos para guardar el historico
                                        const newHistoricoBeneficiario: any = {
                                          datos: crearBeneficiario,
                                          usuario: '',
                                          accion: codigosCatalogoAcciones.crear,
                                          entidad: codigoEntidades.entidadBeneficiario,
                                          tipo: codigosCatalogo.reparticionFondos,
                                        };

                                        //Crear historico del beneficiario
                                        this.crearHistoricoService.crearHistoricoServer(
                                          newHistoricoBeneficiario,
                                        );

                                        //Objeto de la transaccion
                                        const transaccionDto = {
                                          estado: estadosTransaccion.activa,
                                          monto: valorEstimadoProyecto,
                                          moneda: listaCodigosMonedas.USD,
                                          descripcion:
                                            descripcionTransPagosGazelook.fondosReservados,
                                          origen: catalogoOrigen.fondos_reservados,
                                          balance: [],
                                          beneficiario: crearBeneficiario._id,
                                          metodoPago: null,
                                          informacionPago: null,
                                          usuario: null,
                                          conversionTransaccion: [],
                                          destino: null,
                                          numeroRecibo: null,
                                          comisionTransferencia: null,
                                          totalRecibido: null,
                                          origenPais: null,
                                        };
                                        //Crea la transaccion de tipo fondos reservados
                                        let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                          transaccionDto,
                                          opts,
                                        );

                                        //Objeto de la bonificacion
                                        let objBonificacion: any = {
                                          proyecto: idProyecto,
                                          montoCubierto: null,
                                          montoFaltante: null,
                                          fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                          fondosReservados: crearTransaccion._id,
                                          transacciones: [],
                                          estado: codigosEstadosBonificacion.activa,
                                        };

                                        //Se guarda la bonificacion
                                        let bonificacion = await new this.bonificacionModel(
                                          objBonificacion,
                                        ).save(opts);

                                        //datos para guardar el historico
                                        const newHistoricoBonificacion: any = {
                                          datos: bonificacion,
                                          usuario: '',
                                          accion: codigosCatalogoAcciones.crear,
                                          entidad: codigoEntidades.entidadBonificacion,
                                          tipo: codigosCatalogo.reparticionFondos,
                                        };

                                        //Crear historico de la bonificacion
                                        this.crearHistoricoService.crearHistoricoServer(
                                          newHistoricoBonificacion,
                                        );

                                        //Actualiza el array de proyectos en el evento
                                        await this.eventoModel.updateOne(
                                          { _id: idEvento },
                                          {
                                            $push: { proyectos: idProyecto },
                                            estado: estadoEvento.ejecutado,
                                            fechaActualizacion: new Date(),
                                          },
                                          opts,
                                        );

                                        //Actualiza el evento en proyectos
                                        await this.proyectoModel.updateOne(
                                          { _id: idProyecto },
                                          {
                                            evento: idEvento,
                                            fechaActualizacion: new Date(),
                                          },
                                          opts,
                                        );
                                      }
                                    }
                                  }
                                }
                              }
                            } else {

                              //si se financia este proyecto
                              proyectoAnteriorFinanciado = true


                              //proyectos en espera de fondos
                              let objProyectosEsperaFondos = {
                                _id: idProyecto,
                                valorEstimado: valorEstimadoProyecto,
                                moneda: getMonedaProyecto,
                                estado: estadoProyecto,
                              };

                              //Reparte los fondos de espera de fondos a los proyectos
                              let repartirFondos = await this.repartirEsperaFondos(
                                getFondosEsperaFondos,
                                codigosFormulasEvento.monto_por_porcentaje,
                                getFondosFinanciamientoEsperaFondos,
                                objProyectosEsperaFondos,
                                idEvento,
                                getTipoProyecto,
                                opts,
                                ciclo
                              );

                              getFondosEsperaFondos = repartirFondos

                            }
                          } else {
                            //no se financia este proyecto
                            proyectoAnteriorFinanciado = false

                            //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                            if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                              //suma de monto de preestrategia mas la espera de fondos
                              // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                              //El sistema ubica al proyecto en estado de preseleccionado cuando 
                              //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                              //lugar de los más votados dentro de cada tipo de proyecto. 
                              let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                idProyecto,
                                estadoProyecto,
                                estadosProyecto.preseleccionado,
                                null,
                                null,
                                opts
                              );

                            } else {
                              //no se financia este proyecto
                              proyectoAnteriorFinanciado = false

                              //Actualiza el estado del proyecto a activo
                              let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                idProyecto,
                                estadoProyecto,
                                estadosProyecto.proyectoActivo,
                                null,
                                null,
                                opts,
                              );

                            }
                          }
                        } else {
                          if (proyectoRecorridos <= limiteProyectos) {
                            //Cubre el 25% de valor estimado del proyecto
                            if (getFondosEsperaFondos >= porct25ValorEstimado) {

                              //si cubre el 100% del valor estimado del proyecto
                              if (getFondosEsperaFondos >= valorEstimadoProyecto) {

                                //si se financia este proyecto
                                proyectoAnteriorFinanciado = true

                                //Obtengo la configuracion del evento de pre-estrategia
                                const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                                  codigosConfiguracionEvento.preEstrategia,
                                );
                                if (getConfEventoPreEstrategia) {
                                  if (getConfEventoPreEstrategia.formulas.length > 0) {
                                    for (const getFormula of getConfEventoPreEstrategia.formulas) {
                                      let getFormulaEventoPreEstrategia: any;

                                      const getCodFormulaPreEstrategia = getFormula;
                                      //Obtengo la formula del evento de pre-estrategia
                                      getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                                        getCodFormulaPreEstrategia,
                                      );
                                      let formulaPreEstrategia;

                                      //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                                      if (
                                        getCodFormulaPreEstrategia ===
                                        codigosFormulasEvento.pre_estrategia
                                      ) {
                                        //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                                        formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                          'TOTAL_MONTO',
                                          getFondosEsperaFondos,
                                        );
                                        formulaPreEstrategia = formulaPreEstrategia.replace(
                                          'VALOR_ESTIMADO',
                                          valorEstimadoProyecto,
                                        );
                                        //Fondos segun el tipo de proyecto
                                        getFondosEsperaFondos = eval(formulaPreEstrategia);

                                        //Actualiza el estado del proyecto a pre estrategia
                                        let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                          idProyecto,
                                          estadoProyecto,
                                          estadosProyecto.proyectoPreEstrategia,
                                          null,
                                          null,
                                          opts,
                                        );
                                        if (actualizaProyecto) {
                                          //Objeto de beneficiario
                                          const beneficiarioDto = {
                                            tipo: beneficiario.proyecto,
                                            usuario: null,
                                            proyecto: idProyecto,
                                            estado: codigosEstadosBeneficiario.activa,
                                          };

                                          //Se guarda el beneficiario
                                          let crearBeneficiario = await new this.beneficiarioModel(
                                            beneficiarioDto,
                                          ).save(opts);

                                          //datos para guardar el historico
                                          const newHistoricoBeneficiario: any = {
                                            datos: crearBeneficiario,
                                            usuario: '',
                                            accion: codigosCatalogoAcciones.crear,
                                            entidad: codigoEntidades.entidadBeneficiario,
                                            tipo: codigosCatalogo.reparticionFondos,
                                          };

                                          //Crear historico del beneficiario
                                          this.crearHistoricoService.crearHistoricoServer(
                                            newHistoricoBeneficiario,
                                          );

                                          //Objeto de la transaccion
                                          const transaccionDto = {
                                            estado: estadosTransaccion.activa,
                                            monto: valorEstimadoProyecto,
                                            moneda: listaCodigosMonedas.USD,
                                            descripcion:
                                              descripcionTransPagosGazelook.fondosReservados,
                                            origen: catalogoOrigen.fondos_reservados,
                                            balance: [],
                                            beneficiario: crearBeneficiario._id,
                                            metodoPago: null,
                                            informacionPago: null,
                                            usuario: null,
                                            conversionTransaccion: [],
                                            destino: null,
                                            numeroRecibo: null,
                                            comisionTransferencia: null,
                                            totalRecibido: null,
                                            origenPais: null,
                                          };
                                          //Crea la transaccion de tipo fondos reservados
                                          let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                            transaccionDto,
                                            opts,
                                          );

                                          //Objeto de la bonificacion
                                          let objBonificacion: any = {
                                            proyecto: idProyecto,
                                            montoCubierto: null,
                                            montoFaltante: null,
                                            fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                            fondosReservados: crearTransaccion._id,
                                            transacciones: [],
                                            estado: codigosEstadosBonificacion.activa,
                                          };

                                          //Se guarda la bonificacion
                                          let bonificacion = await new this.bonificacionModel(
                                            objBonificacion,
                                          ).save(opts);

                                          //datos para guardar el historico
                                          const newHistoricoBonificacion: any = {
                                            datos: bonificacion,
                                            usuario: '',
                                            accion: codigosCatalogoAcciones.crear,
                                            entidad: codigoEntidades.entidadBonificacion,
                                            tipo: codigosCatalogo.reparticionFondos,
                                          };

                                          //Crear historico de la bonificacion
                                          this.crearHistoricoService.crearHistoricoServer(
                                            newHistoricoBonificacion,
                                          );

                                          //Actualiza el array de proyectos en el evento
                                          await this.eventoModel.updateOne(
                                            { _id: idEvento },
                                            {
                                              $push: { proyectos: idProyecto },
                                              estado: estadoEvento.ejecutado,
                                              fechaActualizacion: new Date(),
                                            },
                                            opts,
                                          );

                                          //Actualiza el evento en proyectos
                                          await this.proyectoModel.updateOne(
                                            { _id: idProyecto },
                                            {
                                              evento: idEvento,
                                              fechaActualizacion: new Date(),
                                            },
                                            opts,
                                          );
                                        }
                                      }
                                    }
                                  }
                                }
                              } else {

                                //si se financia este proyecto
                                proyectoAnteriorFinanciado = true


                                //proyectos en espera de fondos
                                let objProyectosEsperaFondos = {
                                  _id: idProyecto,
                                  valorEstimado: valorEstimadoProyecto,
                                  moneda: getMonedaProyecto,
                                  estado: estadoProyecto,
                                };

                                //Reparte los fondos de espera de fondos a los proyectos
                                let repartirFondos = await this.repartirEsperaFondos(
                                  getFondosEsperaFondos,
                                  getFondosFinanciamientoEsperaFondos,
                                  codigosFormulasEvento.monto_por_porcentaje,
                                  objProyectosEsperaFondos,
                                  idEvento,
                                  getTipoProyecto,
                                  opts,
                                  ciclo
                                );

                                getFondosEsperaFondos = repartirFondos
                                ciclo = 2

                              }
                            } else {
                              //no se financia este proyecto
                              proyectoAnteriorFinanciado = false

                              //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                              if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                                //suma de monto de preestrategia mas la espera de fondos
                                // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                                //El sistema ubica al proyecto en estado de preseleccionado cuando 
                                //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                                //lugar de los más votados dentro de cada tipo de proyecto. 
                                let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                  idProyecto,
                                  estadoProyecto,
                                  estadosProyecto.preseleccionado,
                                  null,
                                  null,
                                  opts
                                );

                              } else {
                                //no se financia este proyecto
                                proyectoAnteriorFinanciado = false

                                //Actualiza el estado del proyecto a activo
                                let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                  idProyecto,
                                  estadoProyecto,
                                  estadosProyecto.proyectoActivo,
                                  null,
                                  null,
                                  opts,
                                );

                              }
                            }

                          }
                        }
                      }
                    }
                  }
                } else {
                  if (proyectoRecorridos <= limiteProyectos) {
                    if (getFondosPreEstrategia > 0) {
                      //Cubre el 25% de valor estimado del proyecto
                      if (getFondosPreEstrategia >= porct25ValorEstimado) {

                        //si cubre el 100% del valor estimado del proyecto
                        if (getFondosPreEstrategia >= valorEstimadoProyecto) {

                          //si se financia este proyecto
                          proyectoAnteriorFinanciado = true

                          //Obtengo la configuracion del evento de pre-estrategia
                          const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                            codigosConfiguracionEvento.preEstrategia,
                          );
                          if (getConfEventoPreEstrategia) {
                            if (getConfEventoPreEstrategia.formulas.length > 0) {
                              for (const getFormula of getConfEventoPreEstrategia.formulas) {
                                let getFormulaEventoPreEstrategia: any;

                                const getCodFormulaPreEstrategia = getFormula;
                                //Obtengo la formula del evento de pre-estrategia
                                getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                                  getCodFormulaPreEstrategia,
                                );
                                let formulaPreEstrategia;

                                //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                                if (
                                  getCodFormulaPreEstrategia ===
                                  codigosFormulasEvento.pre_estrategia
                                ) {
                                  //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                                  formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                    'TOTAL_MONTO',
                                    getFondosPreEstrategia,
                                  );
                                  formulaPreEstrategia = formulaPreEstrategia.replace(
                                    'VALOR_ESTIMADO',
                                    valorEstimadoProyecto,
                                  );
                                  //Fondos segun el tipo de proyecto
                                  getFondosPreEstrategia = eval(formulaPreEstrategia);

                                  //Actualiza el estado del proyecto a pre estrategia
                                  let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                    idProyecto,
                                    estadoProyecto,
                                    estadosProyecto.proyectoPreEstrategia,
                                    null,
                                    null,
                                    opts,
                                  );
                                  if (actualizaProyecto) {
                                    //Objeto de beneficiario
                                    const beneficiarioDto = {
                                      tipo: beneficiario.proyecto,
                                      usuario: null,
                                      proyecto: idProyecto,
                                      estado: codigosEstadosBeneficiario.activa,
                                    };

                                    //Se guarda el beneficiario
                                    let crearBeneficiario = await new this.beneficiarioModel(
                                      beneficiarioDto,
                                    ).save(opts);

                                    //datos para guardar el historico
                                    const newHistoricoBeneficiario: any = {
                                      datos: crearBeneficiario,
                                      usuario: '',
                                      accion: codigosCatalogoAcciones.crear,
                                      entidad: codigoEntidades.entidadBeneficiario,
                                      tipo: codigosCatalogo.reparticionFondos,
                                    };

                                    //Crear historico del beneficiario
                                    this.crearHistoricoService.crearHistoricoServer(
                                      newHistoricoBeneficiario,
                                    );

                                    //Objeto de la transaccion
                                    const transaccionDto = {
                                      estado: estadosTransaccion.activa,
                                      monto: valorEstimadoProyecto,
                                      moneda: listaCodigosMonedas.USD,
                                      descripcion:
                                        descripcionTransPagosGazelook.fondosReservados,
                                      origen: catalogoOrigen.fondos_reservados,
                                      balance: [],
                                      beneficiario: crearBeneficiario._id,
                                      metodoPago: null,
                                      informacionPago: null,
                                      usuario: null,
                                      conversionTransaccion: [],
                                      destino: null,
                                      numeroRecibo: null,
                                      comisionTransferencia: null,
                                      totalRecibido: null,
                                      origenPais: null,
                                    };
                                    //Crea la transaccion de tipo fondos reservados
                                    let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                      transaccionDto,
                                      opts,
                                    );

                                    //Objeto de la bonificacion
                                    let objBonificacion: any = {
                                      proyecto: idProyecto,
                                      montoCubierto: null,
                                      montoFaltante: null,
                                      fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                      fondosReservados: crearTransaccion._id,
                                      transacciones: [],
                                      estado: codigosEstadosBonificacion.activa,
                                    };

                                    //Se guarda la bonificacion
                                    let bonificacion = await new this.bonificacionModel(
                                      objBonificacion,
                                    ).save(opts);

                                    //datos para guardar el historico
                                    const newHistoricoBonificacion: any = {
                                      datos: bonificacion,
                                      usuario: '',
                                      accion: codigosCatalogoAcciones.crear,
                                      entidad: codigoEntidades.entidadBonificacion,
                                      tipo: codigosCatalogo.reparticionFondos,
                                    };

                                    //Crear historico de la bonificacion
                                    this.crearHistoricoService.crearHistoricoServer(
                                      newHistoricoBonificacion,
                                    );

                                    //Actualiza el array de proyectos en el evento
                                    await this.eventoModel.updateOne(
                                      { _id: idEvento },
                                      {
                                        $push: { proyectos: idProyecto },
                                        estado: estadoEvento.ejecutado,
                                        fechaActualizacion: new Date(),
                                      },
                                      opts,
                                    );

                                    //Actualiza el evento en proyectos
                                    await this.proyectoModel.updateOne(
                                      { _id: idProyecto },
                                      {
                                        evento: idEvento,
                                        fechaActualizacion: new Date(),
                                      },
                                      opts,
                                    );
                                  }
                                }
                              }
                            }
                          }
                        } else {

                          //si se financia este proyecto
                          // proyectoAnteriorFinanciado = true


                          //proyectos en espera de fondos
                          let objProyectosEsperaFondos = {
                            _id: idProyecto,
                            valorEstimado: valorEstimadoProyecto,
                            moneda: getMonedaProyecto,
                            estado: estadoProyecto,
                          };

                          //Reparte los fondos de espera de fondos a los proyectos
                          let repartirFondos = await this.repartirEsperaFondos(
                            getFondosPreEstrategia,
                            codigosFormulasEvento.monto_por_porcentaje,
                            getFondosFinanciamientoEsperaFondos,
                            objProyectosEsperaFondos,
                            idEvento,
                            getTipoProyecto,
                            opts,
                            ciclo
                          );

                          getFondosPreEstrategia = repartirFondos

                        }
                      } else {

                        proyectoAnteriorFinanciado = false
                        //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                        if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                          //se financia este proyecto


                          //suma de monto de preestrategia mas la espera de fondos
                          // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                          //El sistema ubica al proyecto en estado de preseleccionado cuando 
                          //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                          //lugar de los más votados dentro de cada tipo de proyecto. 
                          let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                            idProyecto,
                            estadoProyecto,
                            estadosProyecto.preseleccionado,
                            null,
                            null,
                            opts
                          );

                        } else {
                          getFondosEsperaFondos = getFondosEsperaFondos + getFondosPreEstrategia

                          //Si se existen fondos en el monto de espera de fondos
                          if (getFondosEsperaFondos > 0) {
                            if (proyectoAnteriorFinanciado) {
                              //Cubre el 25% de valor estimado del proyecto
                              if (getFondosEsperaFondos >= porct25ValorEstimado) {

                                //si cubre el 100% del valor estimado del proyecto
                                if (getFondosEsperaFondos >= valorEstimadoProyecto) {

                                  //si se financia este proyecto
                                  proyectoAnteriorFinanciado = true

                                  //Obtengo la configuracion del evento de pre-estrategia
                                  const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                                    codigosConfiguracionEvento.preEstrategia,
                                  );
                                  if (getConfEventoPreEstrategia) {
                                    if (getConfEventoPreEstrategia.formulas.length > 0) {
                                      for (const getFormula of getConfEventoPreEstrategia.formulas) {
                                        let getFormulaEventoPreEstrategia: any;

                                        const getCodFormulaPreEstrategia = getFormula;
                                        //Obtengo la formula del evento de pre-estrategia
                                        getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                                          getCodFormulaPreEstrategia,
                                        );
                                        let formulaPreEstrategia;

                                        //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                                        if (
                                          getCodFormulaPreEstrategia ===
                                          codigosFormulasEvento.pre_estrategia
                                        ) {
                                          //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                                          formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                            'TOTAL_MONTO',
                                            getFondosEsperaFondos,
                                          );
                                          formulaPreEstrategia = formulaPreEstrategia.replace(
                                            'VALOR_ESTIMADO',
                                            valorEstimadoProyecto,
                                          );
                                          //Fondos segun el tipo de proyecto
                                          getFondosEsperaFondos = eval(formulaPreEstrategia);

                                          //Actualiza el estado del proyecto a pre estrategia
                                          let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                            idProyecto,
                                            estadoProyecto,
                                            estadosProyecto.proyectoPreEstrategia,
                                            null,
                                            null,
                                            opts,
                                          );
                                          if (actualizaProyecto) {
                                            //Objeto de beneficiario
                                            const beneficiarioDto = {
                                              tipo: beneficiario.proyecto,
                                              usuario: null,
                                              proyecto: idProyecto,
                                              estado: codigosEstadosBeneficiario.activa,
                                            };

                                            //Se guarda el beneficiario
                                            let crearBeneficiario = await new this.beneficiarioModel(
                                              beneficiarioDto,
                                            ).save(opts);

                                            //datos para guardar el historico
                                            const newHistoricoBeneficiario: any = {
                                              datos: crearBeneficiario,
                                              usuario: '',
                                              accion: codigosCatalogoAcciones.crear,
                                              entidad: codigoEntidades.entidadBeneficiario,
                                              tipo: codigosCatalogo.reparticionFondos,
                                            };

                                            //Crear historico del beneficiario
                                            this.crearHistoricoService.crearHistoricoServer(
                                              newHistoricoBeneficiario,
                                            );

                                            //Objeto de la transaccion
                                            const transaccionDto = {
                                              estado: estadosTransaccion.activa,
                                              monto: valorEstimadoProyecto,
                                              moneda: listaCodigosMonedas.USD,
                                              descripcion:
                                                descripcionTransPagosGazelook.fondosReservados,
                                              origen: catalogoOrigen.fondos_reservados,
                                              balance: [],
                                              beneficiario: crearBeneficiario._id,
                                              metodoPago: null,
                                              informacionPago: null,
                                              usuario: null,
                                              conversionTransaccion: [],
                                              destino: null,
                                              numeroRecibo: null,
                                              comisionTransferencia: null,
                                              totalRecibido: null,
                                              origenPais: null,
                                            };
                                            //Crea la transaccion de tipo fondos reservados
                                            let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                              transaccionDto,
                                              opts,
                                            );

                                            //Objeto de la bonificacion
                                            let objBonificacion: any = {
                                              proyecto: idProyecto,
                                              montoCubierto: null,
                                              montoFaltante: null,
                                              fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                              fondosReservados: crearTransaccion._id,
                                              transacciones: [],
                                              estado: codigosEstadosBonificacion.activa,
                                            };

                                            //Se guarda la bonificacion
                                            let bonificacion = await new this.bonificacionModel(
                                              objBonificacion,
                                            ).save(opts);

                                            //datos para guardar el historico
                                            const newHistoricoBonificacion: any = {
                                              datos: bonificacion,
                                              usuario: '',
                                              accion: codigosCatalogoAcciones.crear,
                                              entidad: codigoEntidades.entidadBonificacion,
                                              tipo: codigosCatalogo.reparticionFondos,
                                            };

                                            //Crear historico de la bonificacion
                                            this.crearHistoricoService.crearHistoricoServer(
                                              newHistoricoBonificacion,
                                            );

                                            //Actualiza el array de proyectos en el evento
                                            await this.eventoModel.updateOne(
                                              { _id: idEvento },
                                              {
                                                $push: { proyectos: idProyecto },
                                                estado: estadoEvento.ejecutado,
                                                fechaActualizacion: new Date(),
                                              },
                                              opts,
                                            );

                                            //Actualiza el evento en proyectos
                                            await this.proyectoModel.updateOne(
                                              { _id: idProyecto },
                                              {
                                                evento: idEvento,
                                                fechaActualizacion: new Date(),
                                              },
                                              opts,
                                            );
                                          }
                                        }
                                      }
                                    }
                                  }
                                } else {

                                  //si se financia este proyecto
                                  proyectoAnteriorFinanciado = true


                                  //proyectos en espera de fondos
                                  let objProyectosEsperaFondos = {
                                    _id: idProyecto,
                                    valorEstimado: valorEstimadoProyecto,
                                    moneda: getMonedaProyecto,
                                    estado: estadoProyecto,
                                  };

                                  //Reparte los fondos de espera de fondos a los proyectos
                                  let repartirFondos = await this.repartirEsperaFondos(
                                    getFondosEsperaFondos,
                                    codigosFormulasEvento.monto_por_porcentaje,
                                    getFondosFinanciamientoEsperaFondos,
                                    objProyectosEsperaFondos,
                                    idEvento,
                                    getTipoProyecto,
                                    opts,
                                    ciclo
                                  );

                                  getFondosEsperaFondos = repartirFondos

                                }
                              } else {
                                //no se financia este proyecto
                                proyectoAnteriorFinanciado = false

                                //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                                if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                                  //suma de monto de preestrategia mas la espera de fondos
                                  // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                                  //El sistema ubica al proyecto en estado de preseleccionado cuando 
                                  //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                                  //lugar de los más votados dentro de cada tipo de proyecto. 
                                  let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                    idProyecto,
                                    estadoProyecto,
                                    estadosProyecto.preseleccionado,
                                    null,
                                    null,
                                    opts
                                  );

                                } else {
                                  //no se financia este proyecto
                                  proyectoAnteriorFinanciado = false

                                  //Actualiza el estado del proyecto a activo
                                  let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                    idProyecto,
                                    estadoProyecto,
                                    estadosProyecto.proyectoActivo,
                                    null,
                                    null,
                                    opts,
                                  );

                                }
                              }
                            } else {
                              if (proyectoRecorridos <= limiteProyectos) {
                                //Cubre el 25% de valor estimado del proyecto
                                if (getFondosEsperaFondos >= porct25ValorEstimado) {

                                  //si cubre el 100% del valor estimado del proyecto
                                  if (getFondosEsperaFondos >= valorEstimadoProyecto) {

                                    //si se financia este proyecto
                                    proyectoAnteriorFinanciado = true

                                    //Obtengo la configuracion del evento de pre-estrategia
                                    const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                                      codigosConfiguracionEvento.preEstrategia,
                                    );
                                    if (getConfEventoPreEstrategia) {
                                      if (getConfEventoPreEstrategia.formulas.length > 0) {
                                        for (const getFormula of getConfEventoPreEstrategia.formulas) {
                                          let getFormulaEventoPreEstrategia: any;

                                          const getCodFormulaPreEstrategia = getFormula;
                                          //Obtengo la formula del evento de pre-estrategia
                                          getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                                            getCodFormulaPreEstrategia,
                                          );
                                          let formulaPreEstrategia;

                                          //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                                          if (
                                            getCodFormulaPreEstrategia ===
                                            codigosFormulasEvento.pre_estrategia
                                          ) {
                                            //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                                            formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                              'TOTAL_MONTO',
                                              getFondosEsperaFondos,
                                            );
                                            formulaPreEstrategia = formulaPreEstrategia.replace(
                                              'VALOR_ESTIMADO',
                                              valorEstimadoProyecto,
                                            );
                                            //Fondos segun el tipo de proyecto
                                            getFondosEsperaFondos = eval(formulaPreEstrategia);

                                            //Actualiza el estado del proyecto a pre estrategia
                                            let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                              idProyecto,
                                              estadoProyecto,
                                              estadosProyecto.proyectoPreEstrategia,
                                              null,
                                              null,
                                              opts,
                                            );
                                            if (actualizaProyecto) {
                                              //Objeto de beneficiario
                                              const beneficiarioDto = {
                                                tipo: beneficiario.proyecto,
                                                usuario: null,
                                                proyecto: idProyecto,
                                                estado: codigosEstadosBeneficiario.activa,
                                              };

                                              //Se guarda el beneficiario
                                              let crearBeneficiario = await new this.beneficiarioModel(
                                                beneficiarioDto,
                                              ).save(opts);

                                              //datos para guardar el historico
                                              const newHistoricoBeneficiario: any = {
                                                datos: crearBeneficiario,
                                                usuario: '',
                                                accion: codigosCatalogoAcciones.crear,
                                                entidad: codigoEntidades.entidadBeneficiario,
                                                tipo: codigosCatalogo.reparticionFondos,
                                              };

                                              //Crear historico del beneficiario
                                              this.crearHistoricoService.crearHistoricoServer(
                                                newHistoricoBeneficiario,
                                              );

                                              //Objeto de la transaccion
                                              const transaccionDto = {
                                                estado: estadosTransaccion.activa,
                                                monto: valorEstimadoProyecto,
                                                moneda: listaCodigosMonedas.USD,
                                                descripcion:
                                                  descripcionTransPagosGazelook.fondosReservados,
                                                origen: catalogoOrigen.fondos_reservados,
                                                balance: [],
                                                beneficiario: crearBeneficiario._id,
                                                metodoPago: null,
                                                informacionPago: null,
                                                usuario: null,
                                                conversionTransaccion: [],
                                                destino: null,
                                                numeroRecibo: null,
                                                comisionTransferencia: null,
                                                totalRecibido: null,
                                                origenPais: null,
                                              };
                                              //Crea la transaccion de tipo fondos reservados
                                              let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                                transaccionDto,
                                                opts,
                                              );

                                              //Objeto de la bonificacion
                                              let objBonificacion: any = {
                                                proyecto: idProyecto,
                                                montoCubierto: null,
                                                montoFaltante: null,
                                                fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                                fondosReservados: crearTransaccion._id,
                                                transacciones: [],
                                                estado: codigosEstadosBonificacion.activa,
                                              };

                                              //Se guarda la bonificacion
                                              let bonificacion = await new this.bonificacionModel(
                                                objBonificacion,
                                              ).save(opts);

                                              //datos para guardar el historico
                                              const newHistoricoBonificacion: any = {
                                                datos: bonificacion,
                                                usuario: '',
                                                accion: codigosCatalogoAcciones.crear,
                                                entidad: codigoEntidades.entidadBonificacion,
                                                tipo: codigosCatalogo.reparticionFondos,
                                              };

                                              //Crear historico de la bonificacion
                                              this.crearHistoricoService.crearHistoricoServer(
                                                newHistoricoBonificacion,
                                              );

                                              //Actualiza el array de proyectos en el evento
                                              await this.eventoModel.updateOne(
                                                { _id: idEvento },
                                                {
                                                  $push: { proyectos: idProyecto },
                                                  estado: estadoEvento.ejecutado,
                                                  fechaActualizacion: new Date(),
                                                },
                                                opts,
                                              );

                                              //Actualiza el evento en proyectos
                                              await this.proyectoModel.updateOne(
                                                { _id: idProyecto },
                                                {
                                                  evento: idEvento,
                                                  fechaActualizacion: new Date(),
                                                },
                                                opts,
                                              );
                                            }
                                          }
                                        }
                                      }
                                    }
                                  } else {

                                    //si se financia este proyecto
                                    proyectoAnteriorFinanciado = true


                                    //proyectos en espera de fondos
                                    let objProyectosEsperaFondos = {
                                      _id: idProyecto,
                                      valorEstimado: valorEstimadoProyecto,
                                      moneda: getMonedaProyecto,
                                      estado: estadoProyecto,
                                    };

                                    //Reparte los fondos de espera de fondos a los proyectos
                                    let repartirFondos = await this.repartirEsperaFondos(
                                      getFondosEsperaFondos,
                                      codigosFormulasEvento.monto_por_porcentaje,
                                      getFondosFinanciamientoEsperaFondos,
                                      objProyectosEsperaFondos,
                                      idEvento,
                                      getTipoProyecto,
                                      opts,
                                      ciclo
                                    );

                                    getFondosEsperaFondos = repartirFondos
                                    ciclo = 2

                                  }
                                } else {
                                  //no se financia este proyecto
                                  proyectoAnteriorFinanciado = false

                                  //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                                  if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                                    //suma de monto de preestrategia mas la espera de fondos
                                    // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                                    //El sistema ubica al proyecto en estado de preseleccionado cuando 
                                    //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                                    //lugar de los más votados dentro de cada tipo de proyecto. 
                                    let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                      idProyecto,
                                      estadoProyecto,
                                      estadosProyecto.preseleccionado,
                                      null,
                                      null,
                                      opts
                                    );

                                  } else {
                                    //no se financia este proyecto
                                    proyectoAnteriorFinanciado = false

                                    //Actualiza el estado del proyecto a activo
                                    let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                      idProyecto,
                                      estadoProyecto,
                                      estadosProyecto.proyectoActivo,
                                      null,
                                      null,
                                      opts,
                                    );

                                  }
                                }

                              }
                            }
                          }


                        }
                      }
                    } else {
                      //Si se existen fondos en el monto de espera de fondos
                      if (getFondosEsperaFondos > 0) {
                        if (proyectoAnteriorFinanciado) {
                          //Cubre el 25% de valor estimado del proyecto
                          if (getFondosEsperaFondos >= porct25ValorEstimado) {

                            //si cubre el 100% del valor estimado del proyecto
                            if (getFondosEsperaFondos >= valorEstimadoProyecto) {

                              //si se financia este proyecto
                              proyectoAnteriorFinanciado = true

                              //Obtengo la configuracion del evento de pre-estrategia
                              const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                                codigosConfiguracionEvento.preEstrategia,
                              );
                              if (getConfEventoPreEstrategia) {
                                if (getConfEventoPreEstrategia.formulas.length > 0) {
                                  for (const getFormula of getConfEventoPreEstrategia.formulas) {
                                    let getFormulaEventoPreEstrategia: any;

                                    const getCodFormulaPreEstrategia = getFormula;
                                    //Obtengo la formula del evento de pre-estrategia
                                    getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                                      getCodFormulaPreEstrategia,
                                    );
                                    let formulaPreEstrategia;

                                    //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                                    if (
                                      getCodFormulaPreEstrategia ===
                                      codigosFormulasEvento.pre_estrategia
                                    ) {
                                      //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                                      formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                        'TOTAL_MONTO',
                                        getFondosEsperaFondos,
                                      );
                                      formulaPreEstrategia = formulaPreEstrategia.replace(
                                        'VALOR_ESTIMADO',
                                        valorEstimadoProyecto,
                                      );
                                      //Fondos segun el tipo de proyecto
                                      getFondosEsperaFondos = eval(formulaPreEstrategia);

                                      //Actualiza el estado del proyecto a pre estrategia
                                      let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                        idProyecto,
                                        estadoProyecto,
                                        estadosProyecto.proyectoPreEstrategia,
                                        null,
                                        null,
                                        opts,
                                      );
                                      if (actualizaProyecto) {
                                        //Objeto de beneficiario
                                        const beneficiarioDto = {
                                          tipo: beneficiario.proyecto,
                                          usuario: null,
                                          proyecto: idProyecto,
                                          estado: codigosEstadosBeneficiario.activa,
                                        };

                                        //Se guarda el beneficiario
                                        let crearBeneficiario = await new this.beneficiarioModel(
                                          beneficiarioDto,
                                        ).save(opts);

                                        //datos para guardar el historico
                                        const newHistoricoBeneficiario: any = {
                                          datos: crearBeneficiario,
                                          usuario: '',
                                          accion: codigosCatalogoAcciones.crear,
                                          entidad: codigoEntidades.entidadBeneficiario,
                                          tipo: codigosCatalogo.reparticionFondos,
                                        };

                                        //Crear historico del beneficiario
                                        this.crearHistoricoService.crearHistoricoServer(
                                          newHistoricoBeneficiario,
                                        );

                                        //Objeto de la transaccion
                                        const transaccionDto = {
                                          estado: estadosTransaccion.activa,
                                          monto: valorEstimadoProyecto,
                                          moneda: listaCodigosMonedas.USD,
                                          descripcion:
                                            descripcionTransPagosGazelook.fondosReservados,
                                          origen: catalogoOrigen.fondos_reservados,
                                          balance: [],
                                          beneficiario: crearBeneficiario._id,
                                          metodoPago: null,
                                          informacionPago: null,
                                          usuario: null,
                                          conversionTransaccion: [],
                                          destino: null,
                                          numeroRecibo: null,
                                          comisionTransferencia: null,
                                          totalRecibido: null,
                                          origenPais: null,
                                        };
                                        //Crea la transaccion de tipo fondos reservados
                                        let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                          transaccionDto,
                                          opts,
                                        );

                                        //Objeto de la bonificacion
                                        let objBonificacion: any = {
                                          proyecto: idProyecto,
                                          montoCubierto: null,
                                          montoFaltante: null,
                                          fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                          fondosReservados: crearTransaccion._id,
                                          transacciones: [],
                                          estado: codigosEstadosBonificacion.activa,
                                        };

                                        //Se guarda la bonificacion
                                        let bonificacion = await new this.bonificacionModel(
                                          objBonificacion,
                                        ).save(opts);

                                        //datos para guardar el historico
                                        const newHistoricoBonificacion: any = {
                                          datos: bonificacion,
                                          usuario: '',
                                          accion: codigosCatalogoAcciones.crear,
                                          entidad: codigoEntidades.entidadBonificacion,
                                          tipo: codigosCatalogo.reparticionFondos,
                                        };

                                        //Crear historico de la bonificacion
                                        this.crearHistoricoService.crearHistoricoServer(
                                          newHistoricoBonificacion,
                                        );

                                        //Actualiza el array de proyectos en el evento
                                        await this.eventoModel.updateOne(
                                          { _id: idEvento },
                                          {
                                            $push: { proyectos: idProyecto },
                                            estado: estadoEvento.ejecutado,
                                            fechaActualizacion: new Date(),
                                          },
                                          opts,
                                        );

                                        //Actualiza el evento en proyectos
                                        await this.proyectoModel.updateOne(
                                          { _id: idProyecto },
                                          {
                                            evento: idEvento,
                                            fechaActualizacion: new Date(),
                                          },
                                          opts,
                                        );
                                      }
                                    }
                                  }
                                }
                              }
                            } else {

                              //si se financia este proyecto
                              proyectoAnteriorFinanciado = true


                              //proyectos en espera de fondos
                              let objProyectosEsperaFondos = {
                                _id: idProyecto,
                                valorEstimado: valorEstimadoProyecto,
                                moneda: getMonedaProyecto,
                                estado: estadoProyecto,
                              };

                              //Reparte los fondos de espera de fondos a los proyectos
                              let repartirFondos = await this.repartirEsperaFondos(
                                getFondosEsperaFondos,
                                codigosFormulasEvento.monto_por_porcentaje,
                                getFondosFinanciamientoEsperaFondos,
                                objProyectosEsperaFondos,
                                idEvento,
                                getTipoProyecto,
                                opts,
                                ciclo
                              );

                              getFondosEsperaFondos = repartirFondos

                            }
                          } else {
                            //no se financia este proyecto
                            proyectoAnteriorFinanciado = false

                            //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                            if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                              //suma de monto de preestrategia mas la espera de fondos
                              // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                              //El sistema ubica al proyecto en estado de preseleccionado cuando 
                              //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                              //lugar de los más votados dentro de cada tipo de proyecto. 
                              let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                idProyecto,
                                estadoProyecto,
                                estadosProyecto.preseleccionado,
                                null,
                                null,
                                opts
                              );

                            } else {
                              //no se financia este proyecto
                              proyectoAnteriorFinanciado = false

                              //Actualiza el estado del proyecto a activo
                              let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                idProyecto,
                                estadoProyecto,
                                estadosProyecto.proyectoActivo,
                                null,
                                null,
                                opts,
                              );

                            }
                          }
                        } else {
                          if (proyectoRecorridos <= limiteProyectos) {
                            //Cubre el 25% de valor estimado del proyecto
                            if (getFondosEsperaFondos >= porct25ValorEstimado) {

                              //si cubre el 100% del valor estimado del proyecto
                              if (getFondosEsperaFondos >= valorEstimadoProyecto) {

                                //si se financia este proyecto
                                proyectoAnteriorFinanciado = true

                                //Obtengo la configuracion del evento de pre-estrategia
                                const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                                  codigosConfiguracionEvento.preEstrategia,
                                );
                                if (getConfEventoPreEstrategia) {
                                  if (getConfEventoPreEstrategia.formulas.length > 0) {
                                    for (const getFormula of getConfEventoPreEstrategia.formulas) {
                                      let getFormulaEventoPreEstrategia: any;

                                      const getCodFormulaPreEstrategia = getFormula;
                                      //Obtengo la formula del evento de pre-estrategia
                                      getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                                        getCodFormulaPreEstrategia,
                                      );
                                      let formulaPreEstrategia;

                                      //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                                      if (
                                        getCodFormulaPreEstrategia ===
                                        codigosFormulasEvento.pre_estrategia
                                      ) {
                                        //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                                        formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                          'TOTAL_MONTO',
                                          getFondosEsperaFondos,
                                        );
                                        formulaPreEstrategia = formulaPreEstrategia.replace(
                                          'VALOR_ESTIMADO',
                                          valorEstimadoProyecto,
                                        );
                                        //Fondos segun el tipo de proyecto
                                        getFondosEsperaFondos = eval(formulaPreEstrategia);

                                        //Actualiza el estado del proyecto a pre estrategia
                                        let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                          idProyecto,
                                          estadoProyecto,
                                          estadosProyecto.proyectoPreEstrategia,
                                          null,
                                          null,
                                          opts,
                                        );
                                        if (actualizaProyecto) {
                                          //Objeto de beneficiario
                                          const beneficiarioDto = {
                                            tipo: beneficiario.proyecto,
                                            usuario: null,
                                            proyecto: idProyecto,
                                            estado: codigosEstadosBeneficiario.activa,
                                          };

                                          //Se guarda el beneficiario
                                          let crearBeneficiario = await new this.beneficiarioModel(
                                            beneficiarioDto,
                                          ).save(opts);

                                          //datos para guardar el historico
                                          const newHistoricoBeneficiario: any = {
                                            datos: crearBeneficiario,
                                            usuario: '',
                                            accion: codigosCatalogoAcciones.crear,
                                            entidad: codigoEntidades.entidadBeneficiario,
                                            tipo: codigosCatalogo.reparticionFondos,
                                          };

                                          //Crear historico del beneficiario
                                          this.crearHistoricoService.crearHistoricoServer(
                                            newHistoricoBeneficiario,
                                          );

                                          //Objeto de la transaccion
                                          const transaccionDto = {
                                            estado: estadosTransaccion.activa,
                                            monto: valorEstimadoProyecto,
                                            moneda: listaCodigosMonedas.USD,
                                            descripcion:
                                              descripcionTransPagosGazelook.fondosReservados,
                                            origen: catalogoOrigen.fondos_reservados,
                                            balance: [],
                                            beneficiario: crearBeneficiario._id,
                                            metodoPago: null,
                                            informacionPago: null,
                                            usuario: null,
                                            conversionTransaccion: [],
                                            destino: null,
                                            numeroRecibo: null,
                                            comisionTransferencia: null,
                                            totalRecibido: null,
                                            origenPais: null,
                                          };
                                          //Crea la transaccion de tipo fondos reservados
                                          let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                            transaccionDto,
                                            opts,
                                          );

                                          //Objeto de la bonificacion
                                          let objBonificacion: any = {
                                            proyecto: idProyecto,
                                            montoCubierto: null,
                                            montoFaltante: null,
                                            fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                            fondosReservados: crearTransaccion._id,
                                            transacciones: [],
                                            estado: codigosEstadosBonificacion.activa,
                                          };

                                          //Se guarda la bonificacion
                                          let bonificacion = await new this.bonificacionModel(
                                            objBonificacion,
                                          ).save(opts);

                                          //datos para guardar el historico
                                          const newHistoricoBonificacion: any = {
                                            datos: bonificacion,
                                            usuario: '',
                                            accion: codigosCatalogoAcciones.crear,
                                            entidad: codigoEntidades.entidadBonificacion,
                                            tipo: codigosCatalogo.reparticionFondos,
                                          };

                                          //Crear historico de la bonificacion
                                          this.crearHistoricoService.crearHistoricoServer(
                                            newHistoricoBonificacion,
                                          );

                                          //Actualiza el array de proyectos en el evento
                                          await this.eventoModel.updateOne(
                                            { _id: idEvento },
                                            {
                                              $push: { proyectos: idProyecto },
                                              estado: estadoEvento.ejecutado,
                                              fechaActualizacion: new Date(),
                                            },
                                            opts,
                                          );

                                          //Actualiza el evento en proyectos
                                          await this.proyectoModel.updateOne(
                                            { _id: idProyecto },
                                            {
                                              evento: idEvento,
                                              fechaActualizacion: new Date(),
                                            },
                                            opts,
                                          );
                                        }
                                      }
                                    }
                                  }
                                }
                              } else {

                                //si se financia este proyecto
                                proyectoAnteriorFinanciado = true


                                //proyectos en espera de fondos
                                let objProyectosEsperaFondos = {
                                  _id: idProyecto,
                                  valorEstimado: valorEstimadoProyecto,
                                  moneda: getMonedaProyecto,
                                  estado: estadoProyecto,
                                };

                                //Reparte los fondos de espera de fondos a los proyectos
                                let repartirFondos = await this.repartirEsperaFondos(
                                  getFondosEsperaFondos,
                                  codigosFormulasEvento.monto_por_porcentaje,
                                  getFondosFinanciamientoEsperaFondos,
                                  objProyectosEsperaFondos,
                                  idEvento,
                                  getTipoProyecto,
                                  opts,
                                  ciclo
                                );

                                getFondosEsperaFondos = repartirFondos
                                ciclo = 2

                              }
                            } else {
                              //no se financia este proyecto
                              proyectoAnteriorFinanciado = false

                              //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                              if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                                //suma de monto de preestrategia mas la espera de fondos
                                // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                                //El sistema ubica al proyecto en estado de preseleccionado cuando 
                                //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                                //lugar de los más votados dentro de cada tipo de proyecto. 
                                let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                  idProyecto,
                                  estadoProyecto,
                                  estadosProyecto.preseleccionado,
                                  null,
                                  null,
                                  opts
                                );

                              } else {
                                //no se financia este proyecto
                                proyectoAnteriorFinanciado = false

                                //Actualiza el estado del proyecto a activo
                                let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                  idProyecto,
                                  estadoProyecto,
                                  estadosProyecto.proyectoActivo,
                                  null,
                                  null,
                                  opts,
                                );

                              }
                            }

                          }
                        }
                      }
                    }

                  }
                }
              } else {
                //Si se existen fondos en el monto de espera de fondos
                if (getFondosEsperaFondos > 0) {
                  if (proyectoAnteriorFinanciado) {
                    //Cubre el 25% de valor estimado del proyecto
                    if (getFondosEsperaFondos >= porct25ValorEstimado) {

                      //si cubre el 100% del valor estimado del proyecto
                      if (getFondosEsperaFondos >= valorEstimadoProyecto) {

                        //si se financia este proyecto
                        proyectoAnteriorFinanciado = true

                        //Obtengo la configuracion del evento de pre-estrategia
                        const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                          codigosConfiguracionEvento.preEstrategia,
                        );
                        if (getConfEventoPreEstrategia) {
                          if (getConfEventoPreEstrategia.formulas.length > 0) {
                            for (const getFormula of getConfEventoPreEstrategia.formulas) {
                              let getFormulaEventoPreEstrategia: any;

                              const getCodFormulaPreEstrategia = getFormula;
                              //Obtengo la formula del evento de pre-estrategia
                              getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                                getCodFormulaPreEstrategia,
                              );
                              let formulaPreEstrategia;

                              //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                              if (
                                getCodFormulaPreEstrategia ===
                                codigosFormulasEvento.pre_estrategia
                              ) {
                                //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                                formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                  'TOTAL_MONTO',
                                  getFondosEsperaFondos,
                                );
                                formulaPreEstrategia = formulaPreEstrategia.replace(
                                  'VALOR_ESTIMADO',
                                  valorEstimadoProyecto,
                                );
                                //Fondos segun el tipo de proyecto
                                getFondosEsperaFondos = eval(formulaPreEstrategia);

                                //Actualiza el estado del proyecto a pre estrategia
                                let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                  idProyecto,
                                  estadoProyecto,
                                  estadosProyecto.proyectoPreEstrategia,
                                  null,
                                  null,
                                  opts,
                                );
                                if (actualizaProyecto) {
                                  //Objeto de beneficiario
                                  const beneficiarioDto = {
                                    tipo: beneficiario.proyecto,
                                    usuario: null,
                                    proyecto: idProyecto,
                                    estado: codigosEstadosBeneficiario.activa,
                                  };

                                  //Se guarda el beneficiario
                                  let crearBeneficiario = await new this.beneficiarioModel(
                                    beneficiarioDto,
                                  ).save(opts);

                                  //datos para guardar el historico
                                  const newHistoricoBeneficiario: any = {
                                    datos: crearBeneficiario,
                                    usuario: '',
                                    accion: codigosCatalogoAcciones.crear,
                                    entidad: codigoEntidades.entidadBeneficiario,
                                    tipo: codigosCatalogo.reparticionFondos,
                                  };

                                  //Crear historico del beneficiario
                                  this.crearHistoricoService.crearHistoricoServer(
                                    newHistoricoBeneficiario,
                                  );

                                  //Objeto de la transaccion
                                  const transaccionDto = {
                                    estado: estadosTransaccion.activa,
                                    monto: valorEstimadoProyecto,
                                    moneda: listaCodigosMonedas.USD,
                                    descripcion:
                                      descripcionTransPagosGazelook.fondosReservados,
                                    origen: catalogoOrigen.fondos_reservados,
                                    balance: [],
                                    beneficiario: crearBeneficiario._id,
                                    metodoPago: null,
                                    informacionPago: null,
                                    usuario: null,
                                    conversionTransaccion: [],
                                    destino: null,
                                    numeroRecibo: null,
                                    comisionTransferencia: null,
                                    totalRecibido: null,
                                    origenPais: null,
                                  };
                                  //Crea la transaccion de tipo fondos reservados
                                  let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                    transaccionDto,
                                    opts,
                                  );

                                  //Objeto de la bonificacion
                                  let objBonificacion: any = {
                                    proyecto: idProyecto,
                                    montoCubierto: null,
                                    montoFaltante: null,
                                    fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                    fondosReservados: crearTransaccion._id,
                                    transacciones: [],
                                    estado: codigosEstadosBonificacion.activa,
                                  };

                                  //Se guarda la bonificacion
                                  let bonificacion = await new this.bonificacionModel(
                                    objBonificacion,
                                  ).save(opts);

                                  //datos para guardar el historico
                                  const newHistoricoBonificacion: any = {
                                    datos: bonificacion,
                                    usuario: '',
                                    accion: codigosCatalogoAcciones.crear,
                                    entidad: codigoEntidades.entidadBonificacion,
                                    tipo: codigosCatalogo.reparticionFondos,
                                  };

                                  //Crear historico de la bonificacion
                                  this.crearHistoricoService.crearHistoricoServer(
                                    newHistoricoBonificacion,
                                  );

                                  //Actualiza el array de proyectos en el evento
                                  await this.eventoModel.updateOne(
                                    { _id: idEvento },
                                    {
                                      $push: { proyectos: idProyecto },
                                      estado: estadoEvento.ejecutado,
                                      fechaActualizacion: new Date(),
                                    },
                                    opts,
                                  );

                                  //Actualiza el evento en proyectos
                                  await this.proyectoModel.updateOne(
                                    { _id: idProyecto },
                                    {
                                      evento: idEvento,
                                      fechaActualizacion: new Date(),
                                    },
                                    opts,
                                  );
                                }
                              }
                            }
                          }
                        }
                      } else {

                        //si se financia este proyecto
                        proyectoAnteriorFinanciado = true


                        //proyectos en espera de fondos
                        let objProyectosEsperaFondos = {
                          _id: idProyecto,
                          valorEstimado: valorEstimadoProyecto,
                          moneda: getMonedaProyecto,
                          estado: estadoProyecto,
                        };

                        //Reparte los fondos de espera de fondos a los proyectos
                        let repartirFondos = await this.repartirEsperaFondos(
                          getFondosEsperaFondos,
                          codigosFormulasEvento.monto_por_porcentaje,
                          getFondosFinanciamientoEsperaFondos,
                          objProyectosEsperaFondos,
                          idEvento,
                          getTipoProyecto,
                          opts,
                          ciclo
                        );

                        getFondosEsperaFondos = repartirFondos

                      }
                    } else {
                      //no se financia este proyecto
                      proyectoAnteriorFinanciado = false

                      //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                      if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                        //suma de monto de preestrategia mas la espera de fondos
                        // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                        //El sistema ubica al proyecto en estado de preseleccionado cuando 
                        //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                        //lugar de los más votados dentro de cada tipo de proyecto. 
                        let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                          idProyecto,
                          estadoProyecto,
                          estadosProyecto.preseleccionado,
                          null,
                          null,
                          opts
                        );

                      } else {
                        //no se financia este proyecto
                        proyectoAnteriorFinanciado = false

                        //Actualiza el estado del proyecto a activo
                        let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                          idProyecto,
                          estadoProyecto,
                          estadosProyecto.proyectoActivo,
                          null,
                          null,
                          opts,
                        );

                      }
                    }
                  } else {
                    if (proyectoRecorridos <= limiteProyectos) {
                      //Cubre el 25% de valor estimado del proyecto
                      if (getFondosEsperaFondos >= porct25ValorEstimado) {

                        //si cubre el 100% del valor estimado del proyecto
                        if (getFondosEsperaFondos >= valorEstimadoProyecto) {

                          //si se financia este proyecto
                          proyectoAnteriorFinanciado = true

                          //Obtengo la configuracion del evento de pre-estrategia
                          const getConfEventoPreEstrategia = await this.configuracionEventoService.obtenerConfiguracionEvento(
                            codigosConfiguracionEvento.preEstrategia,
                          );
                          if (getConfEventoPreEstrategia) {
                            if (getConfEventoPreEstrategia.formulas.length > 0) {
                              for (const getFormula of getConfEventoPreEstrategia.formulas) {
                                let getFormulaEventoPreEstrategia: any;

                                const getCodFormulaPreEstrategia = getFormula;
                                //Obtengo la formula del evento de pre-estrategia
                                getFormulaEventoPreEstrategia = await this.formulaEventoService.obtenerFormulaEvento(
                                  getCodFormulaPreEstrategia,
                                );
                                let formulaPreEstrategia;

                                //Fórmula cada vez que se reparte fondos a un proyecto para pasar a estado de pre estrategia
                                if (
                                  getCodFormulaPreEstrategia ===
                                  codigosFormulasEvento.pre_estrategia
                                ) {
                                  //Reemplaza el TOTAL_MONTO Y EL VALOR_ESTIMADO
                                  formulaPreEstrategia = getFormulaEventoPreEstrategia.replace(
                                    'TOTAL_MONTO',
                                    getFondosEsperaFondos,
                                  );
                                  formulaPreEstrategia = formulaPreEstrategia.replace(
                                    'VALOR_ESTIMADO',
                                    valorEstimadoProyecto,
                                  );
                                  //Fondos segun el tipo de proyecto
                                  getFondosEsperaFondos = eval(formulaPreEstrategia);

                                  //Actualiza el estado del proyecto a pre estrategia
                                  let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                                    idProyecto,
                                    estadoProyecto,
                                    estadosProyecto.proyectoPreEstrategia,
                                    null,
                                    null,
                                    opts,
                                  );
                                  if (actualizaProyecto) {
                                    //Objeto de beneficiario
                                    const beneficiarioDto = {
                                      tipo: beneficiario.proyecto,
                                      usuario: null,
                                      proyecto: idProyecto,
                                      estado: codigosEstadosBeneficiario.activa,
                                    };

                                    //Se guarda el beneficiario
                                    let crearBeneficiario = await new this.beneficiarioModel(
                                      beneficiarioDto,
                                    ).save(opts);

                                    //datos para guardar el historico
                                    const newHistoricoBeneficiario: any = {
                                      datos: crearBeneficiario,
                                      usuario: '',
                                      accion: codigosCatalogoAcciones.crear,
                                      entidad: codigoEntidades.entidadBeneficiario,
                                      tipo: codigosCatalogo.reparticionFondos,
                                    };

                                    //Crear historico del beneficiario
                                    this.crearHistoricoService.crearHistoricoServer(
                                      newHistoricoBeneficiario,
                                    );

                                    //Objeto de la transaccion
                                    const transaccionDto = {
                                      estado: estadosTransaccion.activa,
                                      monto: valorEstimadoProyecto,
                                      moneda: listaCodigosMonedas.USD,
                                      descripcion:
                                        descripcionTransPagosGazelook.fondosReservados,
                                      origen: catalogoOrigen.fondos_reservados,
                                      balance: [],
                                      beneficiario: crearBeneficiario._id,
                                      metodoPago: null,
                                      informacionPago: null,
                                      usuario: null,
                                      conversionTransaccion: [],
                                      destino: null,
                                      numeroRecibo: null,
                                      comisionTransferencia: null,
                                      totalRecibido: null,
                                      origenPais: null,
                                    };
                                    //Crea la transaccion de tipo fondos reservados
                                    let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
                                      transaccionDto,
                                      opts,
                                    );

                                    //Objeto de la bonificacion
                                    let objBonificacion: any = {
                                      proyecto: idProyecto,
                                      montoCubierto: null,
                                      montoFaltante: null,
                                      fondosFinanciamiento: idFondosFinanciamientoPreEstrategia,
                                      fondosReservados: crearTransaccion._id,
                                      transacciones: [],
                                      estado: codigosEstadosBonificacion.activa,
                                    };

                                    //Se guarda la bonificacion
                                    let bonificacion = await new this.bonificacionModel(
                                      objBonificacion,
                                    ).save(opts);

                                    //datos para guardar el historico
                                    const newHistoricoBonificacion: any = {
                                      datos: bonificacion,
                                      usuario: '',
                                      accion: codigosCatalogoAcciones.crear,
                                      entidad: codigoEntidades.entidadBonificacion,
                                      tipo: codigosCatalogo.reparticionFondos,
                                    };

                                    //Crear historico de la bonificacion
                                    this.crearHistoricoService.crearHistoricoServer(
                                      newHistoricoBonificacion,
                                    );

                                    //Actualiza el array de proyectos en el evento
                                    await this.eventoModel.updateOne(
                                      { _id: idEvento },
                                      {
                                        $push: { proyectos: idProyecto },
                                        estado: estadoEvento.ejecutado,
                                        fechaActualizacion: new Date(),
                                      },
                                      opts,
                                    );

                                    //Actualiza el evento en proyectos
                                    await this.proyectoModel.updateOne(
                                      { _id: idProyecto },
                                      {
                                        evento: idEvento,
                                        fechaActualizacion: new Date(),
                                      },
                                      opts,
                                    );
                                  }
                                }
                              }
                            }
                          }
                        } else {

                          //si se financia este proyecto
                          proyectoAnteriorFinanciado = true


                          //proyectos en espera de fondos
                          let objProyectosEsperaFondos = {
                            _id: idProyecto,
                            valorEstimado: valorEstimadoProyecto,
                            moneda: getMonedaProyecto,
                            estado: estadoProyecto,
                          };

                          //Reparte los fondos de espera de fondos a los proyectos
                          let repartirFondos = await this.repartirEsperaFondos(
                            getFondosEsperaFondos,
                            codigosFormulasEvento.monto_por_porcentaje,
                            getFondosFinanciamientoEsperaFondos,
                            objProyectosEsperaFondos,
                            idEvento,
                            getTipoProyecto,
                            opts,
                            ciclo
                          );

                          getFondosEsperaFondos = repartirFondos
                          ciclo = 2

                        }
                      } else {
                        //no se financia este proyecto
                        proyectoAnteriorFinanciado = false

                        //si es el proyecto numero 1 o 2 de la lista del tipo de proyecto
                        if (proyectoRecorridos === 1 || proyectoRecorridos === 2) {
                          //suma de monto de preestrategia mas la espera de fondos
                          // let fondoSumaPreEstrategiaEsperaFondos = getFondosPreEstrategia + getFondosEsperaFondos

                          //El sistema ubica al proyecto en estado de preseleccionado cuando 
                          //no se cubre mínimo el 25% y se encuentre entre el primer y segundo 
                          //lugar de los más votados dentro de cada tipo de proyecto. 
                          let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                            idProyecto,
                            estadoProyecto,
                            estadosProyecto.preseleccionado,
                            null,
                            null,
                            opts
                          );

                        } else {
                          //no se financia este proyecto
                          proyectoAnteriorFinanciado = false

                          //Actualiza el estado del proyecto a activo
                          let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                            idProyecto,
                            estadoProyecto,
                            estadosProyecto.proyectoActivo,
                            null,
                            null,
                            opts,
                          );

                        }
                      }

                    }
                  }
                } else {
                  //no se financia este proyecto
                  proyectoAnteriorFinanciado = false

                  //Actualiza el estado del proyecto a activo
                  let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                    idProyecto,
                    estadoProyecto,
                    estadosProyecto.proyectoActivo,
                    null,
                    null,
                    opts,
                  );
                }

              }


            } //FIN FOR PROYECTOS MAS VOTADOS

          }

          montoSobrante = getFondosPreEstrategia + getFondosEsperaFondos;

        } //FIN FOR TIPOS DE PROYECTOS


      }

      if (montoSobrante > 0) {
        //Objeto de beneficiario
        const beneficiarioDto = {
          tipo: beneficiario.proyecto,
          usuario: null,
          proyecto: null,
          estado: codigosEstadosBeneficiario.activa,
        };

        //Se guarda el beneficiario
        let crearBeneficiario = await new this.beneficiarioModel(
          beneficiarioDto,
        ).save(opts);

        //datos para guardar el historico
        const newHistoricoBeneficiario: any = {
          datos: crearBeneficiario,
          usuario: '',
          accion: codigosCatalogoAcciones.crear,
          entidad: codigoEntidades.entidadBeneficiario,
          tipo: codigosCatalogo.reparticionFondos,
        };

        //Crear historico del beneficiario
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoBeneficiario,
        );

        const idTransaccion = new mongoose.mongo.ObjectId();

        // -------------------------------- Crear Conversion ---------------------------
        const dataConversion: ConversionTransaccionDto = {
          monedaUsuario: listaCodigosMonedas.USD,
          monedaDefault: listaCodigosMonedas.USD,
          montoDefault: montoSobrante,
          idTransaccion: idTransaccion.toHexString(),
        };
        const conversiones = await this.crearConversionTransaccionService.crearConversionTransaccion(
          dataConversion,
          opts,
        );
        

        //Objeto de la transaccion
        const transaccionDto = {
          _id: idTransaccion,
          estado: estadosTransaccion.activa,
          monto: montoSobrante,
          moneda: listaCodigosMonedas.USD,
          descripcion:
            descripcionTransPagosGazelook.montoSobrante,
          origen: catalogoOrigen.monto_sobrante,
          destino: catalogoOrigen.monto_sobrante,
          balance: [],
          beneficiario: crearBeneficiario._id,
          metodoPago: null,
          informacionPago: null,
          usuario: null,
          conversionTransaccion: conversiones,
          numeroRecibo: null,
          comisionTransferencia: null,
          totalRecibido: null,
          origenPais: null,
        };
        //Crea la transaccion de tipo fondos reservados
        let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
          transaccionDto,
          opts,
        );

        
      }

      return true;
    } catch (error) {
      throw error;
    }
  }

  async obtenerMontoTotalTipoProyectos(
    tipoProyectos,
    arrayTipoProyectosRecorridos,
    opts,
  ): Promise<any> {
    //Obtiene el catalogo de porcentajes segun los tipos de proyecto en orden de prioridad
    let getCatalogoPorcentajeProyecto = await this.catalogoPorcentajeProyectoService.obtenerCatalogoPorcentajeProyecto();

    if (getCatalogoPorcentajeProyecto.length > 0) {
      let numTipoProyectos = getCatalogoPorcentajeProyecto.length;
      let numeroTipoProyectosEjecutados = 0;
      let montoSobrante = 0;
      let nuevoBalance = 0;
      let arrayTipoProyectos = [];

      for (const catalogoPorcentajeProyecto of getCatalogoPorcentajeProyecto) {
        let getTipoProyecto = catalogoPorcentajeProyecto.tipo;
        let codigoCatPorcentProyecto = catalogoPorcentajeProyecto.codigo;
        let prioridadTipoProyecto = catalogoPorcentajeProyecto.prioridad;
        if (tipoProyectos.length >= 0) {
          let objTipoProyecto: any;
          if (arrayTipoProyectosRecorridos.length > 0) {
            const encuentraTipoProyecto = await tipoProyectos.find(
              element => element.tipo === getTipoProyecto,
            );

            if (encuentraTipoProyecto) {
              if (encuentraTipoProyecto.monto > 0) {
                montoSobrante = encuentraTipoProyecto.monto;
              }
            }

            const encuentraTipoProyectoLista = await arrayTipoProyectosRecorridos.find(
              element => element.tipo === getTipoProyecto,
            );

            if (!encuentraTipoProyectoLista) {
              ////////////////////////////////////////////////////REVISARRRRRRRRR////////////////////////////////////
              let getFondosTipoproyecto = await this.fondosTipoProyectoModel
                .findOne({
                  catalogoPorcentajeProyecto: codigoCatPorcentProyecto,
                })
                .sort('-fechaCreacion')
                .limit(1)
                .session(opts.session);
              montoSobrante = montoSobrante + getFondosTipoproyecto.monto;
              nuevoBalance = montoSobrante;

              let objTipoProyecto = {
                tipo: getTipoProyecto,
                codigo: codigoCatPorcentProyecto,
                prioridad: prioridadTipoProyecto,
              };
              arrayTipoProyectos.push(objTipoProyecto);
            }
          }
        }
      }

      let objReturn = {
        tipoProyectos: arrayTipoProyectos,
        ultimoBalance: nuevoBalance,
      };
      return objReturn;
    }
  }

  async crearFinanciamientoEsperaFondos(
    valorEstimado,
    montoTotal,
    getCodFormula,
    crearFondosFinanciacion2,
    nuevoFinanciamientoEsperaFondos,
    opts,
  ): Promise<FinanciamientoEsperaFondos> {
    let catalogoPorcentajeEsperaFondos = await this.catalogoPorcentajeEsperaFondosService.obtenerCatalogoPorcentajeEsperaFondos();
    let porcentajeEsperaFondos = catalogoPorcentajeEsperaFondos.porcentaje;
    let codigoCatalogoPorcentajeEsperaFondos =
      catalogoPorcentajeEsperaFondos.codigo;

    //Obtengo la formula del evento para calcular monto por porcentaje
    let getFormulaMontoPorc = await this.formulaEventoService.obtenerFormulaEvento(
      getCodFormula,
    );

    console.log('getCodFormula::::::::::::::::::-------->', getFormulaMontoPorc)
    console.log('valorEstimado::::::::::::::::::-------->', valorEstimado)
    console.log('porcentajeEsperaFondos::::::::::::::::::-------->', porcentajeEsperaFondos)
    //Remmplaza el MONTO Y EL PROCENTAJE
    let formulaMontoPorcentaje = getFormulaMontoPorc.replace(
      'MONTO',
      montoTotal,
    );
    getFormulaMontoPorc = formulaMontoPorcentaje.replace(
      'PORCENTAJE',
      porcentajeEsperaFondos,
    );

    console.log('montoFinal::::::::::::::::::::::----------->', eval(getFormulaMontoPorc))
    //Fondos segun el porcentaje de catalogo espera de fondos
    let montoFinal = eval(getFormulaMontoPorc);

    let referencia;
    if (nuevoFinanciamientoEsperaFondos) {
      referencia = null;
    } else {
      referencia = crearFondosFinanciacion2._id;
    }
    let montoFaltante = valorEstimado - montoFinal
    //Objeto de tipo financiamiento espera de fondos
    let objFinancEsperaFondos = {
      montoIngreso: montoFinal,
      montoTotal: montoFaltante,
      montoFinal: valorEstimado,
      estado: codigosEstadosFinanciamientoEsperaFondos.activa,
      referencia: referencia,
      catalogoPorcentajeEsperaFondos: codigoCatalogoPorcentajeEsperaFondos,
    };

    //Se guarda los fondos de financiacion
    let crearFinanciamientoEsperaFondos = await new this.financiamientoEsperaFondosModel(
      objFinancEsperaFondos,
    ).save(opts);

    //datos para guardar el historico del financiamiento espera de fondos
    const newHistoricoFinanciacionEsperaFondos: any = {
      datos: crearFinanciamientoEsperaFondos,
      usuario: '',
      accion: codigosCatalogoAcciones.crear,
      entidad: codigoEntidades.fondosFinanciamiento,
      tipo: codigosCatalogo.reparticionFondos,
    };

    //Crear historico para el financiamiento espera de fondos
    this.crearHistoricoService.crearHistoricoServer(
      newHistoricoFinanciacionEsperaFondos,
    );
    return crearFinanciamientoEsperaFondos;
  }

  //Metodo que reparte los fondos de tipo espera de fondos a los proyectos
  async repartirEsperaFondos(
    getFondosPreEstrategia,
    getCodFormula,
    crearFondosFinanciacion2,
    listaProyectosEsperaFondos,
    idEvento,
    getTipoProyecto,
    opts,
    ciclo
  ): Promise<any> {
    //Verifica la lista de proyectos que estan para espera de fondos
    const proyectosEsperaFondos = listaProyectosEsperaFondos
    if (proyectosEsperaFondos) {

      let montoTotal = getFondosPreEstrategia;

      if (montoTotal > 0) {


        //let valorPorcentajeEsperaFondosProyecto = porcentajeEsperaFondos * proyectosEsperaFondos.valorEstimado;
        let financiamientoEsperaFondos = await this.crearFinanciamientoEsperaFondos(
          proyectosEsperaFondos.valorEstimado,
          montoTotal,
          getCodFormula,
          crearFondosFinanciacion2,
          true,
          opts,
        );

        //Actualiza el estado del proyecto a ESPERA DE FONDOS
        let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
          proyectosEsperaFondos._id,
          proyectosEsperaFondos.estado,
          estadosProyecto.proyectoEnEsperaFondos,
          null,
          null,
          opts,
        );

        //Objeto de beneficiario
        const beneficiarioDto = {
          tipo: beneficiario.proyecto,
          usuario: null,
          proyecto: proyectosEsperaFondos._id,
          estado: codigosEstadosBeneficiario.activa,
        };

        //Se guarda el beneficiario
        let crearBeneficiario = await new this.beneficiarioModel(
          beneficiarioDto,
        ).save(opts);

        //datos para guardar el historico
        const newHistoricoBeneficiario: any = {
          datos: crearBeneficiario,
          usuario: '',
          accion: codigosCatalogoAcciones.crear,
          entidad: codigoEntidades.entidadBeneficiario,
          tipo: codigosCatalogo.reparticionFondos,
        };

        //Crear historico del beneficiario
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoBeneficiario,
        );

        //Objeto de la transaccion
        const transaccionDto = {
          estado: estadosTransaccion.activa,
          monto: financiamientoEsperaFondos.montoIngreso,
          moneda: listaCodigosMonedas.USD,
          descripcion: descripcionTransPagosGazelook.fondosReservados,
          origen: catalogoOrigen.fondos_reservados,
          destino: catalogoOrigen.fondos_reservados,
          balance: [],
          beneficiario: crearBeneficiario._id,
          metodoPago: null,
          informacionPago: null,
          usuario: null,
          conversionTransaccion: [],
          numeroRecibo: null,
          comisionTransferencia: null,
          totalRecibido: null,
          origenPais: null,
        };
        //Crea la transaccion de tipo fondos reservados
        let crearTransaccion = await this.crearTransaccionFondosProyectoService.crearTransaccionFondosAsignadosProyectos(
          transaccionDto,
          opts,
        );

        //Objeto de la bonificacion
        let objBonificacion: any = {
          proyecto: proyectosEsperaFondos._id,
          montoCubierto: null,
          montoFaltante: null,
          fondosFinanciamiento: null,
          esperaFondos: financiamientoEsperaFondos._id,
          fondosReservados: crearTransaccion._id,
          transacciones: [],
          estado: codigosEstadosBonificacion.activa,
        };

        //Se guarda la bonificaion
        let bonificacion = await new this.bonificacionModel(
          objBonificacion,
        ).save(opts);

        //datos para guardar el historico
        const newHistoricoBonificacion: any = {
          datos: bonificacion,
          usuario: '',
          accion: codigosCatalogoAcciones.crear,
          entidad: codigoEntidades.entidadBonificacion,
          tipo: codigosCatalogo.reparticionFondos,
        };

        //Crear historico de la bonificacion
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoBonificacion,
        );


        //Actualiza el array de proyectos en el evento
        await this.eventoModel.updateOne(
          { _id: idEvento },
          {
            $push: { proyectos: proyectosEsperaFondos._id },
            estado: estadoEvento.ejecutado,
            fechaActualizacion: new Date(),
          },
          opts,
        );

        //Actualiza el evento en proyectos
        await this.proyectoModel.updateOne(
          { _id: proyectosEsperaFondos._id },
          { evento: idEvento, fechaActualizacion: new Date() },
          opts,
        );

        if (ciclo === 2) {
          //Actualiza los proyectos a su estado activo que no han sido financiados con nada de dinero
          await this.proyectoModel.updateMany(
            {
              $and: [
                { estado: estadosProyecto.proyectoForo },
                { tipo: getTipoProyecto },
              ],
            },
            {
              $set: {
                estado: estadosProyecto.proyectoActivo,
                fechaActualizacion: new Date(),
              },
            },
            opts,
          );
        }

      } else
        if (ciclo === 2) {
          //Actualiza los proyectos a su estado activo que no han sido financiados con nada de dinero
          await this.proyectoModel.updateMany(
            {
              $and: [
                { estado: estadosProyecto.proyectoForo },
                { tipo: getTipoProyecto },
              ],
            },
            {
              $set: {
                estado: estadosProyecto.proyectoActivo,
                fechaActualizacion: new Date(),
              },
            },
            opts,
          );
        }
    }

    return 0;
  }
}
