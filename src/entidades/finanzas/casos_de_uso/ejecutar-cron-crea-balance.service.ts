import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Evento } from '../../../drivers/mongoose/interfaces/evento/evento.interface';
import {
  codigosConfiguracionEvento,
  estadoEvento,
} from '../../../shared/enum-sistema';
import { CrearBalanceService } from './crear-balance.service';

@Injectable()
export class EjecutarCronFinanzasService {
  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
    private crearBalanceService: CrearBalanceService,
  ) { }

  // Se ejecuta todos los dias a las 10 de la noche
  // @Cron(CronExpression.EVERY_DAY_AT_10PM)
  //Se crea el evento de ejecutar balance
  async ejecutarCronBalance() {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let localTime = moment().format('YYYY-MM-DD'); // store localTime
      let fechaFormat = localTime + 'T22:00:00.000Z';
      let formatFechaActual = new Date(fechaFormat);
      //Obteniendo fecha actual
      formatFechaActual.setDate(new Date(fechaFormat).getDate());

      const getEvento = await this.eventoModel
        .find({
          estado: estadoEvento.pendiente,
          configuracionEvento: codigosConfiguracionEvento.finanzasCONFEVT_2,
        })
        .sort('-fechaActualizacion')
        .limit(1);

      let fecFinal = getEvento[0].fechaFin;
      let formatFinal =
        fecFinal.getUTCFullYear() +
        '-' +
        (fecFinal.getUTCMonth() + 1) +
        '-' +
        fecFinal.getUTCDate();

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es mayor o igual a 10
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() >= 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 1) +
          '-' +
          fecFinal.getUTCDate();
      }

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es igual a 32
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() === 32) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 2) +
          '-01';
      }

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es menor a 10
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() < 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 1) +
          '-0' +
          fecFinal.getUTCDate();
      }

      //Si el mes de la fecha final es mayor a 10 y el dia de la fecha final es menor a 10
      if (fecFinal.getUTCMonth() + 1 >= 10 && fecFinal.getUTCDate() < 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-' +
          (fecFinal.getUTCMonth() + 1) +
          '-0' +
          fecFinal.getUTCDate();
      }

      let fechaFinEvento = formatFinal + 'T22:00:00.000Z';
      let formatFechaFin = new Date(fechaFinEvento);

      //Fecha fin del evento
      formatFechaFin.setDate(new Date(formatFechaFin).getDate());

      console.log('fecha actual: ', formatFechaActual);
      console.log('fecha final para crear balance: ', formatFechaFin);
      
      if (formatFechaActual === formatFechaFin) {
        // actualizar evento como ejecutado
        await this.eventoModel.updateOne(
          { _id: getEvento[0]._id },
          { estado: estadoEvento.ejecutado, fechaActualizacion: new Date() },
          opts,
        );

        //crea el balance de fondos disponibles para usuarios y fondos disponibles para gazelook
        await this.crearBalanceService.crearBalance(opts);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }

      //Finaliza la transaccion
      await session.endSession();
    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
