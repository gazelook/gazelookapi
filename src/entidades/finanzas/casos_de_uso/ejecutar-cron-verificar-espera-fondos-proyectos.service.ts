import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Beneficiario } from 'src/drivers/mongoose/interfaces/beneficiario/beneficiario.interface';
import { Bonificacion } from 'src/drivers/mongoose/interfaces/bonificacion/bonificacion.interface';
import { FinanciamientoEsperaFondos } from 'src/drivers/mongoose/interfaces/financiamiento_espera_fondos/financiamiento-espera-fondos.interface';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { ActualizaEstadoProyectoService } from 'src/entidades/proyectos/casos_de_uso/actualiza-estado-proyecto.service';
import { ObtenerFechaFinForoProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-fecha-fin-foro-proyecto.service';
import {
  catalogoOrigen,
  codigoEntidades,
  codigosCatalogoAcciones,
  codigosEstadosBeneficiario,
  codigosEstadosBonificacion,
  codigosEstadosFinanciamientoEsperaFondos,
  descripcionTransPagosGazelook,
  estadosProyecto,
  estadosTransaccion,
} from 'src/shared/enum-sistema';

@Injectable()
export class EjecutarCronVerificarEsperaFondosProyectosService {
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: Model<Proyecto>,
    @Inject('BONIFICACION_MODEL')
    private readonly bonificacionModel: Model<Bonificacion>,
    @Inject('BENEFICIARIO_MODEL')
    private readonly beneficiarioModel: Model<Beneficiario>,
    @Inject('FINANCIAMIENTO_ESPERA_FONDOS_MODEL')
    private readonly financiamientoEsperaFondosModel: Model<
      FinanciamientoEsperaFondos
    >,
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private obtenerFechaFinForoProyectoService: ObtenerFechaFinForoProyectoService,
    private actualizaEstadoProyectoService: ActualizaEstadoProyectoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  // Se ejecuta cada DIA A LAS 10PM
  // @Cron(CronExpression.EVERY_DAY_AT_10PM)
  //Se crea el evento de ejecutar balance
  async ejecutarCronVerificarEsperaFondosProyectos() {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let localTime = moment().format('YYYY-MM-DD'); // store localTime
      let fechaFormat = localTime + 'T00:00:00.000Z';
      let formatFechaActual = new Date(fechaFormat);
      //Obteniendo fecha actual
      formatFechaActual.setDate(new Date(fechaFormat).getDate());

      const getFechaFinForo = await this.obtenerFechaFinForoProyectoService.obtenerFechaFinForoProyecto();
      console.log('getFechaFinForo: ', getFechaFinForo);
      if (getFechaFinForo) {
        //Obteniendo fecha actual
        let fechaFinForoFormat = new Date(getFechaFinForo.fecha);
        //Dos dias antes de la fecha de inicio de foro proximo
        fechaFinForoFormat.setDate(
          new Date(getFechaFinForo.fecha).getDate() - 2,
        );
        console.log('fechaFinForoFormat: ', fechaFinForoFormat);
        console.log('formatFechaActual: ', formatFechaActual);
        if (fechaFinForoFormat == formatFechaActual) {
          //Obtiene todos los proyectos
          let getProyectos = await this.proyectoModel.find({
            estado: estadosProyecto.proyectoEnEsperaFondos,
          });
          if (getProyectos.length > 0) {
            for (const proyecto of getProyectos) {
              const idProyecto = proyecto._id;

              //Actualiza el estado del proyecto a activo
              let actualizaProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
                idProyecto,
                estadosProyecto.proyectoEnEsperaFondos,
                estadosProyecto.proyectoActivo,
                null,
                null,
                opts,
              );
              if (actualizaProyecto) {
                let getBonificacion = await this.bonificacionModel.findOne({
                  proyecto: idProyecto,
                });
                if (getBonificacion) {
                  let idEsperaFondos = getBonificacion.esperaFondos;
                  let idTransaccion = getBonificacion.fondosReservados;

                  //Elimina la bonificacion
                  await this.bonificacionModel.updateOne(
                    { _id: getBonificacion._id },
                    {
                      estado: codigosEstadosBonificacion.eliminado,
                      fechaActualizacion: new Date(),
                    },
                    opts,
                  );

                  //Elimina el financiamiento espera de fondos
                  await this.financiamientoEsperaFondosModel.updateOne(
                    { _id: idEsperaFondos },
                    {
                      estado:
                        codigosEstadosFinanciamientoEsperaFondos.eliminado,
                      fechaActualizacion: new Date(),
                    },
                    opts,
                  );

                  //Elimina el beneficiario
                  let getBeneficiario = await this.beneficiarioModel.findOne({
                    estado: codigosEstadosBeneficiario.activa,
                    proyecto: idProyecto,
                  });
                  let idBeneficiario = getBeneficiario._id;
                  if (getBeneficiario) {
                    await this.beneficiarioModel.updateOne(
                      { _id: idBeneficiario },
                      {
                        estado: codigosEstadosBeneficiario.eliminado,
                        fechaActualizacion: new Date(),
                      },
                      opts,
                    );
                  }

                  let getTransaccion = await this.transaccionModel.findOne({
                    _id: idTransaccion,
                  });
                  if (getTransaccion) {
                    //Elimina la transaccion
                    await this.transaccionModel.updateOne(
                      { _id: getTransaccion._id },
                      {
                        estado: estadosTransaccion.eliminada,
                        fechaActualizacion: new Date(),
                      },
                      opts,
                    );

                    //Objeto de la transaccion
                    const transaccionDto = {
                      estado: estadosTransaccion.activa,
                      monto: getTransaccion.monto,
                      moneda: getTransaccion.moneda,
                      descripcion: descripcionTransPagosGazelook.montoSobrante,
                      origen: catalogoOrigen.monto_sobrante,
                      balance: [],
                      beneficiario: null,
                      metodoPago: null,
                      informacionPago: null,
                      usuario: null,
                    };
                    //Se guarda la nueva transaccion
                    let crearTransaccion = await new this.transaccionModel(
                      transaccionDto,
                    ).save(opts);

                    //datos para guardar el historico
                    const newHistoricoTransaccion: any = {
                      datos: crearTransaccion,
                      usuario: '',
                      accion: codigosCatalogoAcciones.crear,
                      entidad: codigoEntidades.entidadTransaccion,
                    };

                    //Crear historico del beneficiario
                    this.crearHistoricoService.crearHistoricoServer(
                      newHistoricoTransaccion,
                    );
                  }
                }
              }
            }
          }

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          // return reparteFondos;
        }
      }

      //Finaliza la transaccion
      await session.endSession();
      return true;
    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
