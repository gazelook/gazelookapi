import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { ConfiguracionBalance } from 'src/drivers/mongoose/interfaces/configuracion_balance/configuracion-balance.interface';
import { ConfiguracionEvento } from 'src/drivers/mongoose/interfaces/configuracion_evento/configuracion-evento.interface';
import { ConfiguracionForo } from 'src/drivers/mongoose/interfaces/configuracion_foro/configuracion-foro.interface';
import { ConfiguracionPreestrategia } from 'src/drivers/mongoose/interfaces/configuracion_preestrategia/configuracion-preestrategia.interface';
import { FormulaEventoService } from 'src/entidades/catalogos/casos_de_uso/formula_evento.service';
import { Evento } from '../../../drivers/mongoose/interfaces/evento/evento.interface';
import {
  codigosConfiguracionBalance,
  codigosConfiguracionEvento,
  codigosConfiguracionPreestrategia,
  codigosFormulasEvento,
  estadoEvento,
  estadosConfiguracionBalance,
  estadosConfiguracionPreestrategia
} from '../../../shared/enum-sistema';

@Injectable()
export class CronEventosService {
  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
    @Inject('CONFIGURACION_EVENTO_MODEL')
    private readonly configEventoModel: Model<ConfiguracionEvento>,
    @Inject('CONFIGURACION_BALANCE_MODEL')
    private readonly configBalanceModel: Model<ConfiguracionBalance>,
    private readonly formulaEventoService: FormulaEventoService
  ) { }

  // Se ejecuta todos los dias a las 10 de la noche
  // @Cron(CronExpression.EVERY_DAY_AT_10PM)
  //Se crea el evento de balance se ejecuta mes a mes
  async crearCronEvento() {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const configEvento = await this.configEventoModel.findOne({
        codigo: codigosConfiguracionEvento.finanzasCONFEVT_2,
      });

      const configBalance = await this.configBalanceModel.find({}).sort({ orden: 1 });

      // si es la primera vez que se crea el evento
      if (!configEvento.fechaProximo && configEvento.ciclico) {

        let semanasBalance
        if (configBalance.length > 0) {
          for (const balance of configBalance) {
            if (balance.orden === 1 && balance.estado === estadosConfiguracionBalance.activa) {
              semanasBalance = balance.semanas
              // actualizar estado de la configuracion CONFBAL_1
              await this.configBalanceModel.updateOne(
                { codigo: codigosConfiguracionBalance.CONFBAL_1 },
                { estado: estadosConfiguracionBalance.inactivo, fechaActualizacion: new Date() },
                opts,
              );
              // actualizar estado de la configuracion CONFBAL_2
              await this.configBalanceModel.updateOne(
                { codigo: codigosConfiguracionBalance.CONFBAL_2 },
                { estado: estadosConfiguracionBalance.activa, fechaActualizacion: new Date() },
                opts,
              );
            }

            if (balance.orden === 2 && balance.estado === estadosConfiguracionBalance.activa) {
              semanasBalance = balance.semanas
              // actualizar estado de la configuracion CONFBAL_2
              await this.configBalanceModel.updateOne(
                { codigo: codigosConfiguracionBalance.CONFBAL_2 },
                { estado: estadosConfiguracionBalance.inactivo, fechaActualizacion: new Date() },
                opts,
              );
              // actualizar estado de la configuracion CONFBAL_3
              await this.configBalanceModel.updateOne(
                { codigo: codigosConfiguracionBalance.CONFBAL_3 },
                { estado: estadosConfiguracionBalance.activa, fechaActualizacion: new Date() },
                opts,
              );
            }


            if (balance.orden === 3 && balance.estado === estadosConfiguracionBalance.activa) {
              semanasBalance = balance.semanas
              // actualizar estado de la configuracion CONFBAL_3
              await this.configBalanceModel.updateOne(
                { codigo: codigosConfiguracionBalance.CONFBAL_3 },
                { estado: estadosConfiguracionBalance.inactivo, fechaActualizacion: new Date() },
                opts,
              );
              // actualizar estado de la configuracion CONFBAL_1
              await this.configBalanceModel.updateOne(
                { codigo: codigosConfiguracionBalance.CONFBAL_1 },
                { estado: estadosConfiguracionBalance.activa, fechaActualizacion: new Date() },
                opts,
              );
            }

          }
        }

        //valor obtenido de la FORMULA SEMANAS X DIAS
        const getForm = await this.formulaEventoService.obtenerFormulaEvento(
          codigosFormulasEvento.semanasxdias,
        );
        //Remmplaza las SEMANAS
        let formulaSemanasxDias = getForm.replace(
          'SEMANAS',
          semanasBalance,
        );
        //Numero de dias
        const getSemanasxDias = eval(formulaSemanasxDias);

        // const intervalo = configEvento.intervalo / (60 * 60 * 24 * 1000); // cada 30 dias se creara un nuevo evento
        const intervalo1 = getSemanasxDias
        const intervalo2 = getSemanasxDias - 1

        const fechaProxima = new Date(configEvento.fechaInicio);
        const fechaFin = new Date(configEvento.fechaInicio);
        fechaProxima.setDate(fechaProxima.getDate() + intervalo1);
        fechaFin.setDate(configEvento.fechaInicio.getDate() + intervalo2);

        // crear evento
        // await this.crearEvento(configEvento.fechaInicio, fechaFin, fechaProxima, intervalo, configEvento._id);

        const evento = {
          estado: estadoEvento.pendiente,
          fechaInicio: configEvento.fechaInicio,
          fechaFin: fechaFin,
          configuracionEvento: codigosConfiguracionEvento.finanzasCONFEVT_2,
          proyectos: null,
        };
        await new this.eventoModel(evento).save(opts);

        // actualizar fechaProximo
        const configEventoProximo = await this.configEventoModel.updateOne(
          { codigo: codigosConfiguracionEvento.finanzasCONFEVT_2 },
          { fechaProximo: fechaProxima, fechaActualizacion: new Date() },
          opts,
        );
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }

      // si existe un fecha proxima del evento, comprueba si le toca actulizar el evnto para la proxima fecha

      if (configEvento.fechaProximo) {

        let fechaSiguiente = moment(configEvento.fechaProximo).format(
          'YYYY-MM-DD',
        );
        //Obtiene la fecha proxima menos siete (5) dias para que se cree un nuevo evento
        //Los 5 dias es para anticipar la creacion del evento
        let formatFechaSiguiente = new Date(fechaSiguiente);
        formatFechaSiguiente.setDate(new Date(fechaSiguiente).getDate() - 5);
        //YYYY-MM-DD

        let fechaActual = moment().format('YYYY-MM-DD');
        let formatFechaActual = new Date(fechaActual);
        console.log('formatFechaActual: ', formatFechaActual)
        console.log('formatFechaSiguiente: ', formatFechaSiguiente)

        if (formatFechaActual.getTime() === formatFechaSiguiente.getTime()) {

          
          let semanasBalance
          if (configBalance.length > 0) {
            for (const balance of configBalance) {
              if (balance.orden === 1 && balance.estado === estadosConfiguracionBalance.activa) {
                semanasBalance = balance.semanas
                // actualizar estado de la configuracion CONFBAL_1
                await this.configBalanceModel.updateOne(
                  { codigo: codigosConfiguracionBalance.CONFBAL_1 },
                  { estado: estadosConfiguracionBalance.inactivo, fechaActualizacion: new Date() },
                  opts,
                );
                // actualizar estado de la configuracion CONFBAL_2
                await this.configBalanceModel.updateOne(
                  { codigo: codigosConfiguracionBalance.CONFBAL_2 },
                  { estado: estadosConfiguracionBalance.activa, fechaActualizacion: new Date() },
                  opts,
                );
              }

              if (balance.orden === 2 && balance.estado === estadosConfiguracionBalance.activa) {
                semanasBalance = balance.semanas
                // actualizar estado de la configuracion CONFBAL_2
                await this.configBalanceModel.updateOne(
                  { codigo: codigosConfiguracionBalance.CONFBAL_2 },
                  { estado: estadosConfiguracionBalance.inactivo, fechaActualizacion: new Date() },
                  opts,
                );
                // actualizar estado de la configuracion CONFBAL_3
                await this.configBalanceModel.updateOne(
                  { codigo: codigosConfiguracionBalance.CONFBAL_3 },
                  { estado: estadosConfiguracionBalance.activa, fechaActualizacion: new Date() },
                  opts,
                );
              }


              if (balance.orden === 3 && balance.estado === estadosConfiguracionBalance.activa) {
                semanasBalance = balance.semanas
                // actualizar estado de la configuracion CONFBAL_3
                await this.configBalanceModel.updateOne(
                  { codigo: codigosConfiguracionBalance.CONFBAL_3 },
                  { estado: estadosConfiguracionBalance.inactivo, fechaActualizacion: new Date() },
                  opts,
                );
                // actualizar estado de la configuracion CONFBAL_1
                await this.configBalanceModel.updateOne(
                  { codigo: codigosConfiguracionBalance.CONFBAL_1 },
                  { estado: estadosConfiguracionBalance.activa, fechaActualizacion: new Date() },
                  opts,
                );
              }

            }
          }

          //valor obtenido de la FORMULA SEMANAS X DIAS
          const getForm = await this.formulaEventoService.obtenerFormulaEvento(
            codigosFormulasEvento.semanasxdias,
          );
          //Remmplaza las SEMANAS
          let formulaSemanasxDias = getForm.replace(
            'SEMANAS',
            semanasBalance,
          );
          //Numero de dias
          const getSemanasxDias = eval(formulaSemanasxDias);
          const intervalo1 = getSemanasxDias
          const intervalo2 = getSemanasxDias - 1

          const fechaInicio = new Date(configEvento.fechaProximo);
          const fechaProxima = new Date(configEvento.fechaProximo);
          const fechaFin = new Date(configEvento.fechaProximo);
          fechaProxima.setDate(fechaProxima.getDate() + intervalo1);
          fechaFin.setDate(configEvento.fechaProximo.getDate() + intervalo2);

          // await this.crearEvento(fechaInicio, fechaFin, intervalo, fechaProxima, configEvento._id);
          const evento = {
            estado: estadoEvento.pendiente,
            fechaInicio: fechaInicio,
            fechaFin: fechaFin,
            configuracionEvento: codigosConfiguracionEvento.finanzasCONFEVT_2,
            proyectos: null,
          };
          await new this.eventoModel(evento).save(opts);

          await this.configEventoModel.updateOne(
            { codigo: codigosConfiguracionEvento.finanzasCONFEVT_2 },
            { fechaProximo: fechaProxima, fechaActualizacion: new Date() },
            opts,
          );
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
        } else {
          //Finaliza la transaccion
          await session.endSession();
        }
      }
    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
