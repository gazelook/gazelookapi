import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Balance } from 'src/drivers/mongoose/interfaces/balance/balance.interface';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoOrigenService } from 'src/entidades/catalogos/casos_de_uso/catalogo-origen.service';
import { ConfiguracionEventoService } from 'src/entidades/catalogos/casos_de_uso/configuracion-evento.service';
import { FormulaEventoService } from 'src/entidades/catalogos/casos_de_uso/formula_evento.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { v4 as uuidv4 } from 'uuid';
import {
  catalogoOrigen,
  catalogoTipoBalance,
  codigoDeBalance,
  codigosConfiguracionEvento,
  codigosFormulasEvento,
  codigosMetodosPago,
  estadosTransaccion,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from './../../../shared/enum-sistema';

@Injectable()
export class CrearBalanceService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    @Inject('BALANCE_MODEL')
    private readonly balanceModel: Model<Balance>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private configuracionEventoService: ConfiguracionEventoService,
    private formulaEventoService: FormulaEventoService,
    private catalogoOrigenService: CatalogoOrigenService,
  ) { }

  //crea el balance de fondos disponibles para usuarios y fondos disponibles para gazelook
  async crearBalance(opts): Promise<any> {
    try {
      //entidad balance
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.balance,
      );

      //estado activa de le entidad balance
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      //Accion crear
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );

      let totalMontoIngreso = 0;
      let arrayTransaccion = [];
      let arrayTransaccionFondosGrazelook = [];
      let totalMontoEgreso = 0;
      let fondosReservadosGaazelook = 0;
      let arrayTransaccionValorExtra = [];
      let arrayTransaccionMontoSobrante = [];
      let codigoBalance = codigoDeBalance.bal_.concat(uuidv4());
      let totalMontoIngresoValorExtra = 0;
      let totalMontoIngresoMontoSobrante = 0;
      // let getValorActualAnteriorUsuarios = 0;
      // let getValorActualAnteriorGazelook = 0;
      let getBalanceAnteriorUsuariosId: any;
      let getBalanceAnteriorGazelookId: any;

      // //Obtener valor actual del balance anterior de usuarios
      // let getBalanceAnteriorUsuarios = await this.balanceModel
      //   .find({ tipo: catalogoTipoBalance.fondos_usuario })
      //   .sort('-fechaCreacion')
      //   .limit(1);

      // if (getBalanceAnteriorUsuarios.length > 0) {
      //   if (getBalanceAnteriorUsuarios[0].reparticionFondos === false) {
      //     getValorActualAnteriorUsuarios =
      //       getBalanceAnteriorUsuarios[0].valorActual;
      //     getBalanceAnteriorUsuariosId = getBalanceAnteriorUsuarios[0]._id;
      //   }
      // }

      // //Obtener valor actual del balance anterior de gazelook
      // let getBalanceAnteriorGazelook = await this.balanceModel
      //   .find({ tipo: catalogoTipoBalance.fondos_gazelook })
      //   .sort('-fechaCreacion')
      //   .limit(1);

      // if (getBalanceAnteriorGazelook.length > 0) {
      //   getValorActualAnteriorGazelook =
      //     getBalanceAnteriorGazelook[0].valorActual;
      //   getBalanceAnteriorGazelookId = getBalanceAnteriorGazelook[0]._id;
      // }

      //Obtengo los totales ingresos y egresos actuales de transacciones
      const getFondos = await this.transaccionModel.find({
        estado: estadosTransaccion.activa,
      });

      if (getFondos.length > 0) {
        for (const getTransaccion of getFondos) {
          let catOrigen = getTransaccion.origen;
          let metodoPago = getTransaccion.metodoPago;
          let catDestino = getTransaccion.destino;
          //Verificar que la transaccion ya no se haya ejecutado con ningun balance
          if (getTransaccion.balance.length === 0) {
            // if (metodoPago === codigosMetodosPago.stripe) {
            if (getTransaccion.totalRecibido) {
              //Verifica que la trasaccion sea de tipo suscripcion
              // const getOrigenIngreso = await this.catalogoOrigenService.obtenerCatalogoOrigenIngreso(catOrigen)
              if (catOrigen === catalogoOrigen.suscripciones) {
                let getIdTransaccion: any;
                //Obtener ID de transaccion
                getIdTransaccion = getTransaccion._id;
                //LLenando array de transacciones
                arrayTransaccion.push(getIdTransaccion);
                //LLenando array de transacciones de gazelook
                arrayTransaccionFondosGrazelook.push(getIdTransaccion);
                //Obteniendo monto de la transaccion
                const getFondoMonto = getTransaccion.totalRecibido;
                //sumando monto de la transaccion conforme se va recorriendo el for
                totalMontoIngreso = getFondoMonto + totalMontoIngreso;
              }

              // }

              // if (metodoPago === codigosMetodosPago.stripe) {

              if (catOrigen === catalogoOrigen.valor_extra) {
                //Verifica que la trasaccion sea de tipo ingreso y valor extra
                const getOrigenIngresoValorExtra = await this.catalogoOrigenService.obtenerCatalogoOrigenValorExtra(
                  catOrigen,
                );
                if (getOrigenIngresoValorExtra) {
                  let getIdTransaccion: any;
                  //Obtener ID de transaccion
                  getIdTransaccion = getTransaccion._id;
                  //LLenando array de transacciones
                  arrayTransaccion.push(getIdTransaccion);
                  //Obteniendo monto de la transaccion
                  const getFondoMontoValorExtra =
                    getTransaccion.totalRecibido;
                  //sumando monto de la transaccion conforme se va recorriendo el for
                  totalMontoIngresoValorExtra =
                    getFondoMontoValorExtra + totalMontoIngresoValorExtra;
                }
              }

              if (catDestino === catalogoOrigen.fondos_reservados_gazelook) {
                let getIdTransaccion: any;
                //Obtener ID de transaccion
                getIdTransaccion = getTransaccion._id;
                //LLenando array de transacciones
                arrayTransaccionFondosGrazelook.push(getIdTransaccion);
                //Obteniendo monto de la transaccion
                const getFondosReservadosGazelook =
                  getTransaccion.totalRecibido;
                //sumando monto de la transaccion conforme se va recorriendo el for
                fondosReservadosGaazelook =
                  getFondosReservadosGazelook + fondosReservadosGaazelook;
              }
            }
            // }

            if (catOrigen === catalogoOrigen.monto_sobrante) {
              //Verifica que la trasaccion sea de tipo ingreso y monto sobrante
              // const getOrigenIngresoMontoSobrante = await this.catalogoOrigenService.obtenerCatalogoOrigenIngreso(catOrigen)
              // if (getOrigenIngresoMontoSobrante) {

              let getIdTransaccion: any;
              //Obtener ID de transaccion
              getIdTransaccion = getTransaccion._id;
              //LLenando array de transacciones
              arrayTransaccion.push(getIdTransaccion);
              //Obteniendo monto de la transaccion
              const getFondoMontoMontoSobrante = getTransaccion.monto;
              //sumando monto de la transaccion conforme se va recorriendo el for
              totalMontoIngresoMontoSobrante =
                getFondoMontoMontoSobrante + totalMontoIngresoMontoSobrante;
              // }
            }

            //Verifica que la trasaccion sea de tipo egreso (gastos operativos) Payments
            // if (
            //   metodoPago === codigosMetodosPago.payments1 ||
            //   metodoPago === codigosMetodosPago.payments2
            // ) {
            if (getTransaccion.totalRecibido) {
              // const getOrigenEgreso = await this.catalogoOrigenService.obtenerCatalogoOrigenEgreso(catOrigen)
              if (catDestino === catalogoOrigen.gastos_operativos) {
                let getIdTransaccion: any;
                //Obtener ID de transaccion
                getIdTransaccion = getTransaccion._id;
                //LLenando array de transacciones
                arrayTransaccion.push(getIdTransaccion);
                //Obteniendo monto de la transaccion
                const getFondoMonto = getTransaccion.totalRecibido;
                //sumando monto de la transaccion conforme se va recorriendo el for
                totalMontoEgreso = getFondoMonto + totalMontoEgreso;
              }

              // const getOrigenEgreso = await this.catalogoOrigenService.obtenerCatalogoOrigenEgreso(catOrigen)
            }
            // }
          }
        }
      }

      //Generar el balance de fondos disponibles para usuarios, tomando el 75% de los fondos reunidos antes del corte
      let getFondosUsuarios;
      //Generar el balance de fondos disponibles para GazeLook, tomando el 25% de los fondos reunidos antes del corte
      let getFondosGazelook;

      //total fondos para repartir
      let totalFondos = totalMontoIngreso - totalMontoEgreso;


      //Obtengo la configuracion del evento
      const getConfEvento = await this.configuracionEventoService.obtenerConfiguracionEvento(
        codigosConfiguracionEvento.finanzasCONFEVT_2,
      );

      for (let i = 0; i < getConfEvento.formulas.length; i++) {
        let getFormulaEvento: any;
        const getCodFormula = getConfEvento.formulas[i];
        //Obtengo la formula del evento
        getFormulaEvento = await this.formulaEventoService.obtenerFormulaEvento(
          getCodFormula,
        );

        let formula;
        //Obteniendo formula para balance de usuarios
        if (getCodFormula === codigosFormulasEvento.fondos_balance_usuarios) {
          //Remmplazo el total de monto
          formula = getFormulaEvento.replace('TOTAL_MONTO', totalFondos);
          getFondosUsuarios = eval(formula);
          // getFondosUsuarios =
          //   getFondosUsuarios + getValorActualAnteriorUsuarios;
          getFondosUsuarios = getFondosUsuarios + totalMontoIngresoValorExtra;
          getFondosUsuarios =
            getFondosUsuarios + totalMontoIngresoMontoSobrante;
        }

        //Obteniendo formula para balance de gazelook
        if (getCodFormula === codigosFormulasEvento.fondos_balance_gazelook) {
          //Remmplazo el total de monto
          formula = getFormulaEvento.replace('TOTAL_MONTO', totalFondos);
          getFondosGazelook = eval(formula);
          // getFondosGazelook =
          //   getFondosGazelook + getValorActualAnteriorGazelook;
          getFondosGazelook = getFondosGazelook + fondosReservadosGaazelook;
        }
      }

      //Guardar informacion de balance para fondos de usuario
      const BalanceUsuariosDto = {
        estado: estado.codigo,
        valorActual: getFondosUsuarios,
        totalIngreso: totalMontoIngreso,
        totalEgreso: totalMontoEgreso,
        totalIngresosExtra: totalMontoIngresoValorExtra,
        totalMontoSobrante: totalMontoIngresoMontoSobrante,
        reparticionFondos: false,
        proviene: getBalanceAnteriorUsuariosId || null,
        transacciones: arrayTransaccion,
        tipo: catalogoTipoBalance.fondos_usuario,
        codigo: codigoBalance,
      };

      //Guarda el balance de usuarios
      const balanceUsuarios = new this.balanceModel(BalanceUsuariosDto);
      const crearBalanceUsuarios = await balanceUsuarios.save(opts);

      //datos para guardar el historico del balance de fondos de usuarios
      const newHistoricoBalanceUsuarios: any = {
        datos: crearBalanceUsuarios,
        usuario: '',
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para balance de fondos de usuarios
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoBalanceUsuarios,
      );

      //Actualiza el balance de la transaccion
      if (arrayTransaccion.length > 0) {
        for (const getTransaccionId of arrayTransaccion) {
          await this.transaccionModel.updateOne(
            { _id: getTransaccionId },
            {
              $push: { balance: crearBalanceUsuarios._id },
              fechaActualizacion: new Date(),
            },
            opts,
          );
        }
      }

      //Guardar informacion de balance para fondos de gazelook
      const BalanceGazelookDto = {
        estado: estado.codigo,
        valorActual: getFondosGazelook,
        totalIngreso: totalMontoIngreso,
        totalEgreso: 0,
        totalIngresosExtra: 0,
        totalMontoSobrante: 0,
        reparticionFondos: false,
        proviene: getBalanceAnteriorGazelookId || null,
        transacciones: arrayTransaccionFondosGrazelook,
        tipo: catalogoTipoBalance.fondos_gazelook,
        codigo: codigoBalance,
      };

      //Guarda el balance de fondos de gazelook
      const balanceGazelook = new this.balanceModel(BalanceGazelookDto);
      const crearBalanceGazelook = await balanceGazelook.save(opts);

      //datos para guardar el historico del balance de gazelook
      const newHistoricoBalanceGazelook: any = {
        datos: crearBalanceGazelook,
        usuario: '',
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para el balance de gazelook
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoBalanceGazelook,
      );

      //Actualiza el balance de la transaccion
      if (arrayTransaccionFondosGrazelook.length > 0) {
        for (const getTransaccionId of arrayTransaccionFondosGrazelook) {
          await this.transaccionModel.updateOne(
            { _id: getTransaccionId },
            {
              $push: { balance: crearBalanceGazelook._id },
              fechaActualizacion: new Date(),
            },
            opts,
          );
        }
      }

      return crearBalanceUsuarios;
    } catch (error) {
      throw error;
    }
  }
}
