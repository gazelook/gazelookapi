import { GastoOperacionService } from './crear-gasto-operacion.service';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { ObtenerGastosOperacionesService } from './obtener-gastos-operaciones.service';
import { CronEventosService } from './cron-eventos-balance.service';
import { finanzasProviders } from './../drivers/finanzas.provider';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CrearBalanceService } from './crear-balance.service';
import { ProyectosServicesModule } from 'src/entidades/proyectos/casos_de_uso/proyectos-services.module';
import { EjecutarCronRepartirFondosProyectosService } from './ejecutar-cron-repartir-fondos-proyectos.service';
import { RepartirFondosService } from './repartir-fondos.service';
import { TransaccionServicesModule } from 'src/entidades/transaccion/casos_de_uso/transaccion-services.module';
import { EjecutarCronFinanzasService } from './ejecutar-cron-crea-balance.service';
import { EjecutarCronVerificarEsperaFondosProyectosService } from './ejecutar-cron-verificar-espera-fondos-proyectos.service';

@Module({
  imports: [
    DBModule,
    PerfilServiceModule,
    forwardRef(() => ProyectosServicesModule),
    forwardRef(() => CatalogosServiceModule),
    forwardRef(() => TransaccionServicesModule),
  ],
  providers: [
    ...finanzasProviders,
    GastoOperacionService,
    CronEventosService,
    ObtenerGastosOperacionesService,
    CrearBalanceService,
    EjecutarCronRepartirFondosProyectosService,
    RepartirFondosService,
    EjecutarCronFinanzasService,
    EjecutarCronVerificarEsperaFondosProyectosService,
  ],
  exports: [
    ...finanzasProviders,
    GastoOperacionService,
    CronEventosService,
    ObtenerGastosOperacionesService,
    CrearBalanceService,
    EjecutarCronRepartirFondosProyectosService,
    EjecutarCronFinanzasService,
    RepartirFondosService,
    EjecutarCronVerificarEsperaFondosProyectosService,
  ],
  controllers: [],
})
export class FinanzasServicesModule {}
