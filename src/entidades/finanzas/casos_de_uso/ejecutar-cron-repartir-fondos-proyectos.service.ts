import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Evento } from 'src/drivers/mongoose/interfaces/evento/evento.interface';
import { ObtenerFechaFinForoProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-fecha-fin-foro-proyecto.service';
import { RepartirFondosService } from './repartir-fondos.service';

@Injectable()
export class EjecutarCronRepartirFondosProyectosService {
  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
    private obtenerFechaFinForoProyectoService: ObtenerFechaFinForoProyectoService,
    private repartirFondosService: RepartirFondosService,
  ) { }

  // Se ejecuta todos los dias a las 11 de la noche
  // @Cron(CronExpression.EVERY_DAY_AT_11PM)
  //Se crea el evento de ejecutar balance
  async ejecutarCronRepartirFondosProyectos() {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let localTime = moment().format('YYYY-MM-DD'); // store localTime
      let fechaFormat = localTime + 'T00:00:00.000Z';
      let formatFechaActual = new Date(fechaFormat);
      //Obteniendo fecha actual
      formatFechaActual.setDate(new Date(fechaFormat).getDate());

      const getFechaFinForo = await this.obtenerFechaFinForoProyectoService.obtenerFechaFinForoProyecto();

      if (getFechaFinForo) {
        // console.log('getFechaFinForo.fecha: ', getFechaFinForo.fecha)
        // console.log('formatFechaActual: ', formatFechaActual)
        if (getFechaFinForo.fecha == formatFechaActual) {

          //reparte los fondos a los proyectos mas votados por el tipo de proyecto
          let reparteFondos = await this.repartirFondosService.repartirFondosProyectos(
            getFechaFinForo.evento._id,
            opts,
          );

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          return reparteFondos;
        }
      }

      //Finaliza la transaccion
      await session.endSession();
      return true;
    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
