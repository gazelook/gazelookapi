import { ScheduleModule } from '@nestjs/schedule';
import { FinanzasControllerModule } from './controladores/finanzas-controller.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [FinanzasControllerModule, ScheduleModule.forRoot()],
  providers: [],
  controllers: [],
})
export class FinanzasModule {}
