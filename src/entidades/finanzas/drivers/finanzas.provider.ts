import { ConfiguracionEventoModelo } from 'src/drivers/mongoose/modelos/configuracion_evento/configuracion-evento.schema';
import { EventoModelo } from './../../../drivers/mongoose/modelos/evento/evento.schema';
import { entidadesModelo } from './../../../drivers/mongoose/modelos/catalogoEntidades/catalogoEntidadesModelo';
import { Connection } from 'mongoose';
import { TransaccionModelo } from 'src/drivers/mongoose/modelos/transaccion/transaccion.schema';
import { BalanceModelo } from 'src/drivers/mongoose/modelos/balance/balance.schema';
import { BonificacionModelo } from 'src/drivers/mongoose/modelos/bonificacion/bonificacion.schema';
import { BeneficiarioModelo } from 'src/drivers/mongoose/modelos/beneficiario/beneficiario.schema';
import { FondosTipoProyectoModelo } from 'src/drivers/mongoose/modelos/fondos_tipo_proyecto/fondos-tipo-proyecto.schema';
import { FondosFinanciamientoModelo } from 'src/drivers/mongoose/modelos/fondos_financiamiento/fondos-financiamiento.schema';
import { FinanciamientoEsperaFondosModelo } from 'src/drivers/mongoose/modelos/financiamiento_espera_fondos/financiamiento-espera-fondos.schema';
import { ProyectoModelo } from 'src/drivers/mongoose/modelos/proyectos/proyecto.schema';
import { CatalogoPorcentajeFondosReservadosModelo } from 'src/drivers/mongoose/modelos/catalogo_porcentaje_fondos_reservados/catalogo-porcentaje-fondos-reservados.schema';
import { ConfiguracionForoModelo } from 'src/drivers/mongoose/modelos/configuracion_foro/configuracion-foro.schema';
import { ConfiguracionBalanceModelo } from 'src/drivers/mongoose/modelos/configuracion_balance/configuracion-balance.schema';
import { ConfiguracionPreestrategiaModelo } from 'src/drivers/mongoose/modelos/configuracion_preestrategia/configuracion-preestrategia.schema';

export const finanzasProviders = [
  {
    provide: 'TRANSACCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('transaccion', TransaccionModelo, 'transaccion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ESTADOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('catalogo_estados', entidadesModelo, 'catalogo_estados'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'EVENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('evento', EventoModelo, 'evento'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_EVENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_evento',
        ConfiguracionEventoModelo,
        'configuracion_evento',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'BALANCE_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('balance', BalanceModelo, 'balance'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'BONIFICACION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('bonificacion', BonificacionModelo, 'bonificacion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'BENEFICIARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('beneficiario', BeneficiarioModelo, 'beneficiario'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'FONDOS_TIPO_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'fondos_tipo_proyecto',
        FondosTipoProyectoModelo,
        'fondos_tipo_proyecto',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'FONDOS_FINANCIAMIENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'fondos_financiamiento',
        FondosFinanciamientoModelo,
        'fondos_financiamiento',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'FINANCIAMIENTO_ESPERA_FONDOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'financiamiento_espera_fondos',
        FinanciamientoEsperaFondosModelo,
        'financiamiento_espera_fondos',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('proyecto', ProyectoModelo, 'proyecto'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_PORCENTAJE_FONDOS_RESERVADOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_porcentaje_fondos_reservados',
        CatalogoPorcentajeFondosReservadosModelo,
        'catalogo_porcentaje_fondos_reservados',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_PREESTRATEGIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_preestrategia',
        ConfiguracionPreestrategiaModelo,
        'configuracion_preestrategia',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_FORO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_foro',
        ConfiguracionForoModelo,
        'configuracion_foro',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_BALANCE_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_balance',
        ConfiguracionBalanceModelo,
        'configuracion_balance',
      ),
    inject: ['DB_CONNECTION'],
  },
];
