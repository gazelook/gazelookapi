import { AuthGuard } from '@nestjs/passport';
import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiBody,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Headers,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import * as mongoose from 'mongoose';
import { TransferirProyectoService } from '../casos_de_uso/trasferir-proyecto.service';
import { Funcion } from 'src/shared/funcion';
import { TrasferenciaOkDto } from '../entidad/transferencia-ok.dto';

@ApiTags('Proyectos')
@Controller('api/transferencia-proyecto-ok')
export class TransferenciaOkControlador {
  constructor(
    private readonly transferirProyectoService: TransferirProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: TrasferenciaOkDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Trasfiere el proyecto a otro perfil' })
  @ApiResponse({ status: 201, description: 'El proyecto ha sido transferido' })
  @ApiResponse({ status: 404, description: 'El email no puede enviarse' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async transferirProyecto(
    @Headers() headers,
    @Body() dataTransferir: TrasferenciaOkDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(dataTransferir.idProyecto) &&
        mongoose.isValidObjectId(dataTransferir.idPerfilNuevo) &&
        mongoose.isValidObjectId(dataTransferir.idPerfilPropietario)
        // && dataTransferir.idioma
        // && dataTransferir.token
      ) {
        const transProyecto = await this.transferirProyectoService.trasferirProyecto(
          dataTransferir.idProyecto,
          dataTransferir.idPerfilPropietario,
          dataTransferir.idPerfilNuevo,
        );
        if (transProyecto) {
          const TRANSFERIR_PROYECTO = await this.i18n.translate(
            codIdioma.concat('.TRANSFERIR_PROYECTO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: TRANSFERIR_PROYECTO,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
