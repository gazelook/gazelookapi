import { ProyectoResumidoPaginacionResponseDto } from './../entidad/proyecto-response-dto';
import { Response } from 'express';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Res,
  HttpStatus,
  Get,
  Headers,
  Query,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { ObtenerProyectosRecientePaginacionService } from '../casos_de_uso/obtener-proyectos-recientes-paginacion.service';
import * as mongoose from 'mongoose';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Proyectos')
@Controller('api/proyectos-recientes')
export class ObtenerProyectoRecientesPaginacionControlador {
  constructor(
    private readonly obtenerProyectosRecientePaginacionService: ObtenerProyectosRecientePaginacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary:
      'Obtiene una lista de Proyectos, en un rango de fechas con paginacion los filtros son {nuevos_proyectos}, {mas_votados} {menos_votados}',
  })
  @ApiResponse({
    status: 200,
    type: ProyectoResumidoPaginacionResponseDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerProyectosRecientes(
    @Headers() headers,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Query('filtro') filtro: string,
    @Query('tipo') tipo: string,
    @Query('perfil') perfil: string,
    @Query('estado') estado: string,
    @Query('fechaInicial') fechaInicial: Date,
    @Query('fechaFinal') fechaFinal: Date,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(perfil) &&
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0 &&
        filtro &&
        tipo
      ) {
        const proyectos = await this.obtenerProyectosRecientePaginacionService.obtenerProyectoRecientePaginacion(
          codIdioma,
          limite,
          pagina,
          filtro,
          tipo,
          perfil,
          estado,
          fechaInicial,
          fechaFinal,
          response,
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: proyectos,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
