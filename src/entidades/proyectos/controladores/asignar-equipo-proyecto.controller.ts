import {
  Body, Controller, Headers, HttpStatus, Post, UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AsignarEquipoProyectoService } from '../casos_de_uso/asignar-equipo-proyecto.service';
import { EquipoProyectoDto } from '../entidad/equipo-proyecto-dto';

@ApiTags('Proyectos')
@Controller('api/proyecto')
export class AsignarEquipoControlador {
  constructor(
    private readonly asignarEquipoProyectoService: AsignarEquipoProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) { }

  @Post('/asignar-equipo')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Asigna el equipo del proyecto' })
  @ApiResponse({ status: 201, description: 'Creación correcta' })
  @ApiResponse({ status: 404, description: 'Error al crear' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async crearProyecto(
    @Headers() headers,
    @Body() equipoProyectoDto: EquipoProyectoDto
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(equipoProyectoDto.idPerfil) &&
        mongoose.isValidObjectId(equipoProyectoDto.idProyecto) &&
        equipoProyectoDto.perfiles.length > 0
      ) {
        const asignarRol = await this.asignarEquipoProyectoService.asignarEquipo(
          equipoProyectoDto.idProyecto,
          equipoProyectoDto.idPerfil,
          equipoProyectoDto.perfiles,
        );

        if (asignarRol) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
