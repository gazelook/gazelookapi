import {
  Controller, Get,
  Headers, HttpStatus, Query, Res, UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation, ApiResponse, ApiSecurity, ApiTags
} from '@nestjs/swagger';
import { Response } from 'express';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerTodosProyectosPaginacionService } from '../casos_de_uso/obtener-todos-proyectos-paginacion.service';
import { ProyectoResumidoPaginacionResponseDto } from './../entidad/proyecto-response-dto';

@ApiTags('Proyectos')
@Controller('api/proyectos-all')
export class ObtenerTodosProyectosPaginacionControlador {
  constructor(
    private readonly obtenerTodosProyectosPaginacionService: ObtenerTodosProyectosPaginacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary:
      'Obtiene todos los Proyectos con paginacion',
  })
  @ApiResponse({
    status: 200,
    type: ProyectoResumidoPaginacionResponseDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerProyectosRecientes(
    @Headers() headers,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0
      ) {
        const proyectos = await this.obtenerTodosProyectosPaginacionService.obtenerTodosProyectosPaginacion(
          codIdioma,
          limite,
          pagina,
          response,
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: proyectos,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
