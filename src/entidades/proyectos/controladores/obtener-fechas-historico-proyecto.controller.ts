import { Response } from 'express';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Res,
  HttpStatus,
  Get,
  Headers,
  Query,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import * as mongoose from 'mongoose';
import { ObtenerFechasHistoricoProyectoService } from '../casos_de_uso/obtener-fechas-historico-proyecto.service';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Proyectos')
@Controller('api/fechas-historico-proyecto')
export class ObtenerFechasHistoricoProyectoControlador {
  constructor(
    private readonly obtenerFechasHistoricoProyectoService: ObtenerFechasHistoricoProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Obtiene la lista de historicos de un proyecto' })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerFechasHistoricoProyecto(
    @Headers() headers,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Query('idProyecto') idProyecto: string,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(idProyecto) &&
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0
      ) {
        const proyectos = await this.obtenerFechasHistoricoProyectoService.obtenerFechasHistoricoProyecto(
          idProyecto,
          limite,
          pagina,
          response,
          codIdioma,
        );
        return proyectos;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
