import { ProyectoResumidoPaginacionResponseDto } from './../entidad/proyecto-response-dto';
import { HadersInterfaceNombres } from '../../../shared/header-response-interface';
import { Response } from 'express';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Res,
  HttpStatus,
  Get,
  Headers,
  Query,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import * as mongoose from 'mongoose';
import { AuthGuard } from '@nestjs/passport';
import { ObtenerProyectosRecomendadosPaginacionService } from '../casos_de_uso/obtener-proyectos-recomendados-paginacion.service';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Proyectos')
@Controller('api/proyectos-recomendados')
export class ObtenerProyectoReomendadoPaginacionControlador {
  constructor(
    private readonly obtenerProyectosRecomendadosPaginacionService: ObtenerProyectosRecomendadosPaginacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary:
      'Obtiene una lista de Proyectos recomendados por gazelook (recomendado por gazelook: true si no false)',
  })
  @ApiResponse({
    status: 200,
    type: ProyectoResumidoPaginacionResponseDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerProyectosRecomendados(
    @Headers() headers,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Query('tipo') tipo: string,
    @Query('perfil') perfil: string,
    @Res() response: Response,
  ) {
    const headerNombre = new HadersInterfaceNombres();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(perfil) &&
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0 &&
        tipo
      ) {
        const proyectos = await this.obtenerProyectosRecomendadosPaginacionService.obtenerProyectoRecomendadosPaginacion(
          codIdioma,
          limite,
          pagina,
          tipo,
          perfil,
          response,
        );

        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: proyectos,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }

  calcularDias(fechaActual, fechaActualizacion) {
    let dias =
      (fechaActual.getTime() - fechaActualizacion.getTime()) /
      (60 * 60 * 24 * 1000);
    if (dias <= 3) {
      return true;
    } else return false;
  }
}
