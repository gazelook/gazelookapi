import { I18nService } from 'nestjs-i18n';
import { HadersInterfaceNombres } from '../../../shared/header-response-interface';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Headers,
  UseGuards,
  Get,
  Res,
  Query,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import * as mongoose from 'mongoose';
import { ListaCoAutoresService } from '../casos_de_uso/obtener-lista-co-autores.service';
import { Response } from 'express';
import { ListaCoAutoresDto } from '../entidad/lista-coautores-dto';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Proyectos')
@Controller('api/lista-co-autores')
export class ListaCoAutoresControlador {
  constructor(
    private readonly listaCoAutoresService: ListaCoAutoresService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Obtiene una lista de coautores de un proyecto' })
  @ApiResponse({ status: 200, type: ListaCoAutoresDto, description: 'OK' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async listaCoAutoresProyecto(
    @Headers() headers,
    @Query('idPerfil') idPerfil: string,
    @Query('idProyecto') idProyecto: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const headerNombre = new HadersInterfaceNombres();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(idPerfil) &&
        mongoose.isValidObjectId(idProyecto) &&
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0
      ) {
        const listaCoAutores = await this.listaCoAutoresService.obtenerListaCoAutores(
          idProyecto,
          idPerfil,
          codIdioma,
          limite,
          pagina,
        );

        if (listaCoAutores.docs.length > 0) {
          for (let index = 0; index < listaCoAutores.docs.length; index++) {
            delete listaCoAutores.docs[index].id;
          }
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: listaCoAutores.docs,
          });

          response.set(headerNombre.totalDatos, listaCoAutores.totalDocs);
          response.set(headerNombre.totalPaginas, listaCoAutores.totalPages);
          response.set(headerNombre.proximaPagina, listaCoAutores.hasNextPage);
          response.set(headerNombre.anteriorPagina, listaCoAutores.hasPrevPage);

          response.send(respuesta);
          return respuesta;
        } else {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: listaCoAutores.docs,
          });
          response.send(respuesta);
          return respuesta;
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
