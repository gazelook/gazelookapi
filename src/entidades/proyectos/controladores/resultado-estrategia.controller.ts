import { AuthGuard } from '@nestjs/passport';
import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiBody,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  HttpException,
  Inject,
  Get,
  Headers,
  Param,
  BadRequestException,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { CrearProyectoService } from '../casos_de_uso/crear-proyecto.service';
import { CrearProyectoDto } from '../entidad/crear-proyecto-dto';
import { ProyectoDto } from '../entidad/proyecto-dto';
import * as mongoose from 'mongoose';
import { Funcion } from 'src/shared/funcion';
import { CrearEstrategiaDto } from '../entidad/estrategia-dto';
import { ResultadoEstrategiaProyectoService } from '../casos_de_uso/resultado-estrategia.service';

@ApiTags('Proyectos')
@Controller('api/proyecto')
export class ResultadoEstrategiaControlador {
  constructor(
    private readonly resultadoEstrategiaProyecto: ResultadoEstrategiaProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/crear-resultado-estrategia')
  @ApiSecurity('Authorization')
  @ApiBody({ type: CrearEstrategiaDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Crea un nuevo resultado de la estrategia para un proyecto',
  })
  @ApiResponse({ status: 201, description: 'Creación correcta' })
  @ApiResponse({ status: 404, description: 'Error al crear' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async resultadoEstrategiaConcluida(
    @Headers() headers,
    @Body() crearEstrategiaDTO: CrearEstrategiaDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(crearEstrategiaDTO.perfil._id) &&
        mongoose.isValidObjectId(crearEstrategiaDTO.proyecto._id) &&
        crearEstrategiaDTO.estadoEstrategia.codigo
        // && crearEstrategiaDTO.adjuntos[0]._id
      ) {
        const estrategia = await this.resultadoEstrategiaProyecto.resultadoEstrategiaConcluida(
          crearEstrategiaDTO.proyecto._id,
          crearEstrategiaDTO.perfil._id,
          crearEstrategiaDTO.adjuntos,
          crearEstrategiaDTO.estadoEstrategia.codigo,
        );

        if (estrategia) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
            datos: { _id: estrategia._id },
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
