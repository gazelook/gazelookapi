import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Get,
  Headers,
  UseGuards,
} from '@nestjs/common';
import { CatalogoTipoProyectoService } from '../casos_de_uso/obtener-tipo-proyecto.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { CatalogoTipoProyectoDto } from '../entidad/obtener-tipo-proyecto-dto';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Proyectos')
@Controller('api/tipo-proyecto')
export class ObtenerCatalogoTipoProyectoControlador {
  constructor(
    private readonly catalogoTipoProyectoService: CatalogoTipoProyectoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Obtiene los tipos de proyectos segun el idioma enviado',
  })
  @ApiResponse({
    status: 200,
    type: CatalogoTipoProyectoDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  //@UseGuards(AuthGuard('jwt'))
  public async obtenerCatalogoTipoProyecto(@Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const catalogoTipoProyecto = await this.catalogoTipoProyectoService.obtenerCatalogoTipoProyecto(
        codIdioma,
      );

      if (!catalogoTipoProyecto || catalogoTipoProyecto.length === 0) {
        //llama al metodo de traduccion dinamica
        const ERROR_OBTENER = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_OBTENER',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ERROR_OBTENER,
        });
      }
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: catalogoTipoProyecto,
      });
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
