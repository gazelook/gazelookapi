import { ObtenerProyectosTitulosControlador } from './obtener-proyecto-titulos.controller';
import { forwardRef, Module } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { CrearProyectoControlador } from './crear-proyecto.controller';
import { ProyectosServicesModule } from '../casos_de_uso/proyectos-services.module';
import { ObtenerCatalogoTipoProyectoControlador } from './obtener-catalogo-tipo-proyecto.controller';
import { VotoProyectoUnicoControlador } from './voto-proyecto-unico.controller';
import { ActualizarProyectoControlador } from './actualizar-proyecto.controller';
import { ObtenerProyectoRecientesPaginacionControlador } from './obtener-proyecto-reciente-paginacion.controller';
import { ObtenerProyectoUnicaControlador } from './obtener-proyecto-unico.controller';
import { EliminarProyectoUnicoControlador } from './eliminar-proyecto-unico.controller';
import { ListaCoAutoresControlador } from './obtener-lista-co-autores.controller';
import { TransferirProyectoControlador } from './trasferir-proyecto.controller';
import { ObtenerProyectosRangoControlador } from './obtener-proyectos-rango.controller';
import { ObtenerProyectoReomendadoPaginacionControlador } from './obtener-proyecto-recomendado-paginacion.controller';
import { ObtenerFechasHistoricoProyectoControlador } from './obtener-fechas-historico-proyecto.controller';
import { Funcion } from 'src/shared/funcion';
import { TransferenciaOkControlador } from './transferencia-ok.controller';
import { ModificarValorEstimadoProyectoControlador } from './modificar-valor-estimado-proyecto.controller';
import { ObtenerProyectosPerfilControlador } from './obtener-proyectos-perfil.controller';
import { NotificacionServicesModule } from 'src/entidades/notificacion/casos_de_uso/notificacion-services.module';
import { ObtenerFechaInicioForoProyectoControlador } from './obtener-fecha-inicio-foro-proyecto.controller';
import { ObtenerFechaFinForoProyectoControlador } from './obtener-fecha-fin-foro-proyecto.controller';
import { AsignarRolesControlador } from './asignar-roles-proyecto.controller';
import { ResultadoEstrategiaControlador } from './resultado-estrategia.controller';
import { ObtenerResultadoEstrategiaControlador } from './obtener-resultado-estrategia-proyecto.controller';
import { ObtenerProyectosSeleccionadosControlador } from './obtener-proyectos-seleccionados.controller';
import { ActualizarEstadoProyectoControlador } from './actualiza-estado-proyecto.controller';
import { ObtenerProyectosFinalizadosControlador } from './obtener-proyectos-finalizados.controller';
import { RecomendarProyectoControlador } from './recomendadar-proyecto.controller';
import { ObtenerAllProyectosPaginacionControlador } from './obtener-all-proyectos-paginacion.controller';
import { ObtenerTodosProyectosPaginacionControlador } from './obtener-todos-proyectos-paginacion.controller';
import { ObtenerProyectosEsperaFondosControlador } from './obtener-proyectos-espera-fondos.controller';
import { AsignarEquipoControlador } from './asignar-equipo-proyecto.controller';
// import { DonacionProyectoControlador } from './donacion-proyecto.controller';

@Module({
  imports: [
    ProyectosServicesModule,
    forwardRef(() => NotificacionServicesModule),
  ],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    CrearProyectoControlador,
    ObtenerCatalogoTipoProyectoControlador,
    VotoProyectoUnicoControlador,
    ActualizarProyectoControlador,
    ObtenerProyectoRecientesPaginacionControlador,
    ObtenerProyectoUnicaControlador,
    ObtenerProyectosTitulosControlador,
    EliminarProyectoUnicoControlador,
    ListaCoAutoresControlador,
    TransferirProyectoControlador,
    ObtenerProyectosRangoControlador,
    ObtenerProyectoReomendadoPaginacionControlador,
    ObtenerFechasHistoricoProyectoControlador,
    TransferenciaOkControlador,
    ModificarValorEstimadoProyectoControlador,
    ObtenerProyectosPerfilControlador,
    ObtenerFechaInicioForoProyectoControlador,
    ObtenerFechaFinForoProyectoControlador,
    AsignarRolesControlador,
    ResultadoEstrategiaControlador,
    ObtenerResultadoEstrategiaControlador,
    ObtenerProyectosSeleccionadosControlador,
    ActualizarEstadoProyectoControlador,
    ObtenerProyectosFinalizadosControlador,
    RecomendarProyectoControlador,
    ObtenerAllProyectosPaginacionControlador,
    ObtenerTodosProyectosPaginacionControlador,
    ObtenerProyectosEsperaFondosControlador,
    AsignarEquipoControlador
  ],
})
export class ProyectosControllerModule {}
