import { I18nService } from 'nestjs-i18n';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiBody,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Body,
  HttpStatus,
  Headers,
  Put,
  UseGuards,
  Req,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import * as mongoose from 'mongoose';
import { ActualizarEstadoProyectoDto } from '../entidad/actualizar-proyecto.dto';
import { Funcion } from 'src/shared/funcion';
import { ActualizaEstadoProyectoService } from '../casos_de_uso/actualiza-estado-proyecto.service';

@ApiTags('Proyectos')
@Controller('api/proyecto')
export class ActualizarEstadoProyectoControlador {
  constructor(
    private readonly actualizaEstadoProyectoService: ActualizaEstadoProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/actualiza-estado')
  @ApiSecurity('Authorization')
  @ApiBody({ type: ActualizarEstadoProyectoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Actualiza el estado de un proyecto segun el filtro escogido EJM:(activa, eliminado, ejecucion, esperaFondos) ',
  })
  @ApiResponse({ status: 200, description: 'Actualización correcta' })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarEstadoProyecto(
    @Headers() headers,
    @Req() req,
    @Body() actualizarEstadoProyectoDto: ActualizarEstadoProyectoDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(actualizarEstadoProyectoDto._id) &&
      mongoose.isValidObjectId(actualizarEstadoProyectoDto.perfil._id) &&
      actualizarEstadoProyectoDto.filtro
    ) {
      try {
        const proyectos = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
          actualizarEstadoProyectoDto._id,
          null,
          null,
          actualizarEstadoProyectoDto.perfil._id,
          actualizarEstadoProyectoDto.filtro,
          null,
        );

        if (proyectos) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
