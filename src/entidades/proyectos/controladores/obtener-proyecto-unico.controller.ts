import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Get,
  Headers,
  UseGuards,
  Query,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { I18nService } from 'nestjs-i18n';
import * as mongoose from 'mongoose';
import { ObtenerProyectoUnicoService } from '../casos_de_uso/obtener-proyecto-unico.service';
import { ProyectoCompletoDto } from '../entidad/proyecto-response-dto';
import { AuthGuard } from '@nestjs/passport';
import { Funcion } from 'src/shared/funcion';
import { ObtenerPortadaPredeterminadaProyectoService } from '../casos_de_uso/obtener-portada-predeterminada-proyecto.service';
import { ObtenerLocalidadProyectoService } from '../casos_de_uso/obtener-localidad-proyecto.service';
import { CatalogoTipoProyectoByCodigoService } from '../casos_de_uso/obtener-tipo-proyecto-by-codigo.service';

@ApiTags('Proyectos')
@Controller('api/proyecto-unico')
export class ObtenerProyectoUnicaControlador {
  constructor(
    private readonly obtenerProyectoUnicoService: ObtenerProyectoUnicoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Devuelve la información de un proyecto' })
  @ApiResponse({ status: 200, type: ProyectoCompletoDto, description: 'OK' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerProyectoUnica(
    @Headers() headers,
    @Query('idProyecto') idProyecto: string,
    @Query('idPerfil') idPerfil: string,
    @Query('estado') estado?: string,
    @Query('original') original?: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(idProyecto) &&
        mongoose.isValidObjectId(idPerfil)
      ) {
        const proyecto = await this.obtenerProyectoUnicoService.obtenerProyectoUnico(
          idProyecto,
          idPerfil,
          codIdioma,
          estado,
          original,
        );

        if (proyecto) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: proyecto,
          });
        } else {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_OBTENER,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
