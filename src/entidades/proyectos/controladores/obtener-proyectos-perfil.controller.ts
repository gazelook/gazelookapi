import { I18nService } from 'nestjs-i18n';
import { Response } from 'express';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Res,
  HttpStatus,
  Get,
  Headers,
  UseGuards,
  Query,
} from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';
import { ObtenerProyectosPerfilService } from '../casos_de_uso/obtener-proyectos-perfil.service';

@ApiTags('Proyectos')
@Controller('api/proyectos-perfil')
export class ObtenerProyectosPerfilControlador {
  constructor(
    private readonly obtenerProyectosPerfilService: ObtenerProyectosPerfilService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Devuelve todos los poryectos de un perfil.' })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 406, description: 'Error al obtener proyectos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerProyectos(
    @Headers() headers,
    @Query('perfil') perfil: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Query('traducir') traducir: string,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      !isNaN(limite) &&
      !isNaN(pagina) &&
      limite > 0 &&
      pagina > 0 &&
      traducir &&
      mongoose.isValidObjectId(perfil)
    ) {
      try {
        const proyectos = await this.obtenerProyectosPerfilService.obtenerProyectosPerfilPaginacion(
          perfil,
          codIdioma,
          limite,
          pagina,
          traducir,
          response,
        );

        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: proyectos,
        });
        response.send(respuesta);
        return respuesta;
      } catch (e) {
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
        response.send(respuesta);
        return respuesta;
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_FOUND,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
