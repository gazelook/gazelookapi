import { ObneterProyetoRangoService } from './../casos_de_uso/obtener-proyectos-rango.service';
import { BusquedaProyectoRangoDto } from './../entidad/proyecto-dto';

import { AuthGuard } from '@nestjs/passport';
import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Res,
  HttpStatus,
  Get,
  Headers,
  UseGuards,
  Query,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { CatalogoTipoProyectoService } from '../casos_de_uso/obtener-tipo-proyecto.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { Response } from 'express';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Proyectos')
@Controller('api/buscar-proyectos-rango')
export class ObtenerProyectosRangoControlador {
  constructor(
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly obneterProyetoRangoService: ObneterProyetoRangoService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Busqueda de proyectos por rango' })
  @ApiResponse({
    status: 200,
    type: BusquedaProyectoRangoDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerProyectosRango(
    @Headers() headers,
    @Query('tipoProyecto') tipoProyecto: string,
    @Query('fechaInicial') fechaInicial: string,
    @Query('fechaFinal') fechaFinal: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const proyectos = await this.obneterProyetoRangoService.obtenerPorRango(
        codIdioma,
        tipoProyecto,
        fechaInicial,
        fechaFinal,
        limite,
        pagina,
        response,
      );

      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: proyectos,
      });
      response.send(respuesta);
      return respuesta;
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
