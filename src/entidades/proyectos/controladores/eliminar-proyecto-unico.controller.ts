import { I18nService } from 'nestjs-i18n';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Headers,
  Param,
  UseGuards,
  Delete,
  Req,
} from '@nestjs/common';
import * as mongoose from 'mongoose';
import { EliminarProyectoUnicoService } from '../casos_de_uso/eliminar-proyecto-unico.service';
import { AuthGuard } from '@nestjs/passport';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Proyectos')
@Controller('api/proyecto-unico')
export class EliminarProyectoUnicoControlador {
  constructor(
    private readonly eliminarProyectoUnicoService: EliminarProyectoUnicoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Delete('/:perfil/:idProyecto')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Elimina logicamente un proyecto' })
  @ApiResponse({ status: 200, description: 'Eliminación correcta' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se puede eliminar el documento',
  })
  @UseGuards(AuthGuard('jwt'))
  public async eliminarNoticiaUnica(
    @Headers() headers,
    @Req() req,
    @Param('perfil') perfil: string,
    @Param('idProyecto') idProyecto: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(idProyecto) &&
      mongoose.isValidObjectId(perfil)
    ) {
      //Verifica el perfil que se envia en una peticion con el perfil del token autorization
      // let perfilVerificado = this.funcion.verificarPerfilToken(perfil, req.user.user.perfiles);

      // if (perfilVerificado) {
      try {
        const proyecto = await this.eliminarProyectoUnicoService.eliminarProyectoUnico(
          perfil,
          idProyecto,
        );

        if (proyecto === true) {
          const ELIMINACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ELIMINACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        } else {
          const ERROR_ELIMINAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ELIMINAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }

    // } else {
    //     const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
    //         lang: codIdioma
    //     });
    //     return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })

    // }
  }
}
