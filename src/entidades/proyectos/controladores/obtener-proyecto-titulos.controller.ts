import { BusquedaProyectoTituloDto } from './../entidad/proyecto-dto';
import { ObneterProyetoTituloService } from './../casos_de_uso/obtener-proyecto-titulo.service';

import { AuthGuard } from '@nestjs/passport';
import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Res,
  HttpStatus,
  Get,
  Headers,
  UseGuards,
  Query,
} from '@nestjs/common';
import { CatalogoTipoProyectoService } from '../casos_de_uso/obtener-tipo-proyecto.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { Response } from 'express';
import { Funcion } from 'src/shared/funcion';
import { I18nService } from 'nestjs-i18n';

@ApiTags('Proyectos')
@Controller('api/buscar-proyectos')
export class ObtenerProyectosTitulosControlador {
  constructor(
    private readonly obneterProyetoTituloService: ObneterProyetoTituloService,
    private readonly funcion: Funcion,
    private readonly i18n: I18nService,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Busqueda de proyectos por título' })
  @ApiResponse({
    status: 200,
    type: BusquedaProyectoTituloDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerProyectosTitulo(
    @Headers() headers,
    @Query('titulo') titulo: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
    @Query('estado') estadoProyecto?: string,
  ) {
    const headerNombre = new HadersInterfaceNombres();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        titulo &&
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0
      ) {
        const proyectos = await this.obneterProyetoTituloService.obtenerPorTitulo(
          codIdioma,
          titulo,
          limite,
          pagina,
          estadoProyecto,
          response,
        );

        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: proyectos,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
