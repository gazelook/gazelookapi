import { AuthGuard } from '@nestjs/passport';
import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiBody,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Headers,
  UseGuards,
  Req,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import * as mongoose from 'mongoose';
import { TrasferirProyectoDto } from '../entidad/trasferir-proyecto-dto';
import { TransferirProyectoService } from '../casos_de_uso/trasferir-proyecto.service';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Proyectos')
@Controller('api/trasferir-proyecto')
export class TransferirProyectoControlador {
  constructor(
    private readonly transferirProyectoService: TransferirProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: TrasferirProyectoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Trasfiere el proyecto a otro perfil' })
  @ApiResponse({ status: 201, description: 'Email enviado exitosamente' })
  @ApiResponse({ status: 404, description: 'El email no puede enviarse' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async transferirProyecto(
    @Headers() headers,
    @Body() trasferirProyectoDto: TrasferirProyectoDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(trasferirProyectoDto.perfilPropietario._id) &&
        mongoose.isValidObjectId(trasferirProyectoDto.perfilNuevo._id) &&
        mongoose.isValidObjectId(trasferirProyectoDto.proyecto._id) &&
        trasferirProyectoDto.perfilNuevo._id &&
        trasferirProyectoDto.perfilPropietario._id &&
        trasferirProyectoDto.proyecto._id
      ) {
        const proyecto = await this.transferirProyectoService.EnviarCorreoConfirmacion(
          trasferirProyectoDto,
          codIdioma,
        );

        //await this.transferirProyectoService.trasferirProyecto(trasferirProyectoDto.idProyecto, trasferirProyectoDto.perfilPropietario, trasferirProyectoDto.perfilNuevo)
        if (proyecto) {
          const EMAIL_ENVIADO = await this.i18n.translate(
            codIdioma.concat('.EMAIL_ENVIADO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: EMAIL_ENVIADO,
          });
        } else {
          const EMAIL_NO_PUEDE_CREARSE = await this.i18n.translate(
            codIdioma.concat('.EMAIL_NO_PUEDE_CREARSE'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: EMAIL_NO_PUEDE_CREARSE,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
