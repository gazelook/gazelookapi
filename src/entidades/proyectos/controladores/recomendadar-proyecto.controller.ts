import { I18nService } from 'nestjs-i18n';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiBody,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Body,
  HttpStatus,
  Headers,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import * as mongoose from 'mongoose';
import { RecomiendaProyectoDto } from '../entidad/actualizar-proyecto.dto';
import { Funcion } from 'src/shared/funcion';
import { RecomendarProyectoService } from '../casos_de_uso/recomendar-proyecto.service';

@ApiTags('Proyectos')
@Controller('api/proyecto')
export class RecomendarProyectoControlador {
  constructor(
    private readonly recomendarProyectoService: RecomendarProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) { }

  @Put('/recomendar-proyecto-gazelook')
  @ApiSecurity('Authorization')
  @ApiBody({ type: RecomiendaProyectoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Recomiendo un proyecto solo si el perfil que se envia es de un usuario administrador de gazelook',
  })
  @ApiResponse({ status: 200, description: 'Actualización correcta' })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @UseGuards(AuthGuard('jwt'))
  public async recomendarProyecto(
    @Headers() headers,
    @Body() recomiendaProyectoDto: RecomiendaProyectoDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(recomiendaProyectoDto._id) &&
        mongoose.isValidObjectId(recomiendaProyectoDto.perfil._id)
      ) {

        const proyectos = await this.recomendarProyectoService.recomendarProyecto(
          recomiendaProyectoDto._id,
          recomiendaProyectoDto.perfil._id,
        );

        if (proyectos) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }

  }
}
