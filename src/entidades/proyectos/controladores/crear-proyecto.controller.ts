import { AuthGuard } from '@nestjs/passport';
import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiBody,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  HttpException,
  Inject,
  Get,
  Headers,
  Param,
  BadRequestException,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { CrearProyectoService } from '../casos_de_uso/crear-proyecto.service';
import { CrearProyectoDto } from '../entidad/crear-proyecto-dto';
import { ProyectoDto } from '../entidad/proyecto-dto';
import * as mongoose from 'mongoose';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Proyectos')
@Controller('api/proyecto')
export class CrearProyectoControlador {
  constructor(
    private readonly crearProyectoService: CrearProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: CrearProyectoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Crea un nuevo proyecto' })
  @ApiResponse({
    status: 201,
    type: ProyectoDto,
    description: 'Creación correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al crear' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async crearProyecto(
    @Headers() headers,
    @Body() crearProyectoDTO: CrearProyectoDto,
  ) {
    console.log('crearProyectoDTO: ', crearProyectoDTO);

    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(crearProyectoDTO.perfil) &&
        crearProyectoDTO.tipo &&
        crearProyectoDTO.direccion &&
        crearProyectoDTO.valorEstimado &&
        crearProyectoDTO.moneda.codNombre &&
        crearProyectoDTO.traducciones[0].descripcion &&
        crearProyectoDTO.traducciones[0].titulo &&
        crearProyectoDTO.traducciones[0].tituloCorto
      ) {
        const proyecto = await this.crearProyectoService.crearProyecto(
          crearProyectoDTO,
          codIdioma,
        );

        if (proyecto) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
            datos: { _id: proyecto._id },
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
