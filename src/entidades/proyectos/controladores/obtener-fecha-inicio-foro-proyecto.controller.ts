import { I18nService } from 'nestjs-i18n';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiBody,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Body,
  HttpStatus,
  Headers,
  Param,
  Put,
  UseGuards,
  Get,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ActualizarProyectoDto } from '../entidad/actualizar-proyecto.dto';
import {
  fechaMaximaValorEstimadoDto,
  ProyectoDto,
} from '../entidad/proyecto-dto';
import { Funcion } from 'src/shared/funcion';
import { ModificarValorEstimadoProyectoService } from '../casos_de_uso/modificar-valor-estimado-proyecto.service';
import { ObtenerFechaIniciaForoProyectoService } from '../casos_de_uso/obtener-fecha-inicia-foro-proyecto.service';

@ApiTags('Proyectos')
@Controller('api/inicia-foro')
export class ObtenerFechaInicioForoProyectoControlador {
  constructor(
    private obtenerFechaIniciaForoProyectoService: ObtenerFechaIniciaForoProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Retorna la fecha en que inicia el próximo foro mas cercano',
  })
  @ApiResponse({
    status: 200,
    type: fechaMaximaValorEstimadoDto,
    description: 'OK',
  })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerFechaIniciaForoProyecto(@Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const proyectos = await this.obtenerFechaIniciaForoProyectoService.obtenerFechaIniciaForoProyecto();

      if (proyectos) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: proyectos,
        });
      } else {
        const ERROR_OBTENER = await this.i18n.translate(
          codIdioma.concat('.ERROR_OBTENER'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ERROR_OBTENER,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
