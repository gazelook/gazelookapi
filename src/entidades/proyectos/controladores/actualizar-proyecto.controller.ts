import { I18nService } from 'nestjs-i18n';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiBody,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Body,
  HttpStatus,
  Headers,
  Param,
  Put,
  UseGuards,
  Req,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import * as mongoose from 'mongoose';
import { ActualizarProyectoService } from '../casos_de_uso/actualizar-proyecto.service';
import { ActualizarProyectoDto } from '../entidad/actualizar-proyecto.dto';
import { ProyectoDto } from '../entidad/proyecto-dto';
import { Funcion } from 'src/shared/funcion';
import { Request } from 'express';

@ApiTags('Proyectos')
@Controller('api/proyecto')
export class ActualizarProyectoControlador {
  constructor(
    private readonly actualizarProyectoService: ActualizarProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: ActualizarProyectoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Actualiza la información de un proyecto' })
  @ApiResponse({
    status: 200,
    type: ProyectoDto,
    description: 'Actualización correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarProyecto(
    @Headers() headers,
    @Req() req,
    @Body() actualizarProyectoDTO: ActualizarProyectoDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(actualizarProyectoDTO._id) &&
      mongoose.isValidObjectId(actualizarProyectoDTO.perfil._id)
    ) {
      // console.log('req.user.user.perfiles: ', req)
      //Verifica el perfil que se envia en una peticion con el perfil del token autorization
      // let perfilVerificado = await this.funcion.verificarPerfilToken(actualizarProyectoDTO.perfil._id, req.user.user.perfiles);

      // if (perfilVerificado) {
      try {
        const proyectos = await this.actualizarProyectoService.actualizarProyecto(
          actualizarProyectoDTO,
          actualizarProyectoDTO._id,
          codIdioma,
        );

        if (proyectos) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
            datos: proyectos,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      console.log('paramin valid 2');
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
    // } else {
    //     console.log('paramin valid 1')
    //     const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
    //         lang: codIdioma
    //     });
    //     return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })

    // }
  }
}
