import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Get,
  Headers,
  UseGuards,
  Query,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { I18nService } from 'nestjs-i18n';
import * as mongoose from 'mongoose';
import { ObtenerProyectoUnicoService } from '../casos_de_uso/obtener-proyecto-unico.service';
import { ProyectoCompletoDto } from '../entidad/proyecto-response-dto';
import { AuthGuard } from '@nestjs/passport';
import { Funcion } from 'src/shared/funcion';
import { ObtenerPortadaPredeterminadaProyectoService } from '../casos_de_uso/obtener-portada-predeterminada-proyecto.service';
import { ObtenerLocalidadProyectoService } from '../casos_de_uso/obtener-localidad-proyecto.service';
import { CatalogoTipoProyectoByCodigoService } from '../casos_de_uso/obtener-tipo-proyecto-by-codigo.service';
import { ObtenerResultadoEstrategiaService } from '../casos_de_uso/obtener-resultado-estrategia-proyecto.service';

@ApiTags('Proyectos')
@Controller('api/proyecto')
export class ObtenerResultadoEstrategiaControlador {
  constructor(
    private readonly obtenerResultadoEstrategiaService: ObtenerResultadoEstrategiaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/obtener-resultado-estrategia')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Devuelve la información del resultado de la estrategia de un proyecto',
  })
  @ApiResponse({ status: 200, type: ProyectoCompletoDto, description: 'OK' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerResultadoEstrategia(
    @Headers() headers,
    @Query('idProyecto') idProyecto: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (mongoose.isValidObjectId(idProyecto)) {
        const proyecto = await this.obtenerResultadoEstrategiaService.obtenerResultadoEstrategia(
          idProyecto,
          codIdioma,
        );

        if (proyecto) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: proyecto,
          });
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: {},
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
