import { I18nService } from 'nestjs-i18n';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Body,
  HttpStatus,
  Headers,
  Param,
  Put,
  UseGuards,
  Post,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import * as mongoose from 'mongoose';
import { VotoProyectoDto } from '../entidad/voto-proyecto-dto';
import { VotoProyectoService } from '../casos_de_uso/voto-proyecto-unico.service';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Proyectos')
@Controller('api/voto-proyecto-unico')
export class VotoProyectoUnicoControlador {
  constructor(
    private readonly votoProyectoService: VotoProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Se realiza una votacion por un perfil a un proyecto',
  })
  @ApiResponse({ status: 201, description: 'Voto Correcto' })
  @ApiResponse({ status: 404, description: 'Error al votar' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @UseGuards(AuthGuard('jwt'))
  public async votoProyectoUnico(
    @Headers() headers,
    @Body() votoProyectoDto: VotoProyectoDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(votoProyectoDto.proyecto._id) &&
        mongoose.isValidObjectId(votoProyectoDto.perfil._id)
      ) {
        const proyecto = await this.votoProyectoService.votoProyectoUnico(
          votoProyectoDto,
          votoProyectoDto.perfil._id,
          codIdioma,
        );

        if (proyecto) {
          const VOTO_CORRECTO = await this.i18n.translate(
            codIdioma.concat('.VOTO_CORRECTO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: VOTO_CORRECTO,
          });
        } else {
          const ERROR_VOTO = await this.i18n.translate(
            codIdioma.concat('.ERROR_VOTO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_VOTO,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
