import { Module } from '@nestjs/common';
import { ProyectosControllerModule } from './controladores/proyectos-controller.module';
import { ProyectosServicesModule } from './casos_de_uso/proyectos-services.module';

@Module({
  imports: [ProyectosControllerModule],
  providers: [ProyectosServicesModule],
  controllers: [],
})
export class ProyectosModule {}
