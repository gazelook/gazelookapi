import { ApiProperty } from '@nestjs/swagger';
import { PerfilProyectoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import { ProyectoUnicoDto } from './proyecto-dto';
import { DescTraduVotoProyectoDto } from './traduccion-voto-proyecto-dto';

export class VotoProyectoDto {
  @ApiProperty({ required: true })
  proyecto: ProyectoUnicoDto;

  @ApiProperty({ required: true })
  perfil: PerfilProyectoDto;

  @ApiProperty({ required: false, type: [DescTraduVotoProyectoDto] })
  traducciones: [DescTraduVotoProyectoDto];
}

export class IdentVotoProyectoDto {
  @ApiProperty({
    description: 'identificador del voto proyecto',
    example: '5f57a7a1e446d64158ef85l8',
  })
  _id: string;
}
