import { ApiProperty } from '@nestjs/swagger';
import {
  RetornoMediaDto,
  IdentAlbumDto,
} from 'src/entidades/album/entidad/album-dto';
import {
  PerfilProyectoDto,
  PerfilProyectoUnicoDto,
} from 'src/entidades/usuario/dtos/perfil.dto';
import { CatalogoLocalidadProyectoDto } from 'src/entidades/pais/entidad/catalogo-localidad.dto';
import { IdentVotoProyectoDto } from './voto-proyecto-dto';
import {
  TraduccionResumidaProyectoDto,
  TraduccionProyectoDto,
} from './traduccion-proyecto-dto';

// export class ProyectoResumidoResponseDto {

//     @ApiProperty()
//     totalVotos:String;
//     @ApiProperty()
//     traducciones: TraduccionResumidaNoticiaDto;
//     @ApiProperty({example: 'https/fotos.coms.qedqw'})
//     portada:String;
//     @ApiProperty()
//     fechaActualizacion:Date;
//     @ApiProperty()
//     voto:Boolean;
// }

export class ProyectoResumidoPaginacionResponseDto {
  @ApiProperty({
    description: 'Identificador del proyecto',
    example: '5f378265075b2a4be839d534',
  })
  _id: string;
  @ApiProperty({ type: [TraduccionResumidaProyectoDto] })
  traducciones: [TraduccionResumidaProyectoDto];
  @ApiProperty({
    description: 'Numero de votos actuales que tiene el proyecto',
    example: 10,
  })
  totalVotos: number;
  @ApiProperty({
    description: 'Fecha en que se creo el proyecto',
    example: '2020-08-15T06:36:21.436+00:00',
  })
  fechaCreacion: Date;
  @ApiProperty({
    description: 'Fecha en que se actualizo el proyecto',
    example: '2020-08-20T06:36:21.436+00:00',
  })
  fechaActualizacion: Date;
  @ApiProperty({
    description:
      'Si el proyecto ha sido recomendado por el administrador gacelook (recomendado: true si no: false)',
    example: true,
  })
  recomendadoAdmin: boolean;
  @ApiProperty({
    description:
      'Si el proyecto ha sido actualizado recientemente (true) caso contrario (false)',
    example: true,
  })
  actualizado: boolean;
  @ApiProperty({ type: [RetornoMediaDto] })
  adjuntos: [RetornoMediaDto];
  @ApiProperty({
    description: 'Si el perfil ya ha votado (true) caso contrario (false)',
    example: true,
  })
  voto: boolean;
}

export class ProyectoCompletoDto {
  @ApiProperty({
    description: 'Identificador del proyecto',
    example: '5f56653564d0cc3bf47893d5',
  })
  _id: string;
  @ApiProperty({ type: [TraduccionProyectoDto] })
  traducciones: [TraduccionProyectoDto];
  @ApiProperty({ type: [IdentAlbumDto] })
  adjuntos: [IdentAlbumDto];
  @ApiProperty({
    description: 'numero total de votos que acumulo el proyecto',
    example: '20',
  })
  totalVotos: number;
  @ApiProperty({ type: PerfilProyectoUnicoDto })
  perfil: PerfilProyectoUnicoDto;
  @ApiProperty({
    description: 'Localidad tipo localidad de proyecto',
    example: 'LOC_735',
  })
  localidad: string;
  // @ApiProperty({type:[]})
  // votos: [IdentVotoProyectoDto];
  @ApiProperty({
    description: 'Fecha de creacion del proyecto',
    example: '2020-09-07T16:52:05.229Z',
  })
  fechaCreacion: Date;
  @ApiProperty({
    description: 'Fecha de creacion del proyecto',
    example: '2020-09-07T16:52:05.229Z',
  })
  fechaActualizacion: Date;
  @ApiProperty({
    description: 'Si el perfil ya ha votado:true si no false',
    example: false,
  })
  voto: Boolean;
}
