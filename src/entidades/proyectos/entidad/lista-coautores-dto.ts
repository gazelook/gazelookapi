import { ProyectoCompletoDto } from './proyecto-response-dto';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsString,
  IsOptional,
} from 'class-validator';
import { PerfilProyectoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import {
  SetParticipanteProyectoDto,
  CoautoresParticipanteProyectoDto,
} from './participante-proyecto-dto';
import { IdentAlbumDto } from 'src/entidades/album/entidad/album-dto';
import { IdentVotoProyectoDto } from './voto-proyecto-dto';
import { identComentarioDto } from 'src/entidades/comentarios/entidad/comentario-dto';

export class ListaCoAutoresDto {
  @ApiProperty({
    description: 'Identificador del proyecto',
    example: '5f56653564d0cc3bf47893d5',
  })
  _id: string;

  @ApiProperty({ type: [CoautoresParticipanteProyectoDto] })
  participantes: [CoautoresParticipanteProyectoDto];

  @ApiProperty({
    description: 'Identificador del perfil',
    example: '5f57a7a1e446d64158ef85f1',
  })
  perfil: string;
}
