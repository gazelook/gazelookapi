import { ApiProperty } from '@nestjs/swagger';

export class TrasferenciaOkDto {
  /* @ApiProperty({ required: true, description: 'token del email' })
    token: string; */

  @ApiProperty({
    required: true,
    description: 'identificador del proyecto',
    example: '5f52829187cc3537f7a68ce2',
  })
  //@IsNotEmpty()
  idProyecto: string;

  @ApiProperty({
    required: true,
    description:
      'identificador del perfil  al que se va a trasferir el proyecto',
    example: '5f56653564d0cc3bf47893d5',
  })
  //@IsNotEmpty()
  idPerfilNuevo: string;

  @ApiProperty({
    required: true,
    description:
      'identificador del perfil propietario que va a trasferir el proyecto',
  })
  //@IsNotEmpty()
  idPerfilPropietario: string;

  /* @ApiProperty({ required: true, description: 'idioma', example: 'es, en' })
    //@IsNotEmpty()
    idioma: string; */
}
