import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MinLength,
  IsString,
  MaxLength,
  Matches,
  IsEmpty,
  IsEmail,
  IsOptional,
} from 'class-validator';
import { CodCatalogoTipoMonedaDto } from 'src/entidades/catalogos/entidad/catalogo-tipo-moneda.dto';
import { MensajeIdDto } from 'src/entidades/conversacion/entidad/conversacion-dto';
import { Direccion } from 'src/entidades/cuenta/entidad/cuenta.dto';
import { mediaId } from 'src/entidades/media/entidad/archivo-resultado.dto';
import { AlbumNuevo } from 'src/entidades/perfil/entidad/crear-perfil.dto';
import { PerfilProyectoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import {
  CrearTraduccionProyectoDto,
  TraduccionProyectoDto,
} from './traduccion-proyecto-dto';

export class ActualizarProyectoDto {
  @ApiProperty({ required: true, description: 'identificador del proyecto' })
  @IsString()
  _id: string;

  @ApiProperty()
  perfil: PerfilProyectoDto;

  @ApiProperty({ type: [CrearTraduccionProyectoDto] })
  traducciones: [CrearTraduccionProyectoDto];

  @ApiProperty({ required: true, description: 'valor estimado del proyecto' })
  @IsOptional()
  valorEstimado: number;

  @ApiProperty({
    required: true,
    description: 'valor estimado del proyecto en dolares',
  })
  @IsOptional()
  valorEstimadoFinal: number;

  @ApiProperty({ type: CodCatalogoTipoMonedaDto })
  moneda: CodCatalogoTipoMonedaDto;

  @ApiProperty({ type: [AlbumNuevo] })
  @IsOptional()
  adjuntos: AlbumNuevo[];

  @ApiProperty({ type: [mediaId] })
  @IsOptional()
  medias: mediaId[];

  @ApiProperty({ type: MensajeIdDto })
  @IsOptional()
  transferenciaActiva: MensajeIdDto;

  @ApiProperty({ type: Direccion })
  direccion: Direccion;
}

export class ActualizarEstadoProyectoDto {
  @ApiProperty({ required: true, description: 'identificador del proyecto' })
  @IsString()
  _id: string;

  @ApiProperty()
  perfil: PerfilProyectoDto;

  @ApiProperty({
    required: true,
    description:
      'Filtro que se utiliza para saber a que estado cambiar el proyecto',
    example:
      'activa, eliminado, preEstrategia, esperaFondos, revision, ejecucion, esperaDonacion, finalizado, foro, estrategia',
  })
  filtro: string;
}

export class RecomiendaProyectoDto {
  @ApiProperty({ required: true, description: 'identificador del proyecto' })
  @IsString()
  _id: string;

  @ApiProperty()
  perfil: PerfilProyectoDto;
}
