import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class TraduccionProyectoDto {
  @ApiProperty({ description: 'titulo resumido', example: 'titulo resumido' })
  @IsString()
  @IsNotEmpty()
  tituloCorto: string;

  @ApiProperty({ description: 'titulo largo', example: 'titulo largo' })
  @IsString()
  @IsNotEmpty()
  titulo: string;

  @ApiProperty({
    description: 'enunciado que describe el proyecto en su totalidad',
    example: 'Esta es la descripción del proyecto',
  })
  @IsString()
  @IsNotEmpty()
  descripcion: string;

  @ApiProperty({
    type: [],
    description: 'Palabras clave del proyecto',
    example: "[ 'titulo','largo','resumido']",
  })
  tags: [];
}

export class TraduccionResumidaProyectoDto {
  @ApiProperty({ description: 'Titulo del proyecto', example: 'Titulo' })
  titulo: string;

  @ApiProperty({
    description: 'Titulo corto del proyecto',
    example: 'Titulo corto',
  })
  tituloCorto: string;
}

export class CrearTraduccionProyectoDto {
  @ApiProperty({ description: 'titulo resumido', example: 'titulo resumido' })
  @IsString()
  @IsNotEmpty()
  tituloCorto: string;

  @ApiProperty({ description: 'titulo largo', example: 'titulo largo' })
  @IsString()
  @IsNotEmpty()
  titulo: string;

  @ApiProperty({
    description: 'enunciado que describe el proyecto en su totalidad',
    example: 'Esta es la descripción del proyecto',
  })
  @IsString()
  @IsNotEmpty()
  descripcion: string;
}
