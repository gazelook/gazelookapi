import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty
} from 'class-validator';
import { PerfilIdDto } from 'src/entidades/usuario/dtos/perfil.dto';

export class EquipoProyectoDto {

  @ApiProperty({
    required: true,
    description: 'identificador del perfil',
    example: '5f52829187cc3537f7a68ce2',
  })
  //@IsNotEmpty()
  idPerfil: string;
  
  @ApiProperty({
    required: true,
    description: 'identificador del proyecto',
    example: '5f52829187cc3537f7a68ce2',
  })
  //@IsNotEmpty()
  idProyecto: string;

  //array de perfiles del equipo del proyecto
  @ApiProperty({ type: [PerfilIdDto] })
  perfiles: Array<PerfilIdDto>;

  // codigo de la entidad
  // @ApiProperty({type: String})
  // @IsNotEmpty()
  // codigoEntidad: string;

  // // codigo del rol entidad
  // @ApiProperty({type: String})
  // @IsNotEmpty()
  // codigoRol: string;


}
