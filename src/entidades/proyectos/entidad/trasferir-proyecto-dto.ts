import { ApiProperty } from '@nestjs/swagger';
import { PerfilProyectoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import { ProyectoUnicoDto } from './proyecto-dto';

export class TrasferirProyectoDto {
  @ApiProperty({ type: PerfilProyectoDto })
  perfilPropietario: PerfilProyectoDto;

  @ApiProperty({ type: PerfilProyectoDto })
  perfilNuevo: PerfilProyectoDto;

  @ApiProperty({ type: ProyectoUnicoDto })
  proyecto: ProyectoUnicoDto;
}
