import { ApiProperty } from '@nestjs/swagger';
import { IdCatalogoTipoProyectoDto } from './traduccion-catalogo-tipo-proyecto-dto';

export class CatalogoTipoProyectoDto {
  @ApiProperty({
    description: 'Identificador del catalogo tipo de proyecto',
    example: '5f3173e892e08e19101271f8',
  })
  _id: string;
  @ApiProperty({ type: [IdCatalogoTipoProyectoDto] })
  traducciones: [IdCatalogoTipoProyectoDto];
  @ApiProperty({
    description: 'Codigo del catalogo tipo proyecto',
    example: 'CAT_TIPO_PROY_01',
  })
  codigo: string;
  @ApiProperty({
    description: 'Fecha de creacion',
    example: '2020-08-20T14:49:48.277Z',
  })
  fechaCreacion: Date;
}

export class CodCatalogTipoProyectoDto {
  @ApiProperty({
    required: true,
    description: 'Codigo del catalogo tipo proyecto',
    example: 'CAT_TIPO_PROY_01',
  })
  codigo: string;
}
