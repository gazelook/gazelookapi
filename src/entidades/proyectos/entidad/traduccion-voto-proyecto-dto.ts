import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsOptional } from 'class-validator';

export class DescTraduVotoProyectoDto {
  // @ApiProperty()
  // @IsString()
  // _id: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  descripcion: string;
}
