import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsEmail,
  IsNotEmpty, IsNotEmptyObject, IsObject, IsOptional, ValidateNested
} from 'class-validator';
import { PagoCripto } from 'src/entidades/cuenta/entidad/cuenta-coinpayments.dto';
import { DatosFacturacion, Direccion, MetodoPago, MonedaRegistro } from 'src/entidades/cuenta/entidad/cuenta.dto';
import { TransaccionCuentaDto } from 'src/entidades/transaccion/entidad/transaccion.dto';

export class DonacionProyectoDto {

  @ApiProperty({
    required: true,
    description: 'identificador del usuario',
    example: '5f52829187cc3537f7a68ce2',
  })
  //@IsNotEmpty()
  idUsuario: string;

  //usuario
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail() //@Matches(/^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,4})+$/, { message: 'No valid Email!!' })
  email: string;
  
  @ApiProperty({
    required: true,
    description: 'identificador del proyecto',
    example: '5f52829187cc3537f7a68ce2',
  })
  //@IsNotEmpty()
  idProyecto: string;

  //metodo de pago
  @ApiProperty({ type: MetodoPago })
  @IsNotEmpty()
  metodoPago: MetodoPago;

  //monto de pago en diferentes monedas
  @ApiProperty({ type: [TransaccionCuentaDto] })
  @IsNotEmpty()
  @ValidateNested({ each: true })
  transacciones: TransaccionCuentaDto[];

  //pago
  @ApiProperty({ type: DatosFacturacion })
  @IsOptional()
  @IsObject() //@IsInstance(DatosPago)
  datosFacturacion: DatosFacturacion;

  // direccion
  // @ApiProperty({ type: Direccion })
  // @IsNotEmptyObject()
  // direccion: Direccion;


  @ApiProperty({ type: MonedaRegistro })
  @IsObject()
  @IsNotEmptyObject()
  @ValidateNested({ each: true })
  @Type(() => MonedaRegistro)
  monedaRegistro: MonedaRegistro;

  // Direccion temporal
  @ApiProperty({})
  @IsOptional()
  direccionDomiciliaria: string;

  // Documento de identidad temporal
  @ApiProperty({})
  @IsOptional()
  documentoIdentidad: string;

  @ApiProperty({type: PagoCripto})
  pagoCripto: PagoCripto;

  //autorizacionCodePaymentez
  @ApiProperty({ type: String })
  autorizacionCodePaymentez: string;
}
