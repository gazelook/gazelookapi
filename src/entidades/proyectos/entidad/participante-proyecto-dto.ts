import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsString,
  IsOptional,
} from 'class-validator';
import { PerfilProyectoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import { CoautorRolesDto } from 'src/entidades/rol/entidad/roles-dto';
import { CoautorPerfilDto } from 'src/entidades/perfil/entidad/perfil-usuario.dto';

export class SetParticipanteProyectoDto {
  @ApiProperty({
    required: true,
    description: 'Identificador del participante proyecto',
    example: '5f3173e892e08e19101271f8',
  })
  @IsString()
  _id: string;
}

export class CoautoresParticipanteProyectoDto {
  @ApiProperty({
    required: true,
    description: 'Identificador del participante proyecto',
    example: '5f3173e892e08e19101271f8',
  })
  _id: string;

  @ApiProperty({ type: [CoautorRolesDto] })
  roles: [CoautorRolesDto];

  @ApiProperty({ type: CoautorPerfilDto })
  coautor: CoautorPerfilDto;
}
