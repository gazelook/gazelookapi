import { ProyectoCompletoDto } from './proyecto-response-dto';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsString,
  IsOptional,
} from 'class-validator';
import { PerfilProyectoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import { SetParticipanteProyectoDto } from './participante-proyecto-dto';
import { IdentAlbumDto } from 'src/entidades/album/entidad/album-dto';
import { IdentVotoProyectoDto } from './voto-proyecto-dto';
import { identComentarioDto } from 'src/entidades/comentarios/entidad/comentario-dto';
import { TraduccionProyectoDto } from './traduccion-proyecto-dto';

export class ProyectoDto {
  @ApiProperty({
    description: 'Identificador del perfil',
    example: '5f3173e892e08e19101271f8',
  })
  perfil: PerfilProyectoDto;
  @ApiProperty({
    description: 'Codigo del Tipo de proyecto',
    example: '5f3ebf1dd9d7099cad68c637',
  })
  tipo: string;
  @ApiProperty({
    description: 'Localidad a la que va orientada el proyecto',
    example: '',
  })
  localidad: string;
  @ApiProperty({
    type: [],
    description: 'lista de participantes del proyecto',
    example: '',
  })
  participantes: [SetParticipanteProyectoDto];
  @ApiProperty({
    description: 'cuando gazelook recomienda un proyecto',
    example: 'true',
  })
  recomendadoAdmin: boolean;
  @ApiProperty({
    description: 'Costo del proyecto que el usuario pone al crear un proyecto',
    example: '100.000',
  })
  valorEstimado: number;
  @ApiProperty({ type: [IdentAlbumDto] })
  adjuntos: [IdentAlbumDto];
  // @ApiProperty({type:[]})
  // votos: [IdentVotoProyectoDto];
  @ApiProperty({
    description: 'numero total de votos que acumulo el proyecto',
    example: '100',
  })
  totalVotos: number;
  @ApiProperty({
    type: [],
    description: 'Traducciones en diferentes idiomas del proyecto',
    example: '',
  })
  traducciones: [TraduccionProyectoDto];
  @ApiProperty({
    description: 'Resultado de la estrategia concluida',
    example: '',
  })
  estrategia: string;
  @ApiProperty({
    type: [],
    description: 'Comentarios que le realizan al proyecto',
    example: '',
  })
  comentarios: [identComentarioDto];
  @ApiProperty({ description: 'Codigos del tipo de moneda', example: '' })
  moneda: string;
  @ApiProperty({ description: 'codigos de estados', example: '' })
  estado: string;
}
export class ProyectoUnicoDto {
  @ApiProperty()
  @IsString()
  _id: string;
}

export class ObtenerInfoproyectoDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilProyectoDto;
}

export class ObtenerProyectosDto {
  @ApiProperty({
    required: true,
    description: 'Limite de datos que se muestran por pagina',
    example: 5,
  })
  @IsNotEmpty()
  limite: number;

  @ApiProperty({ required: true, description: 'Numero de paginas', example: 1 })
  @IsNotEmpty()
  pagina: number;

  @ApiProperty({
    required: true,
    description: 'Codigo del tipo de proyecto',
    example: 'CAT_TIPO_PROY_01',
  })
  @IsNotEmpty()
  filtro: any;

  @ApiProperty({
    required: true,
    description: 'Filtro requerido que se utiliza en la consulta',
    example: 'CAT_TIPO_PROY_01',
  })
  @IsNotEmpty()
  tipo: string;

  @ApiProperty({
    description: 'Codigo del estado del proyecto',
    example: 'EST_110',
  })
  estado: string;

  @ApiProperty({
    description: 'Identificador del perfil',
    example: '5f4528c05cf8ad362fdbb98f',
  })
  perfil: string;

  @ApiProperty({
    required: false,
    description: 'Fecha inicial para la busqueda',
    example: '2020-09-01',
  })
  fechaInicial: Date;

  @ApiProperty({
    required: false,
    description: 'Fecha final para la busqueda',
    example: '2020-09-03',
  })
  fechaFinal: Date;
}

export class CatalogoLocalidadProyectoDto {
  @ApiProperty({
    required: false,
    description: 'Codigo de la localidad del proyecto',
    example: 'LOC_735',
  })
  codigo: string;
}

export class CodCatalogoTipoProyectoDto {
  @ApiProperty({
    required: false,
    description: 'Codigo del tipo del proyecto',
    example: 'CAT_TIPO_PROY_02',
  })
  codigo: string;
}

export class BusquedaProyectoTituloDto {
  @ApiProperty({ description: 'titulo largo', example: 'titulo largo' })
  titulo: string;

  @ApiProperty()
  proyectos: ProyectoCompletoDto;
}

export class BusquedaProyectoRangoDto {
  @ApiProperty()
  proyectos: ProyectoCompletoDto;
}

export class fechaMaximaValorEstimadoDto {
  @ApiProperty({
    description:
      'Fecha maxima que se puede actualizar el valor estimado del proyecto',
    example: '2020-12-22T00:00:00.000Z',
  })
  fecha: string;
}
