import { ApiProperty } from '@nestjs/swagger';
import { timestamp } from 'aws-sdk/clients/datapipeline';

export class TraduccionCatalogoTipoProyectoDto {
  @ApiProperty({
    description: 'Codigo del catalog tipo proyecto',
    example: 'CAT_TIPO_PROY_01',
  })
  referencia: string;
  @ApiProperty({
    description: 'Nombre del catalogo tipo proyecto',
    example: 'Mundial',
  })
  nombre: string;
  @ApiProperty({
    description: 'Descripción del catalogo tipo proyecto',
    example: 'Descripcion del tipo de proyecto mundial',
  })
  descripcion: string;
  @ApiProperty({
    description: 'codigo del idioma del tipo proyecto',
    example: 'IDI_1',
  })
  idioma: string;
  @ApiProperty({
    description: 'codigo del idioma del tipo proyecto',
    example: true,
  })
  original: boolean;
}

export class IdCatalogoTipoProyectoDto {
  @ApiProperty({
    description: 'Identificador de la traduccion del catalogo tipo proyecto',
    example: '5f3eaacfd9d7099cad68c632',
  })
  _id: string;
  @ApiProperty({
    description: 'Nombre del catalogo tipo proyecto',
    example: 'Mundial',
  })
  nombre: string;
  @ApiProperty({
    description: 'Descripción del catalogo tipo proyecto',
    example: 'Descripcion del tipo de proyecto mundial',
  })
  descripcion: string;
  @ApiProperty({
    description: 'codigo del idioma del tipo proyecto',
    example: 'IDI_1',
  })
  idioma: string;
  @ApiProperty({
    description: 'codigo del idioma del tipo proyecto',
    example: true,
  })
  original: boolean;
}
