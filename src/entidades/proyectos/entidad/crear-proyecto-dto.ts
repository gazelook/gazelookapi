import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsString,
  IsOptional,
} from 'class-validator';
import { CrearTraduccionProyectoDto } from './traduccion-proyecto-dto';
import {
  AlbumNuevo,
  idPerfilDto,
} from 'src/entidades/perfil/entidad/crear-perfil.dto';
import { mediaId } from 'src/entidades/media/entidad/archivo-resultado.dto';
import { Direccion } from 'src/entidades/cuenta/entidad/cuenta.dto';
import { CodCatalogoTipoProyectoDto } from './proyecto-dto';
import { CodCatalogoTipoMonedaDto } from 'src/entidades/catalogos/entidad/catalogo-tipo-moneda.dto';

export class CrearProyectoDto {
  @ApiProperty({ type: idPerfilDto })
  perfil: idPerfilDto;

  @ApiProperty({ required: true, type: [CrearTraduccionProyectoDto] })
  traducciones: Array<CrearTraduccionProyectoDto>;

  @ApiProperty({ type: CodCatalogoTipoProyectoDto })
  tipo: CodCatalogoTipoProyectoDto;

  @ApiProperty({ required: true, type: Direccion })
  direccion: Direccion; //CatalogoLocalidadProyectoDto;

  @ApiProperty({ type: [AlbumNuevo] })
  @IsOptional()
  adjuntos: AlbumNuevo[];

  @ApiProperty({ type: [mediaId] })
  @IsOptional()
  medias: mediaId[];

  @ApiProperty({
    required: true,
    description: 'Costo del proyecto que el usuario pone al crear un proyecto',
    example: 10000,
  })
  valorEstimado: number;

  @ApiProperty({
    required: true,
    description:
      'Costo del proyecto en dolares que el usuario pone al crear un proyecto',
    example: 10000,
  })
  valorEstimadoFinal: number;

  @ApiProperty({ type: CodCatalogoTipoMonedaDto })
  moneda: CodCatalogoTipoMonedaDto;
}
