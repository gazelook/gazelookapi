import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsString,
  IsOptional,
} from 'class-validator';
import { CrearTraduccionProyectoDto } from './traduccion-proyecto-dto';
import {
  AlbumNuevo,
  idPerfilDto,
} from 'src/entidades/perfil/entidad/crear-perfil.dto';
import { mediaId } from 'src/entidades/media/entidad/archivo-resultado.dto';
import { Direccion } from 'src/entidades/cuenta/entidad/cuenta.dto';
import { CodCatalogoTipoProyectoDto, ProyectoUnicoDto } from './proyecto-dto';
import { CodCatalogoTipoMonedaDto } from 'src/entidades/catalogos/entidad/catalogo-tipo-moneda.dto';

export class EstadoEstrategiaDto {
  @ApiProperty({
    description: 'Codigo del estado de la estrategia',
    example: 'EST_123',
  })
  codigo: string;
}
export class CrearEstrategiaDto {
  @ApiProperty({ type: ProyectoUnicoDto })
  proyecto: ProyectoUnicoDto;

  @ApiProperty({ type: idPerfilDto })
  perfil: idPerfilDto;

  @ApiProperty({ type: EstadoEstrategiaDto })
  estadoEstrategia: EstadoEstrategiaDto;

  @ApiProperty({ type: [mediaId] })
  @IsOptional()
  adjuntos: mediaId[];
}
