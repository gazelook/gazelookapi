import { Connection } from 'mongoose';
import { ProyectoModelo } from 'src/drivers/mongoose/modelos/proyectos/proyecto.schema';
import { CatalogoTipoProyectoModelo } from 'src/drivers/mongoose/modelos/catalogo_tipo_proyecto/catalogo-tipo-proyecto.schema';
import { TraduccionProyectoModelo } from 'src/drivers/mongoose/modelos/traduccion_proyecto/traduccion-proyecto.schema';
import { TraduccionCatalogoTipoProyectoModelo } from 'src/drivers/mongoose/modelos/traduccion_catalogo_tipo_proyecto/traduccion-catalogo-tipo-proyecto.schema';
import { VotoProyectoModelo } from 'src/drivers/mongoose/modelos/voto_proyecto/voto-proyecto.schema';
import { TraduccionVotoProyectoModelo } from 'src/drivers/mongoose/modelos/traduccion_voto_proyecto/traduccion-voto-proyecto.schema';
import { perfilModelo } from 'src/drivers/mongoose/modelos/perfil/perfil.schema';
import { ParticipanteProyectoModelo } from 'src/drivers/mongoose/modelos/participante_proyecto/participante_proyecto.schema';
import { HistoricoModelo } from 'src/drivers/mongoose/modelos/historico/historico.schema';
import { EventoModelo } from 'src/drivers/mongoose/modelos/evento/evento.schema';
import { ConfiguracionModelo } from 'src/drivers/mongoose/modelos/configuracion/configuracion.schema';
import { ConfiguracionEstiloModelo } from 'src/drivers/mongoose/modelos/configuracion_estilo/configuracion-estilo.schema';
import { CatalogoColoresModelo } from 'src/drivers/mongoose/modelos/catalogo_colores/catalogo-colores.schema';
import { CatalogoTipoColoresModelo } from 'src/drivers/mongoose/modelos/catalogo_tipo_colores/catalogo-tipo-colores.schema';
import { BeneficiarioModelo } from 'src/drivers/mongoose/modelos/beneficiario/beneficiario.schema';
import { TransaccionModelo } from 'src/drivers/mongoose/modelos/transaccion/transaccion.schema';
import { EstrategiaModelo } from 'src/drivers/mongoose/modelos/estrategia/estrategia.schema';
import { FondosTipoProyectoModelo } from 'src/drivers/mongoose/modelos/fondos_tipo_proyecto/fondos-tipo-proyecto.schema';
import { FondosFinanciamientoModelo } from 'src/drivers/mongoose/modelos/fondos_financiamiento/fondos-financiamiento.schema';

export const proyectoProviders = [
  {
    provide: 'PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('proyecto', ProyectoModelo, 'proyecto'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_proyecto',
        TraduccionProyectoModelo,
        'traduccion_proyecto',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_TIPO_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_tipo_proyecto',
        CatalogoTipoProyectoModelo,
        'catalogo_tipo_proyecto',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_CATALOGO_TIPO_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_catalogo_tipo_proyecto',
        TraduccionCatalogoTipoProyectoModelo,
        'traduccion_catalogo_tipo_proyecto',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'VOTO_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('voto_proyecto', VotoProyectoModelo, 'voto_proyecto'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_VOTO_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_voto_proyecto',
        TraduccionVotoProyectoModelo,
        'traduccion_voto_proyecto',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PARTICIPANTE_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'participante_proyecto',
        ParticipanteProyectoModelo,
        'participante_proyecto',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'HISTORICO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('historico', HistoricoModelo, 'historico'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'EVENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('evento', EventoModelo, 'evento'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_ESTILO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_estilo',
        ConfiguracionEstiloModelo,
        'configuracion_estilo',
      ),
    inject: ['DB_CONNECTION'],
  },
  // {
  //   provide: 'CATALOGO_COLORES_MODEL',
  //   useFactory: (connection: Connection) => connection.model('catalogo_colores', CatalogoColoresModelo, 'catalogo_colores'),
  //   inject: ['DB_CONNECTION']
  // } ,
  // {
  //   provide: 'CATALOGO_TIPO_COLORES_MODEL',
  //   useFactory: (connection: Connection) => connection.model('catalogo_tipo_colores', CatalogoTipoColoresModelo, 'catalogo_tipo_colores'),
  //   inject: ['DB_CONNECTION']
  // }

  {
    provide: 'BENEFICIARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('beneficiario', BeneficiarioModelo, 'beneficiario'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRANSACCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('transaccion', TransaccionModelo, 'transaccion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ESTRATEGIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('estrategia', EstrategiaModelo, 'estrategia'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'FONDOS_TIPO_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'fondos_tipo_proyecto',
        FondosTipoProyectoModelo,
        'fondos_tipo_proyecto',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'FONDOS_FINANCIAMIENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'fondos_financiamiento',
        FondosFinanciamientoModelo,
        'fondos_financiamiento',
      ),
    inject: ['DB_CONNECTION'],
  },
];
