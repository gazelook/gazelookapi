import {
  codigoEntidades,
  codigosCatalogoAcciones,
  estadosEstrategia,
  estadosProyecto,
  filtroBusqueda,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { Estrategia } from 'src/drivers/mongoose/interfaces/estrategia/estrategia.interface';
import { ActualizaEstadoProyectoService } from './actualiza-estado-proyecto.service';
import { VerificarPerfilProyectoService } from './verificar-perfil.service';

const mongoose = require('mongoose');

@Injectable()
export class ResultadoEstrategiaProyectoService {
  getProyectos: any;
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    @Inject('ESTRATEGIA_MODEL')
    private readonly estrategiaModel: Model<Estrategia>,
    private crearHistoricoService: CrearHistoricoService,
    private actualizaEstadoProyectoService: ActualizaEstadoProyectoService,
    private verificarPerfilProyectoService: VerificarPerfilProyectoService,
  ) {}

  async resultadoEstrategiaConcluida(
    idProyecto: string,
    idPerfil: string,
    adjuntos: any,
    estadoEstrategia: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let getProyecto = await this.proyectoModel.findOne({ _id: idProyecto });

      //Verifica si el perfil es coordinador
      let perfilCordinador = await this.verificarPerfilProyectoService.verificarPerfilCoordinador(
        idProyecto,
        idPerfil,
      );

      if (perfilCordinador) {
        //Objeto de tipo estrategia
        let objEstrategia: any = {
          fechaCaducidad: null,
          presupuesto: null,
          justificacion: null,
          adjuntos: adjuntos || [],
          moneda: null,
          estado: estadoEstrategia,
          fechaCreacion: new Date(),
          fechaActualizacion: null,
        };
        //Guarda la estrategia
        const crearEstrategia = await new this.estrategiaModel(
          objEstrategia,
        ).save(opts);

        const dataEstrategia = JSON.parse(JSON.stringify(crearEstrategia));
        delete dataEstrategia.__v;

        let newHistoricoEstrategia: any = {
          descripcion: dataEstrategia,
          usuario: idPerfil,
          accion: codigosCatalogoAcciones.crear,
          entidad: codigoEntidades.entidadEstrategia,
        };

        this.crearHistoricoService.crearHistoricoServer(newHistoricoEstrategia);

        //Actualiza el id estretegia
        await this.proyectoModel.updateOne(
          { _id: idProyecto },
          { estrategia: dataEstrategia._id, fechaActualizacion: new Date() },
          opts,
        );

        //Verifica si el estado de la estrategia esta aprobada para pasar el proyecto a ejecucion
        if (estadoEstrategia == estadosEstrategia.aprobada) {
          //Actualiza el estado del proyecto
          const cambiaEstadoProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
            idProyecto,
            getProyecto.estado,
            estadosProyecto.proyectoEnEjecucion,
            null,
            null,
            opts,
          );
        }
        //Verifica si el estado de la estrategia esta rechazada pasa el proyecto a su estado inicial activo
        if (estadoEstrategia == estadosEstrategia.rechazada) {
          //Actualiza el estado del proyecto
          const cambiaEstadoProyecto = await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(
            idProyecto,
            getProyecto.estado,
            estadosProyecto.proyectoActivo,
            null,
            null,
            opts,
          );
        }

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return true;
      }

      //Finaliza la transaccion
      await session.endSession();
      return false;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
