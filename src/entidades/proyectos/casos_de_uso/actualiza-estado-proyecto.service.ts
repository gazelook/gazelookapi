import { Inject, Injectable } from '@nestjs/common';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { Model } from 'mongoose';
const sw = require('stopword');
import * as mongoose from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  codigosTipoProyecto,
  estadosProyecto,
  filtrosCambioEstadoProyecto,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { VerificarPerfilProyectoService } from './verificar-perfil.service';

@Injectable()
export class ActualizaEstadoProyectoService {
  constructor(
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private verificarPerfilProyectoService: VerificarPerfilProyectoService,
  ) {}

  filtroEstados = filtrosCambioEstadoProyecto;

  async actualizaEstadoProyecto(
    idProyecto: any,
    estadoAnterior?: any,
    estadoProyecto?: string,
    idPerfil?: string,
    filtro?: any,
    opts?: any,
  ): Promise<any> {
    let session;
    if (!opts) {
      //Inicia la transaccion
      //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
      session = await mongoose.startSession();
      await session.startTransaction();
    }

    const accionAc = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    let getAccionModificar = accionAc.codigo;

    //Obtiene la entidad proyecto
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.proyectos,
    );
    let codEntidadProyecto = entidad.codigo;

    try {
      let options;
      if (!opts) {
        //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
        options = { session };
      } else {
        options = opts;
      }

      if (!estadoAnterior && !estadoProyecto) {
        let getProyecto = await this.proyectoModel
          .findOne({ _id: idProyecto })
          .session(options.session);
        estadoAnterior = getProyecto.estado;
        let tipoProyecto = getProyecto.tipo;

        const filtroSel = this.seleccionarFiltro(filtro);

        //Cambia el estado del proyecto a activa
        if (filtroSel === 1) {
          estadoProyecto = estadosProyecto.proyectoActivo;
        }
        //Cambia el estado del proyecto a ejecución
        if (filtroSel === 2) {
          if (tipoProyecto == codigosTipoProyecto.proyectoDeRed) {
            //Verifica que el perfil sea de tipo administrador
            let adminGazelook = await this.verificarPerfilProyectoService.verificarPerfilAdministrador(
              idPerfil,
            );
            if (adminGazelook) {
              estadoProyecto = estadosProyecto.proyectoEnEjecucion;
            }
          } else {
            estadoProyecto = estadosProyecto.proyectoEnEjecucion;
          }
        }
        //Cambia el estado del proyecto a eliminado
        if (filtroSel === 3) {
          estadoProyecto = estadosProyecto.proyectoEliminado;
        }
        //Cambia el estado del proyecto a espera donacion
        if (filtroSel === 4) {
          estadoProyecto = estadosProyecto.proyectoEnEsperaDonacion;
        }
        //Cambia el estado del proyecto a espera de fondos
        if (filtroSel === 5) {
          estadoProyecto = estadosProyecto.proyectoEnEsperaFondos;
        }
        //Cambia el estado del proyecto a finalizado
        if (filtroSel === 6) {
          //Verifica que el perfil sea de tipo coordinador
          let perfilCoordinador = await this.verificarPerfilProyectoService.verificarPerfilCoordinador(
            idProyecto,
            idPerfil,
          );
          if (perfilCoordinador) {
            estadoProyecto = estadosProyecto.proyectoFinalizado;
          }
        }
        //Cambia el estado del proyecto a foro
        if (filtroSel === 7) {
          estadoProyecto = estadosProyecto.proyectoForo;
        }
        //Cambia el estado del proyecto a pre estrategia
        if (filtroSel === 8) {
          estadoProyecto = estadosProyecto.proyectoPreEstrategia;
        }
        //Cambia el estado del proyecto a revision
        if (filtroSel === 9) {
          estadoProyecto = estadosProyecto.proyectoEnRevision;
        }
        //Cambia el estado del proyecto a estrategia
        if (filtroSel === 10) {
          estadoProyecto = estadosProyecto.proyectoEnEstrategia;
        }
      }

      if (estadoProyecto) {
        //Actualiza el estado del proyecto
        await this.proyectoModel.updateOne(
          { _id: idProyecto },
          { estado: estadoProyecto, fechaActualizacion: new Date() },
          options,
        );

        //Data para guardar en el historico
        let dataHisProy = {
          _id: idProyecto,
          descripcion: 'Cambio de estado a '.concat(estadoProyecto),
          estadoAnterior: estadoAnterior,
          estadoActual: estadoProyecto,
        };
        let newHistoricoProyectoUpdate: any = {
          datos: dataHisProy,
          usuario: '',
          accion: getAccionModificar,
          entidad: codEntidadProyecto,
        };
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoProyectoUpdate,
        );
        if (!opts) {
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
        }
        return true;
      }
      if (!opts) {
        //Confirma los cambios de la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      return false;
    } catch (error) {
      if (!opts) {
        //Confirma los cambios de la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      throw error;
    }
  }

  seleccionarFiltro(filtro) {
    switch (filtro) {
      case this.filtroEstados.activa:
        return 1;
      case this.filtroEstados.ejecucion:
        return 2;
      case this.filtroEstados.eliminado:
        return 3;
      case this.filtroEstados.esperaDonacion:
        return 4;
      case this.filtroEstados.esperaFondos:
        return 5;
      case this.filtroEstados.finalizado:
        return 6;
      case this.filtroEstados.foro:
        return 7;
      case this.filtroEstados.preEstrategia:
        return 8;
      case this.filtroEstados.revision:
        return 9;
      case this.filtroEstados.estrategia:
        return 10;
    }
  }
}
