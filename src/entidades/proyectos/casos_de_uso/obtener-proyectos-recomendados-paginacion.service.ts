import {
  filtroBusqueda,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { ObtenerPortadaPredeterminadaProyectoService } from './obtener-portada-predeterminada-proyecto.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { ObtenerMediasProyectoService } from './obtener-medias-proyecto.service';
const mongoose = require('mongoose');

@Injectable()
export class ObtenerProyectosRecomendadosPaginacionService {
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private traducirProyectoService: TraducirProyectoService,
    private obtenerPortadaPredeterminadaProyectoService: ObtenerPortadaPredeterminadaProyectoService,
    private obtenerMediasProyectoService: ObtenerMediasProyectoService,
  ) {}
  filtroBus = filtroBusqueda;

  async obtenerProyectoRecomendadosPaginacion(
    idioma: string,
    limite: number,
    pagina: number,
    tipo: string,
    perfil: string,
    response: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtener el codigo del idioma
      const getIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      let codIdim = getIdioma.codigo;

      //Obtener el codigo de la accion crear
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad traduccionProyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTProyecto = estado.codigo;

      //Obtiene el codigo de la entidad proyectos
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyecto = entidadP.codigo;

      //Obtiene el estado activa de la entidad proyecto
      const estadoP = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyecto,
      );
      let codEstadoProyecto = estadoP.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      const populateTraduccion = {
        path: 'traducciones',
        select:
          '-fechaActualizacion -fechaCreacion -idioma -__v -descripcion -tags -original',
        sort: '-fechaActualizacion',
        match: {
          idioma: codIdim,
          estado: codEstadoTProyecto,
        },
      };
      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdim, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdim, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };

      const medias = {
        path: 'medias',
        select: 'principal enlace miniatura catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdim, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
        ],
      };

      const votos = {
        path: 'votos',
        select: 'perfil',
        populate: { path: 'perfil', select: '_id nombre' },
      };

      const options = {
        lean: true,
        sort: '-fechaActualizacion',
        select: ' -__v -direccion  -perfil ',
        populate: [populateTraduccion, adjuntos, medias, votos],
        page: Number(pagina),
        limit: Number(limite),
      };

      //Obtiene proyectos recomendados por gazelook
      const getProyectos = await this.proyectoModel.paginate(
        {
          $and: [
            { tipo: tipo },
            { estado: codEstadoProyecto },
            { recomendadoAdmin: true },
          ],
        },
        options,
      );

      if (getProyectos.docs.length > 0) {
        const notiFinal = [];
        for (let proyecto of getProyectos.docs) {
          let actu = false;
          let datos: any;
          let votoBo: any;
          let dias = this.calcularDias(new Date(), proyecto.fechaActualizacion);
          if (dias) actu = true;

          //Data adjuntos
          // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
          const getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoService.obtenerPortadaPredeterminadaProyecto(
            proyecto.adjuntos,
            idioma,
            opts,
          );

          //Data de medias
          //Llama al metodo del obtener medias
          let getMedia: any;
          if (proyecto.medias.length > 0) {
            getMedia = await this.obtenerMediasProyectoService.obtenerMediasProyecto(
              proyecto.medias,
              idioma,
              opts,
            );
          } else {
            getMedia = proyecto.medias;
          }

          if (proyecto.votos.length > 0) {
            for (const votos of proyecto.votos) {
              if (votos['perfil']) {
                if (votos['perfil']._id == perfil) {
                  votoBo = true;
                  break;
                } else {
                  votoBo = false;
                }
              }
            }
          } else {
            votoBo = false;
          }

          let getTraduccion: any;
          //Verifica si el proyecto tiene traduccion en el idioma enviado
          if (proyecto.traducciones.length === 0) {
            //Llama al servicio de traduccion de proyecto
            getTraduccion = await this.traducirProyectoService.TraducirProyecto(
              proyecto._id,
              proyecto.perfil['_id'],
              idioma,
              opts,
            );
          } else {
            //Objeto de tipo traducccion proyecto
            let traduccionProyecto = [];
            let trad = {
              tituloCorto: proyecto.traducciones[0]['tituloCorto'],
              titulo: proyecto.traducciones[0]['titulo'],
            };
            traduccionProyecto.push(trad);
            getTraduccion = traduccionProyecto;
          }

          datos = {
            _id: proyecto._id,
            traducciones: getTraduccion,
            totalVotos: proyecto?.totalVotos,
            recomendadoAdmin: proyecto.recomendadoAdmin,
            actualizado: actu, //Si el proyecto ha sido actualizaddo recientemente =true
            adjuntos: getAdjuntos,
            medias: getMedia,
            voto: votoBo, //si el perfil ya a votado =true si no =false
            estado: {
              codigo: proyecto.estado,
            },
            fechaCreacion: proyecto.fechaCreacion,
            fechaActualizacion: proyecto.fechaActualizacion,
          };
          notiFinal.push(datos);
        }

        response.set(headerNombre.totalDatos, getProyectos.totalDocs);
        response.set(headerNombre.totalPaginas, getProyectos.totalPages);
        response.set(headerNombre.proximaPagina, getProyectos.hasNextPage);
        response.set(headerNombre.anteriorPagina, getProyectos.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return notiFinal;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return getProyectos.docs;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
  calcularDias(fechaActual, fechaActualizacion) {
    let dias =
      (fechaActual.getTime() - fechaActualizacion.getTime()) /
      (60 * 60 * 24 * 1000);
    if (dias <= 3) {
      return true;
    } else return false;
  }
}
