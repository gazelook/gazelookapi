import { Inject, Injectable } from '@nestjs/common';
import { Model, PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionCatalogoTipoProyecto } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_proyecto/traduccion-catalogo-tipo-proyecto.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import {
  estadosProyecto,
  filtroBusqueda, nombrecatalogoEstados,
  nombreEntidades
} from '../../../shared/enum-sistema';
import { CalcularDiasProyectoService } from './carcular-dias-proyecto.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from './obtener-portada-predeterminada-proyecto-resumen.service';
import { CatalogoTipoProyectoByCodigoService } from './obtener-tipo-proyecto-by-codigo.service';


const mongoose = require('mongoose');

@Injectable()
export class ObtenerTodosProyectosPaginacionService {
  getProyectos: any;
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    @Inject('TRADUCCION_CATALOGO_TIPO_PROYECTO_MODEL')
    private readonly traduccionTipoProyectoModel: Model<TraduccionCatalogoTipoProyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
    private catalogoTipoProyectoByCodigoService: CatalogoTipoProyectoByCodigoService
  ) { }
  filtroBus = filtroBusqueda;

  async obtenerTodosProyectosPaginacion(
    codIdim: string,
    limite: number,
    pagina: number,
    response: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();

    try {

      //Obtener el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad traduccionProyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTProyecto = estado.codigo;

      //Obtiene el codigo de la entidad proyectos
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyecto = entidadP.codigo;

      //Obtiene el estado activa de la entidad proyecto
      const estadoP = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyecto,
      );
      let codEstadoProyecto = estadoP.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      const sortBusqueda = {};

      sortBusqueda['fechaCreacion'] = -1;


      const populateTraduccion = {
        path: 'traducciones',
        select:
          '-fechaActualizacion -fechaCreacion -idioma -__v -descripcion -tags -original',
        match: {
          idioma: codIdioma,
          estado: codEstadoTProyecto,
        },
      };

      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };

      const medias = {
        path: 'medias',
        select: 'principal enlace miniatura catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
        ],
      };

      const votos = {
        path: 'votos',
        select: 'perfil',
        populate: { path: 'perfil', select: '_id nombre' },
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto nombreContactoTraducido',
      };

      const options = {
        lean: true,
        sort: 'fechaCreacion tipo',
        select: ' -__v -direccion  ',
        populate: [populateTraduccion, adjuntos, medias, votos, populatePerfil],
        page: Number(pagina),
        limit: Number(limite),
      };




      //Obtiene proyectos activos
      this.getProyectos = await this.proyectoModel.paginate(
        {
          $and: [
            { estado: estadosProyecto.proyectoActivo },
          ],
        },
        options,
      );

      if (this.getProyectos.docs.length > 0) {
        const listaProyectos = [];
        for (let proyecto of this.getProyectos.docs) {

          let datos: any;

          //Inicia la transaccion
          //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
          const session = await mongoose.startSession();
          session.startTransaction();
          //Datos de adjuntos
          let getAdjuntos: any;
          try {
            //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
            const opts = { session };

            // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
            getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
              proyecto.adjuntos,
              codIdim,
              opts,
            );

            //getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoService.obtenerPortadaPredeterminadaProyecto(proyecto.adjuntos, codIdioma, opts)
            //Confirma los cambios de la transaccion
            await session.commitTransaction();
            //Finaliza la transaccion
            await session.endSession();
          } catch (error) {
            //Aborta la transaccion
            await session.abortTransaction();
            //Finaliza la transaccion
            await session.endSession();
          }

          let traduccionProyecto = [];
          let trad = {
            tituloCorto: proyecto.traducciones[0]?.tituloCorto,
            titulo: proyecto.traducciones[0]?.titulo,
          };
          traduccionProyecto.push(trad);


          //Busca el tipo de proyecto
        let getTipoProyecto = await this.catalogoTipoProyectoByCodigoService.obtenerCatalogoTipoProyectoByCodigo(
          proyecto.tipo,
          codIdim,
        );

          let objPerfil = {
            _id: proyecto.perfil._id,
            nombre: proyecto.perfil.nombre,
            nombreContacto: proyecto.perfil.nombreContacto,
            nombreContactoTraducido:
              proyecto.perfil?.nombreContactoTraducido || null,
          };
          datos = {
            _id: proyecto._id,
            traducciones: traduccionProyecto,
            adjuntos: getAdjuntos,
            perfil: objPerfil,
            tipo: getTipoProyecto,
            recomendadoAdmin: proyecto.recomendadoAdmin,
            estado: {
              codigo: proyecto.estado,
            },
            fechaCreacion: proyecto.fechaCreacion,
            fechaActualizacion: proyecto.fechaActualizacion,
          };
          listaProyectos.push(datos);
        }

        response.set(headerNombre.totalDatos, this.getProyectos.totalDocs);
        response.set(headerNombre.totalPaginas, this.getProyectos.totalPages);
        response.set(headerNombre.proximaPagina, this.getProyectos.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          this.getProyectos.hasPrevPage,
        );

        return listaProyectos;
      } else {
        response.set(headerNombre.totalDatos, this.getProyectos.totalDocs);
        response.set(headerNombre.totalPaginas, this.getProyectos.totalPages);
        response.set(headerNombre.proximaPagina, this.getProyectos.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          this.getProyectos.hasPrevPage,
        );

        return this.getProyectos.docs;
      }
    } catch (error) {
      throw error;
    }
  }

}
