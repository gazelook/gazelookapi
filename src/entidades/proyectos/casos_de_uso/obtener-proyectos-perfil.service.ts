import { Inject, Injectable } from '@nestjs/common';

import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import {
  codigoEstadoAlbum,
  estadosProyecto,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import * as mongoose from 'mongoose';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { ObtenerLocalidadProyectoService } from './obtener-localidad-proyecto.service';
import { ObtenerMediasProyectoService } from './obtener-medias-proyecto.service';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { ObtenerPortadaPredeterminadaProyectoService } from './obtener-portada-predeterminada-proyecto.service';
import { ObtenerEntidadNotificacionService } from 'src/entidades/notificacion/casos_de_uso/obtener-entidad-notificacion.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from './obtener-portada-predeterminada-proyecto-resumen.service';
import { CalcularDiasProyectoService } from './carcular-dias-proyecto.service';

@Injectable()
export class ObtenerProyectosPerfilService {
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirProyectoService: TraducirProyectoService,
    private obtenerLocalidadProyectoService: ObtenerLocalidadProyectoService,
    private obtenerMediasProyectoService: ObtenerMediasProyectoService,
    private traduccionMediaService: TraduccionMediaService,
    private obtenerPortadaPredeterminadaProyectoService: ObtenerPortadaPredeterminadaProyectoService,
    private obtenerEntidadNotificacionService: ObtenerEntidadNotificacionService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
    private calcularDiasProyectoService: CalcularDiasProyectoService,
  ) {}

  async obtenerProyectosPerfilPaginacion(
    perfil: string,
    codIdim: string,
    limit: number,
    page: number,
    traducir: string,
    response?: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTradProyecto = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradProyecto,
      );
      let codEstadoTProyecto = estado.codigo;

      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyecto = entidadP.codigo;

      //Obtiene el estado
      const estadoP = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyecto,
      );
      let codEstadoProyecto = estadoP.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      let populateTraduccion;
      if (traducir === 'true') {
        console.log('trauceeeeee');
        populateTraduccion = {
          path: 'traducciones',
          select:
            '-fechaActualizacion -fechaCreacion -idioma -__v -tags -original',
          match: {
            idioma: codIdioma,
            estado: codEstadoTProyecto,
          },
        };
      } else {
        console.log('noooooooo trauceeeeee');
        populateTraduccion = {
          path: 'traducciones',
          select:
            '-fechaActualizacion -fechaCreacion -idioma -__v -tags -original',
          match: {
            original: true,
            estado: codEstadoTProyecto,
          },
        };
      }

      const medias = {
        path: 'medias',
        select:
          'principal enlace traducciones miniatura fechaCreacion fechaActualizacion catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
        ],
      };
      const media = {
        path: 'media',
        select:
          'principal enlace traducciones miniatura fechaCreacion fechaActualizacion catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
        ],
      };

      const portada = {
        path: 'portada',
        select:
          'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
        ],
      };
      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: {
          estado: codigoEstadoAlbum.activa,
        },
        populate: [media, portada],
      };

      const votos = {
        path: 'votos',
        select: 'perfil',
        populate: { path: 'perfil', select: '_id nombre' },
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto',
      };

      const options = {
        session: session,
        lean: true,
        sort: { fechaActualizacion: -1 },
        select: ' -__v ',
        populate: [populateTraduccion, adjuntos, medias, votos, populatePerfil],
        page: Number(page),
        limit: Number(limit),
      };

      const proyectos = await this.proyectoModel.paginate(
        {
          $and: [
            { perfil: perfil },
            { estado: { $ne: estadosProyecto.proyectoEliminado } },
          ],
        },
        options,
      );
      console.log('proyectos.docs.length: ', proyectos.docs.length);
      if (proyectos.docs.length > 0) {
        const notiFinal = [];
        for (let proyecto of proyectos.docs) {
          let port: any;
          let actu = false;
          let datos: any;
          let adjun = [];
          let votoBo = false;
          let getAdjuntos: any;
          if (
            proyecto.fechaCreacion.toString() ==
            proyecto.fechaActualizacion.toString()
          ) {
            actu = false;
          } else {
            let dias = this.calcularDiasProyectoService.calcularDias(
              new Date(),
              proyecto.fechaActualizacion,
            );
            if (dias) actu = true;
          }

          let notificacion: any;
          let notif: any;
          if (perfil == proyecto.perfil['_id']) {
            //Obtiene si el proyecto tiene notificaciones entregadas pero no leidas
            notificacion = this.obtenerEntidadNotificacionService.obtenerIdEntidadNotificacion(
              proyecto._id,
            );
            if (notificacion.length > 0) {
              notif = true;
            } else {
              notificacion = [];
              notif = false;
            }
          }

          console.log('proyecto***********: ', proyecto._id);
          console.log('proyecto adjuntos***********: ', proyecto.adjuntos);
          // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
          port = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
            proyecto.adjuntos,
            codIdim,
            opts,
          );
          //port = await this.obtenerPortadaPredeterminadaProyectoService.obtenerPortadaPredeterminadaProyecto(proyecto.adjuntos, codIdim, opts)
          getAdjuntos = port;

          if (proyecto.votos.length > 0) {
            for (const votos of proyecto.votos) {
              if (votos['perfil']) {
                if (votos['perfil']._id == perfil) {
                  votoBo = true;
                  break;
                } else {
                  votoBo = false;
                }
              }
            }
          } else {
            votoBo = false;
          }
          let getTraduccion: any;
          if (proyecto.traducciones[0] === undefined) {
            console.log('se debe traducir: ', proyecto._id);

            getTraduccion = await this.traducirProyectoService.TraducirProyecto(
              proyecto._id,
              perfil,
              codIdim,
              opts,
            );
          } else {
            let trad = [
              {
                tituloCorto: proyecto.traducciones[0]['tituloCorto'],
                titulo: proyecto.traducciones[0]['titulo'],
              },
            ];
            getTraduccion = trad;
          }

          datos = {
            _id: proyecto._id,
            traducciones: getTraduccion,
            adjuntos: getAdjuntos,
            perfil: proyecto.perfil,
            listaNotificacion: notificacion,
            notificacion: notif,
            fechaCreacion: proyecto.fechaCreacion,
            fechaActualizacion: proyecto.fechaActualizacion,
            estado: {
              codigo: proyecto.estado,
            },
            actualizado: actu, //Si el proyecto ha sido actualizada recientemente =true
          };
          notiFinal.push(datos);
        }

        response.set(headerNombre.totalDatos, proyectos.totalDocs);
        response.set(headerNombre.totalPaginas, proyectos.totalPages);
        response.set(headerNombre.proximaPagina, proyectos.hasNextPage);
        response.set(headerNombre.anteriorPagina, proyectos.hasPrevPage);
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return notiFinal;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return proyectos.docs;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
