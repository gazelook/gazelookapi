import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
const sw = require('stopword');
import { codigosConfiguracionEvento, estadoEvento } from '../../../shared/enum-sistema';
import { Evento } from 'src/drivers/mongoose/interfaces/evento/evento.interface';
import { ConfiguracionEventoService } from 'src/entidades/catalogos/casos_de_uso/configuracion-evento.service';
import { FormulaEventoService } from 'src/entidades/catalogos/casos_de_uso/formula_evento.service';

@Injectable()
export class ObtenerFechaIniciaForoProyectoService {
  noticia: any;

  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
  ) {}

  async obtenerFechaIniciaForoProyecto(): Promise<any> {
    try {
      //Obtengo el evento
      const getEvento = await this.eventoModel
        .findOne({
          configuracionEvento:
            codigosConfiguracionEvento.eventoForoProyectosCONFEVT_4,
            estado: estadoEvento.pendiente,
        })
        .sort('-fechaActualizacion')
        .limit(1);

      let objData = null;
      if (getEvento) {
        let getFecha = new Date(getEvento.fechaInicio);

        objData = {
          fecha: getFecha,
        };
      }
      return objData;
    } catch (error) {
      throw error;
    }
  }
}
