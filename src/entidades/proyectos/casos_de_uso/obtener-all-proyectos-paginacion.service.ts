import {
  estadosProyecto,
  filtroBusqueda,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { CalcularDiasProyectoService } from './carcular-dias-proyecto.service';
import { ObtenerEntidadNotificacionService } from 'src/entidades/notificacion/casos_de_uso/obtener-entidad-notificacion.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from './obtener-portada-predeterminada-proyecto-resumen.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { Console } from 'console';

const mongoose = require('mongoose');

@Injectable()
export class ObtenerAllProyectosPaginacionService {
  getProyectos: any;
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private traducirProyectoService: TraducirProyectoService,
    private calcularDiasProyectoService: CalcularDiasProyectoService,
    private obtenerEntidadNotificacionService: ObtenerEntidadNotificacionService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
  ) {}
  filtroBus = filtroBusqueda;

  async obtenerTodosPoyectosPaginacion(
    codIdim: string,
    limite: number,
    pagina: number,
    perfil: string,
    tipo: string,
    response: any,
  ): Promise<any> {
    console.log('tipo: ', tipo);
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const headerNombre = new HadersInterfaceNombres();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtener el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad traduccionProyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTProyecto = estado.codigo;

      //Obtiene el codigo de la entidad proyectos
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyecto = entidadP.codigo;

      //Obtiene el estado activa de la entidad proyecto
      const estadoP = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyecto,
      );
      let codEstadoProyecto = estadoP.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      const populateTraduccion = {
        path: 'traducciones',
        select:
          '-fechaActualizacion -fechaCreacion -idioma -__v -descripcion -tags -original',
        match: {
          idioma: codIdioma,
          estado: codEstadoTProyecto,
        },
      };

      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };

      const medias = {
        path: 'medias',
        select: 'principal enlace miniatura catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
        ],
      };

      const votos = {
        path: 'votos',
        select: 'perfil',
        populate: { path: 'perfil', select: '_id nombre' },
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto',
      };

      const options = {
        lean: true,
        sort: { fechaCreacion: -1 },
        select: ' -__v -direccion  ',
        populate: [populateTraduccion, adjuntos, medias, votos, populatePerfil],
        page: Number(pagina),
        limit: Number(limite),
      };

      let totalProyectos = await this.proyectoModel
        .find({
          estado: {$ne: estadosProyecto.proyectoEliminado},
          tipo: tipo,
        })
        .select('_id perfil')
        .populate({
          path: 'perfil',
          select: '_id nombre nombreContacto',
        });
      let arrayProy = [];

      for (const iterator of totalProyectos) {
        if (iterator.perfil) {
          arrayProy.push(iterator._id);
          const traducciones = await this.traduccionProyectoModel.find({
            $and: [
              { proyecto: iterator._id },
              { idioma: codIdioma },
              { estado: codEstadoTProyecto },
            ],
          });

          if (traducciones.length === 0) {
            //Llama al servicio de traduccion de proyecto
            await this.traducirProyectoService.TraducirProyecto(
              iterator._id,
              iterator.perfil,
              codIdim,
              opts,
            );
          }
        }
      }

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      //Obtiene proyectos segun en rango de fecha
      this.getProyectos = await this.proyectoModel.paginate(
        {
          _id: { $in: arrayProy },
        },
        options,
      );

      if (this.getProyectos.docs.length > 0) {
        const notiFinal = [];
        for (let proyecto of this.getProyectos.docs) {
          let actu = false;
          let datos: any;
          let votoBo: any;
          if (
            proyecto.fechaCreacion.toString() ==
            proyecto.fechaActualizacion.toString()
          ) {
            actu = false;
          } else {
            let dias = this.calcularDiasProyectoService.calcularDias(
              new Date(),
              proyecto.fechaActualizacion,
            );
            if (dias) actu = true;
          }

          let notificacion: any;
          let notif: any;
          if (perfil == proyecto.perfil._id) {
            //Obtiene si el proyecto tiene notificaciones entregadas pero no leidas
            notificacion = this.obtenerEntidadNotificacionService.obtenerIdEntidadNotificacion(
              proyecto._id,
            );
            if (notificacion.length > 0) {
              notif = true;
            } else {
              notificacion = [];
              notif = false;
            }
          }

          //Inicia la transaccion
          //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
          const session = await mongoose.startSession();
          session.startTransaction();
          //Datos de adjuntos
          let getAdjuntos: any;
          try {
            //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
            const opts = { session };

            // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
            getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
              proyecto.adjuntos,
              codIdim,
              opts,
            );
            //getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoService.obtenerPortadaPredeterminadaProyecto(proyecto.adjuntos, codIdioma, opts)
            //Confirma los cambios de la transaccion
            await session.commitTransaction();
            //Finaliza la transaccion
            await session.endSession();
          } catch (error) {
            //Aborta la transaccion
            await session.abortTransaction();
            //Finaliza la transaccion
            await session.endSession();
          }

          if (proyecto.votos.length > 0) {
            for (const votos of proyecto.votos) {
              if (votos.perfil) {
                if (votos.perfil._id == perfil) {
                  votoBo = true;
                  break;
                } else {
                  votoBo = false;
                }
              }
            }
          } else {
            votoBo = false;
          }

          let traduccionProyecto = [];
          let trad = {
            tituloCorto: proyecto.traducciones[0]?.tituloCorto,
            titulo: proyecto.traducciones[0]?.titulo,
          };
          traduccionProyecto.push(trad);

          let objPerfil = {
            _id: proyecto.perfil._id,
            nombre: proyecto.perfil.nombre,
            nombreContacto: proyecto.perfil.nombreContacto,
          };
          datos = {
            _id: proyecto._id,
            traducciones: traduccionProyecto,
            actualizado: actu, //Si el proyecto ha sido actualizaco recientemente =true
            voto: votoBo,
            adjuntos: getAdjuntos,
            perfil: objPerfil,
            listaNotificacion: notificacion,
            notificacion: notif,
            estado: {
              codigo: proyecto.estado,
            },
            fechaCreacion: proyecto.fechaCreacion,
            fechaActualizacion: proyecto.fechaActualizacion,
          };
          notiFinal.push(datos);
        }

        console.log(
          'this.getProyectos.totalDocs: ',
          this.getProyectos.totalDocs,
        );

        response.set(headerNombre.totalDatos, this.getProyectos.totalDocs);
        response.set(headerNombre.totalPaginas, this.getProyectos.totalPages);
        response.set(headerNombre.proximaPagina, this.getProyectos.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          this.getProyectos.hasPrevPage,
        );

        return notiFinal;
      } else {
        response.set(headerNombre.totalDatos, this.getProyectos.totalDocs);
        response.set(headerNombre.totalPaginas, this.getProyectos.totalPages);
        response.set(headerNombre.proximaPagina, this.getProyectos.hasNextPage);
        response.set(
          headerNombre.anteriorPagina,
          this.getProyectos.hasPrevPage,
        );

        return this.getProyectos.docs;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
