import { Injectable } from '@nestjs/common';

@Injectable()
export class CalcularDiasProyectoService {
  constructor() {}
  //Cacula si el proyecto esta actualizado recientemente (ultimos 3 dias)
  calcularDias(fechaActual, fechaActualizacion) {
    if (fechaActualizacion) {
      let dias =
        (fechaActual.getTime() - fechaActualizacion.getTime()) /
        (60 * 60 * 24 * 1000);
      if (dias <= 5) {
        return true;
      } else return false;
    } else {
      return false;
    }
  }
}
