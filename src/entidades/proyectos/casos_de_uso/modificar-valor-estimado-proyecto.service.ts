import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
const sw = require('stopword');
import {
  codigosConfiguracionEvento,
  codigosFormulasEvento,
  duracionEventos,
} from '../../../shared/enum-sistema';
import { Evento } from 'src/drivers/mongoose/interfaces/evento/evento.interface';
import { ConfiguracionEventoService } from 'src/entidades/catalogos/casos_de_uso/configuracion-evento.service';
import { FormulaEventoService } from 'src/entidades/catalogos/casos_de_uso/formula_evento.service';

@Injectable()
export class ModificarValorEstimadoProyectoService {
  noticia: any;

  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
    private configuracionEventoService: ConfiguracionEventoService,
    private formulaEventoService: FormulaEventoService,
  ) {}

  async modificarValorEstimadoProyecto(): Promise<any> {
    try {
      //Obtengo el evento
      const getEvento = await this.eventoModel
        .findOne({
          configuracionEvento:
            codigosConfiguracionEvento.eventoForoProyectosCONFEVT_4,
        })
        .sort('-fechaActualizacion')
        .limit(1);

      let objData = null;
      if (getEvento) {
        //Obtengo la configuracion del evento
        const getConfEvento = await this.configuracionEventoService.obtenerConfiguracionEvento(
          codigosConfiguracionEvento.eventoForoProyectosCONFEVT_4,
        );
        for (let i = 0; i < getConfEvento.formulas.length; i++) {
          let getFormulaEvento: any;

          const getCodFormula = getConfEvento.formulas[i];
          //Obtengo la formula del evento
          getFormulaEvento = await this.formulaEventoService.obtenerFormulaEvento(
            getCodFormula,
          );

          let formula;
          //Obteniendo formula para balance de usuarios
          if (
            getCodFormula === codigosFormulasEvento.fecha_maxima_foro_proyecto
          ) {
            let getFecha = new Date(getEvento.fechaFin);
            // console.log('getFecha: ', getFecha)
            // console.log('getFecha.getDate(): ', getFecha.getDate())
            //Se establece la fecha maxima que se puede modificar un proyecto
            //cuando el evento de foro ya se ha iniciado

            // console.log('eval(getFormulaEvento: ', eval(getFormulaEvento.replace('FECHA', getFecha.getDate())))
            //Remmplazo la fecha con la fecha de inicio del evento
            formula = getFecha.setDate(
              eval(getFormulaEvento.replace('FECHA', getFecha.getDate())),
            );
            let getFechaFin = new Date(formula);
            objData = {
              fecha: getFechaFin,
            };
          }
        }
      }
      return objData;
    } catch (error) {
      throw error;
    }
  }
}
