import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { erroresGeneral } from 'src/shared/enum-errores';
import { VerificarPerfilProyectoService } from './verificar-perfil.service';
const sw = require('stopword');
const mongoose = require('mongoose');

@Injectable()
export class RecomendarProyectoService {
  noticia: any;

  constructor(
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    private verificarPerfilProyectoService: VerificarPerfilProyectoService,
  ) { }

  async recomendarProyecto(idProyecto: string, idPerfil: string): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Verifica si el perfil es un perfil administrador de gazelook
      let adminGazelook = await this.verificarPerfilProyectoService.verificarPerfilAdministrador(
        idPerfil,
      );

      if (!adminGazelook) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_AUTORIZADO,
        };
      }

      if (adminGazelook) {
        let getProyecto = await this.proyectoModel.findById(idProyecto);

        if (getProyecto) {
          if (getProyecto.recomendadoAdmin) {
            //Se recomiendo el proyecto por un administrador de gazelook
            await this.proyectoModel.updateOne(
              { _id: idProyecto },
              { recomendadoAdmin: false, fechaActualizacion: new Date() },
              opts,
            );
          } else {
            //Se recomiendo el proyecto por un administrador de gazelook
            await this.proyectoModel.updateOne(
              { _id: idProyecto },
              { recomendadoAdmin: true, fechaActualizacion: new Date() },
              opts,
            );
          }
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          return true;
        }
        //Finaliza la transaccion
        await session.endSession();

        return false;

      }

    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
