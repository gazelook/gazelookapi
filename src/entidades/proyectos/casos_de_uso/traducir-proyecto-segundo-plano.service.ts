import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  codIdiomas,
  codigoEntidades,
  codigosCatalogoAcciones,
  codigosEstadosTraduccionProyecto,
  idiomas,
} from 'src/shared/enum-sistema';
const mongoose = require('mongoose');
const sw = require('stopword');

@Injectable()
export class TraducirProyectoServiceSegundoPlano {
  constructor(
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async traducirProyectoSegundoPlano(idProyecto, perfil, idioma) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la traduccion en el idioma original
      const getTraductorOriginal = await this.traduccionProyectoModel
        .findOne({
          proyecto: idProyecto,
          original: true,
          estado: codigosEstadosTraduccionProyecto.activa,
        })
        .session(opts.session);

      if (getTraductorOriginal) {
        if (idioma != idiomas.ingles) {
          await this.traducirProyectoIngles(
            idProyecto,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.espanol) {
          await this.traducirProyectoEspaniol(
            idProyecto,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.italiano) {
          await this.traducirProyectoItaliano(
            idProyecto,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.frances) {
          await this.traducirProyectoFrances(
            idProyecto,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.aleman) {
          await this.traducirProyectoAleman(
            idProyecto,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.portugues) {
          await this.traducirProyectoPortugues(
            idProyecto,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return true;
      }

      //Finaliza la transaccion
      await session.endSession();
      return true;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  //TRADUCCION DE PROYECTO A ESPAÑOL
  async traducirProyectoEspaniol(
    idProyecto,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.espanol,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.espanol,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let textoTrducidoDescripcion = await traducirTexto(
        idiomas.espanol,
        getTraductorOriginal.descripcion,
      );
      let descripcion = textoTrducidoDescripcion.textoTraducido;

      // let textoUnido = getTraductorOriginal.titulo + ' [-]' + getTraductorOriginal.tituloCorto +
      //     ' [-]' + getTraductorOriginal.descripcion;

      // let textoTrducido = await traducirTexto(idiomas.ingles, textoUnido);

      // const textS = textoTrducido.textoTraducido.split('[-]');

      // const T = this.gestionProyectoService.detectarIdioma(idiomas.ingles);
      // const titu = textS[0].trim().split(' ');
      // const tagsTitulo = sw.removeStopwords(titu, T);

      // const tituCort = textS[1].trim().split(' ');

      // const tagsTituloCort = sw.removeStopwords(tituCort, T);

      // const tagsUnidos = tagsTitulo.concat(tagsTituloCort);

      // const tags = tagsUnidos.filter(function (item, index, array) {
      //     return array.indexOf(item) === index;
      // })

      //datos para guardar la nueva traduccion

      let newTraduccionProyecto = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.espanol,
        original: false,
        estado: codigosEstadosTraduccionProyecto.activa,
        proyecto: idProyecto,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionProyectoModel(
        newTraduccionProyecto,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionProyecto,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.proyectoModel.updateOne(
        { _id: idProyecto },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE PROYECTO A ITALIANO
  async traducirProyectoItaliano(
    idProyecto,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.italiano,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.italiano,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let textoTrducidoDescripcion = await traducirTexto(
        idiomas.italiano,
        getTraductorOriginal.descripcion,
      );
      let descripcion = textoTrducidoDescripcion.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionProyecto = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.italiano,
        original: false,
        estado: codigosEstadosTraduccionProyecto.activa,
        proyecto: idProyecto,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionProyectoModel(
        newTraduccionProyecto,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionProyecto,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.proyectoModel.updateOne(
        { _id: idProyecto },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE PROYECTO A FRANCES
  async traducirProyectoFrances(
    idProyecto,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.frances,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.frances,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let textoTrducidoDescripcion = await traducirTexto(
        idiomas.frances,
        getTraductorOriginal.descripcion,
      );
      let descripcion = textoTrducidoDescripcion.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionProyecto = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.frances,
        original: false,
        estado: codigosEstadosTraduccionProyecto.activa,
        proyecto: idProyecto,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionProyectoModel(
        newTraduccionProyecto,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionProyecto,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.proyectoModel.updateOne(
        { _id: idProyecto },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE PROYECTO A ALEMAN
  async traducirProyectoAleman(idProyecto, perfil, getTraductorOriginal, opts) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.aleman,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.aleman,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let textoTrducidoDescripcion = await traducirTexto(
        idiomas.aleman,
        getTraductorOriginal.descripcion,
      );
      let descripcion = textoTrducidoDescripcion.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionProyecto = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.aleman,
        original: false,
        estado: codigosEstadosTraduccionProyecto.activa,
        proyecto: idProyecto,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionProyectoModel(
        newTraduccionProyecto,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionProyecto,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.proyectoModel.updateOne(
        { _id: idProyecto },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE PROYECTO A PORTUGUES
  async traducirProyectoPortugues(
    idProyecto,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.portugues,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.portugues,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let textoTrducidoDescripcion = await traducirTexto(
        idiomas.portugues,
        getTraductorOriginal.descripcion,
      );
      let descripcion = textoTrducidoDescripcion.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionProyecto = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.portugues,
        original: false,
        estado: codigosEstadosTraduccionProyecto.activa,
        proyecto: idProyecto,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionProyectoModel(
        newTraduccionProyecto,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionProyecto,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.proyectoModel.updateOne(
        { _id: idProyecto },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE PROYECTO A INGLES
  async traducirProyectoIngles(idProyecto, perfil, getTraductorOriginal, opts) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.ingles,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idiomas.ingles,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let textoTrducidoDescripcion = await traducirTexto(
        idiomas.ingles,
        getTraductorOriginal.descripcion,
      );
      let descripcion = textoTrducidoDescripcion.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionProyecto = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdiomas.ingles,
        original: false,
        estado: codigosEstadosTraduccionProyecto.activa,
        proyecto: idProyecto,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionProyectoModel(
        newTraduccionProyecto,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionProyecto,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.proyectoModel.updateOne(
        { _id: idProyecto },
        { $push: { traducciones: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
