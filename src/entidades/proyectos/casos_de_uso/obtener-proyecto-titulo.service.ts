import { Inject, Injectable } from '@nestjs/common';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import {
  codIdiomas,
  codigosArchivosSistema,
  codigosCatalogoAlbum,
  estadosProyecto,
  idiomas,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import * as mongoose from 'mongoose';
import { ObtenerPortadaService } from 'src/entidades/noticia/casos_de_uso/obtener-portada-predeterminada.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { ObtenerPortadaPredeterminadaProyectoService } from './obtener-portada-predeterminada-proyecto.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from './obtener-portada-predeterminada-proyecto-resumen.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { CatalogoTipoProyectoByCodigoService } from './obtener-tipo-proyecto-by-codigo.service';
import { Console } from 'console';
import { ListaArchivosDefaultTipoService } from 'src/entidades/archivo/casos_de_uso/lista-archivos-default-tipo.service';
import { Funcion } from 'src/shared/funcion';

@Injectable()
export class ObneterProyetoTituloService {
  proyecto: any;

  constructor(
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: PaginateModel<TraduccionProyecto>,
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private traducirProyectoService: TraducirProyectoService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
    private catalogoTipoProyectoByCodigoService: CatalogoTipoProyectoByCodigoService,
    private listaArchivosDefaultTipoService: ListaArchivosDefaultTipoService,
    private readonly funcion: Funcion,
  ) { }

  async obtenerPorTitulo(
    codIdim: string,
    titulo: string,
    limite: number,
    pagina: number,
    estadoProyecto: string,
    response: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    if (!titulo) {
      return [];
    }
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el codigo de la accion crear
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad traduccion Proyecto
      const entidadTraduccionProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTradProyecto = entidadTraduccionProyecto.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estadoTradProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradProyecto,
      );
      let codEstadoTradProyecto = estadoTradProyecto.codigo;

      //Obtiene l codigo de la entidad proyectos
      const entidadProyectos = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyectos = entidadProyectos.codigo;

      //Obtiene el estado activo de la entidad proyectos
      const estadoProyectos = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyectos,
      );

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad proyectos
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      let codEstadoProyectos = estadoProyectos.codigo;

      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      const populateAdjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select:
              'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select:
              'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto nombreContactoTraducido',
      };

      const populateProyecto = {
        path: 'proyecto',
        populate: [
          populateAdjuntos,
          populatePerfil
        ],
      };

      const options = {
        lean: true,
        session: opts.session,
        collation: { locale: 'en' },
        sort: { tituloCorto: 1 },
        select: ' -__v ',
        populate: [populateProyecto],
        page: Number(pagina),
        limit: Number(limite),
      };

      let proyectos;

      //Obtener ultimo digito
      let ultimoCaracter = titulo.charAt(titulo.length - 1);
      //Obtener ultimo digito
      let penultimoCaracter = titulo.charAt(titulo.length - 2);

      let ultDosCracteres = penultimoCaracter.concat(ultimoCaracter);
      console.log('ultDosCracteres: ', ultDosCracteres);

      let eliminar_caracteres = [
        'as',
        'es',
        'is',
        'os',
        'us',
        'AS',
        'ES',
        'IS',
        'OS',
        'US',
      ];
      let eliminarS = ['s', 'S'];
      let nuevoTitulo;
      //Elimina la plural de los dos ultimos caracteres de la palabra que se envia
      for (const recorreCaracter of eliminar_caracteres) {
        if (recorreCaracter === ultDosCracteres) {
          nuevoTitulo = titulo.substring(0, titulo.length - 1);
          nuevoTitulo = nuevoTitulo.substring(0, nuevoTitulo.length - 1);
        }
      }

      let nuevoTitulo1;
      //Elimina la s (S) del ultimo caracter de la palabra que se envia
      for (const recorreCaracter of eliminarS) {
        if (recorreCaracter === ultimoCaracter) {
          nuevoTitulo1 = titulo.substring(0, titulo.length - 1);
        }
      }

      console.log('nuevoTitulo: ', nuevoTitulo);
      //Expresion regular de solo busqueda por palabras  completas
      let regex1 = '\\b'.concat(titulo).concat('\\b');
      console.log('regex1: ', regex1);

      let regex2;
      let regex3;
      if (nuevoTitulo) {
        //Expresion regular por inicio de palabras
        // regex2 = nuevoTitulo.concat("\\b");
        regex2 = '\\b'.concat(nuevoTitulo);
        console.log('regex2: ', regex2);

        //Expresion regular de solo busqueda por palabras  completas
        regex3 = '\\b'.concat(nuevoTitulo).concat('\\b');
        console.log('regex3: ', regex3);
      } else {
        //Expresion regular por inicio de palabras
        // regex2 = titulo.concat("\\b");
        regex2 = '\\b'.concat(titulo);
        console.log('regex2: ', regex2);

        //Expresion regular de solo busqueda por palabras  completas
        regex3 = '\\b'.concat(titulo).concat('\\b');
        console.log('regex3: ', regex3);
      }

      let regex5;
      let regex6;
      if (nuevoTitulo1) {
        //Expresion regular por inicio de palabras
        // regex5 = nuevoTitulo1.concat("\\b");
        regex5 = '\\b'.concat(nuevoTitulo1);
        console.log('regex2: ', regex5);

        //Expresion regular de solo busqueda por palabras  completas
        regex6 = '\\b'.concat(nuevoTitulo1).concat('\\b');
        console.log('regex3: ', regex6);
      } else {
        //Expresion regular por fin de palabras
        // regex5 = titulo.concat("\\b");
        regex5 = '\\b'.concat(titulo);
        console.log('regex2: ', regex5);

        //Expresion regular de solo busqueda por palabras  completas
        regex6 = '\\b'.concat(titulo).concat('\\b');
        console.log('regex3: ', regex6);
      }

      //Expresion regular por fin de palabras
      // let regex4 = titulo.concat("\\b");
      // console.log('regex2: ', regex4)

      //Expresion regular por inicio de palabras
      // let regex3 = "\\b".concat(nuevoTitulo).concat(".+");
      let getProyec
      if (estadoProyecto) {
        getProyec = await this.traduccionProyectoModel
          .find({
            $or: [
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex1),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex1), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex1),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex1), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex2),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex2), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex2),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex2), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex3),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex3), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex3),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex3), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex5),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex5), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex5),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex5), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex6),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex6), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex6),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex6), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // }
            ],
          })
          .populate({
            path: 'proyecto',
            match: {
              estado: estadoProyecto
            }
          });
      } else {
        console.log('SIN ESTAOD DEL PROYECTO')
        getProyec = await this.traduccionProyectoModel
          .find({
            $or: [
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex1),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex1), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex1),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex1), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex2),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex2), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex2),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex2), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex3),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex3), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex3),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex3), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex5),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex5), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex5),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex5), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex6),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { idioma: codIdioma },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex6), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { idioma: codIdioma }
              //   ],
              // },
              {
                $and: [
                  {
                    tituloCorto: {
                      $regex: this.funcion.diacriticSensitiveRegex(regex6),
                      $options: 'i',
                    },
                  },
                  { estado: codEstadoTradProyecto },
                  { original: true },
                ],
              },
              // {
              //   $and: [
              //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex6), $options: 'i' } },
              //     { estado: codEstadoTradProyecto },
              //     { original: true }
              //   ],
              // }
            ],
          })
          .populate({
            path: 'proyecto',
            match: {
              estado: { $ne: estadosProyecto.proyectoEliminado }
            }
          });
      }

      let arrayProyec = [];
      let arrayTradProyecto = [];
      console.log('________________________________--: ', getProyec.length);
      if (getProyec.length > 0) {
        for (const listProyec of getProyec) {
          //Si existe el proyecto
          if (listProyec?.proyecto) {
            //Busca si el proyecto ya esta en el array
            let obtProyect = arrayProyec.includes(
              listProyec.proyecto['_id'].toString(),
            );

            if (obtProyect === false) {
              if (listProyec.idioma === codIdioma) {
                arrayTradProyecto.push(listProyec._id);
              } else {
                let getTradUnica = await this.traduccionProyectoModel.findOne({
                  proyecto: listProyec.proyecto['_id'],
                  idioma: codIdioma,
                  estado: codEstadoTradProyecto,
                });
                if (getTradUnica) {
                  arrayTradProyecto.push(getTradUnica._id);
                } else {
                  let getIdTraducc = await this.traducirProyectoService.TraducirProyecto(
                    listProyec.proyecto['_id'],
                    null,
                    codIdim,
                    null,
                    true,
                  );
                  arrayTradProyecto.push(getIdTraducc);
                }
              }

              arrayProyec.push(listProyec.proyecto['_id'].toString());
            }
          }
        }
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return getProyec;
      }

      proyectos = await this.traduccionProyectoModel.paginate(
        {
          _id: { $in: arrayTradProyecto },
        },
        options,
      );

      let arrayProyectos = [];
      console.log('proyectos.docs: ', proyectos.docs.length);
      for (const getProyecto of proyectos.docs) {
        let arrayTraducciones = [];

        if (getProyecto.idioma != codIdioma) {
          const getIdiomaProyectos = await this.traduccionProyectoModel.findOne(
            {
              $and: [
                { proyecto: getProyecto.proyecto['_id'] },
                { estado: codEstadoTradProyecto },
                { idioma: codIdioma },
              ],
            },
          );
          if (!getIdiomaProyectos) {
            console.log('debe traducir*********');
            arrayTraducciones = await this.traducirProyectoService.TraducirProyecto(
              getProyecto.proyecto['_id'],
              null,
              codIdim,
              opts,
            );
          } else {
            let trad = {
              tituloCorto: getIdiomaProyectos.tituloCorto,
              titulo: getIdiomaProyectos.titulo,
              descripcion: getIdiomaProyectos.descripcion,
            };
            arrayTraducciones.push(trad);
          }
        } else {
          let trad = {
            tituloCorto: getProyecto.tituloCorto,
            titulo: getProyecto.titulo,
            descripcion: getProyecto.descripcion,
          };
          arrayTraducciones.push(trad);
        }

        let album;
        if (getProyecto.proyecto['adjuntos']) {
          album = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
            getProyecto.proyecto['adjuntos'],
            codIdim,
            opts,
          );
        } else {
          // obtener portada defecto
          const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
            codigosArchivosSistema.archivo_default_proyectos,
          );
          album.push({
            tipo: { codigo: codigosCatalogoAlbum.catAlbumGeneral },
            portada: portadaAlbum,
          });
        }

        //Busca el tipo de proyecto
        let getTipoProyecto = await this.catalogoTipoProyectoByCodigoService.obtenerCatalogoTipoProyectoByCodigo(
          getProyecto.proyecto['tipo'],
          codIdim,
        );

        let objPerfil
        if (getProyecto.proyecto['perfil']) {
          objPerfil = {
            _id: getProyecto.proyecto['perfil']._id,
            nombre: getProyecto.proyecto['perfil'].nombre,
            nombreContacto: getProyecto.proyecto['perfil'].nombreContacto,
            nombreContactoTraducido:
              getProyecto.proyecto['perfil']?.nombreContactoTraducido || null,
          };
        }

        this.proyecto = {
          _id: getProyecto.proyecto['_id'],
          traducciones: arrayTraducciones,
          perfil: objPerfil,
          adjuntos: album,
          tipo: getTipoProyecto,
          recomendadoAdmin: getProyecto.proyecto['recomendadoAdmin'],
          estado: {
            codigo: getProyecto.proyecto['estado'],
          },
          fechaCreacion: getProyecto.proyecto['fechaCreacion'],
          fechaActualizacion: getProyecto.proyecto['fechaActualizacion'],
        };

        arrayProyectos.push(this.proyecto);
      }

      response.set(headerNombre.totalDatos, proyectos.totalDocs);
      response.set(headerNombre.totalPaginas, proyectos.totalPages);
      response.set(headerNombre.proximaPagina, proyectos.hasNextPage);
      response.set(headerNombre.anteriorPagina, proyectos.hasPrevPage);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return arrayProyectos;
    } catch (error) {
      throw error;
    }
  }
}
