import { Inject, Injectable } from '@nestjs/common';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  codigosCatalogoAlbum,
  estadosProyecto,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { TraducirProyectoService } from './traducir-proyecto.service';
const mongoose = require('mongoose');

@Injectable()
export class ObtenerProyetoPerfilService {
  proyecto: any;

  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirProyectoService: TraducirProyectoService,
  ) {}

  // obtiene los proyectos que no tienen que eliminarse
  async obtenerProyectoNoEliminar(idPerfil): Promise<any> {
    try {
      const getProyectoNoBorrar = await this.proyectoModel.find({
        $and: [
          {
            $or: [
              { estado: estadosProyecto.proyectoEnEjecucion },
              { estado: estadosProyecto.proyectoEnEstrategia },
              { estado: estadosProyecto.proyectoEnEsperaFondos },
              { estado: estadosProyecto.proyectoPreEstrategia },
              // { estado: estadosProyecto.proyectoForo },
              { estado: estadosProyecto.proyectoEnEsperaDonacion },
            ],
          },
          {
            perfil: idPerfil,
          },
        ],
      });
      return getProyectoNoBorrar;
    } catch (error) {
      throw error;
    }
  }

  // obtiene el proyecto que no tiene que eliminarse
  async proyectoNoEliminar(idProyecto): Promise<any> {
    try {
      const getProyectoNoBorrar = await this.proyectoModel.findOne({
        $and: [
          {
            $or: [
              { estado: estadosProyecto.proyectoEnEjecucion },
              { estado: estadosProyecto.proyectoEnEstrategia },
              { estado: estadosProyecto.proyectoEnEsperaFondos },
              { estado: estadosProyecto.proyectoPreEstrategia },
              // { estado: estadosProyecto.proyectoForo },
              { estado: estadosProyecto.proyectoEnEsperaDonacion },
            ],
          },
          {
            _id: idProyecto,
          },
        ],
      });
      return getProyectoNoBorrar;
    } catch (error) {
      throw error;
    }
  }

  async obtenerProyectosPerfil(idPerfil, codNombredioma): Promise<any> {
    try {
      //Obtiene el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codNombredioma,
      );
      let codIdioma = idioma.codigo;

      const getProyecto = await this.proyectoModel
        .find({
          $and: [
            { perfil: idPerfil },
            { estado: { $ne: estadosProyecto.proyectoEliminado } },
          ],
        })
        .populate([
          {
            path: 'traducciones',
          },
          {
            path: 'adjuntos',
          },
          {
            path: 'medias',
          },
          {
            path: 'comentarios',
          },
        ]);
      return getProyecto;
    } catch (error) {
      throw error;
    }
  }

  async obtenerProyectosRecientesPerfil(
    idPerfil,
    codNombredioma,
    idPerfilOtros,
    opts,
  ): Promise<any> {
    try {
      //Obtiene la entidad traduccionProyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTProyecto = estado.codigo;

      //Obtiene el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codNombredioma,
      );

      let proyectos = await this.getProyectos(
        idPerfil,
        codNombredioma,
        idPerfilOtros,
        opts,
      );

      for (const iterator of proyectos) {
        //const traducciones = await this.traduccionProyectoModel.find({ $and: [{ proyecto: iterator._id }, { idioma: idioma.codigo }, { estado: codEstadoTProyecto }] }).session(opts.session)

        if (iterator.traducciones.length === 0) {
          //Llama al servicio de traduccion de proyecto
          await this.traducirProyectoService.TraducirProyecto(
            iterator._id,
            iterator.perfil,
            codNombredioma,
            opts,
          );
        }
      }
      proyectos = await this.getProyectos(
        idPerfil,
        codNombredioma,
        idPerfilOtros,
        opts,
      );

      return proyectos;
    } catch (error) {
      throw error;
    }
  }

  // obtiene los proyectos recientes limite 3
  async getProyectos(
    idPerfil,
    codNombredioma,
    idPerfilOtros,
    opts,
  ): Promise<any> {
    try {
      //Obtiene la entidad traduccionProyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTProyecto = estado.codigo;

      //Obtiene el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codNombredioma,
      );
      let codIdioma = idioma.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad proyectos
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      let populateTraduccion;
      if (idPerfilOtros) {
        populateTraduccion = {
          path: 'traducciones',
          select: '-__v -descripcion -tags -original',
          sort: '-fechaActualizacion',
          match: {
            idioma: codIdioma,
            estado: codEstadoTProyecto,
          },
        };
      } else {
        populateTraduccion = {
          path: 'traducciones',
          select: '-__v -descripcion -tags -original',
          sort: '-fechaActualizacion',
          match: {
            original: true,
            estado: codEstadoTProyecto,
          },
        };
      }

      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };
      const getProyectos = await this.proyectoModel
        .find({
          $and: [
            { perfil: idPerfil },
            { estado: { $ne: estadosProyecto.proyectoEliminado } },
          ],
        })
        .populate([populateTraduccion, adjuntos])
        .sort('-fechaActualizacion')
        .limit(3)
        .session(opts.session);

      return getProyectos;
    } catch (error) {
      throw error;
    }
  }
}
