import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { TraducirCatalogoTipoProyectoService } from './traducir-tipo-proyecto.service';
import { CatalogoTipoProyecto } from 'src/drivers/mongoose/interfaces/catalogo_tipo_proyecto/catalogo-tipo-proyecto.interface';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
const mongoose = require('mongoose');

@Injectable()
export class CatalogoTipoProyectoService {
  constructor(
    @Inject('CATALOGO_TIPO_PROYECTO_MODEL')
    private readonly catalogoTipoProyectoModel: Model<CatalogoTipoProyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private traducirCatalogoTipoProyectoService: TraducirCatalogoTipoProyectoService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async obtenerCatalogoTipoProyecto(codIdiom: string): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la entidad catalogo tipo proyecto
      const entidadTipoProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.catalogoTipoProyecto,
      );
      let codEntidadTipoProyecto = entidadTipoProyecto.codigo;

      //Obtiene el estado  activa de la entidad catalogo tipo proyecto
      const estadoProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTipoProyecto,
      );
      let codEstadoTipoProyecto = estadoProyecto.codigo;

      //Obtiene la entidad traduccion catalogo tipo proyecto
      const entidadTTipoProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionCatalogoTipoProyecto,
      );
      let codEntidadTProyecto = entidadTTipoProyecto.codigo;

      //Obtiene el estado  activa de la entidad catalogo tipo proyecto
      const estadoTradProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTProyecto,
      );
      let codEstadoTradTipoProyecto = estadoTradProyecto.codigo;

      //Obtiene el idioma por el codigo
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdiom,
      );
      let codIdioma = idioma.codigo;

      //Obtiene el catalogo tipo proyecto
      const obtenerTipoProyecto = await this.catalogoTipoProyectoModel
        .find(
          { estado: codEstadoTipoProyecto },
          { _id: 1, codigo: 1, traducciones: 1, fechaActualizacion: 1 },
        )
        .populate({
          path: 'traducciones',
          match: {
            idioma: codIdioma,
            estado: codEstadoTradTipoProyecto,
          },
          select: 'nombre descripcion idioma original fecha_actualizacion',
        });

      let existe: boolean;

      //Variable para identificar si una traduccion es la original
      let IdentificadorTraduccionTipoProyecto: string;
      let nombreTipoProyecto: string;
      let descripcionTipoProyecto: string;

      //Verifica si tiene proyecto
      if (obtenerTipoProyecto.length > 0) {
        for (let i = 0; i < obtenerTipoProyecto.length; i++) {
          let idTipoProyecto = obtenerTipoProyecto[i]._id;

          let codigoTipoProyecto = obtenerTipoProyecto[i].codigo;
          let traducciones = obtenerTipoProyecto[i].traducciones.length;
          if (traducciones > 0) {
            existe = true;
          } else {
            existe = false;
            //Obtiene los pensamientos uno a uno
            let getTipoProyecto = await this.catalogoTipoProyectoModel
              .findOne({ _id: idTipoProyecto, estado: codEstadoTipoProyecto })
              .populate({
                path: 'traducciones',
                match: {
                  original: true,
                  estado: codEstadoTradTipoProyecto,
                },
              });

            const dataTipoProyecto = JSON.parse(
              JSON.stringify(getTipoProyecto.traducciones[0]),
            );
            nombreTipoProyecto = dataTipoProyecto.nombre;
            descripcionTipoProyecto = dataTipoProyecto.descripcion;
            IdentificadorTraduccionTipoProyecto = dataTipoProyecto._id;
          }

          //Si la traduccion no existe la crea
          if (existe === false) {
            //await this.traducirPensamientoService.TraducirPensamiento(idPensamiento, IdentificadorTraduccionPensamiento, perfil, headers.idioma)
            await this.traducirCatalogoTipoProyectoService.TraducirCatalogoTipoProyecto(
              idTipoProyecto,
              codigoTipoProyecto,
              nombreTipoProyecto,
              descripcionTipoProyecto,
              codIdiom,
              opts,
            );
          }
        }

        //Obtiene el catalogo tipo proyecto
        const getTipoProyecto = await this.catalogoTipoProyectoModel
          .find(
            { estado: codEstadoTipoProyecto },
            { _id: 1, codigo: 1, traducciones: 1, fechaCreacion: 1 },
          )
          .session(session)
          .populate({
            path: 'traducciones',
            match: {
              idioma: codIdioma,
              estado: codEstadoTradTipoProyecto,
            },
            select: 'nombre descripcion idioma original fecha_actualizacion',
          });

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return getTipoProyecto;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
