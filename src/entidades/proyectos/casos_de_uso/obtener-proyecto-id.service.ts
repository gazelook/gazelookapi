import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  codigosEstadosTraduccionProyecto,
  codigosTipoProyecto,
  estadosProyecto,
} from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerProyetoIdService {
  proyecto: any;

  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
  ) {}

  async obtenerProyectoById(idProyecto): Promise<any> {
    try {
      const getProyecto = await this.proyectoModel.findOne({ _id: idProyecto });
      return getProyecto;
    } catch (error) {
      throw error;
    }
  }

  async obtenerProyectoByIdPersonalizado(idProyecto): Promise<any> {
    try {
      const populateMedia = {
        path: 'media',
        select:
          'principal miniatura catalogoMedia fechaActualizacion traducciones',
        populate: [
          {
            path: 'principal',
            select: 'url tipo filename fileDefault duracion fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: 'filename url tipo',
          },
        ],
      };

      const populateReferenciaEntidadCompartida = {
        select: '_id traducciones adjuntos fechaActualizacion',
        populate: [
          {
            path: 'traducciones',
            select: 'idioma titulo tituloCorto',
          },
          {
            path: 'adjuntos',
            populate: [
              populateMedia,
              {
                path: 'portada',
                populate: populateMedia,
              },
            ],
          },
        ],
      };

      let getProyecto = await this.proyectoModel
        .findOne({ _id: idProyecto })
        .select('_id traducciones adjuntos fechaActualizacion')
        .populate([
          {
            path: 'traducciones',
            select: 'idioma titulo tituloCorto',
          },
          {
            path: 'adjuntos',
            populate: [
              populateMedia,
              {
                path: 'portada',
                populate: populateMedia,
              },
            ],
          },
        ])
        .lean();
      return getProyecto;
    } catch (error) {
      throw error;
    }
  }

  async obtenerProyectoOriginal(idProyecto: string, original: boolean) {
    const getProyecto = await this.proyectoModel
      .findOne({ _id: idProyecto })
      .populate({
        path: 'traducciones',
        match: {
          original: original,
          estado: codigosEstadosTraduccionProyecto.activa,
        },
      });

    return getProyecto;
  }

  async obtenerTotalProyectos() {
    try {
      const getProyecto = await this.proyectoModel.find({
        estado: { $ne: estadosProyecto.proyectoEliminado },
      });

      return getProyecto.length;
    } catch (error) {
      throw error;
    }
  }

  async obtenerTotalMundialProyectos() {
    try {
      const getProyecto = await this.proyectoModel.find({
        estado: { $ne: estadosProyecto.proyectoEliminado },
        tipo: codigosTipoProyecto.proyectoMundial,
      });

      return getProyecto.length;
    } catch (error) {
      throw error;
    }
  }

  async obtenerTotalLocalProyectos() {
    try {
      const getProyecto = await this.proyectoModel.find({
        estado: { $ne: estadosProyecto.proyectoEliminado },
        tipo: codigosTipoProyecto.proyectoLocal,
      });

      return getProyecto.length;
    } catch (error) {
      throw error;
    }
  }

  async obtenerTotalPaisProyectos() {
    try {
      const getProyecto = await this.proyectoModel.find({
        estado: { $ne: estadosProyecto.proyectoEliminado },
        tipo: codigosTipoProyecto.proyectoPais,
      });

      return getProyecto.length;
    } catch (error) {
      throw error;
    }
  }

  async obtenerTotalRedProyectos() {
    try {
      const getProyecto = await this.proyectoModel.find({
        estado: { $ne: estadosProyecto.proyectoEliminado },
        tipo: codigosTipoProyecto.proyectoDeRed,
      });

      return getProyecto.length;
    } catch (error) {
      throw error;
    }
  }
}
