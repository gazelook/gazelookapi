import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { CrearParticipanteProyectoRolesService } from 'src/entidades/participante_proyecto/casos_de_uso/crear-participante-proyecto-roles.service';
import { PerfilIdDto } from 'src/entidades/usuario/dtos/perfil.dto';
import { erroresGeneral, erroresProyecto } from 'src/shared/enum-errores';
import { codigoEntidades, codigosCatalogoRol } from 'src/shared/enum-sistema';

const mongoose = require('mongoose');

@Injectable()
export class AsignarEquipoProyectoService {
  getProyectos: any;
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private crearParticipanteProyectoRolesService: CrearParticipanteProyectoRolesService,
  ) { }

  async asignarEquipo(
    idProyecto: string,
    idPerfil: string,
    perfiles: Array<PerfilIdDto>,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //vERIFICA SI ES EL PERFIL PROPIETARIO DEL PROYECTO
      let getProyecto = await this.proyectoModel.findOne({
        _id: idProyecto,
        perfil: idPerfil,
      });

      if (getProyecto) {
        console.log('es propietario del proyecto');
        //Verfica si el rol es estratega y que la entidad sea comentarios
        if (perfiles.length > 0) {
          for (const perfil of perfiles) {
            let crearParticipanteProyecto = await this.crearParticipanteProyectoRolesService.crearParticipanteProyectoByRoles(
              idPerfil,
              idProyecto,
              codigosCatalogoRol.usuarioEquipoProyecto,
              codigoEntidades.entidadProyectos,
              perfil._id,
              opts,
            );
          }

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          return true;
        }
      } else {
        //Finaliza la transaccion
        await session.endSession();
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_AUTORIZADO,
        };
      }
      
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
