import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { PaginateModel } from 'mongoose';
const sw = require('stopword');
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { EliminarParticipanteProyectoCompletoService } from 'src/entidades/participante_proyecto/casos_de_uso/eliminar-participante-proyecto-completo.service';
import { EliminarAlbumCompletoService } from 'src/entidades/album/casos_de_uso/eliminar-album-completo.service';
import { EliminarMediaCompletoService } from 'src/entidades/media/casos_de_uso/eliminar-media-completo.service';
import { VotoProyecto } from 'src/drivers/mongoose/interfaces/voto_proyecto/voto-proyecto.interface';
import { TraduccionesVotoProyecto } from 'src/drivers/mongoose/interfaces/traduccion_voto_proyecto/traduccion-voto-proyecto.interface';
import { EliminarComentarioCompletoService } from 'src/entidades/comentarios/casos_de_uso/eliminar-comentario-completo.service';
import { EliminarDireccionCompletoService } from 'src/entidades/direccion/casos_de_uso/eliminar-direccion-completo.service';

@Injectable()
export class EliminarProyectoCompletoService {
  noticia: any;

  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    @Inject('VOTO_PROYECTO_MODEL')
    private readonly votoProyectoModel: Model<VotoProyecto>,
    @Inject('TRADUCCION_VOTO_PROYECTO_MODEL')
    private readonly traduccionVotoProyectoModel: Model<
      TraduccionesVotoProyecto
    >,
    private eliminarParticipanteProyectoCompletoService: EliminarParticipanteProyectoCompletoService,
    private eliminarAlbumCompletoService: EliminarAlbumCompletoService,
    private eliminarMediaCompletoService: EliminarMediaCompletoService,
    private eliminarComentarioCompletoService: EliminarComentarioCompletoService,
    private eliminarDireccionCompletoService: EliminarDireccionCompletoService,
  ) {}

  async eliminarProyectoCompleto(idPerfil: string, opts): Promise<any> {
    try {
      const proyecto = await this.proyectoModel
        .find({ perfil: idPerfil })
        .populate({
          path: 'comentarios',
          // select: '_id adjuntos',
          populate: {
            path: 'traducciones',
          },
        });

      if (proyecto) {
        for (const getProyecto of proyecto) {
          console.log('getProyecto------------------------: ', getProyecto._id);

          //Comprueba si tiene participantes
          if (getProyecto.participantes.length > 0) {
            //Elimina todos los participantes del proyecto
            await this.eliminarParticipanteProyectoCompletoService.eliminarParticipanteProyectoCompleto(
              getProyecto._id,
              opts,
            );
          }
          console.log(
            'acaaaaaaaaaaaaaaaaaaaaaaaaaaaaa------------------------',
          );
          //Comprueba si el proyecto tiene adjuntos
          if (getProyecto.adjuntos.length > 0) {
            //Elimina todos los adjuntos uno a uno
            for (const getAdjuntos of getProyecto.adjuntos) {
              await this.eliminarAlbumCompletoService.eliminarAlbumCompleto(
                getAdjuntos,
                opts,
              );
            }
          }
          //Comprueba si el proyecto tiene medias
          if (getProyecto.medias.length > 0) {
            //Elimina todos las medias uno a uno
            for (const getMedias of getProyecto.medias) {
              await this.eliminarMediaCompletoService.eliminarMediaCompleto(
                getMedias,
                opts,
              );
            }
          }
          console.log('DESPUES DE ELIMINAR MEDIASSSSSSSS PROYECTO');
          //Comprueba si el proyecto tiene votos
          if (getProyecto.votos.length > 0) {
            //Elimina la traduccion de los votos uno a uno
            for (const getVotoProyecto of getProyecto.votos) {
              await this.traduccionVotoProyectoModel.deleteMany(
                { votoProyecto: getVotoProyecto },
                opts,
              );
            }
            await this.votoProyectoModel.deleteMany(
              { proyecto: getProyecto._id },
              opts,
            );
          }
          console.log('DESPUES DE ELIMINAR VOTOSSSSSSSSSSS PROYECTO');
          //Comprueba si el proyecto tiene traducciones
          if (getProyecto.traducciones.length > 0) {
            //Elimina todas las traducciones del proyecto
            await this.traduccionProyectoModel.deleteMany(
              { proyecto: getProyecto._id },
              opts,
            );
          }
          console.log('DESPUES DE ELIMINAR TRADUCCION PROYECTO');

          //Comprueba si el proyecto tiene comentarios
          if (getProyecto.comentarios.length > 0) {
            //Elimina los comentarios uno a uno
            for (const getComentarios of getProyecto.comentarios) {
              if (getComentarios) {
                await this.eliminarComentarioCompletoService.eliminarComentarioCompleto(
                  getComentarios,
                  opts,
                );
              }
            }
          }
          console.log('DESPUES DE ELIMINAR COMENTARIOS PROYECTO');
          //Comprueba si el proyecto tiene direccion
          if (getProyecto.direccion) {
            //ELimina la direccion completa del proyecto
            await this.eliminarDireccionCompletoService.eliminarDireccionCompleto(
              getProyecto.direccion,
              opts,
            );
          }

          //Elimina el proyecto completo
          await this.proyectoModel.deleteOne({ _id: getProyecto._id }, opts);
          console.log('DESPUES DE ELIMINAR PROYECTO: ', getProyecto._id);
        }
      }

      console.log(
        '**********************____________________________________',
        idPerfil,
      );
      //Comprueba si existe el perfil en algun proyecto como participante para eliminarlo
      const getProyectoParticipante = await this.eliminarParticipanteProyectoCompletoService.eliminarExisteParticipanteProyectoCompleto(
        idPerfil,
        opts,
      );

      console.log('DESPUES DE ELIMINAR PARTICIPANTE PROYECTO DEL PERFIL');
      // retorna el proyecto y los comentarios que ha hecho en otros proyectos.
      return {
        proyecto: proyecto,
        proyectoParticipante: getProyectoParticipante,
      };
    } catch (error) {
      throw error;
    }
  }
}
