import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  codigosRol,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class ListaCoAutoresService {
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async obtenerListaCoAutores(
    idProyecto: string,
    idPerfil: string,
    codIdiom: string,
    limite: number,
    pagina: number,
  ): Promise<any> {
    try {
      //Obtiene la entidad proyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let getCodEntProyecto = entidad.codigo;

      //Obtiene el estado activa de proyectos
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        getCodEntProyecto,
      );
      let codEstadoProyecto = estado.codigo;

      //Obtiene el idioma por el codigo
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdiom,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad traduccion proyecto
      const entidadTN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTN = entidadTN.codigo;

      //Obtiene el estado  activa de la entidad traduccion proyecto
      const estadoTN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTN,
      );
      let codEstadoTN = estadoTN.codigo;

      //Obtiene la entidad participante proyecto
      const entidadParProyec = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.participanteProyecto,
      );
      let codEntidadPartProyecto = entidadParProyec.codigo;

      //Obtiene el estado  activa de la entidad traduccion proyecto
      const estadoPartProyec = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadPartProyecto,
      );
      let codEstadoPartProyecto = estadoPartProyec.codigo;

      //Obtiene la entidad rol_entidad
      const entidadRolEntidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.rolEntidad,
      );
      let codRolEntidad = entidadRolEntidad.codigo;

      //Obtiene el estado  activa de la entidad traduccion proyecto
      const estadoRolEntidad = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codRolEntidad,
      );
      let codEstadoRolEntidad = estadoRolEntidad.codigo;

      //Obtiene la entidad comentarios
      const entidadComentarios = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarios,
      );
      let codEntComentarios = entidadComentarios.codigo;

      const populateRoles = {
        path: 'roles',
        select: 'nombre rol',
        match: {
          rol: codigosRol.cod_rol_ent_co_autor,
          estado: codEstadoRolEntidad,
          entidad: codEntComentarios,
        },
      };
      const populateCoautor = {
        path: 'coautor',
        select: 'nombre nombreContacto',
      };

      const participantes = {
        path: 'participantes',
        select: 'coautor roles',
        match: {
          estado: codEstadoPartProyecto,
          coautor: { $ne: idPerfil },
        },
        populate: [populateRoles, populateCoautor],
      };

      const options = {
        lean: true,
        select: ' _id participantes perfil',
        populate: [participantes],
        page: Number(pagina),
        limit: Number(limite),
      };

      //Obtiene el catalogo tipo proyecto
      const obtenerPerfilesCoautores = await this.proyectoModel.paginate(
        { _id: idProyecto, perfil: idPerfil, estado: codEstadoProyecto },
        options,
      );

      return obtenerPerfilesCoautores;
    } catch (error) {
      throw error;
    }
  }
}
