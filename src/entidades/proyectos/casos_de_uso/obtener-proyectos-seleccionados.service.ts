import { Inject, Injectable } from '@nestjs/common';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { Model, PaginateModel } from 'mongoose';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  codigosConfiguracionEvento,
  estadoEvento,
  estadosProyecto,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ObtenerLocalidadProyectoService } from './obtener-localidad-proyecto.service';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { ObtenerMediasProyectoService } from './obtener-medias-proyecto.service';
import { Evento } from 'src/drivers/mongoose/interfaces/evento/evento.interface';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from './obtener-portada-predeterminada-proyecto-resumen.service';
import { CatalogoTipoProyectoByCodigoService } from './obtener-tipo-proyecto-by-codigo.service';
const mongoose = require('mongoose');

@Injectable()
export class ObtenerProyectosSeleccionadosService {
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    @Inject('EVENTO_MODEL') private readonly eventoModel: PaginateModel<Evento>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private traducirProyectoService: TraducirProyectoService,
    private obtenerLocalidadProyectoService: ObtenerLocalidadProyectoService,
    private catalogoTipoProyectoByCodigoService: CatalogoTipoProyectoByCodigoService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
  ) { }

  async obtenerProyectosSeleccionados(
    idPerfil: string,
    tipo: string,
    fechaInicial: any,
    fechaFinal: any,
    limite: number,
    pagina: number,
    codIdioma: string,
    response: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const headerNombre = new HadersInterfaceNombres();

    //Obtiene el codigo del idioma
    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codIdioma,
    );
    let codIdi = idioma.codigo;

    //Obtiene la entidad traduccion Proyecto
    const entidadTraduccionProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionProyecto,
    );
    let codEntidadTradProyecto = entidadTraduccionProyecto.codigo;

    //Obtiene el estado activa de la entidad traduccion proyecto
    const estadoTradProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradProyecto,
    );
    let codEstadoTradProyecto = estadoTradProyecto.codigo;

    //Obtiene el codigo de la entidad media
    const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    let codEntidadMedia = entidadmedia.codigo;

    //Obtiene el estado activo de la entidad proyectos
    const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadMedia,
    );
    let codEstadoMedia = estadoMedia.codigo;

    //Obtiene l codigo de la entidad album
    const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.album,
    );
    let codEntidadAlbum = entidadAlbum.codigo;

    //Obtiene el estado activo de la entidad album
    const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadAlbum,
    );
    let codEstadoAlbum = estadoAlbum.codigo;

    //Obtiene el codigo de la entidad traduccion media
    const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    let codEntidadTradMedia = entidadTradMedia.codigo;

    //Obtiene el estado activo de la entidad traduccion media
    const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradMedia,
    );
    let codEstadoTradMedia = estadoTradMedia.codigo;

    //Obtiene la entidad traduccionProyecto
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionProyecto,
    );
    let codEntidadTraduccion = entidad.codigo;

    //Obtiene el estado activa de la entidad traduccion proyecto
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTraduccion,
    );
    let codEstadoTProyecto = estado.codigo;

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const populateTraduccion = {
        path: 'traducciones',
        select:
          '-fechaActualizacion -fechaCreacion -idioma -__v -descripcion -tags -original',
        match: {
          idioma: codIdioma,
          estado: codEstadoTProyecto,
        },
      };
      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto nombreContactoTraducido',
      };

      if (!fechaInicial || !fechaFinal) {
        fechaInicial = null;
        fechaFinal = null;
      }

      let getProyectos: any;
      const populateProyectos = {
        sort: '-totalVotos',
        // path: 'proyectos',
        // match: {

        //   tipo: tipo,
        //   estado: estadosProyecto.proyectoPreEstrategia,
        // },
        select: ' -__v ',
        populate: [populateTraduccion, adjuntos, populatePerfil],
        page: Number(pagina),
        limit: Number(limite),
      };


      let getEvento
      if (!fechaInicial && !fechaFinal) {
        //Obtengo el evento
        getEvento = await this.eventoModel.find(
          {
            $and: [
              {
                configuracionEvento:
                  codigosConfiguracionEvento.eventoForoProyectosCONFEVT_4,
              },
              {
                estado: estadoEvento.ejecutado,
              },
            ],
          }
        )
          .populate({
            path: 'proyectos',
            match: {
              tipo: tipo,
              estado: estadosProyecto.proyectoPreEstrategia,
            },
          }).sort({
            fechaCreacion: -1
          }).limit(1);
      } else {
        const ini = new Date(fechaInicial);
        const fecFin = fechaFinal + 'T23:59:59.999Z';
        const fin = new Date(fecFin);
        getEvento = await this.eventoModel.find(
          {
            $and: [
              {
                configuracionEvento:
                  codigosConfiguracionEvento.eventoForoProyectosCONFEVT_4,
              },
              {
                estado: estadoEvento.ejecutado,
              },
              // { fechaFin: { $gte: ini } },
              // { fechaFin: { $lte: fin } },
            ],
          }
        )
          .populate({
            path: 'proyectos',
            match: {
              $and: [
                {
                  tipo: tipo
                },
                {
                  estado: estadosProyecto.proyectoPreEstrategia,
                },
                { fechaActualizacion: { $gte: ini } },
                { fechaActualizacion: { $lte: fin } },
              ]
            },
          }).sort({
            fechaCreacion: -1
          }).limit(1);;
      }

      let dataProyectos = [];
      let proyect = []
      console.log('getEvento.length: ', getEvento.length)
      if (getEvento.length > 0) {
        if (getEvento[0].proyectos.length > 0) {
          console.log('getEvento[0]: ', getEvento[0].proyectos)
          for (const proy of getEvento[0].proyectos) {
            proyect.push(proy._id);
          }
        }
      }

      console.log('proyect: ', proyect)
      getProyectos = await this.proyectoModel.paginate(
        {
          _id: { $in: proyect },

        },
        populateProyectos
      );
      let adjun = [];
      let objAdjun: any;
      let getAdjuntos: any;

      if (getProyectos.docs.length > 0) {
        for (const proyecto of getProyectos.docs) {
          // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
          getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
            proyecto.adjuntos,
            codIdioma,
            opts,
          );


          //Data de traducciones del proyecto
          let traduccionProyecto = [];
          //Verifica si tiene la traduccion
          if (proyecto.traducciones.length === 0) {
            const idPerfil = proyecto.perfil;
            traduccionProyecto = await this.traducirProyectoService.TraducirProyecto(
              proyecto._id,
              idPerfil,
              codIdioma,
              opts,
            );
          } else {
            //Objeto de tipo traducccion proyecto
            let trad = {
              tituloCorto: proyecto.traducciones[0]['tituloCorto'],
              titulo: proyecto.traducciones[0]['titulo'],
              descripcion: proyecto.traducciones[0]['descripcion'],
            };
            traduccionProyecto.push(trad);
          }


          //Busca el tipo de proyecto
          let getTipoProyecto = await this.catalogoTipoProyectoByCodigoService.obtenerCatalogoTipoProyectoByCodigo(
            proyecto.tipo,
            codIdioma,
          );

          let objPerfil = {
            _id: proyecto.perfil._id,
            nombre: proyecto.perfil.nombre,
            nombreContacto: proyecto.perfil.nombreContacto,
            nombreContactoTraducido:
              proyecto.perfil?.nombreContactoTraducido || null,
          };

          //Objeto de proyecto
          let datos = {
            _id: proyecto._id,
            traducciones: traduccionProyecto,
            adjuntos: getAdjuntos,
            perfil: objPerfil,
            valorEstimado: proyecto.valorEstimado,
            tipo: getTipoProyecto,
            totalVotos: proyecto.totalVotos,
            moneda: {
              codNombre: proyecto.moneda,
            },
            estado: {
              codigo: proyecto.estado,
            },
            fechaCreacion: proyecto.fechaCreacion,
            fechaActualizacion: proyecto.fechaActualizacion,
          };
          dataProyectos.push(datos);
        }
        if (dataProyectos.length <= limite) {
          pagina = 1;
        } else {
          pagina = Math.round(dataProyectos.length / limite);
        }

        response.set(headerNombre.totalDatos, getProyectos.totalDocs);
        response.set(headerNombre.totalPaginas, getProyectos.totalPages);
        response.set(headerNombre.proximaPagina, getProyectos.hasNextPage);
        response.set(headerNombre.anteriorPagina, getProyectos.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataProyectos;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataProyectos;
      }

    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
