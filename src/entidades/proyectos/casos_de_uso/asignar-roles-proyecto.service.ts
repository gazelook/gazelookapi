import {
  codigoEntidades,
  codigosCatalogoRol,
  filtroBusqueda,
} from '../../../shared/enum-sistema';
import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { CrearParticipanteProyectoRolesService } from 'src/entidades/participante_proyecto/casos_de_uso/crear-participante-proyecto-roles.service';
import { VerificarPerfilProyectoService } from './verificar-perfil.service';

const mongoose = require('mongoose');

@Injectable()
export class AsignarRolesProyectoService {
  getProyectos: any;
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private crearParticipanteProyectoRolesService: CrearParticipanteProyectoRolesService,
    private verificarPerfilProyectoService: VerificarPerfilProyectoService,
  ) {}

  async asignarRoles(
    idProyecto: string,
    idPerfil: string,
    rolEntidad: string,
    codEntidad: string,
    idPerfilRol: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //VERIFICA SI ES EL PERFIL PROPIETARIO DEL PROYECTO
      let getProyecto = await this.proyectoModel.findOne({
        _id: idProyecto,
        perfil: idPerfil,
      });

      if (getProyecto) {
        console.log('es propietario del proyecto');
        //Verfica si el rol es estratega y que la entidad sea comentarios
        if (
          rolEntidad == codigosCatalogoRol.estrategas &&
          codEntidad == codigoEntidades.entidadComentarios
        ) {
          console.log('Rol: estratega');
          console.log('Entidad: comentarios');
          let crearParticipanteProyecto = await this.crearParticipanteProyectoRolesService.crearParticipanteProyectoByRoles(
            idPerfil,
            idProyecto,
            rolEntidad,
            codEntidad,
            idPerfilRol,
            opts,
          );
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          return crearParticipanteProyecto;
        }
      } else {
        console.log('aacccccaaa');

        let adminGazelook = await this.verificarPerfilProyectoService.verificarPerfilAdministrador(
          idPerfil,
        );

        if (adminGazelook) {
          console.log('Perfil usuario administrador');
          //Verfica si el rol es coordinador y que la entidad sea comentarios
          if (
            rolEntidad == codigosCatalogoRol.coordinadorGazelook &&
            codEntidad == codigoEntidades.entidadComentarios
          ) {
            console.log('Rol: coordinador');
            console.log('Entidad: comentarios');
            let crearParticipanteProyecto = await this.crearParticipanteProyectoRolesService.crearParticipanteProyectoByRoles(
              idPerfil,
              idProyecto,
              rolEntidad,
              codEntidad,
              idPerfilRol,
              opts,
            );

            //Confirma los cambios de la transaccion
            await session.commitTransaction();
            //Finaliza la transaccion
            await session.endSession();

            return crearParticipanteProyecto;
          }

          //Verfica si el rol es coordinador y que la entidad sea estrategia
          if (
            rolEntidad == codigosCatalogoRol.coordinadorGazelook &&
            codEntidad == codigoEntidades.entidadEstrategia
          ) {
            console.log('Rol: coordinador');
            console.log('Entidad: estrategia');

            let crearParticipanteProyecto = await this.crearParticipanteProyectoRolesService.crearParticipanteProyectoByRoles(
              idPerfil,
              idProyecto,
              rolEntidad,
              codEntidad,
              idPerfilRol,
              opts,
            );

            //Confirma los cambios de la transaccion
            await session.commitTransaction();
            //Finaliza la transaccion
            await session.endSession();

            return crearParticipanteProyecto;
          }
        }
      }

      return false;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
