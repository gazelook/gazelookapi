import { Injectable } from '@nestjs/common';
import { codIdiomas, idiomas } from 'src/shared/enum-sistema';
const sw = require('stopword');

@Injectable()
export class GestionProyectoService {
  arrayIdiomas = [
    {
      codigo: codIdiomas.espanol,
      codNombre: idiomas.espanol,
    },
    {
      codigo: codIdiomas.ingles,
      codNombre: idiomas.ingles,
    },
    {
      codigo: codIdiomas.aleman,
      codNombre: idiomas.aleman,
    },
    {
      codigo: codIdiomas.frances,
      codNombre: idiomas.frances,
    },
    {
      codigo: codIdiomas.italiano,
      codNombre: idiomas.italiano,
    },
    {
      codigo: codIdiomas.portugues,
      codNombre: idiomas.portugues,
    },
  ];
  constructor() {}
  detectarIdioma(codNombreIdioma) {
    switch (codNombreIdioma) {
      case idiomas.espanol:
        return sw.es;
      case idiomas.ingles:
        return sw.en;
      case idiomas.aleman:
        return sw.de;
      case idiomas.frances:
        return sw.fr;
      case idiomas.italiano:
        return sw.it;
      case idiomas.portugues:
        return sw.pt;
      default:
        return sw.en;
    }
  }
}
