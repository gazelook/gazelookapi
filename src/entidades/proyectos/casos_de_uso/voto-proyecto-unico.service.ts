import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';

const sw = require('stopword');
import * as mongoose from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionesVotoProyecto } from 'src/drivers/mongoose/interfaces/traduccion_voto_proyecto/traduccion-voto-proyecto.interface';
import { VotoProyecto } from 'src/drivers/mongoose/interfaces/voto_proyecto/voto-proyecto.interface';
import { VotoProyectoDto } from '../entidad/voto-proyecto-dto';
import { ConfiguracionEventoService } from 'src/entidades/catalogos/casos_de_uso/configuracion-evento.service';
import { FormulaEventoService } from 'src/entidades/catalogos/casos_de_uso/formula_evento.service';
import {
  codigosRolSistema,
  codigosConfEvento,
  codigosCatTipoVoto,
  nombreAcciones,
  nombreEntidades,
  nombrecatalogoEstados,
} from 'src/shared/enum-sistema';
import { RolUsuarioService } from 'src/entidades/usuario/casos_de_uso/rol-usuario.service';
import { ModificarValorEstimadoProyectoService } from './modificar-valor-estimado-proyecto.service';
import * as moment from 'moment';

@Injectable()
export class VotoProyectoService {
  noticia: any;
  TOTAL_VOTOS: any;

  constructor(
    @Inject('VOTO_PROYECTO_MODEL')
    private readonly votoProyectoModel: Model<VotoProyecto>,
    @Inject('TRADUCCION_VOTO_PROYECTO_MODEL')
    private readonly traduccionVotoProyectoModel: Model<
      TraduccionesVotoProyecto
    >,
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    //@Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private configuracionEventoService: ConfiguracionEventoService,
    private formulaEventoService: FormulaEventoService,
    @Inject(forwardRef(() => RolUsuarioService))
    private readonly rolUsuarioService: RolUsuarioService,
  ) {}

  async votoProyectoUnico(
    votoProyecto: VotoProyectoDto,
    usuario: string,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codIdiom,
    );
    let codIdioma = idioma.codigo;

    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene la entidad voto proyecto
    const entidadVotoProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.votoProyecto,
    );
    let codEntidadVotoProyecto = entidadVotoProyecto.codigo;

    //Obtiene el estado activa del voto proyecto
    const estadoVotoProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadVotoProyecto,
    );
    let codEstadoVotoProyecto = estadoVotoProyecto.codigo;

    //Obtiene la entidad traduccion Voto Proyecto
    const entidadTraduccionVotoproyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionVotoProyecto,
    );
    let codEntidadTraduccionVotoProyecto = entidadTraduccionVotoproyecto.codigo;

    //Obtiene el estado de la traduccion voto proyecto
    const estadoTraduccionVotoProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTraduccionVotoProyecto,
    );
    let codEstadoTraduccionVotoProyecto = estadoTraduccionVotoProyecto.codigo;

    //Obtiene la entidad proyecto
    const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.proyectos,
    );
    let codEntidadN = entidadN.codigo;

    //Obtiene el estado activa de la entidad proyecto
    const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadN,
    );
    let codEstadoProyecto = estadoN.codigo;

    //Obtiene el estado que de foro del proyecto
    const estadoForoProy = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.foro,
      codEntidadN,
    );
    let codEstadoForoProyecto = estadoForoProy.codigo;

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const getUserRol = await this.rolUsuarioService.ObtenerRolUsuarioByidUserByCodRol(
        usuario,
        codigosRolSistema.cod_rol_sis_gazelook,
      );

      let tipoVoto = '';
      let totalVotos: any;
      let votosProyecto: number;
      let userAdmin: any;
      let proyectoEstadoForo = false;
      if (getUserRol) {
        userAdmin = true;
        tipoVoto = codigosCatTipoVoto.cod_cat_tipo_voto_normal_admin; //"CTVOTO_3"
      } else {
        userAdmin = false;
        tipoVoto = codigosCatTipoVoto.cod_cat_tipo_voto_normal; //"CTVOTO_1"
      }

      const proyecto = await this.proyectoModel
        .findOne({ _id: votoProyecto.proyecto._id })
        .populate({ path: 'votos', select: 'perfil tipo' });

      if (proyecto) {
        if (proyecto.estado === codEstadoForoProyecto) {
          proyectoEstadoForo = true;
          if (getUserRol) {
            userAdmin = true;
            tipoVoto = codigosCatTipoVoto.cod_cat_tipo_voto_foro_admin; //"CTVOTO_4"
          } else {
            userAdmin = false;
            tipoVoto = codigosCatTipoVoto.cod_cat_tipo_voto_foro; //"CTVOTO_2"
          }
        }
        if (proyecto.votos.length > 0) {
          let find: any;
          if (proyectoEstadoForo) {
            if (userAdmin) {
              find = proyecto.votos.filter(
                (element: any) =>
                  element.perfil == usuario &&
                  element.tipo ==
                    codigosCatTipoVoto.cod_cat_tipo_voto_foro_admin,
              );
            } else {
              find = proyecto.votos.filter(
                (element: any) =>
                  element.perfil == usuario &&
                  element.tipo == codigosCatTipoVoto.cod_cat_tipo_voto_foro,
              );
            }
          } else {
            if (userAdmin) {
              find = proyecto.votos.filter(
                (element: any) =>
                  element.perfil == usuario &&
                  element.tipo ==
                    codigosCatTipoVoto.cod_cat_tipo_voto_normal_admin,
              );
            } else {
              find = proyecto.votos.filter(
                (element: any) =>
                  element.perfil == usuario &&
                  element.tipo == codigosCatTipoVoto.cod_cat_tipo_voto_normal,
              );
            }
          }

          console.log('find.length: ', find.length);
          if (find.length >= 1) {
            console.log('ya no puede votar ya ha votado');
            return null;
          } else {
            if (proyectoEstadoForo) {
              // let localTime = moment().format("YYYY-MM-DD"); // store localTime
              // let fechaFormat = localTime + "T00:00:00.000Z";
              // let formatFechaActual = new Date(fechaFormat);
              // formatFechaActual.setDate(new Date(fechaFormat).getDate());
              // const getFechaMaxima= await this.modificarValorEstimadoProyectoService.modificarValorEstimadoProyecto()
              // if(formatFechaActual > getFechaMaxima.fecha){
              //     console.log('ya no puede votar ya paso los primeros 15 dias de foro')
              //     return null
              // }else{
              if (userAdmin) {
                tipoVoto = codigosCatTipoVoto.cod_cat_tipo_voto_foro_admin;
              } else {
                tipoVoto = codigosCatTipoVoto.cod_cat_tipo_voto_foro;
              }
              console.log('aun puede votar');
              // }
            }
          }
        }

        const idVotoProyecto = new mongoose.mongo.ObjectId();

        let dataTraduccion;
        let traducciones = [];
        //verifica si existe descipcion del voto para guardar la traduccion
        console.log(
          'votoProyecto.traducciones.length: ',
          votoProyecto.traducciones.length,
        );
        if (votoProyecto.traducciones.length > 0) {
          //objeto de la traduccion voto proyecto
          let traduccionVotoProyecto: any = {
            // descripcion: votoProyecto.traducciones[0].descripcion,
            idioma: codIdioma,
            estado: codEstadoTraduccionVotoProyecto,
            original: true,
            votoProyecto: idVotoProyecto,
          };

          traduccionVotoProyecto.descripcion =
            votoProyecto.traducciones[0].descripcion;

          //guarda la traduccion del voto proyecto
          const crearTraduccionVotoProyecto = await new this.traduccionVotoProyectoModel(
            traduccionVotoProyecto,
          ).save(opts);

          dataTraduccion = JSON.parse(
            JSON.stringify(crearTraduccionVotoProyecto),
          );
          delete dataTraduccion.__v;

          //objeto del historico traduccion voto proyecto
          let historicoTraduccionVotoProyecto: any = {
            datos: dataTraduccion,
            usuario: proyecto.perfil,
            accion: getAccion,
            entidad: codEntidadTraduccionVotoProyecto,
          };
          this.crearHistoricoService.crearHistoricoServer(
            historicoTraduccionVotoProyecto,
          );

          traducciones.push(dataTraduccion._id);
        }

        totalVotos = proyecto.totalVotos;

        if (userAdmin) {
          //Obtengo la configuracion del evento
          const getConfEvento = await this.configuracionEventoService.obtenerConfiguracionEvento(
            codigosConfEvento.cod_configuracion_evento_proyecto,
          );
          let getFormulaEvento;
          for (let i = 0; i < getConfEvento.formulas.length; i++) {
            const getCodFormula = getConfEvento.formulas[i];
            //Obtengo la formula del evento
            getFormulaEvento = await this.formulaEventoService.obtenerFormulaEvento(
              getCodFormula,
            );

            //Compruebo si la formula del evento no viene vacia
            if (getFormulaEvento) {
              break;
            }
          }

          this.TOTAL_VOTOS = totalVotos;
          let formula;
          //Remmplazo el total de votos
          formula = getFormulaEvento.replace('TOTAL_VOTOS', this.TOTAL_VOTOS);

          formula = formula.replace('TOTAL_VOTOS', this.TOTAL_VOTOS);

          //Redondeo del valor del resultado de la formula
          votosProyecto = Math.round(eval(formula));
          //Si el voto de gazelook suma 0 se le asigna 1 voto automaticamente

          if (votosProyecto === totalVotos) {
            totalVotos = totalVotos + 1;
          } else {
            totalVotos = votosProyecto;
          }
        } else {
          votosProyecto = 1;
          totalVotos = votosProyecto + totalVotos;
        }

        //actualiza los votos del proyecto
        const updateProyecto = await this.proyectoModel.updateOne(
          { _id: votoProyecto.proyecto._id },
          { $set: { totalVotos: totalVotos } },
          opts,
        );

        let newHistoricoProyecto: any = {
          datos: totalVotos,
          usuario: usuario,
          accion: getAccion,
          entidad: codEntidadN,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistoricoProyecto);

        //objeto del voto proyecto
        let nuevoVotoProyecto = {
          _id: idVotoProyecto,
          estado: codEstadoVotoProyecto,
          perfil: usuario,
          proyecto: votoProyecto.proyecto._id,
          traducciones: traducciones,
          tipo: tipoVoto,
          numeroVoto: votosProyecto,
          configuracion: codigosConfEvento.cod_configuracion_evento_proyecto,
        };

        const crearVotoProyecto = await new this.votoProyectoModel(
          nuevoVotoProyecto,
        ).save(opts);

        const dataVotoProyecto = JSON.parse(JSON.stringify(crearVotoProyecto));
        delete dataVotoProyecto.__v;

        let historicoVotoProyecto: any = {
          datos: dataVotoProyecto,
          usuario: usuario,
          accion: getAccion,
          entidad: codEntidadVotoProyecto,
        };
        this.crearHistoricoService.crearHistoricoServer(historicoVotoProyecto);

        await this.proyectoModel.updateOne(
          { _id: votoProyecto.proyecto._id },
          { $push: { votos: dataVotoProyecto._id } },
          opts,
        );

        let historicoUpdateProyecto: any = {
          datos: dataVotoProyecto._id,
          usuario: usuario,
          accion: getAccion,
          entidad: codEntidadN,
        };
        this.crearHistoricoService.crearHistoricoServer(
          historicoUpdateProyecto,
        );
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return crearVotoProyecto;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
