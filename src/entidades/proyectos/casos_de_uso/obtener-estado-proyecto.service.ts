import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';

@Injectable()
export class ObtenerEstadoProyectoService {
  constructor(
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
  ) {}

  async obtenerEstadoProyecto(idProyecto: any): Promise<any> {
    try {
      //Obtiene una localidad segun el codigo enviado
      const estadoProyecto = await this.proyectoModel.findOne({
        _id: idProyecto,
      });
      let getEstadoProyecto = estadoProyecto.estado;

      return getEstadoProyecto;
    } catch (error) {
      throw error;
    }
  }
}
