import { Inject, Injectable } from '@nestjs/common';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { Model, PaginateModel } from 'mongoose';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';

import { ObtenerLocalidadProyectoService } from './obtener-localidad-proyecto.service';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { ObtenerMediasProyectoService } from './obtener-medias-proyecto.service';
import { Evento } from 'src/drivers/mongoose/interfaces/evento/evento.interface';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import {
  estadosProyecto,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
const mongoose = require('mongoose');

@Injectable()
export class ObtenerProyectosEjecutadosFinalizadosService {
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirProyectoService: TraducirProyectoService,
    private obtenerLocalidadProyectoService: ObtenerLocalidadProyectoService,
    private traduccionMediaService: TraduccionMediaService,
  ) {}

  async obtenerProyectosFinalizados(
    idPerfil: string,
    tipo: string,
    fechaInicial: any,
    fechaFinal: any,
    limite: number,
    pagina: number,
    codIdioma: string,
    response: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const headerNombre = new HadersInterfaceNombres();

    //Obtiene el codigo del idioma
    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codIdioma,
    );
    let codIdi = idioma.codigo;

    //Obtiene la entidad traduccion Proyecto
    const entidadTraduccionProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionProyecto,
    );
    let codEntidadTradProyecto = entidadTraduccionProyecto.codigo;

    //Obtiene el estado activa de la entidad traduccion proyecto
    const estadoTradProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradProyecto,
    );
    let codEstadoTradProyecto = estadoTradProyecto.codigo;

    //Obtiene el codigo de la entidad media
    const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    let codEntidadMedia = entidadmedia.codigo;

    //Obtiene el estado activo de la entidad proyectos
    const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadMedia,
    );
    let codEstadoMedia = estadoMedia.codigo;

    //Obtiene l codigo de la entidad album
    const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.album,
    );
    let codEntidadAlbum = entidadAlbum.codigo;

    //Obtiene el estado activo de la entidad album
    const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadAlbum,
    );
    let codEstadoAlbum = estadoAlbum.codigo;

    //Obtiene el codigo de la entidad traduccion media
    const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    let codEntidadTradMedia = entidadTradMedia.codigo;

    //Obtiene el estado activo de la entidad traduccion media
    const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradMedia,
    );
    let codEstadoTradMedia = estadoTradMedia.codigo;

    //Obtiene la entidad traduccionProyecto
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionProyecto,
    );
    let codEntidadTraduccion = entidad.codigo;

    //Obtiene el estado activa de la entidad traduccion proyecto
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTraduccion,
    );
    let codEstadoTProyecto = estado.codigo;

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const populateTraduccion = {
        path: 'traducciones',
        select:
          '-fechaActualizacion -fechaCreacion -idioma -__v -descripcion -tags -original',
        match: {
          idioma: codIdioma,
          estado: codEstadoTProyecto,
        },
      };
      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto',
      };

      const populateEsrategia = {
        path: 'estrategia',
        select: 'adjuntos estado presupuesto fechaCaducidad',
        populate: {
          path: 'adjuntos', //Media
          select:
            'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
          match: { estado: codEstadoMedia },
          populate: [
            {
              path: 'traducciones',
              select: 'descripcion',
              match: { idioma: codIdi, estado: codEstadoTradMedia },
            },
            {
              path: 'principal',
              select: 'url tipo duracion fileDefault fechaActualizacion',
            },
            {
              path: 'miniatura',
              select: 'url tipo duracion fileDefault fechaActualizacion',
            },
          ],
        },
      };

      if (!fechaInicial || !fechaFinal) {
        fechaInicial = null;
        fechaFinal = null;
      }

      let getProyectos: any;
      const options = {
        select: ' -__v ',
        sort: '-fechaActualizacion',
        populate: [
          populateTraduccion,
          adjuntos,
          populatePerfil,
          populateEsrategia,
        ],
        page: Number(pagina),
        limit: Number(limite),
      };

      if (!fechaInicial && !fechaFinal) {
        //Obtengo el evento
        getProyectos = await this.proyectoModel.paginate(
          {
            $and: [
              { tipo: tipo },
              { estado: estadosProyecto.proyectoFinalizado },
            ],
          },
          options,
        );
      } else {
        const ini = new Date(fechaInicial);
        const fecFin = fechaFinal + 'T23:59:59.999Z';
        const fin = new Date(fecFin);
        getProyectos = await this.proyectoModel.paginate(
          {
            $and: [
              { tipo: tipo },
              { estado: estadosProyecto.proyectoFinalizado },
              { fechaActualizacion: { $gt: ini } },
              { fechaActualizacion: { $lt: fin } },
            ],
          },
          options,
        );
      }

      let dataProyectos = [];
      if (getProyectos.docs.length > 0) {
        let adjun = [];
        let objAdjun: any;
        let getAdjuntos: any;
        let dataEstrategia: any;

        for (const proyecto of getProyectos.docs) {
          if (proyecto.estrategia) {
            let adjuntosEstrategia = [];
            if (proyecto.estrategia['adjuntos'].length > 0) {
              for (const getMediaEstrategia of proyecto.estrategia[
                'adjuntos'
              ]) {
                let obMiniatura: any;

                //Verifica si la media tiene miniatura
                if (getMediaEstrategia.miniatura) {
                  //Objeto de tipo miniatura (archivo)
                  obMiniatura = {
                    _id: getMediaEstrategia.miniatura._id,
                    url: getMediaEstrategia.miniatura.url,
                    tipo: {
                      codigo: getMediaEstrategia.miniatura.tipo,
                    },
                    fileDefault: getMediaEstrategia.miniatura.fileDefault,
                    fechaActualizacion:
                      getMediaEstrategia.miniatura.fechaActualizacion,
                  };
                  if (getMediaEstrategia.miniatura.duracion) {
                    obMiniatura.duracion =
                      getMediaEstrategia.miniatura.duracion;
                  }
                }
                //Objeto de tipo media
                let objMedia: any = {
                  _id: getMediaEstrategia._id,
                  catalogoMedia: {
                    codigo: getMediaEstrategia.catalogoMedia,
                  },
                  principal: {
                    _id: getMediaEstrategia.principal._id,
                    url: getMediaEstrategia.principal.url,
                    tipo: {
                      codigo: getMediaEstrategia.principal.tipo,
                    },
                    fileDefault: getMediaEstrategia.principal.fileDefault,
                    fechaCreacion: getMediaEstrategia.principal.fechaCreacion,
                    fechaActualizacion:
                      getMediaEstrategia.principal.fechaActualizacion,
                  },
                  miniatura: obMiniatura,
                  fechaCreacion: getMediaEstrategia.fechaCreacion,
                  fechaActualizacion: getMediaEstrategia.fechaActualizacion,
                };
                if (getMediaEstrategia.principal.duracion) {
                  objMedia.principal.duracion =
                    getMediaEstrategia.principal.duracion;
                }
                //Verifica si existe la traduccion de la media

                if (getMediaEstrategia.traducciones[0] === undefined) {
                  const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                    getMediaEstrategia._id,
                    codIdioma,
                    opts,
                  );
                  if (traduccionMedia) {
                    let arrayTrad = [];
                    const traducciones = {
                      _id: traduccionMedia._id,
                      descripcion: traduccionMedia.descripcion,
                    };
                    arrayTrad.push(traducciones);
                    objMedia.traducciones = arrayTrad;
                  } else {
                    objMedia.traducciones = [];
                  }
                } else {
                  let arrayTraducciones = [];
                  arrayTraducciones.push(getMediaEstrategia.traducciones[0]);
                  objMedia.traducciones = arrayTraducciones;
                }

                adjuntosEstrategia.push(objMedia);
              }
            }
            dataEstrategia = {
              presupuesto: proyecto.estrategia['presupuesto'],
              fechaCaducidad: proyecto.estrategia['fechaCaducidad'],
              adjuntos: adjuntosEstrategia,
              estado: {
                codigo: proyecto.estrategia['estado'],
              },
            };
          }

          //Verifica si el proyecto tiene adjuntos
          if (proyecto.adjuntos.length > 0) {
            for (const album of proyecto.adjuntos) {
              let media = [];
              let objPortada: any;
              //Verifica si el album tiene portada
              if (album['portada']) {
                //Verifica si la media tiene miniatura
                let objMiniatura: any;
                if (album['portada'].miniatura) {
                  //Objeto de tipo miniatura (archivo)
                  objMiniatura = {
                    _id: album['portada'].miniatura._id,
                    url: album['portada'].miniatura.url,
                    tipo: {
                      codigo: album['portada'].miniatura.tipo,
                    },
                    fileDefault: album['portada'].miniatura.fileDefault,
                    fechaActualizacion:
                      album['portada'].miniatura.fechaActualizacion,
                  };
                  if (album['portada'].miniatura.duracion) {
                    objMiniatura.duracion = album['portada'].miniatura.duracion;
                  }
                }
                //Objeto de tipo portada(media)
                objPortada = {
                  _id: album['portada']._id,
                  catalogoMedia: {
                    codigo: album['portada'].catalogoMedia,
                  },
                  principal: {
                    _id: album['portada'].principal._id,
                    url: album['portada'].principal.url,
                    tipo: {
                      codigo: album['portada'].principal.tipo,
                    },
                    fileDefault: album['portada'].principal.fileDefault,
                    fechaCreacion: album['portada'].principal.fechaCreacion,
                    fechaActualizacion:
                      album['portada'].principal.fechaActualizacion,
                  },
                  miniatura: objMiniatura,
                  fechaCreacion: album['portada'].fechaCreacion,
                  fechaActualizacion: album['portada'].fechaActualizacion,
                };
                if (album['portada'].principal.duracion) {
                  objPortada.principal.duracion =
                    album['portada'].principal.duracion;
                }

                if (album['portada'].traducciones.length === 0) {
                  const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                    album['portada']._id,
                    codIdioma,
                    opts,
                  );
                  if (traduccionMedia) {
                    let arrayTrad = [];
                    const traducciones = {
                      _id: traduccionMedia._id,
                      descripcion: traduccionMedia.descripcion,
                    };
                    arrayTrad.push(traducciones);
                    objPortada.traducciones = arrayTrad;
                  } else {
                    objPortada.traducciones = [];
                  }
                } else {
                  let arrayTraduccionesPort = [];
                  arrayTraduccionesPort.push(album['portada'].traducciones[0]);
                  objPortada.traducciones = arrayTraduccionesPort;
                }
              }

              //Verifica si el album tiene medias
              if (album['media'].length > 0) {
                for (const getMedia of album['media']) {
                  let obMiniatura: any;

                  //Verifica si la media tiene miniatura
                  if (getMedia.miniatura) {
                    //Objeto de tipo miniatura (archivo)
                    obMiniatura = {
                      _id: getMedia.miniatura._id,
                      url: getMedia.miniatura.url,
                      tipo: {
                        codigo: getMedia.miniatura.tipo,
                      },
                      fileDefault: getMedia.miniatura.fileDefault,
                      fechaActualizacion: getMedia.miniatura.fechaActualizacion,
                    };
                    if (getMedia.miniatura.duracion) {
                      obMiniatura.duracion = getMedia.miniatura.duracion;
                    }
                  }
                  //Objeto de tipo media
                  let objMedia: any = {
                    _id: getMedia._id,
                    catalogoMedia: {
                      codigo: getMedia.catalogoMedia,
                    },
                    principal: {
                      _id: getMedia.principal._id,
                      url: getMedia.principal.url,
                      tipo: {
                        codigo: getMedia.principal.tipo,
                      },
                      fileDefault: getMedia.principal.fileDefault,
                      fechaCreacion: getMedia.principal.fechaCreacion,
                      fechaActualizacion: getMedia.principal.fechaActualizacion,
                    },
                    miniatura: obMiniatura,
                    fechaCreacion: getMedia.fechaCreacion,
                    fechaActualizacion: getMedia.fechaActualizacion,
                  };
                  if (getMedia.principal.duracion) {
                    objMedia.principal.duracion = getMedia.principal.duracion;
                  }
                  //Verifica si existe la traduccion de la media

                  if (getMedia.traducciones[0] === undefined) {
                    const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                      getMedia._id,
                      codIdioma,
                      opts,
                    );
                    if (traduccionMedia) {
                      let arrayTrad = [];
                      const traducciones = {
                        _id: traduccionMedia._id,
                        descripcion: traduccionMedia.descripcion,
                      };
                      arrayTrad.push(traducciones);
                      objMedia.traducciones = arrayTrad;
                    } else {
                      objMedia.traducciones = [];
                    }
                  } else {
                    let arrayTraducciones = [];
                    arrayTraducciones.push(getMedia.traducciones[0]);
                    objMedia.traducciones = arrayTraducciones;
                  }

                  media.push(objMedia);
                }
              }

              //Objeto de tipo adjunto (album)
              objAdjun = {
                _id: album['_id'],
                tipo: {
                  codigo: album['tipo'],
                },
                traducciones: album['traducciones'],
                predeterminado: album['predeterminado'],
                portada: objPortada,
                media: media,
              };
              adjun.push(objAdjun);
            }
          }
          getAdjuntos = adjun;

          //Data de traducciones del proyecto
          let traduccionProyecto = [];
          //Verifica si tiene la traduccion
          if (proyecto.traducciones.length === 0) {
            const idPerfil = proyecto.perfil;
            traduccionProyecto = await this.traducirProyectoService.TraducirProyecto(
              proyecto._id,
              idPerfil,
              codIdioma,
              opts,
            );
          } else {
            //Objeto de tipo traducccion proyecto
            let trad = {
              tituloCorto: proyecto.traducciones[0]['tituloCorto'],
              titulo: proyecto.traducciones[0]['titulo'],
              descripcion: proyecto.traducciones[0]['descripcion'],
            };
            traduccionProyecto.push(trad);
          }

          //Objeto de proyecto
          let datos = {
            _id: proyecto._id,
            traducciones: traduccionProyecto,
            adjuntos: getAdjuntos,
            perfil: proyecto.perfil,
            valorEstimado: proyecto.valorEstimado,
            tipo: {
              codigo: proyecto.tipo,
            },
            estrategia: dataEstrategia,
            moneda: {
              codNombre: proyecto.moneda,
            },
            estado: {
              codigo: proyecto.estado,
            },
            fechaCreacion: proyecto.fechaCreacion,
            fechaActualizacion: proyecto.fechaActualizacion,
          };
          dataProyectos.push(datos);
        }

        response.set(headerNombre.totalDatos, getProyectos.totalDocs);
        response.set(headerNombre.totalPaginas, getProyectos.totalPages);
        response.set(headerNombre.proximaPagina, getProyectos.hasNextPage);
        response.set(headerNombre.anteriorPagina, getProyectos.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataProyectos;
      }
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return dataProyectos;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
