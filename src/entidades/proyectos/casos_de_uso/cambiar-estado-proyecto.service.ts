import { Inject, Injectable } from '@nestjs/common';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { Model } from 'mongoose';
const sw = require('stopword');
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

@Injectable()
export class CambiarEstadoProyectoService {
  noticia: any;

  constructor(
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async cambiarEstadoProyectos(
    estadoProyecto: string,
    opts: any,
  ): Promise<any> {
    const accionAc = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    let getAccionModificar = accionAc.codigo;

    //Obtiene la entidad proyecto
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.proyectos,
    );
    let codEntidadProyecto = entidad.codigo;

    //Obtiene el estado que se envia como parametro del proyecto
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      estadoProyecto,
      codEntidadProyecto,
    );
    let codEstadoProyecto = estado.codigo;

    //Obtiene el estado activa del proyecto
    const estadoActproyecto = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadProyecto,
    );
    let codEstadoProyectoActiva = estadoActproyecto.codigo;

    try {
      //Acatualiza todos los proyectos que estan en estado activo a tipo foro
      await this.proyectoModel.updateMany(
        { estado: codEstadoProyectoActiva },
        { estado: codEstadoProyecto },
        opts,
      );

      //Data para guardar en el historico
      let dataHisProy = {
        descripcion: 'Cambio de estado a '
          .concat(estadoProyecto)
          .concat(' a todos los proyectos que estaban con estado activa'),
        estadoAnterior: codEstadoProyectoActiva,
        estadoActual: codEstadoProyecto,
      };
      let newHistoricoProyectoUpdate: any = {
        datos: dataHisProy,
        usuario: '',
        accion: getAccionModificar,
        entidad: codEntidadProyecto,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoProyectoUpdate,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
