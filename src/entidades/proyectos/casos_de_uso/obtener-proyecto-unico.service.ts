import { Inject, Injectable } from '@nestjs/common';

import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  codidosEstadosTraduccionDireccion,
  estadosProyecto,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ObtenerPortadaPredeterminadaProyectoService } from './obtener-portada-predeterminada-proyecto.service';
import { CatalogoTipoProyectoByCodigoService } from './obtener-tipo-proyecto-by-codigo.service';
import { ObtenerLocalidadProyectoService } from './obtener-localidad-proyecto.service';
import { CatalogoColoresService } from 'src/entidades/catalogos/casos_de_uso/catalogo-colores.service';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { CatalogoConfiguracionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-configuracion.service';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { ObtenerMediasProyectoService } from './obtener-medias-proyecto.service';
import { CalcularDiasProyectoService } from './carcular-dias-proyecto.service';
import { ObtenerBeneficiarioTransaccionService } from 'src/entidades/transaccion/casos_de_uso/obtener-beneficiario-transaccion.service';
import { ObtenerProyetoIdService } from './obtener-proyecto-id.service';
import { TraducirDireccionService } from 'src/entidades/direccion/casos_de_uso/traducir-direccion.service';
import { ObtenerDireccionService } from 'src/entidades/direccion/casos_de_uso/obtener-direccion.service';
import { ObtenerAlbumPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil.service';
const mongoose = require('mongoose');

@Injectable()
export class ObtenerProyectoUnicoService {
  proyecto: any;

  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private traducirProyectoService: TraducirProyectoService,
    private obtenerPortadaPredeterminadaProyectoService: ObtenerPortadaPredeterminadaProyectoService,
    private catalogoTipoProyectoByCodigoService: CatalogoTipoProyectoByCodigoService,
    private obtenerLocalidadProyectoService: ObtenerLocalidadProyectoService,
    private catalogoColoresService: CatalogoColoresService,
    private catalogoEstilosService: CatalogoEstilosService,
    private catalogoConfiguracionService: CatalogoConfiguracionService,
    private traduccionMediaService: TraduccionMediaService,
    private obtenerMediasProyectoService: ObtenerMediasProyectoService,
    private calcularDiasProyectoService: CalcularDiasProyectoService,
    private obtenerBeneficiarioTransaccionService: ObtenerBeneficiarioTransaccionService,
    private obtenerProyetoIdService: ObtenerProyetoIdService,
    private traducirDireccionService: TraducirDireccionService,
    private obtenerDireccionService: ObtenerDireccionService,
    private obtenerAlbumPerfilService: ObtenerAlbumPerfilService,
  ) { }

  async obtenerProyectoUnico(
    idProyecto: string,
    idPerfil: string,
    codIdioma: string,
    estado?: string,
    original?: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    //verificar original
    const prOriginal: any =
      original === 'true'
        ? await this.obtenerProyetoIdService.obtenerProyectoOriginal(
          idProyecto,
          true,
        )
        : null;

    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codIdioma,
    );

    //Obtiene el codigo del idioma
    let codIdi = idioma.codigo;
    console.log('codIdi--------', codIdi);

    //Obtiene el codigo de la accion crear
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene la entidad traduccion Proyecto
    const entidadTraduccionProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionProyecto,
    );
    let codEntidadTradProyecto = entidadTraduccionProyecto.codigo;

    //Obtiene el estado activa de la entidad traduccion proyecto
    const estadoTradProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradProyecto,
    );
    let codEstadoTradProyecto = estadoTradProyecto.codigo;

    //Obtiene l codigo de la entidad proyectos
    const entidadProyectos = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.proyectos,
    );
    let codEntidadProyectos = entidadProyectos.codigo;

    let codEstadoProyectos: any;
    let proyectoEsperaFondos = false;
    if (!estado) {
      const proyect = await this.proyectoModel.findOne({
        _id: idProyecto,
        estado: { $ne: estadosProyecto.proyectoEliminado }
      });
      if(proyect){
        codEstadoProyectos = proyect.estado
        if (codEstadoProyectos === estadosProyecto.proyectoEnEsperaFondos) {
          proyectoEsperaFondos = true;
        }
      }

    } else {
      codEstadoProyectos = estado;
      if (estado === estadosProyecto.proyectoEnEsperaFondos) {
        proyectoEsperaFondos = true;
      }
    }

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };
      let perfilPropietario = false;
      let getProyecto = await this.proyectoModel.findOne({
        _id: idProyecto,
        estado: { $ne: estadosProyecto.proyectoEliminado },
        perfil: idPerfil,
      });
      if (getProyecto) {
        perfilPropietario = true;
      }
      console.log('perfilPropietario: ', perfilPropietario);
      const proyecto = await this.verificarTraduccion(
        idProyecto,
        codIdioma,
        codIdi,
        codEstadoTradProyecto,
        codEstadoProyectos,
        opts,
        session,
        perfilPropietario,
        false,
      );
      console.log('proyecto---------------', proyecto);

      if (proyecto) {
        let port: any;
        let votoBo: any;
        let adjun = [];
        let objAdjun: any;
        let getAdjuntos: any;
        let actu = false;
        if (
          proyecto.fechaCreacion.toString() ==
          proyecto.fechaActualizacion.toString()
        ) {
          actu = false;
        } else {
          let dias = this.calcularDiasProyectoService.calcularDias(
            new Date(),
            proyecto.fechaActualizacion,
          );
          if (dias) actu = true;
        }

        let getMontoFaltanteProyecto: any;
        if (proyectoEsperaFondos) {
          
          getMontoFaltanteProyecto = await this.obtenerBeneficiarioTransaccionService.obtenerMontoFaltanteEntregarProyecto(
            proyecto._id,
            proyecto.valorEstimado,
          );
        }

        //Verifica si el proyecto tiene adjuntos

        if (proyecto.adjuntos.length > 0) {
          for (const album of proyecto.adjuntos) {
            let media = [];
            let objPortada: any;
            //Verifica si el album tiene portada
            if (album.portada) {
              console.log('album********: ', album._id);
              console.log('album.portada********: ', album.portada);
              //Verifica si la media tiene miniatura
              let objMiniatura: any;
              if (album.portada.miniatura) {
                //Objeto de tipo miniatura (archivo)
                objMiniatura = {
                  _id: album.portada.miniatura._id,
                  url: album.portada.miniatura.url,
                  tipo: {
                    codigo: album.portada.miniatura.tipo,
                  },
                  fileDefault: album.portada.miniatura.fileDefault,
                  fechaActualizacion:
                    album.portada.miniatura.fechaActualizacion,
                };
                if (album.portada.miniatura.duracion) {
                  objMiniatura.duracion = album.portada.miniatura.duracion;
                }
              }
              //Objeto de tipo portada(media)
              objPortada = {
                _id: album.portada._id,
                catalogoMedia: {
                  codigo: album.portada.catalogoMedia,
                },
                principal: {
                  _id: album.portada.principal._id,
                  url: album.portada.principal.url,
                  tipo: {
                    codigo: album.portada.principal.tipo,
                  },
                  fileDefault: album.portada.principal.fileDefault,
                  fechaCreacion: album.portada.principal.fechaCreacion,
                  fechaActualizacion:
                    album.portada.principal.fechaActualizacion,
                },
                miniatura: objMiniatura,
                fechaCreacion: album.portada.fechaCreacion,
                fechaActualizacion: album.portada.fechaActualizacion,
              };
              if (album.portada.principal.duracion) {
                objPortada.principal.duracion =
                  album.portada.principal.duracion;
              }

              if (album.portada.traducciones.length === 0) {
                const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                  album.portada._id,
                  codIdioma,
                  opts,
                );
                if (traduccionMedia) {
                  let arrayTrad = [];
                  const traducciones = {
                    _id: traduccionMedia._id,
                    descripcion: traduccionMedia.descripcion,
                  };
                  arrayTrad.push(traducciones);
                  objPortada.traducciones = arrayTrad;
                } else {
                  objPortada.traducciones = [];
                }
              } else {
                let arrayTraduccionesPort = [];
                arrayTraduccionesPort.push(album.portada.traducciones[0]);
                objPortada.traducciones = arrayTraduccionesPort;
              }
            }

            //Verifica si el album tiene medias
            if (album.media.length > 0) {
              for (const getMedia of album.media) {
                let obMiniatura: any;

                //Verifica si la media tiene miniatura
                if (getMedia.miniatura) {
                  //Objeto de tipo miniatura (archivo)
                  obMiniatura = {
                    _id: getMedia.miniatura._id,
                    url: getMedia.miniatura.url,
                    tipo: {
                      codigo: getMedia.miniatura.tipo,
                    },
                    fileDefault: getMedia.miniatura.fileDefault,
                    fechaActualizacion: getMedia.miniatura.fechaActualizacion,
                  };
                  if (getMedia.miniatura.duracion) {
                    obMiniatura.duracion = getMedia.miniatura.duracion;
                  }
                }
                //Objeto de tipo media
                let objMedia: any = {
                  _id: getMedia._id,
                  catalogoMedia: {
                    codigo: getMedia.catalogoMedia,
                  },
                  principal: {
                    _id: getMedia.principal._id,
                    url: getMedia.principal.url,
                    tipo: {
                      codigo: getMedia.principal.tipo,
                    },
                    fileDefault: getMedia.principal.fileDefault,
                    fechaCreacion: getMedia.principal.fechaCreacion,
                    fechaActualizacion: getMedia.principal.fechaActualizacion,
                  },
                  miniatura: obMiniatura,
                  fechaCreacion: getMedia.fechaCreacion,
                  fechaActualizacion: getMedia.fechaActualizacion,
                };
                if (getMedia.principal.duracion) {
                  objMedia.principal.duracion = getMedia.principal.duracion;
                }
                //Verifica si existe la traduccion de la media

                if (getMedia.traducciones[0] === undefined) {
                  const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                    getMedia._id,
                    codIdioma,
                    opts,
                  );
                  if (traduccionMedia) {
                    let arrayTrad = [];
                    const traducciones = {
                      _id: traduccionMedia._id,
                      descripcion: traduccionMedia.descripcion,
                    };
                    arrayTrad.push(traducciones);
                    objMedia.traducciones = arrayTrad;
                  } else {
                    objMedia.traducciones = [];
                  }
                } else {
                  let arrayTraducciones = [];
                  arrayTraducciones.push(getMedia.traducciones[0]);
                  objMedia.traducciones = arrayTraducciones;
                }

                media.push(objMedia);
              }
            }

            //Objeto de tipo adjunto (album)
            objAdjun = {
              _id: album._id,
              tipo: {
                codigo: album.tipo,
              },
              traducciones: album.traducciones,
              predeterminado: album.predeterminado,
              portada: objPortada,
              media: media,
            };
            adjun.push(objAdjun);
          }
        }
        getAdjuntos = adjun;

        //Data de medias
        //Llama al metodo del obtener medias
        let getMedia: any;
        if (proyecto.medias.length > 0) {
          getMedia = await this.obtenerMediasProyectoService.obtenerMediasProyecto(
            proyecto.medias,
            codIdioma,
            opts,
          );
        } else {
          getMedia = proyecto.medias;
        }

        //Verifica si el proyecto tiene votos
        if (proyecto.votos.length > 0) {
          for (const votos of proyecto.votos) {
            //verifica si el perfil que se envia como parametro existe dentro de los votos
            //Si existe significa que ha hecho un voto al proyecto
            if (votos.perfil) {
              if (votos.perfil._id == idPerfil) {
                votoBo = true;
                break;
              } else {
                //Si no significa que aun no ha votado por el proyecto
                votoBo = false;
              }
            }
          }
        } else {
          votoBo = false;
        }

        //Data de comentarios
        let arrayComentarios = [];
        //Verifica si el proyecto tiene coemtnarios
        if (proyecto.comentarios.length > 0) {
          // ______________________get comentarios no return__________________________________
          /* for (const comentarios of proyecto.comentarios) {
            let arrayAdjComent = [];
            let objAdjunComentario: any;

            //Verifica si el comentario tiene adjuntos
            if (comentarios.adjuntos.length > 0) {

              for (const album of comentarios.adjuntos) {
                let mediaComentario: any;
                let objPortadaComent: any;
                //Verifica si el album tiene portada
                if (album.portada) {
                  //Data de medias
                  //Llama al metodo del obtener medias
                  objPortadaComent = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(album.portada, codIdioma, opts)

                }

                //Verifica si el album tiene media
                if (album.media.length > 0) {
                  //Data de medias
                  //Llama al metodo del obtener medias
                  mediaComentario = await this.obtenerMediasProyectoService.obtenerMediasProyecto(album.media, codIdioma, opts)
                } else {
                  mediaComentario = album.media
                }

                //Objeto de tipo adjuntos (album)
                objAdjunComentario = {
                  _id: album._id,
                  tipo: {
                    codigo: album.tipo
                  },
                  traducciones: album.traducciones,
                  predeterminado: album.predeterminado,
                  portada: objPortadaComent,
                  media: mediaComentario
                }
                arrayAdjComent.push(objAdjunComentario)
              }
            }

            let listaTradComent = [];
            if (comentarios.traducciones.length > 0) {
              for (const getTradComentarios of comentarios.traducciones) {
                
                let objListaComent = {
                  _id: getTradComentarios._id,
                  texto: getTradComentarios.texto,
                  idioma: {
                    codigo: getTradComentarios.idioma
                  },
                  original: getTradComentarios.original
                }
                listaTradComent.push(objListaComent);
    
              }
            }
            
            //objeto de tipo comentario
            let objComentarios = {
              _id: comentarios._id,
              adjuntos: arrayAdjComent,
              traducciones: listaTradComent,
              coautor: comentarios.coautor,
              importante: comentarios.importante,
              tipo: {
                codigo: comentarios.tipo
              }
            }
            arrayComentarios.push(objComentarios);
          } */
        }

        //Data Participantes
        let arrayParticipantes = [];
        //Verifica si el proyecto tiene coemtnarios
        if (proyecto.participantes.length > 0) {
          for (const participantes of proyecto.participantes) {
            console.log('PARTICIOANTE PROYECTO: ', participantes._id);
            if (participantes.coautor) {
              let arrayRolParticipante = [];
              if (participantes.roles.length > 0) {
                for (const rolParticipante of participantes.roles) {
                  let arrayAccParticipante = [];
                  if (rolParticipante.acciones.length > 0) {
                    for (const getAcciones of rolParticipante.acciones) {
                      let objAccionesPart = {
                        _id: getAcciones._id,
                        codigo: getAcciones.codigo,
                        nombre: getAcciones.nombre,
                      };
                      arrayAccParticipante.push(objAccionesPart);
                    }
                  }

                  let objRolesPartic = {
                    _id: rolParticipante._id,
                    nombre: rolParticipante.nombre,
                    rol: {
                      codigo: rolParticipante.rol,
                    },
                    acciones: arrayAccParticipante,
                  };
                  arrayRolParticipante.push(objRolesPartic);
                }
              }
              let arrayComentPartic = [];

              if (participantes.comentarios.length > 0) {
                for (const comentParticipante of participantes.comentarios) {
                  let ObjCoautorParticProyecto: any;
                  if (comentParticipante.coautor.coautor) {
                    ObjCoautorParticProyecto = {
                      coautor: {
                        _id: comentParticipante.coautor.coautor._id,
                        nombreContacto:
                          comentParticipante.coautor.coautor.nombreContacto,
                        nombreContactoTraducido:
                          comentParticipante.coautor.coautor
                            ?.nombreContactoTraducido || null,
                        nombre: comentParticipante.coautor.coautor.nombre,
                      },
                    };
                  }

                  //Verifica si el comentario del paarticipante proyecto tiene adjuntos
                  let arrayAdjComentPartic: any;

                  if (comentParticipante.adjuntos.length > 0) {
                    // llamamos al servicio para obtener medias
                    arrayAdjComentPartic = await this.obtenerMediasProyectoService.obtenerMediasProyecto(
                      comentParticipante.adjuntos,
                      codIdioma,
                      opts,
                    );
                  }

                  let objComentPartic: any = {
                    _id: comentParticipante._id,
                    // traducciones: arrayTraducComentarioParti,
                    coautor: ObjCoautorParticProyecto,
                    importante: comentParticipante.importante,
                    tipo: {
                      codigo: comentParticipante.tipo,
                    },
                    adjuntos: arrayAdjComentPartic,
                  };
                  let arrayTraducComentarioParti = [];
                  //Verifica si el comentario del participante tiene traducciones
                  if (comentParticipante.traducciones.length > 0) {
                    let objTradComentPartic = {
                      _id: comentParticipante.traducciones[0]._id,
                      texto: comentParticipante.traducciones[0].texto,
                      idioma: {
                        codigo: comentParticipante.traducciones[0].idioma,
                      },
                      original: comentParticipante.traducciones[0].original,
                    };
                    arrayTraducComentarioParti.push(objTradComentPartic);
                    objComentPartic.traducciones = arrayTraducComentarioParti;
                  }
                  arrayComentPartic.push(objComentPartic);
                }
              }

              let arrayConfigPartic = [];
              //Verifica si el participante tiene configuraciones
              if (participantes.configuraciones.length > 0) {
                for (const confParticipante of participantes.configuraciones) {
                  let arrayEstilosConfigPartic = [];
                  //Verifica si tiene estilos
                  if (confParticipante.estilos.length > 0) {
                    for (const estilosParticipante of confParticipante.estilos) {
                      //Obtiene el color
                      console.log(
                        'acaaaaaaaaaa se caeeeeeeeeeeeee: ',
                        estilosParticipante,
                      );
                      const getColor = await this.catalogoColoresService.obtenerCatalogoColoresByCodigo(
                        estilosParticipante.color,
                      );

                      //Objeto de tipo catalogo estilos
                      //Obtiene el catalogo estilos
                      const getCatEstilos = await this.catalogoEstilosService.obtenerCatalogoEstilosByCodigo(
                        estilosParticipante.tipo,
                      );

                      //Verifica si el estilo tiene media
                      let mediaEstiloPartic: any;
                      if (estilosParticipante.media) {
                        //Data de medias
                        //Llama al metodo del obtener medias
                        mediaEstiloPartic = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(
                          estilosParticipante.media,
                          codIdioma,
                          opts,
                        );
                      }
                      let objEstiloPartic = {
                        _id: estilosParticipante._id,
                        codigo: estilosParticipante.codigo,
                        color: getColor,
                        tipo: getCatEstilos,
                        media: mediaEstiloPartic,
                      };
                      arrayEstilosConfigPartic.push(objEstiloPartic);
                    }
                  }

                  //Verifica si la configuracion tiene tono de notificacion
                  let objMediaTonoNotificacion: any;
                  if (confParticipante.tonoNotificacion) {
                    //Data de medias
                    //Llama al metodo del obtener medias
                    objMediaTonoNotificacion = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(
                      confParticipante.tonoNotificacion,
                      codIdioma,
                      opts,
                    );
                  }

                  //Obtiene el catalogo configuracion segun el codigo
                  const getCatConfiguracion = await this.catalogoConfiguracionService.obtenerCatalogoConfiguracionByCodigo(
                    confParticipante.tipo,
                  );

                  let objConfigPartic = {
                    _id: confParticipante._id,
                    codigo: confParticipante.codigo,
                    tonoNotificacion: objMediaTonoNotificacion,
                    estilos: arrayEstilosConfigPartic,
                    tipo: getCatConfiguracion,
                  };
                  arrayConfigPartic.push(objConfigPartic);
                }
              }
              let objCoautorPartic = {
                _id: participantes.coautor._id,
                nombreContacto: participantes.coautor.nombreContacto,
                nombreContactoTraducido:
                  participantes.coautor?.nombreContactoTraducido || null,
                nombre: participantes.coautor.nombre,
              };
              //objeto de tipo comentario
              let objParticipantes = {
                _id: participantes._id,
                roles: arrayRolParticipante,
                comentarios: arrayComentPartic,
                configuraciones: arrayConfigPartic,
                coautor: objCoautorPartic,
              };
              arrayParticipantes.push(objParticipantes);
            }
          }
        }

        //Data de traducciones del proyecto
        // let traduccionProyecto = []
        // //Objeto de tipo traducccion proyecto
        // let trad = {
        //   tituloCorto: proyecto.traducciones[0].tituloCorto,
        //   titulo: proyecto.traducciones[0].titulo,
        //   descripcion: proyecto.traducciones[0].descripcion,
        // }
        // traduccionProyecto.push(trad)

        let objDireccion: any;
        let traduccionDireccion = [];
        if (proyecto.direccion) {
          //Objeto de tipo direccion
          objDireccion = {
            _id: proyecto.direccion._id,
          };
          if (proyecto.direccion.localidad) {
            //Obtiene la localidad segun el codigo enviado
            const getLocalidad = await this.obtenerLocalidadProyectoService.obtenerLocalidadProyecto(
              proyecto.direccion.localidad,
              codIdioma,
            );
            objDireccion.localidad = getLocalidad;
          }

          // if (proyecto.direccion.traducciones) {

          console.log(
            'TRADUCCION DIRECCION: ',
            proyecto.direccion.traducciones.length,
          );
          if (proyecto.direccion.traducciones.length > 0) {
            //Si la traduccion no es la original
            if (!proyecto.direccion.traducciones[0].original) {
              console.log('direccion NO es original');
              let obtTraduccionDireccion = {
                descripcion: proyecto.direccion.traducciones[0].descripcion,
                idioma: proyecto.direccion.traducciones[0].idioma,
                original: proyecto.direccion.traducciones[0].original,
              };
              traduccionDireccion.push(obtTraduccionDireccion);

              //Obtiene traduccion original
              let getTraDirecOriginal = await this.obtenerDireccionService.obtenerDireccionTraduccionOriginal(
                proyecto.direccion._id,
                opts,
              );
              traduccionDireccion.push(getTraDirecOriginal);

              objDireccion.traducciones = traduccionDireccion;
            } else {
              console.log('direccion SI es original');
              let obtTraduccionDireccion = {
                descripcion: proyecto.direccion.traducciones[0].descripcion,
                idioma: proyecto.direccion.traducciones[0].idioma,
                original: proyecto.direccion.traducciones[0].original,
              };
              traduccionDireccion.push(obtTraduccionDireccion);

              objDireccion.traducciones = traduccionDireccion;
            }
          } else {
            //traduce la direccion
            let getTradDir = await this.traducirDireccionService.traducirDireccion(
              proyecto.direccion._id,
              codIdioma,
              opts,
            );
            if (getTradDir.length > 0) {
              objDireccion.traducciones = getTradDir;
              //Obtiene traduccion original
              let getTraDirecOriginal = await this.obtenerDireccionService.obtenerDireccionTraduccionOriginal(
                proyecto.direccion._id,
                opts,
              );

              objDireccion.traducciones.push(getTraDirecOriginal);
            }
          }
          // }

          objDireccion.pais = proyecto.direccion.pais
            ? await this.obtenerLocalidadProyectoService.obtenerPaisProyecto(
              proyecto.direccion.pais,
              codIdioma,
            )
            : null;
        }

        //Obtiene el tipo de proyecto segun el codigo enviado
        const getTipoProyecto = await this.catalogoTipoProyectoByCodigoService.obtenerCatalogoTipoProyectoByCodigo(
          proyecto.tipo,
          codIdioma,
        );

        if (proyecto.transferenciaActiva) {
          proyecto.transferenciaActiva = proyecto.transferenciaActiva;
        } else {
          proyecto.transferenciaActiva = null;
        }
        // //Objeto de proyecto
        // if (prOriginal && prOriginal.traducciones[0]?.idioma !== idioma.codigo) {
        //   const dataOriginal = {
        //     tituloCorto: prOriginal.traducciones[0].tituloCorto,
        //     titulo: prOriginal.traducciones[0].titulo,
        //     descripcion: prOriginal.traducciones[0].descripcion,
        //     original: prOriginal.traducciones[0].original
        //   }
        //   traduccionProyecto.push(dataOriginal);
        // }

        let getAlbumPerfil = await this.obtenerAlbumPerfilService.obtenerAlbumPerfil(
          proyecto.perfil['_id'],
        );

        let dataPerfil = {
          _id: proyecto.perfil['_id'],
          nombreContacto: proyecto.perfil['nombreContacto'],
          nombreContactoTraducido: proyecto.perfil['nombreContactoTraducido'],
          album: [getAlbumPerfil.album],
        };

        let datos = {
          _id: proyecto._id,
          traducciones: proyecto.traducciones,
          adjuntos: getAdjuntos,
          medias: getMedia,
          totalVotos: proyecto?.totalVotos,
          perfil: dataPerfil,
          direccion: objDireccion || null,
          votos: proyecto.votos,
          comentarios: arrayComentarios,
          participantes: arrayParticipantes,
          tipo: getTipoProyecto,
          valorEstimado: proyecto.valorEstimado,
          transferenciaActiva: proyecto.transferenciaActiva,
          montoFaltante: getMontoFaltanteProyecto,
          moneda: {
            codNombre: proyecto.moneda,
          },
          estado: {
            codigo: proyecto.estado,
          },
          fechaCreacion: proyecto.fechaCreacion,
          fechaActualizacion: proyecto.fechaActualizacion,
          actualizado: actu, //Si el proyecto ha sido actualizaco recientemente =true
          voto: votoBo, //si el perfil ya a votado =true si no =false
        };

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return datos;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async verificarTraduccion(
    idProyecto,
    codIdioma,
    codIdim,
    codEstadoTradProyecto,
    codEstadoProyectos,
    opts,
    session,
    perfilPropietario,
    original?,
  ) {
    console.log('original: ', original);
    console.log('idProyecto: ', idProyecto);
    //Obtiene el codigo de la entidad media
    const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    let codEntidadMedia = entidadmedia.codigo;

    //Obtiene el estado activo de la entidad proyectos
    const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadMedia,
    );
    let codEstadoMedia = estadoMedia.codigo;

    //Obtiene l codigo de la entidad album
    const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.album,
    );
    let codEntidadAlbum = entidadAlbum.codigo;

    //Obtiene el estado activo de la entidad album
    const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadAlbum,
    );
    let codEstadoAlbum = estadoAlbum.codigo;

    //Obtiene l codigo de la entidad voto proyecto
    const entidaVotoProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.votoProyecto,
    );
    let codEntidadVotoProyecto = entidaVotoProyecto.codigo;

    //Obtiene el estado activo de la entidad voto proyectos
    const estadoVotoProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadVotoProyecto,
    );
    let codEstadoVotoProyecto = estadoVotoProyecto.codigo;

    //Obtiene el codigo de la entidad comentarios
    const entidaComentarios = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.comentarios,
    );
    let codEntidadComentarios = entidaComentarios.codigo;

    //Obtiene el estado activo de la entidad coemntarios
    const estadoComentarios = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadComentarios,
    );
    let codEstadoComentarios = estadoComentarios.codigo;

    //Obtiene el codigo de la entidad traduccion comentarios
    const entidaTradComentarios = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionComentario,
    );
    let codEntidadTradComentarios = entidaTradComentarios.codigo;

    //Obtiene el estado activo de la entidad coemntarios
    const estadoTradComentarios = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradComentarios,
    );
    let codEstadoTradComentarios = estadoTradComentarios.codigo;

    //Obtiene el codigo de la entidad participante proyecto
    const entidadPartiProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.participanteProyecto,
    );
    let codEntidadPartProyecto = entidadPartiProyecto.codigo;

    //Obtiene el estado activo de la entidad participante proyecto
    const estadoPartiProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadPartProyecto,
    );
    let codEstadoPartiproyecto = estadoPartiProyecto.codigo;

    //Obtiene el codigo de la entidad participante proyecto
    const entidadConfigEstilo = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.configuracionEstilo,
    );
    let codEntidadConfigEstilo = entidadConfigEstilo.codigo;

    //Obtiene el estado activo de la entidad participante proyecto
    const estadoConfigEstilo = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadConfigEstilo,
    );
    let codEstadoConfigEstilo = estadoConfigEstilo.codigo;

    //Obtiene el codigo de la entidad traduccion media
    const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    let codEntidadTradMedia = entidadTradMedia.codigo;

    //Obtiene el estado activo de la entidad traduccion media
    const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradMedia,
    );
    let codEstadoTradMedia = estadoTradMedia.codigo;

    let proyectoTraduccionOriginal;
    if (!original) {
      if (perfilPropietario) {
        this.proyecto = await this.proyectoModel
          .findOne({
            $and: [
              { _id: idProyecto },
              { estado: { $ne: estadosProyecto.proyectoEliminado } },
              // { estado: codEstadoProyectos },
            ],
          })
          .session(session)
          .select('-__v')
          .populate({
            path: 'traducciones',
            select:
              '-fechaActualizacion -__v  -idioma -tags -estado -fechaCreacion -proyecto',
            match: {
              original: true,
              estado: codEstadoTradProyecto,
            },
          })
          .populate({
            path: 'perfil',
            select: 'nombre nombreContacto nombreContactoTraducido _id',
          })
          .populate({
            path: 'adjuntos',
            select: '-fechaActualizacion -fechaCreacion -__v -nombre',
            match: { estado: codEstadoAlbum },
            populate: [
              {
                path: 'media',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: ' url tipo duracion fileDefault fechaActualizacion',
                  },
                  {
                    path: 'miniatura',
                    select: ' url tipo duracion fileDefault fechaActualizacion',
                  },
                ],
              },
              {
                path: 'portada',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: 'url tipo duracion fileDefault fechaActualizacion',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fileDefault fechaActualizacion',
                  },
                ],
              },
            ],
          })
          .populate({
            path: 'medias',
            select:
              'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
            match: { estado: codEstadoMedia },
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdim, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          })
          .populate({
            path: 'comentarios',
            select: 'adjuntos traducciones coautor importante tipo',
            match: { estado: codEstadoComentarios },
            populate: [
              {
                path: 'adjuntos',
                match: { estado: codEstadoMedia },
                select:
                  'principal traducciones enlace miniatura fechaCreacion fechaActualizacion catalogoMedia traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select:
                      'url tipo duracion  fileDefault fechaActualizacion ',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fileDefault fechaActualizacion ',
                  },
                ],
              },
              {
                path: 'traducciones',
                select: 'texto idioma original _id',
                match: { estado: codEstadoTradComentarios },
              },
              {
                path: 'coautor',
                select: '-_id coautor',
                populate: {
                  path: 'coautor',
                  select: '_id nombre nombreContacto nombreContactoTraducido',
                },
              },
            ],
          })
          .populate({
            path: 'participantes',
            select: 'configuraciones coautor comentarios roles',
            match: { estado: codEstadoPartiproyecto },
            populate: [
              {
                path: 'configuraciones',
                select: 'codigo estilos tonoNotificacion tipo',
                match: { estado: codEstadoConfigEstilo },
                populate: [
                  {
                    path: 'estilos',
                    select: 'codigo media color tipo',
                    populate: [
                      {
                        path: 'media',
                        match: { estado: codEstadoMedia },
                        select:
                          'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion',
                        populate: [
                          {
                            path: 'traducciones',
                            select: 'descripcion',
                            match: {
                              idioma: codIdim,
                              estado: codEstadoTradMedia,
                            },
                          },
                          {
                            path: 'principal',
                            select:
                              ' url tipo duracion fileDefault fechaActualizacion',
                          },
                          {
                            path: 'miniatura',
                            select:
                              ' url tipo duracion fileDefault fechaActualizacion',
                          },
                        ],
                      },
                    ],
                  },
                ],
              },
              {
                path: 'coautor',
                select: '_id nombre nombreContacto nombreContactoTraducido',
              },
              {
                path: 'comentarios',
                select: 'adjuntos traducciones coautor importante tipo',
                match: { estado: codEstadoComentarios },
                populate: [
                  {
                    path: 'adjuntos',
                    match: { estado: codEstadoMedia },
                    select:
                      'principal traducciones enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
                    populate: [
                      {
                        path: 'traducciones',
                        select: 'descripcion',
                        match: { idioma: codIdim, estado: codEstadoTradMedia },
                      },
                      {
                        path: 'principal',
                        select:
                          'url tipo duracion  fileDefault fechaActualizacion ',
                      },
                      {
                        path: 'miniatura',
                        select:
                          'url tipo duracion fileDefault fechaActualizacion ',
                      },
                    ],
                  },
                  {
                    path: 'traducciones',
                    select: 'texto idioma original _id',
                    match: { estado: codEstadoTradComentarios },
                  },
                  {
                    path: 'coautor',
                    select: '-_id coautor',
                    populate: {
                      path: 'coautor',
                      select:
                        '_id nombre nombreContacto nombreContactoTraducido',
                    },
                  },
                ],
              },
              {
                path: 'roles',
                select: 'nombre rol acciones',
                populate: {
                  path: 'acciones',
                  select: 'codigo nombre',
                },
              },
            ],
          })
          .populate({
            path: 'votos',
            select: 'perfil',
            match: { estado: codEstadoVotoProyecto },
            populate: {
              path: 'perfil',
              select: '_id nombre',
            },
          })
          .populate({
            path: 'direccion',
            select: 'traducciones localidad pais',
            populate: {
              path: 'traducciones',
              select: 'descripcion idioma original estado',
              match: {
                estado: codidosEstadosTraduccionDireccion.activa,
                original: true,
              },
            },
          });
      } else {
        this.proyecto = await this.proyectoModel
          .findOne({
            $and: [
              { _id: idProyecto },
              { estado: { $ne: estadosProyecto.proyectoEliminado } },
              // { estado: codEstadoProyectos },
            ],
          })
          .session(session)
          .select('-__v')
          .populate({
            path: 'traducciones',
            select:
              '-fechaActualizacion -idioma -__v  -tags -estado -fechaCreacion -proyecto',
            match: {
              idioma: codIdim,
              estado: codEstadoTradProyecto,
            },
          })
          .populate({
            path: 'perfil',
            select: 'nombre nombreContacto nombreContactoTraducido _id',
          })
          .populate({
            path: 'adjuntos',
            select: '-fechaActualizacion -fechaCreacion -__v -nombre',
            match: { estado: codEstadoAlbum },
            populate: [
              {
                path: 'media',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: ' url tipo duracion fileDefault fechaActualizacion',
                  },
                  {
                    path: 'miniatura',
                    select: ' url tipo duracion fileDefault fechaActualizacion',
                  },
                ],
              },
              {
                path: 'portada',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: 'url tipo duracion fileDefault fechaActualizacion',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fileDefault fechaActualizacion',
                  },
                ],
              },
            ],
          })
          .populate({
            path: 'medias',
            select:
              'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion traducciones',
            match: { estado: codEstadoMedia },
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdim, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          })
          .populate({
            path: 'comentarios',
            select: 'adjuntos traducciones coautor importante tipo',
            match: { estado: codEstadoComentarios },
            populate: [
              {
                path: 'media',
                match: { estado: codEstadoMedia },
                select:
                  'principal traducciones enlace miniatura fechaCreacion fechaActualizacion catalogoMedia traducciones',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdim, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select:
                      'url tipo duracion  fileDefault fechaActualizacion ',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fileDefault fechaActualizacion ',
                  },
                ],
              },
              {
                path: 'traducciones',
                select: 'texto idioma original _id',
                match: { estado: codEstadoTradComentarios },
              },
              {
                path: 'coautor',
                select: '-_id coautor',
                populate: {
                  path: 'coautor',
                  select: '_id nombre nombreContacto nombreContactoTraducido',
                },
              },
            ],
          })
          .populate({
            path: 'participantes',
            select: 'configuraciones coautor comentarios roles',
            match: { estado: codEstadoPartiproyecto },
            populate: [
              {
                path: 'configuraciones',
                select: 'codigo estilos tonoNotificacion tipo',
                match: { estado: codEstadoConfigEstilo },
                populate: [
                  {
                    path: 'estilos',
                    select: 'codigo media color tipo',
                    populate: [
                      {
                        path: 'media',
                        match: { estado: codEstadoMedia },
                        select:
                          'principal enlace miniatura catalogoMedia fechaCreacion fechaActualizacion',
                        populate: [
                          {
                            path: 'traducciones',
                            select: 'descripcion',
                            match: {
                              idioma: codIdim,
                              estado: codEstadoTradMedia,
                            },
                          },
                          {
                            path: 'principal',
                            select:
                              ' url tipo duracion fileDefault fechaActualizacion',
                          },
                          {
                            path: 'miniatura',
                            select:
                              ' url tipo duracion fileDefault fechaActualizacion',
                          },
                        ],
                      },
                    ],
                  },
                ],
              },
              {
                path: 'coautor',
                select: '_id nombre nombreContacto nombreContactoTraducido',
              },
              {
                path: 'comentarios',
                select: 'adjuntos traducciones coautor importante tipo',
                match: { estado: codEstadoComentarios },
                populate: [
                  {
                    path: 'adjuntos',
                    match: { estado: codEstadoMedia },
                    select:
                      'principal traducciones enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
                    populate: [
                      {
                        path: 'traducciones',
                        select: 'descripcion',
                        match: { idioma: codIdim, estado: codEstadoTradMedia },
                      },
                      {
                        path: 'principal',
                        select:
                          'url tipo duracion  fileDefault fechaActualizacion ',
                      },
                      {
                        path: 'miniatura',
                        select:
                          'url tipo duracion fileDefault fechaActualizacion ',
                      },
                    ],
                  },
                  {
                    path: 'traducciones',
                    select: 'texto idioma original _id',
                    match: { estado: codEstadoTradComentarios },
                  },
                  {
                    path: 'coautor',
                    select: '-_id coautor',
                    populate: {
                      path: 'coautor',
                      select:
                        '_id nombre nombreContacto nombreContactoTraducido',
                    },
                  },
                ],
              },
              {
                path: 'roles',
                select: 'nombre rol acciones',
                populate: {
                  path: 'acciones',
                  select: 'codigo nombre',
                },
              },
            ],
          })
          .populate({
            path: 'votos',
            select: 'perfil',
            match: { estado: codEstadoVotoProyecto },
            populate: {
              path: 'perfil',
              select: '_id nombre',
            },
          })
          .populate({
            path: 'direccion',
            select: 'traducciones localidad pais',
            populate: {
              path: 'traducciones',
              select: 'descripcion idioma original estado',
              match: {
                estado: codidosEstadosTraduccionDireccion.activa,
                idioma: codIdim,
              },
            },
          });
      }
    } else {
      //Obtiene el proyecto en su traduccion original
      proyectoTraduccionOriginal = await this.proyectoModel
        .findOne({
          $and: [
            { _id: idProyecto },
            { estado: { $ne: estadosProyecto.proyectoEliminado } },
            // { estado: codEstadoProyectos },
          ],
        })
        .session(session)
        .select('-__v')
        .populate({
          path: 'traducciones',
          select:
            '-fechaActualizacion -idioma -__v -estado -fechaCreacion -proyecto -tags',
          match: {
            original: true,
            estado: codEstadoTradProyecto,
          },
        })
        .populate({
          path: 'direccion',
          select: 'traducciones localidad pais',
          populate: {
            path: 'traducciones',
            select: 'descripcion idioma original estado',
            match: {
              estado: codidosEstadosTraduccionDireccion.activa,
              original: true,
            },
          },
        });
    }

    if (proyectoTraduccionOriginal) {
      await this.proyecto.traducciones.push(
        proyectoTraduccionOriginal.traducciones[0],
      );
      return this.proyecto;
    }

    if (this.proyecto) {
      const idPerfil = this.proyecto.perfil._id;
      if (this.proyecto.traducciones.length === 0) {
        await this.traducirProyectoService.TraducirProyecto(
          idProyecto,
          idPerfil,
          codIdioma,
          opts,
        );
        return await this.verificarTraduccion(
          idProyecto,
          codIdioma,
          codIdim,
          codEstadoTradProyecto,
          codEstadoProyectos,
          opts,
          session,
          perfilPropietario,
          false,
        );
      } else {
        if (this.proyecto.traducciones[0].original) {
          return this.proyecto;
        } else {
          return await this.verificarTraduccion(
            idProyecto,
            codIdioma,
            codIdim,
            codEstadoTradProyecto,
            codEstadoProyectos,
            opts,
            session,
            perfilPropietario,
            true,
          );
        }
      }
    } else {
      return null;
    }
  }
}
