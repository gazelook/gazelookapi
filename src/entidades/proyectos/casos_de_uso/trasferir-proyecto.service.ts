import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { CrearParticipanteProyectoDto } from 'src/entidades/participante_proyecto/entidad/participante-proyecto-dto';
import { CrearParticipanteProyectoService } from 'src/entidades/participante_proyecto/casos_de_uso/crear-participante-proyecto.service';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import {
  codigosRol,
  idiomas,
  codIdiomas,
  codigoCatalogosTipoEmail,
  nombreEntidades,
  nombrecatalogoEstados,
  nombreAcciones,
} from 'src/shared/enum-sistema';
import { CrearEmailTransferirProyectoService } from 'src/entidades/emails/casos_de_uso/crear-email-transferir-proyecto.service';
import { TrasferirProyectoDto } from '../entidad/trasferir-proyecto-dto';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { ParticipanteProyecto } from 'src/drivers/mongoose/interfaces/participante_proyecto/participante_proyecto.interface';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { erroresGeneral, erroresProyecto } from '../../../shared/enum-errores';
import { CrearComentarioService } from 'src/entidades/comentarios/casos_de_uso/crear-comentario.service';

const sw = require('stopword');
const mongoose = require('mongoose');

@Injectable()
export class TransferirProyectoService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    @Inject('PARTICIPANTE_PROYECTO_MODEL')
    private readonly participanteProyectoModel: Model<ParticipanteProyecto>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearEmailTransferirProyectoService: CrearEmailTransferirProyectoService,
    private gestionRolService: GestionRolService,
    private crearParticipanteProyectoService: CrearParticipanteProyectoService,
    private traducirProyectoService: TraducirProyectoService,
    private crearHistoricoService: CrearHistoricoService,
    private crearComentarioService: CrearComentarioService,
  ) {}
  getCodIdiomas = codIdiomas;
  getIdiomas = idiomas;
  async EnviarCorreoConfirmacion(
    trasferirProyectoDto: TrasferirProyectoDto,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la entidad proyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let getCodEntProyecto = entidad.codigo;

      //Obtiene el estado de proyectos
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        getCodEntProyecto,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad perfiles
      const entidadPerf = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.perfiles,
      );
      let codEntidadPerfiles = entidadPerf.codigo;

      //Obtiene el estado  activa de la entidad perfiles
      const estadoPerfiles = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadPerfiles,
      );
      let codEstadoPerfiles = estadoPerfiles.codigo;

      //Obtiene la entidad traduccion proyecto
      const entidadTradProy = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTradProy = entidadTradProy.codigo;

      //Obtiene el estado  activa de la entidad participante proyecto
      const estadoTradProye = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradProy,
      );
      let codEstadoTradProy = estadoTradProye.codigo;

      const getEmailDest = await this.perfilModel
        .findOne({
          _id: trasferirProyectoDto.perfilNuevo._id,
          estado: codEstadoPerfiles,
        })
        .populate({
          path: 'usuario',
          select: '-_id email idioma',
        });

      if (getEmailDest) {
        let getIdiomaUsuario = getEmailDest.usuario['idioma'];

        const getProyecto = await this.proyectoModel
          .findOne({
            _id: trasferirProyectoDto.proyecto._id,
            estado: codEstado,
          })
          .populate([
            {
              path: 'traducciones',
              select: '_id titulo tituloCorto descripcion',
              match: {
                idioma: getIdiomaUsuario,
                estado: codEstadoTradProy,
              },
            },
            {
              path: 'perfil',
              select: 'nombre nombreContacto',
            },
          ]);
        if (!getProyecto) {
          throw {
            codigo: HttpStatus.CONFLICT,
            codigoNombre: erroresProyecto.ERROR_TRANSFERIR_PROYECTO,
          };
        }
        let getTituloProyecto;
        let getTituloCortoProyecto;
        let getDescripcionProyecto;
        const filtroSel = this.seleccionarIdioma(getIdiomaUsuario);
        let codIdiomaUsuario;
        if (filtroSel === 1) {
          codIdiomaUsuario = this.getIdiomas.espanol;
        }
        if (filtroSel === 2) {
          codIdiomaUsuario = this.getIdiomas.ingles;
        }
        if (filtroSel === 3) {
          codIdiomaUsuario = this.getIdiomas.aleman;
        }
        if (filtroSel === 4) {
          codIdiomaUsuario = this.getIdiomas.portugues;
        }
        if (filtroSel === 5) {
          codIdiomaUsuario = this.getIdiomas.frances;
        }
        if (filtroSel === 6) {
          codIdiomaUsuario = this.getIdiomas.italiano;
        }

        if (getProyecto.traducciones.length === 0) {
          //Llama al servicio de traduccion de proyecto
          const tradProyecto = await this.traducirProyectoService.TraducirProyecto(
            trasferirProyectoDto.proyecto._id,
            trasferirProyectoDto.perfilPropietario._id,
            codIdiomaUsuario,
            opts,
          );

          const proyecto = await this.proyectoModel
            .findOne({ _id: trasferirProyectoDto.proyecto._id })
            .session(session)
            .populate([
              {
                path: 'traducciones',
                select: '_id titulo tituloCorto descripcion',
                match: {
                  idioma: getIdiomaUsuario,
                  estado: codEstadoTradProy,
                },
              },
              {
                path: 'perfil',
                select: 'nombre nombreContacto',
              },
            ]);
          console.log('proyecto: ', proyecto);
          getTituloProyecto = proyecto.traducciones[0]['titulo'];
          getTituloCortoProyecto = proyecto.traducciones[0]['tituloCorto'];
          getDescripcionProyecto = proyecto.traducciones[0]['descripcion'];
        } else {
          getTituloProyecto = getProyecto.traducciones[0]['titulo'];
          getTituloCortoProyecto = getProyecto.traducciones[0]['tituloCorto'];
          getDescripcionProyecto = getProyecto.traducciones[0]['descripcion'];
        }

        let getEmail = getEmailDest.usuario['email'];
        let getNombrePerfil = getEmailDest.nombre;

        const newTranfProyecto = {
          perfilPropietario: trasferirProyectoDto.perfilPropietario._id,
          nombrePropietario: getProyecto.perfil['nombre'],
          nombrePerfilNuevo: getNombrePerfil,
          perfilNuevo: trasferirProyectoDto.perfilNuevo._id,
          emailDestinatario: getEmail, //'marvinrjj@gmail.com',
          idProyecto: trasferirProyectoDto.proyecto._id,
          codigo: codigoCatalogosTipoEmail.transferir_proyecto,
          titulo: getTituloProyecto,
          tituloCorto: getTituloCortoProyecto,
          descripcion: getDescripcionProyecto,
          idioma: codIdiomaUsuario,
        };

        //Llama al metodo de enviar correo para trasferir proyecto
        const trasfProyecto = await this.crearEmailTransferirProyectoService.crearEmailTrasferirProyecto(
          newTranfProyecto,
          opts,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return trasfProyecto;
      } else {
        await session.abortTransaction();
        await session.endSession();
        return null;
      }
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }

  //Metodo de cambio de roles propietarios de proyecto
  async trasferirProyecto(
    idProyecto,
    perfilPropietario,
    perfilNuevo,
  ): Promise<any> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    let getAccion = accion.codigo;

    //Obtiene la entidad proyecto
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.proyectos,
    );
    let getCodEntProyecto = entidad.codigo;

    //Obtiene el estado de proyectos
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      getCodEntProyecto,
    );
    let codEstado = estado.codigo;

    //Obtiene la entidad participante proyecto
    const entidadParProyec = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.participanteProyecto,
    );
    let codEntidadPartProyecto = entidadParProyec.codigo;

    //Obtiene el estado  activa de la entidad participante proyecto
    const estadoPartProyec = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadPartProyecto,
    );
    let codEstadoPartProyecto = estadoPartProyec.codigo;

    //Obtiene el estado  eliminado de la entidad participante proyecto
    const estadoEliPartProyec = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.eliminado,
      codEntidadPartProyecto,
    );
    let codEstadoEliPartProyecto = estadoEliPartProyec.codigo;

    //Obtiene la entidad comentarios
    const entidadComent = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.comentarios,
    );
    let getCodEntComent = entidadComent.codigo;

    //Obtiene la entidad comentario
    const entidadComentario = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.comentarios,
    );
    let codEntidadComentario = entidadComentario.codigo;

    let session = await mongoose.startSession();
    session.startTransaction();

    try {
      if (perfilPropietario != perfilNuevo) {
        const opts = { session };

        //Actualizo el nuevo usuario del proyecto
        await this.proyectoModel.findByIdAndUpdate(
          { _id: idProyecto },
          { perfil: perfilNuevo, fechaActualizacion: new Date() },
          opts,
        );

        const getParProyec = await this.participanteProyectoModel
          .findOne({
            coautor: perfilNuevo,
            proyecto: idProyecto,
            estado: codEstadoPartProyecto,
          })
          .populate({
            path: 'roles',
            select: '_id nombre rol entidad',
          });
        //Verifico si ya existe el perfil al que se le va a trasferir el proyecto como participante del proyecto
        if (getParProyec) {
          let listaRoles = [];
          let getparticipanteProyId = getParProyec._id;
          //Verifica que tenga roles asignados
          if (getParProyec.roles.length > 0) {
            //Si tiene rol se le quita los roles que tiene
            for (const getRol of getParProyec.roles) {
              listaRoles.push(getRol['_id'].toString());
            }
            //Obtengo el rol usuario coautor: Comentarios
            const getRolEntidadCoautorComentario = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
              codigosRol.cod_rol_ent_co_autor,
              getCodEntComent,
            );
            const idRolEntidadCoautorComentario =
              getRolEntidadCoautorComentario._id;

            let index = listaRoles.indexOf(idRolEntidadCoautorComentario);
            //Eliminamos el elemento del array
            listaRoles.splice(index, 1);

            // ______ datos roles_____
            let dataRoles: any = {
              roles: listaRoles,
              fechaActualizacion: new Date(),
            };

            //Se le quita el rol al participante proyecto
            await this.participanteProyectoModel.findByIdAndUpdate(
              getparticipanteProyId,
              dataRoles,
              opts,
            );
          }
          //Obtengo el rol usuario propietario: Proyectos
          const getRolEntidadPropietarioProyecto = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
            codigosRol.cod_rol_ent_propietario,
            getCodEntProyecto,
          );
          const idRolEntidadPropietarioProyecto =
            getRolEntidadPropietarioProyecto._id;

          //Se le asgina al array de roles el rol propietario proyecto
          await this.participanteProyectoModel.updateOne(
            { _id: getParProyec._id },
            {
              $push: { roles: idRolEntidadPropietarioProyecto },
              fechaActualizacion: new Date(),
            },
            opts,
          );
        } else {
          //Si no existe el nuevo perfil como participante del proyecto

          //Obtengo el rol usuario propietario: Proyectos
          const getRolEntidadPropietarioProyecto = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
            codigosRol.cod_rol_ent_propietario,
            getCodEntProyecto,
          );
          const idRolEntidadPropietarioProyecto =
            getRolEntidadPropietarioProyecto._id;
          let rolEntPropietarioProyecto = [];
          rolEntPropietarioProyecto.push(idRolEntidadPropietarioProyecto);

          //Obtener una configuracion aleatoria para participante proyecto
          const getConfigEstilo = await this.crearComentarioService.ObtenerConfiguracionEstilo(
            codEntidadComentario,
          );

          var participanteProyectoDto: CrearParticipanteProyectoDto;
          participanteProyectoDto = {
            coautor: perfilNuevo,
            proyecto: idProyecto,
            roles: rolEntPropietarioProyecto[0],
            comentarios: [],
            configuraciones: [getConfigEstilo],
            estado: codEstadoPartProyecto,
            totalComentarios: 0,
          };

          //Llama al metodo de crear nuevo participante del proyecto
          const crearParcipanteProyec = await this.crearParticipanteProyectoService.crearParticipanteProyecto(
            participanteProyectoDto,
            opts,
          );

          const getIdPartProy = crearParcipanteProyec._id;

          //Actualiza el array de id de participantes
          await this.proyectoModel.updateOne(
            { _id: idProyecto },
            {
              $push: { participantes: getIdPartProy },
              transferenciaActiva: null,
              fechaActualizacion: new Date(),
            },
            opts,
          );
        }

        const getParProyecAntiguo = await this.participanteProyectoModel
          .findOne({
            coautor: perfilPropietario,
            proyecto: idProyecto,
            estado: codEstadoPartProyecto,
          })
          .populate({
            path: 'roles',
            select: '_id nombre rol entidad',
          });
        //Verifico si existe el perfil propietario actual del proyecto como participante del proyecto
        if (getParProyecAntiguo) {
          let listaRolesPropAntiguo = [];
          let getparticipanteAntiguoProyId = getParProyecAntiguo._id;
          //Verifica que tenga roles asignados
          if (getParProyecAntiguo.roles.length > 0) {
            //Si tiene rol se le quita los roles que tiene
            for (const getRol of getParProyecAntiguo.roles) {
              listaRolesPropAntiguo.push(getRol['_id'].toString());
            }
            //Obtengo el rol usuario propietario: Proyectos
            const getRolEntidadPropietarioProyecto = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
              codigosRol.cod_rol_ent_propietario,
              getCodEntProyecto,
            );
            const idRolEntidadPropietarioProyecto =
              getRolEntidadPropietarioProyecto._id;

            let index = listaRolesPropAntiguo.indexOf(
              idRolEntidadPropietarioProyecto,
            );
            //Eliminamos el elemento del array
            listaRolesPropAntiguo.splice(index, 1);

            // ______ datos roles_____
            let dataRolesPart: any = {
              roles: listaRolesPropAntiguo,
              fechaActualizacion: new Date(),
            };

            //Se le quita el rol al participante proyecto
            await this.participanteProyectoModel.findByIdAndUpdate(
              getparticipanteAntiguoProyId,
              dataRolesPart,
              opts,
            );
          }
          //Obtengo el rol usuario coautor: Comentarios
          const getRolEntidadCoautorComentario = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
            codigosRol.cod_rol_ent_co_autor,
            getCodEntComent,
          );
          const idRolEntidadCoautorComentario =
            getRolEntidadCoautorComentario._id;

          //Se le asigna al array de roles el rol propietario proyecto
          await this.participanteProyectoModel.updateOne(
            { _id: getParProyecAntiguo._id },
            {
              $push: { roles: idRolEntidadCoautorComentario },
              fechaActualizacion: new Date(),
            },
            opts,
          );
        }

        //Objeto para guardar el historico de la trasferencia del proyecto
        let datosTransferir = {
          _id: idProyecto,
          perfilPropietario: perfilPropietario,
          perfilNuevo: perfilNuevo,
        };
        let newHistoricoTransferirProyecto: any = {
          datos: datosTransferir, //Datos de la transferencia del proyecto
          usuario: perfilPropietario,
          accion: getAccion,
          entidad: getCodEntProyecto,
        };

        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoTransferirProyecto,
        );

        // finalizar session transaccion
        await session.commitTransaction();
        await session.endSession();

        return true;
      } else {
        await session.endSession();
        return false;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();

      throw error;
    }
  }

  seleccionarIdioma(filtro) {
    switch (filtro) {
      case this.getCodIdiomas.espanol:
        return 1;
      case this.getCodIdiomas.ingles:
        return 2;
      case this.getCodIdiomas.aleman:
        return 3;
      case this.getCodIdiomas.portugues:
        return 4;
      case this.getCodIdiomas.frances:
        return 5;
      case this.getCodIdiomas.italiano:
        return 6;
    }
  }
}
