import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { CrearProyectoDto } from '../entidad/crear-proyecto-dto';
import { CrearParticipanteProyectoDto } from 'src/entidades/participante_proyecto/entidad/participante-proyecto-dto';
import { CrearParticipanteProyectoService } from 'src/entidades/participante_proyecto/casos_de_uso/crear-participante-proyecto.service';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import {
  codigosRol,
  estadoLocalidad,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { CrearAlbumService } from 'src/entidades/album/casos_de_uso/crear-album.service';
import { ActualizarMediaService } from 'src/entidades/media/casos_de_uso/actualizar-media.service';
import { CrearDireccionService } from 'src/entidades/direccion/casos_de_uso/crear-direccion.service';
import { CrearComentarioService } from 'src/entidades/comentarios/casos_de_uso/crear-comentario.service';
import { TraducirProyectoServiceSegundoPlano } from './traducir-proyecto-segundo-plano.service';
import { GestionProyectoService } from './gestion-proyecto.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { CrearLocalidadService } from '../../pais/casos_de_uso/crear-localidad.service';
import { ConvertirMontoMonedaService } from 'src/entidades/moneda/casos_de_uso/convertir-monto-moneda.service';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
const sw = require('stopword');
const mongoose = require('mongoose');

@Injectable()
export class CrearProyectoService {
  constructor(
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearParticipanteProyectoService: CrearParticipanteProyectoService,
    private gestionRolService: GestionRolService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private crearAlbumService: CrearAlbumService,
    private actualizarMediaService: ActualizarMediaService,
    private crearDireccionService: CrearDireccionService,
    private crearComentarioService: CrearComentarioService,
    private traducirProyectoServiceSegundoPlano: TraducirProyectoServiceSegundoPlano,
    private gestionProyectoService: GestionProyectoService,
    private crearLocalidadService: CrearLocalidadService,
    private convertirMontoMonedaService: ConvertirMontoMonedaService,
  ) {}
  private idiomaTrad: any;

  async crearProyecto(
    proyectoDto: CrearProyectoDto,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad proyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let getCodEntProyecto = entidad.codigo;

      //Obtiene el estado de proyectos
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        getCodEntProyecto,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad traduccion proyecto
      const entidadTN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTN = entidadTN.codigo;

      //Obtiene el estado  activa de la entidad traduccion proyecto
      const estadoTN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTN,
      );
      let codEstadoTN = estadoTN.codigo;

      //Obtiene la entidad participante proyecto
      const entidadParProyec = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.participanteProyecto,
      );
      let codEntidadPartProyecto = entidadParProyec.codigo;

      //Obtiene el estado  activa de la entidad participante proyecto
      const estadoPartProyec = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadPartProyecto,
      );
      let codEstadoPartProyecto = estadoPartProyec.codigo;

      //Obtiene la entidad comentario
      const entidadComentario = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarios,
      );
      let codEntidadComentario = entidadComentario.codigo;

      let textoTraducido = await traducirTexto(
        codIdiom,
        proyectoDto.traducciones[0].titulo,
      );
      let idiomaDetectado = textoTraducido.idiomaDetectado;

      //Obtiene el idioma por el codigo
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idiomaDetectado,
      );
      let codIdioma = idioma.codigo;

      //Objeto de traduccion proyectos
      const idProyecto = new mongoose.mongo.ObjectId();

      let newTraduccionProyecto = {
        titulo: proyectoDto.traducciones[0].titulo,
        tituloCorto: proyectoDto.traducciones[0].tituloCorto,
        descripcion: proyectoDto.traducciones[0].descripcion,
        idioma: codIdioma,
        original: true,
        estado: codEstadoTN,
        proyecto: idProyecto,
      };

      //Crea la traduccion del proyecto
      const crearTraduccionProyecto = await new this.traduccionProyectoModel(
        newTraduccionProyecto,
      ).save(opts);

      const dataTraduccion = JSON.parse(
        JSON.stringify(crearTraduccionProyecto),
      );
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      let newHistoricoTraduccionProyecto: any = {
        datos: dataTraduccion,
        usuario: proyectoDto.perfil,
        accion: getAccion,
        entidad: codEntidadTN,
      };

      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionProyecto,
      );

      let codTraduccionProyecto = [];
      codTraduccionProyecto.push(dataTraduccion._id);

      const getRolEntidad = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
        codigosRol.cod_rol_ent_propietario,
        getCodEntProyecto,
      );
      const idRolEntidad = getRolEntidad._id;
      let rolEnt = [];
      rolEnt.push(idRolEntidad);

      //Obtener una configuracion aleatoria para participante proyecto
      const getConfigEstilo = await this.crearComentarioService.ObtenerConfiguracionEstilo(
        codEntidadComentario,
      );

      var participanteProyectoDto: CrearParticipanteProyectoDto;
      participanteProyectoDto = {
        coautor: proyectoDto.perfil._id,
        proyecto: idProyecto,
        roles: rolEnt[0],
        comentarios: [],
        configuraciones: [getConfigEstilo],
        estado: codEstadoPartProyecto,
        totalComentarios: 0,
      };

      const crearParcipanteProyec = await this.crearParticipanteProyectoService.crearParticipanteProyecto(
        participanteProyectoDto,
        opts,
      );
      const getIdePartProy = crearParcipanteProyec._id;
      let codParticipantes = [];
      codParticipantes.push(getIdePartProy);

      let votos = [];
      let totalVotos = 0;

      //Comprueba si llega la direccion
      const direccionProyecto = proyectoDto.direccion;
      let dataDireccionProyecto;

      let crearDireccion;

      if (direccionProyecto) {
        console.log('direccionProyecto: ************', direccionProyecto);
        if (
          direccionProyecto?.localidad &&
          direccionProyecto?.localidad?.estado?.codigo ===
            estadoLocalidad.enRevision
        ) {
          const nuevaLocalidad = await this.crearLocalidadService.crearLocalidad(
            direccionProyecto.localidad.nombre,
            direccionProyecto.localidad.catalogoPais.codigo,
            proyectoDto.perfil._id,
            opts,
          );
          dataDireccionProyecto = {
            latitud: direccionProyecto.latitud,
            longitud: direccionProyecto.longitud,
            traducciones: direccionProyecto.traducciones,
            localidad: nuevaLocalidad.codigo,
            usuario: proyectoDto.perfil._id,
          };
        } else {
          dataDireccionProyecto = {
            latitud: direccionProyecto?.latitud,
            longitud: direccionProyecto?.longitud,
            traducciones: direccionProyecto?.traducciones,
            localidad: direccionProyecto?.localidad?.codigo,
            pais: direccionProyecto?.pais?.codigo,
            usuario: proyectoDto.perfil._id,
          };
        }
        //Objeto de tipo direccion
        crearDireccion = await this.crearDireccionService.crearDireccion(
          dataDireccionProyecto,
          opts,
        );
      }

      let valorEstimadoConvertido;
      if (proyectoDto.moneda.codNombre != listaCodigosMonedas.USD) {
        valorEstimadoConvertido = await this.convertirMontoMonedaService.convertirMoney(
          proyectoDto.valorEstimado,
          proyectoDto.moneda.codNombre,
          listaCodigosMonedas.USD,
        );
      } else {
        valorEstimadoConvertido = proyectoDto.valorEstimado;
      }

      let nuevoProyecto = {
        _id: idProyecto,
        perfil: proyectoDto.perfil,
        tipo: proyectoDto.tipo.codigo,
        direccion: crearDireccion?._id || null,
        participantes: codParticipantes,
        recomendadoAdmin: false,
        valorEstimado: proyectoDto.valorEstimado,
        valorEstimadoFinal: valorEstimadoConvertido,
        adjuntos: [],
        medias: [],
        votos: votos,
        totalVotos: totalVotos,
        traducciones: codTraduccionProyecto,
        estrategia: null,
        comentarios: [],
        moneda: proyectoDto.moneda.codNombre,
        evento: null,
        estado: codEstado,
        fechaCreacion: new Date(),
        fechaActualizacion: new Date(),
      };
      //Guarda el proyecto
      const crearProyecto = await new this.proyectoModel(nuevoProyecto).save(
        opts,
      );

      // _________________adjuntos_____________________
      let nuevoAlbum;
      let idUsuario;

      const getUsuarioPerfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
        proyectoDto.perfil._id,
      );
      idUsuario = getUsuarioPerfil.usuario._id;

      if (proyectoDto.adjuntos.length > 0) {
        for (const album of proyectoDto.adjuntos) {
          const dataAlbum: any = {
            nombre: album.nombre,
            tipo: album.tipo.codigo,
            media: album.media,
            predeterminado: album.predeterminado,
            usuario: getUsuarioPerfil._id,
            idioma: codIdiom,
          };
          if (album.portada && album.portada._id) {
            dataAlbum.portada = album.portada;
          }
          //// actualiza album
          // if (album._id !== undefined) {
          //   dataAlbum._id = album._id;
          //   await this.actualizarAlbumService.actualizarAlbumconMedia(album._id, dataAlbum);
          // }
          //if (album._id === undefined) {
          //crea el nuevo album
          nuevoAlbum = await this.crearAlbumService.crearAlbum(dataAlbum, opts);
          //Acatualiza el array de adjuntos
          await this.proyectoModel.updateOne(
            { _id: idProyecto },
            { $push: { adjuntos: nuevoAlbum._id } },
            opts,
          );
          //}
        }
      }
      // _________________medias_____________________
      if (proyectoDto.medias.length > 0) {
        for (const media of proyectoDto.medias) {
          let idMedia = media._id;
          ////cambia de estado sinAsignar a activa
          await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
            idMedia,
            getUsuarioPerfil._id,
            codIdioma,
            null,
            false,
            opts,
          );
          //Actualiza el array de medias
          await this.proyectoModel.updateOne(
            { _id: idProyecto },
            { $push: { medias: idMedia } },
            opts,
          );
        }
      }
      const dataProyecto = JSON.parse(JSON.stringify(crearProyecto));
      delete dataProyecto.__v;

      let newHistoricoProyecto: any = {
        descripcion: dataProyecto,
        usuario: proyectoDto.perfil,
        accion: getAccion,
        entidad: entidad.codigo,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistoricoProyecto);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      this.traducirProyectoServiceSegundoPlano.traducirProyectoSegundoPlano(
        crearProyecto._id,
        proyectoDto.perfil,
        idiomaDetectado,
      );

      return crearProyecto;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
