import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';

@Injectable()
export class ObtenerPerfilProyectoService {
  constructor(
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
  ) {}

  async obtenerPropietarioProyecto(idProyecto: string): Promise<any> {
    try {
      let getCreador = await this.proyectoModel.findOne({ _id: idProyecto });
      let perfilProyecto = getCreador.perfil;
      return perfilProyecto;
    } catch (error) {
      throw error;
    }
  }
}
