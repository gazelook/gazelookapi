import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoLocalidad } from 'src/drivers/mongoose/interfaces/catalogo_localidad/catalogo-localidad.interface';
import { CatalogoPais } from 'src/drivers/mongoose/interfaces/catalogo_pais/catalogo-pais.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';

@Injectable()
export class ObtenerLocalidadProyectoService {
  constructor(
    @Inject('CATALOGO_LOCALIDAD_MODEL')
    private readonly catalogoLocalidadModel: Model<CatalogoLocalidad>,
    @Inject('CATALOGO_PAIS_MODEL')
    private readonly catalogoPaisModel: Model<CatalogoPais>,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async obtenerLocalidadProyecto(
    codigoLocalidad: any,
    idioma: any,
  ): Promise<any> {
    try {
      //Obtiene el codigo del idioma
      const getIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      let codIdioma = getIdioma.codigo;

      //Obtiene una localidad segun el codigo enviado
      const catalogoLocalidad = await this.catalogoLocalidadModel.findOne({
        codigo: codigoLocalidad,
      });
      let getCatPais = catalogoLocalidad.catalogoPais;

      //Obtiene una pais segun el codigo enviado
      const catalogoPais = await this.catalogoPaisModel
        .findOne({ codigo: getCatPais })
        .populate({
          path: 'traducciones',
          match: {
            idioma: codIdioma,
          },
        });

      let ObjCatPais = {
        codigo: catalogoPais.codigo,
        codigoNombre: catalogoPais.codigoNombre,
        traducciones: [
          {
            nombre: catalogoPais.traducciones[0]['nombre'],
          },
        ],
      };
      let ObjResp = {
        codigo: catalogoLocalidad.codigo,
        nombre: catalogoLocalidad.nombre,
        codigoPostal: catalogoLocalidad.codigoPostal,
        catalogoPais: ObjCatPais,
      };
      return ObjResp;
    } catch (error) {
      throw error;
    }
  }

  async obtenerPaisProyecto(codigoPais: any, idioma: any): Promise<any> {
    try {
      //Obtiene el codigo del idioma
      const getIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      let codIdioma = getIdioma.codigo;

      //Obtiene una pais segun el codigo enviado
      const catalogoPais = await this.catalogoPaisModel
        .findOne({ codigo: codigoPais })
        .populate({
          path: 'traducciones',
          match: {
            idioma: codIdioma,
          },
        });

      let ObjCatPais = {
        codigo: catalogoPais.codigo,
        codigoNombre: catalogoPais.codigoNombre,
        traducciones: [
          {
            nombre: catalogoPais.traducciones[0]['nombre'],
          },
        ],
      };
      return ObjCatPais;
    } catch (error) {
      throw error;
    }
  }
}
