import { ObneterProyetoTituloService } from './obtener-proyecto-titulo.service';
import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogoProviders } from 'src/entidades/catalogos/drivers/catalogo.provider';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { CrearProyectoService } from './crear-proyecto.service';
import { proyectoProviders } from '../drivers/proyecto.provider';
import { TraducirCatalogoTipoProyectoService } from './traducir-tipo-proyecto.service';
import { CatalogoTipoProyectoService } from './obtener-tipo-proyecto.service';
import { VotoProyectoService } from './voto-proyecto-unico.service';
import { ObtenerProyectoUnicoService } from './obtener-proyecto-unico.service';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { ActualizarProyectoService } from './actualizar-proyecto.service';
import { ConfiguracionEventoService } from 'src/entidades/catalogos/casos_de_uso/configuracion-evento.service';
import { FormulaEventoService } from 'src/entidades/catalogos/casos_de_uso/formula_evento.service';
import { ObtenerProyectosRecientePaginacionService } from './obtener-proyectos-recientes-paginacion.service';
import { UsuarioServicesModule } from 'src/entidades/usuario/casos_de_uso/usuario-services.module';
import { EliminarProyectoUnicoService } from './eliminar-proyecto-unico.service';
import { ParticipanteProyectoServicesModule } from 'src/entidades/participante_proyecto/casos_de_uso/participante-proyecto-services.module';
import { RolServiceModule } from 'src/entidades/rol/casos_de_uso/rol.services.module';
import { ListaCoAutoresService } from './obtener-lista-co-autores.service';
import { TransferirProyectoService } from './trasferir-proyecto.service';
import { EmailServicesModule } from 'src/entidades/emails/casos_de_uso/email-services.module';
import { ObneterProyetoRangoService } from './obtener-proyectos-rango.service';
import { ObtenerProyectosRecomendadosPaginacionService } from './obtener-proyectos-recomendados-paginacion.service';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { AlbumServiceModule } from 'src/entidades/album/casos_de_uso/album.services.module';
import { MediaServiceModule } from 'src/entidades/media/casos_de_uso/media.services.module';
import { ObtenerProyetoPerfilService } from './obtener-proyecto-perfil.service';
import { EliminarProyectoCompletoService } from './eliminar-proyecto-completo.service';
import { ComentariosServicesModule } from 'src/entidades/comentarios/casos_de_uso/comentarios-services.module';
import { ObtenerFechasHistoricoProyectoService } from './obtener-fechas-historico-proyecto.service';
import { ObtenerProyetoIdService } from './obtener-proyecto-id.service';
import { CalcularDiasProyectoService } from './carcular-dias-proyecto.service';
import { Funcion } from 'src/shared/funcion';
import { DireccionServiceModule } from 'src/entidades/direccion/casos_de_uso/direccion.services.module';
import { CambiarEstadoProyectoService } from './cambiar-estado-proyecto.service';
import { ModificarValorEstimadoProyectoService } from './modificar-valor-estimado-proyecto.service';
import { ObtenerPortadaPredeterminadaProyectoService } from './obtener-portada-predeterminada-proyecto.service';
import { ArchivoServicesModule } from 'src/entidades/archivo/casos_de_uso/archivo-services.module';
import { ObtenerLocalidadProyectoService } from './obtener-localidad-proyecto.service';
import { CatalogoTipoProyectoByCodigoService } from './obtener-tipo-proyecto-by-codigo.service';
import { CatalogoColoresService } from 'src/entidades/catalogos/casos_de_uso/catalogo-colores.service';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { CatalogoConfiguracionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-configuracion.service';
import { ObtenerMediasProyectoService } from './obtener-medias-proyecto.service';
import { ObtenerEstadoProyectoService } from './obtener-estado-proyecto.service';
import { ObtenerPerfilProyectoService } from './obtener-creador-proyecto.service';
import { ObtenerProyectosPerfilService } from './obtener-proyectos-perfil.service';
import { NotificacionServicesModule } from 'src/entidades/notificacion/casos_de_uso/notificacion-services.module';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from './obtener-portada-predeterminada-proyecto-resumen.service';
import { ObtenerFechaIniciaForoProyectoService } from './obtener-fecha-inicia-foro-proyecto.service';
import { ObtenerFechaFinForoProyectoService } from './obtener-fecha-fin-foro-proyecto.service';
import { TraducirProyectoServiceSegundoPlano } from './traducir-proyecto-segundo-plano.service';
import { GestionProyectoService } from './gestion-proyecto.service';
import { ProyectosMasVotadosService } from './proyectos-mas-votados.service';
import { ActualizaEstadoProyectoService } from './actualiza-estado-proyecto.service';
import { ObtenerResumenProyectoService } from './obtener-resumen-proyecto.service';
import { ObtenerBeneficiarioTransaccionService } from 'src/entidades/transaccion/casos_de_uso/obtener-beneficiario-transaccion.service';
import { AsignarRolesProyectoService } from './asignar-roles-proyecto.service';
import { ResultadoEstrategiaProyectoService } from './resultado-estrategia.service';
import { ObtenerResultadoEstrategiaService } from './obtener-resultado-estrategia-proyecto.service';
import { ObtenerProyectosSeleccionadosService } from './obtener-proyectos-seleccionados.service';
import { VerificarPerfilProyectoService } from './verificar-perfil.service';
import { ObtenerProyectosEjecutadosFinalizadosService } from './obtener-proyectos-finalizados.service';
import { RecomendarProyectoService } from './recomendar-proyecto.service';
import { PaisServicesModule } from '../../pais/casos_de_uso/pais.services.module';
import { FondosTipoProyectoService } from './fondos-tipo-proyecto.service';
import { FondosFinanciamientoProyectoService } from './fondos-financiamiento-proyecto.service';
import { ObtenerAllProyectosPaginacionService } from './obtener-all-proyectos-paginacion.service';
import { ConvertirMontoMonedaService } from 'src/entidades/moneda/casos_de_uso/convertir-monto-moneda.service';
import { ObtenerTodosProyectosPaginacionService } from './obtener-todos-proyectos-paginacion.service';
import { ObtenerProyectosEsperaFondosService } from './obtener-proyectos-espera-fondos.service';
// import { DonacionProyectoService } from './donacion-proyecto.service';
import { CrearPagoStripeService } from 'src/entidades/transaccion/casos_de_uso/crear-pago-stripe.service';
import { AsignarEquipoProyectoService } from './asignar-equipo-proyecto.service';
// import { TransaccionServicesModule } from 'src/entidades/transaccion/casos_de_uso/transaccion-services.module';

@Module({
  imports: [
    DBModule,
    forwardRef(() => UsuarioServicesModule),
    forwardRef(() => ParticipanteProyectoServicesModule),
    forwardRef(() => RolServiceModule),
    forwardRef(() => EmailServicesModule),
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => AlbumServiceModule),
    forwardRef(() => MediaServiceModule),
    forwardRef(() => ComentariosServicesModule),
    forwardRef(() => DireccionServiceModule),
    forwardRef(() => ArchivoServicesModule),
    forwardRef(() => NotificacionServicesModule),
    PaisServicesModule,
    // forwardRef(() => TransaccionServicesModule),
    
  ],
  providers: [
    ...proyectoProviders,
    ...CatalogoProviders,
    CatalogoIdiomasService,
    CatalogoEstadoService,
    CatalogoEntidadService,
    CatalogoAccionService,
    TraduccionEstaticaController,
    CrearProyectoService,
    TraducirCatalogoTipoProyectoService,
    CatalogoTipoProyectoService,
    VotoProyectoService,
    ObtenerProyectoUnicoService,
    TraducirProyectoService,
    ActualizarProyectoService,
    ConfiguracionEventoService,
    FormulaEventoService,
    ObtenerProyectosRecientePaginacionService,
    EliminarProyectoUnicoService,
    ObneterProyetoTituloService,
    ListaCoAutoresService,
    TransferirProyectoService,
    ObneterProyetoRangoService,
    ObtenerProyectosRecomendadosPaginacionService,
    ObtenerProyetoPerfilService,
    EliminarProyectoCompletoService,
    ObtenerFechasHistoricoProyectoService,
    ObtenerProyetoIdService,
    CalcularDiasProyectoService,
    Funcion,
    CambiarEstadoProyectoService,
    ModificarValorEstimadoProyectoService,
    ObtenerPortadaPredeterminadaProyectoService,
    ObtenerLocalidadProyectoService,
    CatalogoTipoProyectoByCodigoService,
    CatalogoColoresService,
    CatalogoEstilosService,
    CatalogoConfiguracionService,
    ObtenerMediasProyectoService,
    ObtenerEstadoProyectoService,
    ObtenerPerfilProyectoService,
    ObtenerProyectosPerfilService,
    ObtenerPortadaPredeterminadaProyectoResumenService,
    ObtenerFechaIniciaForoProyectoService,
    ObtenerFechaFinForoProyectoService,
    TraducirProyectoServiceSegundoPlano,
    GestionProyectoService,
    ProyectosMasVotadosService,
    ActualizaEstadoProyectoService,
    ObtenerResumenProyectoService,
    ObtenerBeneficiarioTransaccionService,
    AsignarRolesProyectoService,
    ResultadoEstrategiaProyectoService,
    ObtenerResultadoEstrategiaService,
    ObtenerProyectosSeleccionadosService,
    VerificarPerfilProyectoService,
    ObtenerProyectosEjecutadosFinalizadosService,
    RecomendarProyectoService,
    FondosTipoProyectoService,
    FondosFinanciamientoProyectoService,
    ObtenerAllProyectosPaginacionService,
    ConvertirMontoMonedaService,
    ObtenerTodosProyectosPaginacionService,
    ObtenerProyectosEsperaFondosService,
    AsignarEquipoProyectoService
    
  ],
  exports: [
    CrearProyectoService,
    CatalogoTipoProyectoService,
    VotoProyectoService,
    ObtenerProyectoUnicoService,
    TraducirProyectoService,
    ActualizarProyectoService,
    ObtenerProyectosRecientePaginacionService,
    EliminarProyectoUnicoService,
    ObneterProyetoTituloService,
    ListaCoAutoresService,
    TransferirProyectoService,
    ObneterProyetoRangoService,
    ObtenerProyectosRecomendadosPaginacionService,
    ObtenerProyetoPerfilService,
    EliminarProyectoCompletoService,
    ObtenerFechasHistoricoProyectoService,
    ObtenerProyetoIdService,
    CalcularDiasProyectoService,
    CambiarEstadoProyectoService,
    ModificarValorEstimadoProyectoService,
    ObtenerPortadaPredeterminadaProyectoService,
    ObtenerLocalidadProyectoService,
    CatalogoTipoProyectoByCodigoService,
    ObtenerMediasProyectoService,
    ObtenerEstadoProyectoService,
    ObtenerPerfilProyectoService,
    ObtenerProyectosPerfilService,
    ObtenerPortadaPredeterminadaProyectoResumenService,
    ObtenerFechaIniciaForoProyectoService,
    ObtenerFechaFinForoProyectoService,
    TraducirProyectoServiceSegundoPlano,
    GestionProyectoService,
    ProyectosMasVotadosService,
    ActualizaEstadoProyectoService,
    ObtenerResumenProyectoService,
    AsignarRolesProyectoService,
    ResultadoEstrategiaProyectoService,
    ObtenerResultadoEstrategiaService,
    ObtenerProyectosSeleccionadosService,
    VerificarPerfilProyectoService,
    ObtenerProyectosEjecutadosFinalizadosService,
    RecomendarProyectoService,
    FondosTipoProyectoService,
    FondosFinanciamientoProyectoService,
    ObtenerAllProyectosPaginacionService,
    ObtenerTodosProyectosPaginacionService,
    ObtenerProyectosEsperaFondosService,
    AsignarEquipoProyectoService
  ],
  controllers: [],
})
export class ProyectosServicesModule {}
