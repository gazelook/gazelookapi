import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';

import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
const sw = require('stopword');
import * as mongoose from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { ActualizarProyectoDto } from '../entidad/actualizar-proyecto.dto';
import {
  codigosHibernadoEntidades,
  estadosProyecto,
  idiomas,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { CrearAlbumService } from 'src/entidades/album/casos_de_uso/crear-album.service';
import { ActualizarMediaService } from 'src/entidades/media/casos_de_uso/actualizar-media.service';
import { ActualizarDireccionService } from 'src/entidades/direccion/casos_de_uso/actualizar-direccion.service';
import * as moment from 'moment';
import { ModificarValorEstimadoProyectoService } from './modificar-valor-estimado-proyecto.service';
import { TraducirProyectoServiceSegundoPlano } from './traducir-proyecto-segundo-plano.service';
import { GestionProyectoService } from './gestion-proyecto.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
import { ConvertirMontoMonedaService } from 'src/entidades/moneda/casos_de_uso/convertir-monto-moneda.service';

@Injectable()
export class ActualizarProyectoService {
  noticia: any;

  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private crearAlbumService: CrearAlbumService,
    private actualizarMediaService: ActualizarMediaService,
    private actualizarDireccionService: ActualizarDireccionService,
    private modificarValorEstimadoProyectoService: ModificarValorEstimadoProyectoService,
    private traducirProyectoServiceSegundoPlano: TraducirProyectoServiceSegundoPlano,
    private convertirMontoMonedaService: ConvertirMontoMonedaService,
  ) { }

  async actualizarProyecto(
    proyectoActualizar: ActualizarProyectoDto,
    idProyecto: string,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    const accionAc = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    let getAccionAc = accionAc.codigo;

    //Obtiene la entidad traduccion proyecto
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionProyecto,
    );
    let codEntidadTraduccionProyecto = entidad.codigo;

    //Obtiene el estado activo de la traduccion proyecto
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTraduccionProyecto,
    );
    let codEstadoTProyectoActiva = estado.codigo;

    //Obtiene el estado eliminado de la traduccion proyecto
    const estadoTE = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.eliminado,
      codEntidadTraduccionProyecto,
    );
    let codEstadoTProyectoEli = estadoTE.codigo;

    //Entidad proyecto
    const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.proyectos,
    );
    let codEntidadProyecto = entidadN.codigo;

    //Obtiene el estado activo de la entidad proyecto
    const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadProyecto,
    );
    let codEstadoProyecto = estadoN.codigo;

    //Obtiene el estado de foro del proyecto
    const estadoForoProy = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.foro,
      codEntidadProyecto,
    );
    let codEstadoForoProyecto = estadoForoProy.codigo;

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const proyectoOriginal = await this.proyectoModel
        .findOne({
          $and: [
            { _id: idProyecto },
            { perfil: proyectoActualizar.perfil._id },
            { estado: { $ne: estadosProyecto.proyectoEnEjecucion } },
            { estado: { $ne: estadosProyecto.proyectoFinalizado } },
            { estado: { $ne: estadosProyecto.preseleccionado } },
          ],
        })
        .populate({
          path: 'traducciones',
          select: '-fechaActualizacion  -__v ',
          match: { estado: codEstadoTProyectoActiva, original: true },
        })
        .populate({
          path: 'medias',
          select: '-fechaActualizacion  -__v ',
        });


      let idiomaDetectado: any;
      if (proyectoActualizar.traducciones[0].titulo) {
        let textoTraducido = await traducirTexto(
          codIdiom,
          proyectoActualizar.traducciones[0].titulo,
        );
        idiomaDetectado = textoTraducido.idiomaDetectado;
      } else {
        if (proyectoActualizar.traducciones[0].tituloCorto) {
          let textoTraducido = await traducirTexto(
            codIdiom,
            proyectoActualizar.traducciones[0].tituloCorto,
          );
          idiomaDetectado = textoTraducido.idiomaDetectado;
        } else {
          if (proyectoActualizar.traducciones[0].descripcion) {
            let textoTraducido = await traducirTexto(
              codIdiom,
              proyectoActualizar.traducciones[0].descripcion,
            );
            idiomaDetectado = textoTraducido.idiomaDetectado;
          } else {
            const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
              codIdiom,
            );
            idiomaDetectado = idioma.codigo;
          }
        }
      }

      //Obtiene el idioma por el codigo
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idiomaDetectado,
      );
      let codIdioma = idioma.codigo;

      if (proyectoOriginal) {
        if (proyectoActualizar.traducciones.length > 0) {

          const updateTradu = await this.traduccionProyectoModel.updateMany(
            { proyecto: idProyecto },
            { $set: { estado: codEstadoTProyectoEli } },
            opts,
          );

          let newTraduccionProyecto = {
            titulo: proyectoActualizar.traducciones[0].titulo || null,
            tituloCorto: proyectoActualizar.traducciones[0].tituloCorto || null,
            descripcion: proyectoActualizar.traducciones[0].descripcion || null,
            // tags: tags,
            idioma: codIdioma,
            original: true,
            estado: codEstadoTProyectoActiva,
            proyecto: idProyecto,
          };
          const crearTraduccionProyecto = await new this.traduccionProyectoModel(
            newTraduccionProyecto,
          ).save(opts);
          const dataTProyecto = JSON.parse(
            JSON.stringify(crearTraduccionProyecto),
          );
          //delete dataTProyecto.fechaCreacion;
          //delete dataTProyecto.fechaActualizacion;
          delete dataTProyecto.__v;

          await this.proyectoModel.updateOne(
            { _id: idProyecto },
            {
              $push: { traducciones: crearTraduccionProyecto._id },
              fechaActualizacion: new Date(),
            },
            opts,
          );

        }



        let proyectoEstadoForo = false;
        let modificarValorestimado = true;
        if (proyectoOriginal.estado === codEstadoForoProyecto) {
          proyectoEstadoForo = true;
        }

        if (proyectoEstadoForo) {
          let localTime = moment().format('YYYY-MM-DD'); // store localTime
          let fechaFormat = localTime + 'T00:00:00.000Z';
          let formatFechaActual = new Date(fechaFormat);
          formatFechaActual.setDate(new Date(fechaFormat).getDate());
          const getFechaMaxima = await this.modificarValorEstimadoProyectoService.modificarValorEstimadoProyecto();
          if (formatFechaActual > getFechaMaxima.fecha) {
            console.log(
              'ya no puede actualizar el valor estimado falta una semana para que termine el foro',
            );
            modificarValorestimado = false;
          } else {
            console.log(
              'Si puede actualizar el valor estimado aun no pasa los primeros 7 dias de foro',
            );
            modificarValorestimado = true;
          }
        }

        //Verifica si el proyecto esta en estado preseleccionado o espera de fondos
        if (proyectoOriginal.estado === estadosProyecto.proyectoPreEstrategia
          || proyectoOriginal.estado === estadosProyecto.proyectoEnEsperaFondos
          || proyectoOriginal.estado === estadosProyecto.proyectoEnEstrategia) {
          modificarValorestimado = false;
        }

        let setValorEstimado;
        if (modificarValorestimado) {
          if (
            proyectoActualizar.valorEstimado !== undefined &&
            proyectoActualizar.moneda.codNombre !== undefined
          ) {
            if (
              proyectoActualizar.moneda.codNombre !== proyectoOriginal.moneda
            ) {
              setValorEstimado = proyectoActualizar.valorEstimado;
              // let setValorEstimadoFinal = proyectoActualizar.valorEstimadoFinal;
              let valorEstimadoConvertido;
              if (
                proyectoActualizar.moneda.codNombre != listaCodigosMonedas.USD
              ) {
                valorEstimadoConvertido = await this.convertirMontoMonedaService.convertirMoney(
                  proyectoActualizar.valorEstimado,
                  proyectoActualizar.moneda.codNombre,
                  listaCodigosMonedas.USD,
                );
              } else {
                valorEstimadoConvertido = proyectoActualizar.valorEstimado;
              }
              let valorEstimado = {
                valorEstimado: setValorEstimado,
                valorEstimadoFinal: valorEstimadoConvertido,
                moneda: proyectoActualizar.moneda.codNombre,
                fechaActualizacion: new Date(),
              };
              await this.proyectoModel.findByIdAndUpdate(
                idProyecto,
                valorEstimado,
                opts,
              );
            } else {
              setValorEstimado = proyectoActualizar.valorEstimado;
              // let setValorEstimadoFinal = proyectoActualizar.valorEstimadoFinal;
              let valorEstimadoConvertido;
              if (proyectoOriginal.moneda != listaCodigosMonedas.USD) {
                valorEstimadoConvertido = await this.convertirMontoMonedaService.convertirMoney(
                  proyectoActualizar.valorEstimado,
                  proyectoOriginal.moneda,
                  listaCodigosMonedas.USD,
                );
              } else {
                valorEstimadoConvertido = proyectoActualizar.valorEstimado;
              }
              let valorEstimado = {
                valorEstimado: setValorEstimado,
                valorEstimadoFinal: valorEstimadoConvertido,
                fechaActualizacion: new Date(),
              };
              await this.proyectoModel.findByIdAndUpdate(
                idProyecto,
                valorEstimado,
                opts,
              );
            }
          }
        }

        //Comprueba si viene direccion para actualizar
        if (proyectoActualizar.direccion !== undefined) {
          //Comprueba si llega la direccion
          let traducciones: any;
          if (proyectoActualizar.direccion?.traducciones?.length > 0) {
            traducciones = proyectoActualizar.direccion.traducciones;
          } else {
            traducciones = [];
          }
          //Comprueba si llega la localidad
          let localidad: any;
          if (proyectoActualizar.direccion.localidad) {
            localidad = proyectoActualizar.direccion.localidad.codigo;
          } else {
            localidad = null;
          }
          //Objeto para actualizar la direccion
          const dataDireccion = {
            // latitud: proyectoDto.direccion.latitud,
            // longitud: proyectoDto.direccion.longitud,
            latitud: null,
            longitud: null,
            traducciones: traducciones,
            localidad: localidad,
            pais: proyectoActualizar.direccion?.pais?.codigo,
            _id: proyectoOriginal.direccion,
          };
          const actualizarDireccion = await this.actualizarDireccionService.actualizarDireccion(
            dataDireccion,
            opts,
          );
        }

        //Si viene transferenciaActiva actualiza el proyecto
        if (proyectoActualizar.transferenciaActiva) {
          let transferenciaActiva = {
            transferenciaActiva: proyectoActualizar.transferenciaActiva._id,
            fechaActualizacion: new Date(),
          };
          await this.proyectoModel.findByIdAndUpdate(
            idProyecto,
            transferenciaActiva,
            opts,
          );
        }

        // _________________adjuntos_____________________
        let nuevoAlbum;
        let IdUsuario;

        const getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
          proyectoActualizar.perfil._id,
        );
        IdUsuario = getUsuario.usuario._id;

        if (proyectoActualizar.adjuntos.length > 0) {
          for (const album of proyectoActualizar.adjuntos) {
            const dataAlbum: any = {
              nombre: album.nombre,
              tipo: album.tipo.codigo,
              media: album.media,
              usuario: IdUsuario,
              idioma: idiomaDetectado,
            };
            if (album.portada && album.portada._id) {
              dataAlbum.portada = album.portada;
            }
            //// actualiza album
            // if (album._id !== undefined) {
            //   dataAlbum._id = album._id;
            //   await this.actualizarAlbumService.actualizarAlbumconMedia(album._id, dataAlbum);
            // }
            //if (album._id === undefined) {
            //crea el nuevo album
            nuevoAlbum = await this.crearAlbumService.crearAlbum(
              dataAlbum,
              opts,
            );
            //Acatualiza el array de adjuntos
            await this.proyectoModel.updateOne(
              { _id: idProyecto },
              { $push: { adjuntos: nuevoAlbum._id } },
              opts,
            );
            //}
          }
        }
        // _________________medias_____________________
        if (proyectoActualizar.medias.length > 0) {
          for (const media of proyectoActualizar.medias) {
            let idMedia = media._id;
            ////cambia de estado sinAsignar a activa
            await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
              idMedia,
              IdUsuario,
              codIdioma,
              null,
              false,
              opts,
            );

            //Verifica si tiene media
            if (proyectoOriginal.medias.length > 0) {
              const getIdMedia = proyectoOriginal.medias[0]['_id'];
              const getProyecto = await this.proyectoModel
                .findOne({ _id: idProyecto })
                .session(session);
              let listaMedias = [];
              if (getProyecto.medias) {
                //Si tiene rol se le quita los roles que tiene
                for (const getMedia of getProyecto.medias) {
                  listaMedias.push(getMedia['_id'].toString());
                }
                let index = listaMedias.indexOf(getIdMedia);
                //Eliminamos el elemento del array
                listaMedias.splice(index, 1);
              }

              // ______ datos medias _____
              let dataMedias: any = {
                medias: listaMedias,
                fechaActualizacion: new Date(),
              };

              //Se le quita la medias anteriores
              await this.proyectoModel.findByIdAndUpdate(
                idProyecto,
                dataMedias,
                opts,
              );

              //Actualiza el array de medias
              await this.proyectoModel.updateOne(
                { _id: idProyecto },
                { $push: { medias: idMedia }, fechaActualizacion: new Date() },
                opts,
              );
            } else {
              //Acatualiza el array de medias
              await this.proyectoModel.updateOne(
                { _id: idProyecto },
                { $push: { medias: idMedia }, fechaActualizacion: new Date() },
                opts,
              );
            }
          }
          //En la data no viene medias
        } else {
          //Verifica si tiene media el proyecto
          if (proyectoOriginal.medias.length > 0) {
            const getIdMedia = proyectoOriginal.medias[0]['_id'];
            const getProyecto = await this.proyectoModel
              .findOne({ _id: idProyecto })
              .session(session);
            let listaMedias = [];
            if (getProyecto.medias) {
              //Si tiene rol se le quita los roles que tiene
              for (const getMedia of getProyecto.medias) {
                listaMedias.push(getMedia['_id'].toString());
              }
              let index = listaMedias.indexOf(getIdMedia);
              //Eliminamos el elemento del array
              listaMedias.splice(index, 1);
            }

            // ______ datos medias _____
            let dataMedias: any = {
              medias: listaMedias,
              fechaActualizacion: new Date(),
            };

            //Se le quita la medias anteriores
            await this.proyectoModel.findByIdAndUpdate(
              idProyecto,
              dataMedias,
              opts,
            );
          }
        }

        let newHistoricoProyectoUpdate: any = {
          datos: proyectoActualizar,
          usuario: proyectoActualizar.perfil._id,
          accion: getAccionAc,
          entidad: codEntidadProyecto,
        };
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoProyectoUpdate,
        );

        // const updateProyecto = await this.obtenerProyectoUnicoService.obtenerProyectoUnico(idProyecto, codIdiom);
        // return updateProyecto;

        const returnProyecto = await this.proyectoModel
          .findOne({
            $and: [
              { _id: idProyecto },
              { perfil: proyectoActualizar.perfil._id },
            ],
          })
          .session(session)
          .populate({
            path: 'traducciones ',
            select: '-fechaActualizacion  -__v ',
            match: { estado: codEstadoTProyectoActiva, original: true },
          });
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        this.traducirProyectoServiceSegundoPlano.traducirProyectoSegundoPlano(
          returnProyecto._id,
          returnProyecto.perfil,
          idiomaDetectado,
        );
        return returnProyecto;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async activarProyecto(
    perfil: any,
    idProyecto: string,
    opts: any,
  ): Promise<any> {
    try {
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccionModificar = accion.codigo;

      //Obtiene el codigo de la entidad traduccion proyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTradProyecto = entidad.codigo;

      //Obtiene el codigo de la entidad proyectos
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyecto = entidadP.codigo;

      //Obtiene el codigo del estado activa de la entidad proyectos
      const estadoP = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyecto,
      );
      let codEstadoActProyecto = estadoP.codigo;

      //Obtiene el codigo del estado de la entidad proyectos
      const estadoNE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyecto,
      );
      let codEstadoActiProyec = estadoNE.codigo;

      const proyecto = await this.proyectoModel.findOne({
        $and: [
          { _id: idProyecto },
          { perfil: perfil },
          { estado: codigosHibernadoEntidades.proyectoHibernado },
        ],
      });

      if (!proyecto) {
        return null;
      }

      await this.proyectoModel.updateOne(
        { _id: idProyecto },
        { $set: { estado: codEstadoActiProyec } },
        opts,
      );

      const datosModificado = {
        perfil: perfil,
        _id: idProyecto,
        proyecto: await this.proyectoModel
          .findOne({ _id: idProyecto })
          .session(opts.session)
          .select('-fechaActualizacion -fechaCreacion -__v'),
      };

      let newHistoricoProyecto: any = {
        datos: datosModificado,
        usuario: perfil,
        accion: getAccionModificar,
        entidad: codEntidadProyecto,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistoricoProyecto);

      return true;
    } catch (error) {
      throw error;
    }
  }
}
