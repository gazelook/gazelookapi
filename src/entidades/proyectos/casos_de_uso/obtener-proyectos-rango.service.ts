import { Inject, Injectable } from '@nestjs/common';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { TraducirProyectoService } from './traducir-proyecto.service';
import * as mongoose from 'mongoose';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { ObtenerPortadaPredeterminadaProyectoService } from './obtener-portada-predeterminada-proyecto.service';

@Injectable()
export class ObneterProyetoRangoService {
  proyecto: any;

  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirProyectoService: TraducirProyectoService,
    private obtenerPortadaPredeterminadaProyectoService: ObtenerPortadaPredeterminadaProyectoService,
  ) {}

  async obtenerPorRango(
    idioma: string,
    tipoProyecto: string,
    fechaInicial: string,
    fechaFinal: string,
    limite: number,
    pagina: number,
    response?: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };
      //Obtiene el codigo del idioma
      const codIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      const codIdim = codIdioma.codigo;
      //Obtiene la entidad traduccion Proyecto
      const entidadTraduccionProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTradProyecto = entidadTraduccionProyecto.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estadoTradProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradProyecto,
      );
      let codEstadoTradProyecto = estadoTradProyecto.codigo;

      //Obtiene l codigo de la entidad proyectos
      const entidadProyectos = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyectos = entidadProyectos.codigo;

      //Obtiene el estado activo de la entidad proyectos
      const estadoProyectos = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyectos,
      );
      let codEstadoProyectos = estadoProyectos.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad proyectos
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene el codigo de la entidad participante proyecto
      const entidadPartiProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.participanteProyecto,
      );
      let codEntidadPartProyecto = entidadPartiProyecto.codigo;

      //Obtiene el estado activo de la entidad participante proyecto
      const estadoPartiProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadPartProyecto,
      );
      let codEstadoPartiproyecto = estadoPartiProyecto.codigo;

      //Obtiene el codigo de la entidad participante proyecto
      const entidadConfigEstilo = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.configuracionEstilo,
      );
      let codEntidadConfigEstilo = entidadConfigEstilo.codigo;

      //Obtiene el estado activo de la entidad participante proyecto
      const estadoConfigEstilo = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadConfigEstilo,
      );
      let codEstadoConfigEstilo = estadoConfigEstilo.codigo;

      //Obtiene el codigo de la entidad comentarios
      const entidaComentarios = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarios,
      );
      let codEntidadComentarios = entidaComentarios.codigo;

      //Obtiene el estado activo de la entidad coemntarios
      const estadoComentarios = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadComentarios,
      );
      let codEstadoComentarios = estadoComentarios.codigo;

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      //Obtiene el codigo de la entidad traduccion comentarios
      const entidaTradComentarios = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionComentario,
      );
      let codEntidadTradComentarios = entidaTradComentarios.codigo;

      //Obtiene el estado activo de la entidad coemntarios
      const estadoTradComentarios = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradComentarios,
      );
      let codEstadoTradComentarios = estadoTradComentarios.codigo;

      //Obtiene l codigo de la entidad voto proyecto
      const entidaVotoProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.votoProyecto,
      );
      let codEntidadVotoProyecto = entidaVotoProyecto.codigo;

      //Obtiene el estado activo de la entidad voto proyectos
      const estadoVotoProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadVotoProyecto,
      );
      let codEstadoVotoProyecto = estadoVotoProyecto.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      const populateTraduccion = {
        path: 'traducciones',
        select:
          '-fechaActualizacion -fechaCreacion -idioma -__v -estado -proyecto -_id  -original',
        match: {
          idioma: codIdioma.codigo,
          estado: codEstadoTradProyecto,
        },
      };

      // const populateMedias=(
      //   {
      //     path: 'medias',
      //     select: 'principal enlace miniatura catalogoMedia',
      //     match: { estado: codEstadoMedia },
      //     populate: [
      //       {path: 'traducciones', select: 'descripcion', match: { idioma: codIdim } },
      //       {path: 'principal', select: 'url tipo duracion fileDefault fechaActualizacion'}
      //     ]
      //   }
      // )
      const populateAdjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: { estado: codEstadoAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdim, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: ' url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdim, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fileDefault fechaActualizacion',
              },
            ],
          },
        ],
      };
      // const populateParticipantes=({
      //     path: 'participantes',
      //     select: 'configuraciones coautor comentarios roles',
      //     match: { estado: codEstadoPartiproyecto },
      //     populate: [
      //       {
      //         path: 'configuraciones',
      //         select: 'codigo estilos tonoNotificacion tipo',
      //         match: { estado: codEstadoConfigEstilo },
      //         populate:[
      //           {
      //             path:'estilos',
      //             select:'codigo media color tipo',
      //             populate:[
      //               {
      //                 path:'color',
      //                 select:'codigo tipo ',
      //                 populate:{
      //                   path:'tipo',
      //                   select:'codigo nombre formula'
      //                 }
      //               },
      //               {
      //                 path:'tipo', select:'codigo nombre  descripcion'
      //               },
      //               {
      //                 path: 'media',
      //                 match: { estado: codEstadoMedia },
      //                 select: 'principal enlace miniatura catalogoMedia',
      //                 populate: [
      //                       {path: 'traducciones', select: 'descripcion', match: { idioma: codIdim } },
      //                       {path: 'principal', select: ' url tipo duracion fileDefault fechaActualizacion'},
      //                       {path: 'miniatura', select: ' url tipo duracion fileDefault fechaActualizacion'}
      //                 ]
      //               },
      //             ]
      //           },
      //           {
      //             path:'tipo',
      //             select:'codigo nombre'
      //           }
      //       ]
      //       },
      //       {
      //         path: 'coautor', select: '_id nombre nombreContacto',
      //       },
      //       {
      //         path: 'comentarios',
      //         select: 'adjuntos traducciones coautor importante tipo',
      //         match: { estado: codEstadoComentarios },
      //         populate:[
      //           {
      //             path: 'adjuntos',
      //             select: '-fechaActualizacion -fechaCreacion -__v -nombre',
      //             match:{estado:codEstadoAlbum},
      //             populate: {
      //               path: 'media',
      //               match: { estado: codEstadoMedia },
      //               select: 'principal traducciones enlace miniatura catalogoMedia',
      //               populate: [
      //                     {path: 'traducciones', select: 'descripcion', match: { idioma: codIdim } },
      //                     {path: 'principal', select: 'url tipo duracion  fileDefault fechaActualizacion '},
      //                     {path: 'miniatura', select: 'url tipo duracion fileDefault fechaActualizacion '}
      //               ]
      //             }
      //           },
      //           {
      //             path: 'traducciones',
      //             select: 'texto -_id',
      //             match:{estado:codEstadoTradComentarios},
      //           },
      //           {
      //             path: 'coautor', select: '-_id coautor',
      //             populate:{
      //               path: 'coautor', select: '_id nombre nombreContacto',
      //             }
      //           }
      //         ]
      //       },
      //       {
      //         path:'roles',
      //         select:'nombre rol acciones',
      //         populate:{
      //           path:'acciones',
      //           select:'codigo nombre'
      //         }
      //       }

      //     ]
      // })

      // const populateComentarios=(
      //   {
      //     path: 'comentarios',
      //     select: 'adjuntos traducciones coautor importante tipo',
      //     match: { estado: codEstadoComentarios },
      //     populate:[
      //       {
      //         path: 'adjuntos',
      //         select: '-fechaActualizacion -fechaCreacion -__v -nombre',
      //         match:{estado:codEstadoAlbum},
      //         populate: {
      //           path: 'media',
      //           match: { estado: codEstadoMedia },
      //           select: 'principal traducciones enlace miniatura catalogoMedia',
      //           populate: [
      //                 {path: 'traducciones', select: 'descripcion', match: { idioma: codIdim } },
      //                 {path: 'principal', select: 'url tipo duracion  fileDefault fechaActualizacion '},
      //                 {path: 'miniatura', select: 'url tipo duracion fileDefault fechaActualizacion '}
      //           ]
      //         }
      //       },
      //       {
      //         path: 'traducciones',
      //         select: 'texto -_id',
      //         match:{estado:codEstadoTradComentarios},
      //       },
      //       {
      //         path: 'coautor', select: '-_id coautor',
      //         populate:{
      //           path: 'coautor', select: '_id nombre nombreContacto',
      //         }
      //       }
      //     ]
      //   }
      // )
      // const populateVotos=({
      //   path: 'votos',
      //   select: 'perfil',
      //   match:{estado:codEstadoVotoProyecto},
      //   populate: {
      //     path: 'perfil', select: '_id nombre'
      //   }
      // })
      const populatePerfil = {
        path: 'perfil',
        select: 'nombre nombreContacto _id',
      };
      const options = {
        session: session,
        lean: true,
        sort: { titulo: 1 },
        select:
          ' traducciones perfil adjuntos estado fechaCreacion fechaActualizacion',
        populate: [
          populatePerfil,
          populateTraduccion,
          ,
          populateAdjuntos,
          // populateMedias,
          //  populateParticipantes,
          // populateComentarios,
          // populateVotos
        ],
        page: Number(pagina),
        limit: Number(limite),
      };

      const inicio = new Date(fechaInicial);
      fechaFinal = fechaFinal + 'T23:59:59.999Z';
      const final = new Date(fechaFinal);

      const getProyectos = await this.proyectoModel.paginate(
        {
          $and: [
            { tipo: tipoProyecto },
            { fechaCreacion: { $gt: inicio } },
            { fechaCreacion: { $lt: final } },
            { estado: codEstadoProyectos },
          ],
        },
        options,
      );
      let dataReturn = [];
      if (getProyectos.docs.length > 0) {
        let getAdjuntos: any;
        for (const proyecto of getProyectos.docs) {
          let getTraduccion: any;
          //Verifica si el proyecto tiene traduccion en el idioma enviado
          if (proyecto.traducciones.length === 0) {
            //Llama al servicio de traduccion de proyecto
            getTraduccion = await this.traducirProyectoService.TraducirProyecto(
              proyecto._id,
              proyecto.perfil['_id'],
              idioma,
              opts,
            );
          } else {
            //Objeto de tipo traducccion proyecto
            let traduccionProyecto = [];
            let trad = {
              tituloCorto: proyecto.traducciones[0]['tituloCorto'],
              titulo: proyecto.traducciones[0]['titulo'],
            };
            traduccionProyecto.push(trad);
            getTraduccion = traduccionProyecto;
          }

          //Verifica si el proyecto tiene adjuntos
          // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
          getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoService.obtenerPortadaPredeterminadaProyecto(
            proyecto.adjuntos,
            idioma,
            opts,
          );

          //Objeto de proyecto
          let datos = {
            _id: proyecto._id,
            traducciones: getTraduccion,
            adjuntos: getAdjuntos,
            perfil: proyecto.perfil,
            estado: {
              codigo: proyecto.estado,
            },
            fechaCreacion: proyecto['fechaCreacion'],
            fechaActualizacion: proyecto['fechaActualizacion'],
          };
          dataReturn.push(datos);
        }
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        response.set(headerNombre.totalDatos, getProyectos.totalDocs);
        response.set(headerNombre.totalPaginas, getProyectos.totalPages);
        response.set(headerNombre.proximaPagina, getProyectos.hasNextPage);
        response.set(headerNombre.anteriorPagina, getProyectos.hasPrevPage);

        return dataReturn;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return dataReturn;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
