import { ListaArchivosDefaultTipoService } from 'src/entidades/archivo/casos_de_uso/lista-archivos-default-tipo.service';
import {
  codigosCatalogoAlbum,
  codigosArchivosSistema,
} from './../../../shared/enum-sistema';
import { Inject, Injectable } from '@nestjs/common';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';

@Injectable()
export class ObtenerPortadaPredeterminadaProyectoService {
  constructor(
    private listaArchivosDefaultTipoService: ListaArchivosDefaultTipoService,
    private traduccionMediaService: TraduccionMediaService,
  ) {}

  async obtenerPortadaPredeterminadaProyecto(
    adjuntos: any,
    codIdioma: string,
    opts: any,
  ): Promise<any> {
    try {
      let album = [];
      if (adjuntos.length > 0) {
        for (const adjuntoAlbum of adjuntos) {
          let media = [];
          if (adjuntoAlbum.portada) {
            console.log('Si tiene portada: ', adjuntoAlbum._id);
            let obMiniaturaPortada: any;
            if (adjuntoAlbum.portada.miniatura) {
              obMiniaturaPortada = {
                _id: adjuntoAlbum.portada.miniatura._id,
                url: adjuntoAlbum.portada.miniatura.url,
                tipo: {
                  codigo: adjuntoAlbum.portada.miniatura.tipo,
                },
                fileDefault: adjuntoAlbum.portada.miniatura.fileDefault,
                fechaActualizacion:
                  adjuntoAlbum.portada.miniatura.fechaActualizacion,
              };
            }
            let objPortada: any = {
              _id: adjuntoAlbum.portada._id,
              catalogoMedia: {
                codigo: adjuntoAlbum.portada.catalogoMedia,
              },
              principal: {
                _id: adjuntoAlbum.portada.principal._id,
                url: adjuntoAlbum.portada.principal.url,
                tipo: {
                  codigo: adjuntoAlbum.portada.principal.tipo,
                },
                fileDefault: adjuntoAlbum.portada.principal.fileDefault,
                fechaActualizacion:
                  adjuntoAlbum.portada.principal.fechaActualizacion,
              },
              miniatura: obMiniaturaPortada,
              fechaCreacion: adjuntoAlbum.portada.fechaCreacion,
              fechaActualizacion: adjuntoAlbum.portada.fechaActualizacion,
            };

            if (adjuntoAlbum.portada.traducciones[0] === undefined) {
              console.log('debe traducir descripcion media');

              const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                adjuntoAlbum.portada._id,
                codIdioma,
                opts,
              );
              if (traduccionMedia) {
                const traducciones = [
                  {
                    _id: traduccionMedia._id,
                    descripcion: traduccionMedia.descripcion,
                  },
                ];

                objPortada.traducciones = traducciones;
              } else {
                objPortada.traducciones = [];
              }
            } else {
              let arrayTradPortada = [];
              arrayTradPortada.push(adjuntoAlbum.portada.traducciones[0]);
              objPortada.traducciones = arrayTradPortada;
            }

            if (adjuntoAlbum.media.length > 0) {
              for (const getMedia of adjuntoAlbum.media) {
                let obMiniatura: any;
                if (getMedia.miniatura) {
                  obMiniatura = {
                    _id: getMedia.miniatura._id,
                    url: getMedia.miniatura.url,
                    tipo: {
                      codigo: getMedia.miniatura.tipo,
                    },
                    fileDefault: getMedia.miniatura.fileDefault,
                    fechaActualizacion: getMedia.miniatura.fechaActualizacion,
                  };
                }
                let objMedia: any = {
                  _id: getMedia._id,
                  catalogoMedia: {
                    codigo: getMedia.catalogoMedia,
                  },
                  principal: {
                    _id: getMedia.principal._id,
                    url: getMedia.principal.url,
                    tipo: {
                      codigo: getMedia.principal.tipo,
                    },
                    fileDefault: getMedia.principal.fileDefault,
                    fechaActualizacion: getMedia.principal.fechaActualizacion,
                  },
                  miniatura: obMiniatura,
                  fechaCreacion: getMedia.fechaCreacion,
                  fechaActualizacion: getMedia.fechaActualizacion,
                };
                if (getMedia.principal.duracion) {
                  objMedia.principal.duracion = getMedia.principal.duracion;
                }

                if (getMedia.traducciones[0] === undefined) {
                  console.log('debe traducir descripcion media');

                  const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                    getMedia._id,
                    codIdioma,
                    opts,
                  );
                  if (traduccionMedia) {
                    const traducciones = [
                      {
                        _id: traduccionMedia._id,
                        descripcion: traduccionMedia.descripcion,
                      },
                    ];

                    objMedia.traducciones = traducciones;
                  } else {
                    objMedia.traducciones = [];
                  }
                } else {
                  let arrayTradMedia = [];
                  arrayTradMedia.push(getMedia.traducciones[0]);
                  objMedia.traducciones = arrayTradMedia;
                }
                media.push(objMedia);
              }
            }

            album.push({
              _id: adjuntoAlbum._id,
              tipo: {
                codigo: adjuntoAlbum.tipo,
              },
              traducciones: adjuntoAlbum.traducciones,
              predeterminado: adjuntoAlbum.predeterminado,
              portada: objPortada,
              media: media,
            });
          } else {
            console.log('No tiene portada: ', adjuntoAlbum._id);
            // obneter portada defecto
            const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
              codigosArchivosSistema.archivo_default_proyectos,
            );

            if (adjuntoAlbum.media.length > 0) {
              for (const getMedia of adjuntoAlbum.media) {
                let obMiniatura: any;
                if (getMedia.miniatura) {
                  obMiniatura = {
                    _id: getMedia.miniatura._id,
                    url: getMedia.miniatura.url,
                    tipo: {
                      codigo: getMedia.miniatura.tipo,
                    },
                    fileDefault: getMedia.miniatura.fileDefault,
                    fechaActualizacion: getMedia.miniatura.fechaActualizacion,
                  };
                }
                let objMedia: any = {
                  _id: getMedia._id,
                  catalogoMedia: {
                    codigo: getMedia.catalogoMedia,
                  },
                  principal: {
                    _id: getMedia.principal._id,
                    url: getMedia.principal.url,
                    tipo: {
                      codigo: getMedia.principal.tipo,
                    },
                    fileDefault: getMedia.principal.fileDefault,
                    fechaActualizacion: getMedia.principal.fechaActualizacion,
                  },
                  miniatura: obMiniatura,
                  fechaCreacion: getMedia.fechaCreacion,
                  fechaActualizacion: getMedia.fechaActualizacion,
                };
                if (getMedia.principal.duracion) {
                  objMedia.principal.duracion = getMedia.principal.duracion;
                }

                if (getMedia.traducciones[0] === undefined) {
                  console.log('debe traducir descripcion media');

                  const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                    getMedia._id,
                    codIdioma,
                    opts,
                  );
                  if (traduccionMedia) {
                    const traducciones = [
                      {
                        _id: traduccionMedia._id,
                        descripcion: traduccionMedia.descripcion,
                      },
                    ];

                    objMedia.traducciones = traducciones;
                  } else {
                    objMedia.traducciones = [];
                  }
                } else {
                  let arrayTradMedia = [];
                  arrayTradMedia.push(getMedia.traducciones[0]);
                  objMedia.traducciones = arrayTradMedia;
                }

                media.push(objMedia);
              }
            }
            album.push({
              _id: adjuntoAlbum._id,
              tipo: {
                codigo: codigosCatalogoAlbum.catAlbumGeneral,
              },
              traducciones: adjuntoAlbum.traducciones,
              predeterminado: adjuntoAlbum.predeterminado,
              portada: portadaAlbum,
              media: media,
            });
          }
        }
      } else {
        // obtener portada defecto
        const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
          codigosArchivosSistema.archivo_default_proyectos,
        );
        album.push({
          tipo: { codigo: codigosCatalogoAlbum.catAlbumGeneral },
          portada: portadaAlbum,
          predeterminado: true,
        });
      }

      return album;
    } catch (error) {
      throw error;
    }
  }
}
