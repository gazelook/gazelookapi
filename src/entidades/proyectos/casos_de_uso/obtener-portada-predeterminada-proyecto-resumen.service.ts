import { ListaArchivosDefaultTipoService } from 'src/entidades/archivo/casos_de_uso/lista-archivos-default-tipo.service';
import {
  codigosCatalogoAlbum,
  codigosArchivosSistema,
} from './../../../shared/enum-sistema';
import { Inject, Injectable } from '@nestjs/common';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';

@Injectable()
export class ObtenerPortadaPredeterminadaProyectoResumenService {
  constructor(
    private listaArchivosDefaultTipoService: ListaArchivosDefaultTipoService,
    private traduccionMediaService: TraduccionMediaService,
  ) {}

  async obtenerPortadaPredeterminadaProyectoResumen(
    adjuntos: any,
    codIdioma: string,
    opts: any,
  ): Promise<any> {
    try {
      let album = [];
      let adjuntoAlbum: any;
      if (adjuntos.length > 0) {
        adjuntoAlbum = adjuntos.find(
          element => element.tipo == codigosCatalogoAlbum.catAlbumGeneral,
        );
        // for (const adjuntoAlbum of adjuntos) {
        if (adjuntoAlbum) {
          // let media = [];
          if (adjuntoAlbum.portada) {
            console.log('Si tiene portada: ', adjuntoAlbum._id);
            let obMiniaturaPortada: any;
            if (adjuntoAlbum.portada.miniatura) {
              obMiniaturaPortada = {
                _id: adjuntoAlbum.portada.miniatura._id,
                url: adjuntoAlbum.portada.miniatura.url,
                tipo: {
                  codigo: adjuntoAlbum.portada.miniatura.tipo,
                },
                fileDefault: adjuntoAlbum.portada.miniatura.fileDefault,
                fechaActualizacion:
                  adjuntoAlbum.portada.miniatura.fechaActualizacion,
              };

              if (adjuntoAlbum.portada.miniatura.duracion) {
                obMiniaturaPortada.miniatura.duracion =
                  adjuntoAlbum.portada.miniatura.duracion;
              }
            }
            let objPortada: any = {
              _id: adjuntoAlbum.portada._id,
              catalogoMedia: {
                codigo: adjuntoAlbum.portada.catalogoMedia,
              },
              principal: {
                _id: adjuntoAlbum.portada.principal._id,
                url: adjuntoAlbum.portada.principal.url,
                tipo: {
                  codigo: adjuntoAlbum.portada.principal.tipo,
                },
                fileDefault: adjuntoAlbum.portada.principal.fileDefault,
                fechaActualizacion:
                  adjuntoAlbum.portada.principal.fechaActualizacion,
              },
              miniatura: obMiniaturaPortada,
              fechaCreacion: adjuntoAlbum.portada.fechaCreacion,
              fechaActualizacion: adjuntoAlbum.portada.fechaActualizacion,
            };

            if (adjuntoAlbum.portada.traducciones[0] === undefined) {
              const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                adjuntoAlbum.portada._id,
                codIdioma,
                opts,
              );

              if (traduccionMedia) {
                const traducciones = [
                  {
                    _id: traduccionMedia._id,
                    descripcion: traduccionMedia.descripcion,
                  },
                ];

                objPortada.traducciones = traducciones;
              } else {
                objPortada.traducciones = [];
              }
            } else {
              let arrayTradPortada = [];
              arrayTradPortada.push(adjuntoAlbum.portada.traducciones[0]);
              objPortada.traducciones = arrayTradPortada;
            }
            if (adjuntoAlbum.portada.principal.duracion) {
              objPortada.principal.duracion =
                adjuntoAlbum.portada.principal.duracion;
            }

            album.push({
              _id: adjuntoAlbum._id,
              tipo: {
                codigo: adjuntoAlbum.tipo,
              },
              traducciones: adjuntoAlbum.traducciones,
              portada: objPortada,
            });
          } else {
            // obneter portada defecto
            const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
              codigosArchivosSistema.archivo_default_proyectos,
            );

            album.push({
              _id: adjuntoAlbum._id,
              tipo: {
                codigo: codigosCatalogoAlbum.catAlbumGeneral,
              },
              traducciones: adjuntoAlbum.traducciones,
              portada: portadaAlbum,
            });
          }
        } else {
          // obtener portada defecto
          const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
            codigosArchivosSistema.archivo_default_proyectos,
          );
          album.push({
            tipo: { codigo: codigosCatalogoAlbum.catAlbumGeneral },
            portada: portadaAlbum,
          });
        }
      } else {
        // obtener portada defecto
        const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
          codigosArchivosSistema.archivo_default_proyectos,
        );
        album.push({
          tipo: { codigo: codigosCatalogoAlbum.catAlbumGeneral },
          portada: portadaAlbum,
        });
      }

      return album;
    } catch (error) {
      throw error;
    }
  }
}
