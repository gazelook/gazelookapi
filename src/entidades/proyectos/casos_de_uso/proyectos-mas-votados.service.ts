import {
  filtroBusqueda,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';

const mongoose = require('mongoose');

@Injectable()
export class ProyectosMasVotadosService {
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
  ) {}

  async obtenerProyectosMasVotados(
    tipo: string,
    estadoProy: string,
  ): Promise<any> {
    try {
      const totalProyectos = await this.proyectoModel
        .find({ $and: [{ tipo: tipo }, { estado: estadoProy }] })
        .sort('-totalVotos')
        .select('_id perfil valorEstimado valorEstimadoFinal estado moneda');
      return totalProyectos;
    } catch (error) {
      throw error;
    }
  }
}
