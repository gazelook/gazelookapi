import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
const sw = require('stopword');
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { EliminarAlbumService } from '../../album/casos_de_uso/eliminar-album.service';
import { EliminarMediaService } from '../../media/casos_de_uso/eliminar-media.service';
import { ObtenerComentariosService } from '../../comentarios/casos_de_uso/obtener-comentarios-proyecto.service';
import { EliminarComentarioService } from '../../comentarios/casos_de_uso/eliminar-comentario.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { EliminarDireccionService } from 'src/entidades/direccion/casos_de_uso/eliminar-direccion.service';
import { ObtenerProyetoPerfilService } from './obtener-proyecto-perfil.service';
import { erroresGeneral } from 'src/shared/enum-errores';
const mongoose = require('mongoose');

@Injectable()
export class EliminarProyectoUnicoService {
  noticia: any;

  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private eliminarAlbumService: EliminarAlbumService,
    private eliminarMediaService: EliminarMediaService,
    private obtenerComentariosService: ObtenerComentariosService,
    private eliminarComentarioService: EliminarComentarioService,
    private eliminarDireccionService: EliminarDireccionService,
    private obtenerProyetoPerfilService: ObtenerProyetoPerfilService,
  ) {}

  async eliminarProyectoUnico(
    perfil: any,
    idProyecto: string,
    opts?: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    let session;
    let optsLocal;
    if (!opts) {
      session = await mongoose.startSession();
      session.startTransaction();
    }
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      if (!opts) {
        optsLocal = true;
        opts = { session };
      }

      //Obtiene el codigo de la accion eliminar
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      let getAccionEliminar = accion.codigo;

      //Obtiene el codigo de la entidad traduccion proyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTradProyecto = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradProyecto,
      );
      let codEstadoActTradProyecto = estado.codigo;

      //Obtiene el estado eliminado de la entidad traduccion proyecto
      const estadoTE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadTradProyecto,
      );
      let codEstadoTProyectoEli = estadoTE.codigo;

      //Obtiene el codigo de la entidad proyectos
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyecto = entidadP.codigo;

      //Obtiene el codigo del estado activa de la entidad proyectos
      const estadoP = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyecto,
      );
      let codEstadoActProyecto = estadoP.codigo;

      //Obtiene el codigo del estado eliminado de la entidad proyectos
      const estadoNE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadProyecto,
      );
      let codEstadoEliProyec = estadoNE.codigo;

      // ___________ verificar proyecto __________
      const checkEstadoProyecto = await this.obtenerProyetoPerfilService.proyectoNoEliminar(
        idProyecto,
      );
      if (checkEstadoProyecto) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.ERROR_ELIMINAR,
        };
      }

      const proyecto = await this.proyectoModel
        .findOne({
          $and: [
            { _id: idProyecto },
            { perfil: perfil },
            { estado: codEstadoActProyecto },
          ],
        })
        .session(session)
        .populate([{ path: 'adjuntos' }, { path: 'medias' }]);

      if (proyecto) {
        // eliminar album de adjuntos
        for (const albm of proyecto.adjuntos) {
          const dataAlbum = {
            _id: albm['_id'],
            usuario: perfil,
          };
          await this.eliminarAlbumService.eliminarAlbum(dataAlbum, false, opts);
        }

        // eliminar medias del proyecto
        for (const media of proyecto.medias) {
          await this.eliminarMediaService.eliminarMedia(
            media['_id'],
            perfil,
            false,
            opts,
          );
        }

        // eliminar comentarios
        const getComentarios = await this.obtenerComentariosService.obtenerComentariosByIdProyecto(
          proyecto._id,
        );
        for (const comentario of getComentarios) {
          await this.eliminarComentarioService.eliminarComentario(
            proyecto._id,
            perfil,
            comentario._id,
            opts,
          );
        }

        // eliminar direccion
        if (proyecto.direccion) {
          let data = {
            _id: proyecto.direccion,
            usuario: perfil,
          };
          await this.eliminarDireccionService.eliminarDireccion(
            data,
            false,
            opts,
          );
        }

        //Actualiza el estado a eliminado de las traducciones proyecto
        await this.traduccionProyectoModel.updateMany(
          { proyecto: idProyecto },
          {
            $set: {
              estado: codEstadoTProyectoEli,
              fechaActualizacion: new Date(),
            },
          },
          opts,
        );

        let datosDelete = {
          perfil: perfil,
          proyecto: idProyecto,
        };
        let newHistoricoTProyecto: any = {
          datos: datosDelete,
          usuario: perfil,
          accion: getAccionEliminar,
          entidad: codEntidadTradProyecto,
        };
        //Crea el historico del borrado de la traduccion proyecto
        this.crearHistoricoService.crearHistoricoServer(newHistoricoTProyecto);

        //Actualiza el estado a eliminado del proyecto
        await this.proyectoModel.updateOne(
          { _id: idProyecto },
          {
            $set: {
              estado: codEstadoEliProyec,
              fechaActualizacion: new Date(),
            },
          },
          opts,
        );

        const getProyectElimi = await this.proyectoModel
          .findOne({ _id: idProyecto, estado: codEstadoEliProyec })
          .session(opts.session);

        let newHistoricoProyecto: any = {
          datos: getProyectElimi,
          usuario: perfil,
          accion: getAccionEliminar,
          entidad: entidad.codigo,
        };
        //Crea el historico del borrado del proyecto
        this.crearHistoricoService.crearHistoricoServer(newHistoricoProyecto);

        if (optsLocal) {
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
        }
        return true;
      } else {
        if (optsLocal) {
          await session.abortTransaction();
          await session.endSession();
        }
        return proyecto;
      }
    } catch (error) {
      if (optsLocal) {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }

      throw error;
    }
  }

  async hibernarProyecto(
    perfil: any,
    idProyecto: string,
    opts?: any,
  ): Promise<any> {
    let estadoProyecto: any = '';
    let accion: any = '';

    try {
      //Obtiene el codigo de la accion
      const accionEliminar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      //Obtiene el codigo de la entidad proyectos
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyecto = entidadP.codigo;

      //Obtiene el codigo del estado activa de la entidad proyectos
      const estadoP = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadProyecto,
      );
      let codEstadoActProyecto = estadoP.codigo;

      //Obtiene el codigo del estado eliminado de la entidad proyectos
      const estadoNE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadProyecto,
      );
      let codEstadoEliProyec = estadoNE.codigo;

      //Obtiene el codigo del estado HIBERNADO de la entidad proyectos
      const estadoPHiber = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.hibernado,
        codEntidadProyecto,
      );
      let codEstadoHiberProyecto = estadoPHiber.codigo;

      const proyecto = await this.proyectoModel.findOne({
        $and: [
          { _id: idProyecto },
          { perfil: perfil },
          { estado: codEstadoActProyecto },
        ],
      });

      estadoProyecto = codEstadoHiberProyecto;
      accion = accionModificar.codigo;

      if (proyecto) {
        //Actualiza el estado a hibernado del proyecto
        await this.proyectoModel.updateOne(
          { _id: idProyecto },
          { $set: { estado: estadoProyecto, fechaActualizacion: new Date() } },
          opts,
        );

        const getProyectHibernado = await this.proyectoModel
          .findOne({ _id: idProyecto, estado: estadoProyecto })
          .session(opts.session);

        let newHistoricoProyecto: any = {
          datos: getProyectHibernado,
          usuario: perfil,
          accion: accion,
          entidad: codEntidadProyecto,
        };
        //Crea el historico del proyecto
        this.crearHistoricoService.crearHistoricoServer(newHistoricoProyecto);

        return true;
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }
}
