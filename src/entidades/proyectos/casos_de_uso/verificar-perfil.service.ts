import {
  codigoEntidades,
  codigosCatalogoAcciones,
  codigosCatalogoRol,
  codigosRol,
  estadosEstrategia,
  estadosProyecto,
  filtroBusqueda,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';

@Injectable()
export class VerificarPerfilProyectoService {
  getProyectos: any;
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
  ) {}

  async verificarPerfilCoordinador(
    idProyecto: string,
    idPerfil: string,
  ): Promise<any> {
    try {
      let getProyecto = await this.proyectoModel
        .findOne({ _id: idProyecto })
        .populate({
          path: 'participantes',
          select: 'roles coautor',
          match: {
            coautor: idPerfil,
          },
          populate: {
            path: 'roles',
            select: 'rol entidad acciones',
            populate: {
              path: 'acciones',
              select: 'codigo nombre',
            },
          },
        });

      let perfilCordinador = false;
      if (getProyecto) {
        if (getProyecto.participantes.length > 0) {
          for (const getParticipantes of getProyecto.participantes) {
            if (getParticipantes['roles'].length > 0) {
              for (const getRoles of getParticipantes['roles']) {
                if (
                  getRoles.rol == codigosCatalogoRol.coordinadorGazelook &&
                  getRoles.entidad == codigoEntidades.entidadEstrategia
                ) {
                  perfilCordinador = true;
                  break;
                }
              }
            }
          }
        }
      }

      return perfilCordinador;
    } catch (error) {
      throw error;
    }
  }

  async verificarPerfilAdministrador(idPerfil: string): Promise<any> {
    try {
      let getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
        idPerfil,
      );

      let adminGazelook = false;
      //Verifica si el perfil que se envia es usuario administrador del sistema
      if (getUsuario && getUsuario.usuario) {
        if (getUsuario.usuario.rolSistema.length > 0) {
          for (const getRolSistema of getUsuario.usuario.rolSistema) {
            if (getRolSistema) {
              if (getRolSistema.rol === codigosCatalogoRol.administrador ||
                getRolSistema.rol === codigosCatalogoRol.subAdministradorGeneral ||
                getRolSistema.rol === codigosCatalogoRol.coordinadorGazelook) {
                adminGazelook = true;
                break;
              }
            }
          }
        }
      }

      return adminGazelook;
    } catch (error) {
      throw error;
    }
  }
}
