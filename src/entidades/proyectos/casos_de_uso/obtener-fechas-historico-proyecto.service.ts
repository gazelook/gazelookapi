import { Inject, Injectable, HttpStatus } from '@nestjs/common';

import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import { nombreAcciones, nombreEntidades } from 'src/shared/enum-sistema';
import { Historico } from 'src/drivers/mongoose/interfaces/historico/historico.interface';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';

@Injectable()
export class ObtenerFechasHistoricoProyectoService {
  proyecto: any;

  constructor(
    @Inject('HISTORICO_MODEL')
    private readonly historicoModel: PaginateModel<Historico>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  async obtenerFechasHistoricoProyecto(
    idProyecto: string,
    limite: number,
    pagina: number,
    response: any,
    codIdioma: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    try {
      //Obtiene el codigo de la entidad Proyecto
      const entidadProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyecto = entidadProyecto.codigo;

      //Obtiene el codigo de la accion modificar
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getCodAccionModificar = accionModificar.codigo;

      const options = {
        lean: true,
        sort: '-fechaCreacion',
        // populate: [
        //   populateTraduccion,
        //   adjuntos,
        //   medias,
        //   votos
        // ],
        page: Number(pagina),
        limit: Number(limite),
      };

      //Obtener historico en la base de datos
      const historico = await this.historicoModel.paginate(
        {
          'datos._id': idProyecto,
          entidad: codEntidadProyecto,
          accion: getCodAccionModificar,
        },
        options,
      );
      if (historico.docs.length > 0) {
        let arrayHistorico = [];
        for (const getHistorico of historico.docs) {
          let objHistorico = {
            datos: getHistorico.datos,
            fechaActualizacion: getHistorico.fechaCreacion,
          };
          arrayHistorico.push(objHistorico);
        }

        response.set(headerNombre.totalDatos, historico.totalDocs);
        response.set(headerNombre.totalPaginas, historico.totalPages);
        response.set(headerNombre.proximaPagina, historico.hasNextPage);
        response.set(headerNombre.anteriorPagina, historico.hasPrevPage);

        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: arrayHistorico,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const ERROR_OBTENER = await this.i18n.translate(
          codIdioma.concat('.ERROR_OBTENER'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ERROR_OBTENER,
          datos: historico.docs,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (error) {
      throw error;
    }
  }
}
