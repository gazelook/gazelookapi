import {
  filtroBusqueda,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { TraducirProyectoService } from './traducir-proyecto.service';
import { CalcularDiasProyectoService } from './carcular-dias-proyecto.service';
import { ObtenerEntidadNotificacionService } from 'src/entidades/notificacion/casos_de_uso/obtener-entidad-notificacion.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from './obtener-portada-predeterminada-proyecto-resumen.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';

const mongoose = require('mongoose');

@Injectable()
export class ObtenerResumenProyectoService {
  getProyectos: any;
  constructor(
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: PaginateModel<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private traducirProyectoService: TraducirProyectoService,
    private calcularDiasProyectoService: CalcularDiasProyectoService,
    private obtenerEntidadNotificacionService: ObtenerEntidadNotificacionService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
  ) {}
  filtroBus = filtroBusqueda;

  async obtenerResumenProyecto(
    codIdim: string,
    idProyecto: string,
    opts,
  ): Promise<any> {
    try {
      //Obtener el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtener el codigo de la accion crear
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad traduccionProyecto
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTProyecto = estado.codigo;

      //Obtiene el codigo de la entidad proyectos
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadProyecto = entidadP.codigo;

      //Obtiene el estado eliminado de la entidad proyecto
      const estadoP = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadProyecto,
      );
      let codEstadoProyecto = estadoP.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      //Obtiene proyecto
      this.getProyectos = await this.proyectoModel
        .findOne({
          $and: [{ _id: idProyecto }, { estado: { $ne: codEstadoProyecto } }],
        })
        .populate({
          path: 'perfil',
          select: '_id nombre nombreContacto nombreContactoTraducido',
        })
        .populate({
          path: 'votos',
          select: 'perfil',
          populate: { path: 'perfil', select: '_id nombre' },
        })
        .populate({
          path: 'medias',
          select: 'principal enlace miniatura catalogoMedia',
          match: { estado: codEstadoMedia },
          populate: [
            {
              path: 'traducciones',
              select: 'descripcion',
              match: { idioma: codIdioma },
            },
            {
              path: 'principal',
              select: 'url tipo duracion fileDefault fechaActualizacion',
            },
            {
              path: 'miniatura',
              select: 'url tipo duracion fileDefault fechaActualizacion',
            },
          ],
        })
        .populate({
          path: 'adjuntos',
          select: '-fechaActualizacion -fechaCreacion -__v -nombre',
          match: { estado: codEstadoAlbum },
          populate: [
            {
              path: 'media',
              match: { estado: codEstadoMedia },
              select: 'principal enlace miniatura catalogoMedia',
              populate: [
                {
                  path: 'traducciones',
                  select: 'descripcion',
                  match: { idioma: codIdioma },
                },
                {
                  path: 'principal',
                  select: ' url tipo duracion fileDefault fechaActualizacion',
                },
                {
                  path: 'miniatura',
                  select: ' url tipo duracion fileDefault fechaActualizacion',
                },
              ],
            },
            {
              path: 'portada',
              match: { estado: codEstadoMedia },
              select: 'principal enlace miniatura catalogoMedia',
              populate: [
                {
                  path: 'traducciones',
                  select: 'descripcion',
                  match: { idioma: codIdioma },
                },
                {
                  path: 'principal',
                  select: 'url tipo duracion fileDefault fechaActualizacion',
                },
                {
                  path: 'miniatura',
                  select: 'url tipo duracion fileDefault fechaActualizacion',
                },
              ],
            },
          ],
        })
        .populate({
          path: 'traducciones',
          select:
            '-fechaActualizacion -fechaCreacion -idioma -__v -tags -original',
          match: {
            idioma: codIdioma,
            estado: codEstadoTProyecto,
          },
        });

      if (this.getProyectos) {
        const notiFinal = [];

        let actu = false;
        let datos: any;
        let votoBo: any;
        let dias = this.calcularDiasProyectoService.calcularDias(
          new Date(),
          this.getProyectos.fechaActualizacion,
        );
        if (dias) actu = true;

        let notificacion: any;
        let notif: any;

        //Inicia la transaccion
        //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
        const session = await mongoose.startSession();
        session.startTransaction();
        //Datos de adjuntos
        let getAdjuntos: any;
        try {
          //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
          const opts = { session };
          // llamamos al servicio para obtener una portada predeterminada si no devueleve una por defecto
          getAdjuntos = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
            this.getProyectos.adjuntos,
            codIdim,
            opts,
          );

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
        } catch (error) {
          //Aborta la transaccion
          await session.abortTransaction();
          //Finaliza la transaccion
          await session.endSession();
        }

        // if (this.getProyectos.votos.length > 0) {
        //   for (const votos of this.getProyectos.votos) {
        //     if (votos.perfil._id == perfil) {
        //       votoBo = true;
        //       break;
        //     }
        //     else {
        //       votoBo = false;
        //     }
        //   }
        // } else {

        //   votoBo = false;
        // }

        let traduccionProyecto: any;
        if (this.getProyectos.traducciones.length === 0) {
          const idPerfil = this.getProyectos.perfil;
          traduccionProyecto = await this.traducirProyectoService.TraducirProyecto(
            idProyecto,
            idPerfil,
            codIdim,
            opts,
          );
        } else {
          traduccionProyecto = [];
          let trad = {
            tituloCorto: this.getProyectos.traducciones[0]?.tituloCorto,
            titulo: this.getProyectos.traducciones[0]?.titulo,
            descripcion: this.getProyectos.traducciones[0]?.descripcion,
          };
          traduccionProyecto.push(trad);
        }

        let objPerfil = {
          _id: this.getProyectos.perfil._id,
          nombre: this.getProyectos.perfil.nombre,
          nombreContacto: this.getProyectos.perfil.nombreContacto,
          nombreContactoTraducido:
            this.getProyectos.perfil?.nombreContactoTraducido || null,
        };
        datos = {
          _id: this.getProyectos._id,
          traducciones: traduccionProyecto,
          actualizado: actu, //Si el proyecto ha sido actualizaco recientemente =true
          adjuntos: getAdjuntos,
          perfil: objPerfil,
          // listaNotificacion: notificacion,
          // notificacion: notif,
          estado: {
            codigo: this.getProyectos.estado,
          },
          fechaCreacion: this.getProyectos.fechaCreacion,
          fechaActualizacion: this.getProyectos.fechaActualizacion,
        };
        // notiFinal.push(datos)

        return datos;
      } else {
        return this.getProyectos;
      }
    } catch (error) {
      throw error;
    }
  }
  seleccionarFiltro(filtro) {
    switch (filtro) {
      case this.filtroBus.NUEVOS_PROYECTO:
        return 1;
      case this.filtroBus.MENOS_VOTADOS:
        return 2;
      case this.filtroBus.MAS_VOTADOS:
        return 3;
    }
  }
}
