import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
const sw = require('stopword');
import {
  codigosConfiguracionEvento,
  estadoEvento,
} from '../../../shared/enum-sistema';
import { Evento } from 'src/drivers/mongoose/interfaces/evento/evento.interface';

@Injectable()
export class ObtenerFechaFinForoProyectoService {
  noticia: any;

  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
  ) {}

  async obtenerFechaFinForoProyecto(): Promise<any> {
    try {
      //Obtengo el evento
      const getEvento = await this.eventoModel
        .findOne({
          configuracionEvento:
            codigosConfiguracionEvento.eventoForoProyectosCONFEVT_4,
          estado: estadoEvento.pendiente,
        })
        .sort('-fechaActualizacion')
        .limit(1);

      let objData = null;
      if (getEvento) {
        let getFecha = new Date(getEvento.fechaFin);

        objData = {
          evento: {
            _id: getEvento._id,
          },
          fecha: getFecha,
        };
      }
      return objData;
    } catch (error) {
      throw error;
    }
  }
}
