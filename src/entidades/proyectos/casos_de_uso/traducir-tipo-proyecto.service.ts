import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { TraduccionCatalogoTipoProyecto } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_proyecto/traduccion-catalogo-tipo-proyecto.interface';
import { CatalogoTipoProyecto } from 'src/drivers/mongoose/interfaces/catalogo_tipo_proyecto/catalogo-tipo-proyecto.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
const mongoose = require('mongoose');

@Injectable()
export class TraducirCatalogoTipoProyectoService {
  constructor(
    @Inject('CATALOGO_TIPO_PROYECTO_MODEL')
    private readonly catalogoTipoProyectoModel: Model<CatalogoTipoProyecto>,
    @Inject('TRADUCCION_CATALOGO_TIPO_PROYECTO_MODEL')
    private readonly traduccionCatalogoTipoProyectoModel: Model<
      TraduccionCatalogoTipoProyecto
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async TraducirCatalogoTipoProyecto(
    idCatalogoTipoProyecto,
    codigoProyecto,
    nombre,
    descripcion,
    idioma,
    opts,
  ): Promise<any> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene el idioma por el codigo
    const getIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      idioma,
    );
    let codIdioma = getIdioma.codigo;

    //Obtiene la entidad traduccion catalogo tipo proyecto
    const entidadTN = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionCatalogoTipoProyecto,
    );
    let codEntidadTN = entidadTN.codigo;

    //Obtiene el estado  activa de la entidad traduccion catalogo tipo proyecto
    const estadoTN = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTN,
    );
    let codEstadoTN = estadoTN.codigo;

    try {
      //Obtiene la traduccion en el idioma original
      let textoUnido = nombre + ' [-]' + descripcion;

      //llama al metodo de traduccion dinamica
      const textoTrducido = await traducirTexto(idioma, textoUnido);
      const textS = textoTrducido.textoTraducido.split('[-]');

      //datos para guardar la nueva traduccion
      let newTraduccionCatalogoTipoProyecto = {
        referencia: codigoProyecto,
        nombre: textS[0].trim(),
        descripcion: textS[1].trim(),
        idioma: codIdioma,
        original: false,
        estado: codEstadoTN,
      };

      //guarda la nueva traduccion
      const traduccion = new this.traduccionCatalogoTipoProyectoModel(
        newTraduccionCatalogoTipoProyecto,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionTipoProyecto: any = {
        datos: dataTraduccion, //"Creación de traducción de catalogo tipo proyecto. idTraduccionCatalogoTipoProyecto: ".concat(crearTraduccion._id.toString()),
        usuario: '',
        accion: getAccion,
        entidad: codEntidadTN,
      };
      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionTipoProyecto,
      );

      //Actualiza el array de id de traducciones
      await this.catalogoTipoProyectoModel.updateOne(
        { _id: idCatalogoTipoProyecto },
        { $push: { traducciones: crearTraduccion._id } },
        opts,
      );
      return true;
    } catch (error) {
      throw error;
    }
  }
}
