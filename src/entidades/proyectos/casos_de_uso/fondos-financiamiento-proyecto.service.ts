import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { FondosFinanciamiento } from 'src/drivers/mongoose/interfaces/fondos_financiamiento/fondos_financiamiento.interface';
const sw = require('stopword');
import { FondosTipoProyecto } from 'src/drivers/mongoose/interfaces/fondos_tipo_proyecto/fondos_tipo_proyecto.interface';

@Injectable()
export class FondosFinanciamientoProyectoService {
  constructor(
    @Inject('FONDOS_FINANCIAMIENTO_MODEL')
    private readonly fondosFinanciamientoModel: Model<FondosFinanciamiento>,
  ) {}

  async obtenerFondosFinanciamientoProyecto(
    fondosTipoProyecto,
    catalogoPorcentajeFinanciacion,
    opts,
  ): Promise<any> {
    try {
      //Obtengo el evento
      const getFondosFinanciamiento = await this.fondosFinanciamientoModel
        .find({
          fondosTipoProyecto: fondosTipoProyecto,
          catalogoPorcentajeFinanciacion: catalogoPorcentajeFinanciacion,
        })
        .sort('-fechaCreacion')
        .limit(1)
        .session(opts.session);

      return getFondosFinanciamiento[0];
    } catch (error) {
      throw error;
    }
  }
}
