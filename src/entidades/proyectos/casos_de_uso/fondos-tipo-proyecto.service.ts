import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
const sw = require('stopword');
import { FondosTipoProyecto } from 'src/drivers/mongoose/interfaces/fondos_tipo_proyecto/fondos_tipo_proyecto.interface';

@Injectable()
export class FondosTipoProyectoService {
  constructor(
    @Inject('FONDOS_TIPO_PROYECTO_MODEL')
    private readonly fondosTipoProyectoModel: Model<FondosTipoProyecto>,
  ) {}

  async obtenerFondosTipoProyecto(
    catalogoPorcentajeProyecto,
    opts,
  ): Promise<any> {
    try {
      //Obtengo el evento
      const getFondosTipoproyecto = await this.fondosTipoProyectoModel
        .find({ catalogoPorcentajeProyecto: catalogoPorcentajeProyecto })
        .sort('-fechaCreacion')
        .limit(1)
        .session(opts.session);

      return getFondosTipoproyecto[0];
    } catch (error) {
      throw error;
    }
  }
}
