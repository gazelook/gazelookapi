import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
const sw = require('stopword');
const mongoose = require('mongoose');

@Injectable()
export class TraducirProyectoService {
  constructor(
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL')
    private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async TraducirProyecto(
    idProyecto,
    perfil,
    idioma,
    opts?,
    returnIdTrad?,
  ): Promise<any> {
    let options;
    let session;
    if (!opts) {
      //Inicia la transaccion
      //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
      session = await mongoose.startSession();
      session.startTransaction();
    } else {
      options = opts;
    }

    try {
      if (!opts) {
        //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
        options = { session };
      }

      //Obtiene el codigo del idioma
      const getCodIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      let codIdioma = getCodIdioma.codigo;

      //Obtiene el codigo de la accion crear
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene el codigo de la entidad proyectos
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidad = entidad.codigo;

      //Obtiene el codigo del estado activa de la entidad proyectos
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad traduccion proyecto
      const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionProyecto,
      );
      let codEntTra = entidadTra.codigo;

      //Obtiene el codigo del estado activa traduccion proyecto
      const estadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntTra,
      );
      let codEstadoTra = estadoTra.codigo;

      //Obtiene la traduccion en el idioma original
      const getTraductorOriginal = await this.traduccionProyectoModel.findOne({
        proyecto: idProyecto,
        original: true,
        estado: codEstadoTra,
      });

      // let textoUnido = getTraductorOriginal.titulo + ' [-!-]' + getTraductorOriginal.tituloCorto +
      //     ' [-!-]' + getTraductorOriginal.descripcion;

      let textoTrducidoTitulo = await traducirTexto(
        idioma,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let textoTrducidoTituloCorto = await traducirTexto(
        idioma,
        getTraductorOriginal.tituloCorto,
      );
      let tituloCorto = textoTrducidoTituloCorto.textoTraducido;

      let textoTrducidoDescripcion = await traducirTexto(
        idioma,
        getTraductorOriginal.descripcion,
      );
      let descripcion = textoTrducidoDescripcion.textoTraducido;

      // console.log('textoTrducido: ', textoTrducido)
      // const textS = textoTrducido.textoTraducido.split('[-!-]');

      //datos para guardar la nueva traduccion

      // const T = this.gestionProyectoService.detectarIdioma(idioma);
      // const titu = textS[0].trim().split(' ');
      // const tagsTitulo = sw.removeStopwords(titu, T);

      // const tituCort = textS[1].trim().split(' ');

      // const tagsTituloCort = sw.removeStopwords(tituCort, T);

      // const tagsUnidos = tagsTitulo.concat(tagsTituloCort);

      // const tags = tagsUnidos.filter(function (item, index, array) {
      //     return array.indexOf(item) === index;
      //   })

      // console.log('textS[3].trim().split(','): ', textS[3].trim().split(','))
      // console.log('textS[0].trim(): ', textS[0].trim())
      // console.log('textS[1].trim(): ', textS[1].trim())
      // console.log('textS[2].trim(): ', textS[2].trim())
      let newTraduccionProyecto = {
        titulo: titulo,
        tituloCorto: tituloCorto,
        descripcion: descripcion,
        // tags: tags,
        idioma: codIdioma,
        original: false,
        estado: codEstadoTra,
        proyecto: idProyecto,
      };
      //guarda la nueva traduccion
      const traduccion = new this.traduccionProyectoModel(
        newTraduccionProyecto,
      );
      const crearTraduccion = await traduccion.save(options);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      if (!perfil) {
        perfil = null;
      }
      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: getAccion,
        entidad: entidad.codigo,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.proyectoModel.updateOne(
        { _id: idProyecto },
        { $push: { traducciones: dataTraduccion._id } },
        options,
      );

      let returnTraduccion = [];
      let trad = {
        tituloCorto: dataTraduccion.tituloCorto,
        titulo: dataTraduccion.titulo,
        descripcion: dataTraduccion.descripcion,
      };
      returnTraduccion.push(trad);
      if (returnIdTrad) {
        if (!opts) {
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
        }

        return crearTraduccion._id;
      }
      if (!opts) {
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      return returnTraduccion;
    } catch (error) {
      if (!opts) {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      throw error;
    }
  }
}
