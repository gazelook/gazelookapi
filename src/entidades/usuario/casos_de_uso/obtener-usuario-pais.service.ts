import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoLocalidad } from 'src/drivers/mongoose/interfaces/catalogo_localidad/catalogo-localidad.interface';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import {
  estadosPerfil,
  estadosTransaccion,
  estadosUsuario,
} from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerUsuarioPaisService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    @Inject('CATALOGO_LOCALIDAD_MODEL')
    private readonly localidadModel: Model<CatalogoLocalidad>,
  ) {}

  //Obtiene usuarios segun el pais enviado
  async obtenerUsuarioByPais(codPais: string): Promise<any> {
    const usuarios = await this.userModel
      .find({ estado: estadosUsuario.activa })
      .select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'direccion',
        select: 'localidad',
      });

    let arrayUsuarios = [];
    if (usuarios.length > 0) {
      for (const getUsuario of usuarios) {
        let getLocalidad = await this.localidadModel.findOne({
          codigo: getUsuario.direccion['localidad'],
        });
        let objUsuario: any;
        if (getLocalidad.catalogoPais == codPais) {
          objUsuario = {
            _id: getUsuario._id,
          };
          arrayUsuarios.push(objUsuario);
        }
      }
    }
    return arrayUsuarios;
  }
}
