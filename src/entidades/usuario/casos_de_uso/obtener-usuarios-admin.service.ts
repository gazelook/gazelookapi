import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { Funcion } from 'src/shared/funcion';
import { estadosUsuario } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerUsuariosAdminService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private readonly funcion: Funcion,
  ) {}

  async obtenerUsuariosAdmin(): Promise<any> {
    const user = await this.userModel
      .find({
        $and: [
          {
            $or: [
              { estado: estadosUsuario.activaNoVerificado },
              { estado: estadosUsuario.activa },
              { estado: estadosUsuario.bloqueadoSistema },
              { estado: estadosUsuario.noPermitirAcceso },
            ],
          },
        ],
      })
      .populate({
        path: 'perfiles',
      })
      .populate({
        path: 'rolSistema',
        populate: {
          path: 'rolesEspecificos',
          populate: {
            path: 'acciones',
          },
        },
      });

    let arrayUsuario = [];
    let arrayIdUsuarios = [];
    if (user.length > 0) {
      for (const usuario of user) {
        let arrayRolSistema = [];
        if (usuario.rolSistema.length > 0) {
          for (const rolSistema of usuario.rolSistema) {
            // Verifica si los roles administradores
            const getRolAdmin = await this.funcion.rolesAdmin.includes(
              rolSistema['rol'],
            );

            if (getRolAdmin) {
              const getUsuario = await arrayIdUsuarios.includes(usuario._id);
              if (!getUsuario) {
                arrayIdUsuarios.push(usuario._id);
              }
            }
          }
        }
      }
      console.log('arrayIdUsuarios: ', arrayIdUsuarios);
      const usuarios = await this.userModel
        .find({
          _id: { $in: arrayIdUsuarios },
        })
        .populate({
          path: 'perfiles',
          populate: {
            path: 'telefonos',
          },
        })
        .populate({
          path: 'rolSistema',
          populate: {
            path: 'rolesEspecificos',
            populate: {
              path: 'acciones',
            },
          },
        });
      if (usuarios.length > 0) {
        for (const usuario of usuarios) {
          let arrayRolSistema = [];
          if (usuario.rolSistema.length > 0) {
            for (const rolSistema of usuario.rolSistema) {
              let obRolSistema = {
                _id: rolSistema['_id'],
                nombre: rolSistema['nombre'],
                rol: {
                  codigo: rolSistema['rol'],
                },
                // rolesEspecificos: arrayRolesEspecificos
              };
              arrayRolSistema.push(obRolSistema);
            }
          }
          let arrayPerfiles = [];
          if (usuario.perfiles.length > 0) {
            for (const perfil of usuario.perfiles) {
              let arrayTelefono = [];
              if (perfil.telefonos.length > 0) {
                for (const telefono of perfil.telefonos) {
                  let objTelefono = {
                    numero: telefono.numero,
                    pais: {
                      codigo: telefono.pais,
                    },
                  };
                  arrayTelefono.push(objTelefono);
                }
              }
              let obPerfil = {
                _id: perfil._id,
                nombre: perfil.nombre,
                nombreContacto: perfil.nombreContacto,
                telefonos: arrayTelefono,
              };
              arrayPerfiles.push(obPerfil);
            }
          }

          let objUsuario = {
            _id: usuario._id,
            rolSistema: arrayRolSistema,
            email: usuario.email,
            perfiles: arrayPerfiles,
            estado: {
              codigo: usuario.estado,
            },
          };
          arrayUsuario.push(objUsuario);
        }
      }
      return arrayUsuario;
    }
  }
}
