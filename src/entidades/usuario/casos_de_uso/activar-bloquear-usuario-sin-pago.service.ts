import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';

import * as bcrypt from 'bcrypt';

import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombreAcciones,
  nombreEntidades,
  nombrecatalogoEstados,
  codIdiomas,
  codigosTiposPerfil,
} from '../../../shared/enum-sistema';
import { erroresCuenta, erroresPerfil } from '../../../shared/enum-errores';
import {
  ActivarBloquearUsuarioSinPagoDto,
  UsuarioDto,
  UsuarioSinPagoDto,
} from '../dtos/usuario.dto';
import { CrearDireccionDto } from 'src/entidades/direccion/dtos/crear-direccion.dto';
import { CrearDireccionService } from 'src/entidades/direccion/casos_de_uso/crear-direccion.service';
import { Direccion } from 'src/entidades/cuenta/entidad/cuenta.dto';
import * as mongoose from 'mongoose';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import { CrearPerfilService } from 'src/entidades/perfil/casos_de_uso/crear-perfil.service';
import { ContactnameUnicoPerfilService } from 'src/entidades/perfil/casos_de_uso/contactname-unico-perfil.service';
import { CrearTelefonoService } from 'src/entidades/direccion/casos_de_uso/crear-telefono.service';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';

@Injectable()
export class ActivarBloquearUsuarioSinPagoService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  // Post a single user
  async activarBloquearUsuarioSinPago(
    dataUsuario: ActivarBloquearUsuarioSinPagoDto,
    idioma: string,
  ): Promise<any> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion.codigo;

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.usuarios,
      );
      const codEntidad = entidad.codigo;

      const idUsuario = dataUsuario._id;

      const userUpdate = await this.userModel.findOneAndUpdate(
        { _id: idUsuario },
        { $set: { estado: dataUsuario.estado.codigo } },
        opts,
      );

      const dataHistoricoUser = {
        datos: dataUsuario,
        usuario: dataUsuario._id,
        accion: getAccion,
        entidad: codEntidad,
      };

      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoUser);

      // actualizar el nuevo pago
      await session.commitTransaction();
      session.endSession();

      return true;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
