import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { estadosUsuario } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerUsuariosCriptoService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
  ) { }

  async obtenerUsuariosCripto(): Promise<any> {
    const user = await this.userModel
      .find({
        estado: estadosUsuario.cripto
      })
      .populate({
        path: 'perfiles',
      })    

    return user;
  }
}
