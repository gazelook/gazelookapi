import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';

import * as bcrypt from 'bcrypt';

import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombreAcciones,
  nombreEntidades,
  nombrecatalogoEstados,
  codIdiomas,
  codigosTiposPerfil,
} from '../../../shared/enum-sistema';
import { erroresCuenta, erroresPerfil } from '../../../shared/enum-errores';
import { UsuarioDto, UsuarioSinPagoDto } from '../dtos/usuario.dto';
import { CrearDireccionDto } from 'src/entidades/direccion/dtos/crear-direccion.dto';
import { CrearDireccionService } from 'src/entidades/direccion/casos_de_uso/crear-direccion.service';
import { Direccion } from 'src/entidades/cuenta/entidad/cuenta.dto';
import * as mongoose from 'mongoose';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import { CrearPerfilService } from 'src/entidades/perfil/casos_de_uso/crear-perfil.service';
import { ContactnameUnicoPerfilService } from 'src/entidades/perfil/casos_de_uso/contactname-unico-perfil.service';
import { CrearTelefonoService } from 'src/entidades/direccion/casos_de_uso/crear-telefono.service';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { ActualizarDireccionService } from 'src/entidades/direccion/casos_de_uso/actualizar-direccion.service';

@Injectable()
export class ActualizarUsuarioSinPagoService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearDireccionService: CrearDireccionService,
    private gestionRolService: GestionRolService,
    private crearPerfilService: CrearPerfilService,
    private contactnameUnicoPerfilService: ContactnameUnicoPerfilService,
    private crearTelefonoService: CrearTelefonoService,
    private actualizarDireccionService: ActualizarDireccionService,
  ) {}

  // Post a single user
  async actualizarUsuarioSinPago(
    dataUsuario: UsuarioSinPagoDto,
    idioma: string,
  ): Promise<any> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion.codigo;

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.usuarios,
      );
      const codEntidad = entidad.codigo;
      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado.codigo;

      const idUsuario = dataUsuario._id;

      //Si llega contraseña la actualiza
      if (dataUsuario.contrasena) {
        var BCRYPT_SALT_ROUNDS = 12;
        //Encriptar contraseña con BCRYPT
        const hashedContrasena = await bcrypt.hash(
          dataUsuario.contrasena,
          BCRYPT_SALT_ROUNDS,
        );
        dataUsuario.contrasena = hashedContrasena;
        const userUpdate = await this.userModel.findOneAndUpdate(
          { _id: idUsuario },
          { $set: { contrasena: dataUsuario.contrasena } },
          opts,
        );
      }

      //Si llega rol sistema se agrega un nuevo rol

      // if (dataUsuario.rolSistema && dataUsuario.rolSistema.length) {
      let rolAdmin = [];
      if (dataUsuario.rolSistema.length > 0) {
        for (const rol of dataUsuario.rolSistema) {
          const rolSistema = await this.gestionRolService.obtenerRolSistemaByCodRol(
            rol,
          );
          rolAdmin.push(rolSistema[0]._id);
        }
      }

      const userUpdate = await this.userModel.findOneAndUpdate(
        { _id: idUsuario },
        { $set: { rolSistema: rolAdmin } },
        opts,
      );
      // }

      if (dataUsuario.direccion) {
        // let saveDireccionUsuario: any;

        // direccion usuario
        const dataDireccUsuario = dataUsuario.direccion;

        const direccionUsuario: any = {
          _id: dataDireccUsuario._id,
          traducciones: dataDireccUsuario.traducciones,
          //localidad: dataDireccUsuario.localidad.codigo,
          pais: dataDireccUsuario.pais.codigo,
          latitud: null,
          longitud: null,
        };
        let actualizaDire = await this.actualizarDireccionService.actualizarDireccion(
          direccionUsuario,
          opts,
        );
        // saveDireccionUsuario = await this.crearDireccionService.crearDireccion(direccionUsuario, opts);
        // const userUpdate = await this.userModel.findOneAndUpdate(
        //   { _id: idUsuario },
        //   { $set: { direccion: saveDireccionUsuario._id } },
        //   opts
        // );
      }

      //Si envia email se actualiza

      if (dataUsuario.email) {
        // const existUser = await this.userModel.find({
        //   email: dataUsuario.email.toString(),
        // });
        // if (existUser.length === 0) {
        const userUpdate = await this.userModel.findOneAndUpdate(
          { _id: idUsuario },
          { $set: { email: dataUsuario.email } },
          opts,
        );
        // } else {
        //   throw {
        //     codigo: HttpStatus.NOT_FOUND,
        //     codigoNombre: erroresCuenta.USUARIO_YA_REGISTRADO,
        //   };
        // }
      }

      if (dataUsuario.perfiles && dataUsuario.perfiles.length > 0) {
        for (const perfil of dataUsuario.perfiles) {
          console.log('perfil._ID: ', perfil._id);

          if (perfil.nombre) {
            console.log('perfil.nombre: ', perfil.nombre);
            const userUpdate = await this.perfilModel.findOneAndUpdate(
              { _id: perfil._id },
              { $set: { nombre: perfil.nombre } },
              opts,
            );
          }
          if (perfil.nombreContacto) {
            // verificar nombre de contacto de perfil
            const nombreContactoUnico = await this.contactnameUnicoPerfilService.nombreContactoUnico(
              perfil.nombreContacto,
              'false',
            );

            if (!nombreContactoUnico) {
              throw {
                codigo: HttpStatus.NOT_ACCEPTABLE,
                codigoNombre: erroresPerfil.ERROR_NOMBRE_CONTACTO,
              };
            }

            const userUpdate = await this.perfilModel.findOneAndUpdate(
              { _id: perfil._id },
              { $set: { nombreContacto: perfil.nombreContacto } },
              opts,
            );
          }

          if (perfil.telefonos && perfil.telefonos.length > 0) {
            let idTelefono = [];
            // ____________telefono_________________
            const listaTelefono = perfil.telefonos;
            for (let telefono of listaTelefono) {
              const dataTelefono = {
                numero: telefono.numero,
                pais: telefono.pais.codigo,
                usuario: perfil._id,
              };

              const saveTelefono = await this.crearTelefonoService.crearTelefono(
                dataTelefono,
                opts,
              );
              idTelefono.push(saveTelefono._id);
            }
            //guardar referencia idTelefonos en Perfil
            await this.perfilModel.findOneAndUpdate(
              { _id: perfil._id },
              { $set: { telefonos: idTelefono } },
              opts,
            );
          }
        }
      }

      const dataHistoricoUser = {
        datos: dataUsuario,
        usuario: dataUsuario._id,
        accion: getAccion,
        entidad: codEntidad,
      };

      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoUser);

      // actualizar el nuevo pago
      await session.commitTransaction();
      session.endSession();

      return true;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
