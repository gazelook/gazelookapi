import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { ObtenerPerfilUsuarioService } from '../../perfil/casos_de_uso/obtener-perfil-usuario.service';
import { ObtenerIdUsuarioService } from './obtener-id-usuario.service';

@Injectable()
export class RolUsuarioService {
  //private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService;

  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    //private readonly moduleRef: ModuleRef,
    @Inject(forwardRef(() => ObtenerPerfilUsuarioService))
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
  ) {}

  // recibe como parametro el id del PERFIL
  async ObtenerRolUsuarioByidPerfilByCodRolBycodEntidad(
    idPerfil,
    codigoCatalogoRol,
    codigoEntidad,
  ): Promise<any> {
    const perfilUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
      idPerfil,
    );
    let idUsuario = perfilUsuario.usuario._id;
    const rol = await this.userModel
      .findOne({ _id: idUsuario })
      .select('email')
      .populate({
        path: 'rolSistema',
        select: '-fechaCreacion -fechaActualizacion -__v',
        match: {
          rol: codigoCatalogoRol,
        },
        populate: {
          path: 'rolesEspecificos',
          select: '-fechaCreacion -fechaActualizacion -__v',
          match: {
            entidad: codigoEntidad,
          },
          populate: {
            path: 'acciones',
            select: '-fechaCreacion -fechaActualizacion -__v',
          },
        },
      });
    return rol;
  }
  async ObtenerRolUsuarioByidPerfilByCodRol(
    idPerfil,
    codigoCatalogoRol,
  ): Promise<any> {
    const perfilUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
      idPerfil,
    );
    let idUsuario = perfilUsuario.usuario._id;
    const rol = await this.userModel
      .findOne({ _id: idUsuario })
      .select('email')
      .populate({
        path: 'rolSistema',
        select: '-fechaCreacion -fechaActualizacion -__v',
        match: {
          rol: codigoCatalogoRol,
        },
        populate: {
          path: 'rolesEspecificos',
          select: '-fechaCreacion -fechaActualizacion -__v',
          populate: {
            path: 'acciones',
            select: '-fechaCreacion -fechaActualizacion -__v',
          },
        },
      });
    return rol;
  }

  // recibe como parametro el id del USUARIO
  async ObtenerRolUsuarioByidUserByCodRolBycodEntidad(
    idUsuario,
    codigoCatalogoRol,
    codigoEntidad,
  ): Promise<any> {
    const usuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
      idUsuario,
    );
    let idUser = usuario._id;
    const rol = await this.userModel
      .findOne({ _id: idUser })
      .select('email')
      .populate({
        path: 'rolSistema',
        select: '-fechaCreacion -fechaActualizacion -__v',
        match: {
          rol: codigoCatalogoRol,
        },
        populate: {
          path: 'rolesEspecificos',
          select: '-fechaCreacion -fechaActualizacion -__v',
          match: {
            entidad: codigoEntidad,
          },
          populate: {
            path: 'acciones',
            select: '-fechaCreacion -fechaActualizacion -__v',
          },
        },
      });
    return rol;
  }

  async ObtenerRolUsuarioByidUserByCodRol(
    idUsuario,
    codigoCatalogoRol,
  ): Promise<any> {
    const usuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
      idUsuario,
    );
    if (!usuario) {
      return null;
    }
    let idUser = usuario._id;
    const rol = await this.userModel
      .findOne({ _id: idUser })
      .select('email')
      .populate({
        path: 'rolSistema',
        select: '-fechaCreacion -fechaActualizacion -__v',
        match: {
          rol: codigoCatalogoRol,
        },
        populate: {
          path: 'rolesEspecificos',
          select: '-fechaCreacion -fechaActualizacion -__v',
          populate: {
            path: 'acciones',
            select: '-fechaCreacion -fechaActualizacion -__v',
          },
        },
      });
    return rol;
  }
}
