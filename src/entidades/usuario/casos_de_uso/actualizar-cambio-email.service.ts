import { forwardRef, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombreAcciones,
  nombreEntidades,
  nombrecatalogoEstados,
  codigoEntidades,
  estadoNotificacionesFirebase,
  accionNotificacionFirebase,
} from '../../../shared/enum-sistema';

import * as mongoose from 'mongoose';
import { GenerarTokenService } from '../../login/casos_de_uso/generar-token.service';
import { FirebaseNotificacionService } from '../../../drivers/firebase/services/firebase-notificacion.service';
import { FBNotificacion } from '../../../drivers/firebase/models/fb-notificacion.interface';
import { ActualizarEmailUsuarioDto } from '../dtos/actualizar-datos.dto';
import { String } from 'aws-sdk/clients/cloudsearch';
import { ObtenerEmailService } from '../../emails/casos_de_uso/obtener-email.service';
import { erroresGeneral } from '../../../shared/enum-errores';

@Injectable()
export class ActualizarCambioEmailService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private firebaseNotificacionService: FirebaseNotificacionService,
    private generarTokenService: GenerarTokenService,
    private obtenerEmailService: ObtenerEmailService,
  ) {}

  async actualizarEmailCuenta(
    idUsuario: string,
    dataActualizar: ActualizarEmailUsuarioDto,
    tokenEmail: string,
  ): Promise<any> {
    console.log('dataActualizar', dataActualizar);

    const getTokenEmail = await this.obtenerEmailService.obtenerEmailByToken(
      tokenEmail,
    );

    if (!getTokenEmail) {
      throw {
        codigo: HttpStatus.UNAUTHORIZED,
        codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
      };
    }

    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.usuarios,
    );
    const codEntidad = entidad.codigo;
    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };
      // ________________________generar token__________________________
      const generarToken = await this.generarTokenService.generarToken(
        idUsuario,
        dataActualizar.email,
        dataActualizar.idDispositivo,
        opts,
      );

      const token = generarToken.token;
      const idDispositivo = dataActualizar.idDispositivo;

      // ______________________________actualizar datos del usuario_________________________________
      const dataUser = {
        email: dataActualizar.email,
        emailVerificado: dataActualizar.emailVerificado,
      };
      const userUpdate = await this.userModel.findByIdAndUpdate(
        idUsuario,
        dataUser,
        opts,
      );

      let userActualizado: any = await this.userModel
        .findById(idUsuario)
        .session(opts.session);
      delete userActualizado.fechaCreacion;
      delete userActualizado.fechaActualizacion;
      delete userActualizado.__v;

      let newHistoricoUser: any = {
        datos: userActualizado,
        usuario: idUsuario,
        accion: getAccion,
        entidad: codEntidad,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistoricoUser);

      //___________________________notificar cambio firebase_____________________________
      setTimeout(() => {
        const formatData = {
          usuario: {
            _id: idUsuario,
            email: dataActualizar.email,
          },
          dispositivo: idDispositivo.toString(),
          token: token,
        };

        const dataNotificacion: FBNotificacion = {
          idEntidad: idUsuario,
          leido: false,
          codEntidad: codigoEntidades.entidadUsuarios,
          data: formatData,
          estado: estadoNotificacionesFirebase.activa,
          accion: accionNotificacionFirebase.actualizarTokenDispositivos,
          query: `${codigoEntidades.entidadUsuarios}-${false}`,
        };
        this.firebaseNotificacionService.createDataNotificacion(
          dataNotificacion,
          codigoEntidades.entidadUsuarios,
          idUsuario,
        );
      }, 5000);

      await session.commitTransaction();
      session.endSession();

      return true;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
