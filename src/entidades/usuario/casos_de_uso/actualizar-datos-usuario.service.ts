import { Inject, Injectable, HttpStatus, HttpService } from '@nestjs/common';
import { Model } from 'mongoose';

import * as bcrypt from 'bcrypt';

import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombreAcciones,
  nombreEntidades,
  nombrecatalogoEstados,
  estadosPerfil,
  estadosUsuario,
} from '../../../shared/enum-sistema';
import { erroresGeneral } from '../../../shared/enum-errores';
import { Perfil } from '../../../drivers/mongoose/interfaces/perfil/perfil.interface';

import * as mongoose from 'mongoose';

import { CrearEmailActualizarDataEmailService } from '../../emails/casos_de_uso/crear-email-actualizar-data-email.service';
import { ActualizarDatosUsuarioDto } from '../dtos/actualizar-datos.dto';

@Injectable()
export class ActualizarDatosUsuarioService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearEmailActualizarDataEmailService: CrearEmailActualizarDataEmailService,
  ) {}

  async actualizarDatosusuario(
    idUsuario: string,
    dataUsuario: ActualizarDatosUsuarioDto,
  ): Promise<any> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.usuarios,
    );
    const codEntidad = entidad.codigo;
    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      // check user
      const existUser = await this.userModel.findById(idUsuario);
      if (!existUser) {
        throw {
          codigo: HttpStatus.NOT_FOUND,
          codigoNombre: erroresGeneral.ERROR_ACTUALIZAR,
        };
      }

      let dataUpdateUser: any = {};

      // check password
      if (dataUsuario.contrasena || dataUsuario.nuevaContrasena) {
        if (dataUsuario.contrasena && dataUsuario.nuevaContrasena) {
          const equalPassword = await bcrypt.compare(
            dataUsuario.contrasena,
            existUser.contrasena,
          );
          if (!equalPassword) {
            throw {
              codigo: HttpStatus.NOT_FOUND,
              codigoNombre: erroresGeneral.ERROR_CONTRASENIAS,
            };
          }

          //Encriptar contraseña con BCRYPT
          const BCRYPT_SALT_ROUNDS = 12;

          const hashedContrasena = await bcrypt.hash(
            dataUsuario.nuevaContrasena,
            BCRYPT_SALT_ROUNDS,
          );

          dataUpdateUser.contrasena = hashedContrasena;
        } else {
          throw {
            codigo: HttpStatus.CONFLICT,
            codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
          };
        }
      }

      dataUsuario.fechaNacimiento
        ? (dataUpdateUser.fechaNacimiento = dataUsuario.fechaNacimiento)
        : false;

      // update Email
      if (dataUsuario.email) {
        /* dataUpdateUser.email = dataUsuario.email;
        dataUpdateUser.estado = estadosUsuario.activaNoVerificado; */
        dataUpdateUser.emailVerificado = false;

        // enviar email para confirmacion del correo
        await this.crearEmailActualizarDataEmailService.crearEmailActualizarDataEmail(
          idUsuario,
          dataUsuario.email,
          dataUsuario.idDispositivo,
        );
      }

      // actualizar usuario
      let usuarioUpdated;
      if (dataUpdateUser) {
        usuarioUpdated = await this.userModel.findByIdAndUpdate(
          idUsuario,
          dataUpdateUser,
          opts,
        );
      }

      // actualizar nombre en perfiles
      if (dataUsuario.nombre) {
        await this.perfilModel.updateMany(
          {
            $and: [
              { usuario: idUsuario },
              { estado: { $ne: estadosPerfil.eliminado } },
            ],
          },
          { nombre: dataUsuario.nombre },
          opts,
        );
      }

      const dataUser = usuarioUpdated
        ? JSON.parse(JSON.stringify(usuarioUpdated))
        : await this.perfilModel.findOne({ usuario: idUsuario });
      //eliminar parametro no necesarios
      delete dataUser.fechaCreacion;
      delete dataUser.fechaActualizacion;
      delete dataUser.__v;

      const dataHistoricoUser: any = {
        datos: dataUser,
        usuario: idUsuario,
        accion: getAccion,
        entidad: codEntidad,
      };

      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoUser);

      await session.commitTransaction();
      session.endSession();

      return true;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
