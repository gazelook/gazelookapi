import {
  HttpService,
  HttpStatus,
  Inject,
  Injectable,
  Res,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { ConfigService } from '../../../config/config.service';
import { pathServerM } from '../../../shared/enum-servidor-mantenimiento';
import * as mongoose from 'mongoose';

@Injectable()
export class InformacioAplicacionService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private httpService: HttpService,
    private config: ConfigService,
  ) {}

  async allInformacionUsuarioAplicacion(
    idUsuario: string,
    idioma: string,
  ): Promise<any> {
    //verificar id
    if (!mongoose.isValidObjectId(idUsuario)) {
      throw {
        message: 'id no valido',
      };
    }
    const existUser = await this.userModel.findOne({ _id: idUsuario });
    if (!existUser) {
      throw {
        codigo: HttpStatus.NOT_FOUND,
        codigoNombre: 'USUARIO_NO_REGISTRADO',
      };
    }
    const apiKey = this.config.get('API_KEY');
    const URL_SERVER_MANTENIMIENTO = this.config.get(
      'URL_SERVER_MANTENIMIENTO',
    );

    const headersRequest: any = {
      'Content-Type': 'application/json', // afaik this one is not needed
      apiKey: apiKey,
    };
    this.httpService
      .get(
        `${URL_SERVER_MANTENIMIENTO}${pathServerM.SOLICITAR_INFORMACION_USUARIO}/${idUsuario}`,
        { headers: headersRequest },
      )
      .subscribe(data => {
        console.log('ok');
      });
    return true;
  }
}
