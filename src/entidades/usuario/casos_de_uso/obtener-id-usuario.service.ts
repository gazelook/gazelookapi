import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoLocalidad } from 'src/drivers/mongoose/interfaces/catalogo_localidad/catalogo-localidad.interface';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import {
  catalogoOrigen,
  codidosEstadosTraduccionDireccion,
  estadosPerfil,
  estadosTransaccion,
  estadosUsuario,
} from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerIdUsuarioService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    @Inject('CATALOGO_LOCALIDAD_MODEL')
    private readonly localidadModel: Model<CatalogoLocalidad>,
  ) {}

  async obtenerUsuarioByEmail(email: string): Promise<Usuario> {
    const userLogin = await this.userModel
      .findOne({ email: email })
      .select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'perfiles',
        select: '-fechaCreacion -fechaActualizacion',
      });
    return userLogin;
  }

  async obtenerUsuarioById(id: string, opts?: any): Promise<Usuario> {
    let userLogin;
    if (opts) {
      userLogin = await this.userModel
        .findOne({
          $and: [{ _id: id }, { estado: { $ne: estadosUsuario.eliminado } }],
        })
        .populate({
          path: 'perfiles',
          select: '-fechaCreacion -fechaActualizacion',
        })
        .session(opts.session);
    } else {
      userLogin = await this.userModel
        .findOne({
          $and: [{ _id: id }, { estado: { $ne: estadosUsuario.eliminado } }],
        })
        .populate({
          path: 'perfiles',
          select: '-fechaCreacion -fechaActualizacion',
        });
    }

    return userLogin;
  }

  async obtenerUsuarioWithTransaccionByEmail(
    email: string,
    estadoTransaccion = estadosTransaccion.pendiente,
  ): Promise<Usuario> {
    const userLogin = await this.userModel
      .findOne({
        $and: [{ email: email }, { estado: { $ne: estadosUsuario.eliminado } }],
      })
      .select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'transacciones',
        select: '-fechaCreacion -fechaActualizacion',
        match: {
          estado: estadoTransaccion,
        },
        populate: {
          path: 'informacionPago',
        },
      })
      .populate('direccion');
    return userLogin;
  }

  async obtenerUsuarioForRollback(id: string): Promise<any> {
    const userLogin = await this.userModel
      .findOne({ _id: id })
      .select('-fechaCreacion -fechaActualizacion')
      .populate([
        {
          path: 'transacciones',
          select: '-fechaCreacion -fechaActualizacion',
          match: {
            estado: estadosTransaccion.pendiente, // estado pendiente
          },
          populate: {
            path: 'informacionPago',
          },
        },
        {
          path: 'perfiles',
        },
        {
          path: 'suscripciones',
        },
      ]);
    return userLogin;
  }

  async obtenerUsuarioWithTransaccion(id: string, opts?: any): Promise<any> {
    let userLogin;

    if (opts) {
      userLogin = await this.userModel
        .findOne({ _id: id })
        .select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente, // estado pendiente
              origen: catalogoOrigen.suscripciones
            },
            populate: [
              {
                path: 'informacionPago',
              },
            ],
          },
          {
            path: 'perfiles',
          },
        ])
        .session(opts.session);
    } else {
      // consulta sin session
      userLogin = await this.userModel
        .findOne({ _id: id })
        .select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente, // estado pendiente
              origen: catalogoOrigen.suscripciones
            },
            populate: {
              path: 'informacionPago',
            },
          },
          {
            path: 'perfiles',
          },
        ]);
    }
    return userLogin;
  }

  async obtenerUsuarioWithID(id: string, opts?: any): Promise<any> {
    let userLogin;

    if (opts) {
      userLogin = await this.userModel
        .findOne({ _id: id })
        .select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente, // estado pendiente
              // origen: catalogoOrigen.suscripciones,
            },
            populate: [
              {
                path: 'informacionPago',
              },
            ],
          },
          {
            path: 'perfiles',
          },
        ])
        .session(opts.session);
    } else {
      // consulta sin session
      userLogin = await this.userModel
        .findOne({ _id: id })
        .select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente, // estado pendiente
              // origen: catalogoOrigen.suscripciones,
            },
            populate: {
              path: 'informacionPago',
            },
          },
          {
            path: 'perfiles',
          },
        ]);
    }
    return userLogin;
  }
  async obtenerPerfilesUsuarioSessionById(id: string, opts: any): Promise<any> {
    const userLogin = await this.userModel
      .findOne({ _id: id })
      .select(
        'email estado fechaNacimiento perfilGrupo idioma fechaCreacion direccionDomiciliaria documentoIdentidad',
      )
      .populate({
        path: 'perfiles',
        //select: 'nombre nombreContacto tipoPefil estado'
        match: {
          estado: { $ne: estadosPerfil.eliminado },
        },
        populate: [
          {
            path: 'direcciones',
            populate: {
              path: 'traducciones',
              select: 'descripcion traducciones idioma original estado',
              match: {
                estado: codidosEstadosTraduccionDireccion.activa,
                original: true,
              },
            },
          },
          {
            path: 'telefonos',
          },
        ],
      })
      .session(opts.session);
    return userLogin;
  }

  async obtenerPerfilesActivosFromUsuarioById(id: string): Promise<any> {
    const user = await this.userModel
      .findOne({ _id: id })
      .select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'perfiles',
        select: '-fechaCreacion -fechaActualizacion',
        match: {
          estado: { $ne: estadosPerfil.eliminado },
        },
      });
    return user;
  }

  //Obtiene usuarios segun el pais enviado
  async obtenerUsuarioByPais(codPais: string): Promise<any> {
    const usuarios = await this.userModel
      .find({
        $or: [
          { estado: estadosUsuario.activa },
          { estado: estadosUsuario.activaNoVerificado },
        ],
      })
      .select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'direccion',
        select: 'localidad pais',
      });

    let arrayUsuarios = [];
    if (usuarios.length > 0) {
      for (const getUsuario of usuarios) {
        if (getUsuario.direccion && getUsuario.direccion['pais']) {
          // let getLocalidad = await this.localidadModel.findOne({ codigo: getUsuario.direccion['localidad'] })
          let objUsuario: any;
          if (getUsuario.direccion['pais'] === codPais) {
            objUsuario = {
              _id: getUsuario._id,
            };
            arrayUsuarios.push(objUsuario);
          }
        }
      }
    }
    return arrayUsuarios;
  }

  //Obtiene usuarios segun el pais enviado
  async obtenerUsuarioByLocalidad(codLocalidad: string): Promise<any> {
    const usuarios = await this.userModel
      .find({ estado: estadosUsuario.activa })
      .select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'direccion',
        select: 'localidad',
        match: {
          localidad: codLocalidad,
        },
      });

    let arrayUsuarios = [];
    if (usuarios.length > 0) {
      for (const getUsuario of usuarios) {
        if (getUsuario.direccion) {
          let objUsuario: any;
          objUsuario = {
            _id: getUsuario._id,
          };
          arrayUsuarios.push(objUsuario);
        }
      }
    }
    return arrayUsuarios;
  }
}
