import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { ObtenerIntercambioIdService } from 'src/entidades/intercambio/casos_de_uso/obtener-intercambio-id.service';
import { ObtenerNoticiaIdService } from 'src/entidades/noticia/casos_de_uso/obtener-noticia-id.service';
import { ObtenerProyetoIdService } from 'src/entidades/proyectos/casos_de_uso/obtener-proyecto-id.service';
import { estadosUsuario } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerTotalDatosService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private obtenerProyetoIdService: ObtenerProyetoIdService,
    private obtenerNoticiaIdService: ObtenerNoticiaIdService,
    private obtenerIntercambioIdService: ObtenerIntercambioIdService,
  ) {}

  async obtenerdatosTotales(): Promise<any> {
    try {
      const user = await this.userModel.find({
        estado: { $ne: estadosUsuario.eliminado },
      });

      const totalProyectos = await this.obtenerProyetoIdService.obtenerTotalProyectos();
      const totalProyectosMundial = await this.obtenerProyetoIdService.obtenerTotalMundialProyectos();
      const totalProyectosLocal = await this.obtenerProyetoIdService.obtenerTotalLocalProyectos();
      const totalProyectosPais = await this.obtenerProyetoIdService.obtenerTotalPaisProyectos();
      const totalProyectosRed = await this.obtenerProyetoIdService.obtenerTotalRedProyectos();
      const totalNoticias = await this.obtenerNoticiaIdService.obtenerTotalNoticias();
      const totalIntercambios = await this.obtenerIntercambioIdService.obtenerTotalIntercambios();

      let datos = {
        totalUsuarios: user.length,
        totalProyectos: totalProyectos,
        totalProyectosMundial: totalProyectosMundial,
        totalProyectosLocal: totalProyectosLocal,
        totalProyectosPais: totalProyectosPais,
        totalProyectosRed: totalProyectosRed,
        totalNoticias: totalNoticias,
        totalIntercambios: totalIntercambios,
      };

      return datos;
    } catch (error) {
      throw error;
    }
  }
}
