import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { FBNotificacion } from '../../../drivers/firebase/models/fb-notificacion.interface';
import { FirebaseNotificacionService } from '../../../drivers/firebase/services/firebase-notificacion.service';
import { FirebaseProyectosService } from '../../../drivers/firebase/services/firebase-proyectos.service';
import { erroresGeneral, erroresPerfil } from '../../../shared/enum-errores';
import {
  accionNotificacionFirebase,
  codigoEntidades,
  estadoNotificacionesFirebase,
} from '../../../shared/enum-sistema';
import { EliminarDireccionCompletoService } from '../../direccion/casos_de_uso/eliminar-direccion-completo.service';
import { ObtenerEmailService } from '../../emails/casos_de_uso/obtener-email.service';
import { EliminarParticipanteProyectoCompletoService } from '../../participante_proyecto/casos_de_uso/eliminar-participante-proyecto-completo.service';
import { EliminarPerfilCompletoService } from '../../perfil/casos_de_uso/eliminar-perfil-completo.service';
import { EliminarTransaccionCompletoService } from '../../transaccion/casos_de_uso/eliminar-transaccion-completo.service';
import { ObtenerIdUsuarioService } from './obtener-id-usuario.service';
const mongoose = require('mongoose');

@Injectable()
export class EliminarInformacionCompleto {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private eliminarPerfilCompletoService: EliminarPerfilCompletoService,
    private eliminarTransaccionCompletoService: EliminarTransaccionCompletoService,
    private obtenerEmailService: ObtenerEmailService,
    private firebaseNotificacionService: FirebaseNotificacionService,
    private firebaseProyectosService: FirebaseProyectosService,
    private eliminarParticipanteProyectoCompletoService: EliminarParticipanteProyectoCompletoService,
    private eliminarDireccionCompletoService: EliminarDireccionCompletoService,
  ) {}

  async eliminarInformacionCompletoUsuario(usuario, tokenEmail): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };
    const session = await mongoose.startSession(transactionOptions);
    await session.startTransaction();

    let transaccion = false;

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      // const getTokenEmail = await this.obtenerEmailService.obtenerEmailByToken(
      //   tokenEmail,
      // );

      // if (!getTokenEmail || getTokenEmail.usuario.toString() !== usuario) {
      //   throw {
      //     codigo: HttpStatus.UNAUTHORIZED,
      //     codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
      //   };
      // }

      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
        usuario,
      );
      if (!getUsuario) {
        throw {
          codigo: HttpStatus.NOT_FOUND,
          codigoNombre: erroresPerfil.USUARIO_NO_REGISTRADO,
        };
      }
      let resultProyectosEliminar;
      let transaccion1;
      let transaccion2;
      // const transaccion = await session.withTransaction(async () => {

      //______________________________________ Eliminar Pefiles _____________________________________________
      resultProyectosEliminar = await this.eliminarPerfilCompletoService.eliminarPerfilCompleto(
        getUsuario._id,
        opts,
      );

      for (const result of resultProyectosEliminar) {
        if (result.proyectoParticipante.length > 0) {
          for (const participanteproy of result.proyectoParticipante) {
            console.log('participanteproy', participanteproy);
            await this.eliminarParticipanteProyectoCompletoService.eliminarParticipanteProyectoCompletoById(
              participanteproy._id,
              opts,
            );
          }
        }
      }

      await this.eliminarTransaccionCompletoService.eliminarTransaccionCompleto(
        getUsuario._id,
        opts,
      );

      // eliminar direccion del usuario
      await this.eliminarDireccionCompletoService.eliminarDireccionCompleto(
        getUsuario.direccion,
        opts,
      );
      //______________________________________Eliminar Usuarios_____________________________________________
      await this.userModel.deleteOne({ _id: getUsuario._id }, opts);
      // },
      //     transactionOptions
      // );

      // si hay comentarios realizados en otros proyectos
      // if (transaccion && resultProyectosEliminar.length > 0) {
      // transaccion1 = await session.withTransaction(async () => {
      // eliminar comentarios que ha hecho en otros proyectos.
      // for (const result of resultProyectosEliminar) {
      //     if (result.proyectoParticipante.length > 0) {
      //         for (const participanteproy of result.proyectoParticipante) {
      //             console.log("participanteproy", participanteproy);
      //             await this.eliminarParticipanteProyectoCompletoService.eliminarParticipanteProyectoCompletoById(participanteproy._id, opts);
      //         }
      //     }
      // }

      // },
      //     transactionOptions
      // );
      // }

      //if (transaccion1) {
      // transaccion2 = await session.withTransaction(async () => {
      // ____________eliminar transaccion y suscripcion___________________
      // await this.eliminarTransaccionCompletoService.eliminarTransaccionCompleto(getUsuario._id, opts);

      // // eliminar direccion del usuario
      // await this.eliminarDireccionCompletoService.eliminarDireccionCompleto(getUsuario.direccion);
      // //______________________________________Eliminar Usuarios_____________________________________________
      // await this.userModel.deleteOne({ _id: getUsuario._id }, opts);
      // },
      //     transactionOptions
      // );
      //}

      transaccion = true;
      const result = {
        mensaje: 'DATOS_ELIMINADOS',
      };

      if (transaccion) {
        //___________________________Respuesta____________________________

        setTimeout(() => {
          // firebase: eliminar comentarios
          for (const result of resultProyectosEliminar) {
            // eliminar el proyecto de firebase
            if (result.proyecto.length > 0) {
              for (const project of result.proyecto) {
                this.firebaseProyectosService.eliminarComentariosProyecto(
                  project._id.toString(),
                );
              }
            }
            // eliminar comentarios que ha hecho el usuario en otros proyectos en firebase
            if (result.proyectoParticipante.length > 0) {
              for (const participanteproy of result.proyectoParticipante) {
                for (const comentario of participanteproy.comentarios) {
                  this.firebaseProyectosService.eliminarComentario(
                    participanteproy.proyecto.toString(),
                    comentario.toString(),
                  );
                }
              }
            }
          }

          // fierebase: crear notificacion
          const idUsuario = getUsuario._id.toString();
          const formatData = {
            usuario: {
              _id: idUsuario,
              email: getUsuario.email,
            },
          };

          const dataNotificacion: FBNotificacion = {
            idEntidad: idUsuario,
            leido: false,
            codEntidad: codigoEntidades.entidadUsuarios,
            data: formatData,
            estado: estadoNotificacionesFirebase.activa,
            accion: accionNotificacionFirebase.cerrarSesion,
            query: `${codigoEntidades.entidadUsuarios}-${false}`,
          };
          this.firebaseNotificacionService.createDataNotificacion(
            dataNotificacion,
            codigoEntidades.entidadUsuarios,
            idUsuario,
          );
        }, 3000);
      }

      // Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return result;
    } catch (error) {
      console.error(error);
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
      // } finally {
      //     await session.endSession();
    }
  }

  async eliminarUsuarioPerfilCompleto(usuario, direccion, opts): Promise<any> {
    try {
      //______________________________________ Eliminar Pefiles _____________________________________________
      const resultProyectosEliminar = await this.eliminarPerfilCompletoService.eliminarPerfilCompleto(
        usuario,
        opts,
      );

      for (const result of resultProyectosEliminar) {
        if (result.proyectoParticipante.length > 0) {
          for (const participanteproy of result.proyectoParticipante) {
            await this.eliminarParticipanteProyectoCompletoService.eliminarParticipanteProyectoCompletoById(
              participanteproy._id,
              opts,
            );
          }
        }
      }

      //Eliminar la transaccion fisica del usuario
      await this.eliminarTransaccionCompletoService.eliminarTransaccionFisica(
        usuario,
        opts,
      );

      // eliminar direccion del usuario
      await this.eliminarDireccionCompletoService.eliminarDireccionCompleto(
        direccion,
        opts,
      );
      //______________________________________Eliminar Usuarios_____________________________________________
      await this.userModel.deleteOne({ _id: usuario }, opts);

      return true;
    } catch (error) {
      throw error;
    }
  }
}
