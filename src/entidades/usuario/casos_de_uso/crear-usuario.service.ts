import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';

import * as bcrypt from 'bcrypt';

import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombreAcciones,
  nombreEntidades,
  nombrecatalogoEstados,
  estadosUsuario,
} from '../../../shared/enum-sistema';
import { erroresCuenta } from '../../../shared/enum-errores';
import { UsuarioDto } from '../dtos/usuario.dto';

@Injectable()
export class CrearUsuarioService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
  ) { }

  // Post a single user
  async crearUsuario(dataUsuario: UsuarioDto, opts: any): Promise<Usuario> {
    var BCRYPT_SALT_ROUNDS = 12;

    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.usuarios,
    );
    const codEntidad = entidad.codigo;
    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.inactivaPago,
      codEntidad,
    );
    let codEstado
    if (dataUsuario.estado) {
      codEstado = dataUsuario.estado;
    } else {
      codEstado = estado.codigo;
    }


    //Encriptar contraseña con BCRYPT
    const hashedContrasena = await bcrypt.hash(
      dataUsuario.contrasena,
      BCRYPT_SALT_ROUNDS,
    );
    dataUsuario.contrasena = hashedContrasena;

    dataUsuario.estado = codEstado;

    try {
      const existUser = await this.userModel.find({
        $and: [
          { email: dataUsuario.email.toString() },
          { estado: { $ne: estadosUsuario.eliminado } },
        ],
      });

      if (existUser.length === 0) {
        const usuario = new this.userModel(dataUsuario);
        const crearUsuario = await usuario.save(opts);

        const dataUser = JSON.parse(JSON.stringify(crearUsuario));
        //eliminar parametro no necesarios
        delete dataUser.fechaCreacion;
        delete dataUser.fechaActualizacion;
        delete dataUser.__v;

        const dataHistoricoUser = {
          datos: dataUser,
          usuario: crearUsuario._id,
          accion: getAccion,
          entidad: codEntidad,
        };

        // crear el historico de usuario
        this.crearHistoricoService.crearHistoricoServer(dataHistoricoUser);

        return crearUsuario;
      } else {
        throw {
          codigo: HttpStatus.NOT_FOUND,
          codigoNombre: erroresCuenta.USUARIO_YA_REGISTRADO,
        };
      }
    } catch (error) {
      throw error;
    }
  }
}
