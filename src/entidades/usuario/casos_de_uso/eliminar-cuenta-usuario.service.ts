import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { FBNotificacion } from '../../../drivers/firebase/models/fb-notificacion.interface';
import { FirebaseNotificacionService } from '../../../drivers/firebase/services/firebase-notificacion.service';
import { FirebaseProyectosService } from '../../../drivers/firebase/services/firebase-proyectos.service';
import { ParticipanteProyecto } from '../../../drivers/mongoose/interfaces/participante_proyecto/participante_proyecto.interface';
import { Proyecto } from '../../../drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { Suscripcion } from '../../../drivers/mongoose/interfaces/suscripcion/suscripcion.interface';
import { Transaccion } from '../../../drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { erroresGeneral, erroresPerfil } from '../../../shared/enum-errores';
import {
  accionNotificacionFirebase,
  codigoEntidades,
  codigosEstadoSuscripcion,
  estadoNotificacionesFirebase,
  estadosPerfil,
  estadosUsuario,
  idiomas,
} from '../../../shared/enum-sistema';
import { ObtenerEmailService } from '../../emails/casos_de_uso/obtener-email.service';
import { EliminarPerfilService } from '../../perfil/casos_de_uso/eliminar-perfil.service';
import { ObtenerIdUsuarioService } from './obtener-id-usuario.service';
const mongoose = require('mongoose');

@Injectable()
export class EliminarCuentaUsuarioService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    @Inject('SUSCRIPCION_MODEL')
    private readonly suscripcionModel: Model<Suscripcion>,
    @Inject('PARTICIPANTE_PROYECTO_MODEL')
    private readonly participanteProyectoModel: Model<ParticipanteProyecto>,
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private firebaseNotificacionService: FirebaseNotificacionService,
    private firebaseProyectosService: FirebaseProyectosService,
    private eliminarPerfilService: EliminarPerfilService,
    private obtenerEmailService: ObtenerEmailService,
  ) {}

  async eliminarCuentaUsuario(
    usuario: string,
    tokenEmail: string,
  ): Promise<any> {
    //Inicia la transaccion
    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };
    const session = await mongoose.startSession(transactionOptions);
    await session.startTransaction();

    try {
      const opts = { session };
      let transaccion = false;

      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
        usuario,
      );
      if (!getUsuario) {
        throw {
          codigo: HttpStatus.NOT_FOUND,
          codigoNombre: erroresPerfil.USUARIO_NO_REGISTRADO,
        };
      }

      const getTokenEmail = await this.obtenerEmailService.obtenerEmailByToken(
        tokenEmail,
      );

      if (!getTokenEmail || getTokenEmail.usuario.toString() !== usuario) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }

      let proyectosComenCoautor = [];
      // eliminar perfiles
      for (const perfil of getUsuario.perfiles) {
        const dataPerfil = {
          _id: perfil._id,
          estado: {
            codigo: estadosPerfil.eliminado,
          },
        };

        await this.eliminarPerfilService.eliminarPerfil(
          dataPerfil,
          idiomas.ingles,
          opts,
        );

        const proyectosComentCoaut = await this.getProyectosComentariosCoautor(
          perfil._id,
        );
        proyectosComenCoautor = [
          ...proyectosComenCoautor,
          proyectosComentCoaut,
        ];
      }

      // eliminar usuario
      await this.userModel.findByIdAndUpdate(
        getUsuario._id,
        { estado: estadosUsuario.eliminado },
        opts,
      );

      // eliminar suscripcion
      const suscripciones = await this.suscripcionModel.find({
        usuario: getUsuario._id,
      });
      for (const suscripcion of suscripciones) {
        await this.suscripcionModel.findByIdAndUpdate(
          suscripcion._id,
          { estado: codigosEstadoSuscripcion.retiroUsuario },
          opts,
        );
      }

      transaccion = true;

      const result = {
        mensaje: 'DATOS_ELIMINADOS',
        data: true,
      };

      if (transaccion) {
        //___________________________Respuesta____________________________

        setTimeout(() => {
          // firebase: eliminar comentarios
          for (const result of proyectosComenCoautor) {
            // eliminar el proyecto de firebase
            if (result.proyectos.length > 0) {
              for (const project of result.proyectos) {
                this.firebaseProyectosService.eliminarComentariosProyectoEST(
                  project._id.toString(),
                );
              }
            }
            // eliminar comentarios que ha hecho el usuario en otros proyectos en firebase
            if (result.proyectosParticipante.length > 0) {
              for (const participanteproy of result.proyectosParticipante) {
                for (const comentario of participanteproy.comentarios) {
                  this.firebaseProyectosService.eliminarComentarioEST(
                    participanteproy.proyecto.toString(),
                    comentario.toString(),
                  );
                }
              }
            }
          }

          // fierebase: crear notificacion
          const idUsuario = getUsuario._id.toString();
          const formatData = {
            usuario: {
              _id: idUsuario,
              email: getUsuario.email,
            },
          };

          const dataNotificacion: FBNotificacion = {
            idEntidad: idUsuario,
            leido: false,
            codEntidad: codigoEntidades.entidadUsuarios,
            data: formatData,
            estado: estadoNotificacionesFirebase.activa,
            accion: accionNotificacionFirebase.cerrarSesion,
            query: `${codigoEntidades.entidadUsuarios}-${false}`,
          };
          this.firebaseNotificacionService.createDataNotificacion(
            dataNotificacion,
            codigoEntidades.entidadUsuarios,
            idUsuario,
          );
        }, 3000);
      }

      // Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return result;
    } catch (error) {
      console.error(error);
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async getProyectosComentariosCoautor(idPerfil: string): Promise<any> {
    // obtener proyectos del usuario
    const proyectos = await this.proyectoModel
      .find({ perfil: idPerfil })
      .populate({
        path: 'comentarios',
        populate: {
          path: 'traducciones',
        },
      });

    // obtener comentarios de proyectos como coautor
    const comentariosProyCoautor = await this.participanteProyectoModel.find({
      coautor: idPerfil,
    });

    let dataProyectos = {
      proyectos: proyectos,
      proyectosParticipante: comentariosProyCoautor,
    };
    return dataProyectos;
  }
}
