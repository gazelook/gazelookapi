import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';

import * as bcrypt from 'bcrypt';

import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombreAcciones,
  nombreEntidades,
  nombrecatalogoEstados,
  codIdiomas,
  codigosTiposPerfil,
  estadosUsuario,
} from '../../../shared/enum-sistema';
import { erroresCuenta, erroresPerfil } from '../../../shared/enum-errores';
import { UsuarioDto, UsuarioSinPagoDto } from '../dtos/usuario.dto';
import { CrearDireccionDto } from 'src/entidades/direccion/dtos/crear-direccion.dto';
import { CrearDireccionService } from 'src/entidades/direccion/casos_de_uso/crear-direccion.service';
import { Direccion } from 'src/entidades/cuenta/entidad/cuenta.dto';
import * as mongoose from 'mongoose';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import { CrearPerfilService } from 'src/entidades/perfil/casos_de_uso/crear-perfil.service';
import { ContactnameUnicoPerfilService } from 'src/entidades/perfil/casos_de_uso/contactname-unico-perfil.service';
import { CrearTelefonoService } from 'src/entidades/direccion/casos_de_uso/crear-telefono.service';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';

@Injectable()
export class CrearUsuarioSinPagoService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearDireccionService: CrearDireccionService,
    private gestionRolService: GestionRolService,
    private crearPerfilService: CrearPerfilService,
    private contactnameUnicoPerfilService: ContactnameUnicoPerfilService,
    private crearTelefonoService: CrearTelefonoService,
  ) {}

  // Post a single user
  async crearUsuarioSinPago(
    dataUsuario: UsuarioSinPagoDto,
    idioma: string,
  ): Promise<Usuario> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      var BCRYPT_SALT_ROUNDS = 12;

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const getAccion = accion.codigo;

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.usuarios,
      );
      const codEntidad = entidad.codigo;
      // //Obtiene el estado
      // const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      //   nombrecatalogoEstados.activa,
      //   codEntidad,
      // );
      // const codEstado = estado.codigo;

      //Encriptar contraseña con BCRYPT
      const hashedContrasena = await bcrypt.hash(
        dataUsuario.contrasena,
        BCRYPT_SALT_ROUNDS,
      );
      dataUsuario.contrasena = hashedContrasena;

      const idPerfil = new mongoose.mongo.ObjectId();

      let rolAdmin = [];
      if (dataUsuario.rolSistema.length > 0) {
        for (const rol of dataUsuario.rolSistema) {
          const rolSistema = await this.gestionRolService.obtenerRolSistemaByCodRol(
            rol,
          );
          console.log('rolSistema: ', rolSistema[0]);
          rolAdmin.push(rolSistema[0]._id);
        }
      }

      let saveDireccionUsuario: any;

      // direccion usuario
      const dataDireccUsuario = dataUsuario.direccion;

      const direccionUsuario: CrearDireccionDto = {
        traducciones: dataDireccUsuario.traducciones,
        //localidad: dataDireccUsuario.localidad.codigo,
        pais: dataDireccUsuario.pais.codigo,
        latitud: null,
        longitud: null,
      };
      saveDireccionUsuario = await this.crearDireccionService.crearDireccion(
        direccionUsuario,
        opts,
      );

      const perfiles = dataUsuario.perfiles;

      dataUsuario.estado = dataUsuario.estado || estadosUsuario.activa;
      dataUsuario.emailVerificado = true;
      dataUsuario.fechaNacimiento = dataUsuario.fechaNacimiento || null;
      dataUsuario.idioma = dataUsuario.idioma || codIdiomas.ingles;
      dataUsuario.menorEdad = false;
      dataUsuario.perfilGrupo = false;
      dataUsuario.aceptoTerminosCondiciones = true;
      dataUsuario.direccion = saveDireccionUsuario._id;
      dataUsuario.direccionDomiciliaria =
        dataUsuario.direccionDomiciliaria || null;
      dataUsuario.documentoIdentidad = dataUsuario.documentoIdentidad || null;
      dataUsuario.email = dataUsuario.email;
      dataUsuario.rolSistema = rolAdmin;
      dataUsuario.perfiles = [];

      const existUser = await this.userModel.find({
        email: dataUsuario.email.toString(),
      });

      // verificar nombre de contacto de perfil
      for (const perfil of dataUsuario.perfiles) {
        const nombreContactoUnico = await this.contactnameUnicoPerfilService.nombreContactoUnico(
          perfil.nombreContacto,
          'false',
        );

        if (!nombreContactoUnico) {
          throw {
            codigo: HttpStatus.NOT_ACCEPTABLE,
            codigoNombre: erroresPerfil.ERROR_NOMBRE_CONTACTO,
          };
        }
      }

      if (existUser.length === 0) {
        console.log('nombreContactoUnico: ');
        const usuario = new this.userModel(dataUsuario);
        const crearUsuario = await usuario.save(opts);

        const dataUser = JSON.parse(JSON.stringify(crearUsuario));
        //eliminar parametro no necesarios
        delete dataUser.fechaCreacion;
        delete dataUser.fechaActualizacion;
        delete dataUser.__v;

        const dataHistoricoUser = {
          datos: dataUser,
          usuario: crearUsuario._id,
          accion: getAccion,
          entidad: codEntidad,
        };

        // crear el historico de usuario
        this.crearHistoricoService.crearHistoricoServer(dataHistoricoUser);

        console.log('perfiles: ', perfiles);
        let idPerfiles = [];
        let idTelefono = [];
        for (const perfil of perfiles) {
          const dataPerfil = {
            tipoPerfil: codigosTiposPerfil.tipoPerfilClasico,
            usuario: crearUsuario._id,
            nombreContacto: perfil.nombreContacto,
            nombreContactoTraducido: null,
            nombre: perfil.nombre,
            album: [],
            direcciones: [saveDireccionUsuario._id],
            telefonos: [],
            proyectos: [],
            pensamientos: [],
            noticias: [],
            asociaciones: [],
          };
          //guardar perfil
          const savePerfil = await this.crearPerfilService.crearPerfil(
            dataPerfil,
            null,
            opts,
          );

          idPerfiles.push(savePerfil._id);

          // ____________telefono_________________
          const listaTelefono = perfil.telefonos;
          if (listaTelefono.length > 0) {
            for (let telefono of listaTelefono) {
              const dataTelefono = {
                numero: telefono.numero,
                pais: telefono.pais.codigo,
                usuario: savePerfil._id,
              };

              const saveTelefono = await this.crearTelefonoService.crearTelefono(
                dataTelefono,
                opts,
              );
              idTelefono.push(saveTelefono._id);
            }
            //guardar referencia idTelefonos en Perfil
            await this.perfilModel.findOneAndUpdate(
              { _id: savePerfil._id },
              { $set: { telefonos: idTelefono } },
              opts,
            );
          }
        }

        //guardar referencia idPerfiles en Usuario
        const userOk = await this.userModel.findOneAndUpdate(
          { _id: crearUsuario._id },
          { $set: { perfiles: idPerfiles } },
          opts,
        );

        // actualizar el nuevo pago
        await session.commitTransaction();
        session.endSession();

        return crearUsuario;
      } else {
        throw {
          codigo: HttpStatus.NOT_FOUND,
          codigoNombre: erroresCuenta.USUARIO_YA_REGISTRADO,
        };
      }
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
