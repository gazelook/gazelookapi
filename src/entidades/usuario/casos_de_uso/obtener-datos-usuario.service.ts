import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { estadosUsuario } from 'src/shared/enum-sistema';

@Injectable()
export class ObtenerDatosUsuarioService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
  ) {}

  async obtenerPerfilesUsuario(usuario: string, codigoIdioma): Promise<any> {
    const user: any = await this.userModel
      .findById(usuario)
      .select('email estado')
      .populate({
        path: 'perfiles',
        select: '-fechaCreacion -fechaActualizacion -__v',
        populate: [
          {
            path: 'album',
            select: '-fechaCreacion -fechaActualizacion -__v',
            populate: [
              {
                path: 'idMedia',
                select:
                  '-fechaCreacion -fechaActualizacion -__v -catalogoMedia',
                populate: [
                  {
                    path: 'traduccionMedia',
                    select: 'idioma descripcion',
                    match: { idioma: codigoIdioma },
                  },
                  {
                    path: 'principal',
                    select: 'url',
                  },
                  {
                    path: 'miniatura',
                    select: 'url duracion fechaActualizacion',
                  },
                ],
              },
              {
                path: 'portada',
                select:
                  '-fechaCreacion -fechaActualizacion -__v -catalogoMedia',
                populate: [
                  {
                    path: 'traduccionMedia',
                    select: 'idioma descripcion',
                    match: { idioma: codigoIdioma },
                  },
                  {
                    path: 'principal',
                    select: 'url',
                  },
                  {
                    path: 'miniatura',
                    select: 'url',
                  },
                ],
              },
            ],
          },
          {
            path: 'direcciones',
            select: '-fechaCreacion -fechaActualizacion -__v',
          },
          {
            path: 'telefonos',
            select: '-fechaCreacion -fechaActualizacion -__v',
          },
        ],
      });

    return user;
  }

  async obtenerEmailUsuarios(): Promise<any> {
    const emails: any = await this.userModel
      .find({ estado: estadosUsuario.activa })
      .select('email');
    return emails;
  }
}
