import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { erroresGeneral } from '../../../shared/enum-errores';
import { estadosUsuario } from '../../../shared/enum-sistema';
import * as bcrypt from 'bcrypt';

@Injectable()
export class ObtenerUsuarioLoginService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
  ) {}

  async encontrarUsuario(emailUsuario: string): Promise<Usuario> {
    const user = await this.userModel.findOne({
      $and: [
        {
          $or: [
            { estado: estadosUsuario.activaNoVerificado },
            { estado: estadosUsuario.activa },
            { estado: estadosUsuario.inactivaPago },
            { estado: estadosUsuario.rechazadoResponsable },
            { estado: estadosUsuario.cripto },
            { estado: estadosUsuario.inactivaPagoPaymentez },
          ],
        },
        {email:{'$regex' : '^'.concat(emailUsuario).concat('$'), '$options' : 'i'}}
      ],
    });

    return user;
  }

  async verificaPasswordUser(idUsuario, password): Promise<any> {
    try {
      const user = await this.userModel.findById(idUsuario);

      if (user) {
        const equalPassword = await bcrypt.compare(password, user.contrasena);
        if (equalPassword) {
          return true;
        } else {
          throw {
            codigo: HttpStatus.NOT_FOUND,
            codigoNombre: erroresGeneral.ERROR_CONTRASENIAS,
          };
        }
      } else {
        throw {
          codigo: HttpStatus.NOT_FOUND,
          codigoNombre: erroresGeneral.ERROR_OBTENER,
        };
      }
    } catch (error) {
      throw error;
    }
  }
}
