import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { estadosUsuario } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerUsuariosSinPagoervice {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
  ) {}

  async obtenerUsuariosSinPago(): Promise<any> {
    const user = await this.userModel
      .find({
        $and: [
          {
            $or: [{ estado: estadosUsuario.noPermitirAcceso }],
          },
        ],
      })
      .populate({
        path: 'perfiles',
      })
      .populate({
        path: 'rolSistema',
        populate: {
          path: 'rolesEspecificos',
          populate: {
            path: 'acciones',
          },
        },
      });

    let arrayIdUsuarios = [];
    if (user.length > 0) {
      for (const usuario of user) {
        if (usuario.rolSistema.length === 0) {
          arrayIdUsuarios.push(usuario._id);
        }
      }
      const usuarios = await this.userModel.find({
        _id: { $in: arrayIdUsuarios },
      });

      return usuarios;
    }
    return user;
  }
}
