import { CatalogosServiceModule } from './../../catalogos/casos_de_uso/catalogos-services.module';

import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { usuarioProviders } from '../drivers/usuario.provider';
import { CrearUsuarioService } from './crear-usuario.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { historicoProviders } from 'src/entidades/historico/drivers/historico.provider';
import { ObtenerUsuarioLoginService } from './obtener-usuario-login.service';
import { ObtenerIdUsuarioService } from './obtener-id-usuario.service';

import { CrearEmailService } from '../../emails/casos_de_uso/crear-email.service';
import { emailProviders } from '../../emails/drivers/email.provider';
import { CatalogoProviders } from 'src/entidades/catalogos/drivers/catalogo.provider';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { ObtenerDatosUsuarioService } from './obtener-datos-usuario.service';
import { ActualizarUsuarioService } from './actualizar-usuario.service';
import { RolServiceModule } from '../../rol/casos_de_uso/rol.services.module';
import { RolProviders } from '../../rol/drivers/rol.provider';
import { RolUsuarioService } from './rol-usuario.service';
import { PerfilServiceModule } from '../../perfil/casos_de_uso/perfil.services.module';
import { EmailunicoService } from './email-unico.service';
import { RecuperarContraseniaService } from './recuperar-contrasena.service';
import { ConfirmacionRecuperarContraseniaService } from 'src/entidades/emails/casos_de_uso/crear-email-confirmacion-recuperar-contrasenia.service';
import { EmailServicesModule } from 'src/entidades/emails/casos_de_uso/email-services.module';
import { InformacioAplicacionService } from './informacion-aplicacion.service';
import { ArchivoServicesModule } from '../../archivo/casos_de_uso/archivo-services.module';
import { HttpErrorFilter } from '../../../shared/filters/http-error.filter';
import { EliminarInformacionCompleto } from './eliminar-informacion-completo.service';
import { TransaccionServicesModule } from '../../transaccion/casos_de_uso/transaccion-services.module';
import { ObtenerUsuarioPaisService } from './obtener-usuario-pais.service';
import { ActualizarDatosUsuarioService } from './actualizar-datos-usuario.service';
import { ActualizarCambioEmailService } from './actualizar-cambio-email.service';
import { FirebaseModule } from '../../../drivers/firebase/firebase.module';
import { LoginServicesModule } from '../../login/casos_de_uso/login-services.module';
import { ParticipanteProyectoServicesModule } from '../../participante_proyecto/casos_de_uso/participante-proyecto-services.module';
import { DireccionServiceModule } from '../../direccion/casos_de_uso/direccion.services.module';
import { CrearUsuarioSinPagoService } from './crear-usuario-sin-pago.service';
import { ObtenerUsuariosAdminService } from './obtener-usuarios-admin.service';
import { Funcion } from 'src/shared/funcion';
import { ObtenerUsuariosAdminByEntidadService } from './obtener-usuarios-admin-by-entidad.service';
import { ActualizarUsuarioSinPagoService } from './actualizar-usuario-sin-pago.service';
import { ActivarBloquearUsuarioSinPagoService } from './activar-bloquear-usuario-sin-pago.service';
import { EliminarCuentaUsuarioService } from './eliminar-cuenta-usuario.service';
import { EliminarUsuarioSinPagoService } from './eliminar-usuario-sin-pago.service';
import { ObtenerUsuariosSinPagoervice } from './obtener-usuarios-sin-pago.service';
import { RollbackCreacionCuentaPaymentezService } from './rollback-creacion-cuenta-paymentez.service';
import { StorageModule } from 'src/drivers/amazon_s3/storage.module';
import { AlbumServiceModule } from 'src/entidades/album/casos_de_uso/album.services.module';
import { ObtenerTotalDatosService } from './obtener-total-datos.service';
import { ObtenerProyetoIdService } from 'src/entidades/proyectos/casos_de_uso/obtener-proyecto-id.service';
import { ObtenerNoticiaIdService } from 'src/entidades/noticia/casos_de_uso/obtener-noticia-id.service';
import { ObtenerIntercambioIdService } from 'src/entidades/intercambio/casos_de_uso/obtener-intercambio-id.service';
import { ObtenerUsuariosCriptoService } from './obtener-usuarios-cripto.service';

@Module({
  imports: [
    DBModule,
    HttpModule,
    CatalogosServiceModule,
    RolServiceModule,
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => EmailServicesModule),
    forwardRef(() => ArchivoServicesModule),
    forwardRef(() => TransaccionServicesModule),
    forwardRef(() => LoginServicesModule),
    FirebaseModule,
    ParticipanteProyectoServicesModule,
    DireccionServiceModule,
    AlbumServiceModule,
    StorageModule,
  ],
  providers: [
    ...usuarioProviders,
    ...emailProviders,
    ...CatalogoProviders,
    Funcion,
    HttpErrorFilter,
    CrearUsuarioService,
    CrearUsuarioService,
    ObtenerIdUsuarioService,
    ObtenerUsuarioLoginService,
    CrearEmailService,
    CatalogoAccionService,
    CatalogoEntidadService,
    CatalogoEstadoService,
    ObtenerDatosUsuarioService,
    ActualizarUsuarioService,
    RolUsuarioService,
    EmailunicoService,
    RecuperarContraseniaService,
    ConfirmacionRecuperarContraseniaService,
    InformacioAplicacionService,
    EliminarInformacionCompleto,
    ObtenerUsuarioPaisService,
    ActualizarDatosUsuarioService,
    ActualizarCambioEmailService,
    CrearUsuarioSinPagoService,
    ObtenerUsuariosAdminService,
    ObtenerUsuariosAdminByEntidadService,
    ActualizarUsuarioSinPagoService,
    ActivarBloquearUsuarioSinPagoService,
    EliminarCuentaUsuarioService,
    EliminarUsuarioSinPagoService,
    ObtenerUsuariosSinPagoervice,
    RollbackCreacionCuentaPaymentezService,
    ObtenerTotalDatosService,
    ObtenerProyetoIdService,
    ObtenerNoticiaIdService,
    ObtenerIntercambioIdService,
    ObtenerUsuariosCriptoService
  ],
  exports: [
    CrearUsuarioService,
    ObtenerIdUsuarioService,
    ObtenerUsuarioLoginService,
    ObtenerUsuarioLoginService,
    CrearEmailService,
    ObtenerDatosUsuarioService,
    ActualizarUsuarioService,
    RolUsuarioService,
    EmailunicoService,
    RecuperarContraseniaService,
    ConfirmacionRecuperarContraseniaService,
    InformacioAplicacionService,
    EliminarInformacionCompleto,
    ObtenerUsuarioPaisService,
    ActualizarDatosUsuarioService,
    ActualizarCambioEmailService,
    CrearUsuarioSinPagoService,
    ObtenerUsuariosAdminService,
    ObtenerUsuariosAdminByEntidadService,
    ActualizarUsuarioSinPagoService,
    ActivarBloquearUsuarioSinPagoService,
    EliminarCuentaUsuarioService,
    EliminarUsuarioSinPagoService,
    ObtenerUsuariosSinPagoervice,
    RollbackCreacionCuentaPaymentezService,
    ObtenerTotalDatosService,
    ObtenerProyetoIdService,
    ObtenerNoticiaIdService,
    ObtenerIntercambioIdService,
    ObtenerUsuariosCriptoService
  ],
  controllers: [],
})
export class UsuarioServicesModule {}
