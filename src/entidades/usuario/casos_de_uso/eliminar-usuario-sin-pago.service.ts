import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { EliminarPerfilService } from 'src/entidades/perfil/casos_de_uso/eliminar-perfil.service';
import {
  estadosPerfil,
  estadosUsuario,
  idiomas,
} from '../../../shared/enum-sistema';
import { ObtenerIdUsuarioService } from './obtener-id-usuario.service';
const mongoose = require('mongoose');

@Injectable()
export class EliminarUsuarioSinPagoService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private eliminarPerfilService: EliminarPerfilService,
  ) {}

  async eliminarCuentaUsuario(usuario: string): Promise<any> {
    //Inicia la transaccion
    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };
    const session = await mongoose.startSession(transactionOptions);
    await session.startTransaction();

    try {
      const opts = { session };

      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
        usuario,
      );
      if (getUsuario) {
        // eliminar perfiles
        for (const perfil of getUsuario.perfiles) {
          const dataPerfil = {
            _id: perfil._id,
            estado: {
              codigo: estadosPerfil.eliminado,
            },
          };

          await this.eliminarPerfilService.eliminarPerfil(
            dataPerfil,
            idiomas.ingles,
            opts,
          );
        }

        // eliminar usuario
        await this.userModel.findByIdAndUpdate(
          getUsuario._id,
          { estado: estadosUsuario.eliminado },
          opts,
        );

        const result = {
          mensaje: 'DATOS_ELIMINADOS',
          data: true,
        };

        // Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return result;
      } else {
        //Finaliza la transaccion
        await session.endSession();
        return true;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
