import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { estadosUsuario } from '../../../shared/enum-sistema';
import { RollbackCreacionCuentaPaymentezService } from './rollback-creacion-cuenta-paymentez.service';
// import { EliminarUsuarioInactivoPagoService } from "./eliminar-usuario-inactivo-pago.service";

@Injectable()
export class EmailunicoService {
  // validar_email = /^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,4})+$/;
  validar_email = /^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/;

  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private rollbackCreacionCuentaPaymentezService: RollbackCreacionCuentaPaymentezService,
  ) {}

  async emailUnico(email: string, idioma: string) {
    if (!this.validar_email.test(email)) {
      throw {
        codigo: HttpStatus.NOT_FOUND,
        codigoNombre: 'EMAIL_NO_VALIDO',
      };
    }
    const user = await this.userModel.findOne({
      $and: [{ email: email }, { estado: { $ne: estadosUsuario.eliminado } }],
    });

    const userInactivoPago = await this.userModel.findOne({
      $and: [{ email: email }, { estado: estadosUsuario.inactivaPagoPaymentez }],
    });
    if (userInactivoPago) {
      //_______________ elimina usuario que este como inactivo pago ________________________
      this.rollbackCreacionCuentaPaymentezService.rollBackCrearCuenta(email);
      return true;
    }

    if (user) {
      return false;
    } else {
      return true;
    }
  }
}
