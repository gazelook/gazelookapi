import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import {
  idiomas,
  codIdiomas,
  codigoCatalogosTipoEmail,
  nombreAcciones,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearEmailRecuperarContraseniaService } from 'src/entidades/emails/casos_de_uso/crear-email-recuperar-contrasenia.service';
import * as bcrypt from 'bcrypt';
import { ConfirmacionRecuperarContraseniaService } from 'src/entidades/emails/casos_de_uso/crear-email-confirmacion-recuperar-contrasenia.service';

const sw = require('stopword');
const mongoose = require('mongoose');

@Injectable()
export class RecuperarContraseniaService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly usuarioModel: Model<Usuario>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearEmailRecuperarContraseniaService: CrearEmailRecuperarContraseniaService,
    private confirmacionRecuperarContraseniaService: ConfirmacionRecuperarContraseniaService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}
  getCodIdiomas = codIdiomas;
  getIdiomas = idiomas;
  //Metodo para colicitar recuperar contraseña
  async solicitudRecuperarContrasenia(
    email: string,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const newRecContrasnia = {
        emailDestinatario: email,
        codigo: codigoCatalogosTipoEmail.recuperar_contrasenia,
        idioma: codIdiom,
      };

      //Llama al metodo de enviar correo para cambiar contraseña
      const recupContrasenia = await this.crearEmailRecuperarContraseniaService.crearEmailRecuperarContrasenia(
        newRecContrasnia,
        opts,
      );
      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return recupContrasenia;
    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      throw error;
    }
  }

  //Metodo para actualizar contraseña
  async actualizarContrasenia(email, nuevaContrasenia) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      var BCRYPT_SALT_ROUNDS = 12;

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad usuarios
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.usuarios,
      );
      let getCodEntUsuarios = entidad.codigo;

      //Encriptar contraseña con BCRYPT
      const hashedContrasena = await bcrypt.hash(
        nuevaContrasenia,
        BCRYPT_SALT_ROUNDS,
      );

      //Actualizo la nueva contraseña
      await this.usuarioModel.findOneAndUpdate(
        { email: email },
        {
          $set: {
            contrasena: hashedContrasena,
            fechaActualizacion: new Date(),
          },
        },
        opts,
      );

      const getParProyec = await this.usuarioModel
        .findOne({ email: email })
        .session(opts.session);

      //Objeto para guardar el historico de la recuperacion del proyecto
      let datosUsuario = {
        _id: getParProyec._id,
        email: getParProyec.email,
        contrasena: getParProyec.contrasena,
        fechaActualizacion: getParProyec.fechaActualizacion,
      };

      let newHistoricoActualizarPass: any = {
        datos: datosUsuario, //Datos de la actualizacion de contraseña de usuario
        usuario: getParProyec._id,
        accion: getAccion,
        entidad: getCodEntUsuarios,
      };

      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoActualizarPass,
      );

      const newActualizacionPass = {
        emailDestinatario: email,
        codigo: codigoCatalogosTipoEmail.actualizacion_contrasenia,
      };
      //Llama al metodo de enviar correo de aviso de cambio de contraseña
      const getConfirRecContrasenia = await this.confirmacionRecuperarContraseniaService.confirmacionRecuperarContrasenia(
        newActualizacionPass,
        opts,
      );

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return getConfirRecContrasenia;
    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      throw error;
    }
  }
}
