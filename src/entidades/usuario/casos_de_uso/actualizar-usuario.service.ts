import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
const mongoose = require('mongoose');

@Injectable()
export class ActualizarUsuarioService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
  ) {}

  async actualizarEstadoEmailUsuario(
    usuario,
    dataEstadoEmail,
    opts: any,
  ): Promise<any> {
    try {
      const userUpdate = await this.userModel.findByIdAndUpdate(
        usuario,
        dataEstadoEmail,
        opts,
      );
      return userUpdate;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async actualizarEstadoUsuarioValidacionCuenta(
    usuario,
    estado,
    opts: any,
  ): Promise<any> {
    try {
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.usuarios,
      );
      const checkUser = await this.userModel.findById(usuario);
      if (!checkUser) {
        console.log('No existe el usuario');
        return null;
      }

      await this.userModel.findByIdAndUpdate(usuario, { estado: estado }, opts);

      const usuarioActualizado = await this.userModel
        .findById(usuario)
        .session(opts.session);
      console.log('usuarioActualizado:', usuarioActualizado);

      const dataUser = JSON.parse(JSON.stringify(usuarioActualizado));
      //eliminar parametro no necesarios
      delete dataUser.fechaCreacion;
      delete dataUser.fechaActualizacion;
      delete dataUser.__v;

      const dataHistoricoUser: any = {
        datos: dataUser,
        usuario: usuario,
        accion: accion.codigo,
        entidad: entidad.codigo,
      };

      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoUser);
      return usuarioActualizado;
    } catch (error) {
      throw error;
    }
  }

  async agregarUsuarioTransaccion(
    idUsuario,
    idTransaccion,
    opts: any,
  ): Promise<any> {
    try {
      const userUpdate = await this.userModel.findOneAndUpdate(
        { _id: idUsuario },
        { $push: { transacciones: idTransaccion } },
        opts,
      );
      return userUpdate;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
  async agregarUsuarioSuscripcion(
    idUsuario,
    idSuscripcion,
    opts: any,
  ): Promise<any> {
    try {
      const userUpdate = await this.userModel.findOneAndUpdate(
        { _id: idUsuario },
        { $push: { suscripciones: idSuscripcion } },
        opts,
      );
      return userUpdate;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async agregarPerfilUsuario(idUsuario, idPerfil, opts): Promise<any> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    const getAccion = accion.codigo;

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.usuarios,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    try {
      const userUpdate = await this.userModel.findOneAndUpdate(
        { _id: idUsuario },
        { $push: { perfiles: idPerfil } },
        opts,
      );
      const dataUserHistorico = JSON.parse(JSON.stringify(userUpdate));
      //eliminar parametro no necesarios
      delete dataUserHistorico.fechaCreacion;
      delete dataUserHistorico.fechaActualizacion;
      delete dataUserHistorico.__v;

      const dataHistoricoUsuario: any = {
        datos: dataUserHistorico,
        usuario: dataUserHistorico._id,
        accion: getAccion,
        entidad: codEntidad,
      };
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoUsuario);
      return userUpdate;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async actualizarTerminosYCondiciones(opts: any): Promise<any> {
    try {
      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.usuarios,
      );
      const codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );

      const userUpdate = await this.userModel.updateMany(
        { estado: estado.codigo },
        { $set: { aceptoTerminosCondiciones: false } },
        opts,
      );
      return userUpdate;
    } catch (error) {
      throw error;
    }
  }
}
