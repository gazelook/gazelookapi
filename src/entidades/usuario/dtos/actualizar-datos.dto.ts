import { ApiProperty } from '@nestjs/swagger';
import {
  IsDate,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class ActualizarDatosUsuarioDto {
  @ApiProperty()
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsString()
  idDispositivo: string;

  @ApiProperty()
  @IsOptional()
  contrasena: string;
  @ApiProperty()
  @IsOptional()
  @IsString()
  nuevaContrasena: string;
  @ApiProperty()
  @IsOptional()
  @IsString()
  fechaNacimiento: string;
  @ApiProperty()
  @IsOptional()
  nombre: string;
}

export class ActualizarEmailUsuarioDto {
  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsOptional()
  emailVerificado: boolean;

  @ApiProperty()
  @IsString()
  idDispositivo: string;
}
