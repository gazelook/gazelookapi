import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsNotEmptyObject } from 'class-validator';
import {
  Direccion,
  PerfilCuenta,
} from 'src/entidades/cuenta/entidad/cuenta.dto';
import { CrearDireccionDto } from 'src/entidades/direccion/dtos/crear-direccion.dto';
import { PerfilSinPago } from './perfil.dto';

export class UsuarioDto {
  @ApiProperty()
  _id?: string;

  @ApiProperty()
  email: string;
  @ApiProperty()
  contrasena: string;
  @ApiProperty()
  fechaNacimiento: Date;
  @ApiProperty()
  aceptoTerminosCondiciones: boolean;
  @ApiProperty()
  perfilGrupo: boolean;
  @ApiProperty()
  emailVerificado: boolean;
  @ApiProperty()
  menorEdad: boolean;
  @ApiProperty()
  idioma: string;
  @ApiProperty()
  estado: string;
  @ApiProperty()
  direccion: string;
  @ApiProperty()
  emailResponsable: string;
  @ApiProperty()
  nombreResponsable: string;
  @ApiProperty()
  responsableVerificado: boolean;
  @ApiProperty()
  direccionDomiciliaria: string;
  @ApiProperty()
  documentoIdentidad: string;
}

export class EstadoUsuarioSinPagoDto {
  @ApiProperty()
  codigo: string;
}

export class UsuarioSinPagoDto {
  @ApiProperty()
  _id?: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  contrasena: string;
  @ApiProperty()
  fechaNacimiento?: Date;
  // direccion
  @ApiProperty({ type: Direccion })
  direccion: Direccion;
  @ApiProperty()
  aceptoTerminosCondiciones: boolean;
  @ApiProperty()
  perfilGrupo: boolean;
  @ApiProperty()
  emailVerificado: boolean;
  @ApiProperty()
  menorEdad: boolean;
  @ApiProperty()
  idioma: string;
  @ApiProperty({ type: Array })
  rolSistema: Array<string>;
  //perfil
  @ApiProperty({ type: [PerfilSinPago] })
  perfiles: Array<PerfilSinPago>;
  @ApiProperty({ type: String })
  estado: string;
  @ApiProperty()
  direccionDomiciliaria?: string;
  @ApiProperty()
  documentoIdentidad?: string;
}

export class IdUsuarioDto {
  @ApiProperty()
  _id: string;
}

export class ActivarBloquearUsuarioSinPagoDto {
  @ApiProperty()
  _id: string;
  @ApiProperty({ type: EstadoUsuarioSinPagoDto })
  estado: EstadoUsuarioSinPagoDto;
}
