import { albumPerfilSistemaDto } from '../../album/entidad/album-dto';
import { ApiProperty } from '@nestjs/swagger';
import { ArrayNotEmpty, IsNotEmpty, IsOptional } from 'class-validator';
import { Telefono } from 'src/entidades/cuenta/entidad/cuenta.dto';

export class PerfilDto {
  @ApiProperty()
  perfil: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  descripcion: string;
  @ApiProperty()
  idioma: string;
  @ApiProperty()
  codigoTipoPerfil: string;
}

export class PerfilIdDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
}

export class PerfilNoticiaDto {
  @ApiProperty()
  _id: string;
}

export class PerfilNoticiaUpdateDto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  nombre: string;
}

export class PerfilProyectoDto {
  @ApiProperty({
    required: true,
    description: 'identificador del perfil',
    example: '5f4d0f02ecea1c15acb66d64',
  })
  _id: string;
}

export class PerfilAnuncioDto {
  @ApiProperty({
    required: true,
    description: 'identificador del perfil',
    example: '5f4d0f02ecea1c15acb66d64',
  })
  _id: string;
}

export class PerfilProyectoUnicoDto {
  @ApiProperty({
    description: 'identificador del perfil',
    example: '5f4d0f02ecea1c15acb66d64',
  })
  _id: string;

  @ApiProperty({ description: 'Nombre del perfil', example: 'Leonardo' })
  nombre: string;
}

export class PerfilContactoDto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  nombreContacto: string;
  @ApiProperty()
  album: albumPerfilSistemaDto;
}

export class PerfilPropietarioDto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  nombreContacto: string;

  @ApiProperty({ type: [albumPerfilSistemaDto] })
  album: [albumPerfilSistemaDto];
}

export class PerfilPropietarioMensajeDto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  nombreContacto: string;
}

export class PerfilSinPago {
  @ApiProperty()
  _id?: string;
  @ApiProperty()
  @IsNotEmpty()
  nombreContacto: string;
  @ApiProperty()
  @IsNotEmpty()
  nombre: string;
  @ApiProperty({ type: [Telefono] })
  @IsOptional()
  @ArrayNotEmpty()
  telefonos: Telefono[];
}
