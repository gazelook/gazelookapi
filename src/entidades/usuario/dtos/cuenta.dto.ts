import { ApiProperty } from '@nestjs/swagger';

export class Archivo {
  @ApiProperty()
  codigoArchivo: string;
  @ApiProperty()
  tipoMedia: string;
  @ApiProperty({ required: false })
  descripcion?: string;
}

export class Album {
  @ApiProperty()
  nombre?: string;
  @ApiProperty()
  tipoAlbum: string;
  @ApiProperty({ type: Archivo, required: false })
  portada: Archivo;
}

export class Perfil {
  @ApiProperty()
  tipoPerfil: string;
  @ApiProperty({ type: Album })
  album: Album;
  @ApiProperty({ type: [Archivo] })
  archivos: Archivo[];
}

export class CuentaDto {
  //usuario
  @ApiProperty()
  email: string;
  @ApiProperty()
  contrasena: string;
  @ApiProperty()
  fechaNacimiento: Date;
  @ApiProperty()
  aceptoTerminosCondiciones: boolean;
  @ApiProperty()
  perfilGrupo: boolean;
  @ApiProperty()
  menorEdad: boolean;
  @ApiProperty()
  emailResponsable: string;
  @ApiProperty()
  id_catalogoIdioma: string;

  //perfil
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  nombreContacto: string;
  @ApiProperty({ type: [Perfil] })
  perfil: Perfil[];

  //transaccion
  @ApiProperty({
    description: 'Tipo de pago',
    required: true,
    example: 'stripe | paypal',
  })
  tipoPago: string;
  @ApiProperty()
  telefono: number;
  @ApiProperty()
  direccion: string;
  @ApiProperty()
  pais: string;
}
