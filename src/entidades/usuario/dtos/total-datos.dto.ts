import { ApiProperty } from '@nestjs/swagger';

export class TotalDatosDto {
  @ApiProperty({ type: Number })
  totalUsuarios: number;
  @ApiProperty({ type: Number })
  totalProyectos: number;
  @ApiProperty({ type: Number })
  totalProyectosMundial: number;
  @ApiProperty({ type: Number })
  totalProyectosLocal: number;
  @ApiProperty({ type: Number })
  totalProyectosPais: number;
  @ApiProperty({ type: Number })
  totalProyectosRed: number;
  @ApiProperty({ type: Number })
  totalNoticias: number;
  @ApiProperty({ type: Number })
  totalIntercambios: number;
}
