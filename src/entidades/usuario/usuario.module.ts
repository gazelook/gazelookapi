import { CatalogosModule } from './../catalogos/catalogos.module';
import { UsuarioControllerModule } from './controladores/usuario-controller.module';
import { Module } from '@nestjs/common';
import { UsuarioServicesModule } from './casos_de_uso/usuario-services.module';
import { CatalogosServiceModule } from '../catalogos/casos_de_uso/catalogos-services.module';

@Module({
  imports: [UsuarioControllerModule],
  providers: [],
  controllers: [],
  exports: [UsuarioControllerModule],
})
export class UsuarioModule {}
