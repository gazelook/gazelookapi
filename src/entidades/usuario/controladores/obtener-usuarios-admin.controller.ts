import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerUsuariosAdminService } from '../casos_de_uso/obtener-usuarios-admin.service';

@ApiTags('Usuario')
@Controller('api/usuario/obtener-usuarios-admin')
export class ObtenerUsuariosAdminControlador {
  constructor(
    private readonly obtenerUsuariosAdminService: ObtenerUsuariosAdminService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary:
      'Devuelve todos los usuarios admin segun los parametros que se envien',
  })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 406, description: 'Error al obtener proyectos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerUsuariosAdmin(
    @Headers() headers,
    // @Res() response: Response
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const usuarios = await this.obtenerUsuariosAdminService.obtenerUsuariosAdmin();

      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: usuarios,
      });
      // response.send(respuesta)
      return respuesta;
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      // response.send(respuesta)
      return respuesta;
    }
  }
}
