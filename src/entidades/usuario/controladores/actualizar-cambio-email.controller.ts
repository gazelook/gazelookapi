import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import {
  Controller,
  Body,
  HttpStatus,
  Headers,
  UseGuards,
  Put,
  Param,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { isValidObjectId } from 'mongoose';
import { ActualizarEmailUsuarioDto } from '../dtos/actualizar-datos.dto';
import { ActualizarCambioEmailService } from '../casos_de_uso/actualizar-cambio-email.service';
import { isUUID } from 'class-validator';

@ApiTags('Usuario')
@Controller('api/usuario/actualizar-cambio-email')
export class ActualizarCambioEmailController {
  constructor(
    private readonly actualizarCambioEmailService: ActualizarCambioEmailService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/:idUsuario')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    required: true,
    description: 'key para usar las rutas',
  })
  @ApiOperation({
    summary:
      'Actualiza el email del usuario genera nuevo token de autenticacion',
  })
  @ApiResponse({ status: 200, description: 'Actualizacion correcta' })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  //@UseGuards(AuthGuard('jwt'))

  // recibe el token del email desde el api de mantenimiento para validar dicha operacion
  public async actualizarDatosUsuario(
    @Headers() headers,
    @Body() userData: ActualizarEmailUsuarioDto,
    @Param('idUsuario') idUsuario: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (isValidObjectId(idUsuario) && isUUID(headers.token)) {
        const userUpdated = await this.actualizarCambioEmailService.actualizarEmailCuenta(
          idUsuario,
          userData,
          headers.token,
        );

        if (userUpdated) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
