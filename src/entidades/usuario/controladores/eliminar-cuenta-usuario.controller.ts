import { Controller, Delete, Headers, HttpStatus, Param } from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { isUUID } from 'class-validator';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { EliminarCuentaUsuarioService } from '../casos_de_uso/eliminar-cuenta-usuario.service';

@ApiTags('Usuario')
@Controller('api/usuario/eliminar-cuenta-usuario')
export class EliminarCuentaUsuarioController {
  constructor(
    private readonly eliminarCuentaUsuarioService: EliminarCuentaUsuarioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Delete('/:idUsuario')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    required: true,
    description: 'key para usar las rutas',
  })
  @ApiOperation({ summary: 'Elimina logicamente la cuenta de usuario' })
  @ApiResponse({ status: 201, description: 'Datos Eliminados' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  //@UseGuards(AuthGuard('jwt'))

  // recibe el token del email desde el api de mantenimiento para validar dicha operacion
  public async eliminarCuentaUsuario(
    @Headers() headers,
    @Param('idUsuario') idUsuario: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (mongoose.isValidObjectId(idUsuario) && isUUID(headers.token)) {
        const informacion = await this.eliminarCuentaUsuarioService.eliminarCuentaUsuario(
          idUsuario,
          headers.token,
        );

        if (informacion.mensaje) {
          const DATOS_ELIMINADOS = await this.i18n.translate(
            codIdioma.concat('.DATOS_ELIMINADOS'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: DATOS_ELIMINADOS,
          });
        } else {
          const ERROR_ELIMINAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ELIMINAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
