import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
  ApiBody,
} from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Headers,
  Req,
  UseGuards,
  Put,
} from '@nestjs/common';
import { Funcion } from '../../../shared/funcion';
import { I18nService } from 'nestjs-i18n';
import { UsuarioSinPagoDto } from '../dtos/usuario.dto';
import { CrearUsuarioSinPagoService } from '../casos_de_uso/crear-usuario-sin-pago.service';
import { AuthGuard } from '@nestjs/passport';
import { ActualizarUsuarioSinPagoService } from '../casos_de_uso/actualizar-usuario-sin-pago.service';
import { isValidObjectId } from 'mongoose';

@ApiTags('Usuario')
@Controller('api/usuario/actualizar-usuario-sin-pago')
export class ActualizarUsuarioSinPagoControlador {
  constructor(
    private readonly actualizarUsuarioSinPagoService: ActualizarUsuarioSinPagoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: UsuarioSinPagoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Actualiza los datos del usuario desde administración',
  })
  @ApiResponse({ status: 200, description: 'Actualizacion correcta' })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarUsuarioSinPago(
    @Body() usuarioDto: UsuarioSinPagoDto,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    //const dataToken = req.user;
    //console.log("dispositivoToken", dataToken);
    try {
      if (isValidObjectId(usuarioDto._id)) {
        const usuario = await this.actualizarUsuarioSinPagoService.actualizarUsuarioSinPago(
          usuarioDto,
          codIdioma,
        );

        if (usuario) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
