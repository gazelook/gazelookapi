import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import {
  Controller,
  Body,
  HttpStatus,
  Headers,
  UseGuards,
  Put,
  Param,
  Request,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ActualizarDatosUsuarioService } from '../casos_de_uso/actualizar-datos-usuario.service';
import { isValidObjectId } from 'mongoose';
import { ActualizarDatosUsuarioDto } from '../dtos/actualizar-datos.dto';

@ApiTags('Usuario')
@Controller('api/usuario/actualizar-datos')
export class ActualizarDatosUsuarioController {
  constructor(
    private readonly actualizarDatosUsuarioService: ActualizarDatosUsuarioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  //funcion = new Funcion(this.i18n);

  @Put('/:idUsuario')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    required: true,
    description: 'key para usar las rutas',
  })
  @ApiOperation({
    summary:
      'Actualiza los datos del usuario. (email, nombre, fechaNacimiento, contraseña)',
  })
  @ApiResponse({ status: 200, description: 'Actualizacion correcta' })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 409, description: 'Contraseñas no son iguales' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarDatosUsuario(
    @Headers() headers,
    @Body() userData: ActualizarDatosUsuarioDto,
    @Param('idUsuario') idUsuario: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (isValidObjectId(idUsuario)) {
        const userUpdated = await this.actualizarDatosUsuarioService.actualizarDatosusuario(
          idUsuario,
          userData,
        );

        if (userUpdated) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
