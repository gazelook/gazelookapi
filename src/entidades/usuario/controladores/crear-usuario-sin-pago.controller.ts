import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
  ApiBody,
} from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Headers,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Funcion } from '../../../shared/funcion';
import { I18nService } from 'nestjs-i18n';
import { UsuarioSinPagoDto } from '../dtos/usuario.dto';
import { CrearUsuarioSinPagoService } from '../casos_de_uso/crear-usuario-sin-pago.service';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Usuario')
@Controller('api/usuario/crear-usuario-sin-pago')
export class CrearUsuarioSinPagoControlador {
  constructor(
    private readonly crearUsuarioSinPagoService: CrearUsuarioSinPagoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: UsuarioSinPagoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Crea un nuevo usuario desde administración' })
  @ApiResponse({ status: 201, description: 'Creación correcta' })
  @ApiResponse({ status: 404, description: 'Error al crear' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async crearUsuarioSinPago(
    @Body() usuarioDto: UsuarioSinPagoDto,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    //const dataToken = req.user;
    //console.log("dispositivoToken", dataToken);
    try {
      const usuario = await this.crearUsuarioSinPagoService.crearUsuarioSinPago(
        usuarioDto,
        codIdioma,
      );

      // proceso normal
      if (usuario) {
        const CREACION_CORRECTA = await this.i18n.translate(
          codIdioma.concat('.CREACION_CORRECTA'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: CREACION_CORRECTA,
          datos: usuario,
        });
      } else {
        const ERROR_CREACION = await this.i18n.translate(
          codIdioma.concat('.ERROR_CREACION'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_CREACION,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
