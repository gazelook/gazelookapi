import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
  ApiBody,
} from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Headers,
  Req,
  UseGuards,
  Put,
} from '@nestjs/common';
import { Funcion } from '../../../shared/funcion';
import { I18nService } from 'nestjs-i18n';
import {
  ActivarBloquearUsuarioSinPagoDto,
  UsuarioSinPagoDto,
} from '../dtos/usuario.dto';
import { CrearUsuarioSinPagoService } from '../casos_de_uso/crear-usuario-sin-pago.service';
import { AuthGuard } from '@nestjs/passport';
import { ActualizarUsuarioSinPagoService } from '../casos_de_uso/actualizar-usuario-sin-pago.service';
import { isValidObjectId } from 'mongoose';
import { ActivarBloquearUsuarioSinPagoService } from '../casos_de_uso/activar-bloquear-usuario-sin-pago.service';

@ApiTags('Usuario')
@Controller('api/usuario/activar-bloquear-usuario-sin-pago')
export class ActivarBloquearUsuarioSinPagoControlador {
  constructor(
    private readonly activarBloquearUsuarioSinPagoService: ActivarBloquearUsuarioSinPagoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: ActivarBloquearUsuarioSinPagoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Activa o bloquea un usuario desde administración' })
  @ApiResponse({ status: 200, description: 'Actualizacion correcta' })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async activarBloquearUsuarioSinPago(
    @Body() usuarioDto: ActivarBloquearUsuarioSinPagoDto,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    //const dataToken = req.user;
    //console.log("dispositivoToken", dataToken);
    try {
      if (isValidObjectId(usuarioDto._id) && usuarioDto.estado.codigo) {
        const usuario = await this.activarBloquearUsuarioSinPagoService.activarBloquearUsuarioSinPago(
          usuarioDto,
          codIdioma,
        );

        if (usuario) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
