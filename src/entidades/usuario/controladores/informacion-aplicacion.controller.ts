import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import {
  Controller,
  Res,
  HttpStatus,
  Get,
  Headers,
  Param,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { InformacioAplicacionService } from '../casos_de_uso/informacion-aplicacion.service';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Usuario')
@Controller('api/usuario/solicitar-informacion')
export class InformacionAplicacionControlador {
  constructor(
    private readonly informacioAplicacionService: InformacioAplicacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/:idUsuario')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    required: true,
    description: 'key para usar las rutas',
  })
  @ApiOperation({
    summary: 'Retorna la informacion del usuario que tiene en la aplicacion',
  })
  @ApiResponse({
    status: 201,
    description: 'Datos del usuario en la aplicacion',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async informacionAplicacion(
    @Headers() headers,
    @Param('idUsuario') idUsuario: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (mongoose.isValidObjectId(idUsuario)) {
        const informacion = await this.informacioAplicacionService.allInformacionUsuarioAplicacion(
          idUsuario,
          codIdioma,
        );

        if (informacion) {
          const SOLICITUD_EN_PROCESO = await this.i18n.translate(
            codIdioma.concat('.SOLICITUD_EN_PROCESO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: SOLICITUD_EN_PROCESO,
            datos: informacion,
          });
        } else {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_OBTENER,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
