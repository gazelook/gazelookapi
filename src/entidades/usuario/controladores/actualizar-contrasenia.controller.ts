import { AuthGuard } from '@nestjs/passport';
import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiBasicAuth,
} from '@nestjs/swagger';
import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  Headers,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { RecuperarContraseniaService } from '../casos_de_uso/recuperar-contrasena.service';
import { ActualizarContraseniaDto } from '../dtos/actualizar_contrasenia.dto';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Usuario')
@Controller('api/actualizar-contrasenia')
export class ActualizarContraseniaControlador {
  constructor(
    private readonly recuperarContraseniaService: RecuperarContraseniaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  //funcion = new Funcion(this.i18n);

  @Post('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    required: true,
    description: 'key para usar las rutas',
  })
  @ApiOperation({
    summary:
      'Actualiza la contraseña de un usuario Body: (email, newPass, confirmPass)',
  })
  @ApiResponse({ status: 201, description: 'Actualizacion correcta' })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 409, description: 'Contraseñas no son iguales' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarContrasenia(
    @Headers() headers,
    @Body() userData: ActualizarContraseniaDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (userData.email && userData.newPass && userData.confirmPass) {
        if (userData.newPass === userData.confirmPass) {
          const recContrasenia = await this.recuperarContraseniaService.actualizarContrasenia(
            userData.email,
            userData.newPass,
          );

          if (recContrasenia) {
            const ACTUALIZACION_CORRECTA = await this.i18n.translate(
              codIdioma.concat('.ACTUALIZACION_CORRECTA'),
              {
                lang: codIdioma,
              },
            );
            return this.funcion.enviarRespuestaOptimizada({
              codigoEstado: HttpStatus.OK,
              mensaje: ACTUALIZACION_CORRECTA,
            });
          } else {
            const ERROR_ACTUALIZAR = await this.i18n.translate(
              codIdioma.concat('.ERROR_ACTUALIZAR'),
              {
                lang: codIdioma,
              },
            );
            return this.funcion.enviarRespuestaOptimizada({
              codigoEstado: HttpStatus.NOT_FOUND,
              mensaje: ERROR_ACTUALIZAR,
            });
          }
        } else {
          const ERROR_CONTRASENIAS = await this.i18n.translate(
            codIdioma.concat('.ERROR_CONTRASENIAS'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_CONTRASENIAS,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
