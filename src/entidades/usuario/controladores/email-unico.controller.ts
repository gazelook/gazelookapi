import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
} from '@nestjs/swagger';

import { Controller, Headers, HttpStatus, Param, Get } from '@nestjs/common';
import { EmailunicoService } from '../casos_de_uso/email-unico.service';
import { Funcion } from 'src/shared/funcion';
import { I18nService } from 'nestjs-i18n';

@ApiTags('Usuario')
@Controller('api/usuario/email-unico')
export class EmailUnicoControlador {
  constructor(
    private readonly emailunicoService: EmailunicoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  // Add User: /users/create
  @Get('/:email')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'Email disponible' })
  @ApiResponse({ status: 404, description: 'Error al consultar' })
  @ApiOperation({
    summary: 'Este metodo retorna OK si el nombre de contacto esta disponible',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async verificarEmail(
    @Param('email') email: string,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const result = await this.emailunicoService.emailUnico(email, codIdioma);

      if (!result) {
        const EMAIL_YA_REGISTRADO = await this.i18n.translate(
          codIdioma.concat('.EMAIL_YA_REGISTRADO'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: EMAIL_YA_REGISTRADO,
        });
      } else {
        const EMAIL_DISPONIBLE = await this.i18n.translate(
          codIdioma.concat('.EMAIL_DISPONIBLE'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: EMAIL_DISPONIBLE,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
