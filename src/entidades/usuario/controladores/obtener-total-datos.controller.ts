import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerTotalDatosService } from '../casos_de_uso/obtener-total-datos.service';
import { ObtenerUsuariosAdminService } from '../casos_de_uso/obtener-usuarios-admin.service';
import { TotalDatosDto } from '../dtos/total-datos.dto';

@ApiTags('Usuario')
@Controller('api/obtener-total-datos')
export class ObtenerTotalDatosControlador {
  constructor(
    private readonly obtenerTotalDatosService: ObtenerTotalDatosService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  // @ApiBody({ type: TotalDatosDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary:
      'Devuelve todos los datos de total usuarios, total proyectos, total segun tipo de proyectos, total noticias, total intercambios',
  })
  @ApiResponse({ status: 200, type: TotalDatosDto, description: 'OK' })
  @ApiResponse({ status: 406, description: 'Error al obtener los datos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerTotalDatos(@Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const usuarios = await this.obtenerTotalDatosService.obtenerdatosTotales();

      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: usuarios,
      });
      return respuesta;
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      return respuesta;
    }
  }
}
