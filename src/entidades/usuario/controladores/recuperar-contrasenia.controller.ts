import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, HttpStatus, Headers, Query } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { RecuperarContraseniaService } from '../casos_de_uso/recuperar-contrasena.service';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Usuario')
@Controller('api/recuperar-contrasenia')
export class RecuperarContraseniaControlador {
  constructor(
    private readonly recuperarContraseniaService: RecuperarContraseniaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Se envia un correo al usuario que desea cambiar su contraseña',
  })
  @ApiResponse({ status: 201, description: 'Email enviado exitosamente' })
  @ApiResponse({ status: 404, description: 'El email no puede enviarse' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  //@UseGuards(AuthGuard('jwt'))
  public async recuperarContrasenia(
    @Headers() headers,
    @Query('email') email: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (email) {
        const recContrasenia = await this.recuperarContraseniaService.solicitudRecuperarContrasenia(
          email,
          codIdioma,
        );

        //await this.transferirProyectoService.trasferirProyecto(trasferirProyectoDto.idProyecto, trasferirProyectoDto.perfilPropietario, trasferirProyectoDto.perfilNuevo)
        if (recContrasenia) {
          const EMAIL_ENVIADO = await this.i18n.translate(
            codIdioma.concat('.EMAIL_ENVIADO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: EMAIL_ENVIADO,
          });
        } else {
          const EMAIL_NO_PUEDE_CREARSE = await this.i18n.translate(
            codIdioma.concat('.EMAIL_NO_PUEDE_CREARSE'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: EMAIL_NO_PUEDE_CREARSE,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
