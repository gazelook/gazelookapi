import { Module } from '@nestjs/common';
import { UsuarioServicesModule } from '../casos_de_uso/usuario-services.module';
import { HttpErrorFilter } from 'src/shared/filters/http-error.filter';
import { EmailUnicoControlador } from './email-unico.controller';
import { ActualizarContraseniaControlador } from './actualizar-contrasenia.controller';
import { RecuperarContraseniaControlador } from './recuperar-contrasenia.controller';
import { InformacionAplicacionControlador } from './informacion-aplicacion.controller';
import { EliminarDatosUsuarioCompletoControlador } from './eliminar-datos-usuario-completo.controller';
import { Funcion } from 'src/shared/funcion';
import { ActualizarDatosUsuarioController } from './actualizar-datos-usuario.controller';
import { ActualizarCambioEmailController } from './actualizar-cambio-email.controller';
import { CrearUsuarioSinPagoControlador } from './crear-usuario-sin-pago.controller';
import { ObtenerUsuariosAdminControlador } from './obtener-usuarios-admin.controller';
import { ObtenerUsuariosAdminByEntidadControlador } from './obtener-usuarios-admin-by-entidad.controller';
import { ActualizarUsuarioSinPagoControlador } from './actualizar-usuario-sin-pago.controller';
import { ActivarBloquearUsuarioSinPagoControlador } from './activar-bloquear-usuario-sin-pago.controller';
import { EliminarCuentaUsuarioController } from './eliminar-cuenta-usuario.controller';
import { ObtenerTotalDatosControlador } from './obtener-total-datos.controller';

@Module({
  imports: [UsuarioServicesModule],
  providers: [HttpErrorFilter, Funcion],
  exports: [UsuarioServicesModule],
  controllers: [
    ActualizarContraseniaControlador,
    //RecuperarContraseniaControlador,
    EmailUnicoControlador,
    InformacionAplicacionControlador,
    EliminarDatosUsuarioCompletoControlador,
    ActualizarDatosUsuarioController,
    ActualizarCambioEmailController,
    CrearUsuarioSinPagoControlador,
    ObtenerUsuariosAdminControlador,
    ObtenerUsuariosAdminByEntidadControlador,
    ActualizarUsuarioSinPagoControlador,
    ActivarBloquearUsuarioSinPagoControlador,
    EliminarCuentaUsuarioController,
    ObtenerTotalDatosControlador,
  ],
})
export class UsuarioControllerModule {}
