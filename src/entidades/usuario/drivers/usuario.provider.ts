import { estadosModelo } from './../../../drivers/mongoose/modelos/catalogoEstado/catalogoEstadoModelo';
import { Connection } from 'mongoose';
import { usuarioModelo } from '../../../drivers/mongoose/modelos/usuarios/usuario.schema';
import { perfilModelo } from '../../../drivers/mongoose/modelos/perfil/perfil.schema';
import { TransaccionModelo } from '../../../drivers/mongoose/modelos/transaccion/transaccion.schema';
import { SuscripcionModelo } from '../../../drivers/mongoose/modelos/suscripcion/suscripcion.schema';
import { ParticipanteProyectoModelo } from '../../../drivers/mongoose/modelos/participante_proyecto/participante_proyecto.schema';
import { ProyectoModelo } from '../../../drivers/mongoose/modelos/proyectos/proyecto.schema';
import { albumModelo } from 'src/drivers/mongoose/modelos/album/album.schema';
import { DireccionModelo } from 'src/drivers/mongoose/modelos/direcciones/direccion.schema';
import { MediaModelo } from 'src/drivers/mongoose/modelos/media/mediaModelo';
import { TraduccionMediaModelo } from 'src/drivers/mongoose/modelos/traducion_media/traduccion-media.schema';
import { archivoModelo } from 'src/drivers/mongoose/modelos/archivo/archivo.schema';
import { DocumentoUsuario } from 'src/drivers/mongoose/modelos/documento_usuario/documento-usuario.schema';
import { noticiaModelo } from 'src/drivers/mongoose/modelos/noticias/noticia-schema';
import { IntercambioModelo } from 'src/drivers/mongoose/modelos/intercambio/intercambio.schema';

export const usuarioProviders = [
  {
    provide: 'USUARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('usuario', usuarioModelo, 'usuario'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRANSACCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('transaccion', TransaccionModelo, 'transaccion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'SUSCRIPCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('suscripcion', SuscripcionModelo, 'suscripcion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PARTICIPANTE_PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'participante_proyecto',
        ParticipanteProyectoModelo,
        'participante_proyecto',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('proyecto', ProyectoModelo, 'proyecto'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'NOTICIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('noticia', noticiaModelo, 'noticia'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'INTERCAMBIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('intercambio', IntercambioModelo, 'intercambio'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ALBUM_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('album', albumModelo, 'album'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'DIRECCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('direccion', DireccionModelo, 'direccion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'MEDIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('media', MediaModelo, 'media'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_MEDIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_media',
        TraduccionMediaModelo,
        'traduccion_media',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ARCHIVO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('archivo', archivoModelo, 'archivo'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'DOCUMENTO_USUARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'documento_usuario',
        DocumentoUsuario,
        'documento_usuario',
      ),
    inject: ['DB_CONNECTION'],
  },
];
