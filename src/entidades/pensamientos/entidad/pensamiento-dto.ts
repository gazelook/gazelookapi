import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsString,
  IsBoolean,
} from 'class-validator';
import { PerfilProyectoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import { IdentTraduccionPensamiento } from './traduccion-pensamiento-dto';

export class PensamientoDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  perfil: PerfilProyectoDto;

  @ApiProperty({ required: true, type: [IdentTraduccionPensamiento] })
  traducciones: Array<IdentTraduccionPensamiento>;

  @ApiProperty({
    description: 'Si el pensamiento es publico o privado',
    required: true,
  })
  @IsBoolean()
  publico: boolean;
}
