import { ApiProperty } from '@nestjs/swagger';
import { timestamp } from 'aws-sdk/clients/datapipeline';

export class UpdateEstadoPensamientoDto {
  @ApiProperty({
    description: 'Identificador autogenerado del pensamiento ',
    example: '5f3173e892e08e19101271f8',
  })
  _id: string;
  @ApiProperty({
    description: 'Codigos de la traducciones del pensamiento',
    required: true,
    example: '[{_id: 5f3aef6745f8842d4cc293d0}]',
  })
  traducciones: string;
  @ApiProperty({
    description:
      'Si el pensamiento es público o privado(publico=true, privado=false)',
    example: 'true',
  })
  publico: boolean;
  @ApiProperty({
    description: 'Fecha de Actualizacion',
    example: '2020-08-12T13:30:23.366+00:00',
  })
  fechaActualizacion: timestamp;
}
