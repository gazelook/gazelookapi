import { ApiProperty } from '@nestjs/swagger';
import { MinLength, MaxLength } from 'class-validator';

export class IdentTraduccionPensamiento {
  @MinLength(1, {
    message: 'Debe contener mínimo 1 caracter',
  })
  @MaxLength(230, {
    message: 'Debe contener máximo 230 caracter',
  })
  @ApiProperty({
    required: true,
    description: 'Texto del pensamiento',
    example: 'hola este es mi pensamiento',
  })
  texto: string;
}
