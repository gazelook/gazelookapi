import { ApiProperty } from '@nestjs/swagger';
import { DateTime } from 'aws-sdk/clients/devicefarm';

export class PensamientoResponseDto {
  @ApiProperty({
    description: 'Identificador autogenerado de la traducción del pensamiento',
    example: '5f3173e892e08e19101271f8',
  })
  _id: string;
  @ApiProperty({
    description: 'Texto de la traduccion',
    required: true,
    example: "[{texto: 'hola'}]",
  })
  traducciones: Array<string>;
  // @ApiProperty({description: 'Identificador del perfil', example: "5f3173e892e08e19101271f8"})
  // perfil:string;
  @ApiProperty({
    description:
      'Si el pensamiento es público o privado(publico=true, privado=false)',
    example: 'true',
  })
  publico: boolean;
  // @ApiProperty({description: 'Código del estado', example: "EST_22"})
  // estado:string;
  // @ApiProperty({description: 'Fecha de creación', example: "2020-08-12T13:30:23.366+00:00"})
  // fechaCreacion:timestamp;
  @ApiProperty({
    description: 'Fecha de Actualizacion',
    example: '2020-08-12T13:30:23.366+00:00',
  })
  fechaActualizacion: DateTime;
}
