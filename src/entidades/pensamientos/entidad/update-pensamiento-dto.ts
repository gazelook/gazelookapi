import { ApiProperty } from '@nestjs/swagger';
import { timestamp } from 'aws-sdk/clients/datapipeline';
import { IsString, IsNotEmpty, MinLength, MaxLength } from 'class-validator';
import { IdentTraduccionPensamiento } from './traduccion-pensamiento-dto';

export class UpdatePensamientoDto {
  @ApiProperty({
    description: 'Identificador autogenerado del pensamiento ',
    example: '5f3173e892e08e19101271f8',
  })
  @IsString()
  @IsNotEmpty()
  _id: string;

  @ApiProperty({ required: true, type: [IdentTraduccionPensamiento] })
  traducciones: Array<IdentTraduccionPensamiento>;
}
