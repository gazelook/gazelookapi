import { ApiProperty } from '@nestjs/swagger';
import { timestamp } from 'aws-sdk/clients/datapipeline';

export class ObtenerPensamientoResponseDto {
  @ApiProperty({
    description: 'Identificador autogenerado de la traduccion del pensamiento ',
    example: '5f3173e892e08e19101271f8',
  })
  _id: string;
  @ApiProperty({
    description: 'Arrays de traducciones[]',
    required: true,
    example: '[{_id: 5f3aef6745f8842d4cc293d0, texto: Primer pensamiento}]',
  })
  traducciones: Array<string>;
  @ApiProperty({
    description:
      'Si el pensamiento es público o privado(publico=true, privado=false)',
    example: 'true',
  })
  publico: boolean;
  @ApiProperty({
    description: 'Fecha de actualizacion del pensamiento',
    required: true,
    example: '2020-08-12T16:39:22.340Z',
  })
  fechaActualizacion: timestamp;
}
