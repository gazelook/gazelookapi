import { Module } from '@nestjs/common';
import { PensamientoServicesModule } from './casos_de_uso/pensamientos-services.module';
import { PensamientoControllerModule } from './controladores/pensamientos-controller.module';

@Module({
  imports: [PensamientoControllerModule],
  providers: [PensamientoServicesModule],
  controllers: [],
})
export class PensamientoModule {}
