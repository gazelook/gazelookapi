import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { TraduccionPensamiento } from 'src/drivers/mongoose/interfaces/traduccion_pensamiento/traduccion-pensamiento.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
const mongoose = require('mongoose');

@Injectable()
export class ActualizacionEstadoPensamientoService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    @Inject('TRADUCCION_PENSAMIENTO_MODEL')
    private readonly traduccionPensamientoModel: Model<TraduccionPensamiento>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEstadoService: CatalogoEstadoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private readonly funcion: Funcion,
  ) {}

  async actualizacionEstadoPensamiento(
    idPensamiento,
    idioma: string,
  ): Promise<any> {
    const respuesta = new RespuestaInterface();
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el codigo de la accion a modificar
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccionModificar = accionModificar.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.pensamientos,
      );

      //Obtiene el estado
      const estadoActiva = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      let codEstadoActiva = estadoActiva.codigo;

      //Obtiene la entidad traduccion pensamientos
      const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionPensamientos,
      );
      let codEntTra = entidadTra.codigo;

      //Obtiene el estado de activa traduccion pensamientos
      const estadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntTra,
      );
      let codEstadoTra = estadoTra.codigo;

      const codIdiomaQuery = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      let codIdioma = codIdiomaQuery.codigo;

      //Obtiene el pensamiento

      const getPensamientos = await this.pensamientoModel.findOne({
        _id: idPensamiento,
        estado: codEstadoActiva,
      });
      if (getPensamientos) {
        //Obtiene el estado del pensamiento si es publico(true) o privado(false)
        const publicoPensamiento = getPensamientos.publico;
        if (publicoPensamiento === true) {
          let date = new Date();
          //Actualiza a privado
          await this.pensamientoModel.updateOne(
            { _id: idPensamiento },
            { publico: false, fechaActualizacion: date },
            opts,
          );

          let dataTraduccion = {
            perfil: getPensamientos.perfil,
            _id: getPensamientos._id,
            publico: false,
          };
          //datos para guardar el historico de la traduccion del pensamiento
          let newHistoricoTraduccionPensamiento: any = {
            datos: dataTraduccion, //"Cambio de estado del pensamiento. idPensamiento: ".concat(getPensamientos._id),
            usuario: getPensamientos.perfil,
            accion: getAccionModificar,
            entidad: codEntTra,
          };

          //Guarda el historico del cambio del estado del pensamiento
          this.crearHistoricoService.crearHistoricoServer(
            newHistoricoTraduccionPensamiento,
          );

          //Obtiene el pensamiento con el nuevo estado
          const pensamiento = await this.pensamientoModel
            .findOne(
              { _id: idPensamiento, estado: codEstadoActiva },
              { _id: 1, traducciones: 1, publico: 1, fechaActualizacion: 1 },
            )
            .session(session)
            .populate({
              path: 'traducciones',
              match: {
                estado: codEstadoTra,
                idioma: codIdioma,
              },
              select: '_id',
            });
          //llama al metodo de traduccion dinamica
          const ACTUALIZACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
            idioma,
            'ACTUALIZACION_CORRECTA',
          );
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
          return respuesta;
        } else {
          let date = new Date();
          await this.pensamientoModel.updateOne(
            { _id: idPensamiento },
            { publico: true, fechaActualizacion: date },
            opts,
          );
          let dataTraduccion = {
            perfil: getPensamientos.perfil,
            _id: getPensamientos._id,
            publico: true,
          };
          //datos para guardar el historico dl cambio de estado del pensamiento
          let newHistoricoTraduccionPensamiento: any = {
            datos: dataTraduccion, //"Cambio de estado del pensamiento. idPensamiento: ".concat(getPensamientos._id),
            usuario: getPensamientos.perfil,
            accion: getAccionModificar,
            entidad: codEntTra,
          };

          //Guarda el historico de la traduccion del pensamiento
          this.crearHistoricoService.crearHistoricoServer(
            newHistoricoTraduccionPensamiento,
          );

          //const pensamiento = await this.pensamientoModel.findOne({_id:idPensamiento}, { _id:1, traducciones:1, publico:1,  fechaActualizacion:1 });
          //Obtiene los pensamientos
          const pensamiento = await this.pensamientoModel
            .findOne(
              { _id: idPensamiento, estado: codEstadoActiva },
              { _id: 1, traducciones: 1, publico: 1, fechaActualizacion: 1 },
            )
            .session(session)
            .populate({
              path: 'traducciones',
              match: {
                estado: codEstadoTra,
                idioma: codIdioma,
              },
              select: '_id',
            });

          //llama al metodo de traduccion dinamica
          const ACTUALIZACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
            idioma,
            'ACTUALIZACION_CORRECTA',
          );
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
          return respuesta;
        }
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        //llama al metodo de traduccion dinamica
        const ERROR_ACTUALIZAR = await this.traduccionEstaticaController.traduccionEstatica(
          idioma,
          'ERROR_ACTUALIZAR',
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_ACTUALIZAR,
        });
        return respuesta;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.messaje,
      });
      return respuesta;
    }
  }
}
