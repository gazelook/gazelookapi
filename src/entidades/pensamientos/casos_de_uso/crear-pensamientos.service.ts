import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { TraduccionPensamiento } from 'src/drivers/mongoose/interfaces/traduccion_pensamiento/traduccion-pensamiento.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { PensamientoDto } from '../entidad/pensamiento-dto';
import { TraducirPensamientoSegundoPlanoService } from './traducir-pensamiento-segundo-plano.service';

@Injectable()
export class CrearPensamientoService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    @Inject('TRADUCCION_PENSAMIENTO_MODEL')
    private readonly traduccionPensamientoModel: Model<TraduccionPensamiento>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private traducirPensamientoSegundoPlanoService: TraducirPensamientoSegundoPlanoService,
  ) {}

  async crearPensamiento(
    PensamientoDto: PensamientoDto,
    codigoIdioma,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.pensamientos,
      );

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad traduccion pensamientos
      const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionPensamientos,
      );
      let codEntTra = entidadTra.codigo;

      //Obtiene el estado de activa traduccion pensamientos
      const estadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntTra,
      );
      let codEstadoTra = estadoTra.codigo;

      //detectaar el idioma que se envia el texto
      let textoTraducido = await traducirTexto(
        codigoIdioma,
        PensamientoDto.traducciones[0].texto,
      );
      let idiomaDetectado = textoTraducido.idiomaDetectado;

      //Obtiene el idioma por el codigo
      const codIdiomaQuery = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idiomaDetectado,
      );
      let codIdioma = codIdiomaQuery.codigo;

      //Objeto de traduccion noticia
      const idPensamiento = new mongoose.mongo.ObjectId();

      //Objeto de traduccion pensamiento
      let newTraduccionPensamiento = {
        pensamiento: idPensamiento,
        texto: PensamientoDto.traducciones[0].texto,
        idioma: codIdioma,
        original: true,
        estado: codEstadoTra,
      };

      //Guarda la traduccion del pensamiento
      const guardaTradPensamiento = new this.traduccionPensamientoModel(
        newTraduccionPensamiento,
      );
      const traduccionPensamiento = await guardaTradPensamiento.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(traduccionPensamiento));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionPensamiento: any = {
        datos: dataTraduccion,
        usuario: PensamientoDto.perfil._id,
        accion: getAccion,
        entidad: codEntTra,
      };

      //Guarda el historico de la traduccion del pensamiento
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionPensamiento,
      );

      //Lista de traducciones pensamientos
      let codTraduccionPensamiento = [];
      codTraduccionPensamiento.push(traduccionPensamiento['_id']);

      //Objeto de pensamientos
      let newPensamiento = {
        _id: idPensamiento,
        perfil: PensamientoDto.perfil._id,
        traducciones: codTraduccionPensamiento,
        publico: PensamientoDto.publico,
        estado: codEstado,
        fechaActualizacion: new Date(),
      };

      //Guarda el pensamiento
      const guardaPensamiento = new this.pensamientoModel(newPensamiento);
      const crearPensamiento = await guardaPensamiento.save(opts);

      const dataPensamiento = JSON.parse(JSON.stringify(crearPensamiento));
      delete dataPensamiento.fechaCreacion;
      delete dataPensamiento.fechaActualizacion;
      delete dataPensamiento.__v;

      //datos para guardar el historico de pensamiento
      let newHistoricoPensamiento: any = {
        datos: dataPensamiento, //"Creación de pensamiento. idPensamiento: ".concat(crearPensamiento._id.toString()),
        usuario: PensamientoDto.perfil._id,
        accion: getAccion,
        entidad: entidad.codigo,
      };

      //Guarda el historico del pensamiento
      this.crearHistoricoService.crearHistoricoServer(newHistoricoPensamiento);

      //Obtiene los pensamientos uno a uno
      const obtenerPensamiento = await this.pensamientoModel
        .findOne(
          { _id: crearPensamiento._id },
          { _id: 1, traducciones: 1, publico: 1, fechaActualizacion: 1 },
        )
        .session(session)
        .populate({
          path: 'traducciones',
          select: '-_id texto',
        });

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      this.traducirPensamientoSegundoPlanoService.TraducirPensamiento(
        crearPensamiento._id,
        crearPensamiento.perfil,
        idiomaDetectado,
      );

      //Retorna datos
      return obtenerPensamiento;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
