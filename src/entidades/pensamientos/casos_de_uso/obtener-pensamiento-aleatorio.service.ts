import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { TraduccionPensamiento } from 'src/drivers/mongoose/interfaces/traduccion_pensamiento/traduccion-pensamiento.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { TraducirPensamientoService } from './traducir-pensamiento.service';

@Injectable()
export class ObtenerPensamientoAleatorioService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    @Inject('TRADUCCION_PENSAMIENTO_MODEL')
    private readonly traduccionPensamientoModel: Model<TraduccionPensamiento>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private traducirPensamientoService: TraducirPensamientoService,
    private readonly funcion: Funcion,
  ) {}

  async obtenerPensamientoAleatorio(idioma: string): Promise<any> {
    const respuesta = new RespuestaInterface();

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.connections[1].startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.pensamientos,
      );

      //Obtiene el estado activo
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      let codEstado = estado.codigo;

      const codIdiomaQuery = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      let codIdioma = codIdiomaQuery.codigo;

      //Obtiene la entidad traduccion pensamientos
      const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionPensamientos,
      );
      let codEntTra = entidadTra.codigo;

      //Obtiene el estado de activa traduccion pensamientos
      const estadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntTra,
      );
      let codEstadoTra = estadoTra.codigo;

      console.log('codEstado: ', codEstado);
      const matchTraduccion = {
        $match: {
          publico: true,
          estado: codEstado,
        },
      };
      const projectTraduccion = {
        $project: {
          _id: 1,
          traducciones: 1,
          fechaActualizacion: 1,
          pensamiento: 1,
        },
      };

      const lookupTraduccion = {
        $lookup: {
          from: 'traduccion_pensamiento',
          let: { post_id: '$_id' },
          pipeline: [
            {
              $match: {
                // original: true,
                idioma: codIdioma,
                estado: codEstadoTra,
                $expr: {
                  $and: [{ $gt: ['$_id', '$$post_id'] }],
                },
              },
            },
            {
              $project: {
                texto: 1,
              },
            },
            {
              $sample: {
                size: 1,
              },
            },
          ],
          as: 'traducciones',
        },
      };

      //Obtiene los pensamientos aleatorios
      const ListPensamientos = await this.pensamientoModel.find({
        publico: true,
        estado: codEstado,
      });
      let arrayPensamientos = [];
      if (ListPensamientos.length > 0) {
        for (const pensamiento of ListPensamientos) {
          arrayPensamientos.push(pensamiento._id);
        }
        //Obtiene pensamientos aleatorios
        const getPensamientos = await this.traduccionPensamientoModel
          .aggregate([
            {
              $match: {
                idioma: codIdioma,
                estado: codEstadoTra,
                pensamiento: { $in: arrayPensamientos },
              },
            },
            { $sample: { size: 1 } },
            // lookupTraduccion,
            // projectTraduccion
            {
              $project: {
                _id: 1,
                texto: 1,
                pensamiento: 1,
                fechaActualizacion: 1,
              },
            },
          ])
          .limit(1);

        if (getPensamientos[0]) {
          let objTraducciones = {
            _id: getPensamientos[0]._id,
            texto: getPensamientos[0].texto,
          };
          let arrayTraducciones = [];
          arrayTraducciones.push(objTraducciones);

          let devolverPensamiento = {
            _id: getPensamientos[0].pensamiento,
            traducciones: arrayTraducciones,
            fechaActualizacion: getPensamientos[0].fechaActualizacion,
          };

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: devolverPensamiento,
          });
        } else {
          //Finaliza la transaccion
          await session.endSession();
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: {},
          });
        }
      } else {
        //Finaliza la transaccion
        await session.endSession();
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: {},
        });
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
    }
  }
}
