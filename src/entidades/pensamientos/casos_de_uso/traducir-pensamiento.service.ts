import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { TraduccionPensamiento } from 'src/drivers/mongoose/interfaces/traduccion_pensamiento/traduccion-pensamiento.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class TraducirPensamientoService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    @Inject('TRADUCCION_PENSAMIENTO_MODEL')
    private readonly traduccionPensamientoModel: Model<TraduccionPensamiento>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async TraducirPensamiento(
    idPensamiento,
    idTraduccionPensamiento: string,
    perfil: any,
    idioma: string,
    opts?: any,
  ): Promise<any> {
    //Obtiene el codigo del idioma
    const getCodIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      idioma,
    );
    let codIdioma = getCodIdioma.codigo;

    //Obtiene la accion crear
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene la entidad pensaientos
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.pensamientos,
    );
    let codEntidad = entidad.codigo;

    //Obtiene el estado de activa
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    let codEstado = estado.codigo;

    //Obtiene la entidad traduccion pensamientos
    const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionPensamientos,
    );
    let codEntTra = entidadTra.codigo;

    //Obtiene el estado de activa traduccion pensamientos
    const estadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntTra,
    );
    let codEstadoTra = estadoTra.codigo;

    try {
      //Obtiene la traduccion en el idioma original
      const getTraductorOriginal = await this.traduccionPensamientoModel
        .findOne({
          _id: idTraduccionPensamiento,
          original: true,
          estado: codEstadoTra,
        })
        .session(opts.session);

      //llama al metodo de traduccion dinamica
      const textoTraducido = await traducirTexto(
        idioma,
        getTraductorOriginal.texto,
      );

      //datos para guardar la nueva traduccion
      let newTraduccionPensamiento = {
        pensamiento: idPensamiento,
        texto: textoTraducido.textoTraducido,
        idioma: codIdioma,
        original: textoTraducido.original,
        estado: codEstadoTra,
      };

      //guarda la nueva traduccion
      const traduccion = new this.traduccionPensamientoModel(
        newTraduccionPensamiento,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      if (!perfil) {
        perfil = null;
      }
      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionPensamiento: any = {
        datos: dataTraduccion, //"Creación de traducción de pensamiento. idTraduccionPensamiento: ".concat(crearTraduccion._id.toString()),
        usuario: perfil,
        accion: getAccion,
        entidad: entidad.codigo,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionPensamiento,
      );

      //Actualiza el array de id de traducciones
      await this.pensamientoModel.updateOne(
        { _id: idPensamiento },
        { $push: { traducciones: crearTraduccion._id } },
        opts,
      );

      let objTraduccion = {
        _id: crearTraduccion._id,
        texto: crearTraduccion.texto,
      };
      return objTraduccion;
    } catch (error) {
      throw error;
    }
  }
}
