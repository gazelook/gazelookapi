import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { TraduccionPensamiento } from 'src/drivers/mongoose/interfaces/traduccion_pensamiento/traduccion-pensamiento.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codigosHibernadoEntidades,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { UpdatePensamientoDto } from '../entidad/update-pensamiento-dto';
import { TraducirPensamientoSegundoPlanoService } from './traducir-pensamiento-segundo-plano.service';
const mongoose = require('mongoose');

@Injectable()
export class ActualizacionPensamientoService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    @Inject('TRADUCCION_PENSAMIENTO_MODEL')
    private readonly traduccionPensamientoModel: Model<TraduccionPensamiento>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private traducirPensamientoSegundoPlanoService: TraducirPensamientoSegundoPlanoService,
  ) {}

  async actualizacionPensamiento(
    updatePensamientoDto: UpdatePensamientoDto,
    idioma: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    let session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el codigo de la accion a crear
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccionCrear = accionCrear.codigo;

      //Obtiene el codigo de la accion a modificar
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccionModificar = accionModificar.codigo;

      //Obtiene el codigo de la accion a eliminar
      const accionEliminar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      let getAccionEliminar = accionEliminar.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.pensamientos,
      );

      //Obtiene el estado activo del pensamiento
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad traduccion pensamientos
      const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionPensamientos,
      );
      let codEntTra = entidadTra.codigo;

      //Obtiene el estado eliminado de la traduccion del pensamiento
      const estadoActiva = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntTra,
      );
      let codEstadoActiva = estadoActiva.codigo;

      //detectaar el idioma que se envia el texto
      let textoTraducido = await traducirTexto(
        idioma,
        updatePensamientoDto.traducciones[0].texto,
      );
      let idiomaDetectado = textoTraducido.idiomaDetectado;

      //Obtiene el idioma por el codigo
      const codIdiomaQuery = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idiomaDetectado,
      );
      let codIdioma = codIdiomaQuery.codigo;

      //Obtiene el pensamiento

      const getPensamientos = await this.pensamientoModel
        .findOne({ _id: updatePensamientoDto._id, estado: codEstado })
        .populate('traducciones');

      if (getPensamientos) {
        const perfil = getPensamientos.perfil;

        for (let i = 0; i < getPensamientos.traducciones.length; i++) {
          //Identificador de la traduccion del pensamiento
          const getIdTraduccionPensamiento =
            getPensamientos.traducciones[i]['_id'];

          //Obtiene el estado eliminado de la traduccion del pensamiento
          const estadoEliminado = await this.catalogoEstadoService.obtenerNombreEstado(
            nombrecatalogoEstados.eliminado,
            codEntTra,
          );
          let codEstado_eliminado = estadoEliminado.codigo;

          let date = new Date();
          await this.traduccionPensamientoModel.updateOne(
            { _id: getIdTraduccionPensamiento },
            { estado: codEstado_eliminado, fechaActualizacion: date },
            opts,
          );

          let dataTraduccion = {
            perfil: getPensamientos.perfil,
            _id: getIdTraduccionPensamiento,
            texto: updatePensamientoDto.traducciones[0].texto,
          };
          //datos para guardar el historico de la traduccion del pensamiento
          let newHistoricoTraduccionPensamiento: any = {
            datos: dataTraduccion, //"Cambio de estado de traducción de pensamiento a eliminado. idTraduccionPensamiento: ".concat(getIdTraduccionPensamiento),
            usuario: perfil,
            accion: getAccionEliminar,
            entidad: codEntTra,
          };

          //Guarda el historico de la traduccion del pensamiento
          this.crearHistoricoService.crearHistoricoServer(
            newHistoricoTraduccionPensamiento,
          );
        }

        //Objeto de traduccion pensamiento
        let newTraduccionPensamiento = {
          pensamiento: getPensamientos._id,
          texto: updatePensamientoDto.traducciones[0].texto,
          idioma: codIdioma,
          original: true,
          estado: codEstadoActiva,
        };

        //Guarda la traduccion del pensamiento
        const traduccionPensamiento = new this.traduccionPensamientoModel(
          newTraduccionPensamiento,
        );
        const crearTraduccionPensamiento = await traduccionPensamiento.save(
          opts,
        );

        const dataTraduccion = JSON.parse(
          JSON.stringify(crearTraduccionPensamiento),
        );
        delete dataTraduccion.fechaCreacion;
        delete dataTraduccion.fechaActualizacion;
        delete dataTraduccion.__v;

        //datos para guardar el historico de la traduccion del pensamiento
        let newHistoricoTraduccionPensamiento: any = {
          datos: dataTraduccion, //"Creacion de traducción de pensamiento. idTraduccionPensamiento: ".concat(crearTraduccionPensamiento._id.toString()),
          usuario: perfil,
          accion: getAccionCrear,
          entidad: codEntTra,
        };

        //Guarda el historico de la traduccion del pensamiento
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoTraduccionPensamiento,
        );

        //Actualiza el array de id de traducciones
        await this.pensamientoModel.updateOne(
          { _id: updatePensamientoDto._id },
          {
            $push: { traducciones: crearTraduccionPensamiento._id },
            fechaActualizacion: new Date(),
          },
          opts,
        );

        let datosUpdate = {
          _id: getPensamientos._id,
          perfil: getPensamientos.perfil,
          traducciones: {
            _id: crearTraduccionPensamiento._id,
          },
        };
        //datos para guardar el historico de la traduccion del pensamiento
        let newHistoricoPensamiento: any = {
          datos: datosUpdate,
          usuario: getPensamientos.perfil,
          accion: getAccionModificar,
          entidad: entidad.codigo,
        };

        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoPensamiento,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        this.traducirPensamientoSegundoPlanoService.TraducirPensamiento(
          getPensamientos._id,
          getPensamientos.perfil,
          idiomaDetectado,
        );

        return crearTraduccionPensamiento;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return getPensamientos;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async activarPensamiento(idPerfil: string, opts: any): Promise<any> {
    try {
      //Obtiene el codigo de la accion
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccionModificar = accionModificar.codigo;

      //Obtiene la entidad pensamientos
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.pensamientos,
      );

      //Obtiene el estado activa del pensamiento
      const estadoPens = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      let codEstadoPens = estadoPens.codigo;

      //Obtiene la entidad traduccion pensamientos
      const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionPensamientos,
      );
      let codEntTra = entidadTra.codigo;

      //Obtiene el estado activa de la traduccion del pensamiento
      const estadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntTra,
      );
      let codEstadoActivaTra = estadoTra.codigo;

      //Obtiene el pensamiento
      const getPensamientos = await this.pensamientoModel
        .find({
          perfil: idPerfil,
          estado: codigosHibernadoEntidades.pensamientoHibernado,
        })
        .session(opts.session)
        .populate('traducciones');

      if (getPensamientos.length > 0) {
        /* for (let i = 0; i < getPensamiento.traducciones.length; i++) {

          //Identificador de la traduccion del pensamiento
          const getIdTraduccionPensamiento = getPensamiento.traducciones[i]['_id']

          //Actualiza la traducción del pensamiento a activado
          let date = new Date();
          await this.traduccionPensamientoModel.updateOne({ _id: getIdTraduccionPensamiento }, { estado: codEstadoActivaTra, fechaActualizacion: date })
        } */

        //Actualiza el pensamiento a activado
        let datosModificado;
        let date = new Date();
        const dataUpdate = {
          estado: codEstadoPens,
          fechaActualizacion: date,
        };
        for (const pensamiento of getPensamientos) {
          const pensaUpdate = await this.pensamientoModel.findByIdAndUpdate(
            pensamiento._id,
            dataUpdate,
            opts,
          );
          datosModificado = {
            perfil: pensaUpdate.perfil,
            _id: pensaUpdate._id,
            pensamiento: pensaUpdate,
          };
        }

        let newHistoricoPensamiento: any = {
          datos: datosModificado,
          usuario: idPerfil,
          accion: getAccionModificar,
          entidad: entidad.codigo,
        };
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoPensamiento,
        );

        return true;
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }
}
