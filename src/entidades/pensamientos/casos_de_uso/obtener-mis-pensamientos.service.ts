import { Inject, Injectable } from '@nestjs/common';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { TraducirPensamientoService } from './traducir-pensamiento.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
const mongoose = require('mongoose');

@Injectable()
export class ObtenerMisPensamientosService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: PaginateModel<Pensamiento>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private readonly catalogoIdiomasService: CatalogoIdiomasService,
    private traducirPensamientoService: TraducirPensamientoService,
  ) {}

  async obtenerMisPensamientos(
    idPerfil: string,
    idioma: string,
    limit: number,
    page: number,
    response?,
    traducir?,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let catalogoIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      const getCodIdioma = catalogoIdioma.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.pensamientos,
      );

      //Obtiene el estado activo
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad traduccion pensamientos
      const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionPensamientos,
      );
      let codEntTra = entidadTra.codigo;

      //Obtiene el estado de activa traduccion pensamientos
      const estadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntTra,
      );
      let codEstadoTra = estadoTra.codigo;

      let populateTraduccion;

      if (traducir) {
        populateTraduccion = {
          path: 'traducciones',
          select:
            ' -idioma -estado -fechaCreacion -fechaActualizacion -__v -pensamiento',
          match: {
            idioma: getCodIdioma,
            estado: codEstadoTra,
          },
        };
      } else {
        populateTraduccion = {
          path: 'traducciones',
          select:
            ' -idioma -estado -fechaCreacion -fechaActualizacion -__v -pensamiento',
          match: {
            original: true,
            estado: codEstadoTra,
          },
        };
      }

      const options = {
        lean: true,
        sort: { fechaActualizacion: -1 },
        select:
          ' -perfil -publico -estado -fechaCreacion -fechaActualizacionSistema -__v',
        populate: [populateTraduccion],
        page: Number(page),
        limit: Number(limit),
      };

      //Obtiene los pensamientos mas recientes

      const allPensamientos = await this.pensamientoModel.paginate(
        { perfil: idPerfil, estado: codEstado, publico: true },
        options,
      );

      // if (traduce == 'true') {
      //Variable para identificar si una traduccion es la original
      let IdentificadorTraduccionPensamiento: any;
      //Verifica si tiene pensamientos
      if (allPensamientos.docs.length > 0) {
        //Bandera para verificar si existe la traduccion del pensamiento
        let existe: boolean;

        let pensamientos = [];
        //Recorre todos los pensamientos obtenidos
        for (let i = 0; i < allPensamientos.docs.length; i++) {
          //variable para almacenar el id del pensamiento
          let idPensamiento = allPensamientos.docs[i]._id;

          if (allPensamientos.docs[i].traducciones.length > 0) {
            pensamientos.push(allPensamientos.docs[i]);
            existe = true;
          } else {
            existe = false;

            //Obtiene los pensamientos uno a uno
            let obtenerPensamiento = await this.pensamientoModel
              .findOne({ _id: idPensamiento, estado: codEstado })
              .populate({
                path: 'traducciones',
                match: {
                  original: true,
                  estado: codEstadoTra,
                },
              })
              .session(opts.session);

            const dataTraduccion = JSON.parse(
              JSON.stringify(obtenerPensamiento.traducciones[0]),
            );
            IdentificadorTraduccionPensamiento = dataTraduccion._id;
          }

          //Si la traduccion no existe la crea
          if (existe === false) {
            const pensamientoTrad = await this.traducirPensamientoService.TraducirPensamiento(
              idPensamiento,
              IdentificadorTraduccionPensamiento,
              idPerfil,
              idioma,
              opts,
            );
            let pensamiento = {
              _id: idPensamiento,
              traducciones: [pensamientoTrad],
              fechaActualizacion: allPensamientos.docs[i].fechaActualizacion,
            };
            pensamientos.push(pensamiento);
          }
        }

        response.set(headerNombre.totalDatos, allPensamientos.totalDocs);
        response.set(headerNombre.totalPaginas, allPensamientos.totalPages);
        response.set(headerNombre.proximaPagina, allPensamientos.hasNextPage);
        response.set(headerNombre.anteriorPagina, allPensamientos.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return pensamientos;
      } else {
        response.set(headerNombre.totalDatos, allPensamientos.totalDocs);
        response.set(headerNombre.totalPaginas, allPensamientos.totalPages);
        response.set(headerNombre.proximaPagina, allPensamientos.hasNextPage);
        response.set(headerNombre.anteriorPagina, allPensamientos.hasPrevPage);
        //Finaliza la transaccion
        await session.endSession();
        return allPensamientos.docs;
      }
      // } else {
      //   //Verifica si tiene pensamientos
      //   if (allPensamientos.docs.length > 0) {
      //     for (let index = 0; index < allPensamientos.docs.length; index++) {
      //       delete allPensamientos.docs[index].id
      //     }
      //     response.set(headerNombre.totalDatos, allPensamientos.totalDocs);
      //     response.set(headerNombre.totalPaginas, allPensamientos.totalPages);
      //     response.set(headerNombre.proximaPagina, allPensamientos.hasNextPage);
      //     response.set(headerNombre.anteriorPagina, allPensamientos.hasPrevPage);

      //     //Finaliza la transaccion
      //     await session.endSession();
      //     return allPensamientos.docs;
      //   }else{
      //     response.set(headerNombre.totalDatos, allPensamientos.totalDocs);
      //     response.set(headerNombre.totalPaginas, allPensamientos.totalPages);
      //     response.set(headerNombre.proximaPagina, allPensamientos.hasNextPage);
      //     response.set(headerNombre.anteriorPagina, allPensamientos.hasPrevPage);
      //     //Finaliza la transaccion
      //     await session.endSession();
      //     return allPensamientos.docs;
      //   }
      // }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
