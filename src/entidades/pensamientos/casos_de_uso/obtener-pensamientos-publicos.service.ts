import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { TraducirPensamientoService } from './traducir-pensamiento.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import * as mongoose from 'mongoose';

@Injectable()
export class ObtenerPensamientoService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private traducirPensamientoService: TraducirPensamientoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
  ) {}

  async obtenerPensamientosPerfil(perfil: string, codigoIdioma): Promise<any> {
    const respuesta = new RespuestaInterface();
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.pensamientos,
      );

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad traduccion pensamientos
      const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionPensamientos,
      );
      let codEntTra = entidadTra.codigo;

      //Obtiene el estado de activa traduccion pensamientos
      const estadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntTra,
      );
      let codEstadoTra = estadoTra.codigo;

      const codIdiomaQuery = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codigoIdioma,
      );
      let codIdioma = codIdiomaQuery.codigo;

      //Obtiene los pensamientos publicos de un usuario(maximo tres los mas recientes)
      const getPensamientos = await this.pensamientoModel
        .find(
          { perfil: perfil, estado: codEstado, publico: true },
          { _id: 1, traducciones: 1, publico: 1, fechaActualizacion: 1 },
        )
        .populate({
          path: 'traducciones',
          select: 'texto',
          match: {
            estado: codEstadoTra,
            idioma: codIdioma,
          },
        })
        .limit(3)
        .sort({ fechaActualizacion: -1 });

      //Variable para identificar si una traduccion es la original
      let IdentificadorTraduccionPensamiento: string;

      //Verifica si tiene pensamientos
      if (getPensamientos.length > 0) {
        //Bandera para verificar si existe la traduccion del pensamiento
        let existe: boolean;

        //Recorre todos los pensamientos obtenidos
        for (let i = 0; i < getPensamientos.length; i++) {
          //variable para almacenar el id del pensamiento
          let idPensamiento = getPensamientos[i]._id;

          if (getPensamientos[i].traducciones.length > 0) {
            existe = true;
          } else {
            existe = false;

            //Obtiene los pensamientos uno a uno
            let obtenerPensamiento = await this.pensamientoModel
              .findOne({ _id: idPensamiento, estado: codEstado })
              .populate({
                path: 'traducciones',
                match: {
                  original: true,
                  estado: codEstadoTra,
                },
              });

            const dataTraduccion = JSON.parse(
              JSON.stringify(obtenerPensamiento.traducciones[0]),
            );
            IdentificadorTraduccionPensamiento = dataTraduccion._id;
          }

          //Si la traduccion no existe la crea
          if (existe === false) {
            await this.traducirPensamientoService.TraducirPensamiento(
              idPensamiento,
              IdentificadorTraduccionPensamiento,
              perfil,
              codigoIdioma,
              opts,
            );
          }
        }

        const allPensamientos = await this.pensamientoModel
          .find(
            { perfil: perfil, estado: codEstado, publico: true },
            { _id: 1, traducciones: 1, publico: 1, fechaActualizacion: 1 },
          )
          .session(opts.session)
          .populate({
            path: 'traducciones',
            select: 'texto',
            match: {
              estado: codEstadoTra,
              idioma: codIdioma,
            },
          })
          .limit(3)
          .sort({ fechaActualizacion: -1 });

        respuesta.codigoEstado = HttpStatus.OK;
        respuesta.respuesta = {
          datos: allPensamientos,
        };
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return respuesta;
      } else {
        //llama al metodo de traduccion estatica
        const ERROR_OBTENER = await this.traduccionEstaticaController.traduccionEstatica(
          codigoIdioma,
          'ERROR_OBTENER',
        );

        respuesta.codigoEstado = HttpStatus.OK;
        respuesta.respuesta = {
          datos: getPensamientos,
        };
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return respuesta;
      }
    } catch (error) {
      respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
      respuesta.respuesta = {
        mensaje: error.message,
      };
      return respuesta;
    }
  }
}
