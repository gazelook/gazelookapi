import { DBModule } from 'src/drivers/db_conection/db.module';
import { pensamientoProviders } from '../drivers/pensamientos.provider';
import { ObtenerPensamientoService } from './obtener-pensamientos-publicos.service';
import { CrearPensamientoService } from './crear-pensamientos.service';
import { CatalogoProviders } from 'src/entidades/catalogos/drivers/catalogo.provider';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { ObtenerPensamientoAleatorioService } from './obtener-pensamiento-aleatorio.service';
import { ObtenerMisPensamientosService } from './obtener-mis-pensamientos.service';
import { ActualizacionPensamientoService } from './actualizacion-pensamiento.service';
import { ActualizacionEstadoPensamientoService } from './actualizacion-estados-pensamientos.service';
import { EliminarPensamientoService } from './eliminar-pensamiento.service';
import { TraducirPensamientoService } from './traducir-pensamiento.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { ObtenerPensamientosPrivadosService } from './obtener-pensamientos-privados.service';
import { ObtenerMisPensamientosPrivadosService } from './obtener-mis-pensamientos-privados.service';
import { ObtenerPensamientosPerfilService } from './obtener-pensamientos-perfil.service';
import { EliminarPensamientoCompletoService } from './eliminar-pensamiento-completo.service';
import { Funcion } from 'src/shared/funcion';
import { Module } from '@nestjs/common';
import { TraducirPensamientoSegundoPlanoService } from './traducir-pensamiento-segundo-plano.service';

@Module({
  imports: [DBModule],
  providers: [
    ...pensamientoProviders,
    ...CatalogoProviders,
    ObtenerPensamientoService,
    ObtenerPensamientoAleatorioService,
    CatalogoIdiomasService,
    CatalogoEstadoService,
    CrearPensamientoService,
    CatalogoEntidadService,
    CatalogoAccionService,
    ObtenerMisPensamientosService,
    ActualizacionPensamientoService,
    ActualizacionEstadoPensamientoService,
    EliminarPensamientoService,
    TraducirPensamientoService,
    TraduccionEstaticaController,
    ObtenerPensamientosPrivadosService,
    ObtenerMisPensamientosPrivadosService,
    ObtenerPensamientosPerfilService,
    EliminarPensamientoCompletoService,
    Funcion,
    TraducirPensamientoSegundoPlanoService,
  ],
  exports: [
    ObtenerPensamientoService,
    ObtenerPensamientoAleatorioService,
    CrearPensamientoService,
    ObtenerMisPensamientosService,
    ActualizacionPensamientoService,
    ActualizacionEstadoPensamientoService,
    EliminarPensamientoService,
    TraducirPensamientoService,
    ObtenerPensamientosPrivadosService,
    ObtenerMisPensamientosPrivadosService,
    ObtenerPensamientosPerfilService,
    EliminarPensamientoCompletoService,
    TraducirPensamientoSegundoPlanoService,
  ],
  controllers: [],
})
export class PensamientoServicesModule {}
