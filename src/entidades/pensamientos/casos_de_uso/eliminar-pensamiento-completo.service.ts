import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { TraduccionPensamiento } from 'src/drivers/mongoose/interfaces/traduccion_pensamiento/traduccion-pensamiento.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class EliminarPensamientoCompletoService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    @Inject('TRADUCCION_PENSAMIENTO_MODEL')
    private readonly traduccionPensamientoModel: Model<TraduccionPensamiento>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
  ) {}

  async eliminarPensamientoCompleto(idPerfil: string, opts): Promise<any> {
    if (mongoose.isValidObjectId(idPerfil)) {
      try {
        //Obtiene la entidad pensamientos
        const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
          nombreEntidades.pensamientos,
        );

        //Obtiene el estado activa del pensamiento
        const estadoActiva = await this.catalogoEstadoService.obtenerNombreEstado(
          nombrecatalogoEstados.activa,
          entidad.codigo,
        );
        let codEstadoActiva = estadoActiva.codigo;

        //Obtiene el pensamiento
        const getPensamientos = await this.pensamientoModel
          .find({ perfil: idPerfil, estado: codEstadoActiva })
          .populate('traducciones');

        if (getPensamientos) {
          for (const itpensamiento of getPensamientos) {
            //Eliminar las traducciones
            await this.traduccionPensamientoModel.deleteMany(
              { pensamiento: itpensamiento._id },
              opts,
            );
            //elimina el pensamiento
            await this.pensamientoModel.deleteOne(
              { _id: itpensamiento._id },
              opts,
            );
          }

          return true;
        }
      } catch (error) {
        throw error;
      }
    } else {
      null;
    }
  }
}
