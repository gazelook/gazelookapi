import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { TraduccionPensamiento } from 'src/drivers/mongoose/interfaces/traduccion_pensamiento/traduccion-pensamiento.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import {
  codIdiomas,
  codigoEntidades,
  codigosCatalogoAcciones,
  estadosTraduccionPensamiento,
  idiomas,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
const mongoose = require('mongoose');

@Injectable()
export class TraducirPensamientoSegundoPlanoService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    @Inject('TRADUCCION_PENSAMIENTO_MODEL')
    private readonly traduccionPensamientoModel: Model<TraduccionPensamiento>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async TraducirPensamiento(idPensamiento, perfil, idioma): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la traduccion en el idioma original
      const getTraductorOriginal = await this.traduccionPensamientoModel
        .findOne({
          pensamiento: idPensamiento,
          original: true,
          estado: estadosTraduccionPensamiento.activa,
        })
        .session(opts.session);

      if (getTraductorOriginal) {
        if (idioma != idiomas.ingles) {
          await this.traducirPensamientoIngles(
            idPensamiento,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.espanol) {
          await this.traducirPensamientoEspaniol(
            idPensamiento,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.italiano) {
          await this.traducirPensamientoItaliano(
            idPensamiento,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.frances) {
          await this.traducirPensamientoFrances(
            idPensamiento,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.aleman) {
          await this.traducirPensamientoAleman(
            idPensamiento,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.portugues) {
          await this.traducirPensamientoPortugues(
            idPensamiento,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return true;
      }

      //Finaliza la transaccion
      await session.endSession();
      return true;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
  async traducirPensamientoIngles(
    idPensamiento,
    perfil,
    getTraductorOriginal: TraduccionPensamiento,
    opts: { session: any },
  ) {
    try {
      //llama al metodo de traduccion dinamica
      const textoTraducido = await traducirTexto(
        idiomas.ingles,
        getTraductorOriginal.texto,
      );

      //datos para guardar la nueva traduccion
      let newTraduccionPensamiento = {
        pensamiento: idPensamiento,
        texto: textoTraducido.textoTraducido,
        idioma: codIdiomas.ingles,
        original: false,
        estado: estadosTraduccionPensamiento.activa,
      };

      //guarda la nueva traduccion
      const traduccion = new this.traduccionPensamientoModel(
        newTraduccionPensamiento,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      if (!perfil) {
        perfil = null;
      }
      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionPensamiento: any = {
        datos: dataTraduccion, //"Creación de traducción de pensamiento. idTraduccionPensamiento: ".concat(crearTraduccion._id.toString()),
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionPensamiento,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionPensamiento,
      );

      //Actualiza el array de id de traducciones
      await this.pensamientoModel.updateOne(
        { _id: idPensamiento },
        { $push: { traducciones: crearTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  async traducirPensamientoEspaniol(
    idPensamiento,
    perfil,
    getTraductorOriginal: TraduccionPensamiento,
    opts: { session: any },
  ) {
    try {
      //llama al metodo de traduccion dinamica
      const textoTraducido = await traducirTexto(
        idiomas.espanol,
        getTraductorOriginal.texto,
      );

      //datos para guardar la nueva traduccion
      let newTraduccionPensamiento = {
        pensamiento: idPensamiento,
        texto: textoTraducido.textoTraducido,
        idioma: codIdiomas.espanol,
        original: false,
        estado: estadosTraduccionPensamiento.activa,
      };

      //guarda la nueva traduccion
      const traduccion = new this.traduccionPensamientoModel(
        newTraduccionPensamiento,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      if (!perfil) {
        perfil = null;
      }
      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionPensamiento: any = {
        datos: dataTraduccion, //"Creación de traducción de pensamiento. idTraduccionPensamiento: ".concat(crearTraduccion._id.toString()),
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionPensamiento,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionPensamiento,
      );

      //Actualiza el array de id de traducciones
      await this.pensamientoModel.updateOne(
        { _id: idPensamiento },
        { $push: { traducciones: crearTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  async traducirPensamientoFrances(
    idPensamiento,
    perfil,
    getTraductorOriginal: TraduccionPensamiento,
    opts: { session: any },
  ) {
    try {
      //llama al metodo de traduccion dinamica
      const textoTraducido = await traducirTexto(
        idiomas.frances,
        getTraductorOriginal.texto,
      );

      //datos para guardar la nueva traduccion
      let newTraduccionPensamiento = {
        pensamiento: idPensamiento,
        texto: textoTraducido.textoTraducido,
        idioma: codIdiomas.frances,
        original: false,
        estado: estadosTraduccionPensamiento.activa,
      };

      //guarda la nueva traduccion
      const traduccion = new this.traduccionPensamientoModel(
        newTraduccionPensamiento,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      if (!perfil) {
        perfil = null;
      }
      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionPensamiento: any = {
        datos: dataTraduccion, //"Creación de traducción de pensamiento. idTraduccionPensamiento: ".concat(crearTraduccion._id.toString()),
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionPensamiento,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionPensamiento,
      );

      //Actualiza el array de id de traducciones
      await this.pensamientoModel.updateOne(
        { _id: idPensamiento },
        { $push: { traducciones: crearTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  async traducirPensamientoItaliano(
    idPensamiento,
    perfil,
    getTraductorOriginal: TraduccionPensamiento,
    opts: { session: any },
  ) {
    try {
      //llama al metodo de traduccion dinamica
      const textoTraducido = await traducirTexto(
        idiomas.italiano,
        getTraductorOriginal.texto,
      );

      //datos para guardar la nueva traduccion
      let newTraduccionPensamiento = {
        pensamiento: idPensamiento,
        texto: textoTraducido.textoTraducido,
        idioma: codIdiomas.italiano,
        original: false,
        estado: estadosTraduccionPensamiento.activa,
      };

      //guarda la nueva traduccion
      const traduccion = new this.traduccionPensamientoModel(
        newTraduccionPensamiento,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      if (!perfil) {
        perfil = null;
      }
      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionPensamiento: any = {
        datos: dataTraduccion, //"Creación de traducción de pensamiento. idTraduccionPensamiento: ".concat(crearTraduccion._id.toString()),
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionPensamiento,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionPensamiento,
      );

      //Actualiza el array de id de traducciones
      await this.pensamientoModel.updateOne(
        { _id: idPensamiento },
        { $push: { traducciones: crearTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  async traducirPensamientoPortugues(
    idPensamiento,
    perfil,
    getTraductorOriginal: TraduccionPensamiento,
    opts: { session: any },
  ) {
    try {
      //llama al metodo de traduccion dinamica
      const textoTraducido = await traducirTexto(
        idiomas.portugues,
        getTraductorOriginal.texto,
      );

      //datos para guardar la nueva traduccion
      let newTraduccionPensamiento = {
        pensamiento: idPensamiento,
        texto: textoTraducido.textoTraducido,
        idioma: codIdiomas.portugues,
        original: false,
        estado: estadosTraduccionPensamiento.activa,
      };

      //guarda la nueva traduccion
      const traduccion = new this.traduccionPensamientoModel(
        newTraduccionPensamiento,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      if (!perfil) {
        perfil = null;
      }
      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionPensamiento: any = {
        datos: dataTraduccion, //"Creación de traducción de pensamiento. idTraduccionPensamiento: ".concat(crearTraduccion._id.toString()),
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionPensamiento,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionPensamiento,
      );

      //Actualiza el array de id de traducciones
      await this.pensamientoModel.updateOne(
        { _id: idPensamiento },
        { $push: { traducciones: crearTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  async traducirPensamientoAleman(
    idPensamiento,
    perfil,
    getTraductorOriginal: TraduccionPensamiento,
    opts: { session: any },
  ) {
    try {
      //llama al metodo de traduccion dinamica
      const textoTraducido = await traducirTexto(
        idiomas.aleman,
        getTraductorOriginal.texto,
      );

      //datos para guardar la nueva traduccion
      let newTraduccionPensamiento = {
        pensamiento: idPensamiento,
        texto: textoTraducido.textoTraducido,
        idioma: codIdiomas.aleman,
        original: false,
        estado: estadosTraduccionPensamiento.activa,
      };

      //guarda la nueva traduccion
      const traduccion = new this.traduccionPensamientoModel(
        newTraduccionPensamiento,
      );
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.fechaCreacion;
      delete dataTraduccion.fechaActualizacion;
      delete dataTraduccion.__v;

      if (!perfil) {
        perfil = null;
      }
      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionPensamiento: any = {
        datos: dataTraduccion, //"Creación de traducción de pensamiento. idTraduccionPensamiento: ".concat(crearTraduccion._id.toString()),
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.traduccionPensamiento,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionPensamiento,
      );

      //Actualiza el array de id de traducciones
      await this.pensamientoModel.updateOne(
        { _id: idPensamiento },
        { $push: { traducciones: crearTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
