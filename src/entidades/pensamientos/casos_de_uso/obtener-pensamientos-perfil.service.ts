import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { TraducirPensamientoService } from './traducir-pensamiento.service';

@Injectable()
export class ObtenerPensamientosPerfilService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private traducirPensamientoService: TraducirPensamientoService,
  ) {}
  // obtener los pensamientos para activarlos
  async obtenerPensamientosPorPerfil(perfil: string): Promise<any> {
    try {
      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.pensamientos,
      );

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      const getPensamientos = await this.pensamientoModel
        .find({
          $and: [{ perfil: perfil }, { estado: estado.codigo }],
        })
        .populate({
          path: 'traducciones',
        });
      return getPensamientos;
    } catch (error) {
      throw error;
    }
  }

  // obtener pensamientos publicos (perfil general, basico) limite 3
  async obtenerPerfilPensamientosRecientes(
    perfil: string,
    codNombreIdioma,
    limite: number,
    idPerfilOtros: string,
    opts?: any,
  ): Promise<any> {
    try {
      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.pensamientos,
      );

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad traduccion pensamientos
      const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionPensamientos,
      );
      let codEntTra = entidadTra.codigo;

      //Obtiene el estado de activa traduccion pensamientos
      const estadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntTra,
      );
      let codEstadoTra = estadoTra.codigo;

      const codIdiomaQuery = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codNombreIdioma,
      );
      let codIdioma = codIdiomaQuery.codigo;

      //Obtiene los pensamientos publicos de un usuario(maximo tres los mas recientes)
      let populateTraducciones;
      if (idPerfilOtros) {
        populateTraducciones = {
          path: 'traducciones',
          select: 'texto',
          match: {
            estado: codEstadoTra,
            idioma: codIdioma,
          },
        };
      } else {
        populateTraducciones = {
          path: 'traducciones',
          select: 'texto',
          match: {
            estado: codEstadoTra,
            original: true,
          },
        };
      }

      const getPensamientos = await this.pensamientoModel
        .find({ perfil: perfil, estado: codEstado, publico: true })
        .populate([populateTraducciones])
        .sort('-fechaActualizacion')
        .limit(limite)
        .session(opts.session);

      //Variable para identificar si una traduccion es la original
      let IdentificadorTraduccionPensamiento: string;

      //Verifica si tiene pensamientos
      if (getPensamientos.length > 0) {
        //Bandera para verificar si existe la traduccion del pensamiento
        let existe: boolean;

        let pensamientos = [];
        //Recorre todos los pensamientos obtenidos
        for (let i = 0; i < getPensamientos.length; i++) {
          //variable para almacenar el id del pensamiento
          let idPensamiento = getPensamientos[i]._id;

          if (getPensamientos[i].traducciones.length > 0) {
            pensamientos.push(getPensamientos[i]);
            existe = true;
          } else {
            existe = false;

            //Obtiene los pensamientos uno a uno
            let obtenerPensamiento = await this.pensamientoModel
              .findOne({ _id: idPensamiento, estado: codEstado })
              .populate({
                path: 'traducciones',
                match: {
                  original: true,
                  estado: codEstadoTra,
                },
              })
              .session(opts.session);

            const dataTraduccion = JSON.parse(
              JSON.stringify(obtenerPensamiento.traducciones[0]),
            );
            IdentificadorTraduccionPensamiento = dataTraduccion._id;
          }

          //Si la traduccion no existe la crea
          if (existe === false) {
            //llama al metodo para crear una nueva traduccion
            const pensamientoTrad = await this.traducirPensamientoService.TraducirPensamiento(
              idPensamiento,
              IdentificadorTraduccionPensamiento,
              perfil,
              codNombreIdioma,
              opts,
            );
            let pensamiento = {
              _id: idPensamiento,
              publico: true,
              traducciones: [pensamientoTrad],
              fechaActualizacion: getPensamientos[i].fechaActualizacion,
            };
            pensamientos.push(pensamiento);
          }
        }

        return pensamientos;
      }
      return getPensamientos;
    } catch (error) {
      throw error;
    }
  }
}
