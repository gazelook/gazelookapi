import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Pensamiento } from 'src/drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { TraduccionPensamiento } from 'src/drivers/mongoose/interfaces/traduccion_pensamiento/traduccion-pensamiento.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class EliminarPensamientoService {
  constructor(
    @Inject('PENSAMIENTO_MODEL')
    private readonly pensamientoModel: Model<Pensamiento>,
    @Inject('TRADUCCION_PENSAMIENTO_MODEL')
    private readonly traduccionPensamientoModel: Model<TraduccionPensamiento>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEstadoService: CatalogoEstadoService,
  ) {}

  async eliminarPensamiento(idPensamiento, opts?: any): Promise<any> {
    if (mongoose.isValidObjectId(idPensamiento)) {
      //Inicia la transaccion
      //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
      let session;
      let optsLocal;
      if (!opts) {
        session = await mongoose.startSession();
        session.startTransaction();
      }
      try {
        //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
        if (!opts) {
          opts = { session };
          optsLocal = true;
        }

        //Obtiene el codigo de la accion a eliminar
        const accionEliminar = await this.catalogoAccionService.obtenerNombreAccion(
          nombreAcciones.eliminar,
        );
        let getAccionEliminar = accionEliminar.codigo;

        //Obtiene la entidad pensamientos
        const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
          nombreEntidades.pensamientos,
        );

        //Obtiene el estado activa del pensamiento
        const estadoActiva = await this.catalogoEstadoService.obtenerNombreEstado(
          nombrecatalogoEstados.activa,
          entidad.codigo,
        );
        let codEstadoActiva = estadoActiva.codigo;

        //Obtiene el estado eliminado del pensamiento
        const estadoEliminadoPens = await this.catalogoEstadoService.obtenerNombreEstado(
          nombrecatalogoEstados.eliminado,
          entidad.codigo,
        );
        let codEstadoEliminadoPens = estadoEliminadoPens.codigo;

        //Obtiene la entidad traduccion pensamientos
        const entidadTra = await this.catalogoEntidadService.obtenerNombreEntidad(
          nombreEntidades.traduccionPensamientos,
        );
        let codEntTra = entidadTra.codigo;

        //Obtiene el estado eliminado de la traduccion del pensamiento
        const estadoEliminadoTra = await this.catalogoEstadoService.obtenerNombreEstado(
          nombrecatalogoEstados.eliminado,
          codEntTra,
        );
        let codEstadoEliminadoTra = estadoEliminadoTra.codigo;

        //Obtiene el pensamiento
        const getPensamientos = await this.pensamientoModel
          .findOne({ _id: idPensamiento, estado: codEstadoActiva })
          .populate('traducciones');
        if (getPensamientos) {
          for (const getTraducciones of getPensamientos.traducciones) {
            //Identificador de la traduccion del pensamiento
            const getIdTraduccionPensamiento = getTraducciones['_id'];

            //Actualiza la traducción del pensamiento a eliminado
            //Actualiza el estado a hibernado del proyecto
            await this.traduccionPensamientoModel.updateOne(
              { _id: getIdTraduccionPensamiento },
              {
                $set: {
                  estado: codEstadoEliminadoTra,
                  fechaActualizacion: new Date(),
                },
              },
              opts,
            );
          }
          //Actualiza el pensamiento a eliminado
          await this.pensamientoModel.updateOne(
            { _id: getPensamientos._id },
            {
              $set: {
                estado: codEstadoEliminadoPens,
                fechaActualizacion: new Date(),
              },
            },
            opts,
          );

          let datosDelete = {
            perfil: getPensamientos.perfil,
            _id: getPensamientos._id,
          };
          //datos para guardar el historico de la traduccion del pensamiento
          let newHistoricoTraduccionPensamiento: any = {
            datos: datosDelete, //"Eliminación del pensamiento. idPensamiento: ".concat(getPensamientos._id),
            usuario: getPensamientos.perfil,
            accion: getAccionEliminar,
            entidad: entidad.codigo,
          };

          //Guarda el historico de la traduccion del pensamiento
          this.crearHistoricoService.crearHistoricoServer(
            newHistoricoTraduccionPensamiento,
          );

          if (optsLocal) {
            //Confirma los cambios de la transaccion
            await session.commitTransaction();
            //Finaliza la transaccion
            await session.endSession();
          }
          return getPensamientos;
        } else {
          if (optsLocal) {
            //Confirma los cambios de la transaccion
            await session.abortTransaction();
            //Finaliza la transaccion
            await session.endSession();
          }
          return getPensamientos;
        }
      } catch (error) {
        if (optsLocal) {
          //Aborta la transaccion
          await session.abortTransaction();
          //Finaliza la transaccion
          await session.endSession();
        }
        throw error;
      }
    } else {
      return null;
    }
  }

  async hibernarPensamiento(idPensamiento, opts): Promise<any> {
    if (mongoose.isValidObjectId(idPensamiento)) {
      try {
        //Obtiene el codigo de la accion a eliminar
        const accionMod = await this.catalogoAccionService.obtenerNombreAccion(
          nombreAcciones.modificar,
        );
        let getAccionMod = accionMod.codigo;

        //Obtiene la entidad pensamientos
        const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
          nombreEntidades.pensamientos,
        );

        //Obtiene el estado activa del pensamiento
        const estadoActiva = await this.catalogoEstadoService.obtenerNombreEstado(
          nombrecatalogoEstados.activa,
          entidad.codigo,
        );
        let codEstadoActiva = estadoActiva.codigo;

        //Obtiene el estado HIBERNADO del pensamiento
        const estadoHiberPens = await this.catalogoEstadoService.obtenerNombreEstado(
          nombrecatalogoEstados.hibernado,
          entidad.codigo,
        );
        let codEstadoHiberPens = estadoHiberPens.codigo;

        //Obtiene el pensamiento
        const getPensamientos = await this.pensamientoModel
          .findOne({ _id: idPensamiento, estado: codEstadoActiva })
          .session(opts.session)
          .populate('traducciones');

        if (getPensamientos) {
          //Actualiza el pensamiento a hibernado
          let date = new Date();
          await this.pensamientoModel.updateOne(
            { _id: getPensamientos._id },
            { estado: codEstadoHiberPens, fechaActualizacion: date },
            opts,
          );
          let datosHiber = {
            perfil: getPensamientos.perfil,
            _id: getPensamientos._id,
          };

          let newHistoricoPensam: any = {
            datos: datosHiber,
            usuario: getPensamientos.perfil,
            accion: getAccionMod,
            entidad: entidad.codigo,
          };
          //Crea el historico del proyecto
          this.crearHistoricoService.crearHistoricoServer(newHistoricoPensam);

          return getPensamientos;
        } else {
          return getPensamientos;
        }
      } catch (error) {
        //Aborta la transaccion
        throw error;
      }
    } else {
      return null;
    }
  }
}
