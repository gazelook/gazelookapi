import { Connection } from 'mongoose';
import { PensamientoModelo } from '../../../drivers/mongoose/modelos/pensamientos/pensamiento.schema';
import { TraduccionPensamientoModelo } from '../../../drivers/mongoose/modelos/traduccion_pensamientos/traduccion-pensamiento.schema';
import { perfilModelo } from '../../../drivers/mongoose/modelos/perfil/perfil.schema';

export const pensamientoProviders = [
  {
    provide: 'PENSAMIENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('pensamiento', PensamientoModelo, 'pensamiento'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_PENSAMIENTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_pensamiento',
        TraduccionPensamientoModelo,
        'traduccion_pensamiento',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION'],
  },
];
