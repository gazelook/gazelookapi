import {
  Controller,
  Headers,
  HttpStatus,
  Param,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { ActualizacionEstadoPensamientoService } from '../casos_de_uso/actualizacion-estados-pensamientos.service';
import { UpdateEstadoPensamientoDto } from '../entidad/update-estado-pensamiento-dto';

@ApiTags('Pensamientos')
@Controller('api/pensamiento')
export class ActualizacionEstadoPensamientoControlador {
  constructor(
    private readonly actualizacionEstadoPensamientoService: ActualizacionEstadoPensamientoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Put('/:idPensamiento')
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiOperation({
    summary:
      'Actualiza el estado de un pensamiento, si esta en público lo cambia a privado y viceversa (publico:true||privado:false)',
  })
  @ApiResponse({
    status: 200,
    type: UpdateEstadoPensamientoDto,
    description: 'Actualización correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizacionEstadoPensamiento(
    @Headers() headers,
    @Param('idPensamiento') idPensamiento: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (mongoose.isValidObjectId(idPensamiento)) {
      const getPensamiento = await this.actualizacionEstadoPensamientoService.actualizacionEstadoPensamiento(
        idPensamiento,
        codIdioma,
      );
      return getPensamiento;
    } else {
      //llama al metodo de traduccion estatica
      const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
        codIdioma,
        'PARAMETROS_NO_VALIDOS',
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
