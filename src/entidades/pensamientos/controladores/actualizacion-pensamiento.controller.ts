import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { ActualizacionPensamientoService } from '../casos_de_uso/actualizacion-pensamiento.service';
import { UpdatePensamientoDto } from '../entidad/update-pensamiento-dto';

@ApiTags('Pensamientos')
@Controller('api/pensamiento')
export class ActualizacionPensamientoControlador {
  constructor(
    private readonly actualizacionPensamientoService: ActualizacionPensamientoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Put('/')
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiOperation({ summary: 'Actualiza el contenido de un pensamiento' })
  @ApiResponse({
    status: 200,
    type: UpdatePensamientoDto,
    description: 'Actualización correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizacionPensamiento(
    @Headers() headers,
    @Body() updatePensamientoDTO: UpdatePensamientoDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(updatePensamientoDTO._id) &&
        updatePensamientoDTO.traducciones[0].texto
      ) {
        const getPensamiento = await this.actualizacionPensamientoService.actualizacionPensamiento(
          updatePensamientoDTO,
          codIdioma,
        );

        if (!getPensamiento || getPensamiento.length === 0) {
          //llama al metodo de traduccion dinamica
          const ERROR_ACTUALIZAR = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ERROR_ACTUALIZAR',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        } else {
          //llama al metodo de traduccion dinamica
          const ACTUALIZACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ACTUALIZACION_CORRECTA',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        }
      } else {
        //llama al metodo de traduccion estatica
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
