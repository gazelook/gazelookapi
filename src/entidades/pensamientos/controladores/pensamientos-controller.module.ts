import { Module } from '@nestjs/common';
import { ObtenerPensamientoControlador } from './obtener-pensamientos-publicos.controller';
import { PensamientoServicesModule } from '../casos_de_uso/pensamientos-services.module';
import { CrearPensamientoControlador } from './crear-pensamientos.controller';
import { ObtenerPensamientosAleatoriosControlador } from './obtener-pensamiento-aleatorio.controller';
import { ObtenerMisPensamientosControlador } from './obtener-mis-pensamientos.controller';
import { ActualizacionPensamientoControlador } from './actualizacion-pensamiento.controller';
import { ActualizacionEstadoPensamientoControlador } from './actualizacion-estado-pensamiento.controller';
import { EliminarPensamientoControlador } from './eliminar-pensamiento.controller';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { ObtenerPensamientosPrivadosControlador } from './obtener-pensamientos-privados.controller';
import { ObtenerMisPensamientosPrivadosControlador } from './obtener-mis-pensamientos-privados.controller';
import { Funcion } from 'src/shared/funcion';

@Module({
  imports: [PensamientoServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    ObtenerPensamientoControlador,
    CrearPensamientoControlador,
    ObtenerPensamientosAleatoriosControlador,
    ObtenerMisPensamientosControlador,
    ActualizacionPensamientoControlador,
    ActualizacionEstadoPensamientoControlador,
    EliminarPensamientoControlador,
    ObtenerPensamientosPrivadosControlador,
    ObtenerMisPensamientosPrivadosControlador,
  ],
})
export class PensamientoControllerModule {}
