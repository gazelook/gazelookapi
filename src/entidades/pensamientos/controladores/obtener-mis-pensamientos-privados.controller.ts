import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import * as mongoose from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { ObtenerMisPensamientosPrivadosService } from '../casos_de_uso/obtener-mis-pensamientos-privados.service';
import { ObtenerPensamientosAleatoriosResponseDto } from '../entidad/obtener-pensamiento-aleatorio-response-dto';

@ApiTags('Pensamientos')
@Controller('api/pensamiento')
export class ObtenerMisPensamientosPrivadosControlador {
  constructor(
    private readonly obtenerMisPensamientosPrivadosService: ObtenerMisPensamientosPrivadosService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Get('/misPensamientos/privados/')
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiOperation({ summary: 'Devuelve la lista de mis pensamientos (privados)' })
  @ApiResponse({
    status: 200,
    type: ObtenerPensamientosAleatoriosResponseDto,
    description: 'OK',
  })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerMisPensamientosPrivados(
    @Headers() headers,
    @Query('perfil') perfil: string,
    // @Query('traducir') traducir: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const headerNombre = new HadersInterfaceNombres();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(perfil) &&
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0
      ) {
        const getPensamiento = await this.obtenerMisPensamientosPrivadosService.obtenerMisPensamientosPrivados(
          perfil,
          codIdioma,
          limite,
          pagina,
          response,
        );

        if (getPensamiento.length === 0) {
          //llama al metodo de traduccion estatica
          //const ERROR_OBTENER= await this.traduccionEstaticaController.traduccionEstatica(codIdioma, 'ERROR_OBTENER');
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: getPensamiento,
          });
          response.send(respuesta);
          return respuesta;
        } else {
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: getPensamiento,
          });
          response.send(respuesta);
          return respuesta;
        }
      } else {
        //llama al metodo de traduccion estatica
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
