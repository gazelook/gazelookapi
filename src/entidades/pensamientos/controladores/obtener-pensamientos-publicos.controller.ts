import { ObtenerPensamientoService } from '../casos_de_uso/obtener-pensamientos-publicos.service';

import { ApiResponse, ApiTags, ApiHeader, ApiOperation } from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Get,
  Headers,
  Param,
  UseGuards,
} from '@nestjs/common';
import { ObtenerPensamientoResponseDto } from '../entidad/obtener-pensamiento-response-dto';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { AuthGuard } from '@nestjs/passport';
import * as mongoose from 'mongoose';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Pensamientos')
@Controller('api/pensamiento')
export class ObtenerPensamientoControlador {
  constructor(
    private readonly obtenerPensamientoService: ObtenerPensamientoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Get('/publico/:perfil')
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiOperation({
    summary:
      'Devuelve la lista de pensamientos públicos de un usuario(máximo tres, los mas recientes)',
  })
  @ApiResponse({
    status: 200,
    type: ObtenerPensamientoResponseDto,
    description: 'OK',
  })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerPensamiento(
    @Headers() headers,
    @Param('perfil') perfil: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    if (mongoose.isValidObjectId(perfil)) {
      const getPensamiento = await this.obtenerPensamientoService.obtenerPensamientosPerfil(
        perfil,
        codIdioma,
      );
      return getPensamiento;
    } else {
      //llama al metodo de traduccion estatica
      const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
        codIdioma,
        'PARAMETROS_NO_VALIDOS',
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
