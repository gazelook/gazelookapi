import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import {
  Controller,
  Post,
  Body,
  HttpStatus,
  Headers,
  UseGuards,
} from '@nestjs/common';
import { PensamientoDto } from '../entidad/pensamiento-dto';
import { CrearPensamientoService } from '../casos_de_uso/crear-pensamientos.service';
import { PensamientoResponseDto } from '../entidad/pensamiento-response-dto';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { AuthGuard } from '@nestjs/passport';
import * as mongoose from 'mongoose';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Pensamientos')
@Controller('api/pensamiento')
export class CrearPensamientoControlador {
  constructor(
    private readonly crearPensamientoService: CrearPensamientoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Crea un nuevo pensamiento.' })
  @ApiResponse({
    status: 201,
    type: PensamientoResponseDto,
    description: 'Creación correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al crear' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async crearPensamiento(
    @Headers() headers,
    @Body() crearPensamientoDTO: PensamientoDto,
  ) {
    const respuesta = new RespuestaInterface();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(crearPensamientoDTO.perfil._id) &&
        crearPensamientoDTO.traducciones[0].texto &&
        (crearPensamientoDTO.publico !== null ||
          crearPensamientoDTO.publico !== undefined)
      ) {
        const pensamiento = await this.crearPensamientoService.crearPensamiento(
          crearPensamientoDTO,
          codIdioma,
        );

        if (!pensamiento || pensamiento.length === 0) {
          //llama al metodo de traduccion dinamica
          const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ERROR_CREACION',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }

        //llama al metodo de traduccion dinamica
        const CREACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'CREACION_CORRECTA',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.CREATED,
          mensaje: CREACION_CORRECTA,
          datos: pensamiento,
        });
      } else {
        const PARAMETROS_NO_VALIDOS = await await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
