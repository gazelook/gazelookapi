import { ApiResponse, ApiTags, ApiHeader, ApiOperation } from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Headers,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { EliminarPensamientoService } from '../casos_de_uso/eliminar-pensamiento.service';
import { AuthGuard } from '@nestjs/passport';
import * as mongoose from 'mongoose';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Pensamientos')
@Controller('api/pensamiento')
export class EliminarPensamientoControlador {
  constructor(
    private readonly eliminarPensamientoService: EliminarPensamientoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Delete('/:idPensamiento')
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiOperation({ summary: 'Elimina el pensamiento logicamente' })
  @ApiResponse({ status: 200, description: 'Eliminación correcta' })
  @ApiResponse({ status: 404, description: 'No se puede eliminar' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async eliminarPensamientoPensamiento(
    @Headers() headers,
    @Param('idPensamiento') idPensamiento: string,
  ) {
    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

      if (mongoose.isValidObjectId(idPensamiento)) {
        const getPensamiento = await this.eliminarPensamientoService.eliminarPensamiento(
          idPensamiento,
        );

        if (!getPensamiento || getPensamiento.length === 0) {
          //llama al metodo de traduccion dinamica
          const ERROR_ELIMINAR = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ERROR_ELIMINAR',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ELIMINAR,
          });
        } else {
          //llama al metodo de traduccion dinamica
          const ELIMINACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ELIMINACION_CORRECTA',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        }
      } else {
        //llama al metodo de traduccion estatica
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.messaje,
      });
    }
  }
}
