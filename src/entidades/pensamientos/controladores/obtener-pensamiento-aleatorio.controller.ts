import { ApiResponse, ApiTags, ApiHeader, ApiOperation } from '@nestjs/swagger';
import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import { ObtenerPensamientosAleatoriosResponseDto } from '../entidad/obtener-pensamiento-aleatorio-response-dto';
import { ObtenerPensamientoAleatorioService } from '../casos_de_uso/obtener-pensamiento-aleatorio.service';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Pensamientos')
@Controller('api/pensamiento')
export class ObtenerPensamientosAleatoriosControlador {
  constructor(
    private readonly obtenerPensamientoAleatoriosService: ObtenerPensamientoAleatorioService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiOperation({ summary: 'Devuelve un pensamiento aleatorio público' })
  @ApiResponse({
    status: 200,
    type: ObtenerPensamientosAleatoriosResponseDto,
    description: 'OK',
  })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  public async obtenerPensamientosAleatorios(@Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const getPensamiento = await this.obtenerPensamientoAleatoriosService.obtenerPensamientoAleatorio(
        codIdioma,
      );
      return getPensamiento;
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
    }
  }
}
