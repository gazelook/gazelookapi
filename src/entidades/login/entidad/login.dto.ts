import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class LoginDto {
  @ApiProperty()
  @IsNotEmpty()
  email: string;
  @ApiProperty()
  @IsNotEmpty()
  contrasena: string;
}

export class LoginDispositivoDto {
  @ApiProperty()
  ipRequest: string;
  @ApiProperty()
  userAgent: any;
}
