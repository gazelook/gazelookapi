import { ApiProperty } from '@nestjs/swagger';

export class TokenRefreshDto {
  @ApiProperty({
    description: 'Nuevo token refresh para utilizarlo la proxima vez',
    required: true,
    example:
      'tU78zIBBlcnQVr18H34vQvBWK9z8ykmmoIakLQqbe8G7dvYBasRvb2dNkjvFPecf9bPDeSIVbNLYQOJEkuzJxxi44V3AlxrzAQDSD4Om3aCRvWeRoqfrA6qXu4mXEYgaG95eTm1Rp7KDS4126ZTFhKWdEzEIoITB9VWAGaRFCKROmrT4UP6AZoHrSTv2aclJqrNVQugwH8ot0zMcivA8XVcYLZ5ATbtChdET0tDr2wqvoAOcOyphUBRxLAlSFVgb',
  })
  tokenRefresh: string;
}

export class TokenRefreshResponseDto {
  @ApiProperty({
    description: 'Nuevo token valido',
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Imxlb25hcmRvLWRhbmllbGwwNkBob3RtYWlsLmNvbSIsImlhdCI6MTU5ODQ3MDI4NywiZXhwIjoxNTk4NTEzNDg3fQ.DEY0iq0cIr8b_MIf9J2M0Hra0-_84Emq1_AaRZzzgIM',
  })
  tokenAccess: string;
  @ApiProperty({
    description: 'Nuevo token refresh para utilizarlo la proxima vez',
    required: true,
    example:
      'tU78zIBBlcnQVr18H34vQvBWK9z8ykmmoIakLQqbe8G7dvYBasRvb2dNkjvFPecf9bPDeSIVbNLYQOJEkuzJxxi44V3AlxrzAQDSD4Om3aCRvWeRoqfrA6qXu4mXEYgaG95eTm1Rp7KDS4126ZTFhKWdEzEIoITB9VWAGaRFCKROmrT4UP6AZoHrSTv2aclJqrNVQugwH8ot0zMcivA8XVcYLZ5ATbtChdET0tDr2wqvoAOcOyphUBRxLAlSFVgb',
  })
  tokenRefresh: string;
}
