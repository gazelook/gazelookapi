import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';
import { ConfigService } from '../../../config/config.service';
import { estadosDispositivo } from '../../../shared/enum-sistema';
import { ObtenerDispositivoUsuarioService } from '../../dispositivos/casos_de_uso/obtener-dispositivo-usuario.service';
import { PayloadTokenDto } from '../entidad/token.dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    private readonly obIdUsService: ObtenerIdUsuarioService,
    private readonly obtenerDispositivoUsuarioService: ObtenerDispositivoUsuarioService,
    private config: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get<string>('JWT_SECRET'),
    });
  }

  async validate(payload: PayloadTokenDto) {
    let user = await this.obIdUsService.obtenerUsuarioByEmail(payload.email);
    const dispositivo = await this.obtenerDispositivoUsuarioService.getDispositivoById(
      payload.dispositivo,
    );

    if (
      dispositivo &&
      user &&
      dispositivo.estado === estadosDispositivo.activo
    ) {
      user.dispositivos = [dispositivo];
      return { user, dispositivo };
    }
    return null;
  }
}
