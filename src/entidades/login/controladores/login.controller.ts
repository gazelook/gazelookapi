import {
  Controller,
  Headers,
  HttpStatus,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Request } from 'express';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { Usuario } from '../../../drivers/mongoose/interfaces/usuarios/usuario.interface';
import { estadosUsuario } from '../../../shared/enum-sistema';
import { RetornoValidarCuentaDto } from '../../cuenta/entidad/retorno-validar-cuenta.dto';
import { LoginService } from '../casos_de_uso/login.service';
import { LocalAuthGuard } from '../drivers/local-auth.guard';
import { LoginDispositivoDto, LoginDto } from '../entidad/login.dto';

@ApiTags('Autenticación')
@Controller('api/auth')
export class LoginController {
  constructor(
    private readonly authService: LoginService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  // funcion = new Funcion(this.i18n);

  @ApiBody({ type: LoginDto })
  @ApiOperation({
    summary:
      'se debe enviar usuario y contraseña para obtencion de token, el contrasena es el email',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiResponse({
    status: 201,
    type: RetornoValidarCuentaDto,
    description: 'created',
  })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @UseGuards(LocalAuthGuard)
  @Post('/iniciar-sesion')
  async login(@Req() req: Request, @Headers() headers) {
    let codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const os = require('os');
      console.log('hostnamePeticion:', req.hostname);
      console.log('os.homedir():', os.homedir());
      console.log(
        'os.hostname():',
        os.hostname(),
      ); /*
      console.log("os.networkInterfaces():", os.networkInterfaces());
      console.log("os.type(): ",os.type());
      console.log("userAgent: headers['userLogin-agent'],: ", headers['userLogin-agent']) */

      let dataDispositivo: LoginDispositivoDto = {
        ipRequest: req.ip,
        userAgent: headers['userLogin-agent'],
      };

      const reqUser = req.user as Usuario;

      const userLogin = await this.authService.login(
        reqUser,
        codIdioma,
        dataDispositivo,
      );

      console.log('userLogin: ----------------->', userLogin);
      if (userLogin) {
        let mensaje;
        if (userLogin.usuario.estado.codigo === estadosUsuario.inactivaPago) {
          const INACTIVA_PAGO = await this.i18n.translate(
            codIdioma.concat('.INACTIVA_PAGO'),
            {
              lang: codIdioma,
            },
          );
          mensaje = INACTIVA_PAGO;
        }
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: mensaje,
          datos: {
            usuario: userLogin.usuario,
            tokenAccess: userLogin.accessToken,
            tokenRefresh: userLogin.tokenRefresh,
          },
        });
      } else {
        const CREDENCIALES_INCORRECTAS = await this.i18n.translate(
          codIdioma.concat('.CREDENCIALES_INCORRECTAS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.UNAUTHORIZED,
          mensaje: CREDENCIALES_INCORRECTAS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
