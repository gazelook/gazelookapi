import { LogoutController } from './logout.controller';
import { LoginController } from './login.controller';
import { RefrescarTokenController } from './refrescar-token.controller';
import { LoginServicesModule } from './../casos_de_uso/login-services.module';
import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';

@Module({
  imports: [LoginServicesModule],
  providers: [Funcion],
  exports: [],
  controllers: [RefrescarTokenController, LoginController, LogoutController],
})
export class loginControllerModule {}
