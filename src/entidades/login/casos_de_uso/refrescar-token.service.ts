import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TokenInterface } from 'src/drivers/mongoose/interfaces/token_usuario/token.interface';
import { Model } from 'mongoose';
import { uid } from 'rand-token';
import * as mongoose from 'mongoose';
import { ObtenerDispositivoUsuarioService } from '../../dispositivos/casos_de_uso/obtener-dispositivo-usuario.service';
import { estadosDispositivo } from '../../../shared/enum-sistema';
import { erroresGeneral } from '../../../shared/enum-errores';
import { PayloadTokenDto } from '../entidad/token.dto';

@Injectable()
export class RefrescarTokenService {
  constructor(
    private readonly jwtService: JwtService,
    @Inject('TOKEN_USUARIO_MODEL')
    private readonly tokenUsuarioModel: Model<TokenInterface>,
    private obtenerDispositivoUsuarioService: ObtenerDispositivoUsuarioService,
  ) {}

  async refreshToken(dataToken: TokenInterface) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();

    try {
      let transaccion;
      const transactionOptions: any = {
        readPreference: 'primary',
        readConcern: { level: 'local' },
        writeConcern: { w: 'majority' },
      };

      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const userToken = await this.tokenUsuarioModel.findOne({
        tokenRefresh: dataToken.tokenRefresh,
      });
      console.log('userToken', userToken);

      const getDispositivo = userToken
        ? await this.obtenerDispositivoUsuarioService.getDispositivoByIdToken(
            userToken._id,
          )
        : null;

      if (getDispositivo?.estado === estadosDispositivo.eliminado) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.ERROR_ACTUALIZAR,
        };
      }

      if (userToken) {
        const payload: PayloadTokenDto = {
          email: userToken.emailUsuario,
          dispositivo: getDispositivo._id,
        };
        const token = this.jwtService.sign(payload);
        const tokenRefresh = uid(256);
        const dataUpdate = {
          token: token,
          tokenRefresh: tokenRefresh,
        };
        transaccion = await session.withTransaction(async () => {
          const updateToken = await this.tokenUsuarioModel.findByIdAndUpdate(
            userToken._id,
            dataUpdate,
            opts,
          );

          if (!updateToken) {
            await session.abortTransaction();
            return;
          }
        }, transactionOptions);

        if (transaccion) {
          return { accessToken: token, tokenRefresh: tokenRefresh };
        }
      }
    } catch (error) {
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    } finally {
      await session.endSession();
    }
  }
}
