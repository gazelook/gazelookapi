import { Injectable } from '@nestjs/common';
import { EliminarDispositivoService } from './../../dispositivos/casos_de_uso/eliminar-dispositivo.service';

@Injectable()
export class LogoutService {
  constructor(private eliminarDispositivoService: EliminarDispositivoService) {}

  async logout(idUsuario: string, idDispositivo: string) {
    try {
      // const usuario = await  this.obtenerIdUsuarioService.obtenerUsuarioById(idUsuario);
      const dispositivo = this.eliminarDispositivoService.eliminarDispositivo(
        idDispositivo,
        idUsuario,
      );
      if (!dispositivo) return false;

      return dispositivo;
    } catch (error) {
      throw error;
    }
  }
}
