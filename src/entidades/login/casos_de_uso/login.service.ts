import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { uid } from 'rand-token';
import { TokenInterface } from 'src/drivers/mongoose/interfaces/token_usuario/token.interface';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { ObtenerUsuarioLoginService } from 'src/entidades/usuario/casos_de_uso/obtener-usuario-login.service';
import { Usuario } from '../../../drivers/mongoose/interfaces/usuarios/usuario.interface';
import { erroresGeneral, erroresLogin } from '../../../shared/enum-errores';
import {
  codigosMetodosPago,
  estadosTransaccion,
  estadosUsuario,
  nombreAcciones,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoTipoEmailService } from '../../catalogos/casos_de_uso/catalogo-tipo-email.service';
import { CrearEmailMenorEdadService } from '../../emails/casos_de_uso/crear-email-menor-edad.service';
import { CrearEmailService } from '../../emails/casos_de_uso/crear-email.service';
import { ActualizarSuscripcionService } from '../../suscripcion/casos_de_uso/actualizar-suscripcion.service';
import { ActualizarTransaccionService } from '../../transaccion/casos_de_uso/actualizar-transaccion.service';
import { GestionPagoPaypalService } from '../../transaccion/casos_de_uso/gestion-pago-paypal.service';
import { GestionPagoStripeService } from '../../transaccion/casos_de_uso/gestion-pago-stripe.service';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { LoginDispositivoDto } from '../entidad/login.dto';
import { PayloadTokenDto } from '../entidad/token.dto';
import { CatalogoAccionService } from './../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from './../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearDispositivoService } from './../../dispositivos/casos_de_uso/crear-dispositivo.service';
import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class LoginService {
  constructor(
    private readonly loginService: ObtenerUsuarioLoginService,
    private readonly jwtService: JwtService,
    @Inject('TOKEN_USUARIO_MODEL')
    private readonly tokenUsuarioModel: Model<TokenInterface>,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private gestionPagoStripeService: GestionPagoStripeService,
    private gestionPagoPaypalService: GestionPagoPaypalService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private readonly perfilService: ObtenerPerfilUsuarioService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private dispositivoService: CrearDispositivoService,
    private nombreTipoEmail: CatalogoTipoEmailService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private crearEmailMenorEdadService: CrearEmailMenorEdadService,
    private crearEmail: CrearEmailService,
    private actualizarSuscripcionService: ActualizarSuscripcionService,
  ) {}

  async validateUser(email: string, pass: string): Promise<Usuario> {
    const user = await this.loginService.encontrarUsuario(email);

    if (user) {
      const isMatch = await bcrypt.compare(pass, user.contrasena);
      if (isMatch) {
        return user;
      }
    }
    return null;
  }

  async login(
    user: Usuario,
    codIdioma?: string,
    dispositivoUsuario?: LoginDispositivoDto,
    opts?: any,
  ) {
    let session;
    let optsLocal;
    if (!opts) {
      session = await mongoose.startSession();
      await session.startTransaction();
    }

    try {
      if (!opts) {
        optsLocal = true;
        opts = { session };
      }

      this.validarEstadoUsuario(user);

      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.tokenUsuario,
      );

      const usuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithTransaccion(
        user._id,
        opts,
      );

      // verifica si el usuario realizo el pago y no se actualizo la informacion en la BD, actualiza
      //________________________________ACTUALIZAR ESTADO Y enviar email SOLO si inicia sesion________________________________________________________
      // console.log('usuario:---------->', usuario);
      if (optsLocal) {
        if(usuario.estado !== estadosUsuario.cripto){
          const checkPago = await this.verificarPago(usuario, opts);
          if (!checkPago) {
            throw {
              codigo: HttpStatus.UNAUTHORIZED,
              codigoNombre: erroresLogin.INACTIVA_PAGO,
            };
          }
        }
       
      }
      //_____________________generar nuevo pago__________________
      let nuevoPago;
      /* if (optsLocal) {
        if (usuario?.estado === estadosUsuario.inactivaPago && usuario.transacciones[0]?.estado === estadosTransaccion.pendiente) {
          nuevoPago = await this.generarNuevoPagoService.generarNuevoPago(usuario.transacciones[0]._id, opts);
        }
      }
      let datosNuevoPago: any = {}
      if (nuevoPago) {
        datosNuevoPago.idPago = nuevoPago.idPago;
        datosNuevoPago.idTransaccion = nuevoPago.idTransaccion;
      } */

      if (user._id != undefined) {
        const idDispositivo = new mongoose.mongo.ObjectId();
        const payload: PayloadTokenDto = {
          // id: user._id,
          email: user.email,
          dispositivo: idDispositivo.toString(),
        };
        const token = this.jwtService.sign(payload);
        const tokenRefresh = uid(256);
        const tokenUsuario = {
          emailUsuario: user.email,
          usuario: user._id,
          token: token,
          tokenRefresh: tokenRefresh,
        };

        const saveToken = await new this.tokenUsuarioModel(tokenUsuario).save(
          opts,
        );
        const dataTokens = JSON.parse(JSON.stringify(saveToken));
        delete dataTokens.fechaCreacion;
        delete dataTokens.fechaActualizacion;
        delete dataTokens.__v;

        let newHistoricoTokens: any = {
          datos: dataTokens,
          usuario: user._id,
          accion: getAccion,
          entidad: entidad.codigo,
        };

        this.crearHistoricoService.crearHistoricoServer(newHistoricoTokens);

        let getPerfil = await this.perfilService.obtenerDatosUsuarioWithPerfiles(
          user._id,
          opts,
        );

        const result: any = {
          accessToken: token,
          tokenRefresh: tokenRefresh,
          usuario: getPerfil,
        };

        if (dispositivoUsuario) {
          const dispositivo = {
            ...dispositivoUsuario,
            _id: idDispositivo.toHexString(),
            tokenUsuario: saveToken._id,
          };

          const dispo = await this.dispositivoService.crearDispositivo(
            user._id,
            dispositivo,
            opts,
          );
          result.usuario.dispositivos = [
            {
              _id: dispo._id,
            },
          ];
        }
        // FINISH TRANSACTION
        if (optsLocal) {
          await session.commitTransaction();
          await session.endSession();
        }

        return result;
      } else {
        return null;
      }
    } catch (error) {
      if (optsLocal) {
        await session.abortTransaction();
        session.endSession();
      }
      throw error;
    }
  }

  // verifica los usuarios con el pago en estado pendiente
  async verificarPago(usuario, opts): Promise<any> {
    console.log(
      'usuario?.transacciones[0]?.estado: ',
      usuario?.transacciones[0]?.estado,
    );
    if (usuario?.transacciones[0]?.estado === estadosTransaccion.pendiente) {
      const idPago = usuario.transacciones[0].informacionPago.idPago;
      const metodoPago = usuario.transacciones[0].metodoPago;
      let stripe, paypal;
      if (metodoPago === codigosMetodosPago.stripe) {
        stripe = await this.gestionPagoStripeService.obtenerEstadoPagoStripe(
          idPago,
        );
      }
      // else if (metodoPago === codigosMetodosPago.paypal) {
      //   paypal = await this.gestionPagoPaypalService.obtenerEstadoPagoPaypal(idPago);
      // }
      if (stripe || paypal) {
        // actualizar transaccion
        for (const transaccion of usuario.transacciones) {
          const updateTransaccion = await this.actualizarTransaccionService.actualizarTransaccion(
            transaccion._id,
            opts,
          );
          const suscripcion = await this.actualizarSuscripcionService.actualizarSuscripcion(
            transaccion._id,
            usuario._id,
            opts,
          );
        }

        // actualizar estado usuario
        const usuarioUpdated = await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
          usuario._id,
          estadosUsuario.activaNoVerificado,
          opts,
        );
        const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(
          usuario.idioma,
        );
        const dataEmail: any = {
          usuario: usuario._id,
          emailDestinatario: '',
          menorEdad: usuario.menorEdad,
          codigo: '',
          idioma: idiomaUsuario.codNombre,
          nombre: usuario.perfiles[0].nombre,
        };
        if (usuario.menorEdad) {
          const codTipEmaR = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
            'validacionResponsable',
          );
          let codEstadoValiRes = codTipEmaR.codigo;
          dataEmail.codigo = codEstadoValiRes;
          dataEmail.emailDestinatario = usuario.emailResponsable;

          await this.crearEmail.crearEmail(dataEmail, opts);

          await this.crearEmailMenorEdadService.crearEmailMenorEdad(
            usuario,
            opts,
          );
        } else {
          const codTipEma = await this.nombreTipoEmail.obtenerCodigoTipoEmail(
            'validacion',
          );
          let codTipoEmailValid = codTipEma.codigo;
          dataEmail.menorEdad = false;
          dataEmail.emailDestinatario = usuario.email;
          dataEmail.codigo = codTipoEmailValid;

          await this.crearEmail.crearEmail(dataEmail, opts);
        }

        return usuarioUpdated;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  validarEstadoUsuario(usuario) {
    /* if (usuario.estado === estadosUsuario.activaNoVerificado){
      throw { codigo: HttpStatus.UNAUTHORIZED, codigoNombre: erroresLogin.VALIDAR_CUENTA };
    } */

    if (usuario.estado === estadosUsuario.rechazadoResponsable) {
      throw {
        codigo: HttpStatus.UNAUTHORIZED,
        codigoNombre: erroresLogin.CUENTA_RECHAZADA_RESPONSABLE,
      };
    }

    if (usuario.estado === estadosUsuario.noPermitirAcceso) {
      throw {
        codigo: HttpStatus.UNAUTHORIZED,
        codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
      };
    }

    if (usuario.estado === estadosUsuario.inactivaPagoPaymentez) {
      throw {
        codigo: HttpStatus.UNAUTHORIZED,
        codigoNombre: erroresGeneral.NO_AUTORIZADO,
      };
    }
  }
}
