import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { expiracionToken } from 'src/shared/enum-sistema';
import { ConfigService } from '../../../config/config.service';
import { EmailServicesModule } from '../../emails/casos_de_uso/email-services.module';
import { SuscripcionServicesModule } from '../../suscripcion/casos_de_uso/suscripcion-services.module';
import { DispositivoServicesModule } from './../../dispositivos/casos_de_uso/dispositivo-services.module';
import { DispositivosModule } from './../../dispositivos/dispositivos.module';
import { PerfilServiceModule } from './../../perfil/casos_de_uso/perfil.services.module';
import { TransaccionServicesModule } from './../../transaccion/casos_de_uso/transaccion-services.module';
import { UsuarioServicesModule } from './../../usuario/casos_de_uso/usuario-services.module';
import { JwtStrategy } from './../drivers/jwt.strategy';
import { LocalStrategy } from './../drivers/local.strategy';
import { tokenProviders } from './../drivers/token.provider';
import { GenerarTokenService } from './generar-token.service';
import { LoginService } from './login.service';
import { LogoutService } from './logout.service';
import { RefrescarTokenService } from './refrescar-token.service';

@Module({
  imports: [
    DBModule,
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get<string>('JWT_SECRET'),
          signOptions: {
            expiresIn: expiracionToken.segundos,
          },
        };
      },
    }),

    CatalogosServiceModule,
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => EmailServicesModule),
    forwardRef(() => TransaccionServicesModule),
    forwardRef(() => UsuarioServicesModule),
    DispositivosModule,
    DispositivoServicesModule,
    SuscripcionServicesModule,
  ],
  providers: [
    ...tokenProviders,
    RefrescarTokenService,
    LogoutService,
    LoginService,
    JwtStrategy,
    LocalStrategy,
    GenerarTokenService,
  ],
  exports: [
    RefrescarTokenService,
    LogoutService,
    LoginService,
    RefrescarTokenService,
    LogoutService,
    LoginService,
    JwtStrategy,
    LocalStrategy,
    GenerarTokenService,
  ],
  controllers: [],
})
export class LoginServicesModule {}
