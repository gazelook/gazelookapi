import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';
import { uid } from 'rand-token';
import { TokenInterface } from 'src/drivers/mongoose/interfaces/token_usuario/token.interface';
import { nombreAcciones, nombreEntidades } from '../../../shared/enum-sistema';
import { PayloadTokenDto } from '../entidad/token.dto';
import { CatalogoAccionService } from './../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from './../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class GenerarTokenService {
  constructor(
    private jwtService: JwtService,
    @Inject('TOKEN_USUARIO_MODEL')
    private readonly tokenUsuarioModel: Model<TokenInterface>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async generarToken(
    idUsuario: string,
    email: string,
    idDispositivo: string,
    opts,
  ) {
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.tokenUsuario,
    );

    try {
      //________________________generar token y nuevo dispositivo_____________________

      const payload: PayloadTokenDto = {
        email: email,
        dispositivo: idDispositivo,
      };
      const token = this.jwtService.sign(payload);
      const tokenRefresh = uid(256);
      const tokenUsuario = {
        emailUsuario: email,
        token: token,
        tokenRefresh: tokenRefresh,
        usuario: idUsuario,
      };

      const saveToken: any = await new this.tokenUsuarioModel(
        tokenUsuario,
      ).save(opts);
      delete saveToken.fechaCreacion;
      delete saveToken.fechaActualizacion;
      delete saveToken.__v;

      let newHistoricoTokens = {
        datos: saveToken,
        usuario: idUsuario,
        accion: getAccion,
        entidad: entidad.codigo,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistoricoTokens);

      return { token: token };
    } catch (error) {
      throw error;
    }
  }
}
