import { SuscripcionModelo } from './../../../drivers/mongoose/modelos/suscripcion/suscripcion.schema';
import { Connection } from 'mongoose';

export const suscripcionProviders = [
  {
    provide: 'SUSCRIPCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('suscripcion', SuscripcionModelo, 'suscripcion'),
    inject: ['DB_CONNECTION'],
  },
];
