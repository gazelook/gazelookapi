import { Module } from '@nestjs/common';
import { SuscripcionControllerModule } from './controladores/suscripcion-controller.module';

@Module({
  imports: [SuscripcionControllerModule],
  providers: [],
  controllers: [],
})
export class SuscripcionModule {}
