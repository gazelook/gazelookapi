import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { CrearSuscripcionService } from './crear-suscripcion.service';
import { Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { suscripcionProviders } from '../drivers/suscripcion.provider';
import { ActualizarSuscripcionService } from './actualizar-suscripcion.service';
import { EliminarSuscripcionCompletoService } from './eliminar-suscripcion-completo.service';
import { ObtenerSuscripcionService } from './obtener-suscripcion.service';

@Module({
  imports: [DBModule, CatalogosServiceModule],
  providers: [
    ...suscripcionProviders,
    CrearSuscripcionService,
    ActualizarSuscripcionService,
    EliminarSuscripcionCompletoService,
    ObtenerSuscripcionService,
  ],
  exports: [
    CrearSuscripcionService,
    ActualizarSuscripcionService,
    EliminarSuscripcionCompletoService,
    ObtenerSuscripcionService,
  ],
  controllers: [],
})
export class SuscripcionServicesModule {}
