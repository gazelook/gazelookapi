import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { Suscripcion } from './../../../drivers/mongoose/interfaces/suscripcion/suscripcion.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import * as moment from 'moment';

@Injectable()
export class ObtenerSuscripcionService {
  constructor(
    @Inject('SUSCRIPCION_MODEL')
    private readonly suscripcionModel: Model<Suscripcion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async getSucripcionesFechaVencida(): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.suscripcion,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      const suscripciones = await this.suscripcionModel.find({
        estado: estado.codigo,
        fechaFinalizacion: { $lt: new Date() },
      });

      return suscripciones;
    } catch (error) {
      throw error;
    }
  }

  // obtiene la ultima transaccion por fecha de finalizacion
  async diasTerminaSuscripcion(usuario: string): Promise<any> {
    const formatFechaActual = moment().format('YYYY-MM-DD');
    const fechaActual = moment(new Date(formatFechaActual));

    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.suscripcion,
    );
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      entidad.codigo,
    );

    //ultima suscripcion
    const getSuscripcions = await this.suscripcionModel
      .find({ estado: estado.codigo, usuario: usuario })
      .sort('-fechaFinalizacion')
      .limit(1);

    if (getSuscripcions.length > 0) {
      const suscripcion = getSuscripcions[0];

      const fechaFinalizacion = moment(suscripcion.fechaFinalizacion);
      const result = {
        _id: suscripcion._id,
        fechaFinalizacion: suscripcion.fechaFinalizacion,
        usuario: suscripcion.usuario,
        dias: fechaFinalizacion.diff(fechaActual, 'days'),
      };
      return result;
    }

    return null;
  }

  async getSuscripcionById(idSuscripcion: string): Promise<Suscripcion> {
    const formatFechaActual = moment().format('YYYY-MM-DD');
    const fechaActual = moment(new Date(formatFechaActual));

    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.suscripcion,
    );
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      entidad.codigo,
    );

    //ultima suscripcion
    const getSuscripcion = await this.suscripcionModel.findOne({
      _id: idSuscripcion,
    });

    return getSuscripcion;
  }
}
