import { CatalogoSuscripcionService } from './../../catalogos/casos_de_uso/catalogo-suscripcion.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { Suscripcion } from './../../../drivers/mongoose/interfaces/suscripcion/suscripcion.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class EliminarSuscripcionCompletoService {
  constructor(
    @Inject('SUSCRIPCION_MODEL')
    private readonly suscripcionModel: Model<Suscripcion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
  ) {}

  // no se borrara la suscripcion solo se pasa a un estado "usuarioNoExistente"
  async eliminarSuscripcionCompleto(
    idTransaccion: string,
    opts?: any,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.suscripcion,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.retiroUsuario,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );

      const suscripcion = await this.suscripcionModel.findOne({
        transaccion: idTransaccion,
      });

      if (suscripcion) {
        //await this.suscripcionModel.deleteOne({ _id: suscripcion._id });
        const dataupdate = {
          estado: estado.codigo,
        };
        const updateSuscripcion = await this.suscripcionModel.findByIdAndUpdate(
          suscripcion._id,
          dataupdate,
          opts,
        );
      } else {
        return false;
      }

      return true;
    } catch (error) {
      throw error;
    }
  }
  async eliminarSuscripcionFisica(
    idTransaccion: string,
    opts: any,
  ): Promise<any> {
    try {
      await this.suscripcionModel.deleteOne(
        { transaccion: idTransaccion },
        opts,
      );
      return true;
    } catch (error) {
      throw error;
    }
  }
}
