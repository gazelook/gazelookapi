import { CatalogoSuscripcionService } from './../../catalogos/casos_de_uso/catalogo-suscripcion.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Suscripcion } from './../../../drivers/mongoose/interfaces/suscripcion/suscripcion.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { SuscripcionDto } from '../entidad/suscripcion-dto';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import * as moment from 'moment';

@Injectable()
export class CrearSuscripcionService {
  constructor(
    @Inject('SUSCRIPCION_MODEL')
    private readonly suscripcionModel: Model<Suscripcion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoSuscripcionService: CatalogoSuscripcionService,
  ) {}
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async crearSuscripcion(
    idTransaccion: any,
    usuario: string,
    tipoSuscrip: string,
    opts: any,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.suscripcion,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.pendiente,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const tipoSuscripcion: any = await this.catalogoSuscripcionService.obtenerTipoSuscripcionByCodigo(
        tipoSuscrip,
      );
      const suscripcionDto: SuscripcionDto = {
        estado: estado.codigo,
        tipo: tipoSuscripcion.codigo,
        transaccion: idTransaccion,
        usuario: usuario,
        fechaFinalizacion: this.calcularFechaFinalSuscripcion(
          tipoSuscripcion.duracion,
        ),
      };

      const suscripcion = await new this.suscripcionModel(suscripcionDto).save(
        opts,
      );
      //datos para guardar el historico
      const newHistorico: any = {
        datos: suscripcion,
        usuario: usuario,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      return suscripcion;
    } catch (error) {
      throw error;
    }
  }

  async crearNuevaSuscripcion(
    idTransaccion: any,
    usuario: string,
    tipoSuscrip: string,
    fechaFinalUltimaSuscripcion,
    opts: any,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.suscripcion,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.pendiente,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const tipoSuscripcion: any = await this.catalogoSuscripcionService.obtenerTipoSuscripcionByCodigo(
        tipoSuscrip,
      );
      const suscripcionDto: SuscripcionDto = {
        estado: estado.codigo,
        tipo: tipoSuscripcion.codigo,
        transaccion: idTransaccion,
        usuario: usuario,
        fechaFinalizacion: this.calcularFechaFinalNewSuscripcion(
          tipoSuscripcion.duracion,
          fechaFinalUltimaSuscripcion,
        ),
      };

      const suscripcion = await new this.suscripcionModel(suscripcionDto).save(
        opts,
      );
      //datos para guardar el historico
      const newHistorico: any = {
        datos: suscripcion,
        usuario: usuario,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      return suscripcion;
    } catch (error) {
      throw error;
    }
  }

  calcularFechaFinalSuscripcion(duracion: number) {
    const formatFechaActual = moment().format('YYYY-MM-DD HH:mm:ss');
    const fechaActual = new Date(formatFechaActual);

    const fechaFinalizacion = new Date(formatFechaActual);
    fechaFinalizacion.setDate(fechaActual.getDate() + duracion);

    return fechaFinalizacion;
  }

  calcularFechaFinalNewSuscripcion(duracion: number, dataFechaInicio: Date) {
    const formatFechaActual = moment(dataFechaInicio).format(
      'YYYY-MM-DD HH:mm:ss',
    );
    const fechaInicio = new Date(formatFechaActual);

    const fechaFinalizacion = new Date(formatFechaActual);
    fechaFinalizacion.setDate(fechaInicio.getDate() + duracion);

    return fechaFinalizacion;
  }
}
