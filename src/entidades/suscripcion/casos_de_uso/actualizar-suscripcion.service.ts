import { CatalogoSuscripcionService } from './../../catalogos/casos_de_uso/catalogo-suscripcion.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Suscripcion } from './../../../drivers/mongoose/interfaces/suscripcion/suscripcion.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class ActualizarSuscripcionService {
  constructor(
    @Inject('SUSCRIPCION_MODEL')
    private readonly suscripcionModel: Model<Suscripcion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
  ) {}
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async actualizarSuscripcion(
    idTransaccion: any,
    usuario: string,
    opts?: any,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.suscripcion,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionActualizar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      const suscripcion = await this.suscripcionModel.findOneAndUpdate(
        { transaccion: idTransaccion },
        { estado: estado.codigo },
        opts,
      );

      if (!suscripcion) {
        console.error('no existe la suscripcion');
        return null;
      }
      //datos para actuaizar el historico
      const newHistorico: any = {
        datos: await this.suscripcionModel
          .findOne({ _id: suscripcion._id })
          .session(opts.session)
          .select('-fechaActualizacion -fechaCreacion -__v'),
        usuario: usuario,
        accion: accionActualizar.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      return suscripcion;
    } catch (error) {
      throw error;
    }
  }

  async actualizarSuscripcionEstado(
    idSuscripcion: string,
    estado: string,
    opts,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.suscripcion,
      );
      //const estado = await this.catalogoEstadoService.obtenerNombreEstado("", entidad.codigo);
      const accionActualizar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      const suscripcion = await this.suscripcionModel.findOneAndUpdate(
        { _id: idSuscripcion },
        { estado: estado },
        opts,
      );
      //datos para actuaizar el historico
      const newHistorico: any = {
        datos: await this.suscripcionModel
          .findOne({ _id: suscripcion._id })
          .session(opts.session)
          .select('-fechaActualizacion -fechaCreacion -__v'),
        usuario: suscripcion.usuario.toString(),
        accion: accionActualizar.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      return suscripcion;
    } catch (error) {
      throw error;
    }
  }

  async addReferenciaSuscripcion(
    idSuscripcion: string,
    referencia: string,
    opts,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.suscripcion,
      );
      //const estado = await this.catalogoEstadoService.obtenerNombreEstado("", entidad.codigo);
      const accionActualizar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      const suscripcion = await this.suscripcionModel.findOneAndUpdate(
        { _id: idSuscripcion },
        { referencia: referencia },
        opts,
      );
      //datos para actuaizar el historico
      const newHistorico: any = {
        datos: await this.suscripcionModel
          .findOne({ _id: suscripcion._id })
          .session(opts.session)
          .select('-fechaActualizacion -fechaCreacion -__v'),
        usuario: suscripcion.usuario.toString(),
        accion: accionActualizar.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      return suscripcion;
    } catch (error) {
      throw error;
    }
  }
}
