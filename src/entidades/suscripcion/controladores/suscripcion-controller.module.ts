import { Module } from '@nestjs/common';
import { CrearSuscripcionControlador } from './crear-suscripcion.controller';
import { SuscripcionServicesModule } from '../casos_de_uso/suscripcion-services.module';

@Module({
  imports: [SuscripcionServicesModule],
  providers: [],
  exports: [],
  controllers: [CrearSuscripcionControlador],
})
export class SuscripcionControllerModule {}
