import { CrearSuscripcionService } from '../casos_de_uso/crear-suscripcion.service';
import { ApiResponse } from '@nestjs/swagger';
import { Controller, Post, Body, Res } from '@nestjs/common';
import { SuscripcionDto } from '../entidad/suscripcion-dto';

// import {CrearSuscripcionAdaptador}from '../adaptador/v1/adapterCrearSuscripcion'

// @ApiTags('Suscripciones')
@Controller('api/suscripcion')
export class CrearSuscripcionControlador {
  constructor(
    private readonly crearSuscripcionService: CrearSuscripcionService,
  ) {}
  // Add User: /users/create
  @Post('/intento-pago')
  @ApiResponse({ status: 201, description: 'Creado' })
  @ApiResponse({ status: 406, description: 'El email ya esta registrado.' })
  public async crearSuscripcion(
    @Res() res,
    @Body() suscripcionDto: SuscripcionDto,
  ) {}
}
