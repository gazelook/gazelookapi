import { ApiProperty } from '@nestjs/swagger';

export class SuscripcionDto {
  @ApiProperty()
  estado: string;
  @ApiProperty()
  tipo: string;
  @ApiProperty()
  transaccion: string;
  @ApiProperty()
  fechaFinalizacion: Date;
  @ApiProperty()
  usuario: string;
}
