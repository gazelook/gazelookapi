import { Module } from '@nestjs/common';
import { RolServiceModule } from './casos_de_uso/rol.services.module';

@Module({
  imports: [],
  providers: [RolServiceModule],
  controllers: [],
  exports: [],
})
export class RolModule {}
