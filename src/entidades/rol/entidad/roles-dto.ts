import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsString,
  IsOptional,
} from 'class-validator';

export class IdentRolesDto {
  @ApiProperty({
    description: 'Identificador del rol',
    example: '5f3173e892e08e19101271f8',
  })
  @IsNotEmpty()
  _id: string;
}

export class CoautorRolesDto {
  @ApiProperty({
    description: 'Identificador del rol',
    example: '5f3173e892e08e19101271f8',
  })
  _id: string;

  @ApiProperty({
    description: 'Nombre del rol',
    example: 'usuario coautor: Comentarios',
  })
  nombre: string;

  @ApiProperty({ description: 'Codigo del rol', example: 'CATROL_2' })
  rol: string;
}
