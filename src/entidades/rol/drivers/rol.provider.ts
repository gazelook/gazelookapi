import { Connection } from 'mongoose';
import { RolSistemaModelo } from '../../../drivers/mongoose/modelos/rol_sistema/rol-sistema.schema';
import { CatalogoTipoRolModelo } from '../../../drivers/mongoose/modelos/catalogo_tipo_rol/catalogo-tipo-rol.schema';
import { CatalogoRolModelo } from '../../../drivers/mongoose/modelos/catalogo_rol/catalogo-rol.schema';
import { RolEntidadModelo } from '../../../drivers/mongoose/modelos/rol_entidad/rol-entidad.schema';

export const RolProviders = [
  {
    provide: 'ROL_SISTEMA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('rol_sistema', RolSistemaModelo, 'rol_sistema'),
    inject: ['DB_CONNECTION'],
  },

  {
    provide: 'CATALOGO_TIPO_ROL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_tipo_rol',
        CatalogoTipoRolModelo,
        'catalogo_tipo_rol',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_ROL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('catalogo_rol', CatalogoRolModelo, 'catalogo_rol'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ROL_ENTIDAD_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('rol_entidad', RolEntidadModelo, 'rol_entidad'),
    inject: ['DB_CONNECTION'],
  },
];
