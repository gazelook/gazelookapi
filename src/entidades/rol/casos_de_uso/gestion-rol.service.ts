import { Injectable, Inject } from '@nestjs/common';
import { RolSistema } from '../../../drivers/mongoose/interfaces/rol_sistema/rol-sistema.interface';
import { Model } from 'mongoose';
import { CatalogoTipoRol } from '../../../drivers/mongoose/interfaces/catalogo_tipo_rol/catalogo-tipo-rol.interface';
import { CatalogoRol } from '../../../drivers/mongoose/interfaces/catalogo_rol/catalogo-rol.interface';
import { RolEntidad } from '../../../drivers/mongoose/interfaces/rol_entidad/rol-entidad.interface';
import { codigosRol } from '../../../shared/enum-sistema';

@Injectable()
export class GestionRolService {
  CODIGO_ROL_COAUTOR = codigosRol.cod_rol_ent_co_autor;
  CODIGO_ROL_COORDINADOR_GAZELOOK = codigosRol.cod_rol_coordinador_gazelook;
  CODIGO_ROL_PROPIETARIO = codigosRol.cod_rol_ent_propietario;
  CODIGO_ROL_USUARIO_SISTEMA = codigosRol.cod_rol_usuario_sistema;

  constructor(
    @Inject('ROL_SISTEMA_MODEL')
    private readonly rolSistemaModel: Model<RolSistema>,
    @Inject('ROL_ENTIDAD_MODEL')
    private readonly rolEntidadModel: Model<RolEntidad>,
    @Inject('CATALOGO_TIPO_ROL_MODEL')
    private readonly catalogoTipoRolModel: Model<CatalogoTipoRol>,
    @Inject('CATALOGO_ROL_MODEL')
    private readonly catalogoRolModel: Model<CatalogoRol>,
  ) {}

  async crearRol(codigoRol): Promise<any> {
    try {
      // crearCatalogoAccionRol
    } catch (error) {
      throw error;
    }
  }
  // busca en rol_entidad
  async obtenerRolEntidadByCodRol(codigo: string): Promise<any> {
    const rol = await this.rolEntidadModel
      .find({ rol: codigo })
      .select('-fechaCreacion -fechaActualizacion');
    return rol;
  }

  // busca en rol_entidad
  async obtenerRolEntidadByCodEntRol(
    codigo: string,
    codEntidad: string,
  ): Promise<any> {
    const rol = await this.rolEntidadModel
      .findOne({ rol: codigo, entidad: codEntidad })
      .select('-fechaCreacion -fechaActualizacion');
    return rol;
  }

  // busca en rol_sistema
  async obtenerRolSistemaByCodRol(codigo: string): Promise<any> {
    const rol = await this.rolSistemaModel
      .find({ rol: codigo })
      .select('-fechaCreacion -fechaActualizacion');
    return rol;
  }
}
