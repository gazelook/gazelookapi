import { Module, HttpModule } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from './../../catalogos/casos_de_uso/catalogos-services.module';
import { RolProviders } from '../drivers/rol.provider';
import { GestionRolService } from './gestion-rol.service';

@Module({
  imports: [DBModule, HttpModule, CatalogosServiceModule],
  providers: [...RolProviders, GestionRolService],
  exports: [GestionRolService],
  controllers: [],
})
export class RolServiceModule {}
