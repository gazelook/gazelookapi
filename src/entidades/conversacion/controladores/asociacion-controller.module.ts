import { Module } from '@nestjs/common';
import { AuthService } from 'src/auth/auth.service';
import { ProyectosServicesModule } from 'src/entidades/proyectos/casos_de_uso/proyectos-services.module';
import { ConversacionServicesModule } from '../casos_de_uso/conversacion-services.module';
import { ChatModule } from '../socket/chat.module';
import { ArchivoServicesModule } from './../../archivo/casos_de_uso/archivo-services.module';
import { MediaServiceModule } from './../../media/casos_de_uso/media.services.module';
import { EliminarMensajesControlador } from './eliminar-mensajes.controller';
import { GuardaMensajeControlador } from './guarda-mensaje.controller';
import { ObtenerConversacionControlador } from './obtener-conversacion.controller';
import { ObtenerMensajesNoLeidosControlador } from './obtener-mensajes-no-leidos.controller';

@Module({
  imports: [
    ConversacionServicesModule,
    MediaServiceModule,
    ArchivoServicesModule,
    ProyectosServicesModule,
    ChatModule,
  ],
  providers: [AuthService],
  exports: [],
  controllers: [
    ObtenerConversacionControlador,
    EliminarMensajesControlador,
    ObtenerMensajesNoLeidosControlador,
    GuardaMensajeControlador,
  ],
})
export class ConversacionControllerModule {}
