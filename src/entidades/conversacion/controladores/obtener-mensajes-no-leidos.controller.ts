import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerMensajesNoLeidosService } from './../casos_de_uso/obtener-mensajes-no-leidos.service';
import { MensajesNoLeidosDto } from './../entidad/conversacion-dto';

@ApiTags('Asociacion')
@Controller('api/obtener-mensajes-no-leidos')
export class ObtenerMensajesNoLeidosControlador {
  constructor(
    private readonly obtenerMensajesNoLeidosService: ObtenerMensajesNoLeidosService,
    private readonly i18n: I18nService,
  ) {}

  funcion = new Funcion(this.i18n);

  @Get('/')
  //@ApiBody({ type: any })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Obtiene el ultimo mensaje de la conversacion' })
  @ApiResponse({
    status: 200,
    type: MensajesNoLeidosDto,
    description: 'mensajes no leidos',
  })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al crear la asociacion' })
  @UseGuards(AuthGuard('jwt'))
  public async ObtenerMensajesNoLeidosControlador(
    @Headers() headers,
    @Query('perfil') perfil: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
  ) {
    let codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (mongoose.isValidObjectId(perfil)) {
      try {
        const conversacion = await this.obtenerMensajesNoLeidosService.obtenerMensajesNoLeidos(
          perfil,
          limite,
          pagina,
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: conversacion,
        });
      } catch (e) {
        return this.funcion.enviarRespuesta(
          HttpStatus.INTERNAL_SERVER_ERROR,
          await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'ERROR_SOLICITUD',
          ),
        );
      }
    } else {
      return this.funcion.enviarRespuesta(
        HttpStatus.NOT_FOUND,
        await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        ),
      );
    }
  }
}
