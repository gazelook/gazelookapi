import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ConversacionRespuestaDto } from '../entidad/conversacion-dto';
import { HadersInterfaceNombres } from './../../../shared/header-response-interface';
import { ObtenerConversacionService } from './../casos_de_uso/obtener-conversacion.service';

@ApiTags('Asociacion')
@Controller('api/obtener-conversacion')
export class ObtenerConversacionControlador {
  constructor(
    private readonly obtenerConversacionService: ObtenerConversacionService,
    private readonly i18n: I18nService,
  ) {}

  funcion = new Funcion(this.i18n);

  @Get('/')
  //@ApiBody({ type: any })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Obtiene lista de mensajes de la conversacion' })
  @ApiResponse({
    status: 201,
    type: ConversacionRespuestaDto,
    description: 'Conversacion obtenida',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Error al crear la asociacion' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerConversacion(
    @Headers() headers: any,
    @Query('idPerfil') idPerfil: string,
    @Query('asociacion') asociacion: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const headerNombre = new HadersInterfaceNombres();
    let codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    // !isNaN(limite) && !isNaN(pagina) && limite > 0 && pagina > 0 && isNaN(filtro) &&
    if (
      !isNaN(limite) &&
      !isNaN(pagina) &&
      limite > 0 &&
      pagina > 0 &&
      mongoose.isValidObjectId(asociacion)
    ) {
      try {
        const conversacion = await this.obtenerConversacionService.obtenerConversacionDesdeFecha(
          idPerfil,
          asociacion,
          codIdioma,
          null,
          limite,
          pagina,
        );

        for (let item of conversacion.docs) {
          delete item.id;
        }

        var respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: conversacion.docs,
        });

        response.set(headerNombre.totalDatos, conversacion.totalDocs);
        response.set(headerNombre.totalPaginas, conversacion.totalPages);
        response.set(headerNombre.proximaPagina, conversacion.hasNextPage);
        response.set(headerNombre.anteriorPagina, conversacion.hasPrevPage);

        response.send(respuesta);
        return respuesta;
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: e.message,
        });
      }
    } else {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        ),
      });
    }
  }

  @Get('/desde-fecha')
  //@ApiBody({ type: any })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Obtiene lista de mensajes de la conversacion' })
  @ApiResponse({
    status: 201,
    type: ConversacionRespuestaDto,
    description: 'Conversacion obtenida',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Error al crear la asociacion' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerConversacionDesdeFecha(
    @Headers() headers: any,
    @Query('idPerfil') idPerfil: string,
    @Query('asociacion') asociacion: string,
    @Query('fecha') fecha: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const headerNombre = new HadersInterfaceNombres();
    let codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      fecha &&
      !isNaN(limite) &&
      !isNaN(pagina) &&
      limite > 0 &&
      pagina > 0 &&
      mongoose.isValidObjectId(asociacion)
    ) {
      try {
        const conversacion = await this.obtenerConversacionService.obtenerConversacionDesdeFecha(
          idPerfil,
          asociacion,
          codIdioma,
          fecha,
          limite,
          pagina,
        );

        for (let item of conversacion.docs) {
          delete item.id;
        }

        var respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: conversacion.docs,
        });

        response.set(headerNombre.totalDatos, conversacion.totalDocs);
        response.set(headerNombre.totalPaginas, conversacion.totalPages);
        response.set(headerNombre.proximaPagina, conversacion.hasNextPage);
        response.set(headerNombre.anteriorPagina, conversacion.hasPrevPage);

        response.send(respuesta);
        return respuesta;
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: e.message,
        });
      }
    } else {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        ),
      });
    }
  }
}
