import {
  Body,
  Controller,
  Delete,
  Headers,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { EliminarMensajesService } from './../casos_de_uso/eliminar-mensajes.service';
import { MensajesEliminaraDto } from './../entidad/conversacion-dto';

@ApiTags('Asociacion')
@Controller('api/eliminar-mensajes')
export class EliminarMensajesControlador {
  constructor(
    private readonly eliminarMensajeService: EliminarMensajesService,
    private readonly i18n: I18nService,
  ) {}

  funcion = new Funcion(this.i18n);

  @Delete('/')
  //@ApiBody({ type: any })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Eliminar Mensajes' })
  @ApiResponse({ status: 201, description: 'Eliminar mensajes' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al crear la asociacion' })
  @UseGuards(AuthGuard('jwt'))
  public async eliminarListaMensaje(
    @Headers() headers,
    @Body() data: MensajesEliminaraDto,
  ) {
    let codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (data) {
      try {
        const elimMensajes = await this.eliminarMensajeService.eliminarListaMensaje(
          data,
          data.perfil._id,
        );
        return this.funcion.enviarRespuesta(
          HttpStatus.OK,
          await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'ELIMINACION_CORRECTA',
          ),
        );
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        ),
      });
    }
  }
}
