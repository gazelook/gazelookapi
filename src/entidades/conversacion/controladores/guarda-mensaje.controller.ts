import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FilesInterceptor } from '@nestjs/platform-express';
import {
  ApiBody,
  ApiConsumes,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import {
  codigoCatalogoTipoMedia,
  codigosCatalogoMedia,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { SubirArchivoService } from '../../media/casos_de_uso/subir-archivo.service';
import { GestionArchivoService } from './../../archivo/casos_de_uso/gestion-archivo.service';

export const ApiFile = (fileName: string = 'file'): MethodDecorator => (
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor,
) => {
  ApiBody({
    required: true,
    schema: {
      type: 'object',
      properties: {
        [fileName]: {
          type: 'string',
          format: 'binary',
        },
        fileDefault: {
          type: 'Boolean',
          description:
            'Este campo controla si la imagen es del usuario(false por defecto-> es controlado en el api, no es obligatorio) o si es del sistema es true (Obligatorio)',
        },
        catalogoArchivoDefault: {
          type: 'string',
          description:
            'Este campo es la referencia al tipo de archivo por defecto(Ejemplo: CAT_ARC_DEFAULT_01 es de tipo album_general | CAT_ARC_DEFAULT_02 es de tipo album_perfil',
        },
        relacionAspecto: {
          type: 'string',
        },
        catalogoMedia: {
          type: 'string',
          description:
            'Obligatorio: referencia al codigo de catalogo media, Ejemplo: CATMED_1 es de tipo link | CATMED_2 es de tipo compuesto | CATMED_3 es de tipo simple',
        },
        descripcion: {
          type: 'string',
          description: 'descripcion de la imagen',
        },
        enlace: {
          type: 'string',
          description:
            'url o link. Este campo debe de guardarse junto con el codigo catalogoMedia: CATMED_1 que es de tipo link',
        },
      },
    },
  })(target, propertyKey, descriptor);
};

@ApiTags('conversacion')
@Controller('api/subir-archivos')
export class GuardaMensajeControlador {
  constructor(
    private readonly subirArchivoService: SubirArchivoService,
    private readonly validarArchivo: GestionArchivoService,
    private readonly i18n: I18nService,
  ) {}

  funcion = new Funcion(this.i18n);

  @Post('/')
  @ApiSecurity('Authorization')
  // @ApiResponse({ status: 201, type: RetornoArchivoDto, description: 'Archivo creado' })
  @ApiResponse({ status: 404, description: 'Error al guardar el archivo' })
  @ApiOperation({
    summary:
      'Esta función guarda el archivo y retorna las caracteristicas del mismo',
  })
  @ApiConsumes('multipart/form-data')
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: true,
  })
  @ApiFile('archivo')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FilesInterceptor('files'))
  public async subirArchivos(
    @UploadedFiles() files,
    @Body() archivoDto: any,
    @Headers() headers,
  ) {
    try {
      const listaArchivo = [];
      for (const iterator of files) {
        const validar = this.validarArchivo.validarArchivo(iterator);

        if (validar.validate) {
          var catalogoMedia =
            validar.tipo === codigoCatalogoTipoMedia.codImage ||
            validar.tipo === codigoCatalogoTipoMedia.codVideo
              ? codigosCatalogoMedia.catMediaCompuesto
              : validar.tipo === codigoCatalogoTipoMedia.codEnlace
              ? codigosCatalogoMedia.catMediaLink
              : codigosCatalogoMedia.catMediaSimple;

          let arch: any = {
            catalogoMedia: catalogoMedia,
            archivo: iterator,
          };

          if (catalogoMedia === codigosCatalogoMedia.catMediaCompuesto) {
            arch.relacionAspecto = '1:1';
          }

          const result = await this.subirArchivoService.subirArchivo(
            arch,
            headers.idioma,
          );

          if (result) listaArchivo.push(result);
        }
      }

      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: listaArchivo,
      });
    } catch (e) {
      this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          this.funcion.obtenerIdiomaDefecto(headers.idioma),
          'ERROR_SOLICITUD',
        ),
      });
    }
  }
}
