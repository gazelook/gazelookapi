import { Module } from '@nestjs/common';
import { ConversacionControllerModule } from './controladores/asociacion-controller.module';

@Module({
  imports: [ConversacionControllerModule],
  providers: [],
  controllers: [],
  exports: [ConversacionControllerModule],
})
export class ConversacionModule {}
