import { Connection } from 'mongoose';
import { perfilModelo } from 'src/drivers/mongoose/modelos/perfil/perfil.schema';
import { ConversacionModelo } from '../../../drivers/mongoose/modelos/conversaciones/conversacion.schema';
import { ParticipanteAsociacionModelo } from '../../../drivers/mongoose/modelos/participante_asociacion/participante-asociacion.schema';
import { MensajeModelo } from './../../../drivers/mongoose/modelos/mensajes/mensaje.schema';
import { traduccionMensajeModelo } from './../../../drivers/mongoose/modelos/traduccion_mensaje/traduccion-mensaje.schema';

export const conversacionProviders = [
  {
    provide: 'CONVERSACION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('conversacion', ConversacionModelo, 'conversacion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'MENSAJE_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('mensaje', MensajeModelo, 'mensaje'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRAD_MSJ_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_mensajes',
        traduccionMensajeModelo,
        'traduccion_mensajes',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PARTI_ASOC_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'participante_asociacion',
        ParticipanteAsociacionModelo,
        'participante_asociacion',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION'],
  },
];
