import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { NoticiaServicesModule } from 'src/entidades/noticia/casos_de_uso/noticia-services.module';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { proyectoProviders } from 'src/entidades/proyectos/drivers/proyecto.provider';
import { TraduccionEstaticaController } from '../../../multiIdioma/controladores/traduccion-estatica-controller';
import { MediaServiceModule } from '../../media/casos_de_uso/media.services.module';
import { ProyectosServicesModule } from '../../proyectos/casos_de_uso/proyectos-services.module';
import { conversacionProviders } from '../drivers/conversacion.provider';
import { ChatModule } from '../socket/chat.module';
import { ParticipanteAsociacionServicesModule } from './../../asociacion_participante/casos_de_uso/participante-asociacion-services.module';
import { ParticipanteAsociacionControllerModule } from './../../asociacion_participante/controladores/participante-asociacion-controller.module';
import { ActualizarConversacionService } from './actualizar-conversacion.service';
import { CambiarMensajesNoLeidosService } from './cambiar-mensajes-no-leidos.service';
import { MensajeTransferirProyectoService } from './confirmar-transferir-proyecto.service';
import { CrearConversacionService } from './crear-conversacion.service';
import { EliminarMensajesCompletoService } from './eliminar-mensajes-completo.service';
import { EliminarMensajesService } from './eliminar-mensajes.service';
import { GuardarMensajeService } from './guarda-mensaje.service';
import { ObtenerConversacionService } from './obtener-conversacion.service';
import { ObtenerMensajesNoLeidosService } from './obtener-mensajes-no-leidos.service';
import { UltimoMensajesNoLeidoService } from './ultimo-mensaje-no-leidos.service';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    ParticipanteAsociacionControllerModule,
    ParticipanteAsociacionServicesModule,
    MediaServiceModule,
    ChatModule,
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => NoticiaServicesModule),
    ProyectosServicesModule,
  ],
  providers: [
    ...conversacionProviders,
    ...proyectoProviders,
    CrearConversacionService,
    ObtenerConversacionService,
    GuardarMensajeService,
    EliminarMensajesService,
    ObtenerMensajesNoLeidosService,
    CambiarMensajesNoLeidosService,
    UltimoMensajesNoLeidoService,
    ActualizarConversacionService,
    EliminarMensajesCompletoService,
    MensajeTransferirProyectoService,
    TraduccionEstaticaController,
  ],
  exports: [
    CrearConversacionService,
    ObtenerConversacionService,
    GuardarMensajeService,
    EliminarMensajesService,
    ObtenerMensajesNoLeidosService,
    CambiarMensajesNoLeidosService,
    UltimoMensajesNoLeidoService,
    ActualizarConversacionService,
    EliminarMensajesCompletoService,
    MensajeTransferirProyectoService,
  ],
  controllers: [],
})
export class ConversacionServicesModule {}
