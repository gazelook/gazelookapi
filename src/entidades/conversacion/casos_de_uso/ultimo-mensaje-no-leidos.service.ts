import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Conversacion } from 'src/drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { ObtenerTodosParticipantePerfilService } from 'src/entidades/asociacion_participante/casos_de_uso/obtener-todos-participante-perfil.service';
import { estadoDeMensaje } from '../../../shared/enum-sistema';
import { CatalogoMensajesService } from '../../catalogos/casos_de_uso/catalogo-mensajes.service';

@Injectable()
export class UltimoMensajesNoLeidoService {
  constructor(
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: Model<Conversacion>,
    // @Inject('MENSAJE_MODEL') private readonly mensajeModel: PaginateModel<Mensaje>,

    private catalogoMensaje: CatalogoMensajesService,
    private participanteAso: ObtenerTodosParticipantePerfilService,
  ) {}
  filtroBus = estadoDeMensaje;
  async UltimoMensajesNoLeido(perfil: any, asociacion: any): Promise<any> {
    try {
      const TipEnviado = await this.catalogoMensaje.obtenerCodigoCatalogoMensj(
        'enviado',
      );
      const codEnviado = TipEnviado.codigo;

      let idPerfil = perfil;

      const participantes = await this.participanteAso.obtenerTodosParticipantePerfil(
        perfil,
      );

      const idAsociaciones = [];
      const idPropietarios = [];

      for (const iterator of participantes) {
        idAsociaciones.push(iterator.asociacion);
      }
      for (const iterator of participantes) {
        idPropietarios.push(iterator._id);
      }

      let busqueda: any;
      const sortBusqueda = {};
      sortBusqueda['fechaActualizacion'] = -1;

      const populateTraducciones = {
        path: 'traducciones',
        select: '-_id contenido',
      };

      const populatePropietario = {
        path: 'propietario',
        select: '_id perfil',

        populate: {
          path: 'perfil',
          select: '_id nombreContacto nombre',
          match: { _id: { $ne: idPerfil } },
        },
      };
      const ultimoMensajeTra = {
        path: 'ultimoMensaje',
        // select: '-_id traducciones estatus',
        match: { estatus: codEnviado },
        // populate: {
        //     path: 'traducciones',
        //     select: '-_id contenido',
        // }
      };
      // const ultimoMensajePro = {
      //     path: 'ultimoMensaje',
      //     select: '-_id propietario',
      //     populate: {
      //         path: 'propietario',
      //         select: '-_id perfil',
      //         populate:{
      //             path: 'perfil',
      //             select: '_id nombreContacto nombre',
      //         }
      //     }
      // }
      const options = {
        sort: sortBusqueda,
        select: '-fechaCreacion -estado -__v  ',
        populate: [
          // ultimoMensajeTra,
          // ultimoMensajePro
        ],
      };

      // busqueda = await this.mensajeModel.paginate({
      //     $and: [
      //         { conversacion: idAsociaciones },
      //         { estado: codEstadoMensaje },
      //         {
      //             $or: [
      //                 { estatus: codEnviado },
      //                 { estatus: codEntreg }
      //             ]
      //         }
      //     ]

      busqueda = await this.conversacionModel.findOne({
        $and: [
          { asociacion: asociacion },
          {
            'ultimoMensaje.perfil._id': { $ne: perfil },
            ultimoMensaje: { $ne: null },
          },
        ],
      });

      // const busqueda2 = await this.conversacionModel.find(
      //     { $and: [{ asociacion: { $in: idAsociaciones } }, { 'ultimoMensaje.perfil._id': { $ne: idPerfil } }] }
      // );

      // .populate({ path: 'ultimoMensaje',null, {estatus: codEnviado  })

      return busqueda;
    } catch (error) {
      return null;
    }
  }
}
