import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Conversacion } from 'src/drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { Mensaje } from 'src/drivers/mongoose/interfaces/mensaje/mensaje.interface';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoMensajesService } from 'src/entidades/catalogos/casos_de_uso/catalogo-mensajes.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { ObtenerNoticiaIdService } from 'src/entidades/noticia/casos_de_uso/obtener-noticia-id.service';
import { ObtenerPortadaService } from 'src/entidades/noticia/casos_de_uso/obtener-portada-predeterminada.service';
import { GetFotoPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil-general.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from 'src/entidades/proyectos/casos_de_uso/obtener-portada-predeterminada-proyecto-resumen.service';
import { ObtenerProyetoIdService } from 'src/entidades/proyectos/casos_de_uso/obtener-proyecto-id.service';
import {
  estadoDeMensaje,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { MensajeEnvioSocketDto } from '../entidad/conversacion-dto';
import { TraduccionMensaje } from './../../../drivers/mongoose/interfaces/traduccion_mensaje/traduccion-mensaje.interface';

@Injectable()
export class GuardarMensajeService {
  constructor(
    @Inject('MENSAJE_MODEL') private readonly mensajeModel: Model<Mensaje>,
    @Inject('TRAD_MSJ_MODEL')
    private readonly tradMensajeModel: Model<TraduccionMensaje>,
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: Model<Conversacion>,
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteModel: Model<Participante>,
    @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
    private obtenerProyetoIdService: ObtenerProyetoIdService,
    private obtenerNoticiaIdService: ObtenerNoticiaIdService,
    private crearHistoricoService: CrearHistoricoService,
    private readonly catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoMensajeService: CatalogoMensajesService,
    private getFotoPerfilService: GetFotoPerfilService,
    private obtenerPortadaService: ObtenerPortadaService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );

  async guardarMensaje(
    mensajeDto: MensajeEnvioSocketDto,
    idConversacion,
    listaParticipante,
    codIdioma,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const catalogoAccionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const codAccionCrear = catalogoAccionCrear.codigo;

      const catalogoAccionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const codAcccionModificar = catalogoAccionModificar.codigo;

      const codEntidadProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );

      const catalogoEstadoEntidadMensaje = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.mensaje,
        nombrecatalogoEstados.activa,
      );
      const catalogoEstadoEntidadTraduccion = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.traduccionMensajes,
        nombrecatalogoEstados.activa,
      );
      const catalogoEstadoMensaje = await this.catalogoMensajeService.obtenerCodigoCatalogoMensj(
        estadoDeMensaje.enviado,
      );

      // Se obtiene ID de mensaje
      const idMensaje = new mongoose.mongo.ObjectId();

      let codTraduccionMensaje = [];
      if (mensajeDto.traducciones && mensajeDto.traducciones.length) {
        let traduccion = mensajeDto.traducciones[0];

        // Se crea modelo de Traducción (Por ahora no hay traducción para chat)
        let newTraduccionMensaje = {
          contenido: traduccion.contenido,
          idioma: traduccion.idioma.codNombre,
          original: true,
          estado: catalogoEstadoEntidadTraduccion.catalogoEstado.codigo,
          referencia: idMensaje,
        };

        // Se guarda Traducción en BD.
        const crearTraduccionMensaje = await new this.tradMensajeModel(
          newTraduccionMensaje,
        ).save(opts);

        // Se convierte objeto BD a JSON.
        const dataTraduccion = JSON.parse(
          JSON.stringify(crearTraduccionMensaje),
        );

        // Se asigna ID de Traducción para utilizar en mensaje
        codTraduccionMensaje = [crearTraduccionMensaje._id];

        //  Se eliminar campos no necesarios para guardar en Historico
        delete dataTraduccion.__v;

        // Se crea modelo de Historico de Traducción de Mensaje
        let newHistoricoTraduccionMensaje: any = {
          datos: dataTraduccion,
          usuario: mensajeDto.propietario.perfil._id,
          accion: codAccionCrear,
          entidad: catalogoEstadoEntidadTraduccion.catalogoEntidad.codigo,
        };

        // Se guarda Historico de Traducción de Mensaje en BD.
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoTraduccionMensaje,
        );
      }

      var participantePropietarioMensaje = await this.participanteModel.findOne(
        {
          asociacion: idConversacion,
          perfil: mensajeDto.propietario.perfil._id,
        },
      );

      // Se crear modelo de Mensaje incluyendo traducciones
      let nuevoMensje = {
        _id: idMensaje,
        estado: catalogoEstadoEntidadMensaje.catalogoEstado.codigo,
        estatus: catalogoEstadoMensaje.codigo,
        tipo: mensajeDto.tipo.codigo,
        traducciones: codTraduccionMensaje,
        adjuntos: mensajeDto.adjuntos,
        conversacion: idConversacion,
        propietario: participantePropietarioMensaje._id,
        identificadorTemporal: mensajeDto.identificadorTemporal,
        entregadoA: listaParticipante,
        leidoPor: [mensajeDto.propietario.perfil._id],
      };

      if (
        mensajeDto.entidadCompartida &&
        mensajeDto.referenciaEntidadCompartida
      ) {
        nuevoMensje['entidadCompartida'] = mensajeDto.entidadCompartida.codigo;
        nuevoMensje['referenciaEntidadCompartida'] =
          mensajeDto.referenciaEntidadCompartida;
      }

      // Se guarda Mensaje en BD.
      const crearMensaje = await new this.mensajeModel(nuevoMensje).save(opts);

      // Se busca Mensaje en BD por ID de creación.
      var mensjGuardado = await this.mensajeModel
        .findById(crearMensaje._id)
        .session(opts.session)
        .select('-fechaActualizacion -__v')
        .populate(this.funcion.getListaPopulateMensaje())
        .lean();

      await this.funcion.formatoMensaje(
        mensjGuardado,
        codIdioma,
        this.catalogoIdiomasService,
        this.obtenerPortadaPredeterminadaProyectoResumenService,
        this.obtenerProyetoIdService,
        this.obtenerNoticiaIdService,
        this.obtenerPortadaService,
        opts,
      );

      let perfil = mensjGuardado.propietario['perfil'];
      const album = await this.getFotoPerfilService.getFotoPerfil(perfil._id);
      mensjGuardado.propietario['perfil'].album = [album.objAlbumTipoPerfil];

      // Se obtiene información de quien envia el mensaje
      let nombre = mensjGuardado.propietario['perfil']['nombre'];
      let nombreContacto =
        mensjGuardado.propietario['perfil']['nombreContacto'];
      let id = mensjGuardado.propietario['perfil']['_id'];

      // Se obtiene y reemplaza mensaje ya existente, y si no existe lo agrega a la lista
      /*const consultaListaUltimoMensaje = await this.conversacionModel.findById(idConversacion)
                .populate([{
                    path: 'ultimoMensaje -listaUltimoMensaje',
                    select: '_id conversacion traducciones propietario leidoPor fechaCreacion fechaActualizacion',
                    populate: [
                        populateConversacion,
                        populateTraducciones,
                        populatePropietario,
                        populateLeidoPor
                    ]
                }])
                .session(opts.session);

            let listaUltimoMensaje = consultaListaUltimoMensaje.listaUltimoMensaje;

            let existe = false;
            for (let i = 0; i < listaUltimoMensaje.length; i++) {
                let ultimoMensaje = listaUltimoMensaje[i];
                if (ultimoMensaje['propietario'].perfil._id == mensajeDto.propietario.perfil._id) {
                    listaUltimoMensaje[i] = crearMensaje._id;
                    existe = true;
                    break;
                }
            }

            if (!existe) {
                listaUltimoMensaje.push(crearMensaje._id);
            }*/

      // Se actualiza Conversación en BD.
      const updateConver = await this.conversacionModel.findOneAndUpdate(
        { _id: idConversacion },
        {
          $set: {
            /*listaUltimoMensaje: listaUltimoMensaje, */ ultimoMensaje:
              crearMensaje._id,
          },
        },
        opts,
      );

      if (
        mensajeDto.entidadCompartida &&
        mensajeDto.entidadCompartida.codigo === codEntidadProyecto.codigo
      ) {
        await this.proyectoModel.findOneAndUpdate(
          { _id: mensajeDto.referenciaEntidadCompartida },
          { $set: { transferenciaActiva: crearMensaje._id } },
          opts,
        );
      }

      // Se convierte objecto BD a JSON, y se elimina datos no necesario para guardar en Historico
      const dataMensaje = JSON.parse(JSON.stringify(crearMensaje));
      delete dataMensaje.__v;

      // Se crea modelo de Historico Mensaje.
      let newHistoricoMensaje: any = {
        datos: dataMensaje,
        usuario: mensjGuardado.propietario['perfil']['id'],
        accion: codAccionCrear,
        entidad: catalogoEstadoEntidadMensaje.catalogoEntidad.codigo,
      };

      // Se guarda Historico de Notifica en BD.
      this.crearHistoricoService.crearHistoricoServer(newHistoricoMensaje);

      // Se convierte objecto BD a JSON, y se elimina datos no necesario para guardar en Historico de Conversación
      const dataUpdateConver = JSON.parse(JSON.stringify(updateConver));
      delete dataUpdateConver.__v;

      // Se crea modelo de Historico Noticia.
      let newHistoricodataUpdateConver: any = {
        datos: updateConver,
        usuario: mensjGuardado.propietario['perfil']['id'],
        accion: codAcccionModificar,
        entidad: catalogoEstadoEntidadMensaje.catalogoEntidad.codigo,
      };
      // Se guarda Historico de Conversación en BD.
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricodataUpdateConver,
      );

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return mensjGuardado;
    } catch (error) {
      console.log(error);
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async actualizarConversacionMensajeNoLeido(mensajeDto: any): Promise<any> {
    try {
      const itemAccion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let codigoAccion = itemAccion.codigo;

      const itemEntidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.conversacion,
      );
      let codigoEntidad = itemEntidad.codigo;

      // Se actualiza mensaje en BD
      await this.conversacionModel.findOneAndUpdate(
        { _id: mensajeDto.conversacion._id },
        { ultimoMensaje: mensajeDto },
      );

      // Se crea modelo de Historico.
      let newHistorico: any = {
        datos: mensajeDto,
        usuario: mensajeDto.propietario['perfil']['id'],
        accion: codigoAccion,
        entidad: codigoEntidad,
        tipo: '',
      };

      // Se guarda Historico en BD.
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return true;
    } catch (error) {
      return null;
    }
  }

  async obtenerMensajeTransferencia(idMensaje, codIdioma, opts) {
    // Se busca Mensaje en BD por ID de creación.
    var mensjGuardado = await this.mensajeModel
      .findById(idMensaje)
      .session(opts.session)
      .select('-fechaActualizacion -__v')
      .populate(this.funcion.getListaPopulateMensaje())
      .lean();

    await this.funcion.formatoMensaje(
      mensjGuardado,
      codIdioma,
      this.catalogoIdiomasService,
      this.obtenerPortadaPredeterminadaProyectoResumenService,
      this.obtenerProyetoIdService,
      this.obtenerNoticiaIdService,
      this.obtenerPortadaService,
      opts,
    );

    return mensjGuardado;
  }
}
