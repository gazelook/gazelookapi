import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Conversacion } from 'src/drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { Mensaje } from 'src/drivers/mongoose/interfaces/mensaje/mensaje.interface';
import { ObtenerTodosParticipantePerfilService } from 'src/entidades/asociacion_participante/casos_de_uso/obtener-todos-participante-perfil.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { GetFotoPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil-general.service';
import { Funcion } from 'src/shared/funcion';
import {
  estadoDeMensaje,
  estadosPerfil,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';

@Injectable()
export class ObtenerMensajesNoLeidosService {
  constructor(
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: PaginateModel<Conversacion>,
    @Inject('MENSAJE_MODEL')
    private readonly mensajeModel: PaginateModel<Mensaje>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private participanteAso: ObtenerTodosParticipantePerfilService,
    private getFotoPerfilService: GetFotoPerfilService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );
  filtroBus = estadoDeMensaje;

  async obtenerMensajesNoLeidos(perfil: any, limite, pagina): Promise<any> {
    try {
      const participantes = await this.participanteAso.obtenerTodosParticipantePerfil(
        perfil,
      );

      const listaIdAsociacion = []; // Se coloca todas las converrsaciones del perfil

      for (const iterator of participantes) {
        listaIdAsociacion.push(iterator.asociacion);
      }

      const sortBusqueda = {};
      sortBusqueda['fechaActualizacion'] = -1;

      const populateConversacion = {
        path: 'conversacion',
        select: '_id',
      };

      const populateTraducciones = {
        path: 'traducciones',
        select: '-_id contenido',
      };

      const populatePropietario = {
        path: 'propietario',
        select: '_id perfil',
        populate: {
          path: 'perfil',
          select: '_id nombreContacto nombre',
          match: { _id: { $ne: perfil }, estado: estadosPerfil.activa },
        },
      };

      const populateLeidoPor = {
        path: 'leidoPor',
        select: '_id nombreContacto nombre',
      };

      const options = {
        lean: true,
        sort: sortBusqueda,
        select:
          '-listaUltimoMensaje -fechaCreacion -fechaActualizacion -estado -__v',
        populate: [
          {
            path: 'ultimoMensaje -listaUltimoMensaje',
            select:
              '_id conversacion traducciones propietario leidoPor fechaCreacion fechaActualizacion',
            populate: [
              populateConversacion,
              populateTraducciones,
              populatePropietario,
              populateLeidoPor,
            ],
            match: { leidoPor: { $ne: perfil }, estado: 'EST_99' }, // EST_99 -> MENSAJE ACTIVO
          },
          {
            path: 'asociacion',
            select: '-__v -estado',
          },
        ],
        page: Number(pagina),
        limit: Number(limite),
      };

      let busqueda = await this.conversacionModel.paginate(
        {
          asociacion: { $in: listaIdAsociacion },
          ultimoMensaje: { $ne: null },
        },
        options,
      );

      let lista = [];

      for (let item of busqueda.docs) {
        if (
          item.ultimoMensaje &&
          item.ultimoMensaje['propietario'] &&
          item.ultimoMensaje['propietario'].perfil &&
          item.ultimoMensaje['leidoPor'] &&
          item.ultimoMensaje['propietario'].perfil
        ) {
          let perfil = item.ultimoMensaje['propietario'].perfil;
          const album = await this.getFotoPerfilService.getFotoPerfil(
            perfil._id,
          );

          item.ultimoMensaje['propietario'].perfil.album = [
            album.objAlbumTipoPerfil,
          ];

          item.asociacion['tipo'] = {
            codigo: item.asociacion['tipo'],
          };

          lista.push(item);
        }
      }

      return lista;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async actualizarLecturaMensajesNoLeidos(
    idConversacion,
    idPerfil,
  ): Promise<any> {
    console.log(idPerfil);
    try {
      const catalogoEstadoEntidadMensajeActivo = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.mensaje,
        nombrecatalogoEstados.activa,
      );

      await this.mensajeModel.updateMany(
        {
          conversacion: idConversacion,
          estado: catalogoEstadoEntidadMensajeActivo.catalogoEstado.codigo, // Que esten activos por el propietario
          eliminadoPor: { $ne: idPerfil },
          leidoPor: { $ne: idPerfil },
        },
        {
          $addToSet: { leidoPor: idPerfil },
        },
      );
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async actualizarLecturaMensajeNoLeidos(idMensaje, idPerfil): Promise<any> {
    try {
      await this.mensajeModel.updateOne(
        {
          _id: idMensaje,
        },
        {
          $addToSet: { leidoPor: idPerfil },
        },
      );
    } catch (error) {
      throw error;
    }
  }
}
