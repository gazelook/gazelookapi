import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Conversacion } from 'src/drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { Mensaje } from 'src/drivers/mongoose/interfaces/mensaje/mensaje.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Funcion } from 'src/shared/funcion';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
const mongoose = require('mongoose');

@Injectable()
export class CambiarMensajesNoLeidosService {
  constructor(
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: Model<Conversacion>,
    @Inject('MENSAJE_MODEL') private readonly mensajeModel: Model<Mensaje>,

    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );

  async actualizarLeidoPor(itemMensaje: any, idPerfil): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const catalogoAccionEliminar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );

      const catalogoEstadoEntidadMensaje = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.mensaje,
        nombrecatalogoEstados.eliminado,
      );

      let iPerfilPropietarioMensaje = itemMensaje.propietario.perfil._id;
      // Se verifica si el usuario que elimina es propietario del mensaje

      let consultaMensaje = await this.mensajeModel
        .findById(itemMensaje._id)
        .session(opts.session);
      let listaLeidoPor = [];

      if (consultaMensaje.leidoPor) {
        listaLeidoPor = consultaMensaje.leidoPor;
      }

      listaLeidoPor.push(idPerfil);

      let updateMensaje = await this.mensajeModel.findOneAndUpdate(
        { _id: itemMensaje._id },
        { leidoPor: listaLeidoPor },
        opts,
      );

      const datosMensaje = JSON.parse(JSON.stringify(updateMensaje));
      delete datosMensaje.fechaCreacion;
      delete datosMensaje.fechaActualizacion;
      delete datosMensaje.__v;

      let newHistoricoMensaje: any = {
        datos: datosMensaje,
        usuario: idPerfil,
        accion: catalogoAccionEliminar.codigo,
        entidad: catalogoEstadoEntidadMensaje.catalogoEntidad.codigo,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistoricoMensaje);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
    } catch (e) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw e;
    }
  }
}
