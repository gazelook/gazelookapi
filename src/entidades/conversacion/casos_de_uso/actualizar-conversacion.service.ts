import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Conversacion } from 'src/drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { nombreAcciones, nombreEntidades } from '../../../shared/enum-sistema';
const mongoose = require('mongoose');
@Injectable()
export class ActualizarConversacionService {
  constructor(
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: Model<Conversacion>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
  ) {}

  async actualizarEstadoConversacion(
    asociacion: string,
    codEstado,
    opts?: any,
  ): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccion = accion.codigo;

      const entidadCo = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.conversacion,
      );
      let codEntidadConve = entidadCo.codigo;

      const dataActu = {
        estado: codEstado,
      };

      const conversacion = await this.conversacionModel.updateOne(
        { asociacion: asociacion },
        { $set: { estado: codEstado } },
        opts,
      );

      let newHistoriConver: any = {
        datos: await this.conversacionModel
          .findOne({ asociacion: asociacion })
          // .session(opts.session)
          .select('-fechaActualizacion -fechaCreacion -__v'),
        usuario: asociacion,
        accion: getAccion,
        entidad: codEntidadConve,
      };
      return true;
    } catch (error) {
      throw error;
    }
  }
}
