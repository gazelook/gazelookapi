import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Conversacion } from '../../../drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { Mensaje } from '../../../drivers/mongoose/interfaces/mensaje/mensaje.interface';
import { TraduccionEstaticaController } from '../../../multiIdioma/controladores/traduccion-estatica-controller';
import { erroresGeneral, erroresProyecto } from '../../../shared/enum-errores';
import { catalogoEstatusMensaje } from '../../../shared/enum-sistema';
import { ObtenerProyetoIdService } from '../../proyectos/casos_de_uso/obtener-proyecto-id.service';
import { TransferirProyectoService } from '../../proyectos/casos_de_uso/trasferir-proyecto.service';
import { GuardarMensajeService } from './guarda-mensaje.service';
const mongoose = require('mongoose');

@Injectable()
export class MensajeTransferirProyectoService {
  constructor(
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: Model<Conversacion>,
    @Inject('MENSAJE_MODEL') private readonly mensajeModel: Model<Mensaje>,
    private transferirProyectoService: TransferirProyectoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private obtenerProyetoIdService: ObtenerProyetoIdService,
    private guardarMensajeService: GuardarMensajeService,
  ) {}

  async confirmarTransferencia(idMensaje, status, idioma): Promise<any> {
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      const getMensaje: any = await this.mensajeModel
        .findById(idMensaje)
        .populate([
          {
            path: 'propietario',
            select: 'perfil',
          },
        ]);

      if (!getMensaje) {
        await session.abortTransaction();
        session.endSession();
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresGeneral.ERROR_ACTUALIZAR,
        };
      }
      const idProyecto = getMensaje.referenciaEntidadCompartida.toString();
      const getProyecto = await this.obtenerProyetoIdService.obtenerProyectoById(
        idProyecto,
      );

      if (!getProyecto) {
        await session.abortTransaction();
        session.endSession();
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresGeneral.ERROR_ACTUALIZAR,
        };
      }
      // VALIDAR EL MENSAJE CON EL DE TRANSFERENCIA_ACTIVA
      if (
        getMensaje.estatus === catalogoEstatusMensaje.enviado &&
        getMensaje._id === getProyecto?.transferenciaActiva.toString()
      ) {
        //_______________________________________________ACEPTA la transferencia____________________________
        if (catalogoEstatusMensaje.aceptado === status) {
          const perfilPropietario = getProyecto.perfil.toString();
          let perfilNuevo;
          // obtener el perfil del nuevo propietario
          console.log('getMensaje.entregadoA:', getMensaje.entregadoA);

          if (getMensaje.entregadoA.length > 0) {
            for (const perfil of getMensaje.entregadoA) {
              if (perfil.toString() !== perfilPropietario) {
                perfilNuevo = perfil.toString();
                break;
              }
            }
          } else {
            await session.abortTransaction();
            session.endSession();
            throw {
              codigo: HttpStatus.OK,
              codigoNombre: erroresProyecto.ERROR_TRANSFERIR_PROYECTO,
            };
          }

          await this.transferirProyectoService.trasferirProyecto(
            idProyecto,
            perfilPropietario,
            perfilNuevo,
          );

          // actualizar estado mensaje
          await this.mensajeModel.updateOne(
            { _id: idMensaje },
            { estatus: catalogoEstatusMensaje.aceptado },
            opts,
          );

          const TRANSFERIR_PROYECTO = await this.traduccionEstaticaController.traduccionEstatica(
            idioma,
            'TRANSFERIR_PROYECTO',
          );

          const resultTransferencia = await this.guardarMensajeService.obtenerMensajeTransferencia(
            getMensaje._id,
            idioma,
            opts,
          );
          await session.commitTransaction();
          session.endSession();

          return { mensaje: TRANSFERIR_PROYECTO, data: resultTransferencia };

          //_______________________________________________Rechaza la transferencia____________________________
        } else if (catalogoEstatusMensaje.rechazado === status) {
          // actualizar estado mensaje
          await this.mensajeModel.updateOne(
            { _id: idMensaje },
            { estatus: catalogoEstatusMensaje.rechazado },
            opts,
          );
          const SOLICITUD_CANCELADA = await this.traduccionEstaticaController.traduccionEstatica(
            idioma,
            'SOLICITUD_CANCELADA',
          );

          const resultTransferencia = await this.guardarMensajeService.obtenerMensajeTransferencia(
            getMensaje._id,
            idioma,
            opts,
          );
          await session.commitTransaction();
          session.endSession();

          return { mensaje: SOLICITUD_CANCELADA, data: resultTransferencia };
        } else {
          await session.abortTransaction();
          session.endSession();
          throw {
            codigo: HttpStatus.OK,
            codigoNombre: erroresGeneral.PARAMETROS_NO_VALIDOS,
          };
        }
      } else {
        await session.abortTransaction();
        session.endSession();
        throw {
          codigo: HttpStatus.CONFLICT,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
