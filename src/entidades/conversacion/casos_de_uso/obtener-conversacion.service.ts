import { Inject, Injectable } from '@nestjs/common';
import { Model, PaginateModel } from 'mongoose';
import { Conversacion } from 'src/drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { Mensaje } from 'src/drivers/mongoose/interfaces/mensaje/mensaje.interface';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerNoticiaIdService } from 'src/entidades/noticia/casos_de_uso/obtener-noticia-id.service';
import { ObtenerPortadaService } from 'src/entidades/noticia/casos_de_uso/obtener-portada-predeterminada.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from 'src/entidades/proyectos/casos_de_uso/obtener-portada-predeterminada-proyecto-resumen.service';
import { ObtenerProyetoIdService } from 'src/entidades/proyectos/casos_de_uso/obtener-proyecto-id.service';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';

@Injectable()
export class ObtenerConversacionService {
  constructor(
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: Model<Conversacion>,
    @Inject('MENSAJE_MODEL')
    private readonly mensajeModel: PaginateModel<Mensaje>,

    private readonly catalogoIdiomasService: CatalogoIdiomasService,
    private readonly catalogoEntidadService: CatalogoEntidadService,
    private readonly catalogoEstadoService: CatalogoEstadoService,
    private readonly obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
    private obtenerProyetoIdService: ObtenerProyetoIdService,
    private obtenerNoticiaIdService: ObtenerNoticiaIdService,
    private obtenerPortadaService: ObtenerPortadaService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );

  async obtenerConversacionDesdeFecha(
    idPerfil,
    idAsociacion,
    codIdioma,
    fecha,
    limite,
    pagina,
  ): Promise<any> {
    try {
      const catalogoEstadoEntidadMensajeActivo = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.mensaje,
        nombrecatalogoEstados.activa,
      );

      const sortBusqueda = {};
      sortBusqueda['fechaCreacion'] = fecha ? 1 : -1;

      const options = {
        lean: true,
        sort: sortBusqueda,
        select: '-fechaActualizacion -__v',
        populate: this.funcion.getListaPopulateMensaje(),
        page: Number(pagina),
        limit: Number(limite),
      };

      let filtro = {
        conversacion: idAsociacion, // Que sean de la conversacion
        estado: catalogoEstadoEntidadMensajeActivo.catalogoEstado.codigo, // Que esten activos por el propietario
        eliminadoPor: { $ne: idPerfil },
      };

      if (fecha) {
        filtro['fechaCreacion'] = { $gte: new Date(fecha) };
      }

      let results = await this.mensajeModel.paginate(filtro, options);

      for (let mensaje of results.docs) {
        await this.funcion.formatoMensaje(
          mensaje,
          codIdioma,
          this.catalogoIdiomasService,
          this.obtenerPortadaPredeterminadaProyectoResumenService,
          this.obtenerProyetoIdService,
          this.obtenerNoticiaIdService,
          this.obtenerPortadaService,
          options,
        );
      }

      return results;
    } catch (error) {
      throw error;
    }
  }

  async obtenerConversacionPorId(idConversacion): Promise<any> {
    try {
      return await this.conversacionModel
        .findById(idConversacion)
        .select('-listaUltimoMensaje -fechaCreacion -fechaActualizacion -__v')
        .populate({
          path: 'asociacion',
          select: '-__v -estado',
        })
        .lean();
    } catch (error) {
      throw error;
    }
  }
}
