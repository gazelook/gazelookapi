import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';
import {
  estadoDeMensaje,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { Inject } from '@nestjs/common';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Conversacion } from 'src/drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { Mensaje } from 'src/drivers/mongoose/interfaces/mensaje/mensaje.interface';
import { TraduccionMensaje } from '../../../drivers/mongoose/interfaces/traduccion_mensaje/traduccion-mensaje.interface';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';

@Injectable()
export class EliminarMensajesService {
  constructor(
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: Model<Conversacion>,
    @Inject('MENSAJE_MODEL') private readonly mensajeModel: Model<Mensaje>,
    @Inject('TRAD_MSJ_MODEL')
    private readonly traduccionMensajesModel: Model<TraduccionMensaje>,

    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );
  filtroBus = estadoDeMensaje;

  async eliminarListaMensaje(listaMensaje: any, idPerfil: any): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session, new: true };

      const catalogoAccionEliminar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );

      const catalogoEstadoEntidadMensaje = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.mensaje,
        nombrecatalogoEstados.eliminado,
      );

      let idPerfilElimina = idPerfil;

      // Se recorre lista de mensajes a eliminar
      var listaMensajeEliminado = [];
      for (const itemMensaje of listaMensaje) {
        let iPerfilPropietarioMensaje = itemMensaje.propietario.perfil._id;
        // Se verifica si el usuario que elimina es propietario del mensaje
        let updateMensaje = null;

        if (idPerfilElimina === iPerfilPropietarioMensaje) {
          updateMensaje = await this.mensajeModel.findOneAndUpdate(
            { _id: itemMensaje._id },
            { estado: catalogoEstadoEntidadMensaje.catalogoEstado.codigo },
            opts,
          );
        } else {
          updateMensaje = await this.mensajeModel.findOneAndUpdate(
            { _id: itemMensaje._id },
            { $addToSet: { eliminadoPor: idPerfilElimina } },
            opts,
          );
        }

        const datosMensaje = JSON.parse(JSON.stringify(updateMensaje));
        delete datosMensaje.fechaCreacion;
        delete datosMensaje.fechaActualizacion;
        delete datosMensaje.__v;

        let newHistoricoMensaje: any = {
          datos: datosMensaje,
          usuario: idPerfilElimina,
          accion: catalogoAccionEliminar.codigo,
          entidad: catalogoEstadoEntidadMensaje.catalogoEntidad.codigo,
        };

        this.crearHistoricoService.crearHistoricoServer(newHistoricoMensaje);

        let listaEliminadoPor = [];

        for (let item of updateMensaje.eliminadoPor) {
          listaEliminadoPor.push({
            _id: item,
          });
        }

        listaMensajeEliminado.push({
          _id: updateMensaje._id,
          estado: {
            codigo: updateMensaje.estado,
          },
          tipo: {
            codigo: updateMensaje.tipo,
          },
          eliminadoPor: listaEliminadoPor,
        });
      }

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return listaMensajeEliminado;
    } catch (e) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw e;
    }
  }

  async eliminarMensajesPerfil(
    propietario: string,
    asociacion: string,
    opts: any,
  ): Promise<any> {
    try {
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      let getAccion = accion.codigo;

      const entidadMen = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.mensaje,
      );
      let codEntidadMens = entidadMen.codigo;
      const estadoMens = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadMens,
      );
      let codEstadoMensaje = estadoMens.codigo;

      // conversacion
      const entidadCo = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.conversacion,
      );
      let codEntidadConve = entidadCo.codigo;
      const estadoCo = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadConve,
      );
      let codEstadoCo = estadoCo.codigo;

      // traduccion mensajes
      const entidadTradMen = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMensajes,
      );
      let codEntidadTradMens = entidadTradMen.codigo;
      const estadoTradMens = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadTradMens,
      );
      let codEstadoTradMensaje = estadoTradMens.codigo;

      const getConversacion = await this.conversacionModel.findOne({
        asociacion: asociacion,
      });

      const getMensaje = await this.mensajeModel
        .findOne({
          $and: [
            { propietario: propietario },
            { conversacion: getConversacion._id },
          ],
        })
        .populate([
          {
            path: 'traducciones',
          },
          {
            path: 'adjuntos',
          },
          {
            path: 'propietario',
          },
        ]);

      if (!getMensaje) {
        return null;
      }

      if (getMensaje?.traducciones?.length > 0) {
        for (const traduccion of getMensaje.traducciones) {
          // estado eliminado traducc mensj
          await this.traduccionMensajesModel.updateOne(
            { _id: traduccion['_id'] },
            {
              $set: {
                estado: codEstadoTradMensaje,
                fechaActualizacion: new Date(),
              },
            },
            opts,
          );
        }
      }

      /*if (getMensaje.adjuntos?.length > 0) {
                for (const adjunto of getMensaje.adjuntos) {
                    this.eliminarMediaService.eliminarMedia(
                        adjunto['_id'], getMensaje.propietario['perfil'].toString(), false, opts
                    );
                }
            }*/

      // estado eliminar mensaje y conversacion
      const dataMens = {
        estado: codEstadoMensaje,
        fechaActualizacion: new Date(),
      };
      const updatemesj = await this.mensajeModel.findByIdAndUpdate(
        getMensaje._id,
        dataMens,
        opts,
      );
      // historico mensaje
      let newHistoricoMensaje: any = {
        datos: updatemesj,
        usuario: getMensaje.propietario['perfil'].toString(),
        accion: getAccion,
        entidad: entidadMen.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistoricoMensaje);

      // conversacion
      const dataConversacion = {
        estado: codEstadoCo,
        fechaActualizacion: new Date(),
      };
      const conversacionUpdate = await this.conversacionModel.findByIdAndUpdate(
        getConversacion._id,
        dataConversacion,
        opts,
      );

      // historico conversacion
      let newHistoricoConversa: any = {
        datos: conversacionUpdate,
        usuario: getMensaje.propietario['perfil'].toString(),
        accion: getAccion,
        entidad: entidadMen.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistoricoConversa);

      return true;
    } catch (error) {
      throw error;
    }
  }
}
