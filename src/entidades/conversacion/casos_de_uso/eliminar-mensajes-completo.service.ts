import { Inject } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Conversacion } from 'src/drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { Mensaje } from 'src/drivers/mongoose/interfaces/mensaje/mensaje.interface';
import { TraduccionMensaje } from '../../../drivers/mongoose/interfaces/traduccion_mensaje/traduccion-mensaje.interface';
import { EliminarMediaCompletoService } from '../../media/casos_de_uso/eliminar-media-completo.service';

@Injectable()
export class EliminarMensajesCompletoService {
  constructor(
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: Model<Conversacion>,
    @Inject('MENSAJE_MODEL') private readonly mensajeModel: Model<Mensaje>,
    @Inject('TRAD_MSJ_MODEL')
    private readonly traduccionMensajesModel: Model<TraduccionMensaje>,
    private eliminarMediaCompletoService: EliminarMediaCompletoService,
  ) {}

  async EliminarMensajesCompleto(
    propietario: string,
    asociacion: string,
    opts?: any,
  ): Promise<any> {
    try {
      const getConversacion = await this.conversacionModel.findOne({
        asociacion: asociacion,
      });

      if (!getConversacion) {
        return null;
      }
      const getMensaje = await this.mensajeModel
        .findOne({
          $and: [
            { propietario: propietario },
            { conversacion: getConversacion._id },
          ],
        })
        .populate([
          {
            path: 'traducciones',
          },
          {
            path: 'adjuntos',
          },
        ]);

      if (getMensaje?.traducciones?.length > 0) {
        for (const traduccion of getMensaje.traducciones) {
          await this.traduccionMensajesModel.deleteOne(
            { _id: traduccion['_id'] },
            opts,
          );
        }
      }

      if (getMensaje?.adjuntos?.length > 0) {
        for (const adjunto of getMensaje.adjuntos) {
          await this.eliminarMediaCompletoService.eliminarMediaCompleto(
            adjunto['_id'],
            opts,
          );
        }
      }
      // eliminar mensaje y conversacion
      if (getMensaje) {
        await this.mensajeModel.deleteOne({ _id: getMensaje._id }, opts);
        await this.conversacionModel.deleteOne(
          { _id: getConversacion._id },
          opts,
        );
      }

      return true;
    } catch (error) {
      throw error;
    }
  }
}
