import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { Conversacion } from 'src/drivers/mongoose/interfaces/conversacion/conversacion.interface';
import { nombreAcciones, nombreEntidades } from 'src/shared/enum-sistema';

@Injectable()
export class CrearConversacionService {
  constructor(
    @Inject('CONVERSACION_MODEL')
    private readonly conversacionModel: Model<Conversacion>,
    private crearHistoricoService: CrearHistoricoService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
  ) {}

  async crearConversacion(crearConversacion: any, opts: any): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      const entidadCo = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.conversacion,
      );
      let codEntidadConve = entidadCo.codigo;

      // const crearConversacion = {
      //     _id: idConversacion,
      //     asociacion: crearAsociacion._id,
      // }

      const conversacion = await new this.conversacionModel(
        crearConversacion,
      ).save(opts);
      let newHistoriConver: any = {
        datos: conversacion,
        usuario: crearConversacion.asociacion,
        accion: getAccion,
        entidad: codEntidadConve,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistoriConver);

      return true;
    } catch (error) {
      throw error;
    }
  }
}
