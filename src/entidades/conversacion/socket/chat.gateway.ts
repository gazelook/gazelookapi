import { Controller, HttpStatus, Post } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {
  ApiBearerAuth,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Server, Socket } from 'socket.io';
import { AuthService } from 'src/auth/auth.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearComentarioService } from 'src/entidades/comentarios/casos_de_uso/crear-comentario.service';
import { RespuestaComentarioDto } from 'src/entidades/comentarios/entidad/respuesta-comentario-dto';
import { ActualizarNotificacionService } from 'src/entidades/notificacion/casos_de_uso/actualizar-notificacion.service';
import { CrearNotificacionService } from 'src/entidades/notificacion/casos_de_uso/crear-notificacion.service';
import {
  CrearNotificacionDto,
  listaNotificacionDto,
  notificacionDto,
} from 'src/entidades/notificacion/entidad/notificacion-dto.service';
import { ObtenerPerfilBasicoService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-basico.service';
import { ObtenerPerfilProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-creador-proyecto.service';
import {
  codigosCatalogoEventoNotificacion,
  codigosCatalogoStatusNotificacion,
  codigosEstadosNotificacion,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { ObtenerParticipanteAsociacionTipoService } from '../../asociacion_participante/casos_de_uso/obtener-participante-asociacion-tipo.service';
import { MensajeTransferirProyectoService } from '../casos_de_uso/confirmar-transferir-proyecto.service';
import { EliminarMensajesService } from '../casos_de_uso/eliminar-mensajes.service';
import { GuardarMensajeService } from '../casos_de_uso/guarda-mensaje.service';
import { ObtenerConversacionService } from '../casos_de_uso/obtener-conversacion.service';
import { ObtenerMensajesNoLeidosService } from '../casos_de_uso/obtener-mensajes-no-leidos.service';
import {
  ConversacionDto,
  ListaMensajeDto,
  ListaMensajeEliminarDto,
  MensajeLeidoPorSocketDto,
} from '../entidad/conversacion-dto';

@ApiTags('Chat')
@Controller('api/chat')
@WebSocketGateway()
export class ChatGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  usuariosConectados = [];
  usuariosEnSalas = [];

  constructor(
    private obtenerConversacionService: ObtenerConversacionService,
    private guardarMensajeService: GuardarMensajeService,
    private eliminarMensajesService: EliminarMensajesService,
    private obtenerParticipanteAsociacionTipoService: ObtenerParticipanteAsociacionTipoService,
    private obtenerMensajeNoLeido: ObtenerMensajesNoLeidosService,
    private mensajeTransferirProyectoService: MensajeTransferirProyectoService,
    private crearComentarioService: CrearComentarioService,
    private obtenerPerfilProyectoService: ObtenerPerfilProyectoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearNotificacionService: CrearNotificacionService,
    private actualizarNotificacionService: ActualizarNotificacionService,
    private obtenerPerfilBasicoService: ObtenerPerfilBasicoService,
    private authService: AuthService,
    private jwtService: JwtService,
    private i18n: I18nService,
  ) {}

  funcion = new Funcion(this.i18n);

  @WebSocketServer()
  server: Server;

  // Levantamiento del servidor
  afterInit(server: Server) {
    console.log('Servidor socket iniciado correctamente.');
  }

  // Conexión del cliente
  @Post('/conectar')
  @ApiBearerAuth('Authorization Bearer')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({ name: 'idPerfil', description: 'Id de perfil' })
  @ApiOperation({ summary: 'Conexión al socket; ruta => http://{host}:{port}' })
  @ApiResponse({
    status: 200,
    description:
      'Comunica al resto de usuarios conectados que este usuario se ha conectado',
  })
  async handleConnection(client: Socket, ...args: any[]) {
    this.esSesionValida(client);

    const idPerfil = client.handshake.query.idPerfil;
    let user = {
      socket: client,
      clienteId: client.id,
      perfil: idPerfil,
    };

    const encuentraOtroUsuario = await this.usuariosConectados.find(
      element => element.perfil === idPerfil,
    );

    for (const [i, iterator] of this.usuariosConectados.entries()) {
      if (iterator.perfil === idPerfil) {
        this.usuariosConectados.splice(i, 1);
        break;
      }
    }

    this.usuariosConectados.push(user);

    this.server.emit('users', client.id);

    console.log('Usuarios conectados: ' + this.usuariosConectados.length);
    console.log(
      this.usuariosConectados.map(function(el) {
        return el.perfil;
      }),
    );
  }

  // Desconexión del cliente
  @Post('/desconectar')
  @ApiOperation({
    summary: 'Desconexión del socket; ruta => http://{host}:{port}',
  })
  @ApiResponse({
    status: 200,
    description:
      'Comunica al resto de usuarios conectados que este usuario se ha desconectado',
  })
  async handleDisconnect(client: Socket) {
    console.log('Desconectado: ' + client.id);

    const idPerfil = client.handshake.query.idPerfil;
    let user = {
      socket: client,
      clienteId: client.id,
      perfil: idPerfil,
    };

    this.removeClient(this.usuariosConectados, user);

    console.log('Usuarios conectados: ' + this.usuariosConectados.length);
    console.log(
      this.usuariosConectados.map(function(el) {
        return el.perfil;
      }),
    );

    //this.server.emit('users', client.id);
  }

  @Post('/unirseConversacion')
  @ApiBearerAuth('Authorization Bearer')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({ name: 'idPerfil', description: 'Id de perfil' })
  //@ApiParam({ name: 'asociacion', type: ConversacionDto, description: 'Objeto con Id de la asociacion'})
  @ApiOperation({
    summary:
      'Unirse a una conversación; ruta => http://{host}:{port}/unirseConversacion',
  })
  @ApiResponse({
    status: 200,
    description: 'Se ha unido a la conversación correctamente.',
  })
  @SubscribeMessage('unirseConversacion')
  async unirseConversacion(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: ConversacionDto,
  ) {
    this.esSesionValida(client);

    const event = 'unirseConversacion';

    // Se obtiene ID de conversación
    const idConversacion = data.asociacion._id;

    if (mongoose.isValidObjectId(idConversacion)) {
      await this.obtenerMensajeNoLeido.actualizarLecturaMensajesNoLeidos(
        idConversacion,
        client.handshake.query.idPerfil,
      );

      client.in(idConversacion).emit(
        event,
        this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: data,
        }),
      );

      // Se une socket => cliente a la conversación
      client.join(idConversacion);
    }
  }

  @Post('/salirConversacion')
  @ApiBearerAuth('Authorization Bearer')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({ name: 'idPerfil', description: 'Id de perfil' })
  //@ApiParam({ name: 'data', type: ConversacionDto, description: 'Objeto con Id de la asociacion'})
  @ApiOperation({
    summary:
      'Salirse de una conversación; ruta => http://{host}:{port}/salirConversacion',
  })
  @ApiResponse({
    status: 200,
    description: 'Se ha salido de la conversación correctamente.',
  })
  @SubscribeMessage('salirConversacion')
  async salirConversacion(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: ConversacionDto,
  ) {
    this.esSesionValida(client);

    const event = 'salirConversacion';

    // Se obtiene ID de conversación
    var idConversacion = data.asociacion._id;

    if (mongoose.isValidObjectId(idConversacion)) {
      // Se sale socket => cliente de la conversación
      client.leave(idConversacion);

      client.in(idConversacion).emit(
        event,
        this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: data,
        }),
      );
    }
  }

  @Post('/mensaje')
  @ApiBearerAuth('Authorization Bearer')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({ name: 'idPerfil', description: 'Id de perfil' })
  //@ApiParam({ name: 'data', type: MensajeEnvioSocketDto, description: 'Objeto del mensaje'})
  @ApiOperation({
    summary: 'Enviar / Reenviar mensaje; ruta => http://{host}:{port}/mensaje',
  })
  @ApiResponse({
    status: 200,
    description: 'El mensaje se ha enviado / reenviado correctamente.',
  })
  @SubscribeMessage('mensaje')
  async nuevoMensaje(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: ListaMensajeDto,
  ) {
    this.esSesionValida(client);

    const codIdioma = this.funcion.obtenerIdiomaDefecto(
      client.handshake.query.idioma,
    );
    let event = 'mensaje';

    if (data.listaMensaje && data.listaMensaje.length) {
      var listaMensaje = data.listaMensaje;
      for (let mensaje of listaMensaje) {
        console.log(mensaje);
        if (
          mensaje.listaIdConversacion &&
          mensaje.listaIdConversacion.length &&
          mensaje.identificadorTemporal &&
          mensaje.propietario &&
          mensaje.tipo &&
          ((mensaje.traducciones && mensaje.traducciones.length) ||
            (mensaje.adjuntos && mensaje.adjuntos.length) ||
            mensaje.entidadCompartida ||
            mensaje.referenciaEntidadCompartida)
        ) {
          mensaje.estatus = {
            codigo: 'CTM_2',
          }; // Es un estado que ya no se ocupa.

          var listaIdConversacion = mensaje.listaIdConversacion;

          for (let conversacion of listaIdConversacion) {
            try {
              const listaParticipante = await this.obtenerParticipanteAsociacionTipoService.obtenerPerfilesDeAsociacionSimple(
                conversacion['_id'],
              );
              const listaIdPerfilParticipante = listaParticipante.map(function(
                el,
              ) {
                return el.perfil['_id'];
              });

              let gMsj = await this.guardarMensajeService.guardarMensaje(
                mensaje,
                conversacion['_id'],
                listaIdPerfilParticipante,
                codIdioma,
              );

              if (gMsj) {
                let listaIdSocketConversacion = await this.obtenerClientesEnConversacion(
                  conversacion['_id'],
                );

                // RECORRE LOS PARTICIPANTES DE LA CONVERSACION
                for (const participante of listaParticipante) {
                  // RECORRE LOS SOCKET CONECTADOS COMO PARTICIPANTES DE LA CONVERSACION
                  let esEnviadoParticipantes = false;
                  for (const idSocket of listaIdSocketConversacion) {
                    if (
                      this.server.sockets.connected[idSocket].handshake.query
                        .idPerfil == participante.perfil['_id']
                    ) {
                      this.server.sockets.connected[idSocket].emit(
                        event,
                        this.funcion.enviarRespuestaOptimizada({
                          codigoEstado: HttpStatus.OK,
                          datos: gMsj,
                        }),
                      );
                      esEnviadoParticipantes = true;
                    }
                  }

                  // SI NO SE HA ENVIADO MENSAJES A PARTICIPANTES DENTRO DEL CHAT, SE ENVIA COMO NOTIFICACION
                  if (!esEnviadoParticipantes) {
                    let usuario = this.usuariosConectados.find(
                      element => element.perfil == participante.perfil['_id'],
                    );

                    if (usuario) {
                      let conversacionMensaje = await this.obtenerConversacionService.obtenerConversacionPorId(
                        conversacion['_id'],
                      );
                      conversacionMensaje.ultimoMensaje = gMsj;

                      let cn = new CrearNotificacionDto();
                      cn.evento = {
                        codigo:
                          codigosCatalogoEventoNotificacion.mensajeNoLeido,
                      };

                      cn.entidad = {
                        codigo: (
                          await this.catalogoEntidadService.obtenerNombreEntidad(
                            nombreEntidades.conversacion,
                          )
                        ).codigo,
                      };

                      cn.data = conversacionMensaje;

                      usuario.socket.emit(
                        'notificacion',
                        this.funcion.enviarRespuestaOptimizada({
                          codigoEstado: HttpStatus.OK,
                          datos: cn,
                        }),
                      );
                    }
                  }
                }

                return this.funcion.enviarRespuestaOptimizada({
                  codigoEstado: HttpStatus.OK,
                  mensaje: 'Proceso realizado correctamente',
                  datos: gMsj,
                });
              }
            } catch (e) {
              console.error(e);
              return this.funcion.enviarRespuestaOptimizada({
                codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
                mensaje: await this.funcion.obtenerTraduccionEstatica(
                  codIdioma,
                  'ERROR_SOLICITUD',
                ),
                datos: mensaje,
              });
            }
          }
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_ACCEPTABLE,
            mensaje: await this.funcion.obtenerTraduccionEstatica(
              codIdioma,
              'PARAMETROS_NO_VALIDOS',
            ),
            datos: mensaje,
          });
        }
      }
    } else {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        ),
        datos: data,
      });
    }
  }

  @Post('/eliminarMensaje')
  @ApiBearerAuth('Authorization Bearer')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({ name: 'idPerfil', description: 'Id de perfil' })
  //@ApiParam({ name: 'data', type: ListaMensajeDto, description: 'Lista de objetos de mensajes'})
  @ApiOperation({
    summary:
      'Eliminar mensaje(s); ruta => http://{host}:{port}/eliminarMensaje',
  })
  @ApiResponse({
    status: 200,
    description:
      'El/Los mensaje(s) se han eliminado correctamente. Y se comunica a todos los miembros de la conversación, para la respectiva eliminación en el frondend.',
  })
  @SubscribeMessage('eliminarMensaje')
  async eliminarMensaje(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: ListaMensajeEliminarDto,
  ) {
    this.esSesionValida(client);
    const codIdioma = this.funcion.obtenerIdiomaDefecto(
      client.handshake.query.idioma,
    );
    let event = 'eliminarMensaje';

    var listaMensaje = data.listaMensaje;

    try {
      if (listaMensaje.length) {
        var listaEliminado = await this.eliminarMensajesService.eliminarListaMensaje(
          listaMensaje,
          client.handshake.query.idPerfil,
        );

        this.server.in(listaMensaje[0].conversacion._id).emit(
          event,
          this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: listaEliminado,
            //PRUEBAS INTERFAZ DALTON
            /*datos: {
            perfil: {
              _id: client.handshake.query.idPerfil
            },
            listaMensaje: listaEliminado
          }*/
          }),
        );
      }
    } catch (e) {
      console.error(e);
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'ERROR_SOLICITUD',
        ),
        datos: data,
      });
    }
  }

  @Post('/leidoPor')
  @ApiBearerAuth('Authorization Bearer')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({ name: 'idPerfil', description: 'Id de perfil' })
  //@ApiParam({ name: 'data', type: ListaMensajeDto, description: 'Lista de objetos de mensajes'})
  @ApiOperation({
    summary:
      'Establecer que el mensaje se ha leido por un perfil de usuario ruta => http://{host}:{port}/leidoPor',
  })
  @ApiResponse({
    status: 200,
    description:
      'El mensaje se ha leido correctamente. Y se comunica a todos los miembros de la conversación, para la respectiva notificacion en el frondend.',
  })
  @SubscribeMessage('leidoPor')
  async leidoPor(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: MensajeLeidoPorSocketDto,
  ) {
    this.esSesionValida(client);
    const codIdioma = this.funcion.obtenerIdiomaDefecto(
      client.handshake.query.idioma,
    );
    let event = 'leidoPor';

    var mensaje = data;

    try {
      await this.obtenerMensajeNoLeido.actualizarLecturaMensajeNoLeidos(
        mensaje._id,
        client.handshake.query.idPerfil,
      );

      this.server.in(mensaje.conversacion._id).emit(
        event,
        this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: mensaje,
        }),
      );
    } catch (e) {
      console.error(e);
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'ERROR_SOLICITUD',
        ),
        datos: data,
      });
    }
  }

  @Post('/transferenciaProyecto')
  @ApiBearerAuth('Authorization Bearer')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({ name: 'idPerfil', description: 'Id de perfil' })
  //@ApiParam({ name: 'data', type: ListaMensajeDto, description: 'Lista de objetos de mensajes'})
  @ApiOperation({
    summary:
      'Establecer que el mensaje se ha leido por un perfil de usuario ruta => http://{host}:{port}/leidoPor',
  })
  @ApiResponse({
    status: 200,
    description:
      'El mensaje se ha leido correctamente. Y se comunica a todos los miembros de la conversación, para la respectiva notificacion en el frondend.',
  })
  @SubscribeMessage('transferenciaEntidad')
  async transferirProyecto(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: any,
  ) {
    //data: MensajeTransferenciaDto
    this.esSesionValida(client);
    const codIdioma = this.funcion.obtenerIdiomaDefecto(
      client.handshake.query.idioma,
    );
    let event = 'mensaje';

    const mensaje = data.mensaje;
    console.log('dataMensajeTransferir: ', data);

    try {
      const transferencia = await this.mensajeTransferirProyectoService.confirmarTransferencia(
        mensaje._id,
        mensaje.estatus.codigo,
        codIdioma,
      );
      console.log('transferenciaProy', transferencia);

      this.server.in(mensaje.conversacion._id).emit(
        event,
        this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: transferencia.mensaje,
          datos: transferencia.data,
        }),
      );
    } catch (e) {
      console.error(e);
      if (e?.codigoNombre) {
        /* const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
          lang: codIdioma
        }); */
        const MENSAJE = await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          e.codigoNombre,
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'ERROR_SOLICITUD',
          ),
          datos: mensaje,
        });
      }
    }
  }

  @Post('/notificacion')
  @ApiBearerAuth('Authorization Bearer')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({ name: 'idPerfil', description: 'Id de perfil' })
  //@ApiParam({ name: 'data', type: ListaMensajeDto, description: 'Lista de objetos de mensajes'})
  @ApiOperation({
    summary: 'Crea una nueva notificacion => http://{host}:{port}/notificacion',
  })
  @ApiResponse({
    status: 201,
    type: RespuestaComentarioDto,
    description: 'Creado',
  })
  @SubscribeMessage('notificacion')
  async notificacion(
    @ConnectedSocket() client: Socket,
    @MessageBody() notificacion: notificacionDto,
  ) {
    this.esSesionValida(client);
    const codIdioma = this.funcion.obtenerIdiomaDefecto(
      client.handshake.query.idioma,
    );
    let event = 'notificacion';

    try {
      let crearNotificacion: any;
      if (
        notificacion.evento.codigo ===
        codigosCatalogoEventoNotificacion.crearComentario
      ) {
        crearNotificacion = await this.notificacionComentario(
          event,
          notificacion,
          codIdioma,
        );
      } else if (
        notificacion.evento.codigo ===
          codigosCatalogoEventoNotificacion.llamadaDeAudio ||
        notificacion.evento.codigo ===
          codigosCatalogoEventoNotificacion.videoLlamada
      ) {
        //crea la notificacion
        crearNotificacion = await this.crearNotificacionService.crearNotificacion(
          {
            entidad: notificacion.entidad.codigo,
            evento: notificacion.evento.codigo,
            accion: notificacion.accion.codigo || null,
            data: notificacion.data,
            estado: codigosEstadosNotificacion.activa,
            status: codigosCatalogoStatusNotificacion.entregada,
            idEntidad: notificacion.idEntidad,
            perfil: null,
            listaPerfiles: notificacion.listaPerfiles.map(function(el) {
              return el._id;
            }),
          },
        );

        crearNotificacion.data.propietario.perfil = await this.obtenerPerfilBasicoService.obtenerPerfilSimple(
          crearNotificacion.data.propietario.perfil._id,
        );

        for (let i = 0; i < crearNotificacion.listaPerfiles.length; i++) {
          crearNotificacion.listaPerfiles[
            i
          ] = await this.obtenerPerfilBasicoService.obtenerPerfilSimple(
            crearNotificacion.listaPerfiles[i],
          );
        }

        for (const listaPerfiles of notificacion.listaPerfiles) {
          let usuario = this.usuariosConectados.find(
            element => element.perfil == listaPerfiles._id,
          );

          if (usuario) {
            usuario.socket.emit(
              event,
              this.funcion.enviarRespuestaOptimizada({
                codigoEstado: HttpStatus.OK,
                datos: crearNotificacion,
              }),
            );
          }
        }
      } else {
        //crea la notificacion
        crearNotificacion = await this.crearNotificacionService.crearNotificacion(
          {
            entidad: notificacion.entidad.codigo,
            evento: notificacion.evento.codigo,
            accion: notificacion.accion.codigo || null,
            data: notificacion.data,
            estado: codigosEstadosNotificacion.activa,
            status: codigosCatalogoStatusNotificacion.entregada,
            idEntidad: notificacion.idEntidad,
            perfil: null,
            listaPerfiles: notificacion.listaPerfiles.map(function(el) {
              return el._id;
            }),
          },
        );

        for (const listaPerfiles of notificacion.listaPerfiles) {
          let usuario = this.usuariosConectados.find(
            element => element.perfil == listaPerfiles._id,
          );

          if (usuario) {
            usuario.socket.emit(
              event,
              this.funcion.enviarRespuestaOptimizada({
                codigoEstado: HttpStatus.OK,
                datos: crearNotificacion,
              }),
            );
          }
        }
      }

      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: crearNotificacion,
      });
    } catch (e) {
      console.error(e);
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'ERROR_SOLICITUD',
        ),
        // datos: mensaje
      });
    }
  }

  @Post('/notificacionLeida')
  @ApiBearerAuth('Authorization Bearer')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiHeader({ name: 'idPerfil', description: 'Id de perfil' })
  //@ApiParam({ name: 'data', type: ListaMensajeDto, description: 'Lista de objetos de mensajes'})
  @ApiOperation({
    summary:
      'Actualiza la notificacion a leida => http://{host}:{port}/notificacionLeida',
  })
  @ApiResponse({ status: 200, description: 'La notificacion se ha leido.' })
  @SubscribeMessage('notificacionLeida')
  async notificacionLeida(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: listaNotificacionDto,
  ) {
    this.esSesionValida(client);
    const codIdioma = this.funcion.obtenerIdiomaDefecto(
      client.handshake.query.idioma,
    );

    try {
      let actualizaNotificacion;
      for (const getData of data.listaNotificaciones) {
        actualizaNotificacion = await this.actualizarNotificacionService.actualizarNotificacion(
          getData,
        );
      }

      if (actualizaNotificacion) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
        });
      }
    } catch (e) {
      console.error(e);
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'ERROR_SOLICITUD',
        ),
        datos: data,
      });
    }
  }

  // Obtiene los clientes de una conversación
  async obtenerClientesEnConversacion(idConversacion: any) {
    const clientIdList: string[] = await new Promise(resolve => {
      this.server
        .of('/')
        .in(idConversacion)
        .clients((err, clients: string[]) => {
          resolve(clients);
        });
    });

    return clientIdList;
  }

  // Elimina registro de una lista
  removeClient(arr, item) {
    //const i = arr.indexOf(item);
    const i = arr
      .map(function(e) {
        return e.clienteId;
      })
      .indexOf(item.clienteId);

    if (i !== -1) {
      arr.splice(i, 1);
    }
  }

  esSesionValida(client: Socket) {
    const idPerfil = client.handshake.query.idPerfil;
    const apiKey = client.handshake.query.apiKey;
    const token = client.handshake.query.Authorization.substring(7);

    if (mongoose.isValidObjectId(idPerfil) && apiKey && token) {
      if (this.authService.validateApiKey(apiKey)) {
        try {
          this.jwtService.verify(token);
        } catch (e) {
          client.disconnect();
        }
      } else {
        client.disconnect();
      }
    } else {
      client.disconnect();
    }
  }

  async notificacionComentario(
    event,
    notificacion: notificacionDto,
    codIdioma,
  ) {
    //Obtiene la entidad notificacion
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.notificacion,
    );
    let codEntidad = entidad.codigo;

    //Obtiene el estado activa de la notificacion
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    let codEstadoActNotificacion = estado.codigo;

    const getComentario = await this.crearComentarioService.comentar(
      notificacion.data,
      codIdioma,
    );
    const getPerfilProyecto = await this.obtenerPerfilProyectoService.obtenerPropietarioProyecto(
      notificacion.data.proyecto._id,
    );

    let arrayPerfiles = [];
    arrayPerfiles.push(getPerfilProyecto);
    let objNotificacion: any = {
      entidad: notificacion.entidad.codigo,
      evento: notificacion.evento.codigo,
      data: getComentario,
      accion: notificacion.accion.codigo || null,
      estado: codigosEstadosNotificacion.activa,
      status: codigosCatalogoStatusNotificacion.entregada,
      idEntidad: notificacion.idEntidad,
      perfil: notificacion.data.coautor.coautor._id,
      listaPerfiles: arrayPerfiles,
    };
    //crea la notificacion
    let crearNotificacion = await this.crearNotificacionService.crearNotificacion(
      objNotificacion,
    );

    for (const listaPerfiles of crearNotificacion.listaPerfiles) {
      let usuario = this.usuariosConectados.find(
        element => element.perfil == listaPerfiles._id,
      );

      if (usuario) {
        usuario.socket.emit(
          event,
          this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            // mensaje: getComentario,
            datos: crearNotificacion,
          }),
        );
      }
    }
    return crearNotificacion;
  }
}

// Interfaz para respuesta
interface GatewayEventInterface<T> {
  event: string;
  data: T;
}
