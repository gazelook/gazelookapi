import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from 'src/auth/auth.service';
import { ArchivoServicesModule } from 'src/entidades/archivo/casos_de_uso/archivo-services.module';
import { asociacionProviders } from 'src/entidades/asociaciones/drivers/asociacion.provider';
import { ObtenerParticipanteAsociacionTipoService } from 'src/entidades/asociacion_participante/casos_de_uso/obtener-participante-asociacion-tipo.service';
import { ParticipanteAsociacionServicesModule } from 'src/entidades/asociacion_participante/casos_de_uso/participante-asociacion-services.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { ComentariosServicesModule } from 'src/entidades/comentarios/casos_de_uso/comentarios-services.module';
import { HistoricoServiceModule } from 'src/entidades/historico/casos_de_uso/historico-services.module';
import { NoticiaServicesModule } from 'src/entidades/noticia/casos_de_uso/noticia-services.module';
import { CrearNotificacionService } from 'src/entidades/notificacion/casos_de_uso/crear-notificacion.service';
import { NotificacionServicesModule } from 'src/entidades/notificacion/casos_de_uso/notificacion-services.module';
import { notificacionProviders } from 'src/entidades/notificacion/drivers/notificacion.provider';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { ConfigService } from '../../../config/config.service';
import { TraduccionEstaticaController } from '../../../multiIdioma/controladores/traduccion-estatica-controller';
import { expiracionToken } from '../../../shared/enum-sistema';
import { ProyectosServicesModule } from '../../proyectos/casos_de_uso/proyectos-services.module';
import { MensajeTransferirProyectoService } from '../casos_de_uso/confirmar-transferir-proyecto.service';
import { EliminarMensajesService } from '../casos_de_uso/eliminar-mensajes.service';
import { GuardarMensajeService } from '../casos_de_uso/guarda-mensaje.service';
import { ObtenerConversacionService } from '../casos_de_uso/obtener-conversacion.service';
import { ObtenerMensajesNoLeidosService } from '../casos_de_uso/obtener-mensajes-no-leidos.service';
import { DBModule } from './../../../drivers/db_conection/db.module';
import { conversacionProviders } from './../../../entidades/conversacion/drivers/conversacion.provider';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    HistoricoServiceModule,
    ArchivoServicesModule,
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => ParticipanteAsociacionServicesModule),
    forwardRef(() => NotificacionServicesModule),
    forwardRef(() => NoticiaServicesModule),
    forwardRef(() => PerfilServiceModule),
    ProyectosServicesModule,
    ComentariosServicesModule,

    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get<string>('JWT_SECRET'),
          signOptions: {
            expiresIn: expiracionToken.segundos,
          },
        };
      },
    }),
  ],
  providers: [
    ...asociacionProviders,
    ...conversacionProviders,
    ...notificacionProviders,
    GuardarMensajeService,
    EliminarMensajesService,
    ObtenerParticipanteAsociacionTipoService,
    ObtenerMensajesNoLeidosService,
    ObtenerConversacionService,
    MensajeTransferirProyectoService,
    AuthService,
    //ChatGateway,
    TraduccionEstaticaController,
    CrearNotificacionService,
  ],
  controllers: [
    //ChatGateway
  ],
})
export class ChatModule {}
