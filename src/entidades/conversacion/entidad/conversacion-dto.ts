import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import {
  PerfilIdDto,
  PerfilNoticiaDto,
  PerfilPropietarioMensajeDto,
} from '../../usuario/dtos/perfil.dto';
import {
  ParticipanteAsociacionIdDto,
  propietarioDto,
  propietarioMensjeDto,
} from './../../asociacion_participante/entidad/participante-dto.service';
import { TipoAsociacionDto } from './tipo-asociacion.dto';
import { CatalogoMensaje } from './transferencia-confirmar.dto';

export class AsociacionDto {
  @ApiProperty()
  @IsNotEmpty()
  tipo: TipoAsociacionDto;

  @ApiProperty()
  @IsNotEmpty()
  nombre: String;
  @ApiProperty()
  @IsOptional()
  participantes: Array<PerfilNoticiaDto>;
}

export class AsociacionIdDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
}

export class CatalogoIdiomasDto {
  @ApiProperty()
  codNombre: string;
}

export class traduccionMensajeDto {
  @ApiProperty()
  contenido: String;

  @ApiProperty()
  idioma: CatalogoIdiomasDto;
}

export class ConversacionRespuestaDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: String;

  @ApiProperty()
  @IsNotEmpty()
  traducciones: traduccionMensajeDto;

  @ApiProperty()
  @IsNotEmpty()
  importante: Boolean;

  @ApiProperty()
  @IsNotEmpty()
  adjuntos: String;

  @ApiProperty()
  @IsNotEmpty()
  estatus: String;

  @ApiProperty()
  @IsNotEmpty()
  propietario: propietarioMensjeDto;

  @ApiProperty()
  @IsNotEmpty()
  fechaCreacion: String;
}

export class MensajesDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: String;

  @ApiProperty()
  @IsNotEmpty()
  propietario: ParticipanteAsociacionIdDto;
}

export class MensajesEliminaraDto {
  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilIdDto;

  @ApiProperty()
  @IsNotEmpty()
  listaMensaje: Array<MensajesDto>;
}

export class mensajeNL {
  @ApiProperty()
  @IsNotEmpty()
  _id: String;

  @ApiProperty()
  @IsNotEmpty()
  traducciones: traduccionMensajeDto;

  @ApiProperty()
  @IsNotEmpty()
  importante: Boolean;

  @ApiProperty()
  @IsOptional()
  adjuntos: String;

  @ApiProperty()
  @IsNotEmpty()
  propietario: ParticipanteAsociacionIdDto;

  @ApiProperty()
  @IsNotEmpty()
  conversacion: AsociacionIdDto;
}

export class MensajeEnvioDto {
  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilPropietarioMensajeDto;

  @ApiProperty()
  @IsNotEmpty()
  mensaje: mensajeNL;
}

export class MensajesNoLeidosDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;

  @ApiProperty()
  @IsNotEmpty()
  asociacion: string;

  @ApiProperty()
  @IsNotEmpty()
  fechaActualizacion: string;

  @ApiProperty()
  @IsNotEmpty()
  ultimoMensaje: MensajeEnvioDto;
}

export class ConversacionDto {
  @ApiProperty()
  _id: string;

  @ApiProperty()
  asociacion: AsociacionIdDto;
}

export class MensajeEnvioSocketDto {
  @ApiProperty()
  estatus: CatalogoMensaje;

  @ApiProperty()
  traducciones: Array<traduccionMensajeDto>;

  @ApiProperty()
  tipo: CatalogoMensaje;

  @ApiProperty()
  entidadCompartida: CatalogoMensaje;

  @ApiProperty()
  referenciaEntidadCompartida: String;

  @ApiProperty()
  adjuntos: Array<string>;

  @ApiProperty()
  propietario: propietarioDto;

  @ApiProperty()
  listaIdConversacion: Array<string>;

  @ApiProperty()
  identificadorTemporal: Number;
}

export class ListaMensajeDto {
  @ApiProperty()
  listaMensaje: Array<MensajeEnvioSocketDto>;
}

export class MensajeEliminarSocketDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;

  @ApiProperty()
  @IsNotEmpty()
  propietario: propietarioMensjeDto;

  @ApiProperty()
  @IsNotEmpty()
  conversacion: ConversacionDto;
}

export class ListaMensajeEliminarDto {
  @ApiProperty()
  @IsNotEmpty()
  listaMensaje: Array<MensajeEliminarSocketDto>;
}

export class MensajeLeidoPorSocketDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;

  @ApiProperty()
  @IsNotEmpty()
  conversacion: AsociacionIdDto;
}

export class MensajeTransferenciaDto {
  @ApiProperty()
  @IsNotEmpty()
  conversacion: AsociacionIdDto;

  @ApiProperty({ description: 'id del mensaje' })
  @IsNotEmpty()
  _id: string;

  @ApiProperty({ description: 'aceptar o rechazar' })
  @IsNotEmpty()
  estatus: CatalogoMensaje;
}

export class MensajeIdDto {
  @ApiProperty({ description: 'id del mensaje' })
  @IsNotEmpty()
  _id: string;
}
