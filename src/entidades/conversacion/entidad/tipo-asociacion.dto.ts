import { ApiProperty } from '@nestjs/swagger';

export class TipoAsociacionDto {
  @ApiProperty()
  codigo: string;
}
