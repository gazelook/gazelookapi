import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty } from 'class-validator';

export class CatalogoMensaje {
  @ApiProperty()
  codigo: string;
}

export class TrasferenciaProyectoConfirmarDto {
  @ApiProperty({ required: true, description: 'Id del mensaje' })
  @IsNotEmpty()
  @IsMongoId()
  _id: string;

  @ApiProperty({
    required: true,
    type: CatalogoMensaje,
    description: 'Estado de aceptado o rechazado',
    example: '',
  })
  @IsNotEmpty()
  status: CatalogoMensaje;
}
