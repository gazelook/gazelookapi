import { Module } from '@nestjs/common';
import { ParticipanteAsociacionControllerModule } from './controladores/participante-asociacion-controller.module';

@Module({
  imports: [ParticipanteAsociacionControllerModule],
  providers: [],
  controllers: [],
  exports: [],
})
export class ParticipanteAsociacionModule {}
