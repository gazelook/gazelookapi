import { ApiProperty } from '@nestjs/swagger';
import { String } from 'aws-sdk/clients/acm';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { TipoAsociacionDto } from 'src/entidades/conversacion/entidad/tipo-asociacion.dto';
import {
  PerfilContactoDto,
  PerfilIdDto,
  PerfilNoticiaDto,
  PerfilPropietarioDto,
  PerfilPropietarioMensajeDto,
} from '../../usuario/dtos/perfil.dto';
import {
  AsociacionIdDto,
  AsociacionNombreDto,
} from './../../asociaciones/entidad/asociacion-dto';

export class ParticipanteAsociacionDto {
  @ApiProperty()
  @IsOptional()
  estado: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  perfil: PerfilNoticiaDto;

  @ApiProperty({ required: true })
  @IsOptional()
  contactoDe: PerfilNoticiaDto;

  @ApiProperty()
  @IsOptional()
  invitadoPor: any;

  @ApiProperty()
  @IsOptional()
  asociacion: AsociacionIdDto;
}

export class ParticipanteAsociacionIdDto {
  @ApiProperty()
  _id: String;
}

export class ContactoDto {
  @ApiProperty()
  _id: String;

  @ApiProperty({ required: true })
  perfil: PerfilContactoDto;

  @ApiProperty()
  asociacion: AsociacionNombreDto;

  @ApiProperty()
  fechaCreacion: String;
}

export class propietarioDto {
  @ApiProperty()
  _id: String;

  @ApiProperty()
  perfil: PerfilPropietarioDto;
}

export class participanteIdPerfilDto {
  @ApiProperty()
  perfil: PerfilNoticiaDto;
}

export class propietarioMensjeDto {
  @ApiProperty()
  _id: String;

  @ApiProperty()
  perfil: PerfilPropietarioMensajeDto;
}

export class cambioEstadoParticipanteDto {
  @ApiProperty()
  @IsNotEmpty()
  estado: TipoAsociacionDto;

  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilIdDto;

  @ApiProperty()
  @IsNotEmpty()
  asociacion: AsociacionIdDto;
}

export class listaSolicitudesDto {
  @ApiProperty()
  _id: String;

  @ApiProperty()
  asociacion: AsociacionNombreDto;

  @ApiProperty()
  fechaCreacon: Date;

  @ApiProperty()
  InvitadoPor: propietarioMensjeDto;
}

export class asociacionEntrePerfilesDto {
  @ApiProperty()
  _id: String;

  @ApiProperty()
  estado: String;

  @ApiProperty()
  perfil: PerfilNoticiaDto;

  @ApiProperty()
  asociacion: AsociacionIdDto;
}
