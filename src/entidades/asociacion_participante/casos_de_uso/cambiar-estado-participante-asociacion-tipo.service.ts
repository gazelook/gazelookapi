import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';
import {
  estadosPartAso,
  nombreAcciones,
  nombreEntidades,
  nombrecatalogoEstados,
  codigosEstadosPartAsociacion,
  codigoEstadosAsociacion,
} from '../../../shared/enum-sistema';
import { Inject } from '@nestjs/common';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { ActualizarAsociacionService } from '../../asociaciones/casos_de_uso/actualizar-asociacion.service';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Funcion } from 'src/shared/funcion';
import { FirebaseNotificacionService } from '../../../drivers/firebase/services/firebase-notificacion.service';
import { Asociacion } from '../../../drivers/mongoose/interfaces/asociacion/asociacion.interface';
import { GetFotoPerfilService } from '../../perfil/casos_de_uso/obtener-album-perfil-general.service';
import { FBNotificacion } from '../../../drivers/firebase/models/fb-notificacion.interface';

import {
  codigoEntidades,
  estadoNotificacionesFirebase,
  accionNotificacionFirebase,
} from '../../../shared/enum-sistema';

const mongoose = require('mongoose');
@Injectable()
export class CambiarEstadoParticipanteAsociacionTipoService {
  constructor(
    @Inject('ASOCIACION_MODEL')
    private readonly asociacionModel: Model<Asociacion>,
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEstadoService: CatalogoEstadoService,
    private actualizarAsociacionService: ActualizarAsociacionService,
    private firebaseNotificacionService: FirebaseNotificacionService,
    private getFotoPerfilService: GetFotoPerfilService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );

  async cambiarEstadoParticipanteAsociacionTipo(estadoParAsoDTO: any) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const catalogoAccionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const catalogoEntidadAsociacion = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.participanteAsociacion,
      );

      const catalogoEstadoEntidadAsocicion = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.asociacion,
        nombrecatalogoEstados.eliminado,
      );

      let codEstadoPar = estadoParAsoDTO.estado.codigo;

      const estadoPartAsoContacto = await this.participanteAsoModel.updateMany(
        { asociacion: estadoParAsoDTO.asociacion._id },
        { $set: { estado: codEstadoPar } },
        opts,
      );

      console.log('estadoPartAsoContacto: ', estadoPartAsoContacto)

      if (estadoPartAsoContacto.ok == 1) {
        let newHistoriupdate: any = {
          datos: codEstadoPar,
          usuario: estadoParAsoDTO.asociacion._id,
          accion: catalogoAccionModificar.codigo,
          entidad: catalogoEntidadAsociacion.codigo,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistoriupdate);
      }

      // Actualizar el estado de la asociacion segun el estado del participante asociacion
      if (
        codEstadoPar === codigosEstadosPartAsociacion.rechazada ||
        codEstadoPar === codigosEstadosPartAsociacion.cancelada ||
        codEstadoPar === codigosEstadosPartAsociacion.eliminado
      ) {
        await this.actualizarAsociacionService.actualizarEstadoAsociacion(
          estadoParAsoDTO.asociacion._id,
          codigoEstadosAsociacion.eliminado,
          estadoParAsoDTO.perfil._id,
          opts,
        );
      } else {
        await this.actualizarAsociacionService.actualizarEstadoAsociacion(
          estadoParAsoDTO.asociacion._id,
          codigoEstadosAsociacion.activa,
          estadoParAsoDTO.perfil._id,
          opts,
        );
      }

      // Enviar Notificacion de contacto
      if (codEstadoPar === codigosEstadosPartAsociacion.contacto) {
        await this.notificarPerfilAceptacionSolicitud(
          estadoParAsoDTO.perfil._id,
          estadoParAsoDTO.asociacion._id,
          opts,
        );
      }

      // eliminar la notificacion de solicitud cuando el usuario rechaza la solicitud
      if (codEstadoPar === codigosEstadosPartAsociacion.rechazada) {
        await this.firebaseNotificacionService.eliminarNotificacion(
          codigoEntidades.entidadPerfiles,
          estadoParAsoDTO.perfil._id.toString(),
          codigoEntidades.entidadAsociacion,
          estadoParAsoDTO.asociacion._id.toString(),
        );
      }

      // eliminar la notificacion de solicitud cuando el usuario cancela la solicitud
      if (codEstadoPar === codigosEstadosPartAsociacion.cancelada) {
        const getAsociacion = await this.asociacionModel
          .findById(estadoParAsoDTO.asociacion._id)
          .populate({
            path: 'participantes',
            select: 'perfil estado',
            populate: {
              path: 'perfil',
              select: 'nombreContacto',
            },
          })
          .select('estado tipo participantes')
          .session(opts.session);
        const perfilCancelar = await getAsociacion.participantes.filter(
          data =>
            data['perfil']._id.toString() !=
            estadoParAsoDTO.perfil._id.toString(),
        );
        console.log('perfilCancelar', perfilCancelar);
        await this.firebaseNotificacionService.eliminarNotificacion(
          codigoEntidades.entidadPerfiles,
          perfilCancelar[0]['perfil']._id.toString(),
          codigoEntidades.entidadAsociacion,
          estadoParAsoDTO.asociacion._id.toString(),
        );
      }

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return true;
    } catch (e) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw e;
    }
  }

  async notificarPerfilAceptacionSolicitud(
    perfilAceptaSolicitud: string,
    idAsociacion: string,
    opts,
  ) {
    let formatData,
      listParticipantes = [];
    let dataAlbum = [];
    const getAsociacion = await this.asociacionModel
      .findById(idAsociacion)
      .populate({
        path: 'participantes',
        select: 'perfil estado',
        populate: {
          path: 'perfil',
          select: 'nombreContacto',
        },
      })
      .select('estado tipo participantes')
      .session(opts.session);

    console.log('getAsociacion.participantes', getAsociacion.participantes);

    //______________crear la conversacion_____________
    let participantesConversacion = [];
    let dataConversacion = {
      id: getAsociacion._id.toString(),
      asociacion: {
        id: getAsociacion._id.toString(),
        participantes: [],
      },
      ultimoMensaje: {
        id: '',
      },
    };

    formatData = {
      id: getAsociacion._id.toString(),
      estado: {
        codigo: getAsociacion.estado,
      },
      tipo: {
        codigo: getAsociacion.tipo,
      },
    };

    let idPerfilNotificar;

    for (const participante of getAsociacion.participantes) {
      //____________data conversacion___________
      const dataParticipanteConversacion = {
        id: participante['_id'].toString(),
        perfil: {
          _id: participante['perfil']._id.toString(),
          nombreContacto: participante['perfil'].nombreContacto,
        },
      };
      participantesConversacion.push(dataParticipanteConversacion);
      //---------------------------------------------------

      if (
        participante['perfil']._id.toString() ===
        perfilAceptaSolicitud.toString()
      ) {
        const getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
          perfilAceptaSolicitud.toString(),
        );
        dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);

        const dataParticipante = {
          id: participante['_id'].toString(),
          estado: {
            codigo: participante['estado'],
          },
          perfil: {
            _id: participante['perfil']._id.toString(),
            nombreContacto: participante['perfil'].nombreContacto,
            album: dataAlbum,
          },
        };
        listParticipantes.push(dataParticipante);
      } else {
        idPerfilNotificar = participante['perfil']._id.toString();
      }
    }
    listParticipantes.length > 0
      ? (formatData.participantes = listParticipantes)
      : false;

    //_______________Notificar Aceptacion de solicitud___________________________

    const dataNotificarParticipante: FBNotificacion = {
      idEntidad: getAsociacion._id,
      leido: false,
      codEntidad: codigoEntidades.entidadAsociacion,
      data: formatData,
      estado: estadoNotificacionesFirebase.activa,
      accion: accionNotificacionFirebase.ver,
      query: `${codigoEntidades.entidadAsociacion}-${false}`,
    };
    this.firebaseNotificacionService.createDataNotificacion(
      dataNotificarParticipante,
      codigoEntidades.entidadPerfiles,
      idPerfilNotificar,
    );

    // ______________________crear data de conversacion________________________
    dataConversacion.asociacion.participantes = participantesConversacion;
    this.firebaseNotificacionService.createDataConversacion(
      dataConversacion,
      dataConversacion.id,
    );
  }

  verificarEstado(filtro) {
    switch (filtro) {
      case estadosPartAso.contacto:
        return true;
      default:
        return false;
    }
  }
}
