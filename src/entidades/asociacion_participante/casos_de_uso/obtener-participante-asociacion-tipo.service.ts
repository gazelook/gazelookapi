import { Inject, Injectable } from '@nestjs/common';
import { Model, PaginateModel } from 'mongoose';
import { Asociacion } from 'src/drivers/mongoose/interfaces/asociacion/asociacion.interface';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { GetFotoPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil-general.service';
import {
  codigoEstadosAsociacion,
  codigosEstadosPartAsociacion,
  codigosTipoAsociacion,
  estadosPartAso,
  estadosPerfil,
  filtroBusqueda,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';

@Injectable()
export class ObtenerParticipanteAsociacionTipoService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: PaginateModel<Perfil>,
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsociacionModel: PaginateModel<Participante>,
    @Inject('ASOCIACION_MODEL')
    private readonly asociacionModel: Model<Asociacion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private getFotoPerfilService: GetFotoPerfilService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreEstado: CatalogoEstadoService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );

  filtroBus = filtroBusqueda;
  async obtenerParticipanteAsociacionTipo(
    perfil,
    filtro,
    filtroOrden,
    limite,
    pagina,
  ) {
    try {
      // estado asociacion
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.asociacion,
      );
      let codEntidadAso = entidad.codigo;

      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAso,
      );
      let codEstadoAso = estado.codigo;

      let options: any;

      let codEstadoPar = filtro;

      const filtroSel = this.seleccionarFiltro(filtroOrden);

      // let sortBusqueda
      const sortBusqueda = {};

      if (filtroSel === 1) {
        console.log('orden por fecha**************');
        let returnData = await this.obtenerParticipanteAsociacionTipoOrdenFecha(
          perfil,
          limite,
          pagina,
          codEstadoPar,
          codEstadoAso,
        );
        return returnData;
      }
      if (filtroSel === 2) {
        console.log('orden alfabetico**************');
        options = {
          lean: true,
          collation: { locale: 'en' },
          sort: { nombreContacto: 1 },
          select: 'nombre nombreContacto nombreContactoTraducido',
          page: Number(pagina),
          limit: Number(limite),
        };
      }

      const populatePerfil = {
        path: 'perfil',
        select: 'nombre nombreContacto nombreContactoTraducido',
        match: {
          estado: estadosPerfil.activa,
        },
      };

      const getParticAsociacion = await this.participanteAsociacionModel
        .find({
          perfil: perfil,
          estado: codEstadoPar,
        })
        .select('contactoDe invitadoPor fechaCreacion asociacion -_id')
        .populate([
          {
            path: 'asociacion',
            select: 'nombre tipo estado',
            match: {
              tipo: codigosTipoAsociacion.contacto,
              estado: codigoEstadosAsociacion.activa,
            },
          },
          {
            path: 'contactoDe',
            select: 'nombre nombreContacto nombreContactoTraducido',
            match: {
              estado: estadosPerfil.activa,
            },
          },
          {
            path: 'invitadoPor',
            select: 'perfil',
            populate: [populatePerfil],
          },
        ])
        .sort('-fechaCreacion');

      let arrayPerfil = [];
      console.log(
        'getParticAsociacion.length------->: ',
        getParticAsociacion.length,
      );
      if (getParticAsociacion.length > 0) {
        for (const getPart of getParticAsociacion) {
          if (
            getPart.asociacion &&
            getPart.asociacion['estado'] === codEstadoAso
          ) {
            if (codEstadoPar === estadosPartAso.enviada) {
              //invita
              if (getPart.invitadoPor['perfil']) {
                arrayPerfil.push(getPart.invitadoPor['perfil']._id);
              }
            }
            if (codEstadoPar === estadosPartAso.contacto) {
              //contactoDe
              console.log('perfillllllllllllll: ', getPart.contactoDe['_id']);
              if (getPart.contactoDe) {
                arrayPerfil.push(getPart.contactoDe['_id']);
              }
            }
          }
        }
      }

      console.log('ARRAY: ', arrayPerfil);
      const getAsociacion = await this.perfilModel.paginate(
        {
          $or: [
            {
              $or: [
                {
                  $and: [
                    { estado: estadosPerfil.activa },
                    { _id: { $in: arrayPerfil } },
                  ],
                },
              ],
            },
          ],
        },
        options,
      );

      let dataPerfiles = [];

      console.log('getAsociacion.docs.length: ', getAsociacion.docs.length);

      if (getAsociacion.docs.length > 0) {
        for (const parAsociacion of getAsociacion.docs) {
          
          //Data para mateo de la asociacion
          let dataAlbum = [];
          let dataPerfil = {};
          let objPerfil = {};

          let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
            parAsociacion._id,
          );
          dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);

          let arrayAsociaciones = [];
          // dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil)

          if (codEstadoPar === estadosPartAso.enviada) {
            //invita
            const getAsociacion = await this.participanteAsociacionModel
              .find({
                perfil: perfil,
                estado: codEstadoPar,
              })
              .populate({
                path: 'invitadoPor',
                select: 'perfil',
                populate: [
                  {
                    path: 'perfil',
                    select: 'nombre nombreContacto nombreContactoTraducido',
                    match: {
                      estado: estadosPerfil.activa,
                      _id: parAsociacion._id,
                    },
                  },
                ],
              });
              console.log('parAsociacion._id: ', parAsociacion._id);
              console.log('getAsociacion.length: ', getAsociacion.length);
            if (getAsociacion.length > 0) {
              for (const asoc of getAsociacion) {
                
                if (asoc.invitadoPor['perfil']) {
                  console.log('asoc.invitadoPor[perfil] ', asoc.invitadoPor['perfil'])
                  let objAsociacion = {
                    _id: asoc.asociacion,
                  };

                  dataPerfil = {
                    _id: parAsociacion._id,
                    nombre: parAsociacion.nombre,
                    nombreContacto: parAsociacion.nombreContacto,
                    nombreContactoTraducido:
                      parAsociacion.nombreContactoTraducido,
                    album: dataAlbum,
                  };
                  //Data para el mapeo del participante asociacion
                  objPerfil = {
                    _id: asoc._id,
                    estado: {
                      codigo: asoc.estado,
                    },
                    //perfil: dataPerfil,
                    asociacion: objAsociacion,
                    contactoDe: dataPerfil,
                    fechaCreacion: asoc.fechaCreacion,
                  };
                  dataPerfiles.push(objPerfil);
                }
              }
            }
          }
          if (codEstadoPar === estadosPartAso.contacto) {
            //contactoDe
            const getAsociacion = await this.participanteAsociacionModel.findOne(
              {
                perfil: perfil,
                contactoDe: parAsociacion._id,
                estado: codEstadoPar,
              },
            );

            if (getAsociacion) {
              console.log('PERFIL::::::::: ', parAsociacion._id);
              let objAsociacion = {
                _id: getAsociacion.asociacion,
              };

              dataPerfil = {
                _id: parAsociacion._id,
                nombre: parAsociacion.nombre,
                nombreContacto: parAsociacion.nombreContacto,
                nombreContactoTraducido: parAsociacion.nombreContactoTraducido,
                album: dataAlbum,
              };
              //Data para el mapeo del participante asociacion
              objPerfil = {
                _id: getAsociacion._id,
                estado: {
                  codigo: getAsociacion.estado,
                },
                //perfil: dataPerfil,
                asociacion: objAsociacion,
                contactoDe: dataPerfil,
                fechaCreacion: getAsociacion.fechaCreacion,
              };
              dataPerfiles.push(objPerfil);
            }
          }
        }
      }
      let data: any = {
        totalDocs: getAsociacion.totalDocs,
        totalPages: getAsociacion.totalPages,
        hasNextPage: getAsociacion.hasNextPage,
        hasPrevPage: getAsociacion.hasPrevPage,
        docs: dataPerfiles,
      };

      return data;
    } catch (error) {
      throw error;
    }
  }

  async obtenerPerfilesDeAsociacion(idConversacion) {
    try {
      let lista = await this.participanteAsociacionModel
        .find({ asociacion: idConversacion })
        .populate({
          path: 'perfil',
          select: 'nombre nombreContacto nombreContactoTraducido album',
          populate: { path: 'album' },
        })
        .select('perfil');

      for (let item of lista) {
        let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
          item.perfil['_id'],
        );
        item.perfil['album'] = getFotoPerfil.objAlbumTipoPerfil;
      }

      return lista;
    } catch (error) {
      throw error;
    }
  }

  async obtenerPerfilesDeAsociacionSimple(idConversacion) {
    try {
      let lista = await this.participanteAsociacionModel
        .find({ asociacion: idConversacion })
        .populate({
          path: 'perfil',
          select: 'nombre nombreContacto nombreContactoTraducido album',
          populate: { path: 'album' },
        })
        .select('perfil')
        .lean();

      return lista;
    } catch (error) {
      throw error;
    }
  }

  async obtenerAsociacion(idConversacion) {
    try {
      let asociacion: any = await this.asociacionModel
        .findById(idConversacion)
        .populate({
          path: 'participantes',
          match: {
            estado: { $ne: codigosEstadosPartAsociacion.eliminado },
          },
          select: '-__v',
          populate: {
            path: 'perfil',
            select: 'nombre nombreContacto nombreContactoTraducido album',
            match: {
              estado: estadosPerfil.activa,
            },
            populate: { path: 'album' },
          },
        })
        .select('-__v')
        .lean();

      asociacion.estado = {
        codigo: asociacion.estado,
      };

      asociacion.tipo = {
        codigo: asociacion.tipo,
      };

      let lista = asociacion.participantes;

      console.log('lista: ', lista.length);
      for (let item of lista) {
        if (item['perfil']) {
          console.log('PERFIL ID', item['perfil']['_id']);
          let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
            item['perfil']['_id'],
          );
          item['perfil']['album'] = [getFotoPerfil.objAlbumTipoPerfil];
        }
      }

      console.log('acaaaaaaaaaaaaaaaaaaaaaaaaaaaaa....');
      return asociacion;
    } catch (error) {
      throw error;
    }
  }

  async obtenerParticipanteAsociacionTipoOrdenFecha(
    perfil,
    limite,
    pagina,
    codEstadoPar,
    codEstadoAso,
  ) {
    const populatePerfil = {
      path: 'perfil',
      select: 'nombre nombreContacto nombreContactoTraducido',
      match: {
        estado: estadosPerfil.activa,
      },
    };
    let filtroPopulate: any;

    if (codEstadoPar === estadosPartAso.enviada) {
      filtroPopulate = {
        path: 'invitadoPor',
        select: 'perfil',
        populate: [populatePerfil],
      };
    }

    if (codEstadoPar === estadosPartAso.contacto) {
      filtroPopulate = {
        path: 'contactoDe',
        select: 'nombre nombreContacto nombreContactoTraducido',
        match: {
          estado: estadosPerfil.activa,
        },
      };
    }

    const asociacion = {
      path: 'asociacion',
      select: 'nombre tipo estado',
      match: {
        tipo: codigosTipoAsociacion.contacto,
      },
    };

    const options = {
      //lean: true,
      sort: { fechaCreacion: -1 },
      select: '-fechaActualizacion -__v -roles -configuraciones -perfil',
      populate: [asociacion, filtroPopulate],
      page: Number(pagina),
      limit: Number(limite),
    };

    const getParticAsociacion = await this.participanteAsociacionModel
      .find({
        perfil: perfil,
        estado: codEstadoPar,
      })
      .select('contactoDe invitadoPor asociacion')
      .populate({
        path: 'asociacion',
        select: 'nombre tipo estado',
        match: {
          tipo: codigosTipoAsociacion.contacto,
          estado: codigoEstadosAsociacion.activa,
        },
      });

    let arrayContactoDe = [];

    if (getParticAsociacion.length > 0) {
      for (const asoc of getParticAsociacion) {
        if (asoc.asociacion) {
          arrayContactoDe.push(asoc._id);
        }
      }
    }

    const busqueda = await this.participanteAsociacionModel.paginate(
      {
        $or: [
          {
            $or: [
              {
                $and: [{ _id: { $in: arrayContactoDe } }],
              },
            ],
          },
        ],
      },
      options,
    );

    let dataPerfiles = [];

    for (let index = 0; index < busqueda.docs.length; index++) {
      delete busqueda.docs[index].id;

      if (
        busqueda.docs[index].asociacion &&
        busqueda.docs[index].asociacion['estado'] === codEstadoAso
      ) {
        //Data para mateo de la asociacion
        let dataAsociacion = {};
        let dataAlbum = [];
        let participanteAsociacion = {};
        let dataPerfil = {};
        let objPerfil = {};

        if (codEstadoPar === estadosPartAso.enviada) {
          //invita
          let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
            busqueda.docs[index].invitadoPor['perfil']._id,
          );
          dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);
          //Data para mateo de la asociacion
          dataAsociacion = {
            _id: busqueda.docs[index].asociacion['_id'],
            nombre: busqueda.docs[index].asociacion['nombre'],
            tipo: {
              codigo: busqueda.docs[index].asociacion['tipo'],
            },
          };
          //Data para mateo de perfil
          dataPerfil = {
            _id: busqueda.docs[index].invitadoPor['perfil']._id,
            nombre: busqueda.docs[index].invitadoPor['perfil'].nombre,
            nombreContacto:
              busqueda.docs[index].invitadoPor['perfil'].nombreContacto,
            nombreContactoTraducido:
              busqueda.docs[index].invitadoPor['perfil']
                .nombreContactoTraducido,
            album: dataAlbum,
          };
          participanteAsociacion = {
            _id: busqueda.docs[index].invitadoPor['_id'],
            perfil: dataPerfil,
          };
          //Data para el mapeo del participante asociacion
          objPerfil = {
            _id: busqueda.docs[index]._id,
            estado: {
              codigo: busqueda.docs[index].estado,
            },
            //perfil: dataPerfil,
            asociacion: dataAsociacion,
            invitadoPor: participanteAsociacion,
            fechaCreacion: busqueda.docs[index].fechaCreacion,
          };
          dataPerfiles.push(objPerfil);
        }
        if (codEstadoPar === estadosPartAso.contacto) {
          //contactoDe
          if (busqueda.docs[index].contactoDe) {
            let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
              busqueda.docs[index].contactoDe['_id'],
            );
            dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);
            //Data para mateo de la asociacion
            dataAsociacion = {
              _id: busqueda.docs[index].asociacion['_id'],
              nombre: busqueda.docs[index].asociacion['nombre'],
              tipo: {
                codigo: busqueda.docs[index].asociacion['tipo'],
              },
            };
            //Data para mateo de perfil
            dataPerfil = {
              _id: busqueda.docs[index].contactoDe['_id'],
              nombre: busqueda.docs[index].contactoDe['nombre'],
              nombreContacto: busqueda.docs[index].contactoDe['nombreContacto'],
              nombreContactoTraducido:
                busqueda.docs[index].contactoDe['nombreContactoTraducido'],
              album: dataAlbum,
            };
            //Data para el mapeo del participante asociacion
            objPerfil = {
              _id: busqueda.docs[index]._id,
              estado: {
                codigo: busqueda.docs[index].estado,
              },
              //perfil: dataPerfil,
              asociacion: dataAsociacion,
              contactoDe: dataPerfil,
              fechaCreacion: busqueda.docs[index].fechaCreacion,
            };
            dataPerfiles.push(objPerfil);
          }
        }
      }
    }

    let data: any = {
      totalDocs: busqueda.totalDocs,
      totalPages: busqueda.totalPages,
      hasNextPage: busqueda.hasNextPage,
      hasPrevPage: busqueda.hasPrevPage,
      docs: dataPerfiles,
    };

    return data;
  }

  seleccionarFiltro(filtro) {
    switch (filtro) {
      case this.filtroBus.FECHA:
        return 1;
      default:
        return 2;
    }
  }
}
