import { Inject } from '@nestjs/common';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Funcion } from 'src/shared/funcion';

@Injectable()
export class ObtenerTodosParticipantePerfilService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );

  async obtenerTodosParticipantePerfil(perfil: any): Promise<any> {
    try {
      const catalogoEstadoEntidadPartAsocContacto = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.participanteAsociacion,
        nombrecatalogoEstados.contacto,
      );

      return await this.participanteAsoModel.find({
        estado: catalogoEstadoEntidadPartAsocContacto.catalogoEstado.codigo,
        perfil: perfil,
      });
    } catch (error) {
      throw error;
    }
  }

  async obtenerParticipantesPorPerfil(perfil: any): Promise<any> {
    try {
      return await this.participanteAsoModel.find({ perfil: perfil });
    } catch (error) {
      throw error;
    }
  }
}
