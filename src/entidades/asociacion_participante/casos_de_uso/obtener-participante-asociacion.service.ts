import { Inject } from '@nestjs/common';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';

@Injectable()
export class ObtenerParticipanteAsociacionService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
  ) {}

  async obtenerParticipanteAsociacion(
    data: any,
    opts?: any,
  ): Promise<Participante> {
    let session: any;
    let options: any;
    if (!opts) {
      //Inicia la transaccion
      //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
      session = await mongoose.startSession();
      await session.startTransaction();
    }
    try {
      if (!opts) {
        //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
        options = { session };
      } else {
        options = opts;
      }
      const getPartAsoc = await this.participanteAsoModel
        .findOne({ asociacion: data.conversacion._id, perfil: data.perfil._id })
        .session(options.session);
      if (!opts) {
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      return getPartAsoc;
    } catch (e) {
      if (!opts) {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      throw e;
    }
  }

  async ObtenerParticipanteAsociacionPerfil(
    asociacion: string,
    perfil: string,
  ): Promise<any> {
    try {
      const participante = await this.participanteAsoModel.findOne({
        $and: [{ asociacion: asociacion }, { perfil: perfil }],
      });
      return participante;
    } catch (error) {
      throw error;
    }
  }
}
