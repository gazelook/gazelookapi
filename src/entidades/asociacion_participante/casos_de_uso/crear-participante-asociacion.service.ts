import { ParticipanteAsociacionDto } from './../entidad/participante-dto.service';
import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from './../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from './../../catalogos/casos_de_uso/catalogo-entidad.service';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { nombreAcciones, nombreEntidades } from 'src/shared/enum-sistema';
import * as mongoose from 'mongoose';

@Injectable()
export class CrearParticipanteAsociacionService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
    private crearHistoricoService: CrearHistoricoService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
  ) {}

  async crearParticipanteAsociacion(
    crearPartAsoDto: ParticipanteAsociacionDto,
    opts?: any,
  ): Promise<any> {
    let session: any;
    let options: any;
    if (!opts) {
      //Inicia la transaccion
      //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
      session = await mongoose.startSession();
      await session.startTransaction();
    }
    try {
      if (!opts) {
        //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
        options = { session };
      } else {
        options = opts;
      }

      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      const entidadPa = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteAsociacion,
      );
      let codEntidadPaAso = entidadPa.codigo;

      const crear = await new this.participanteAsoModel(crearPartAsoDto).save(
        options,
      );
      const dataPaAso = JSON.parse(JSON.stringify(crear));
      delete dataPaAso.__v;

      let newHistoricoCrearPartiAso: any = {
        datos: dataPaAso,
        usuario: crear.perfil,
        accion: getAccion,
        entidad: codEntidadPaAso,
      };

      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoCrearPartiAso,
      );

      if (!opts) {
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      return true;
    } catch (error) {
      if (!opts) {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      throw error;
    }
  }
}
