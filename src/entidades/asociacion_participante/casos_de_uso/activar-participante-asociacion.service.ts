import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';
import { Inject } from '@nestjs/common';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  codigoEstadosAsociacion,
  codigosEstadosPartAsociacion,
  codigosTipoAsociacion,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { ActualizarConversacionService } from '../../conversacion/casos_de_uso/actualizar-conversacion.service';
import { ActualizarAsociacionService } from '../../asociaciones/casos_de_uso/actualizar-asociacion.service';
@Injectable()
export class ActivarParticipanteAsociacionService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
    private crearHistoricoService: CrearHistoricoService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
    private actualizarConversacionService: ActualizarConversacionService,
    private actualizarAsociacionService: ActualizarAsociacionService,
  ) {}
  // activar participante
  async activarParticipante(idPerfil: any, opts): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccion = accion.codigo;

      // estado participante asociacion
      const entidadPa = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteAsociacion,
      );
      let codEntidadPaAso = entidadPa.codigo;

      const estadoPa = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.contacto,
        codEntidadPaAso,
      );

      // estado asociacion
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.asociacion,
      );
      let codEntidadAso = entidad.codigo;

      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAso,
      );
      let codEstadoAso = estado.codigo;

      // estado conversacion
      const entidadConv = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.conversacion,
      );
      let codEntidadConv = entidadConv.codigo;

      const estadoConv = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadConv,
      );
      let codEstadoConv = estadoConv.codigo;

      const getParticipanteAsoc = await this.participanteAsoModel
        .find({
          $and: [{ perfil: idPerfil }],
        })
        .populate([
          {
            path: 'asociacion',
          },
        ]);

      if (getParticipanteAsoc.length === 0) {
        return null;
      }

      console.log('getParticipanteAsoc', getParticipanteAsoc);

      for (const participante of getParticipanteAsoc) {
        console.log('participante', participante);
        if (
          (participante.estado === codigosEstadosPartAsociacion.contacto ||
            participante.estado === codigosEstadosPartAsociacion.enviada) &&
          participante.asociacion['tipo'] === codigosTipoAsociacion.contacto &&
            participante.asociacion['estado'] ===
              codigoEstadosAsociacion.hibernado
        ) {
          // activar conversacion
          await this.actualizarConversacionService.actualizarEstadoConversacion(
            participante.asociacion['_id'],
            codEstadoConv,
            opts,
          );
          // activar asociacion
          const asociacionUpdated = await this.actualizarAsociacionService.actualizarEstadoAsociacion(
            participante.asociacion['_id'],
            codEstadoAso,
            idPerfil,
            opts,
          );

          // registrar en el historico
          const dataHistorico = JSON.parse(JSON.stringify(asociacionUpdated));
          //eliminar parametro no necesarios
          delete dataHistorico.__v;
          // guardar historico
          let newHistoriupdate: any = {
            datos: dataHistorico,
            usuario: idPerfil,
            accion: getAccion,
            entidad: codEntidadPaAso,
          };
          this.crearHistoricoService.crearHistoricoServer(newHistoriupdate);
        }
      }
      return true;
    } catch (error) {
      throw error;
    }
  }
}
