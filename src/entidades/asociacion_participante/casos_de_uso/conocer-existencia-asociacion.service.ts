import { Inject } from '@nestjs/common';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
@Injectable()
export class ConocerExistenciaAsociacionService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
  ) {}

  async conocerExistenciaAsociacion(
    participantesAso: any,
  ): Promise<Participante> {
    try {
      const consulta = await this.participanteAsoModel.findOne({
        $or: [
          {
            $and: [
              { contactoDe: participantesAso[0] },
              { perfil: participantesAso[1] },
            ],
          },
          {
            $and: [
              { contactoDe: participantesAso[1] },
              { perfil: participantesAso[0] },
            ],
          },
        ],
      });

      return consulta;
    } catch (error) {
      throw error;
    }
  }
}
