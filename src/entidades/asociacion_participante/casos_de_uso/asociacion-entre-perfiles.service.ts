import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import {
  codigoEstadosAsociacion,
  estadosPartAso,
} from '../../../shared/enum-sistema';

@Injectable()
export class AsociacionEntrePerfilesService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
  ) {}
  async asociacionEntrePerfiles(
    perfUsua: any,
    perfUsuaConsu: any,
  ): Promise<any> {
    try {
      const idPerf = [];
      idPerf.push(perfUsua);
      idPerf.push(perfUsuaConsu);

      let result: any;

      const idPart = await this.participanteAsoModel
        .find({ perfil: perfUsua })
        .populate({ path: 'perfil', select: '_id' })
        .populate({
          path: 'asociacion',
          select: '_id',
          match: {
            // tipo: codigosTipoAsociacion.contacto,
            estado: codigoEstadosAsociacion.activa,
          },
        })
        .populate({ path: 'invitadoPor', select: '_id perfil' })
        .populate({ path: 'contactoDe', select: '_id nombre nombreContacto' })
        .select('estado perfil asociacion');

      const idPart2 = await this.participanteAsoModel
        .find({ perfil: perfUsuaConsu })
        .populate({ path: 'perfil', select: '_id' })
        .populate({
          path: 'asociacion',
          select: '_id',
          match: {
            // tipo: codigosTipoAsociacion.contacto,
            estado: codigoEstadosAsociacion.activa,
          },
        })
        .populate({ path: 'invitadoPor', select: '_id perfil' })
        .populate({ path: 'contactoDe', select: '_id nombre nombreContacto' })
        .select('estado perfil asociacion');

      const array3 = idPart.concat(idPart2);

      // for (let i = 0; i < idPart.length; i++) {
      //     let a = idPart[i].asociacion;
      //     for (let j = 0; j < idPart2.length; j++) {

      //         let b  = idPart2[j].asociacion;
      //         console.log(a.toString(), b.toString());
      //         console.log(typeof(a), typeof(b));
      //         if (a.toString() === b.toString()) {
      //             console.log('----------------- son iguales ------------');
      //             console.log('arrayEn3i', idPart[i]);
      //             console.log('arrayEn3j', idPart2[j]);

      //         } else {

      //         }
      //     }
      // }

      for (let iterator of idPart) {
        for (let iterator2 of idPart2) {
          if (iterator.asociacion && iterator2.asociacion) {
            if (
              iterator.asociacion.toString() === iterator2.asociacion.toString()
            ) {
              console.log('iterator2', iterator2);
              console.log('iterator1', iterator);
              let particpante1 = null;
              let particpante2 = null;
              if (
                iterator.estado === estadosPartAso.contacto ||
                iterator.estado === estadosPartAso.enviada ||
                iterator2.estado === estadosPartAso.contacto ||
                iterator2.estado === estadosPartAso.enviada
              ) {
                // participante1
                particpante1 = JSON.parse(JSON.stringify(iterator));
                delete particpante1.estado;
                particpante1.estado = {
                  codigo: iterator.estado,
                };

                // participante2
                particpante2 = JSON.parse(JSON.stringify(iterator2));
                delete particpante2.estado;
                particpante2.estado = {
                  codigo: iterator2.estado,
                };
              }
              return (result = particpante2);
            }
          }
        }
      }
      // const busqueda = await this.participanteAsoModel.find({ perfil: { $in: idPerf } });

      // const busqued2a = await this.participanteAsoModel.aggregate([
      //     {
      //         $lookup:
      //         {
      //             from: 'participante_asociacion',
      //             localField: 'asociacion',
      //             foreignField: perfUsua,
      //             as: 'orderdetails'
      //         }
      //     }
      // ])
      // .populate({
      //     path: 'invitadoPor',
      //     select: '_id perfil ',
      //     populate:{
      //         path: 'perfil',
      //         select: '_id nombre '
      //     }
      // })
      // .populate({
      //     path: 'asociacion',
      //     select:'nombre'
      // }).select('-fechaActualizacion -estado -__v');
      return result;
    } catch (error) {
      throw error;
    }
  }
}
