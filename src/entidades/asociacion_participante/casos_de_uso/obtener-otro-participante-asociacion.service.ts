import { Inject } from '@nestjs/common';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

@Injectable()
export class ObtenerOtroParticipanteAsociacionService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
  ) {}
  async ObtenerOtroParticipanteAsociacion(
    idConversacion: any,
    idPropietario: any,
  ): Promise<any> {
    try {
      const busqueda = await this.participanteAsoModel
        .findOne({
          $and: [
            { asociacion: idConversacion },
            { _id: { $ne: idPropietario } },
          ],
        })
        .populate({ path: 'perfil' });
      // .populate({
      //     path: 'invitadoPor',
      //     select: '_id perfil ',
      //     populate:{
      //         path: 'perfil',
      //         select: '_id nombre '
      //     }
      // })
      // .populate({
      //     path: 'asociacion',
      //     select:'nombre'
      // }).select('-fechaActualizacion -estado -__v');ç

      return busqueda;
    } catch (error) {
      return null;
    }
  }
}
