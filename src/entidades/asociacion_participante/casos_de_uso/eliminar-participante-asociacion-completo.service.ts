import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import {
  codigoEstadosAsociacion,
  codigosEstadosConversacion,
  codigosEstadosPartAsociacion,
} from '../../../shared/enum-sistema';
import { ActualizarAsociacionService } from '../../asociaciones/casos_de_uso/actualizar-asociacion.service';
import { ActualizarConversacionService } from '../../conversacion/casos_de_uso/actualizar-conversacion.service';
import { EliminarMensajesCompletoService } from '../../conversacion/casos_de_uso/eliminar-mensajes-completo.service';
import { ObtenerParticipanteAsociacionService } from './obtener-participante-asociacion.service';

@Injectable()
export class EliminarParticipanteAsociacionCompletoService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
    private actualizarAsociacionService: ActualizarAsociacionService,
    private obtenerParticipanteAsociacionService: ObtenerParticipanteAsociacionService,
    private actualizarConversacionService: ActualizarConversacionService,
    private eliminarMensajesCompletoService: EliminarMensajesCompletoService,
  ) {}
  // elimina logicamente
  async eliminarParticipanteAsocCompleto(
    idPerfil: string,
    opts?: any,
  ): Promise<any> {
    try {
      // obtener el participante (asociacion)
      const getParticipanteAsoc = await this.participanteAsoModel.find({
        perfil: idPerfil,
      });
      for (const participante of getParticipanteAsoc) {
        console.log('participante: ', participante._id);
        const otroParticipante = await this.obtenerParticipanteAsociacionService.ObtenerParticipanteAsociacionPerfil(
          participante.asociacion.toString(),
          participante.contactoDe.toString(),
        );
        //verificar si el otro usuario esta en estado eliminado, se pone la conversacion en estado eliminado
        // console.log('otroParticipante: ', otroParticipante)

        if (
          otroParticipante?.estado === codigosEstadosPartAsociacion.eliminado
        ) {
          // actualizar estado conversacion
          await this.actualizarConversacionService.actualizarEstadoConversacion(
            participante.asociacion.toString(),
            codigosEstadosConversacion.eliminado,
            opts,
          );
        }
        if (participante.estado === codigosEstadosPartAsociacion.contacto) {
          // actualizar el estado de la asociacion
          await this.actualizarAsociacionService.actualizarEstadoAsociacion(
            participante.asociacion.toString(),
            codigoEstadosAsociacion.eliminado,
            idPerfil,
            opts,
          );

          // eliminado logico del usuario participante
          const dataActualizarPar = {
            estado: codigosEstadosPartAsociacion.eliminado,
            perfil: null,
            contactoDe: null,
            invitadoPor: null,
            fechaActualizacion: new Date(),
          };

          const parAso = await this.participanteAsoModel.findByIdAndUpdate(
            participante._id,
            dataActualizarPar,
            opts,
          );

          // eliminar mensajes del usuario
          await this.eliminarMensajesCompletoService.EliminarMensajesCompleto(
            participante._id,
            participante.asociacion.toString(),
            opts,
          );
        }

        // si tiene invitaciones pendientes o a enviado una invitacion
        if (
          participante.estado === codigosEstadosPartAsociacion.enviada ||
          participante.estado === codigosEstadosPartAsociacion.aceptada
        ) {
          // actualizar el estado de la asociacion
          await this.actualizarAsociacionService.actualizarEstadoAsociacion(
            participante.asociacion.toString(),
            codigoEstadosAsociacion.eliminado,
            idPerfil,
            opts,
          );

          // eliminado logico del usuario participante
          const dataActualizarPar = {
            estado: codigosEstadosPartAsociacion.eliminado,
            perfil: null,
            contactoDe: null,
            invitadoPor: null,
            fechaActualizacion: new Date(),
          };

          const parAso = await this.participanteAsoModel.findByIdAndUpdate(
            participante._id,
            dataActualizarPar,
            opts,
          );
        }
      }
    } catch (error) {
      throw error;
    }
  }
}
