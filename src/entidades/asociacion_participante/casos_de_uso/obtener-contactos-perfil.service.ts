import { Inject, Injectable, Res } from '@nestjs/common';
import { Response } from 'express';
import { Model, PaginateModel } from 'mongoose';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { GetFotoPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil-general.service';
import {
  codigosEstadosPartAsociacion,
  filtroBusqueda,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';

const mongoose = require('mongoose');
@Injectable()
export class ObtenerContactosPerfilService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: PaginateModel<Participante>,
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreEstado: CatalogoEstadoService,
    private getFotoPerfilService: GetFotoPerfilService,
  ) {}
  filtroBus = filtroBusqueda;
  async obtenerContactosPerfil(
    perfil,
    filtroOrder,
    limite,
    pagina,
    servicioPeticion?: any,
    @Res() response?: Response,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();

    try {
      //Obtiene la entidad perfiles
      const entidadPerfiles = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.perfiles,
      );
      let codEntidadPerfiles = entidadPerfiles.codigo;

      //Obtiene el estado activa de la entidad perfiles
      const estadoPerfiles = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadPerfiles,
      );
      let codEstadoPerfiles = estadoPerfiles.codigo;

      // estado asociacion
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.asociacion,
      );
      let codEntidadAso = entidad.codigo;

      const estado = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAso,
      );
      let codEstadoAso = estado.codigo;

      const perfilOwner = await this.perfilModel.findOne({
        _id: perfil,
        estado: codEstadoPerfiles,
      });
      if (!perfilOwner) {
        return null;
      }

      const entidadPa = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteAsociacion,
      );
      let codEntidadPaAso = entidadPa.codigo;

      const estadoPaContacto = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.contacto,
        codEntidadPaAso,
      );

      const filtroSel = this.seleccionarFiltro(filtroOrder);
      // let sortBusqueda
      const sortBusqueda = {};

      if (filtroSel === 1) {
        sortBusqueda['fechaCreacion'] = -1;
      }
      if (filtroSel === 2) {
        sortBusqueda['perfil'] = -1;
      }
      // const invitadoPor = ({
      //     path: 'invitadoPor',
      //     select: '_id perfil ',
      //     populate:{
      //         path: 'perfil',
      //         select: '_id nombre '
      //     }
      // });
      const asociacion = {
        path: 'asociacion',
        select: 'nombre tipo estado',
        /* match: {
                    estado: codEstadoAso
                } */
      };

      const populatePerfil = {
        path: 'perfil',
        select: 'nombre nombreContacto tipoPerfil album',
        match: {
          estado: codEstadoPerfiles,
        },
      };
      const populateInvitadoPor = {
        path: 'invitadoPor',
        select: '_id estado perfil',
        populate: [populatePerfil],
      };

      let getBusqueda: any = { docs: [] };
      if (servicioPeticion.estado === codigosEstadosPartAsociacion.enviada) {
        const optionsInvitacion = {
          lean: true,
          sort: sortBusqueda,
          select:
            '-fechaActualizacion -__v -roles -configuraciones -contactoDe',
          populate: [asociacion, populatePerfil, populateInvitadoPor],
          page: Number(pagina),
          limit: Number(limite),
        };

        // busqueda de los contactos que tiene un determinado perfil
        getBusqueda = await this.participanteAsoModel.paginate(
          { estado: servicioPeticion.estado, perfil: perfil },
          optionsInvitacion,
        );
      } else {
        const options = {
          lean: true,
          sort: sortBusqueda,
          select:
            '-fechaActualizacion -__v -roles -configuraciones -contactoDe -invitadoPor',
          populate: [asociacion, populatePerfil],
          page: Number(pagina),
          limit: Number(limite),
        };

        //getBusqueda = await this.participanteAsoModel.paginate({ estado: codEstadoPaContacto, contactoDe: perfil }, options);
        getBusqueda.docs = await this.obtenerContactosPerfilAleatorios({
          idPerfil: perfil,
        });
      }

      console.log('getBusqueda', getBusqueda);

      if (getBusqueda.docs?.length > 0) {
        let dataPerfiles = [];

        for (const getPartiAsociacion of getBusqueda.docs) {
          console.log(
            'getPartiAsociacion.asociacion',
            getPartiAsociacion.asociacion,
          );

          if (getPartiAsociacion.asociacion) {
            if (
              getPartiAsociacion.perfil &&
              getPartiAsociacion.asociacion['estado'] === codEstadoAso
            ) {
              // delete getBusqueda.docs[index].id

              //for (let ind = 0; ind < getBusqueda.docs[index].perfil.length; ind++) {
              let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
                getPartiAsociacion.perfil['_id'],
              );
              let dataAlbum = [];

              dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);
              //dataAlbum.push(getFotoPerfil.objAlbumTipoGeneral)

              //Data para mateo de perfil
              let dataPerfil = {
                _id: getPartiAsociacion.perfil['_id'],
                nombre: getPartiAsociacion.perfil['nombre'],
                nombreContacto: getPartiAsociacion.perfil['nombreContacto'],
                album: dataAlbum,
              };
              //Data para mateo de la asociacion
              let dataAsociacion = {
                _id: getPartiAsociacion.asociacion['_id'],
                nombre: getPartiAsociacion.asociacion['nombre'],
                tipo: {
                  codigo: getPartiAsociacion.asociacion['tipo'],
                },
              };
              //Data para el mapeo del participante asociacion
              let objPerfil: any = {
                _id: getPartiAsociacion._id,
                estado: {
                  codigo: getPartiAsociacion.estado,
                },
                perfil: dataPerfil,
                asociacion: dataAsociacion,
                fechaCreacion: getPartiAsociacion.fechaCreacion,
              };
              // obtiene los datos de las invitaciones pendientes
              let dataInvitadoPor;
              if (
                servicioPeticion &&
                getPartiAsociacion.estado ===
                  codigosEstadosPartAsociacion.enviada &&
                getPartiAsociacion.invitadoPor.perfil
              ) {
                const getFotoPerfilInv = await this.getFotoPerfilService.getFotoPerfil(
                  getPartiAsociacion.invitadoPor.perfil['_id'],
                );
                const albumInvitado = [];
                albumInvitado.push(getFotoPerfilInv.objAlbumTipoPerfil);
                dataInvitadoPor = {
                  _id: getPartiAsociacion.invitadoPor._id,
                  perfil: {
                    _id: getPartiAsociacion.invitadoPor.perfil['_id'],
                    nombre: getPartiAsociacion.invitadoPor.perfil['nombre'],
                    nombreContacto:
                      getPartiAsociacion.invitadoPor.perfil['nombreContacto'],
                    album: albumInvitado,
                  },
                };
                objPerfil.invitadoPor = dataInvitadoPor;
              }
              dataPerfiles.push(objPerfil);

              if (servicioPeticion === undefined || !servicioPeticion) {
                response.set(
                  headerNombre.totalDatos,
                  getBusqueda.totalDocs.toString(),
                );
                response.set(
                  headerNombre.totalPaginas,
                  getBusqueda.totalPages.toString(),
                );
                response.set(
                  headerNombre.proximaPagina,
                  getBusqueda.hasNextPage.toString(),
                );
                response.set(
                  headerNombre.anteriorPagina,
                  getBusqueda.hasPrevPage.toString(),
                );
              }
            }
          }
        }

        return dataPerfiles;
      } else {
        null;
      }
    } catch (error) {
      throw error;
    }
  }

  seleccionarFiltro(filtro) {
    switch (filtro) {
      case this.filtroBus.FECHA:
        return 1;
      case this.filtroBus.ALFA:
        return 2;
      default:
        return 2;
    }
  }

  async obtenerContactosPerfilAleatorios({
    idPerfil,
    limite = 6,
    estado = codigosEstadosPartAsociacion.contacto.toString(),
  }): Promise<Array<any>> {
    const perfil = mongoose.Types.ObjectId(idPerfil);

    const contactos = await this.participanteAsoModel
      .aggregate([
        {
          $match: {
            estado: estado,
            contactoDe: { $in: [perfil] },
          },
        },
        {
          $project: {
            _id: 1,
            estado: 1,
            perfil: 1,
            asociacion: 1,
            fechaActualizacion: 1,
          },
        },
        {
          $lookup: {
            from: 'perfil',
            localField: 'perfil',
            foreignField: '_id',
            as: 'perfil',
          },
        },
        {
          $unwind: '$perfil',
        },
        {
          $lookup: {
            from: 'asociacion',
            localField: 'asociacion',
            foreignField: '_id',
            as: 'asociacion',
          },
        },
        {
          $unwind: '$asociacion',
        },
      ])
      .sample(limite);

    if (contactos.length > 0) {
      const contactosUnicos = contactos.reduce(
        (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
        [],
      );

      return contactosUnicos;
    }
    return null;
  }
}
