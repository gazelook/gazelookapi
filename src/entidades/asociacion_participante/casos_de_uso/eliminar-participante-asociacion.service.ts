import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';
import {
  codigoEstadosAsociacion,
  codigosEstadosConversacion,
  codigosEstadosPartAsociacion,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
  codigosTipoAsociacion,
} from '../../../shared/enum-sistema';
import { Inject } from '@nestjs/common';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { EliminarAsociacionService } from '../../asociaciones/casos_de_uso/eliminar-asociacion.service';
import { EliminarMensajesService } from '../../conversacion/casos_de_uso/eliminar-mensajes.service';
import { ActualizarConversacionService } from '../../conversacion/casos_de_uso/actualizar-conversacion.service';
import { ActualizarAsociacionService } from '../../asociaciones/casos_de_uso/actualizar-asociacion.service';

@Injectable()
export class EliminarParticipanteAsociacionService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private readonly participanteAsoModel: Model<Participante>,
    private crearHistoricoService: CrearHistoricoService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
    private eliminarAsociacionService: EliminarAsociacionService,
    private eliminarMensajesService: EliminarMensajesService,
    private actualizarConversacionService: ActualizarConversacionService,
    private actualizarAsociacionService: ActualizarAsociacionService,
  ) {}
  // elimina logicamente
  async eliminarParticipante(idPerfil: any, opts): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      let getAccion = accion.codigo;

      const entidadPa = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteAsociacion,
      );
      let codEntidadPaAso = entidadPa.codigo;
      const estadoPa = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadPaAso,
      );
      let codEstadoEliminado = estadoPa.codigo;

      const getParticipanteAsoc = await this.participanteAsoModel
        .find({ perfil: idPerfil })
        .session(opts.session)
        .populate([
          {
            path: 'asociacion',
          },
        ]);
      let updateParticipanteAsociacion;
      for (const participante of getParticipanteAsoc) {
        if (
          (participante.estado === codigosEstadosPartAsociacion.contacto ||
            participante.estado === codigosEstadosPartAsociacion.enviada ||
            participante.estado === codigosEstadosPartAsociacion.aceptada ||
            participante.estado === codigosEstadosPartAsociacion.hibernado) &&
          participante.asociacion['tipo'] === codigosTipoAsociacion.contacto
        ) {
          // eliminar participante
          updateParticipanteAsociacion = await this.participanteAsoModel.findByIdAndUpdate(
            participante._id,
            { estado: codEstadoEliminado },
            opts,
          );
          // eliminar asociacion
          await this.eliminarAsociacionService.eliminarAsociacion(
            participante.asociacion['_id'],
            idPerfil,
            opts,
          );
          // eliminar mensajes
          await this.eliminarMensajesService.eliminarMensajesPerfil(
            participante._id,
            participante.asociacion['_id'],
            opts,
          );

          // registrar en el historico
          const dataHistorico = JSON.parse(
            JSON.stringify(updateParticipanteAsociacion),
          );
          //eliminar parametro no necesarios
          delete dataHistorico.__v;

          let newHistoriupdate: any = {
            datos: dataHistorico,
            usuario: idPerfil,
            accion: getAccion,
            entidad: codEntidadPaAso,
          };
          this.crearHistoricoService.crearHistoricoServer(newHistoriupdate);
        }
      }
      return true;
    } catch (error) {
      throw error;
    }
  }

  async hibernarParticipante(idPerfil: any, opts): Promise<any> {
    try {
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccion = accion.codigo;

      const entidadPa = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteAsociacion,
      );
      let codEntidadPaAso = entidadPa.codigo;
      const estadoPa = await this.nombreEstado.obtenerNombreEstado(
        nombrecatalogoEstados.hibernado,
        codEntidadPaAso,
      );

      const getParticipanteAsoc = await this.participanteAsoModel
        .find({ perfil: idPerfil })
        .session(opts.session)
        .populate([
          {
            path: 'asociacion',
          },
        ]);
      console.log('getParticipanteAsoc', getParticipanteAsoc);

      for (const participante of getParticipanteAsoc) {
        if (
          (participante.estado === codigosEstadosPartAsociacion.contacto ||
            participante.estado === codigosEstadosPartAsociacion.enviada) &&
          participante.asociacion['tipo'] === codigosTipoAsociacion.contacto &&
            participante.asociacion['estado'] === codigoEstadosAsociacion.activa
        ) {
          // hibernar conversacion
          await this.actualizarConversacionService.actualizarEstadoConversacion(
            participante.asociacion['_id'],
            codigosEstadosConversacion.hibernado,
            opts,
          );
          // hibernar asociacion
          const asociacionUpdated = await this.actualizarAsociacionService.actualizarEstadoAsociacion(
            participante.asociacion['_id'],
            codigoEstadosAsociacion.hibernado,
            idPerfil,
            opts,
          );

          // registrar en el historico
          const dataHistorico = JSON.parse(JSON.stringify(asociacionUpdated));
          //eliminar parametro no necesarios
          delete dataHistorico.__v;

          let newHistoriupdate: any = {
            datos: dataHistorico,
            usuario: idPerfil,
            accion: getAccion,
            entidad: codEntidadPaAso,
          };
          this.crearHistoricoService.crearHistoricoServer(newHistoriupdate);
        }
      }
      return true;
    } catch (error) {
      throw error;
    }
  }
}
