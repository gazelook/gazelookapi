import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { nombreAcciones, nombreEntidades } from 'src/shared/enum-sistema';

@Injectable()
export class ModificiarParticipanteAsociacionService {
  constructor(
    @Inject('PARTI_ASOC_MODEL')
    private participanteAsoModel: Model<Participante>,
    private crearHistoricoService: CrearHistoricoService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
  ) {}

  async modificiarParticipanteAsociacion(partAsoDto, opts: any): Promise<any> {
    // //Inicia la transaccion
    // //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    // const session = await mongoose.startSession();
    // await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      // const opts = { session };

      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.asociacion,
      );

      const entidadPa = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.participanteAsociacion,
      );
      let codEntidadPaAso = entidadPa.codigo;

      const userUpdate = await this.participanteAsoModel.findByIdAndUpdate(
        partAsoDto._id,
        partAsoDto,
        opts,
      );

      let newHistoricoCrearPartiAso: any = {
        datos: partAsoDto,
        usuario: partAsoDto._id,
        accion: getAccion,
        entidad: codEntidadPaAso,
      };

      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoCrearPartiAso,
      );

      // //Confirma los cambios de la transaccion
      // await session.commitTransaction();
      // //Finaliza la transaccion
      // await session.endSession();

      return true;
    } catch (error) {
      // //Aborta la transaccion
      // await session.abortTransaction();
      // //Finaliza la transaccion
      // await session.endSession();
      throw error;
    }
  }
}
