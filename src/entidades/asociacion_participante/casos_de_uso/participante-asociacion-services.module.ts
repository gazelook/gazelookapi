import { ModificiarParticipanteAsociacionService } from './modificar-participante-asociacion.service';
import { ConocerExistenciaAsociacionService } from './conocer-existencia-asociacion.service';
import { AsociacionEntrePerfilesService } from './asociacion-entre-perfiles.service';
import { ObtenerContactosPerfilService } from './obtener-contactos-perfil.service';
import { CambiarEstadoParticipanteAsociacionTipoService } from './cambiar-estado-participante-asociacion-tipo.service';
import { ObtenerParticipanteAsociacionTipoService } from './obtener-participante-asociacion-tipo.service';
import { CrearParticipanteAsociacionService } from './crear-participante-asociacion.service';
import { participanteAsociacionProviders } from './../drivers/participante-asociacion.provider';
import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { ObtenerParticipanteAsociacionService } from './obtener-participante-asociacion.service';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { EliminarParticipanteAsociacionService } from './eliminar-participante-asociacion.service';
import { ObtenerTodosParticipantePerfilService } from './obtener-todos-participante-perfil.service';
import { ObtenerOtroParticipanteAsociacionService } from './obtener-otro-participante-asociacion.service';
import { EliminarParticipanteAsociacionCompletoService } from './eliminar-participante-asociacion-completo.service';
import { AsociacionServicesModule } from '../../asociaciones/casos_de_uso/asociacion-services.module';
import { ConversacionServicesModule } from '../../conversacion/casos_de_uso/conversacion-services.module';
import { ActivarParticipanteAsociacionService } from './activar-participante-asociacion.service';
import { Funcion } from 'src/shared/funcion';
import { asociacionProviders } from 'src/entidades/asociaciones/drivers/asociacion.provider';
import { PerfilProviders } from 'src/entidades/perfil/drivers/perfil.provider';
import { FirebaseModule } from '../../../drivers/firebase/firebase.module';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => AsociacionServicesModule),
    forwardRef(() => ConversacionServicesModule),
    FirebaseModule,
  ],
  providers: [
    ...asociacionProviders,
    ...participanteAsociacionProviders,
    ...PerfilProviders,
    CrearParticipanteAsociacionService,
    ObtenerParticipanteAsociacionTipoService,
    CambiarEstadoParticipanteAsociacionTipoService,
    ObtenerContactosPerfilService,
    ObtenerParticipanteAsociacionService,
    EliminarParticipanteAsociacionService,
    ObtenerTodosParticipantePerfilService,
    AsociacionEntrePerfilesService,
    ObtenerOtroParticipanteAsociacionService,
    ConocerExistenciaAsociacionService,
    ModificiarParticipanteAsociacionService,
    EliminarParticipanteAsociacionCompletoService,
    ActivarParticipanteAsociacionService,
    Funcion,
  ],
  exports: [
    CrearParticipanteAsociacionService,
    ObtenerParticipanteAsociacionTipoService,
    CambiarEstadoParticipanteAsociacionTipoService,
    ObtenerContactosPerfilService,
    ObtenerParticipanteAsociacionService,
    EliminarParticipanteAsociacionService,
    ObtenerTodosParticipantePerfilService,
    AsociacionEntrePerfilesService,
    ObtenerOtroParticipanteAsociacionService,
    ConocerExistenciaAsociacionService,
    ModificiarParticipanteAsociacionService,
    EliminarParticipanteAsociacionCompletoService,
    ActivarParticipanteAsociacionService,
  ],
  controllers: [],
})
export class ParticipanteAsociacionServicesModule {}
