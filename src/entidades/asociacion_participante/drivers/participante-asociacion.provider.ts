import { Connection } from 'mongoose';
import { ParticipanteAsociacionModelo } from '../../../drivers/mongoose/modelos/participante_asociacion/participante-asociacion.schema';

export const participanteAsociacionProviders = [
  {
    provide: 'PARTI_ASOC_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'participante_asociacion',
        ParticipanteAsociacionModelo,
        'participante_asociacion',
      ),
    inject: ['DB_CONNECTION'],
  },
];
