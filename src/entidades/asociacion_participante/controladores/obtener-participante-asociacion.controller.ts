import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerParticipanteAsociacionService } from '../casos_de_uso/obtener-participante-asociacion.service';

@ApiTags('Asociacion')
@Controller('api/obtener-participante-asociacion')
export class ObtenerParticipanteAsociacionControlador {
  constructor(
    private readonly obtenerParticipanteAsociacionService: ObtenerParticipanteAsociacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Devuelve la información de los participantes de una asociacion.',
  })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al obtener participantes' })
  @UseGuards(AuthGuard('jwt'))
  public async ObtenerParticipanteAsociacion(
    @Headers() headers,
    @Body() body: any,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const participante = await this.obtenerParticipanteAsociacionService.obtenerParticipanteAsociacion(
        body,
      );

      if (participante) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: participante,
        });
      } else {
        const ERROR_OBTENER = await this.i18n.translate(
          codIdioma.concat('.ERROR_OBTENER'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_OBTENER,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }

  calcularDias(fechaActual, fechaActualizacion) {
    let dias =
      (fechaActual.getTime() - fechaActualizacion.getTime()) /
      (60 * 60 * 24 * 1000);
    if (dias <= 3) {
      return true;
    } else return false;
  }
}
