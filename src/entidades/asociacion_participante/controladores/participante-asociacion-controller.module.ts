import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { ParticipanteAsociacionServicesModule } from './../casos_de_uso/participante-asociacion-services.module';
import { AsociacionEntrePerfilesControlador } from './asociacion-entre-perfiles.controller';
import { CambiarEstadoParticipanteAsociacionTipoControlador } from './cambiar-estado-participante-asociacion-tipo.controller';
import { ObtenerParticipanteAsociacionTipoControlador } from './obtener-participante-asociacion-tipo.controller';

@Module({
  imports: [ParticipanteAsociacionServicesModule],
  providers: [Funcion],
  exports: [],
  controllers: [
    // CrearParticipanteAsociacionControlador,
    ObtenerParticipanteAsociacionTipoControlador,
    CambiarEstadoParticipanteAsociacionTipoControlador,
    // ObtenerParticipanteAsociacionControlador,
    AsociacionEntrePerfilesControlador,
  ],
})
export class ParticipanteAsociacionControllerModule {}
