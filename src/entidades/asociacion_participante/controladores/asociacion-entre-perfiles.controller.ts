import { asociacionEntrePerfilesDto } from './../entidad/participante-dto.service';
import { AsociacionEntrePerfilesService } from './../casos_de_uso/asociacion-entre-perfiles.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiHeader, ApiOperation } from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Get,
  Headers,
  Param,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Asociacion')
@Controller('api/asociacion-entre-perfiles')
export class AsociacionEntrePerfilesControlador {
  constructor(
    private readonly asoEntrePerfilesService: AsociacionEntrePerfilesService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/:perfUsua/:perfUsuaConsu')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Devuelve la informaciuon de una noticia unica.' })
  @ApiResponse({
    status: 200,
    type: asociacionEntrePerfilesDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al obtener noticia' })
  @UseGuards(AuthGuard('jwt'))
  public async AsociacionEntrePerfiles(
    @Headers() headers,
    @Param('perfUsua') perfUsua: any,
    @Param('perfUsuaConsu') perfUsuaConsu: any,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const participante = await this.asoEntrePerfilesService.asociacionEntrePerfiles(
        perfUsua,
        perfUsuaConsu,
      );

      if (participante) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: participante,
        });
      } else {
        const ERROR_OBTENER = await this.i18n.translate(
          codIdioma.concat('.ERROR_OBTENER'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_OBTENER,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
