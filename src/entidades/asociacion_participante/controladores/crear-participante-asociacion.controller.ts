import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CrearParticipanteAsociacionService } from '../casos_de_uso/crear-participante-asociacion.service';
import { ParticipanteAsociacionDto } from './../entidad/participante-dto.service';

@ApiTags('Asociacion')
@Controller('api/participante-asociacion')
export class CrearParticipanteAsociacionControlador {
  constructor(
    private readonly crearParticiAsociacionService: CrearParticipanteAsociacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  //@ApiBody({ type: any })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Crea una nueva asociacion' })
  //@ApiResponse({ status: 201, type: AsociacionDto, description: 'Noticia creado' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 404, description: 'Error al crear la asociacion' })
  @UseGuards(AuthGuard('jwt'))
  public async crearParticipanteAsociacion(
    @Headers() headers,
    @Body() createPartiAsoDTO: ParticipanteAsociacionDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    // if (mongoose.isValidObjectId(createAsoDTO)) {
    try {
      const partAsociacion = await this.crearParticiAsociacionService.crearParticipanteAsociacion(
        createPartiAsoDTO,
      );

      if (partAsociacion) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.CREATED,
          datos: partAsociacion,
        });
      } else {
        const ERROR_CREACION = await this.i18n.translate(
          codIdioma.concat('.ERROR_CREACION'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_CREACION,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
