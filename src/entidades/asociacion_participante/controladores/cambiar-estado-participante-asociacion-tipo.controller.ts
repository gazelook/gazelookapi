import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CambiarEstadoParticipanteAsociacionTipoService } from './../casos_de_uso/cambiar-estado-participante-asociacion-tipo.service';
import { cambioEstadoParticipanteDto } from './../entidad/participante-dto.service';

@ApiTags('Asociacion')
@Controller('api/cambiar-estado-participante-asociacion-tipo')
export class CambiarEstadoParticipanteAsociacionTipoControlador {
  constructor(
    private readonly cambiarEstadoParticiAsociacionService: CambiarEstadoParticipanteAsociacionTipoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Cambiar el estado de una asociacion y sus participantes',
  })
  @ApiResponse({ status: 200, description: 'cambio de estado correcto' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 404, description: 'Parametros no validos' })
  @ApiResponse({ status: 500, description: 'Error en la solicitud' })
  @UseGuards(AuthGuard('jwt'))
  public async cambiarEstadoParticipanteAsociacionTipo(
    @Headers() headers,
    @Body() estadoParAsoDTO: cambioEstadoParticipanteDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    const verEstado = this.funcion.verificarEstadoAsociacion(
      estadoParAsoDTO.estado.codigo,
    );

    if (
      mongoose.isValidObjectId(estadoParAsoDTO.perfil._id) &&
      mongoose.isValidObjectId(estadoParAsoDTO.asociacion._id) &&
      verEstado
    ) {
      try {
        const partAsociacion = await this.cambiarEstadoParticiAsociacionService.cambiarEstadoParticipanteAsociacionTipo(
          estadoParAsoDTO,
        );

        if (partAsociacion) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
