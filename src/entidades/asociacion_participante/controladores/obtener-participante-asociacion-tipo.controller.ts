import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { ObtenerParticipanteAsociacionTipoService } from '../casos_de_uso/obtener-participante-asociacion-tipo.service';
import { estadosPartAso } from './../../../shared/enum-sistema';
import {
  listaSolicitudesDto,
  propietarioDto,
} from './../entidad/participante-dto.service';

@ApiTags('Asociacion')
@Controller('api/obtener-participante-asociacion-tipo')
export class ObtenerParticipanteAsociacionTipoControlador {
  constructor(
    private readonly obtenerParticiAsociacionService: ObtenerParticipanteAsociacionTipoService,
    private readonly i18n: I18nService,
  ) {}

  funcion = new Funcion(this.i18n);

  @Get('/')
  //@ApiBody({ type: any })
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Obtiene una lista de participantes asociacion por tipo aceptada, cancelada, enviada',
  })
  @ApiResponse({ status: 200, type: listaSolicitudesDto, description: 'OK' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al obtener los datos' })
  // @UseGuards(AuthGuard('jwt'))
  public async obtenerParticipanteAsociacionTipo(
    @Headers() headers: any,
    @Query('perfil') perfil: string,
    @Query('filtro') filtro: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Query('filtroOrden') filtroOrden: string,
    @Res() response: Response,
  ) {
    const headerNombre = new HadersInterfaceNombres();

    const verEstado = this.funcion.verificarEstadoAsociacion(filtro);
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(perfil) &&
      verEstado &&
      !isNaN(limite) &&
      !isNaN(pagina) &&
      limite > 0 &&
      pagina > 0
    ) {
      try {
        const partAsociacion = await this.obtenerParticiAsociacionService.obtenerParticipanteAsociacionTipo(
          perfil,
          filtro,
          filtroOrden,
          limite,
          pagina,
        );

        var respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: partAsociacion.docs,
        });

        response.set(headerNombre.totalDatos, partAsociacion.totalDocs);
        response.set(headerNombre.totalPaginas, partAsociacion.totalPages);
        response.set(headerNombre.proximaPagina, partAsociacion.hasNextPage);
        response.set(headerNombre.anteriorPagina, partAsociacion.hasPrevPage);
        response.send(respuesta);

        return respuesta;
      } catch (e) {
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
        response.send(respuesta);
        return respuesta;
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
      response.send(respuesta);
      return respuesta;
    }
  }

  @Get('/perfiles-de-asociacion')
  //@ApiBody({ type: any })
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Obtiene información de los perfiles de cada participante',
  })
  @ApiResponse({
    status: 200,
    type: propietarioDto,
    description: 'OK',
    isArray: true,
  })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al obtener los datos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerPerfilesDeAsociacion(
    @Headers() headers: any,
    @Query('idPerfil') idPerfil: string,
    @Query('idConversacion') idConversacion: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      idPerfil &&
      idConversacion &&
      mongoose.isValidObjectId(idPerfil) &&
      mongoose.isValidObjectId(idConversacion)
    ) {
      try {
        var result = await this.obtenerParticiAsociacionService.obtenerAsociacion(
          idConversacion,
        );

        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: result,
        });
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: await this.funcion.obtenerTraduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        ),
      });
    }
  }

  verificarEstado(filtro) {
    switch (filtro) {
      case estadosPartAso.cancelada:
        return true;
      case estadosPartAso.enviada:
        return true;
      case estadosPartAso.rechazada:
        return true;
      case estadosPartAso.eliminado:
        return true;
      case estadosPartAso.contacto:
        return true;
      default:
        return false;
    }
  }
}
