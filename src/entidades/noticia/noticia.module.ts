import { NoticiaControllerModule } from './controladores/noticia-controller.module';
import { Module } from '@nestjs/common';
@Module({
  imports: [NoticiaControllerModule],
  providers: [],
  controllers: [],
})
export class NoticiaModule {}
