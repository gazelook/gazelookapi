import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { PaginateModel } from 'mongoose';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import {
  filtroBusqueda,
  nombrecatalogoEstados,
  nombreEntidades,
} from './../../../shared/enum-sistema';
import { CalcularDiasNoticiaService } from './carcular-dias-noticia.service';
import { ObtenerPortadaService } from './obtener-portada-predeterminada.service';
import { TraducirNoticiasPerfilService } from './traducir-noticia.service';

@Injectable()
export class ObtenerNoticiasRecientePaginacionService {
  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirNoticiaService: TraducirNoticiasPerfilService,
    private obtenerPortada: ObtenerPortadaService,
    private calcularDiasNoticiaService: CalcularDiasNoticiaService,
  ) {}
  filtroBus = filtroBusqueda;

  async obtenerNoticiasRecientePaginacion(
    codIdim: string,
    limit: number,
    page: number,
    fechaInicial: any,
    fechaFinal: any,
    filtro: any,
    perfil: string,
    response: any,
  ): Promise<any> {
    console.log('limit: ', limit);
    console.log('page: ', page);
    const headerNombre = new HadersInterfaceNombres();

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstadoTNoticia = estado.codigo;

      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN.codigo;
      //Obtiene el estado
      const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticia = estadoN.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      const estadoActAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoActAlbum = estadoActAlbum.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      const filtroSel = this.selecctionarFitlro(filtro);
      const sortBusqueda = {};
      if (filtroSel === 1) {
        sortBusqueda['fechaCreacion'] = -1;
      } else {
        sortBusqueda['totalVotos'] = -1;
      }

      const populateTraduccion = {
        path: 'traducciones',
        select:
          '-fechaActualizacion -fechaCreacion -idioma -__v -descripcion -tags -original',
        match: {
          idioma: codIdioma,
          estado: codEstadoTNoticia,
        },
      };

      const medias = {
        path: 'medias',
        select:
          'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma, estado: codEstadoTradMedia },
          },
          { path: 'principal', select: 'url tipo duracion fechaActualizacion' },
          {
            path: 'miniatura',
            select: 'url tipo duracion fechaActualizacion ',
          },
        ],
      };

      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -estado -nombre ',
        match: { estado: codEstadoActAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select:
              'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fechaActualizacion ',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fechaActualizacion ',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select:
              'principal enlace miniatura  fechaCreacion fechaActualizacion catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma, estado: codEstadoTradMedia },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fechaActualizacion ',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fechaActualizacion ',
              },
            ],
          },
        ],
      };

      const votos = {
        path: 'votos',
        select: 'perfil',
        populate: { path: 'perfil', select: '_id nombre' },
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto',
      };

      const options = {
        lean: true,
        sort: sortBusqueda,
        select: ' -__v -direccion ',
        populate: [populateTraduccion, adjuntos, medias, votos, populatePerfil],
        page: Number(page),
        limit: Number(limit),
      };

      const I = new Date(fechaInicial);
      fechaFinal = fechaFinal + 'T23:59:59.999Z';
      const F = new Date(fechaFinal);

      const getPNoticiasF = await this.noticiaModel.paginate(
        {
          $and: [
            { estado: codEstadoNoticia },
            { fechaCreacion: { $gt: I } },
            { fechaCreacion: { $lt: F } },
          ],
        },
        options,
      );

      // return response.send(noticias)
      console.log('getPNoticiasF.docs.length: ', getPNoticiasF.docs.length);
      if (getPNoticiasF.docs.length > 0) {
        const notiFinal = [];
        for (let noticia of getPNoticiasF.docs) {
          console.log('noticia: ', noticia._id);

          let port: any;
          let actu = false;
          let datos: any;
          let votoBo: any;
          let princip: any;

          if (
            noticia.fechaCreacion.toString() ==
            noticia.fechaActualizacion.toString()
          ) {
            actu = false;
          } else {
            let dias = this.calcularDiasNoticiaService.calcularDias(
              new Date(),
              noticia.fechaActualizacion,
            );
            if (dias) actu = true;
          }

          // llamamos al servicio para obtener una portada prederder si no devueleve una por defecto
          port = await this.obtenerPortada.obtenerPortada(
            noticia.adjuntos,
            codIdim,
            opts,
          );

          if (noticia.votos.length > 0) {
            for (const votos of noticia.votos) {
              if (votos['perfil']) {
                if (votos['perfil']._id == perfil) {
                  votoBo = true;
                  break;
                } else {
                  votoBo = false;
                }
              }
            }
          } else {
            votoBo = false;
          }

          let getTraduccion: any;
          if (noticia.traducciones[0] === undefined) {
            console.log('se debe traducir: ', noticia._id);

            getTraduccion = await this.traducirNoticiaService.traducirNoticia(
              noticia._id,
              codIdim,
              perfil,
              opts,
            );
          } else {
            let trad = [
              {
                tituloCorto: noticia.traducciones[0]['tituloCorto'],
                titulo: noticia.traducciones[0]['titulo'],
                descripcion: noticia.traducciones[0]['descripcion'],
                //tags: noticia.traducciones[0]['tags'],
              },
            ];
            console.log('trad: ', trad);
            getTraduccion = trad;
          }

          datos = {
            _id: noticia._id,
            // totalVotos: noticia.totalVotos,
            traducciones: getTraduccion,
            actualizado: actu,
            adjuntos: port,
            perfil: noticia.perfil,
            voto: votoBo,
            estado: {
              codigo: noticia.estado,
            },
            fechaCreacion: noticia.fechaCreacion,
            fechaActualizacion: noticia.fechaActualizacion,
          };
          notiFinal.push(datos);
        }
        console.log('notiFinal:------- ', notiFinal.length);
        response.set(headerNombre.totalDatos, getPNoticiasF.totalDocs);
        response.set(headerNombre.totalPaginas, getPNoticiasF.totalPages);
        response.set(headerNombre.proximaPagina, getPNoticiasF.hasNextPage);
        response.set(headerNombre.anteriorPagina, getPNoticiasF.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return notiFinal;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return getPNoticiasF.docs;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
  selecctionarFitlro(filtro) {
    switch (filtro) {
      case this.filtroBus.FECHA:
        return 1;
      case this.filtroBus.VOTO:
        return 2;
      default:
        return 1;
    }
  }

  calcularDias(fechaActual, fechaActualizacion) {
    if (fechaActualizacion) {
      let dias =
        (fechaActual.getTime() - fechaActualizacion.getTime()) /
        (60 * 60 * 24 * 1000);
      if (dias <= 5) {
        return true;
      } else return false;
    } else {
      return false;
    }
  }
}
