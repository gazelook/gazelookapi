import { Injectable } from '@nestjs/common';
import { ListaArchivosDefaultTipoService } from 'src/entidades/archivo/casos_de_uso/lista-archivos-default-tipo.service';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import {
  codigosArchivosSistema,
  codigosCatalogoAlbum,
} from './../../../shared/enum-sistema';

@Injectable()
export class ObtenerPortadaService {
  constructor(
    private listaArchivosDefaultTipoService: ListaArchivosDefaultTipoService,
    private traduccionMediaService: TraduccionMediaService,
  ) {}

  async obtenerPortada(
    adjuntos: any,
    codIdioma?: string,
    opts?: any,
  ): Promise<any> {
    try {
      let album = [];
      let adjuntoAlbum;
      if (adjuntos.length > 0) {
        adjuntoAlbum = adjuntos.find(element => element.predeterminado);

        let media = [];
        if (adjuntoAlbum) {
          // si el album es predeterminado se busca el else se busca uno por defecto
          if (adjuntoAlbum.predeterminado) {
            console.log('adjuntoAlbum: ', adjuntoAlbum._id);
            console.log('adjuntoAlbum.tipo: ', adjuntoAlbum.tipo);
            console.log('adjuntoAlbum.portada: ', adjuntoAlbum.portada);
            if (
              adjuntoAlbum.tipo === codigosCatalogoAlbum.catAlbumLinks &&
              adjuntoAlbum.portada
            ) {
              console.log('el album es de tipo link y tiene portada');

              let obMiniaturaPortada: any;
              if (adjuntoAlbum.portada.miniatura) {
                console.log('si tiene miniatura');

                obMiniaturaPortada = {
                  _id: adjuntoAlbum.portada.miniatura._id,
                  url: adjuntoAlbum.portada.miniatura.url,
                  tipo: {
                    codigo: adjuntoAlbum.portada.miniatura.tipo,
                  },
                  fileDefault: adjuntoAlbum.portada.miniatura.fileDefault,
                  fechaActualizacion:
                    adjuntoAlbum.portada.miniatura.fechaActualizacion,
                };

                if (adjuntoAlbum.portada.miniatura.duracion) {
                  obMiniaturaPortada.duracion =
                    adjuntoAlbum.portada.miniatura.duracion;
                }
                let objPortada: any = {
                  _id: adjuntoAlbum.portada._id,
                  catalogoMedia: {
                    codigo: adjuntoAlbum.portada.catalogoMedia,
                  },
                  principal: {
                    _id: adjuntoAlbum.portada.principal._id,
                    url: adjuntoAlbum.portada.principal.url,
                    tipo: {
                      codigo: adjuntoAlbum.portada.principal.tipo,
                    },
                    fileDefault: adjuntoAlbum.portada.principal.fileDefault,
                    fechaActualizacion:
                      adjuntoAlbum.portada.principal.fechaActualizacion,
                  },
                  miniatura: obMiniaturaPortada,
                  fechaCreacion: adjuntoAlbum.portada.fechaCreacion,
                  fechaActualizacion: adjuntoAlbum.portada.fechaActualizacion,
                };
                if (adjuntoAlbum.portada.principal.duracion) {
                  objPortada.principal.duracion =
                    adjuntoAlbum.portada.principal.duracion;
                }
                //traducir descripcion de la media
                if (adjuntoAlbum.portada.traducciones[0] === undefined) {
                  console.log('debe traducir');

                  const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                    adjuntoAlbum.portada._id,
                    codIdioma,
                    opts,
                  );
                  if (traduccionMedia) {
                    const traducciones = [
                      {
                        _id: traduccionMedia._id,
                        descripcion: traduccionMedia.descripcion,
                      },
                    ];

                    objPortada.traducciones = traducciones;
                  } else {
                    objPortada.traducciones = [];
                  }
                } else {
                  let arrayTradPortada = [];
                  arrayTradPortada.push(adjuntoAlbum.portada.traducciones[0]);
                  objPortada.traducciones =
                    adjuntoAlbum.portada.traducciones[0];
                }

                album.push({
                  _id: adjuntoAlbum._id,
                  tipo: {
                    codigo: adjuntoAlbum.tipo,
                  },
                  traducciones: adjuntoAlbum.traducciones,
                  predeterminado: adjuntoAlbum.predeterminado,
                  portada: objPortada,
                });
              } else {
                // obtener portada defecto
                const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
                  codigosArchivosSistema.archivo_default_proyectos,
                );
                album.push({
                  _id: adjuntoAlbum._id,
                  tipo: {
                    codigo: adjuntoAlbum.tipo,
                  },
                  predeterminado: adjuntoAlbum.predeterminado,
                  portada: portadaAlbum,
                  media: media,
                });
              }
            } else if (adjuntoAlbum.portada) {
              console.log('si tiene portada');
              let obMiniaturaPortada: any;
              if (adjuntoAlbum.portada.miniatura) {
                obMiniaturaPortada = {
                  _id: adjuntoAlbum.portada.miniatura._id,
                  url: adjuntoAlbum.portada.miniatura.url,
                  tipo: {
                    codigo: adjuntoAlbum.portada.miniatura.tipo,
                  },
                  fileDefault: adjuntoAlbum.portada.miniatura.fileDefault,
                  fechaActualizacion:
                    adjuntoAlbum.portada.miniatura.fechaActualizacion,
                };

                if (adjuntoAlbum.portada.miniatura.duracion) {
                  obMiniaturaPortada.duracion =
                    adjuntoAlbum.portada.miniatura.duracion;
                }
              }

              let objPortada: any = {
                _id: adjuntoAlbum.portada._id,
                catalogoMedia: {
                  codigo: adjuntoAlbum.portada.catalogoMedia,
                },
                principal: {
                  _id: adjuntoAlbum.portada.principal._id,
                  url: adjuntoAlbum.portada.principal.url,
                  tipo: {
                    codigo: adjuntoAlbum.portada.principal.tipo,
                  },
                  fileDefault: adjuntoAlbum.portada.principal.fileDefault,
                  fechaActualizacion:
                    adjuntoAlbum.portada.principal.fechaActualizacion,
                },
                miniatura: obMiniaturaPortada,
                fechaCreacion: adjuntoAlbum.portada.fechaCreacion,
                fechaActualizacion: adjuntoAlbum.portada.fechaActualizacion,
              };
              if (adjuntoAlbum.portada.principal.duracion) {
                objPortada.principal.duracion =
                  adjuntoAlbum.portada.principal.duracion;
              }
              //traducir descripcion de la media
              if (adjuntoAlbum.portada.traducciones[0] === undefined) {
                console.log('debe traducir');

                const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                  adjuntoAlbum.portada._id,
                  codIdioma,
                  opts,
                );
                if (traduccionMedia) {
                  const traducciones = [
                    {
                      _id: traduccionMedia._id,
                      descripcion: traduccionMedia.descripcion,
                    },
                  ];

                  objPortada.traducciones = traducciones;
                } else {
                  objPortada.traducciones = [];
                }
              } else {
                let arrayTradPortada = [];
                arrayTradPortada.push(adjuntoAlbum.portada.traducciones[0]);
                objPortada.traducciones = adjuntoAlbum.portada.traducciones[0];
              }

              album.push({
                _id: adjuntoAlbum._id,
                tipo: {
                  codigo: adjuntoAlbum.tipo,
                },
                traducciones: adjuntoAlbum.traducciones,
                predeterminado: adjuntoAlbum.predeterminado,
                portada: objPortada,
              });
            } else {
              // obtener portada defecto
              const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
                codigosArchivosSistema.archivo_default_proyectos,
              );
              album.push({
                _id: adjuntoAlbum._id,
                tipo: {
                  codigo: adjuntoAlbum.tipo,
                },
                predeterminado: adjuntoAlbum.predeterminado,
                portada: portadaAlbum,
                media: media,
              });
            }
          }
        } else {
          // obneter portada defecto
          const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
            codigosArchivosSistema.archivo_default_proyectos,
          );
          album.push({
            portada: portadaAlbum,
            predeterminado: true,
            tipo: { codigo: codigosCatalogoAlbum.catAlbumGeneral },
          });
        }
      } else {
        // obneter portada defecto
        const portadaAlbum = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
          codigosArchivosSistema.archivo_default_proyectos,
        );
        album.push({
          portada: portadaAlbum,
          predeterminado: true,
          tipo: { codigo: codigosCatalogoAlbum.catAlbumGeneral },
        });
      }

      return album;
    } catch (error) {
      throw error;
    }
  }
}
