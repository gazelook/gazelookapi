import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearDireccionService } from 'src/entidades/direccion/casos_de_uso/crear-direccion.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { CrearDireccionDto } from '../../direccion/dtos/crear-direccion.dto';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from './../../../shared/enum-sistema';
import { CrearAlbumService } from './../../album/casos_de_uso/crear-album.service';
import { ActualizarMediaService } from './../../media/casos_de_uso/actualizar-media.service';
import { ObtenerPerfilUsuarioService } from './../../perfil/casos_de_uso/obtener-perfil-usuario.service';
import { NoticiaDto } from './../entidad/noticia-dto';
import { TraducirNoticiaSegundoPlanoService } from './traducir-noticia-segundo-plano.service';

@Injectable()
export class CrearNoticiaService {
  constructor(
    @Inject('NOTICIA_MODEL') private readonly noticiaModel: Model<Noticia>,
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: Model<TraduccionesNoticias>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private crearAlbumService: CrearAlbumService,
    private actualizarMediaService: ActualizarMediaService,
    private crearDireccionService: CrearDireccionService,
    private traducirNoticiaSegundoPlanoService: TraducirNoticiaSegundoPlanoService,
  ) {}

  async crearNoticia(noticiaDto: NoticiaDto, codIdiom: string): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad
      const entidadTN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidadTN = entidadTN.codigo;

      //Obtiene el estado
      const estadoTN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTN,
      );
      let codEstadoTN = estadoTN.codigo;

      let textoTraducido = await traducirTexto(
        codIdiom,
        noticiaDto.traducciones[0].titulo,
      );
      let idiomaDetectado = textoTraducido.idiomaDetectado;

      //Obtiene el idioma por el codigo
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idiomaDetectado,
      );
      let codIdioma = idioma.codigo;

      //Objeto de traduccion noticia
      const idNoticia = new mongoose.mongo.ObjectId();

      let newTraduccionNoticia = {
        tituloCorto: noticiaDto.traducciones[0].tituloCorto,
        titulo: noticiaDto.traducciones[0].titulo || null,
        descripcion: noticiaDto.traducciones[0].descripcion || null,
        idioma: codIdioma,
        original: true,
        estado: codEstadoTN,
        referencia: idNoticia,
      };

      const crearTraduccionNoticia = await new this.traduccionNoticiaModel(
        newTraduccionNoticia,
      ).save(opts);
      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccionNoticia));
      delete dataTraduccion.__v;

      let newHistoricoTraduccionNoticia: any = {
        datos: dataTraduccion,
        usuario: noticiaDto.perfil,
        accion: getAccion,
        entidad: entidad.codigo,
      };

      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionNoticia,
      );

      let codTraduccionNoticia = [];
      codTraduccionNoticia.push(crearTraduccionNoticia._id);

      //Comprueba si llega la direccion
      let traducciones: any;
      if (noticiaDto.direccion.traducciones.length > 0) {
        traducciones = noticiaDto.direccion.traducciones;
      } else {
        traducciones = null;
      }
      //Comprueba si llega la localidad
      let localidad: any;
      if (noticiaDto.direccion.localidad) {
        localidad = noticiaDto.direccion.localidad.codigo;
      } else {
        localidad = null;
      }
      //Objeto de tipo direccion
      const dataDireccion: CrearDireccionDto = {
        latitud: noticiaDto.direccion.latitud || null,
        longitud: noticiaDto.direccion.longitud || null,
        traducciones: traducciones,
        localidad: localidad,
      };
      const crearDireccion = await this.crearDireccionService.crearDireccion(
        dataDireccion,
        opts,
      );

      let nuevaNoticia = {
        _id: idNoticia,
        autor: noticiaDto.autor || null,
        perfil: noticiaDto.perfil._id,
        traducciones: codTraduccionNoticia,
        direccion: crearDireccion._id,
        estado: codEstado,
        fechaCreacion: new Date(),
        fechaActualizacion: new Date(),
      };
      const crearNoticia = await new this.noticiaModel(nuevaNoticia).save(opts);

      let nuevoAlbum;
      let IdUsuario;
      const getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
        noticiaDto.perfil._id,
      );
      IdUsuario = getUsuario.usuario._id;
      if (noticiaDto.adjuntos.length > 0) {
        for (const album of noticiaDto.adjuntos) {
          const dataAlbum: any = {
            nombre: album.nombre,
            tipo: album.tipo.codigo,
            media: album.media,
            usuario: IdUsuario,
            predeterminado: album.predeterminado,
            idioma: codIdiom,
          };
          if (album.portada && album.portada._id) {
            dataAlbum.portada = album.portada;
          }
          nuevoAlbum = await this.crearAlbumService.crearAlbum(dataAlbum, opts);

          await this.noticiaModel.updateOne(
            { _id: idNoticia },
            { $push: { adjuntos: nuevoAlbum._id } },
            opts,
          );
        }
      }

      // _________________medias_____________________
      if (noticiaDto.medias.length > 0) {
        for (const media of noticiaDto.medias) {
          let idMedia = media._id;
          ////cambia de estado sinAsignar a activa
          await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
            idMedia,
            IdUsuario._id,
            codIdioma,
            null,
            false,
            opts,
          );
          //Acatualiza el array de medias
          await this.noticiaModel.updateOne(
            { _id: idNoticia },
            { $push: { medias: idMedia } },
            opts,
          );
        }
      }

      const dataNoticia = JSON.parse(JSON.stringify(crearNoticia));
      delete dataNoticia.__v;

      let newHistoricoNoticia: any = {
        datos: dataNoticia,
        usuario: noticiaDto.perfil,
        accion: getAccion,
        entidad: entidad.codigo,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistoricoNoticia);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      await this.traducirNoticiaSegundoPlanoService.traducirNoticiaSegundoPlano(
        crearNoticia._id,
        idiomaDetectado,
        crearNoticia.perfil,
      );
      return crearNoticia;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
