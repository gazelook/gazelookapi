import { ObtenerPortadaService } from './obtener-portada-predeterminada.service';
import { MediaServiceModule } from './../../media/casos_de_uso/media.services.module';
import { AlbumServiceModule } from './../../album/casos_de_uso/album.services.module';
import { PerfilServiceModule } from './../../perfil/casos_de_uso/perfil.services.module';
import { ObtenerNoticiasRecientePaginacionService } from './obtener-noticias-recientes-paginacion.service';
import { VotoNoticiaUnicaService } from './voto-noticia-unica.service';
import { EliminarNoticiasUnicaService } from './eliminar-noticia-unica.service';
import { ActualizarNoticiaUnicaService } from './actualizar-noticia-unica.service';
import { CrearNoticiaService } from './crear-noticia.service';
import { ObtenerNoticiasPerfilService } from './obtener-noticias-perfil.service';
import { Module, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { noticiaProviders } from '../drivers/noticia.provider';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { TraducirNoticiasPerfilService } from './traducir-noticia.service';
import { ObtenerNoticiaUnicaService } from './obtener-noticia-unica.service';
import { ObneterNoticiasTituloService } from './obtener-noticias-titulo.service';
import { ArchivoServicesModule } from 'src/entidades/archivo/casos_de_uso/archivo-services.module';
import { EliminarNoticiaCompletaService } from './eliminar-noticia-completa.service';
import { ObtenerNoticiaIdService } from './obtener-noticia-id.service';
import { ObtenerLocalidadNoticiaService } from './obtener-localidad-noticia.service';
import { CatalogoProviders } from 'src/entidades/catalogos/drivers/catalogo.provider';
import { ObtenerMediasNoticiasService } from './obtener-medias-noticias.service';
import { CalcularDiasNoticiaService } from './carcular-dias-noticia.service';
import { DireccionServiceModule } from 'src/entidades/direccion/casos_de_uso/direccion.services.module';
import { GestionNoticiaService } from './gestion-noticia.service';
import { TraducirNoticiaSegundoPlanoService } from './traducir-noticia-segundo-plano.service';
import { ObtenerResumenNoticiasService } from './obtener-resumen-noticia.service';
import { PaisServicesModule } from '../../pais/casos_de_uso/pais.services.module';
import { ObtenerAllNoticiasPaginacionService } from './obtener-all-noticias-paginacion.service';
import { Funcion } from 'src/shared/funcion';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => AlbumServiceModule),
    forwardRef(() => MediaServiceModule),
    forwardRef(() => ArchivoServicesModule),
    forwardRef(() => DireccionServiceModule),
    PaisServicesModule,
  ],
  providers: [
    ...noticiaProviders,
    ...CatalogoProviders,
    ObtenerNoticiasPerfilService,
    CrearNoticiaService,
    TraducirNoticiasPerfilService,
    ObtenerNoticiaUnicaService,
    ActualizarNoticiaUnicaService,
    EliminarNoticiasUnicaService,
    VotoNoticiaUnicaService,
    ObtenerNoticiasRecientePaginacionService,
    ObneterNoticiasTituloService,
    ObtenerPortadaService,
    EliminarNoticiaCompletaService,
    ObtenerNoticiaIdService,
    ObtenerLocalidadNoticiaService,
    ObtenerMediasNoticiasService,
    CalcularDiasNoticiaService,
    GestionNoticiaService,
    TraducirNoticiaSegundoPlanoService,
    ObtenerResumenNoticiasService,
    ObtenerAllNoticiasPaginacionService,
    Funcion,
  ],
  exports: [
    ObtenerNoticiasPerfilService,
    CrearNoticiaService,
    TraducirNoticiasPerfilService,
    ObtenerNoticiaUnicaService,
    ActualizarNoticiaUnicaService,
    EliminarNoticiasUnicaService,
    VotoNoticiaUnicaService,
    ObtenerNoticiasRecientePaginacionService,
    ObneterNoticiasTituloService,
    ObtenerPortadaService,
    EliminarNoticiaCompletaService,
    ObtenerNoticiaIdService,
    ObtenerLocalidadNoticiaService,
    ObtenerMediasNoticiasService,
    CalcularDiasNoticiaService,
    GestionNoticiaService,
    TraducirNoticiaSegundoPlanoService,
    ObtenerResumenNoticiasService,
    ObtenerAllNoticiasPaginacionService,
  ],
  controllers: [],
})
export class NoticiaServicesModule {}
