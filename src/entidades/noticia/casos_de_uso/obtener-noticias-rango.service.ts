import { Inject, Injectable } from '@nestjs/common';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class ObneterNoticiasRangoService {
  constructor(
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: PaginateModel<
      TraduccionesNoticias
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
  ) {}

  async obtenerPorRango(
    idioma: string,
    tipoProyecto: string,
    fechaInicial: string,
    fechaFinal: string,
    limite: number,
    pagina: number,
  ): Promise<any> {
    try {
      //Obtiene el codigo del idioma
      const codIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );

      //Obtiene la entidad traduccion Noticia
      const entidadTraduccionNoticia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidadTradNoticia = entidadTraduccionNoticia.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estadoTradNoticia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradNoticia,
      );
      let codEstadoTradNoticia = estadoTradNoticia.codigo;

      //Obtiene la entidad Noticia
      const entidadNoticia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidadNoticia = entidadNoticia.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estadoNoticia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadNoticia,
      );
      let codEstadoNoticia = estadoNoticia.codigo;

      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      const estadoActAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoActAlbum = estadoActAlbum.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      const populateNoticia = {
        path: 'referencia',
        select: '-__v ',
        populate: {
          path: 'adjuntos',
          select: '-fechaActualizacion -fechaCreacion -__v -estado -nombre ',
          match: { estado: codEstadoActAlbum },
          populate: [
            {
              path: 'media',
              match: { estado: codEstadoMedia },
              select:
                'principal enlace miniatura  fechaCreacion fechaActualizacion catalogoMedia',
              populate: [
                {
                  path: 'traducciones',
                  select: 'descripcion',
                  match: { idioma: codIdioma, estado: codEstadoTradMedia },
                },
                {
                  path: 'principal',
                  select: 'url tipo duracion fechaActualizacion ',
                },
                {
                  path: 'miniatura',
                  select: 'url tipo duracion fechaActualizacion ',
                },
              ],
            },
            {
              path: 'portada',
              match: { estado: codEstadoMedia },
              select:
                'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
              populate: [
                {
                  path: 'traducciones',
                  select: 'descripcion',
                  match: { idioma: codIdioma, estado: codEstadoTradMedia },
                },
                {
                  path: 'principal',
                  select: 'url tipo duracion fechaActualizacion ',
                },
                {
                  path: 'miniatura',
                  select: 'url tipo duracion fechaActualizacion ',
                },
              ],
            },
          ],
        },
      };

      const options = {
        lean: true,
        sort: { titulo: 1 },
        select: ' -__v ',
        populate: [populateNoticia],
        page: Number(pagina),
        limit: Number(limite),
      };

      const inicio = new Date(fechaInicial);
      fechaFinal = fechaFinal + 'T23:59:59.999Z';
      const final = new Date(fechaFinal);

      const noticias = await this.traduccionNoticiaModel.paginate(
        {
          $and: [
            // { titulo: { $regex: '.*' + titulo + '.*', $options: 'i' } },
            // { fechaCreacion: { $gt: inicio } },
            // { fechaCreacion: { $lt: final } },
            { estado: codEstadoTradNoticia },
            { idioma: codIdioma.codigo },
          ],
        },
        options,
      );
      return noticias;
    } catch (error) {
      throw error;
    }
  }
}
