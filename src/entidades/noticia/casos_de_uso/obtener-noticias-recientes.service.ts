import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model, PaginateModel } from 'mongoose';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { TraducirNoticiasPerfilService } from './traducir-noticia.service';

@Injectable()
export class ObtenerNoticiasRecienteService {
  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: Model<TraduccionesNoticias>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirNoticiaService: TraducirNoticiasPerfilService,
  ) {}

  async obtenerNoticiasReciente(perfil: string, codIdim: string): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidad = entidad.codigo;
      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstadoTNoticia = estado.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      const populateTraduccion = {
        path: 'traducciones',
        select:
          '-fechaActualizacion -fechaCreacion -idioma -__v -descripcion -tags -original',
        match: {
          idioma: codIdioma,
          estado: codEstadoTNoticia,
        },
      };
      const idAlbum = {
        path: 'idAlbum',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        populate: {
          path: 'idMedia',
          select: 'principal enlace',
          populate: {
            path: 'principal',
            select: 'url fileDefault duracion fechaActualizacion -_id',
          },
        },
      };

      const options = {
        lean: true,
        sort: { fechaCreacion: -1 },
        select: ' -__v -direccion  -perfil -estado  ',
        populate: [populateTraduccion, idAlbum],
      };
      // const getPNoticias = await this.noticiaModel.find({ estado: codEstadoNoticia }, options);
      const noticias = [];

      for (let i = 0; i < 10000; i++) {
        if (noticias.length === 5) {
          break;
        }

        let fechaInicial = this.sumarDias(new Date(), 1 + i);
        fechaInicial.setUTCHours(0, 0, 0, 0);

        let fechaFinal = this.sumarDias(new Date(), 0 + i);
        fechaFinal.setUTCHours(0, 0, 0, 0);

        //{ $and: [{ fechaActualizacion: { $gt: fechaInicial } }, { fechaActualizacion: { $lt: fechaFinal } }] }
        const getPNoticias = await this.noticiaModel
          .find({
            $and: [
              { fechaCreacion: { $gt: fechaInicial } },
              { fechaCreacion: { $lt: fechaFinal } },
            ],
          })
          .limit(3);
        const allNoticias: any = getPNoticias;

        let existe = false;
        for (let i = 0; i < allNoticias.length; i++) {
          existe = false;
          for (
            let index = 0;
            index < allNoticias[i].traducciones.length;
            index++
          ) {
            let idTraduccionNoticia = allNoticias[i].traducciones[index]['_id'];
            let getTraductorNoticia = await this.traduccionNoticiaModel.find({
              $and: [{ _id: idTraduccionNoticia }, { idioma: codIdioma }],
            });
            if (getTraductorNoticia.length > 0) {
              existe = true;
            }
          }
          if (existe === false) {
            await this.traducirNoticiaService.traducirNoticia(
              allNoticias[i]._id,
              codIdim,
              perfil,
              opts,
            );
          }
        }
        const getPNoticiasF = await this.noticiaModel
          .find({
            $and: [
              { fechaCreacion: { $gt: fechaInicial } },
              { fechaCreacion: { $lt: fechaFinal } },
            ],
          })
          .session(session)
          .populate({
            path: 'traducciones',
            select:
              '-fechaActualizacion -fechaCreacion -idioma -__v -descripcion -tags -original',
            match: {
              idioma: codIdioma,
              estado: codEstadoTNoticia,
            },
          })
          .populate({
            path: 'adjuntos',
            select: '-fechaActualizacion -fechaCreacion -__v -nombre',
            populate: {
              path: 'media',
              select: 'principal enlace',
              populate: {
                path: 'principal',
                select: 'url duracion fechaActualizacion -_id',
              },
            },
          })
          .populate({
            path: 'medias',
            select:
              'principal enlace miniatura fechaCreacion fechaActualizacion -_id catalogoMedia',
            match: { estado: codEstadoMedia },
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fechaActualizacion -_id',
              },
            ],
          })
          .populate({
            path: 'votos',
            select: 'perfil',
            populate: {
              path: 'perfil',
              select: '_id nombre',
            },
          })
          .select('-__v -direccion  -perfil -estado ')
          .limit(3);

        noticias.push(getPNoticiasF);
      }
      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return noticias;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  sumarDias(fecha, dia) {
    fecha.setDate(fecha.getDate() - dia);
    return fecha;
  }
}
