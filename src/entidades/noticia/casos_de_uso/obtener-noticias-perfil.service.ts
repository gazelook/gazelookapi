import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { PaginateModel } from 'mongoose';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import {
  codigoEstadoAlbum,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { CalcularDiasNoticiaService } from './carcular-dias-noticia.service';
import { ObtenerPortadaService } from './obtener-portada-predeterminada.service';
import { TraducirNoticiasPerfilService } from './traducir-noticia.service';

@Injectable()
export class ObtenerNoticiasPerfilService {
  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirNoticiaService: TraducirNoticiasPerfilService,
    private obtenerPortada: ObtenerPortadaService,
    private calcularDiasNoticiaService: CalcularDiasNoticiaService,
  ) {}

  async obtenerNoticiasPaginacion(
    perfil: string,
    codIdim: string,
    limit: number,
    page: number,
    traduce: string,
    response?: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstadoTNoticia = estado.codigo;

      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN.codigo;

      //Obtiene el estado
      const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticia = estadoN.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      let populateTraduccion;
      if (traduce === 'true') {
        populateTraduccion = {
          path: 'traducciones',
          select:
            '-fechaActualizacion -fechaCreacion -idioma -__v -tags -original',
          match: {
            idioma: codIdioma,
            estado: codEstadoTNoticia,
          },
        };
      } else {
        populateTraduccion = {
          path: 'traducciones',
          select:
            '-fechaActualizacion -fechaCreacion -idioma -__v -tags -original',
          match: {
            original: true,
            estado: codEstadoTNoticia,
          },
        };
      }

      const medias = {
        path: 'medias',
        select:
          'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
        ],
      };
      const media = {
        path: 'media',
        select:
          'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
        ],
      };

      const portada = {
        path: 'portada',
        select:
          'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
        ],
      };
      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -nombre',
        match: {
          estado: codigoEstadoAlbum.activa,
        },
        populate: [media, portada],
      };

      const votos = {
        path: 'votos',
        select: 'perfil',
        populate: { path: 'perfil', select: '_id nombre' },
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto nombreContactoTraducido',
      };

      const options = {
        session: session,
        lean: true,
        sort: { fechaCreacion: -1 },
        select: ' -__v ',
        populate: [populateTraduccion, adjuntos, medias, votos, populatePerfil],
        page: Number(page),
        limit: Number(limit),
      };

      const noticias = await this.noticiaModel.paginate(
        { perfil: perfil, estado: codEstadoNoticia },
        options,
      );
      console.log('noticias.docs.length: ', noticias.docs.length);
      if (noticias.docs.length > 0) {
        const notiFinal = [];
        for (let noticia of noticias.docs) {
          let port: any;
          let actu = false;
          let datos: any;
          let votoBo = false;
          if (
            noticia.fechaCreacion.toString() ==
            noticia.fechaActualizacion.toString()
          ) {
            actu = false;
          } else {
            let dias = this.calcularDiasNoticiaService.calcularDias(
              new Date(),
              noticia.fechaActualizacion,
            );
            if (dias) actu = true;
          }
          let objAdjun: any;
          // llamamos al servicio para obtener una portada prederder si no devueleve una por defecto
          console.log('****************************************');
          console.log('noticia: ', noticia._id);
          objAdjun = await this.obtenerPortada.obtenerPortada(
            noticia.adjuntos,
            codIdim,
            opts,
          );
          let arrayAdj = [];
          if (objAdjun.length > 0) {
            for (const getAdjuntos of objAdjun) {
              // if (getAdjuntos.predeterminado) {
              arrayAdj.push(getAdjuntos);
              // break
              // }
            }
          }

          if (noticia.votos.length > 0) {
            for (const votos of noticia.votos) {
              if (votos['perfil']) {
                if (votos['perfil']._id == perfil) {
                  votoBo = true;
                  break;
                } else {
                  votoBo = false;
                }
              }
            }
          } else {
            votoBo = false;
          }
          let getTraduccion: any;
          if (noticia.traducciones[0] === undefined) {
            console.log('se debe traducir: ', noticia._id);

            getTraduccion = await this.traducirNoticiaService.traducirNoticia(
              noticia._id,
              codIdim,
              perfil,
              opts,
            );
          } else {
            let trad = [
              {
                tituloCorto: noticia.traducciones[0]['tituloCorto'],
                titulo: noticia.traducciones[0]['titulo'],
                descripcion: noticia.traducciones[0]['descripcion'],
                tags: noticia.traducciones[0]['tags'],
              },
            ];
            getTraduccion = trad;
          }

          datos = {
            _id: noticia._id,
            traducciones: getTraduccion,
            adjuntos: arrayAdj,
            perfil: noticia.perfil,
            fechaCreacion: noticia.fechaCreacion,
            fechaActualizacion: noticia.fechaActualizacion,
            estado: {
              codigo: noticia.estado,
            },
            actualizado: actu, //Si la noticia ha sido actualizada recientemente =true
            voto: votoBo, //si el perfil ya a votado =true si no =false
          };
          notiFinal.push(datos);
        }

        response.set(headerNombre.totalDatos, noticias.totalDocs);
        response.set(headerNombre.totalPaginas, noticias.totalPages);
        response.set(headerNombre.proximaPagina, noticias.hasNextPage);
        response.set(headerNombre.anteriorPagina, noticias.hasPrevPage);
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return notiFinal;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return noticias.docs;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
  // obtiene la lista de noticias de un perfil para activarlas o eliminarlas
  async obtenerNoticiasPerfil(perfil: string): Promise<any> {
    try {
      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidad = entidad.codigo;

      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN.codigo;

      //Obtiene el estado
      const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticia = estadoN.codigo;

      const noticias = await this.noticiaModel
        .find({
          $and: [{ perfil: perfil }, { estado: codEstadoNoticia }],
        })
        .populate([
          {
            path: 'traducciones',
          },
          {
            path: 'adjuntos',
          },
        ]);
      return noticias;
    } catch (error) {
      throw error;
    }
  }

  // obtiene la lista de noticias max 3 para que se visualice en el perfil general
  async obtenerNoticiasRecientesPerfil(
    perfil: string,
    codNombredioma: string,
    idPerfilOtros: string,
    opts: any,
  ): Promise<any> {
    try {
      //Obtiene la entidad traduccionNoticia
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion noticia
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTProyecto = estado.codigo;

      //Obtiene el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codNombredioma,
      );

      let noticias = await this.getNoticias(
        perfil,
        codNombredioma,
        idPerfilOtros,
        opts,
      );

      for (const iterator of noticias) {
        //const traducciones = await this.traduccionNoticiaModel.find({ $and: [{ proyecto: iterator._id }, { idioma: idioma.codigo }, { estado: codEstadoTProyecto }] }).session(opts.session)

        if (iterator.traducciones.length === 0) {
          //Llama al servicio de traduccion de proyecto
          await this.traducirNoticiaService.traducirNoticia(
            iterator._id,
            codNombredioma,
            perfil,
            opts,
          );
        }
      }
      noticias = await this.getNoticias(
        perfil,
        codNombredioma,
        idPerfilOtros,
        opts,
      );

      return noticias;
    } catch (error) {
      throw error;
    }
  }

  // obtiene las noticias recientes limite 3
  async getNoticias(
    idPerfil,
    codNombredioma,
    idPerfilOtros,
    opts,
  ): Promise<any> {
    //Obtiene el codigo de la entidad media
    const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    let codEntidadMedia = entidadmedia.codigo;

    //Obtiene el estado activo de la entidad media
    const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadMedia,
    );
    let codEstadoMedia = estadoMedia.codigo;
    try {
      //Obtiene la entidad traduccionNoticia
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion noticia
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTProyecto = estado.codigo;

      // entidad noticia
      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN.codigo;

      //Obtiene el estado de la noticia
      const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticia = estadoN.codigo;

      const estadoNE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadN,
      );
      let codEstadoNoticiaEli = estadoNE.codigo;

      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      const estadoActAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoActAlbum = estadoActAlbum.codigo;

      //Obtiene el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codNombredioma,
      );
      let codIdioma = idioma.codigo;

      let populateTraduccion;
      if (idPerfilOtros) {
        populateTraduccion = {
          path: 'traducciones',
          select: '-idioma -__v -descripcion -tags -original',
          sort: '-fechaActualizacion',
          match: {
            idioma: codIdioma,
            estado: codEstadoTProyecto,
          },
        };
      } else {
        populateTraduccion = {
          path: 'traducciones',
          select: '-idioma -__v -descripcion -tags -original',
          sort: '-fechaActualizacion',
          match: {
            original: true,
            estado: codEstadoTProyecto,
          },
        };
      }

      const adjuntos = {
        path: 'adjuntos',
        select: '-fechaActualizacion -fechaCreacion -__v -estado -nombre ',
        match: { estado: codEstadoActAlbum },
        populate: [
          {
            path: 'media',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura  catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fechaActualizacion ',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fechaActualizacion ',
              },
            ],
          },
          {
            path: 'portada',
            match: { estado: codEstadoMedia },
            select: 'principal enlace miniatura  catalogoMedia',
            populate: [
              {
                path: 'traducciones',
                select: 'descripcion',
                match: { idioma: codIdioma },
              },
              {
                path: 'principal',
                select: 'url tipo duracion fechaActualizacion ',
              },
              {
                path: 'miniatura',
                select: 'url tipo duracion fechaActualizacion ',
              },
            ],
          },
        ],
      };
      const noticias = await this.noticiaModel
        .find({
          $and: [
            { perfil: idPerfil },
            { estado: { $ne: codEstadoNoticiaEli } },
          ],
        })
        .populate([populateTraduccion, adjuntos])
        .sort('-fechaActualizacion')
        .limit(3)
        .session(opts.session);

      return noticias;
    } catch (error) {
      throw error;
    }
  }
  calcularDias(fechaActual, fechaActualizacion) {
    if (fechaActualizacion) {
      let dias =
        (fechaActual.getTime() - fechaActualizacion.getTime()) /
        (60 * 60 * 24 * 1000);
      if (dias <= 5) {
        return true;
      } else return false;
    } else {
      return false;
    }
  }
}
