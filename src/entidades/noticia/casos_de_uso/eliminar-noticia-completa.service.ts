import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { PaginateModel } from 'mongoose';
import { EliminarAlbumCompletoService } from 'src/entidades/album/casos_de_uso/eliminar-album-completo.service';
import { EliminarMediaCompletoService } from 'src/entidades/media/casos_de_uso/eliminar-media-completo.service';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { TraduccionesVotoNoticia } from 'src/drivers/mongoose/interfaces/traduccion_voto_noticia/traduccion-voto-noticia.interface';
import { VotoNoticia } from 'src/drivers/mongoose/interfaces/voto_noticia/voto-noticia.interface';

@Injectable()
export class EliminarNoticiaCompletaService {
  noticia: any;

  constructor(
    @Inject('VOTO_NOTICIA_MODEL')
    private readonly votoNoticiaModel: Model<VotoNoticia>,
    @Inject('TRADUCCION_VOTO_NOTICIA_MODEL')
    private readonly traduccionVotoNoticiaModel: Model<TraduccionesVotoNoticia>,
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: Model<TraduccionesNoticias>,
    //private eliminarParticipanteProyectoCompletoService: EliminarParticipanteProyectoCompletoService,
    private eliminarAlbumCompletoService: EliminarAlbumCompletoService,
    private eliminarMediaCompletoService: EliminarMediaCompletoService,
  ) //private eliminarComentarioCompletoService: EliminarComentarioCompletoService
  {}

  async eliminarNoticiaCompleta(idPerfil: string, opts): Promise<any> {
    try {
      const noticia = await this.noticiaModel.find({ perfil: idPerfil });

      if (noticia) {
        for (const getNoticia of noticia) {
          //Comprueba si la noticia tiene adjuntos
          if (getNoticia.adjuntos.length > 0) {
            //Elimina todos los adjuntos uno a uno
            for (const getAdjuntos of getNoticia.adjuntos) {
              await this.eliminarAlbumCompletoService.eliminarAlbumCompleto(
                getAdjuntos,
                opts,
              );
            }
          }
          //Comprueba si la noticia tiene medias
          if (getNoticia.medias.length > 0) {
            //Elimina todos las medias uno a uno
            for (const getMedias of getNoticia.medias) {
              await this.eliminarMediaCompletoService.eliminarMediaCompleto(
                getMedias,
                opts,
              );
            }
          }
          //Comprueba si la noticia tiene votos
          if (getNoticia.votos.length > 0) {
            //Elimina la traduccion de los votos uno a uno
            for (const getVotoProyecto of getNoticia.votos) {
              await this.traduccionVotoNoticiaModel.deleteMany(
                { referencia: getVotoProyecto },
                opts,
              );
            }
            await this.votoNoticiaModel.deleteMany(
              { noticia: getNoticia._id },
              opts,
            );
          }
          //Comprueba si la noticia tiene traducciones
          if (getNoticia.traducciones.length > 0) {
            //Elimina todas las traducciones de la noticia
            await this.traduccionNoticiaModel.deleteMany(
              { referencia: getNoticia._id },
              opts,
            );
          }

          //Elimina el noticia completa
          await this.noticiaModel.deleteOne({ _id: getNoticia._id }, opts);
        }
      }

      return true;
    } catch (error) {
      throw error;
    }
  }
}
