import { Inject, Injectable } from '@nestjs/common';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { PaginateModel } from 'mongoose';
import { estadosNoticia } from 'src/shared/enum-sistema';

@Injectable()
export class ObtenerNoticiaIdService {
  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
  ) {}

  async obtenerNoticiaById(idNoticia): Promise<any> {
    try {
      const getNoticia = await this.noticiaModel.findOne({ _id: idNoticia });
      return getNoticia;
    } catch (error) {
      throw error;
    }
  }

  async obtenerNoticiaByIdPersonalizado(idNoticia): Promise<any> {
    try {
      const populateMedia = {
        path: 'media',
        select:
          'principal miniatura catalogoMedia fechaActualizacion traducciones',
        populate: [
          {
            path: 'principal',
            select: 'url tipo filename fileDefault duracion fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: 'filename url tipo',
          },
        ],
      };

      const populateReferenciaEntidadCompartida = {
        select: '_id traducciones adjuntos fechaActualizacion',
        populate: [
          {
            path: 'traducciones',
            select: 'idioma titulo tituloCorto',
          },
          {
            path: 'adjuntos',
            populate: [
              populateMedia,
              {
                path: 'portada',
                populate: populateMedia,
              },
            ],
          },
        ],
      };

      let getNoticia = await this.noticiaModel
        .findOne({ _id: idNoticia })
        .select('_id traducciones adjuntos fechaActualizacion')
        .populate([
          {
            path: 'traducciones',
            select: 'idioma titulo tituloCorto',
          },
          {
            path: 'adjuntos',
            populate: [
              populateMedia,
              {
                path: 'portada',
                populate: populateMedia,
              },
            ],
          },
        ])
        .lean();
      return getNoticia;
    } catch (error) {
      throw error;
    }
  }

  async obtenerTotalNoticias(): Promise<any> {
    try {
      const getNoticia = await this.noticiaModel.find({
        estado: { $ne: estadosNoticia.noticiaEliminada },
      });
      return getNoticia.length;
    } catch (error) {
      throw error;
    }
  }
}
