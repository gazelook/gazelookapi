import { Inject, Injectable } from '@nestjs/common';
import { Model, PaginateModel } from 'mongoose';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { ObtenerPortadaService } from './obtener-portada-predeterminada.service';
import { TraducirNoticiasPerfilService } from './traducir-noticia.service';

@Injectable()
export class ObtenerResumenNoticiasService {
  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: Model<TraduccionesNoticias>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirNoticiaService: TraducirNoticiasPerfilService,
    private obtenerPortada: ObtenerPortadaService,
  ) {}

  async obtenerResumenNoticia(
    idNoticia: string,
    codIdim: string,
    opts,
  ): Promise<any> {
    try {
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstadoTNoticia = estado.codigo;

      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN.codigo;

      //Obtiene el estado
      const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadN,
      );
      let codEstadoNoticia = estadoN.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      const media = {
        path: 'media',
        select:
          'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
        ],
      };

      const portada = {
        path: 'portada',
        select:
          'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fechaActualizacion fileDefault',
          },
        ],
      };

      const noticia = await this.noticiaModel
        .findOne({
          $and: [{ _id: idNoticia }, { estado: { $ne: codEstadoNoticia } }],
        })
        .populate({
          path: 'perfil',
          select: '_id nombre nombreContacto',
        })
        .populate({
          path: 'votos',
          select: 'perfil',
          populate: { path: 'perfil', select: '_id nombre' },
        })
        .populate({
          path: 'adjuntos',
          select: '-fechaActualizacion -fechaCreacion -__v -nombre',
          populate: [media, portada],
        })
        .populate({
          path: 'medias',
          select:
            'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
          match: { estado: codEstadoMedia },
          populate: [
            {
              path: 'traducciones',
              select: 'descripcion',
              match: { idioma: codIdioma, estado: codEstadoTradMedia },
            },
            {
              path: 'principal',
              select: 'url tipo duracion fechaActualizacion fileDefault',
            },
            {
              path: 'miniatura',
              select: 'url tipo duracion fechaActualizacion fileDefault',
            },
          ],
        })
        .populate({
          path: 'traducciones',
          select:
            '-fechaActualizacion -fechaCreacion -idioma -__v -tags -original',
          match: {
            idioma: codIdioma,
            estado: codEstadoTNoticia,
          },
        });

      if (noticia) {
        const notiFinal = [];
        let port: any;
        let actu = false;
        let datos: any;
        let votoBo = false;
        let dias = this.calcularDias(new Date(), noticia.fechaActualizacion);
        if (dias) actu = true;
        let objAdjun: any;
        // llamamos al servicio para obtener una portada predeterminada si no devuelve una por defecto
        objAdjun = await this.obtenerPortada.obtenerPortada(
          noticia.adjuntos,
          codIdim,
          opts,
        );
        let arrayAdj = [];
        if (objAdjun.length > 0) {
          for (const getAdjuntos of objAdjun) {
            // if (getAdjuntos.predeterminado) {
            arrayAdj.push(getAdjuntos);
            // break
            // }
          }
        }

        // if (noticia.votos.length > 0) {
        //   for (const votos of noticia.votos) {

        //     if (votos['perfil']._id == perfil) {
        //       votoBo = true;
        //       break;
        //     } else {
        //       votoBo = false;
        //     }
        //   }
        // } else {
        //   votoBo = false;
        // }

        let getTraduccion: any;
        if (noticia.traducciones[0] === undefined) {
          getTraduccion = await this.traducirNoticiaService.traducirNoticia(
            noticia._id,
            codIdim,
            null,
            opts,
          );
        } else {
          let trad = [
            {
              tituloCorto: noticia.traducciones[0]['tituloCorto'],
              titulo: noticia.traducciones[0]['titulo'],
              descripcion: noticia.traducciones[0]['descripcion'],
              // tags: noticia.traducciones[0]['tags'],
            },
          ];
          getTraduccion = trad;
        }

        datos = {
          _id: noticia._id,
          traducciones: getTraduccion,
          adjuntos: arrayAdj,
          perfil: noticia.perfil,
          fechaCreacion: noticia.fechaCreacion,
          fechaActualizacion: noticia.fechaActualizacion,
          estado: {
            codigo: noticia.estado,
          },
          actualizado: actu, //Si la noticia ha sido actualizada recientemente =true
          // voto: votoBo,//si el perfil ya a votado =true si no =false
        };
        // notiFinal.push(datos)

        return datos;
      } else {
        return noticia;
      }
    } catch (error) {
      throw error;
    }
  }

  calcularDias(fechaActual, fechaActualizacion) {
    if (fechaActualizacion) {
      let dias =
        (fechaActual.getTime() - fechaActualizacion.getTime()) /
        (60 * 60 * 24 * 1000);
      if (dias <= 5) {
        return true;
      } else return false;
    } else {
      return false;
    }
  }
}
