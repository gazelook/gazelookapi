import { idiomas } from './../../../shared/enum-sistema';
import { Injectable } from '@nestjs/common';
const sw = require('stopword');

@Injectable()
export class GestionNoticiaService {
  constructor() {}

  detectarIdioma(codIdioma) {
    switch (codIdioma) {
      case idiomas.espanol:
        return sw.es;
      case idiomas.ingles:
        return sw.en;
      case idiomas.aleman:
        return sw.de;
      case idiomas.frances:
        return sw.fr;
      case idiomas.italiano:
        return sw.it;
      case idiomas.portugues:
        return sw.pt;
      default:
        return sw.en;
    }
  }
}
