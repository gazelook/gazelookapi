import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model, PaginateModel } from 'mongoose';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { CrearAlbumService } from 'src/entidades/album/casos_de_uso/crear-album.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { ActualizarDireccionService } from 'src/entidades/direccion/casos_de_uso/actualizar-direccion.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { ActualizarMediaService } from 'src/entidades/media/casos_de_uso/actualizar-media.service';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { ActualizarNoticiaDto } from '../entidad/actualizar-noticia.dto';
import {
  codigosHibernadoEntidades,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from './../../../shared/enum-sistema';
import { ObtenerNoticiaUnicaService } from './obtener-noticia-unica.service';
import { TraducirNoticiaSegundoPlanoService } from './traducir-noticia-segundo-plano.service';

@Injectable()
export class ActualizarNoticiaUnicaService {
  noticia: any;

  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: Model<TraduccionesNoticias>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private readonly obtenerNoticiaUnicaService: ObtenerNoticiaUnicaService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private crearAlbumService: CrearAlbumService,
    private actualizarMediaService: ActualizarMediaService,
    private actualizarDireccionService: ActualizarDireccionService,
    private traducirNoticiaSegundoPlanoService: TraducirNoticiaSegundoPlanoService,
  ) {}

  async actualizarNoticiaUnica(
    noticiaActualizar: ActualizarNoticiaDto,
    idNoticia: any,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const accionAc = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccionAc = accionAc.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstadoTNoticia = estado.codigo;

      const estadoTE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidad,
      );
      let codEstadoTNoticiaEliminado = estadoTE.codigo;

      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN.codigo;

      //Obtiene el estado
      const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticia = estadoN.codigo;

      //const noticia = await this.noticiaModel.findOne({ $and: [{ _id: idNoticia }, { estado: codEstadoNoticia }, { perfil: noticiaActualizar.perfil._id }] });

      const noticiaOriginal = await this.noticiaModel
        .findOne({
          $and: [
            { _id: idNoticia },
            { estado: codEstadoNoticia },
            { perfil: noticiaActualizar.perfil._id },
          ],
        })
        .populate({
          path: 'traducciones',
          select: '-fechaActualizacion  -__v ',
          match: { estado: codEstadoTNoticia, original: true },
        })
        .populate({
          path: 'medias',
          select: '-fechaActualizacion  -__v ',
        });

      if (noticiaOriginal) {
        const updateTradu = await this.traduccionNoticiaModel.updateMany(
          { referencia: idNoticia },
          { $set: { estado: codEstadoTNoticiaEliminado } },
        );

        let idiomaDetectado: any;
        if (noticiaActualizar.traducciones[0].titulo) {
          let textoTraducido = await traducirTexto(
            codIdiom,
            noticiaActualizar.traducciones[0].titulo,
          );
          idiomaDetectado = textoTraducido.idiomaDetectado;
        } else {
          if (noticiaActualizar.traducciones[0].tituloCorto) {
            let textoTraducido = await traducirTexto(
              codIdiom,
              noticiaActualizar.traducciones[0].tituloCorto,
            );
            idiomaDetectado = textoTraducido.idiomaDetectado;
          } else {
            if (noticiaActualizar.traducciones[0].descripcion) {
              let textoTraducido = await traducirTexto(
                codIdiom,
                noticiaActualizar.traducciones[0].descripcion,
              );
              idiomaDetectado = textoTraducido.idiomaDetectado;
            } else {
              const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
                codIdiom,
              );
              idiomaDetectado = idioma.codigo;
            }
          }
        }

        //Obtiene el idioma por el codigo
        const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
          idiomaDetectado,
        );
        let codIdioma = idioma.codigo;
        // const T = this.gestionNoticiaService.detectarIdioma(codIdiom);

        // let titu, tagsTitulo, tituCort, tagsTituloCort, tituloTrad, tituloCortoTrad, descripTrad;

        // if (noticiaActualizar.traducciones[0].titulo !== undefined) {
        //   titu = noticiaActualizar.traducciones[0].titulo.split(' ');
        //   tituloTrad = noticiaActualizar.traducciones[0].titulo;
        // } else {

        //   titu = noticiaOriginal.traducciones[0]['titulo'].split(' ');
        //   tituloTrad = noticiaOriginal.traducciones[0]['titulo'];
        // }

        // if (noticiaActualizar.traducciones[0].descripcion !== undefined) {
        //   descripTrad = noticiaActualizar.traducciones[0].descripcion;
        // } else {
        //   descripTrad = noticiaOriginal.traducciones[0]['descripcion']
        // }

        // if (noticiaActualizar.traducciones[0].tituloCorto !== undefined) {
        //   tituCort = noticiaActualizar.traducciones[0].tituloCorto.split(' ');
        //   tituloCortoTrad = noticiaActualizar.traducciones[0].tituloCorto;

        // } else {
        //   tagsTituloCort = noticiaOriginal.traducciones[0]['tituloCorto'].split(' ');
        //   tituloCortoTrad = noticiaOriginal.traducciones[0]['tituloCorto'];
        // }

        // tagsTitulo = sw.removeStopwords(titu, T);
        // tagsTituloCort = sw.removeStopwords(tituCort, T);

        // const tagsUnidos = tagsTitulo.concat(tagsTituloCort);
        // const tags = tagsUnidos.filter(function (item, index, array) {
        //   return array.indexOf(item) === index;
        // });

        let newTraduccionNoticia = {
          tituloCorto: noticiaActualizar.traducciones[0].tituloCorto || null,
          titulo: noticiaActualizar.traducciones[0].titulo || null,
          descripcion: noticiaActualizar.traducciones[0].descripcion || null,
          // tags: tags,
          idioma: codIdioma,
          original: true,
          estado: codEstadoTNoticia,
          referencia: idNoticia,
        };
        const crearTraduccionNoticia = await new this.traduccionNoticiaModel(
          newTraduccionNoticia,
        ).save(opts);

        const updt = await this.noticiaModel.updateOne(
          { _id: idNoticia },
          { $push: { traducciones: crearTraduccionNoticia._id } },
          opts,
        );

        if (noticiaActualizar.autor) {
          const dataAct = {
            autor: noticiaActualizar.autor,
            fechaActualizacion: new Date(),
          };
          const b = await this.noticiaModel.findByIdAndUpdate(
            idNoticia,
            dataAct,
            opts,
          );
        }

        //Comprueba si viene direccion para actualizar
        if (noticiaActualizar.direccion !== undefined) {
          //Comprueba si llega la direccion
          let traducciones: any;
          if (noticiaActualizar.direccion.traducciones.length > 0) {
            traducciones = noticiaActualizar.direccion.traducciones;
          } else {
            traducciones = null;
          }
          //Comprueba si llega la localidad
          let localidad: any;
          if (noticiaActualizar.direccion.localidad) {
            localidad = noticiaActualizar.direccion.localidad.codigo;
          } else {
            localidad = null;
          }
          //Objeto para actualizar la direccion
          const dataDireccion = {
            latitud: noticiaActualizar.direccion.latitud || null,
            longitud: noticiaActualizar.direccion.longitud || null,
            traducciones: traducciones,
            localidad: localidad,
            _id: noticiaOriginal.direccion,
          };
          const actualizarDireccion = await this.actualizarDireccionService.actualizarDireccion(
            dataDireccion,
            opts,
          );
        }

        // _________________adjuntos_____________________
        let nuevoAlbum;
        let IdUsuario;

        const getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
          noticiaActualizar.perfil._id,
        );
        IdUsuario = getUsuario.usuario._id;

        if (noticiaActualizar.adjuntos.length > 0) {
          for (const album of noticiaActualizar.adjuntos) {
            const dataAlbum: any = {
              nombre: album.nombre,
              tipo: album.tipo.codigo,
              media: album.media,
              usuario: IdUsuario,
              predeterminado: album.predeterminado,
              idioma: idiomaDetectado,
            };
            if (album.portada && album.portada._id) {
              dataAlbum.portada = album.portada;
            }
            //crea el nuevo album
            nuevoAlbum = await this.crearAlbumService.crearAlbum(
              dataAlbum,
              opts,
            );
            //Actualiza el array de adjuntos
            await this.noticiaModel.updateOne(
              { _id: idNoticia },
              {
                $push: { adjuntos: nuevoAlbum._id },
                fechaActualizacion: new Date(),
              },
              opts,
            );
          }
        }

        // _________________medias_____________________
        if (noticiaActualizar.medias.length > 0) {
          for (const media of noticiaActualizar.medias) {
            let idMedia = media._id;
            ////cambia de estado sinAsignar a activa
            await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
              idMedia,
              IdUsuario,
              codIdioma,
              null,
              false,
              opts,
            );

            //Verifica si tiene media
            if (noticiaOriginal.medias.length > 0) {
              const getIdMedia = noticiaOriginal.medias[0]['_id'];
              const getProyecto = await this.noticiaModel
                .findOne({ _id: idNoticia })
                .session(session);
              let listaMedias = [];
              if (getProyecto.medias) {
                //Si tiene rol se le quita los roles que tiene
                for (const getMedia of getProyecto.medias) {
                  listaMedias.push(getMedia['_id'].toString());
                }
                let index = listaMedias.indexOf(getIdMedia);
                //Eliminamos el elemento del array
                listaMedias.splice(index, 1);
              }

              // ______ datos medias _____
              let dataMedias: any = {
                medias: listaMedias,
                fechaActualizacion: new Date(),
              };

              //Se le quita la medias anteriores
              await this.noticiaModel.findByIdAndUpdate(
                idNoticia,
                dataMedias,
                opts,
              );

              //Actualiza el array de medias
              await this.noticiaModel.updateOne(
                { _id: idNoticia },
                { $push: { medias: idMedia }, fechaActualizacion: new Date() },
                opts,
              );
            } else {
              //Acatualiza el array de medias
              await this.noticiaModel.updateOne(
                { _id: idNoticia },
                { $push: { medias: idMedia }, fechaActualizacion: new Date() },
                opts,
              );
            }
          }
        } else {
          //Verifica si tiene media la nooticia
          if (noticiaOriginal.medias.length > 0) {
            const getIdMedia = noticiaOriginal.medias[0]['_id'];
            const getProyecto = await this.noticiaModel
              .findOne({ _id: idNoticia })
              .session(session);
            let listaMedias = [];
            if (getProyecto.medias) {
              //Si tiene rol se le quita los roles que tiene
              for (const getMedia of getProyecto.medias) {
                listaMedias.push(getMedia['_id'].toString());
              }
              let index = listaMedias.indexOf(getIdMedia);
              //Eliminamos el elemento del array
              listaMedias.splice(index, 1);
            }

            // ______ datos medias _____
            let dataMedias: any = {
              medias: listaMedias,
              fechaActualizacion: new Date(),
            };

            //Se le quita la medias anteriores
            await this.noticiaModel.findByIdAndUpdate(
              idNoticia,
              dataMedias,
              opts,
            );
          }
        }

        let newHistoricoNoticiaUpdate: any = {
          datos: noticiaActualizar,
          usuario: noticiaActualizar?.perfil._id,
          accion: getAccionAc,
          entidad: codEntidadN,
        };
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoNoticiaUpdate,
        );

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        const updateNoticia = await this.obtenerNoticiaUnicaService.obtenerNoticiaUnica(
          idNoticia,
          noticiaActualizar.perfil._id,
          idiomaDetectado,
        );

        await this.traducirNoticiaSegundoPlanoService.traducirNoticiaSegundoPlano(
          idNoticia,
          idiomaDetectado,
          noticiaActualizar.perfil._id,
        );

        return updateNoticia;
      } else {
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async activarNoticiaUnica(perfil: any, opts: any): Promise<any> {
    try {
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      let getAccion = accion['codigo'];

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidad = entidad['codigo'];
      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstadoTNoticia = estado['codigo'];
      const estadoTE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstadoTNoticiaActiva = estadoTE['codigo'];

      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN['codigo'];
      //Obtiene el estado
      const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticia = estadoN['codigo'];
      const estadoNE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticiaActiva = estadoNE['codigo'];

      const noticias = await this.noticiaModel.find({
        $and: [
          { perfil: perfil },
          { estado: codigosHibernadoEntidades.noticiaHibernado },
        ],
      });

      if (noticias.length > 0) {
        let updateNoticia;
        for (const itrNoticia of noticias) {
          const dataNoticia = {
            estado: codEstadoNoticiaActiva,
          };
          updateNoticia = await this.noticiaModel.findByIdAndUpdate(
            itrNoticia._id,
            dataNoticia,
            opts,
          );
          const datosNoti = JSON.parse(JSON.stringify(updateNoticia));
          //eliminar parametro no necesarios
          delete datosNoti.fechaCreacion;
          delete datosNoti.fechaActualizacion;
          delete datosNoti.__v;

          let newHistoricoNoticia: any = {
            datos: datosNoti,
            usuario: perfil,
            accion: getAccion,
            entidad: entidad.codigo,
          };
          this.crearHistoricoService.crearHistoricoServer(newHistoricoNoticia);
        }

        return true;
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }
}
