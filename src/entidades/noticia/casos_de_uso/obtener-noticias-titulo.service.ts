import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { PaginateModel } from 'mongoose';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import {
  estadosNoticia,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { ObtenerPortadaService } from './obtener-portada-predeterminada.service';
import { TraducirNoticiasPerfilService } from './traducir-noticia.service';

@Injectable()
export class ObneterNoticiasTituloService {
  noticia: any;
  constructor(
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: PaginateModel<
      TraduccionesNoticias
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private obtenerPortada: ObtenerPortadaService,
    private traducirNoticiaService: TraducirNoticiasPerfilService,
    private readonly funcion: Funcion,
  ) {}

  async obtenerPorTitulo(
    codIdim: string,
    titulo: string,
    limite: number,
    pagina: number,
    response: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    if (!titulo) {
      return [];
    }
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      const estadoActAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoActAlbum = estadoActAlbum.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene la entidad traduccion Noticia
      const entidadTraduccionNoticia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidadTradNoticia = entidadTraduccionNoticia.codigo;

      //Obtiene el estado activa de la entidad traduccion proyecto
      const estadoTradNoticia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradNoticia,
      );
      let codEstadoTradNoticia = estadoTradNoticia.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      // let textoTraducido = await traducirTexto(idiomas.ingles, titulo);
      // console.log('textoTrducido: ', textoTraducido)

      //Obtiene el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );

      // let codIdioma = idioma.codigo;
      // const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      //   textoTraducido.idiomaDetectado,
      // );
      let codIdioma = idioma.codigo;

      const populateNoticia = {
        path: 'referencia',
        match: {
          estado: estadosNoticia.noticiaActiva,
        },
        select: '-__v',
        populate: [
          {
            path: 'adjuntos',
            select: '-fechaActualizacion -fechaCreacion -__v -estado -nombre ',
            match: { estado: codEstadoActAlbum },
            populate: [
              {
                path: 'media',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdioma, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: 'url tipo duracion fechaActualizacion ',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fechaActualizacion ',
                  },
                ],
              },
              {
                path: 'portada',
                match: { estado: codEstadoMedia },
                select:
                  'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
                populate: [
                  {
                    path: 'traducciones',
                    select: 'descripcion',
                    match: { idioma: codIdioma, estado: codEstadoTradMedia },
                  },
                  {
                    path: 'principal',
                    select: 'url tipo duracion fechaActualizacion ',
                  },
                  {
                    path: 'miniatura',
                    select: 'url tipo duracion fechaActualizacion ',
                  },
                ],
              },
            ],
          },
        ],
      };

      const options = {
        lean: true,
        session: opts.session,
        collation: { locale: 'en' },
        sort: { tituloCorto: 1 },
        select: ' -__v ',
        populate: [populateNoticia],
        page: Number(pagina),
        limit: Number(limite),
      };

      let noticias;

      //Obtener ultimo digito
      let ultimoCaracter = titulo.charAt(titulo.length - 1);
      //Obtener ultimo digito
      let penultimoCaracter = titulo.charAt(titulo.length - 2);

      let ultDosCracteres = penultimoCaracter.concat(ultimoCaracter);
      console.log('ultDosCracteres: ', ultDosCracteres);

      let eliminar_caracteres = [
        'as',
        'es',
        'is',
        'os',
        'us',
        'AS',
        'ES',
        'IS',
        'OS',
        'US',
      ];
      let eliminarS = ['s', 'S'];
      let nuevoTitulo;
      //Elimina la plural de los dos ultimos caracteres de la palabra que se envia
      for (const recorreCaracter of eliminar_caracteres) {
        if (recorreCaracter === ultDosCracteres) {
          nuevoTitulo = titulo.substring(0, titulo.length - 1);
          nuevoTitulo = nuevoTitulo.substring(0, nuevoTitulo.length - 1);
        }
      }

      let nuevoTitulo1;
      //Elimina la s (S) del ultimo caracter de la palabra que se envia
      for (const recorreCaracter of eliminarS) {
        if (recorreCaracter === ultimoCaracter) {
          nuevoTitulo1 = titulo.substring(0, titulo.length - 1);
        }
      }

      console.log('nuevoTitulo: ', nuevoTitulo);
      //Expresion regular de solo busqueda por palabras  completas
      let regex1 = '\\b'.concat(titulo).concat('\\b');
      console.log('regex1: ', regex1);

      let regex2;
      let regex3;
      if (nuevoTitulo) {
        //Expresion regular por inicio de palabras
        // regex2 = nuevoTitulo.concat("\\b");
        regex2 = '\\b'.concat(nuevoTitulo);
        console.log('regex2: ', regex2);

        //Expresion regular de solo busqueda por palabras  completas
        regex3 = '\\b'.concat(nuevoTitulo).concat('\\b');
        console.log('regex3: ', regex3);
      } else {
        //Expresion regular por inicio de palabras
        // regex2 = titulo.concat("\\b");
        regex2 = '\\b'.concat(titulo);
        console.log('regex2: ', regex2);

        //Expresion regular de solo busqueda por palabras  completas
        regex3 = '\\b'.concat(titulo).concat('\\b');
        console.log('regex3: ', regex3);
      }

      let regex5;
      let regex6;
      if (nuevoTitulo1) {
        //Expresion regular por inicio de palabras
        // regex5 = nuevoTitulo1.concat("\\b");
        regex5 = '\\b'.concat(nuevoTitulo1);
        console.log('regex5: ', regex5);

        //Expresion regular de solo busqueda por palabras  completas
        regex6 = '\\b'.concat(nuevoTitulo1).concat('\\b');
        console.log('regex6: ', regex6);
      } else {
        //Expresion regular por fin de palabras
        // regex5 = titulo.concat("\\b");
        regex5 = '\\b'.concat(titulo);
        console.log('regex5: ', regex5);

        //Expresion regular de solo busqueda por palabras  completas
        regex6 = '\\b'.concat(titulo).concat('\\b');
        console.log('regex6: ', regex6);
      }

      let getNotc = await this.traduccionNoticiaModel
        .find({
          $or: [
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex1),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { idioma: codIdioma },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex1), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { idioma: codIdioma }
            //   ]
            // },
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex1),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { original: true },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex1), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { original: true }
            //   ]
            // },
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex2),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { idioma: codIdioma },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex2), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { idioma: codIdioma }
            //   ]
            // },
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex2),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { original: true },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex2), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { original: true }
            //   ]
            // },
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex3),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { idioma: codIdioma },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex3), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { idioma: codIdioma }
            //   ]
            // },
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex3),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { original: true },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex3), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { original: true }
            //   ]
            // },
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex5),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { idioma: codIdioma },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex5), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { idioma: codIdioma }
            //   ]
            // },
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex5),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { original: true },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex5), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { original: true }
            //   ]
            // },
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex6),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { idioma: codIdioma },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex6), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { idioma: codIdioma }
            //   ]
            // },
            {
              $and: [
                {
                  tituloCorto: {
                    $regex: this.funcion.diacriticSensitiveRegex(regex6),
                    $options: 'i',
                  },
                },
                { estado: codEstadoTradNoticia },
                { original: true },
              ],
            },
            // {
            //   $and: [
            //     { titulo: { $regex: this.funcion.diacriticSensitiveRegex(regex6), $options: 'i' } },
            //     { estado: codEstadoTradNoticia },
            //     { original: true }
            //   ]
            // }
          ],
        })
        .populate({
          path: 'referencia',
          estado: estadosNoticia.noticiaActiva,
        });

      let arrayNotic = [];
      let arrayTradNotic = [];
      if (getNotc.length > 0) {
        for (const listNotic of getNotc) {
          if (listNotic?.referencia) {
            //Busca si la noticia ya esta en el array
            let obtProyect = arrayNotic.includes(
              listNotic.referencia['_id'].toString(),
            );

            if (obtProyect === false) {
              if (listNotic.idioma === codIdioma) {
                arrayTradNotic.push(listNotic._id);
              } else {
                let getTradUnica = await this.traduccionNoticiaModel.findOne({
                  referencia: listNotic.referencia['_id'],
                  idioma: codIdioma,
                  estado: codEstadoTradNoticia,
                });
                if (getTradUnica) {
                  arrayTradNotic.push(getTradUnica._id);
                } else {
                  await this.traducirNoticiaService.traducirNoticia(
                    listNotic.referencia['_id'],
                    codIdim,
                    null,
                    null,
                  );
                  let getIdTraducc = await this.traduccionNoticiaModel.findOne({
                    referencia: listNotic.referencia['_id'],
                    idioma: codIdioma,
                    estado: codEstadoTradNoticia,
                  });
                  arrayTradNotic.push(getIdTraducc._id);
                }
              }

              arrayNotic.push(listNotic.referencia['_id'].toString());
            } else {
              if (listNotic.idioma === codIdioma) {
                arrayTradNotic.push(listNotic._id);
              }
            }
          }
        }
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return getNotc;
      }

      //Busca por la traduccion original
      noticias = await this.traduccionNoticiaModel.paginate(
        {
          _id: { $in: arrayTradNotic },
        },
        options,
      );

      console.log('hay: "', noticias.docs.length, '" noticias');
      let arrayNoticias = [];
      const noticiasDocs: any = noticias.docs;
      for (const getNoticias of noticiasDocs) {
        console.log(
          'NOTICIA*********************************: ',
          getNoticias._id,
        );

        let arrayTraducciones = [];
        console.log('getNoticias.ID: ', getNoticias.referencia['_id']);
        console.log('getNoticias.idioma: ', getNoticias.idioma);
        console.log('codIdioma: ', codIdioma);
        if (getNoticias.idioma != codIdioma) {
          const getIdiomaNoticias = await this.traduccionNoticiaModel.findOne({
            $and: [
              { referencia: getNoticias.referencia['_id'] },
              { estado: codEstadoTradNoticia },
              { idioma: codIdioma },
            ],
          });

          // const find = getIdiomaNoticias.docs.find((element) => element.idioma === codIdioma)
          console.log('getIdiomaNoticias: ', getIdiomaNoticias);
          if (!getIdiomaNoticias) {
            console.log('debe traducir*********');
            arrayTraducciones = await this.traducirNoticiaService.traducirNoticia(
              getNoticias.referencia['_id'],
              codIdim,
              null,
              opts,
            );
          } else {
            let trad = {
              tituloCorto: getIdiomaNoticias.tituloCorto,
              titulo: getIdiomaNoticias.titulo,
              descripcion: getIdiomaNoticias.descripcion,
            };
            arrayTraducciones.push(trad);
          }
        } else {
          let trad = {
            tituloCorto: getNoticias.tituloCorto,
            titulo: getNoticias.titulo,
            descripcion: getNoticias.descripcion,
          };
          arrayTraducciones.push(trad);
        }

        let album = await this.obtenerPortada.obtenerPortada(
          getNoticias.referencia['adjuntos'],
          codIdim,
          opts,
        );

        this.noticia = {
          _id: getNoticias.referencia['_id'],
          traducciones: arrayTraducciones,
          adjuntos: album,
          estado: {
            codigo: getNoticias.referencia['estado'],
          },
          fechaCreacion: getNoticias.referencia['fechaCreacion'],
          fechaActualizacion: getNoticias.referencia['fechaActualizacion'],
        };

        // if (arrayNoticias.length === 0) {
        //   arrayNoticias.push(this.noticia);
        // }
        // if (arrayNoticias.length > 0) {
        //   let idIgual = false;
        //   for (const getArrayProyectos of arrayNoticias) {
        //     if (getArrayProyectos._id.toString() === getNoticias.referencia['_id'].toString()) {
        //       idIgual = true;
        //       break;
        //     }
        //   }
        //   if (!idIgual) {
        //     arrayNoticias.push(this.noticia);
        //   }
        // }
        // } else {
        //   console.log('No debe traducir*********')

        //   let album = await this.obtenerPortada.obtenerPortada(find.referencia['adjuntos'], codIdim, opts);

        //   let arrayTraducciones = [];
        //   let objTraducciones = {
        //     tituloCorto: find.tituloCorto,
        //     titulo: find.titulo,
        //     descripcion: find.descripcion
        //   };
        //   arrayTraducciones.push(objTraducciones)

        //   this.noticia = {
        //     _id: find.referencia['_id'],
        //     traducciones: arrayTraducciones,
        //     adjuntos: album,
        //     estado: {
        //       codigo: find.referencia['estado']
        //     },
        //     direccion: direccionNoticia,
        //     fechaCreacion: find.referencia['fechaCreacion'],
        //     fechaActualizacion: find.referencia['fechaActualizacion'],
        //   }
        //   console.log('arrayNoticias.length: ', arrayNoticias.length)
        //   if (arrayNoticias.length === 0) {
        //     arrayNoticias.push(this.noticia);
        //   }
        //   if (arrayNoticias.length > 0) {
        //     let idIgual = false;
        //     for (const getArrayProyectos of arrayNoticias) {
        //       if (getArrayProyectos._id.toString() === find.referencia['_id'].toString()) {

        //         idIgual = true;
        //         break;
        //       }
        //     }
        //     console.log('idIgual: ', idIgual)
        //     if (!idIgual) {
        //       arrayNoticias.push(this.noticia);
        //     }
        //   }
        // }
        arrayNoticias.push(this.noticia);
      }

      // if (arrayNoticias.length <= limite) {
      //   pagina = 1
      // } else {
      //   pagina = Math.round(arrayNoticias.length / limite)
      // }

      response.set(headerNombre.totalDatos, noticias.totalDocs);
      response.set(headerNombre.totalPaginas, noticias.totalPages);
      response.set(headerNombre.proximaPagina, noticias.hasNextPage);
      response.set(headerNombre.anteriorPagina, noticias.hasPrevPage);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return arrayNoticias;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
