import { Inject, Injectable } from '@nestjs/common';
import { Model, PaginateModel } from 'mongoose';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codIdiomas,
  codigoEntidades,
  codigosCatalogoAcciones,
  codigosEstadosTraduccionNoticia,
  idiomas,
} from 'src/shared/enum-sistema';
const mongoose = require('mongoose');

@Injectable()
export class TraducirNoticiaSegundoPlanoService {
  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: Model<TraduccionesNoticias>,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async traducirNoticiaSegundoPlano(
    idNoticia: string,
    codIdim: string,
    perfil: string,
  ) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const getTraductorOriginal = await this.traduccionNoticiaModel.findOne({
        referencia: idNoticia,
        original: true,
        estado: codigosEstadosTraduccionNoticia.activa,
      });

      if (getTraductorOriginal) {
        if (codIdim != idiomas.ingles) {
          await this.traducirNoticiaSegundoIngles(
            idNoticia,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (codIdim != idiomas.espanol) {
          await this.traducirNoticiaSegundoEspaniol(
            idNoticia,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (codIdim != idiomas.frances) {
          await this.traducirNoticiaSegundoFrances(
            idNoticia,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (codIdim != idiomas.aleman) {
          await this.traducirNoticiaSegundoAleman(
            idNoticia,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (codIdim != idiomas.italiano) {
          await this.traducirNoticiaSegundoItaliano(
            idNoticia,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (codIdim != idiomas.portugues) {
          await this.traducirNoticiaSegundoPortugues(
            idNoticia,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return true;
      }

      //Finaliza la transaccion
      await session.endSession();
      return true;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  //TRADUCCION NOTICIA AL INGLES
  async traducirNoticiaSegundoIngles(
    idNoticia: string,
    perfil: string,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let titulo: any;
      if (getTraductorOriginal.titulo) {
        let textoTraducidoTitulo = await traducirTexto(
          idiomas.ingles,
          getTraductorOriginal.titulo,
        );
        titulo = textoTraducidoTitulo.textoTraducido;
      }

      let tituloCorto: any;
      if (getTraductorOriginal.tituloCorto) {
        let textoTraducidoTituloCorto = await traducirTexto(
          idiomas.ingles,
          getTraductorOriginal.tituloCorto,
        );
        tituloCorto = textoTraducidoTituloCorto.textoTraducido;
      }

      let descripcion: any;
      if (getTraductorOriginal.descripcion) {
        let textoTraducidoDescripcion = await traducirTexto(
          idiomas.ingles,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTraducidoDescripcion.textoTraducido;
      }

      // let textoUnido = getTraductorOriginal.tituloCorto + ' [-]' + getTraductorOriginal.titulo +
      //     ' [-]' + getTraductorOriginal.descripcion;

      // let textoTrducido = await traducirTexto(idiomas.ingles, textoUnido);
      // console.log('textoTrducido: ', textoTrducido)
      // const textS = textoTrducido.textoTraducido.split('[-]');

      // const T = this.gestionNoticiaService.detectarIdioma(idiomas.ingles);
      // const titu = textS[0].trim().split(' ');
      // const tagsTitulo = sw.removeStopwords(titu, T);

      // const tituCort = textS[1].trim().split(' ');

      // const tagsTituloCort = sw.removeStopwords(tituCort, T);

      // const tagsUnidos = tagsTitulo.concat(tagsTituloCort);

      // const tags = tagsUnidos.filter(function (item, index, array) {
      //     return array.indexOf(item) === index;
      // })

      let newTraduccionNoticia = {
        tituloCorto: tituloCorto || null,
        titulo: titulo || null,
        descripcion: descripcion || null,
        // tags: tags,
        idioma: codIdiomas.ingles,
        original: false,
        estado: codigosEstadosTraduccionNoticia.activa,
        referencia: idNoticia,
      };

      const crearTraduccionNoticia = await new this.traduccionNoticiaModel(
        newTraduccionNoticia,
      );
      const crearTraduccion = await crearTraduccionNoticia.save(opts);

      const dataNoticia = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataNoticia.__v;

      let newHistoricoTraduccionNoticia: any = {
        datos: dataNoticia,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionNoticia,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionNoticia,
      );

      await this.noticiaModel.updateOne(
        { _id: idNoticia },
        { $push: { traducciones: crearTraduccionNoticia._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION NOTICIA AL ITALIANO
  async traducirNoticiaSegundoItaliano(
    idNoticia: string,
    perfil: string,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let titulo: any;
      if (getTraductorOriginal.titulo) {
        let textoTraducidoTitulo = await traducirTexto(
          idiomas.italiano,
          getTraductorOriginal.titulo,
        );
        titulo = textoTraducidoTitulo.textoTraducido;
      }

      let tituloCorto: any;
      if (getTraductorOriginal.tituloCorto) {
        let textoTraducidoTituloCorto = await traducirTexto(
          idiomas.italiano,
          getTraductorOriginal.tituloCorto,
        );
        tituloCorto = textoTraducidoTituloCorto.textoTraducido;
      }

      let descripcion: any;
      if (getTraductorOriginal.descripcion) {
        let textoTraducidoDescripcion = await traducirTexto(
          idiomas.italiano,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTraducidoDescripcion.textoTraducido;
      }

      let newTraduccionNoticia = {
        tituloCorto: tituloCorto || null,
        titulo: titulo || null,
        descripcion: descripcion || null,
        // tags: tags,
        idioma: codIdiomas.italiano,
        original: false,
        estado: codigosEstadosTraduccionNoticia.activa,
        referencia: idNoticia,
      };

      const crearTraduccionNoticia = await new this.traduccionNoticiaModel(
        newTraduccionNoticia,
      );
      const crearTraduccion = await crearTraduccionNoticia.save(opts);

      const dataNoticia = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataNoticia.__v;

      let newHistoricoTraduccionNoticia: any = {
        datos: dataNoticia,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionNoticia,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionNoticia,
      );

      await this.noticiaModel.updateOne(
        { _id: idNoticia },
        { $push: { traducciones: crearTraduccionNoticia._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION NOTICIA AL FRANCES
  async traducirNoticiaSegundoFrances(
    idNoticia: string,
    perfil: string,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let titulo: any;
      if (getTraductorOriginal.titulo) {
        let textoTraducidoTitulo = await traducirTexto(
          idiomas.frances,
          getTraductorOriginal.titulo,
        );
        titulo = textoTraducidoTitulo.textoTraducido;
      }

      let tituloCorto: any;
      if (getTraductorOriginal.tituloCorto) {
        let textoTraducidoTituloCorto = await traducirTexto(
          idiomas.frances,
          getTraductorOriginal.tituloCorto,
        );
        tituloCorto = textoTraducidoTituloCorto.textoTraducido;
      }

      let descripcion: any;
      if (getTraductorOriginal.descripcion) {
        let textoTraducidoDescripcion = await traducirTexto(
          idiomas.frances,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTraducidoDescripcion.textoTraducido;
      }

      let newTraduccionNoticia = {
        tituloCorto: tituloCorto || null,
        titulo: titulo || null,
        descripcion: descripcion || null,
        // tags: tags,
        idioma: codIdiomas.frances,
        original: false,
        estado: codigosEstadosTraduccionNoticia.activa,
        referencia: idNoticia,
      };

      const crearTraduccionNoticia = await new this.traduccionNoticiaModel(
        newTraduccionNoticia,
      );
      const crearTraduccion = await crearTraduccionNoticia.save(opts);

      const dataNoticia = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataNoticia.__v;

      let newHistoricoTraduccionNoticia: any = {
        datos: dataNoticia,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionNoticia,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionNoticia,
      );

      await this.noticiaModel.updateOne(
        { _id: idNoticia },
        { $push: { traducciones: crearTraduccionNoticia._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION NOTICIA AL ALEMAN
  async traducirNoticiaSegundoAleman(
    idNoticia: string,
    perfil: string,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let titulo: any;
      if (getTraductorOriginal.titulo) {
        let textoTraducidoTitulo = await traducirTexto(
          idiomas.aleman,
          getTraductorOriginal.titulo,
        );
        titulo = textoTraducidoTitulo.textoTraducido;
      }

      let tituloCorto: any;
      if (getTraductorOriginal.tituloCorto) {
        let textoTraducidoTituloCorto = await traducirTexto(
          idiomas.aleman,
          getTraductorOriginal.tituloCorto,
        );
        tituloCorto = textoTraducidoTituloCorto.textoTraducido;
      }

      let descripcion: any;
      if (getTraductorOriginal.descripcion) {
        let textoTraducidoDescripcion = await traducirTexto(
          idiomas.aleman,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTraducidoDescripcion.textoTraducido;
      }

      let newTraduccionNoticia = {
        tituloCorto: tituloCorto || null,
        titulo: titulo || null,
        descripcion: descripcion || null,
        // tags: tags,
        idioma: codIdiomas.aleman,
        original: false,
        estado: codigosEstadosTraduccionNoticia.activa,
        referencia: idNoticia,
      };

      const crearTraduccionNoticia = await new this.traduccionNoticiaModel(
        newTraduccionNoticia,
      );
      const crearTraduccion = await crearTraduccionNoticia.save(opts);

      const dataNoticia = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataNoticia.__v;

      let newHistoricoTraduccionNoticia: any = {
        datos: dataNoticia,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionNoticia,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionNoticia,
      );

      await this.noticiaModel.updateOne(
        { _id: idNoticia },
        { $push: { traducciones: crearTraduccionNoticia._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION NOTICIA AL PORTUGUES
  async traducirNoticiaSegundoPortugues(
    idNoticia: string,
    perfil: string,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let titulo: any;
      if (getTraductorOriginal.titulo) {
        let textoTraducidoTitulo = await traducirTexto(
          idiomas.portugues,
          getTraductorOriginal.titulo,
        );
        titulo = textoTraducidoTitulo.textoTraducido;
      }

      let tituloCorto: any;
      if (getTraductorOriginal.tituloCorto) {
        let textoTraducidoTituloCorto = await traducirTexto(
          idiomas.portugues,
          getTraductorOriginal.tituloCorto,
        );
        tituloCorto = textoTraducidoTituloCorto.textoTraducido;
      }

      let descripcion: any;
      if (getTraductorOriginal.descripcion) {
        let textoTraducidoDescripcion = await traducirTexto(
          idiomas.portugues,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTraducidoDescripcion.textoTraducido;
      }

      let newTraduccionNoticia = {
        tituloCorto: tituloCorto || null,
        titulo: titulo || null,
        descripcion: descripcion || null,
        // tags: tags,
        idioma: codIdiomas.portugues,
        original: false,
        estado: codigosEstadosTraduccionNoticia.activa,
        referencia: idNoticia,
      };

      const crearTraduccionNoticia = await new this.traduccionNoticiaModel(
        newTraduccionNoticia,
      );
      const crearTraduccion = await crearTraduccionNoticia.save(opts);

      const dataNoticia = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataNoticia.__v;

      let newHistoricoTraduccionNoticia: any = {
        datos: dataNoticia,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionNoticia,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionNoticia,
      );

      await this.noticiaModel.updateOne(
        { _id: idNoticia },
        { $push: { traducciones: crearTraduccionNoticia._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION NOTICIA AL ESPAÑOL
  async traducirNoticiaSegundoEspaniol(
    idNoticia: string,
    perfil: string,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let titulo: any;
      if (getTraductorOriginal.titulo) {
        let textoTraducidoTitulo = await traducirTexto(
          idiomas.espanol,
          getTraductorOriginal.titulo,
        );
        titulo = textoTraducidoTitulo.textoTraducido;
      }

      let tituloCorto: any;
      if (getTraductorOriginal.tituloCorto) {
        let textoTraducidoTituloCorto = await traducirTexto(
          idiomas.espanol,
          getTraductorOriginal.tituloCorto,
        );
        tituloCorto = textoTraducidoTituloCorto.textoTraducido;
      }

      let descripcion: any;
      if (getTraductorOriginal.descripcion) {
        let textoTraducidoDescripcion = await traducirTexto(
          idiomas.espanol,
          getTraductorOriginal.descripcion,
        );
        descripcion = textoTraducidoDescripcion.textoTraducido;
      }

      let newTraduccionNoticia = {
        tituloCorto: tituloCorto || null,
        titulo: titulo || null,
        descripcion: descripcion || null,
        // tags: tags,
        idioma: codIdiomas.espanol,
        original: false,
        estado: codigosEstadosTraduccionNoticia.activa,
        referencia: idNoticia,
      };

      const crearTraduccionNoticia = await new this.traduccionNoticiaModel(
        newTraduccionNoticia,
      );
      const crearTraduccion = await crearTraduccionNoticia.save(opts);

      const dataNoticia = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataNoticia.__v;

      let newHistoricoTraduccionNoticia: any = {
        datos: dataNoticia,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTraduccionNoticia,
      };
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionNoticia,
      );

      await this.noticiaModel.updateOne(
        { _id: idNoticia },
        { $push: { traducciones: crearTraduccionNoticia._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
