import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { PaginateModel } from 'mongoose';
import * as mongoose from 'mongoose';
import { EliminarAlbumService } from '../../album/casos_de_uso/eliminar-album.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class EliminarNoticiasUnicaService {
  noticia: any;

  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: Model<TraduccionesNoticias>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private eliminarAlbumService: EliminarAlbumService,
  ) {}

  async eliminarNoticiaUnica(
    perfil: any,
    idNoticia: string,
    opts?: any,
  ): Promise<any> {
    let session;
    let optsLocal;
    if (!opts) {
      session = await mongoose.startSession();
      await session.startTransaction();
    }

    try {
      if (!opts) {
        opts = { session };
        optsLocal = true;
      }

      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidad = entidad.codigo;
      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstadoTNoticia = estado.codigo;

      const estadoTE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidad,
      );
      let codEstadoTNoticiaEli = estadoTE.codigo;

      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN.codigo;

      const estadoNE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadN,
      );
      let codEstadoNoticiaEli = estadoNE.codigo;

      const estadoNAct = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticiaAct = estadoNAct.codigo;

      const noticia = await this.noticiaModel
        .findOne({
          $and: [
            { _id: idNoticia },
            { perfil: perfil },
            { estado: codEstadoNoticiaAct },
          ],
        })
        .populate([{ path: 'adjuntos' }]);

      if (noticia) {
        // eliminar album de adjuntos
        for (const albm of noticia.adjuntos) {
          const dataAlbum = {
            _id: albm['_id'],
            usuario: perfil,
          };
          await this.eliminarAlbumService.eliminarAlbum(dataAlbum, false, opts);
        }

        const updateTradu = await this.traduccionNoticiaModel.updateMany(
          { referencia: idNoticia },
          { $set: { estado: codEstadoTNoticiaEli } },
          opts,
        );

        let newHistoricoTNoticia: any = {
          datos: codEstadoTNoticiaEli,
          usuario: perfil,
          accion: getAccion,
          entidad: entidad.codigo,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistoricoTNoticia);

        // await this.noticiaModel.updateOne({ _id: idNoticia }, { $set: { estado: codEstadoNoticiaEli }, fechaActualizacion:new Date()}, opts);
        await this.noticiaModel.updateOne(
          { _id: idNoticia },
          {
            $set: {
              estado: codEstadoNoticiaEli,
              fechaActualizacion: new Date(),
            },
          },
          opts,
        );
        let newHistoricoNoticia: any = {
          datos: await this.noticiaModel
            .findOne({ _id: idNoticia })
            .session(opts.session)
            .select('-fechaCreacion -__v'),
          usuario: perfil,
          accion: getAccion,
          entidad: entidad.codigo,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistoricoNoticia);

        if (optsLocal) {
          await session.commitTransaction();
          await session.endSession();
        }

        return true;
      } else {
        if (optsLocal) {
          await session.endSession();
        }
        return noticia;
      }
    } catch (error) {
      if (optsLocal) {
        await session.abortTransaction();
        await session.endSession();
      }
      throw error;
    }
  }

  async hibernarNoticia(
    perfil: any,
    idNoticia: string,
    opts: any,
  ): Promise<any> {
    let estadoProyecto: any = '';
    let accion: any = '';

    try {
      //Obtiene el codigo de la accion
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      //Obtiene el codigo de la entidad noticias
      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadNoticia = entidadN.codigo;

      //Obtiene el codigo del estado activa de la entidad noticias
      const estadoNot = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadNoticia,
      );
      let codEstadoActNoticia = estadoNot.codigo;

      //Obtiene el codigo del estado eliminado de la entidad noticias
      const estadoNE = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        codEntidadNoticia,
      );
      let codEstadoEliProyec = estadoNE.codigo;

      //Obtiene el codigo del estado HIBERNADO de la entidad noticias
      const estadoPHiber = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.hibernado,
        codEntidadNoticia,
      );
      let codEstadoHiberProyecto = estadoPHiber.codigo;

      const noticia = await this.noticiaModel
        .findOne({
          $and: [
            { _id: idNoticia },
            { perfil: perfil },
            { estado: codEstadoActNoticia },
          ],
        })
        .session(opts.session);

      estadoProyecto = codEstadoHiberProyecto;
      accion = accionModificar.codigo;

      if (noticia) {
        //Actualiza el estado a hibernado del noticia
        await this.noticiaModel.updateOne(
          { _id: idNoticia },
          { $set: { estado: estadoProyecto, fechaActualizacion: new Date() } },
          opts,
        );

        let datosModNoti = {
          _id: idNoticia,
          perfil: perfil,
          noticia: await this.noticiaModel
            .findOne({ _id: idNoticia })
            .session(opts.session)
            .select('-fechaActualizacion -fechaCreacion -__v'),
        };

        let newHistoricoProyecto: any = {
          datos: datosModNoti,
          usuario: perfil,
          accion: accion,
          entidad: codEntidadNoticia,
        };
        //Crea el historico del noticia
        this.crearHistoricoService.crearHistoricoServer(newHistoricoProyecto);

        return true;
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }
}
