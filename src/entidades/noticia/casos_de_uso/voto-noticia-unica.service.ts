import { VotoNoticiaDto } from './../entidad/voto-noticia.dto';
import { TraduccionesVotoNoticia } from './../../../drivers/mongoose/interfaces/traduccion_voto_noticia/traduccion-voto-noticia.interface';
import { VotoNoticia } from '../../../drivers/mongoose/interfaces/voto_noticia/voto-noticia.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import * as mongoose from 'mongoose';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class VotoNoticiaUnicaService {
  noticia: any;
  constructor(
    @Inject('VOTO_NOTICIA_MODEL')
    private readonly votoNoticiaModel: Model<VotoNoticia>,
    @Inject('TRADUCCION_VOTO_NOTICIA_MODEL')
    private readonly traduccionVotoNoticiaModel: Model<TraduccionesVotoNoticia>,
    @Inject('NOTICIA_MODEL') private readonly noticiaModel: Model<Noticia>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async votoNoticiaUnica(
    votoNoticia: VotoNoticiaDto,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdiom,
      );
      let codIdioma = idioma.codigo;

      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad
      const entidadVotoNoticia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.votoNoticia,
      );
      let codEntidadVotoNoticia = entidadVotoNoticia.codigo;

      const entidadTraduccionVotoNoticia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionVotoNoticia,
      );
      let codEntidadTraduccionVotoNoticia = entidadTraduccionVotoNoticia.codigo;

      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN.codigo;

      //Obtiene el estado
      const estadoVotoNotocia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadVotoNoticia,
      );
      let codEstadoVotoNoticia = estadoVotoNotocia.codigo;

      const estadoTraducVotoNotocia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccionVotoNoticia,
      );
      let codEstadoTraducVotoNoticia = estadoTraducVotoNotocia.codigo;

      const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticia = estadoN.codigo;

      const noticia = await this.noticiaModel
        .findOne({
          $and: [
            { _id: votoNoticia.noticia._id },
            { estado: codEstadoNoticia },
          ],
        })
        .populate({ path: 'votos', select: 'perfil' });

      if (noticia) {
        if (noticia.votos.length > 0) {
          const find = noticia.votos.filter(
            (element: any) => element.perfil == votoNoticia.perfil._id,
          );

          if (find.length > 0) {
            return null;
          }
        }

        const idVotoNoticia = new mongoose.mongo.ObjectId();
        let crearTraduccionVotoNoticia;
        let traducciones = [];
        if (votoNoticia.traducciones.length > 0) {
          // let textoTraducido = await traducirTexto(codIdiom, votoNoticia.descripcion);

          let traduccionVotoNoticia = {
            descripcion: votoNoticia.traducciones[0].descripcion,
            idioma: codIdioma,
            original: true,
            referencia: idVotoNoticia,
            estado: codEstadoTraducVotoNoticia,
          };

          crearTraduccionVotoNoticia = await new this.traduccionVotoNoticiaModel(
            traduccionVotoNoticia,
          ).save(opts);

          const dataNoticia = JSON.parse(
            JSON.stringify(crearTraduccionVotoNoticia),
          );
          delete dataNoticia.fechaCreacion;
          delete dataNoticia.fechaActualizacion;
          delete dataNoticia.__v;
          let historicoTraduccionVotoNoticia: any = {
            datos: dataNoticia,
            usuario: votoNoticia.perfil._id,
            accion: getAccion,
            entidad: codEntidadTraduccionVotoNoticia,
          };
          this.crearHistoricoService.crearHistoricoServer(
            historicoTraduccionVotoNoticia,
          );

          console.log(
            'crearTraduccionVotoNoticia: ',
            crearTraduccionVotoNoticia,
          );
          traducciones.push(crearTraduccionVotoNoticia._id);
        }
        //Total de votos actuales
        const totalVotos = noticia.totalVotos;

        await this.noticiaModel.updateOne(
          { _id: votoNoticia.noticia._id },
          { $set: { totalVotos: totalVotos + 1 } },
          opts,
        );

        let newHistoricoNoticia: any = {
          datos: totalVotos + 1,
          usuario: votoNoticia.perfil._id,
          accion: getAccion,
          entidad: codEntidadN,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistoricoNoticia);

        let nuevoVotoNoticia = {
          _id: idVotoNoticia,
          estado: codEstadoVotoNoticia,
          perfil: votoNoticia.perfil._id,
          traducciones: traducciones,
          noticia: votoNoticia.noticia._id,
        };

        const crearVotoNoticia = await new this.votoNoticiaModel(
          nuevoVotoNoticia,
        ).save(opts);

        const dataVotoNoticia = JSON.parse(JSON.stringify(crearVotoNoticia));
        delete dataVotoNoticia.__v;
        let historicoVotoNoticia: any = {
          datos: dataVotoNoticia,
          usuario: votoNoticia.perfil._id,
          accion: getAccion,
          entidad: codEntidadVotoNoticia,
        };
        this.crearHistoricoService.crearHistoricoServer(historicoVotoNoticia);

        await this.noticiaModel.updateOne(
          { _id: votoNoticia.noticia._id },
          { $push: { votos: crearVotoNoticia._id } },
          opts,
        );

        let historicoUpdateNoticia: any = {
          datos: crearVotoNoticia._id,
          usuario: votoNoticia.perfil._id,
          accion: getAccion,
          entidad: codEntidadN,
        };
        this.crearHistoricoService.crearHistoricoServer(historicoUpdateNoticia);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return crearVotoNoticia;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
