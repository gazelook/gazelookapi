import { Injectable } from '@nestjs/common';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';

@Injectable()
export class ObtenerMediasNoticiasService {
  constructor(private traduccionMediaService: TraduccionMediaService) {}

  async obtenerMediasNoticias(
    medias: any,
    codIdioma: string,
    opts: any,
  ): Promise<any> {
    try {
      if (medias.length > 0) {
        let media = [];
        for (const getMedias of medias) {
          let objMiniatura: any;
          if (getMedias.miniatura) {
            objMiniatura = {
              _id: getMedias.miniatura._id,
              url: getMedias.miniatura.url,
              tipo: {
                codigo: getMedias.miniatura.tipo,
              },
              fileDefault: getMedias.miniatura.fileDefault,
              fechaActualizacion: getMedias.miniatura.fechaActualizacion,
            };
          }
          let objMedias: any = {
            _id: getMedias._id,
            catalogoMedia: {
              codigo: getMedias.catalogoMedia,
            },
            principal: {
              _id: getMedias.principal._id,
              url: getMedias.principal.url,
              tipo: {
                codigo: getMedias.principal.tipo,
              },
              fileDefault: getMedias.principal.fileDefault,
              fechaActualizacion: getMedias.principal.fechaActualizacion,
            },
            miniatura: objMiniatura,
            fechaCreacion: getMedias.fechaCreacion,
            fechaActualizacion: getMedias.fechaActualizacion,
          };

          if (getMedias.traducciones[0] === undefined) {
            console.log('debe traducir descripcion media');

            const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
              getMedias._id,
              codIdioma,
              opts,
            );
            if (traduccionMedia) {
              const traducciones = [
                {
                  _id: traduccionMedia._id,
                  descripcion: traduccionMedia.descripcion,
                },
              ];

              objMedias.traducciones = traducciones;
            } else {
              objMedias.traducciones = [];
            }
          } else {
            let arrayTradMedias = [];
            arrayTradMedias.push(objMedias.traducciones[0]);
            objMedias.traducciones = arrayTradMedias;
          }
          media.push(objMedias);
        }
        return media;
      }
      return [];
    } catch (error) {
      throw error;
    }
  }
}
