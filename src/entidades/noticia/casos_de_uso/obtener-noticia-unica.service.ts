import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { PaginateModel } from 'mongoose';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerDireccionService } from 'src/entidades/direccion/casos_de_uso/obtener-direccion.service';
import { TraducirDireccionService } from 'src/entidades/direccion/casos_de_uso/traducir-direccion.service';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { ObtenerAlbumPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil.service';
import {
  codidosEstadosTraduccionDireccion,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CalcularDiasNoticiaService } from './carcular-dias-noticia.service';
import { ObtenerLocalidadNoticiaService } from './obtener-localidad-noticia.service';
import { ObtenerMediasNoticiasService } from './obtener-medias-noticias.service';
import { TraducirNoticiasPerfilService } from './traducir-noticia.service';

@Injectable()
export class ObtenerNoticiaUnicaService {
  noticia: any;

  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirNoticiaService: TraducirNoticiasPerfilService,
    private obtenerLocalidadNoticiaService: ObtenerLocalidadNoticiaService,
    private traduccionMediaService: TraduccionMediaService,
    private obtenerMediasNoticiasService: ObtenerMediasNoticiasService,
    private calcularDiasNoticiaService: CalcularDiasNoticiaService,
    private traducirDireccionService: TraducirDireccionService,
    private obtenerDireccionService: ObtenerDireccionService,
    private obtenerAlbumPerfilService: ObtenerAlbumPerfilService,
  ) {}

  async obtenerNoticiaUnica(
    idNoticia: string,
    idPerfil: string,
    codIdioma: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdioma,
      );
      let codIdi = idioma.codigo;

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionNoticia,
      );
      let codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      let codEstadoTNoticia = estado.codigo;

      const entidadN = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.noticias,
      );
      let codEntidadN = entidadN.codigo;

      //Obtiene el estado
      const estadoN = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadN,
      );
      let codEstadoNoticia = estadoN.codigo;

      let perfilPropietario = false;
      let getNoticiaPropietario = await this.noticiaModel.findOne({
        _id: idNoticia,
        estado: codEstadoNoticia,
        perfil: idPerfil,
      });
      if (getNoticiaPropietario) {
        perfilPropietario = true;
      }

      let noticia = await this.verificarTraduccion(
        idNoticia,
        codIdi,
        codIdioma,
        codEstadoTNoticia,
        codEstadoNoticia,
        idPerfil,
        opts,
        session,
        perfilPropietario,
        false,
      );

      if (noticia) {
        let votoBo: any;
        let getAdjuntos: any;
        let objAdjun: any;
        let adjun = [];
        let actu = false;

        if (
          noticia.fechaCreacion.toString() ==
          noticia.fechaActualizacion.toString()
        ) {
          actu = false;
        } else {
          let dias = this.calcularDiasNoticiaService.calcularDias(
            new Date(),
            noticia.fechaActualizacion,
          );
          if (dias) actu = true;
        }

        if (noticia.votos.length > 0) {
          for (const votos of noticia.votos) {
            if (votos.perfil) {
              if (votos.perfil._id == idPerfil) {
                votoBo = true;
                break;
              } else {
                votoBo = false;
              }
            }
          }
        } else {
          votoBo = false;
        }

        //Verifica si la noticia tiene adjuntos
        if (noticia.adjuntos.length > 0) {
          for (const album of noticia.adjuntos) {
            let media = [];
            let objPortada: any;
            //Verifica si el album tiene portada
            if (album.portada) {
              //Verifica si la media tiene miniatura
              let obMiniaturaPort: any;
              if (album.portada.miniatura) {
                //Objeto de tipo miniatura (archivo)
                obMiniaturaPort = {
                  _id: album.portada.miniatura._id,
                  url: album.portada.miniatura.url,
                  tipo: {
                    codigo: album.portada.miniatura.tipo,
                  },
                  fileDefault: album.portada.miniatura.fileDefault,
                  fechaActualizacion:
                    album.portada.miniatura.fechaActualizacion,
                };

                if (album.portada.miniatura.duracion) {
                  obMiniaturaPort.duracion = album.portada.miniatura.duracion;
                }
              }
              //Objeto de tipo portada(media)
              objPortada = {
                _id: album.portada._id,
                catalogoMedia: {
                  codigo: album.portada.catalogoMedia,
                },
                principal: {
                  _id: album.portada.principal._id,
                  url: album.portada.principal.url,
                  tipo: {
                    codigo: album.portada.principal.tipo,
                  },
                  fileDefault: album.portada.principal.fileDefault,
                  fechaActualizacion:
                    album.portada.principal.fechaActualizacion,
                },
                miniatura: obMiniaturaPort,
                fechaCreacion: album.portada.fechaCreacion,
                fechaActualizacion: album.portada.fechaActualizacion,
              };

              if (album.portada.principal.duracion) {
                obMiniaturaPort.principal.duracion =
                  album.portada.principal.duracion;
              }
              //traducir descripcion de la media
              if (album.portada.traducciones.length === 0) {
                const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                  album.portada._id,
                  codIdioma,
                  opts,
                );

                if (traduccionMedia) {
                  const traducciones = [
                    {
                      _id: traduccionMedia._id,
                      descripcion: traduccionMedia.descripcion,
                    },
                  ];

                  objPortada.traducciones = traducciones;
                } else {
                  objPortada.traducciones = [];
                }
              } else {
                let arrayTradPortada = [];
                arrayTradPortada.push(album.portada.traducciones[0]);
                objPortada.traducciones = album.portada.traducciones[0];
              }
            }

            //Verifica si el album tiene medias
            if (album.media.length > 0) {
              for (const getMedia of album.media) {
                let obMiniatura: any;

                //Verifica si la media tiene miniatura
                if (getMedia.miniatura) {
                  //Objeto de tipo miniatura (archivo)
                  obMiniatura = {
                    _id: getMedia.miniatura._id,
                    url: getMedia.miniatura.url,
                    tipo: {
                      codigo: getMedia.miniatura.tipo,
                    },
                    fileDefault: getMedia.miniatura.fileDefault,
                    fechaActualizacion: getMedia.miniatura.fechaActualizacion,
                  };

                  if (getMedia.miniatura.duracion) {
                    obMiniatura.duracion = getMedia.miniatura.duracion;
                  }
                }
                //Objeto de tipo media
                let objMedia: any = {
                  _id: getMedia._id,
                  catalogoMedia: {
                    codigo: getMedia.catalogoMedia,
                  },
                  principal: {
                    _id: getMedia.principal._id,
                    url: getMedia.principal.url,
                    tipo: {
                      codigo: getMedia.principal.tipo,
                    },
                    fileDefault: getMedia.principal.fileDefault,
                    fechaActualizacion: getMedia.principal.fechaActualizacion,
                  },
                  miniatura: obMiniatura,
                  fechaCreacion: getMedia.fechaCreacion,
                  fechaActualizacion: getMedia.fechaActualizacion,
                };

                if (getMedia.principal.duracion) {
                  objMedia.principal.duracion = getMedia.principal.duracion;
                }

                //traducir descripcion de la media
                if (getMedia.traducciones[0] === undefined) {
                  const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                    getMedia._id,
                    codIdioma,
                    opts,
                  );

                  if (traduccionMedia) {
                    const traducciones = [
                      {
                        _id: traduccionMedia._id,
                        descripcion: traduccionMedia.descripcion,
                      },
                    ];

                    objMedia.traducciones = traducciones;
                  } else {
                    objMedia.traducciones = [];
                  }
                } else {
                  let arrayTradMedia = [];
                  arrayTradMedia.push(getMedia.traducciones[0]);
                  objMedia.traducciones = arrayTradMedia;
                }
                media.push(objMedia);
              }
            }

            //Objeto de tipo adjunto (album)
            objAdjun = {
              _id: album._id,
              tipo: {
                codigo: album.tipo,
              },
              traducciones: album.traducciones,
              predeterminado: album.predeterminado,
              portada: objPortada,
              media: media,
            };
            adjun.push(objAdjun);
          }
        }

        getAdjuntos = adjun;

        //Data de medias
        //Llama al metodo del obtener medias
        let getMedia: any;
        if (noticia.medias.length > 0) {
          getMedia = await this.obtenerMediasNoticiasService.obtenerMediasNoticias(
            noticia.medias,
            codIdioma,
            opts,
          );
        } else {
          getMedia = noticia.medias;
        }

        //Data de traducciones de la noticia
        // let traduccionNoticia = []
        // //Objeto de tipo traducccion noticia
        // let trad = {
        //   tituloCorto: noticia.traducciones[0].tituloCorto,
        //   titulo: noticia.traducciones[0].titulo,
        //   descripcion: noticia.traducciones[0].descripcion
        // }
        // traduccionNoticia.push(trad)

        //Obtiene la localidad segun el codigo enviado
        // const getLocalidad = await this.obtenerLocalidadNoticiaService.obtenerLocalidadNoticia(noticia.localidad, codIdioma)

        let objDireccion: any;
        if (noticia.direccion) {
          //Objeto de tipo direccion
          objDireccion = {
            _id: noticia.direccion['_id'],
          };

          if (noticia.direccion.localidad) {
            //Obtiene la localidad segun el codigo enviado
            const getLocalidad = await this.obtenerLocalidadNoticiaService.obtenerLocalidadNoticia(
              noticia.direccion.localidad,
              codIdioma,
            );
            objDireccion.localidad = getLocalidad;
          }
          let traduccionDireccion = [];

          if (noticia.direccion.traducciones.length > 0) {
            //Si la traduccion no es la original
            if (!noticia.direccion.traducciones[0].original) {
              let obtTraduccionDireccion = {
                descripcion: noticia.direccion.traducciones[0].descripcion,
                idioma: noticia.direccion.traducciones[0].idioma,
                original: noticia.direccion.traducciones[0].original,
              };
              traduccionDireccion.push(obtTraduccionDireccion);

              //Obtiene traduccion original
              let getTraDirecOriginal = await this.obtenerDireccionService.obtenerDireccionTraduccionOriginal(
                noticia.direccion._id,
                opts,
              );
              traduccionDireccion.push(getTraDirecOriginal);

              objDireccion.traducciones = traduccionDireccion;
            } else {
              let obtTraduccionDireccion = {
                descripcion: noticia.direccion.traducciones[0].descripcion,
                idioma: noticia.direccion.traducciones[0].idioma,
                original: noticia.direccion.traducciones[0].original,
              };
              traduccionDireccion.push(obtTraduccionDireccion);

              objDireccion.traducciones = traduccionDireccion;
            }
          } else {
            // traduce la direccion
            let getTradDir = await this.traducirDireccionService.traducirDireccion(
              noticia.direccion._id,
              codIdioma,
              opts,
            );
            if (getTradDir.length > 0) {
              objDireccion.traducciones = getTradDir;
              //Obtiene traduccion original
              let getTraDirecOriginal = await this.obtenerDireccionService.obtenerDireccionTraduccionOriginal(
                noticia.direccion._id,
                opts,
              );

              objDireccion.traducciones.push(getTraDirecOriginal);
            }
          }
        }

        let getAlbumPerfil = await this.obtenerAlbumPerfilService.obtenerAlbumPerfil(
          noticia.perfil['_id'],
        );
        // getAlbumPerfil._id = intercambio.perfil['_id']
        let dataPerfil = {
          _id: noticia.perfil['_id'],
          nombreContacto: noticia.perfil['nombreContacto'],
          nombreContactoTraducido: noticia.perfil['nombreContactoTraducido'],
          album: [getAlbumPerfil.album],
        };

        const datos = {
          _id: noticia._id,
          traducciones: noticia.traducciones,
          autor: noticia.autor,
          adjuntos: getAdjuntos,
          medias: getMedia,
          totalVotos: noticia.totalVotos,
          perfil: dataPerfil,
          direccion: objDireccion || null,
          votos: noticia.votos,
          fechaCreacion: noticia.fechaCreacion,
          fechaActualizacion: noticia.fechaActualizacion,
          estado: {
            codigo: noticia.estado,
          },
          actualizado: actu,
          voto: votoBo,
        };

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return datos;
      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async verificarTraduccion(
    idNoticia,
    codIdioma,
    codIdim,
    codEstadoTNoticia,
    codEstadoNoticia,
    idPerfil,
    opts,
    session,
    perfilPropietario,
    original?,
  ) {
    try {
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      const estadoActAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoActAlbum = estadoActAlbum.codigo;

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene l codigo de la entidad voto proyecto
      const entidaVotoNoticia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.votoNoticia,
      );
      let codEntidadVotoNoticia = entidaVotoNoticia.codigo;

      //Obtiene el estado activo de la entidad voto proyectos
      const estadoVotoNoticia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadVotoNoticia,
      );
      let codEstadoVotoNoticia = estadoVotoNoticia.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      let noticiaTraduccionOriginal;
      if (!original) {
        if (perfilPropietario) {
          this.noticia = await this.noticiaModel
            .findOne({
              $and: [{ _id: idNoticia }, { estado: codEstadoNoticia }],
            })
            .select(' -__v')
            .populate({
              path: 'traducciones',
              select:
                '-fechaActualizacion -idioma -__v -tags -estado -referencia -fechaCreacion',
              match: {
                original: true,
                estado: codEstadoTNoticia,
              },
            })
            .populate({
              path: 'perfil',
              select: 'nombre nombreContacto nombreContactoTraducido _id',
            })
            .populate({
              path: 'adjuntos',
              select:
                '-fechaActualizacion -fechaCreacion -__v -estado -nombre ',
              match: { estado: codEstadoActAlbum },
              populate: [
                {
                  path: 'media',
                  match: { estado: codEstadoMedia },
                  select:
                    'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
                  populate: [
                    {
                      path: 'traducciones',
                      select: 'descripcion',
                      match: { idioma: codIdioma, estado: codEstadoTradMedia },
                    },
                    {
                      path: 'principal',
                      select:
                        'url tipo duracion fileDefault fechaActualizacion ',
                    },
                    {
                      path: 'miniatura',
                      select:
                        'url tipo duracion fileDefault fechaActualizacion ',
                    },
                  ],
                },
                {
                  path: 'portada',
                  match: { estado: codEstadoMedia },
                  select:
                    'principal enlace miniatura  fechaCreacion fechaActualizacion catalogoMedia',
                  populate: [
                    {
                      path: 'traducciones',
                      select: 'descripcion',
                      match: { idioma: codIdioma, estado: codEstadoTradMedia },
                    },
                    {
                      path: 'principal',
                      select:
                        'url tipo duracion fileDefault fechaActualizacion ',
                    },
                    {
                      path: 'miniatura',
                      select:
                        'url tipo duracion fileDefault fechaActualizacion ',
                    },
                  ],
                },
              ],
            })
            .populate({
              path: 'medias',
              select:
                'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
              match: { estado: codEstadoMedia },
              populate: [
                {
                  path: 'traducciones',
                  select: 'descripcion',
                  match: { idioma: codIdioma, estado: codEstadoTradMedia },
                },
                {
                  path: 'principal',
                  select: 'url tipo duracion fileDefault fechaActualizacion ',
                },
                {
                  path: 'miniatura',
                  select: 'url tipo duracion fileDefault fechaActualizacion ',
                },
              ],
            })
            .populate({
              path: 'votos',
              select: 'perfil',
              match: { estado: codEstadoVotoNoticia },
              populate: {
                path: 'perfil',
                select: '_id nombre',
              },
            })
            .populate({
              path: 'direccion',
              select: 'traducciones localidad',
              populate: {
                path: 'traducciones',
                select: 'descripcion idioma original estado',
                match: {
                  estado: codidosEstadosTraduccionDireccion.activa,
                  original: true,
                },
              },
            });
        } else {
          this.noticia = await this.noticiaModel
            .findOne({
              $and: [{ _id: idNoticia }, { estado: codEstadoNoticia }],
            })
            .select(' -__v')
            .populate({
              path: 'traducciones',
              select:
                '-fechaActualizacion -idioma -__v -tags -estado -referencia -fechaCreacion',
              match: {
                idioma: codIdioma,
                estado: codEstadoTNoticia,
              },
            })
            .populate({
              path: 'perfil',
              select: 'nombre nombreContacto nombreContactoTraducido _id',
            })
            .populate({
              path: 'adjuntos',
              select:
                '-fechaActualizacion -fechaCreacion -__v -estado -nombre ',
              match: { estado: codEstadoActAlbum },
              populate: [
                {
                  path: 'media',
                  match: { estado: codEstadoMedia },
                  select:
                    'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
                  populate: [
                    {
                      path: 'traducciones',
                      select: 'descripcion',
                      match: { idioma: codIdioma, estado: codEstadoTradMedia },
                    },
                    {
                      path: 'principal',
                      select:
                        'url tipo duracion fileDefault fechaActualizacion ',
                    },
                    {
                      path: 'miniatura',
                      select:
                        'url tipo duracion fileDefault fechaActualizacion ',
                    },
                  ],
                },
                {
                  path: 'portada',
                  match: { estado: codEstadoMedia },
                  select:
                    'principal enlace miniatura  fechaCreacion fechaActualizacion catalogoMedia',
                  populate: [
                    {
                      path: 'traducciones',
                      select: 'descripcion',
                      match: { idioma: codIdioma, estado: codEstadoTradMedia },
                    },
                    {
                      path: 'principal',
                      select:
                        'url tipo duracion fileDefault fechaActualizacion ',
                    },
                    {
                      path: 'miniatura',
                      select:
                        'url tipo duracion fileDefault fechaActualizacion ',
                    },
                  ],
                },
              ],
            })
            .populate({
              path: 'medias',
              select:
                'principal enlace miniatura fechaCreacion fechaActualizacion catalogoMedia',
              match: { estado: codEstadoMedia },
              populate: [
                {
                  path: 'traducciones',
                  select: 'descripcion',
                  match: { idioma: codIdioma, estado: codEstadoTradMedia },
                },
                {
                  path: 'principal',
                  select: 'url tipo duracion fileDefault fechaActualizacion ',
                },
                {
                  path: 'miniatura',
                  select: 'url tipo duracion fileDefault fechaActualizacion ',
                },
              ],
            })
            .populate({
              path: 'votos',
              select: 'perfil',
              match: { estado: codEstadoVotoNoticia },
              populate: {
                path: 'perfil',
                select: '_id nombre',
              },
            })
            .populate({
              path: 'direccion',
              select: 'traducciones localidad',
              populate: {
                path: 'traducciones',
                select: 'descripcion idioma original estado',
                match: {
                  estado: codidosEstadosTraduccionDireccion.activa,
                  idioma: codIdioma,
                },
              },
            });
        }
      } else {
        //Obtiene la noticia en su traduccion original
        noticiaTraduccionOriginal = await this.noticiaModel
          .findOne({ $and: [{ _id: idNoticia }, { estado: codEstadoNoticia }] })
          .select(' -__v')
          .populate({
            path: 'traducciones',
            select:
              '-fechaActualizacion -idioma -__v -tags -estado -referencia -fechaCreacion',
            match: {
              original: true,
              estado: codEstadoTNoticia,
            },
          });
      }

      if (noticiaTraduccionOriginal) {
        await this.noticia.traducciones.push(
          noticiaTraduccionOriginal.traducciones[0],
        );
        return this.noticia;
      }
      if (this.noticia) {
        if (this.noticia.traducciones.length === 0) {
          let getTraduccion = await this.traducirNoticiaService.traducirNoticia(
            idNoticia,
            codIdim,
            idPerfil,
          );
          // console.log('getTraduccion: ', getTraduccion)
          return await this.verificarTraduccion(
            idNoticia,
            codIdioma,
            codIdim,
            codEstadoTNoticia,
            codEstadoNoticia,
            idPerfil,
            opts,
            session,
            perfilPropietario,
            false,
          );
        } else {
          if (this.noticia.traducciones[0].original) {
            return this.noticia;
          } else {
            return await this.verificarTraduccion(
              idNoticia,
              codIdioma,
              codIdim,
              codEstadoTNoticia,
              codEstadoNoticia,
              idPerfil,
              opts,
              session,
              perfilPropietario,
              true,
            );
          }
        }
      } else {
        return this.noticia;
      }
    } catch (error) {
      throw error;
    }
  }
}
