import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model, PaginateModel } from 'mongoose';
import { Noticia } from 'src/drivers/mongoose/interfaces/noticia/noticia.interface';
import { TraduccionesNoticias } from 'src/drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codigoEntidades,
  codigosCatalogoAcciones,
  codigosEstadosTraduccionNoticia,
} from 'src/shared/enum-sistema';

@Injectable()
export class TraducirNoticiasPerfilService {
  constructor(
    @Inject('NOTICIA_MODEL')
    private readonly noticiaModel: PaginateModel<Noticia>,
    @Inject('TRADUCCION_NOTICIA_MODEL')
    private readonly traduccionNoticiaModel: Model<TraduccionesNoticias>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async traducirNoticia(
    idNoticia: string,
    codIdim: string,
    perfil: string,
    opts?,
  ): Promise<any> {
    let session;
    let options;
    if (!opts) {
      //Inicia la transaccion
      //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
      session = await mongoose.startSession();
      session.startTransaction();
    }
    try {
      if (!opts) {
        //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
        options = { session };
      } else {
        options = opts;
      }

      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      const getTraductorOriginal = await this.traduccionNoticiaModel.findOne({
        referencia: idNoticia,
        original: true,
        estado: codigosEstadosTraduccionNoticia.activa,
      });

      if (getTraductorOriginal) {
        let titulo: any;
        if (getTraductorOriginal.titulo) {
          let textoTraducidoTitulo = await traducirTexto(
            codIdim,
            getTraductorOriginal.titulo,
          );
          titulo = textoTraducidoTitulo.textoTraducido;
        }

        let tituloCorto: any;
        if (getTraductorOriginal.tituloCorto) {
          let textoTraducidoTituloCorto = await traducirTexto(
            codIdim,
            getTraductorOriginal.tituloCorto,
          );
          tituloCorto = textoTraducidoTituloCorto.textoTraducido;
        }

        let descripcion: any;
        if (getTraductorOriginal.descripcion) {
          let textoTraducidoDescripcion = await traducirTexto(
            codIdim,
            getTraductorOriginal.descripcion,
          );
          descripcion = textoTraducidoDescripcion.textoTraducido;
        }

        let newTraduccionNoticia = {
          tituloCorto: tituloCorto || null,
          titulo: titulo || null,
          descripcion: descripcion || null,
          // tags: tags,
          idioma: codIdioma,
          original: false,
          estado: codigosEstadosTraduccionNoticia.activa,
          referencia: idNoticia,
        };

        const crearTraduccionNoticia = await new this.traduccionNoticiaModel(
          newTraduccionNoticia,
        );
        const crearTraduccion = await crearTraduccionNoticia.save(options);
        const dataNoticia = JSON.parse(JSON.stringify(crearTraduccion));
        delete dataNoticia.__v;

        if (!perfil) {
          perfil = null;
        }
        let newHistoricoTraduccionNoticia: any = {
          datos: dataNoticia,
          usuario: perfil,
          accion: codigosCatalogoAcciones.crear,
          entidad: codigoEntidades.entidadTraduccionNoticia,
        };
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoTraduccionNoticia,
        );

        await this.noticiaModel.updateOne(
          { _id: idNoticia },
          { $push: { traducciones: crearTraduccionNoticia._id } },
          options,
        );

        let returnTraduccion = [];
        let trad = {
          tituloCorto: dataNoticia.tituloCorto,
          titulo: dataNoticia.titulo,
          descripcion: dataNoticia.descripcion,
        };
        returnTraduccion.push(trad);

        if (!opts) {
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();
        }

        return returnTraduccion;
      } else {
        if (!opts) {
          //Finaliza la transaccion
          await session.endSession();
        }
      }
    } catch (error) {
      if (!opts) {
        //Confirma los cambios de la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
      }
      throw error;
    }
  }
}
