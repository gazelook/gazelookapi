import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ActualizarNoticiaUnicaService } from '../casos_de_uso/actualizar-noticia-unica.service';
import { ActualizarNoticiaDto } from './../entidad/actualizar-noticia.dto';
import { NoticiaCompletaDto } from './../entidad/noticia-response-dto';

@ApiTags('Noticias')
@Controller('api/noticia-unica')
export class ActualizarNoticiaUnicaControlador {
  constructor(
    private readonly actualizarNoticiaUnicaService: ActualizarNoticiaUnicaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: ActualizarNoticiaDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Actualiza la información de una noticia' })
  @ApiResponse({ status: 406, description: 'Error al actualizar' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 200, type: NoticiaCompletaDto, description: 'OK' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarNoticiaUnica(
    @Headers() headers,
    @Req() req,
    @Body() crearNoticiaDTO: ActualizarNoticiaDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(crearNoticiaDTO._id) &&
      mongoose.isValidObjectId(crearNoticiaDTO.perfil._id)
    ) {
      //Verifica el perfil que se envia en una peticion con el perfil del token autorization
      // let perfilVerificado = this.funcion.verificarPerfilToken(crearNoticiaDTO.perfil._id, req.user.user.perfiles);

      // if (perfilVerificado) {
      try {
        const noticia = await this.actualizarNoticiaUnicaService.actualizarNoticiaUnica(
          crearNoticiaDTO,
          crearNoticiaDTO._id,
          codIdioma,
        );

        if (noticia) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
            datos: noticia,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
      // } else {
      //     const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
      //         lang: codIdioma
      //     });
      //     return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: PARAMETROS_NO_VALIDOS })
      // }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_FOUND,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
