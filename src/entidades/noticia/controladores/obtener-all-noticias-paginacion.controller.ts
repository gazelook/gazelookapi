import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerAllNoticiasPaginacionService } from '../casos_de_uso/obtener-all-noticias-paginacion.service';
import { NoticiaResumidaPaginacionResponseDto } from './../entidad/noticia-response-dto';

@ApiTags('Noticias')
@Controller('api/todas-noticias')
export class ObtenerAllNoticiasPaginacionControlador {
  constructor(
    private readonly obtenerAllNoticiasPaginacionService: ObtenerAllNoticiasPaginacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary: 'Obtiene todas las noticias ordenadas por fecha de creacion',
  })
  @ApiResponse({
    status: 200,
    type: NoticiaResumidaPaginacionResponseDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al obtener noticia' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerTodasNoticias(
    @Headers() headers,
    @Res() response: Response,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Query('perfil') perfil: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(perfil) &&
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0
      ) {
        const noticias = await this.obtenerAllNoticiasPaginacionService.obtenerTodasNoticiasPaginacion(
          codIdioma,
          limite,
          pagina,
          perfil,
          response,
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: noticias,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
        response.send(respuesta);
        return respuesta;
      }
    }
  }

  calcularDias(fechaActual, fechaActualizacion) {
    let dias =
      (fechaActual.getTime() - fechaActualizacion.getTime()) /
      (60 * 60 * 24 * 1000);
    if (dias <= 3) {
      return true;
    } else return false;
  }
}
