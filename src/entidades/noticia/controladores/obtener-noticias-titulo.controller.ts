import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { CatalogoTipoProyectoDto } from 'src/entidades/proyectos/entidad/obtener-tipo-proyecto-dto';
import { Funcion } from 'src/shared/funcion';
import { ObneterNoticiasTituloService } from './../casos_de_uso/obtener-noticias-titulo.service';

@ApiTags('Noticias')
@Controller('api/buscar-noticias')
export class ObtenerNoticiaTituloControlador {
  constructor(
    // private readonly catalogoTipoProyectoService: CatalogoTipoProyectoService,
    private readonly obneterNoticiasTituloService: ObneterNoticiasTituloService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Busqueda de noticias por título' })
  @ApiResponse({
    status: 200,
    type: CatalogoTipoProyectoDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerProyectosTitulo(
    @Headers() headers,
    @Query('titulo') titulo: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const noticias = await this.obneterNoticiasTituloService.obtenerPorTitulo(
        codIdioma,
        titulo,
        limite,
        pagina,
        response,
      );

      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: noticias,
      });
      response.send(respuesta);
      return respuesta;
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
