import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerNoticiaUnicaService } from '../casos_de_uso/obtener-noticia-unica.service';
import { NoticiaCompletaDto } from './../entidad/noticia-response-dto';

@ApiTags('Noticias')
@Controller('api/noticia-unica')
export class ObtenerNoticiaUnicaControlador {
  constructor(
    private readonly obtenerNoticiaUnicaService: ObtenerNoticiaUnicaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Devuelve la información de una noticia unica.' })
  @ApiResponse({ status: 200, type: NoticiaCompletaDto, description: 'OK' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al obtener noticia' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerNoticiaUnica(
    @Headers() headers,
    @Query('idNoticia') idNoticia: string,
    @Query('idPerfil') idPerfil: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    console.log('codIdioma: ', codIdioma);
    if (
      mongoose.isValidObjectId(idPerfil) &&
      mongoose.isValidObjectId(idNoticia)
    ) {
      try {
        const noticia = await this.obtenerNoticiaUnicaService.obtenerNoticiaUnica(
          idNoticia,
          idPerfil,
          codIdioma,
        );
        if (noticia) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: noticia,
          });
        } else {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_OBTENER,
          });
        }
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_FOUND,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
