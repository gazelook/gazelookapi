import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerNoticiasPerfilService } from '../casos_de_uso/obtener-noticias-perfil.service';
import { NoticiaResumidaPerfilResponseDto } from './../entidad/noticia-response-dto';

@ApiTags('Noticias')
@Controller('api/noticia-perfil')
export class ObtenerNoticiasPerfilControlador {
  constructor(
    private readonly obtenerNoticiaService: ObtenerNoticiasPerfilService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Devuelve todos las noticias de un perfil.' })
  @ApiResponse({
    status: 200,
    type: NoticiaResumidaPerfilResponseDto,
    description: 'OK',
  })
  @ApiResponse({ status: 406, description: 'Error al obtener noticias' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerNoticias(
    @Headers() headers,
    @Query('perfil') perfil: string,
    @Query('traduce') traduce: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    console.log('codIdioma: ', codIdioma);
    if (
      !isNaN(limite) &&
      !isNaN(pagina) &&
      limite > 0 &&
      pagina > 0 &&
      traduce &&
      mongoose.isValidObjectId(perfil)
    ) {
      try {
        const noticias = await this.obtenerNoticiaService.obtenerNoticiasPaginacion(
          perfil,
          codIdioma,
          limite,
          pagina,
          traduce,
          response,
        );

        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: noticias,
        });
        response.send(respuesta);
        return respuesta;
      } catch (e) {
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
        response.send(respuesta);
        return respuesta;
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_FOUND,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
