import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { TraduccionEstaticaController } from './../../../multiIdioma/controladores/traduccion-estatica-controller';
import { NoticiaServicesModule } from './../casos_de_uso/noticia-services.module';
import { ActualizarNoticiaUnicaControlador } from './actualizar-noticia-unica.controller';
import { CrearNoticiaControlador } from './crear-noticia.controller';
import { EliminarNoticiaUnicaControlador } from './eliminar-noticia-unica.controller';
import { ObtenerAllNoticiasPaginacionControlador } from './obtener-all-noticias-paginacion.controller';
import { ObtenerNoticiaRecientesPaginacionControlador } from './obtener-noticia-reciente-paginacion.controller';
import { ObtenerNoticiaUnicaControlador } from './obtener-noticia-unica.controller';
import { ObtenerNoticiasPerfilControlador } from './obtener-noticias-perfil.controller';
import { ObtenerNoticiaTituloControlador } from './obtener-noticias-titulo.controller';
import { VotoNoticiaUnicaControlador } from './voto-noticia-unica.controller';

@Module({
  imports: [NoticiaServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    CrearNoticiaControlador,
    ObtenerNoticiasPerfilControlador,
    ObtenerNoticiaUnicaControlador,
    ActualizarNoticiaUnicaControlador,
    EliminarNoticiaUnicaControlador,
    VotoNoticiaUnicaControlador,
    ObtenerNoticiaRecientesPaginacionControlador,
    ObtenerNoticiaTituloControlador,
    ObtenerAllNoticiasPaginacionControlador,
  ],
})
export class NoticiaControllerModule {}
