import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CrearNoticiaService } from './../casos_de_uso/crear-noticia.service';
import { NoticiaDto } from './../entidad/noticia-dto';
import { NoticiaResumidaPerfilResponseDto } from './../entidad/noticia-response-dto';

@ApiTags('Noticias')
@Controller('api/noticia')
export class CrearNoticiaControlador {
  constructor(
    private readonly crearNoticiaService: CrearNoticiaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: NoticiaDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Crea una nueva noticia' })
  @ApiResponse({
    status: 201,
    type: NoticiaResumidaPerfilResponseDto,
    description: 'Noticia creado',
  })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al crear' })
  @UseGuards(AuthGuard('jwt'))
  public async crearNoticia(
    @Headers() headers,
    @Body() crearNoticiaDTO: NoticiaDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(crearNoticiaDTO.perfil._id) &&
      crearNoticiaDTO.traducciones[0].tituloCorto &&
      crearNoticiaDTO.traducciones[0].titulo &&
      crearNoticiaDTO.autor &&
      crearNoticiaDTO.direccion.traducciones.length > 0
    ) {
      try {
        const noticias = await this.crearNoticiaService.crearNoticia(
          crearNoticiaDTO,
          codIdioma,
        );

        if (noticias) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            datos: { _id: noticias._id },
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_CREACION,
          });
        }
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
