import { Get, Headers, HttpStatus, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerNoticiasRecienteService } from './../casos_de_uso/obtener-noticias-recientes.service';

//@Controller('api/noticias-recientes')
export class ObtenerNoticiaRecientesControlador {
  constructor(
    private readonly obtenerNoticiasRecientesService: ObtenerNoticiasRecienteService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Devuelve la información de una noticia unica.' })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 406, description: 'Error al obtener noticia' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerNoticiaRecientes(
    @Headers() headers,
    @Query('perfil') perfil: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (mongoose.isValidObjectId(perfil)) {
        const noticiaS = await this.obtenerNoticiasRecientesService.obtenerNoticiasReciente(
          perfil,
          codIdioma,
        );

        const notiFinal = [];
        if (noticiaS) {
          for (let noticia of noticiaS) {
            const noticiaFinal = [];
            for (const noticiaDia of noticia) {
              let port: any;
              let actu: any;
              let datos: any;
              if (noticiaDia?.album[0] !== undefined) {
                for (const portada of noticiaDia.album) {
                  for (const iterator of portada.media) {
                    if (iterator?.principal !== undefined) {
                      port = iterator.principal;
                    }
                  }
                }
              }

              datos = {
                _id: noticiaDia._id,
                totalVotos: noticiaDia.totalVotos,
                tituloCorto: noticiaDia.traducciones[0]?.tituloCorto,
                fechaActualizacion: noticiaDia.fechaActualizacion,
                actualizado: actu,
                portada: port,
              };
              noticiaFinal.push(datos);
            }
            notiFinal.push(noticiaFinal);
          }
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: notiFinal,
          });
        } else {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: notiFinal,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
