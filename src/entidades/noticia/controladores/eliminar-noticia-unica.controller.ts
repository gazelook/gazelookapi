import {
  Controller,
  Delete,
  Headers,
  HttpStatus,
  Param,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { EliminarNoticiasUnicaService } from './../casos_de_uso/eliminar-noticia-unica.service';

@ApiTags('Noticias')
@Controller('api/noticia-unica')
export class EliminarNoticiaUnicaControlador {
  constructor(
    private readonly eliminarNoticiaUnicaService: EliminarNoticiasUnicaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Delete('/:perfil/:idNoticia')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Elimina logicamente una noticia' })
  @ApiResponse({ status: 200, description: 'Noticia eliminada' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'No se puede eliminar' })
  @UseGuards(AuthGuard('jwt'))
  public async eliminarNoticiaUnica(
    @Headers() headers,
    @Req() req,
    @Param('perfil') perfil: string,
    @Param('idNoticia') idNoticia: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(idNoticia) &&
      mongoose.isValidObjectId(perfil)
    ) {
      // //Verifica el perfil que se envia en una peticion con el perfil del token autorization
      // let perfilVerificado = this.funcion.verificarPerfilToken(perfil, req.user.user.perfiles);

      // if (perfilVerificado) {
      try {
        const noticia = await this.eliminarNoticiaUnicaService.eliminarNoticiaUnica(
          perfil,
          idNoticia,
        );

        if (noticia) {
          const ELIMINACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ELIMINACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        } else {
          const ERROR_ELIMINAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ELIMINAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } catch (e) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_FOUND,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
    // } else {
    //     const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
    //         lang: codIdioma
    //     });
    //     return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: PARAMETROS_NO_VALIDOS })

    // }
  }
}
