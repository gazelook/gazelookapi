import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { VotoNoticiaUnicaService } from './../casos_de_uso/voto-noticia-unica.service';
import { VotoNoticiaDto } from './../entidad/voto-noticia.dto';

@ApiTags('Noticias')
@Controller('api/voto-noticia-unica')
export class VotoNoticiaUnicaControlador {
  constructor(
    private readonly votoNoticiaUnicaService: VotoNoticiaUnicaService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: VotoNoticiaDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary: 'Se realiza un unico voto por un perfil a una noticia',
  })
  @ApiResponse({ status: 200, description: 'voto correctp' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 404, description: 'Parámetros no válidos' })
  @ApiResponse({ status: 409, description: 'Error al votar' })
  @UseGuards(AuthGuard('jwt'))
  public async votoNoticiaUnica(
    @Headers() headers,
    @Body() crearNoticiaDTO: VotoNoticiaDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(crearNoticiaDTO.noticia._id) &&
        mongoose.isValidObjectId(crearNoticiaDTO.perfil._id)
      ) {
        const noticia = await this.votoNoticiaUnicaService.votoNoticiaUnica(
          crearNoticiaDTO,
          codIdioma,
        );

        if (noticia) {
          // const datos = {
          //     tituloCorto: noticia.traducciones[0].tituloCorto,
          //     titulo: noticia.traducciones[0].titulo,
          //     descripcion: noticia.traducciones[0].descripcion,
          //     tags: noticia.traducciones[0].tags,
          //     autor: noticia.autor,
          //     adjuntos: noticia?.adjuntos[0]?.idMedia,
          //     totalVotos: noticia.totalVotos,
          //     perfil: noticia.perfil,
          //     fechaCreacion: noticia.fechaCreacion
          // }
          // return this.funcion.enviarRespuestaOptimizada({codigoEstado : HttpStatus.OK, datos: datos})
          const VOTO_CORRECTO = await this.i18n.translate(
            codIdioma.concat('.VOTO_CORRECTO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: VOTO_CORRECTO,
          });
        } else {
          const ERROR_VOTO = await this.i18n.translate(
            codIdioma.concat('.ERROR_VOTO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_VOTO,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
