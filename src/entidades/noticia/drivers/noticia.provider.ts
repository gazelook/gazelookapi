import { traduccionVotoNoticiaSchema } from './../../../drivers/mongoose/modelos/traduccion_voto_noticia/traduccion-noticias-modelo';
import { votoNoticiaSchema } from './../../../drivers/mongoose/modelos/voto_noticia/voto-noticia-schema';
import { traduccionNoticiaSchema } from '../../../drivers/mongoose/modelos/traduccion_noticias/traduccion-noticias-modelo';
import { Connection } from 'mongoose';
import { noticiaModelo } from 'src/drivers/mongoose/modelos/noticias/noticia-schema';

export const noticiaProviders = [
  {
    provide: 'NOTICIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('noticia', noticiaModelo, 'noticia'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_NOTICIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_noticia',
        traduccionNoticiaSchema,
        'traduccion_noticia',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'VOTO_NOTICIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('voto_noticia', votoNoticiaSchema, 'voto_noticia'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_VOTO_NOTICIA_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_voto_noticia',
        traduccionVotoNoticiaSchema,
        'traduccion_voto_noticia',
      ),
    inject: ['DB_CONNECTION'],
  },
];
