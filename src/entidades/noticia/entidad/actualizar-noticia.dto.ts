import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Direccion } from 'src/entidades/cuenta/entidad/cuenta.dto';
import { AlbumNuevo } from 'src/entidades/perfil/entidad/crear-perfil.dto';
import { PerfilNoticiaDto } from '../../usuario/dtos/perfil.dto';
import { mediaId } from './../../media/entidad/archivo-resultado.dto';
import { TraduccionNoticiaDto } from './traduccion-noticia-dto';

export class ActualizarNoticiaDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  _id: string;

  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilNoticiaDto;

  @ApiProperty()
  @IsOptional()
  autor: string;

  @ApiProperty({ type: [AlbumNuevo] })
  @IsOptional()
  adjuntos: AlbumNuevo[];

  @ApiProperty({ type: [mediaId] })
  @IsOptional()
  medias: mediaId[];

  @ApiProperty({ type: [TraduccionNoticiaDto] })
  @IsOptional()
  traducciones: [TraduccionNoticiaDto];

  @ApiProperty({ type: Direccion })
  direccion: Direccion;
}
