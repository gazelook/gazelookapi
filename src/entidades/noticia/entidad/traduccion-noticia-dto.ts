import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class TraduccionNoticiaDto {
  @ApiProperty({ description: 'Titulo corto de la noticia' })
  @IsOptional()
  tituloCorto: string;

  @ApiProperty({ description: 'Titulo de la noticia' })
  @IsOptional()
  titulo: string;

  @ApiProperty({ description: 'Descripcion de la noticia' })
  @IsOptional()
  descripcion: string;
}

export class TraduccionResumidaNoticiaDto {
  @ApiProperty()
  @IsNotEmpty()
  tituloCorto: String;
}
export class TraduccionNoticiaPerfilDto {
  @ApiProperty()
  tituloCorto: string;
  @ApiProperty()
  titulo: string;
}

export class TraduccionCompletaNoticiaDto {
  @ApiProperty()
  @IsOptional()
  tituloCorto: String;
  @ApiProperty()
  @IsOptional()
  titulo: String;
  @ApiProperty()
  @IsOptional()
  descripcion: String;
  @ApiProperty()
  @IsOptional()
  tags: [String];
}
