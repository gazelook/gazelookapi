import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { PerfilNoticiaDto } from '../../usuario/dtos/perfil.dto';
import { NoticiaVotoDto } from './noticia-dto';
import { descipcionTraduccionVotoNoticiaDto } from './traduccion-voto-noticia.dto';

export class VotoNoticiaDto {
  @ApiProperty({ type: [descipcionTraduccionVotoNoticiaDto], required: false })
  traducciones: [descipcionTraduccionVotoNoticiaDto];

  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilNoticiaDto;

  @ApiProperty()
  @IsNotEmpty()
  noticia: NoticiaVotoDto;
}
export class VotoResumidoNoticiaDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilNoticiaDto;
}
