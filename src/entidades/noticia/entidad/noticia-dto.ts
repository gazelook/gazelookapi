import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Direccion } from 'src/entidades/cuenta/entidad/cuenta.dto';
import { PerfilNoticiaDto } from '../../usuario/dtos/perfil.dto';
import { mediaId } from './../../media/entidad/archivo-resultado.dto';
import { AlbumNuevo } from './../../perfil/entidad/crear-perfil.dto';
import { TraduccionNoticiaDto } from './traduccion-noticia-dto';

export class NoticiaDto {
  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilNoticiaDto;

  @ApiProperty({ required: true, type: Direccion })
  direccion: Direccion;

  @ApiProperty()
  @IsOptional()
  autor: string;

  @ApiProperty({ type: [AlbumNuevo] })
  @IsOptional()
  adjuntos: AlbumNuevo[];

  @ApiProperty({ type: [mediaId] })
  @IsOptional()
  medias: mediaId[];

  @ApiProperty({ type: [TraduccionNoticiaDto] })
  traducciones: [TraduccionNoticiaDto];
}

export class NoticiaVotoDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
}

export class ObtenerInfoNoticiaDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
  @ApiProperty()
  perfil: PerfilNoticiaDto;
}
