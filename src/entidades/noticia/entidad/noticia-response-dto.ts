import { ApiProperty } from '@nestjs/swagger';
import { adjuntosNoticiasPerfilDto } from 'src/entidades/album/entidad/album-dto';
import { MediaAdjuntos } from 'src/entidades/album/entidad/retorno-media-adjuntos.dto';
import { ObtenerPerfilesNoticiaPerfilDto } from 'src/entidades/perfil/entidad/perfil-usuario.dto';
import { IdentVotoProyectoDto } from 'src/entidades/proyectos/entidad/voto-proyecto-dto';
import { PerfilNoticiaUpdateDto } from '../../usuario/dtos/perfil.dto';
import { CatalogoLocalidadNoticiaDto } from './../../pais/entidad/catalogo-localidad.dto';
import {
  TraduccionCompletaNoticiaDto,
  TraduccionNoticiaPerfilDto,
  TraduccionResumidaNoticiaDto,
} from './traduccion-noticia-dto';
import { VotoResumidoNoticiaDto } from './voto-noticia.dto';
export class NoticiaResumidaPerfilResponseDto {
  @ApiProperty({
    description: 'Identificador de la noticia',
    example: '5f3173e892e08e19101271f8',
  })
  _id: string;
  @ApiProperty({})
  traducciones: TraduccionNoticiaPerfilDto;
  @ApiProperty({ type: [adjuntosNoticiasPerfilDto] })
  adjuntos: [adjuntosNoticiasPerfilDto];
  @ApiProperty({ type: [MediaAdjuntos] })
  medias: [MediaAdjuntos];
  @ApiProperty({ description: 'Total de votos de la noticia', example: 10 })
  totalVotos: number;
  @ApiProperty({ type: ObtenerPerfilesNoticiaPerfilDto })
  perfil: ObtenerPerfilesNoticiaPerfilDto;
  @ApiProperty({ type: CatalogoLocalidadNoticiaDto })
  localidad: CatalogoLocalidadNoticiaDto;
  @ApiProperty({ type: [IdentVotoProyectoDto] })
  votos: [IdentVotoProyectoDto];
  @ApiProperty({
    description: 'fecha de creacion',
    example: '2020-10-01T03:39:52.326Z',
  })
  fechaCreacion: Date;
  @ApiProperty({
    description: 'Fecha de actualizacion',
    example: '2020-10-01T03:39:52.326Z',
  })
  fechaActualizacion: Date;
  @ApiProperty({
    description: 'Si el proyecto ha sido actualizaco recientemente =true',
    example: true,
  })
  actualizado: boolean;
  @ApiProperty({
    description: 'si el perfil ya a votado =true si no =false',
    example: true,
  })
  voto: boolean;
}

export class NoticiaResumidaPaginacionResponseDto {
  @ApiProperty()
  totalVotos: string;
  @ApiProperty()
  traducciones: TraduccionResumidaNoticiaDto;
  @ApiProperty({ example: 'https/fotos.coms.qedqw' })
  portada: string;
  @ApiProperty()
  fechaActualizacion: Date;
  @ApiProperty({ example: true })
  actualizado: Boolean;
  @ApiProperty()
  voto: Boolean;
}

export class NoticiaCompletaDto {
  @ApiProperty()
  traducciones: TraduccionCompletaNoticiaDto;
  @ApiProperty({ example: 'the world knows curation' })
  autor: string;
  @ApiProperty({ example: 'the world knows curation' })
  totalVotos: number;
  @ApiProperty()
  perfil: PerfilNoticiaUpdateDto;
  // @ApiProperty()
  // adjuntos:String;
  @ApiProperty()
  localidad: CatalogoLocalidadNoticiaDto;
  @ApiProperty()
  votos: VotoResumidoNoticiaDto;
  @ApiProperty({ example: 'https/fotos.coms.qedqw' })
  portada: string;
  @ApiProperty()
  fechaCreacion: Date;
  @ApiProperty()
  voto: boolean;
}
