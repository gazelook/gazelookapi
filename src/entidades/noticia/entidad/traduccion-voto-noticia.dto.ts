import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { PerfilNoticiaDto } from '../../usuario/dtos/perfil.dto';

export class descipcionTraduccionVotoNoticiaDto {
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  descripcion: string;
}
export class VotoResumidoNoticiaDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilNoticiaDto;
}
