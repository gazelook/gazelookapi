import { Controller, Delete, Headers, HttpStatus, Query } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { EliminarDispositivoService } from './../casos_de_uso/eliminar-dispositivo.service';

@Controller('api/dispositivo')
export class EliminarDispositivoControlador {
  constructor(
    private readonly eliminarDispositivoService: EliminarDispositivoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Delete('/')
  @ApiResponse({ status: 200, description: 'Eliminado' })
  public async EliminarDispositivo(
    @Query('idDispositivo') idDispositivo: string,
    @Query('idUsuario') idUsuario: string,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const dispositivoEliminado = await this.eliminarDispositivoService.eliminarDispositivo(
        idDispositivo,
        idUsuario,
      );
      if (dispositivoEliminado) {
        //llama al metodo de traduccion estatica
        const ELIMINACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ELIMINACION_CORRECTA',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ELIMINACION_CORRECTA,
        });
      } else {
        //llama al metodo de traduccion estatica
        const ERROR_ELIMINAR = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_ELIMINAR',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_ELIMINAR,
        });
      }
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
    }
  }
}
