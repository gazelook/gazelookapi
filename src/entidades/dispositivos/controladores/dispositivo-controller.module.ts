import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { DispositivoServicesModule } from '../casos_de_uso/dispositivo-services.module';
import { CerrarSesionDispositivosController } from './cerrar-sesion-dispositivos-usuario.controller';

@Module({
  imports: [DispositivoServicesModule],
  providers: [Funcion],
  exports: [],
  controllers: [
    //ObtenerDispositivoUsuarioControlador
    //EliminarDispositivoControlador
    CerrarSesionDispositivosController,
  ],
})
export class DispositivoControllerModule {}
