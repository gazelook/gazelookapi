import {
  Controller,
  Headers,
  HttpStatus,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { isValidObjectId } from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CerrarSesionDispositivosService } from '../casos_de_uso/cerrar-sesion-dispositivos.service';

@ApiTags('Dispositivo')
@Controller('api/dispositivo')
export class CerrarSesionDispositivosController {
  constructor(
    private readonly cerrarSesionDispositivosService: CerrarSesionDispositivosService,
    private readonly funcion: Funcion,
    private readonly i18n: I18nService,
  ) {}

  @Post('/cerrar-sesiones/:dispositivo')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, type: Object, description: 'Sesiones cerradas' })
  @ApiResponse({
    status: 404,
    description: 'Error al cerrar sesión en los dispositivos',
  })
  @ApiOperation({
    summary: 'Cierra las sesiones de todos los dispositivos del usuario',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  async getDispositiv(@Param('dispositivo') dispositivo, @Headers() headers) {
    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
      if (isValidObjectId(dispositivo)) {
        const dispositivos = await this.cerrarSesionDispositivosService.cerrarSesionDispositivos(
          dispositivo,
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: dispositivos,
        });
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
    }
  }
}
