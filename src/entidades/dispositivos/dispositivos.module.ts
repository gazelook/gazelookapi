import { Module } from '@nestjs/common';
import { DispositivoServicesModule } from './casos_de_uso/dispositivo-services.module';
import { DispositivoControllerModule } from './controladores/dispositivo-controller.module';
@Module({
  imports: [DispositivoControllerModule],
  providers: [DispositivoServicesModule],
  controllers: [],
  exports: [],
})
export class DispositivosModule {}
