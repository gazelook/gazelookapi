import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Dispositivo } from 'src/drivers/mongoose/interfaces/dispositivo/dispositivo.interface';
import { TokenInterface } from 'src/drivers/mongoose/interfaces/token_usuario/token.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  nombreAcciones, nombreEntidades
} from 'src/shared/enum-sistema';

@Injectable()
export class EliminarDispositivoService {
  constructor(
    @Inject('DISPOSTIVO_MODEL')
    private readonly dispositivoModel: Model<Dispositivo>,
    @Inject('TOKEN_USUARIO_MODEL')
    private readonly tokenUsuarioModel: Model<TokenInterface>,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) { }

  // Post a single user
  async eliminarDispositivo(
    idDispositivo: string,
    idUsuario: string,
    opts?: any,
  ): Promise<any> {
    let session;
    let optsLocal;
    if (!opts) {
      session = await mongoose.startSession();
      await session.startTransaction();
    }
    try {
      if (!opts) {
        optsLocal = true;
        opts = { session };
      }

      // Obtiene el codigo de la accion a realizarse
      const accion = await this.nombreAccion.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      let getAccion = accion.codigo;

      // Obtiene el codigo de la entidad
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(
        nombreEntidades.dispositivo,
      );
      let codEntidad = entidad.codigo

      const dispositivo = await this.dispositivoModel
        .findOne({ _id: idDispositivo })
        .session(opts.session);

      if (dispositivo) {
        await this.dispositivoModel.deleteOne({ _id: idDispositivo }, opts);

        await this.tokenUsuarioModel.deleteOne(
          { _id: dispositivo.tokenUsuario },
          opts,
        );

        //datos para guardar el historico
        let newHistorico: any = {
          datos: dispositivo,
          usuario: idUsuario,
          accion: getAccion,
          entidad: codEntidad,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistorico);

        // FINISH TRANSACTION
        if (optsLocal) {
          await session.commitTransaction();
          await session.endSession();
        }
        return dispositivo;
      } else {
        // FINISH TRANSACTION
        if (optsLocal) {
          await session.endSession();
        }
        return true
      }

    } catch (error) {
      if (optsLocal) {
        await session.abortTransaction();
        await session.endSession();
      }
      throw error;
    }
  }
}
