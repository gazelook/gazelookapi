import { Connection } from 'mongoose';
import { tokenUsuarioModelo } from 'src/drivers/mongoose/modelos/token_usuario/token_usuario.schema';
import { dispositivoSchema } from './../../../drivers/mongoose/modelos/dispositivos/dispositivo.schema';

export const dispositivoProviders = [
  {
    provide: 'DISPOSTIVO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('dispositivos', dispositivoSchema, 'dispositivos'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TOKEN_USUARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('token_usuario', tokenUsuarioModelo, 'token_usuario'),
    inject: ['DB_CONNECTION'],
  },
];
