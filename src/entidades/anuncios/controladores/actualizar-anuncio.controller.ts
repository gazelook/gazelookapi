import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ActualizarAnuncioService } from '../casos_de_uso/actualizar-anuncio.service';
import { ActualizarAnuncioDto } from '../entidad/actualizar-anuncio.dto';

@ApiTags('Anuncios')
@Controller('api/anuncio')
export class ActualizarAnuncioControlador {
  constructor(
    private readonly actualizarAnuncioService: ActualizarAnuncioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: ActualizarAnuncioDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Actualiza la información de un anuncio' })
  @ApiResponse({
    status: 200, //type: ProyectoDto,
    description: 'Actualización correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarAnuncio(
    @Headers() headers,
    @Req() req,
    @Body() actualizarAnuncioDto: ActualizarAnuncioDto,
  ) {
    console.log(actualizarAnuncioDto);

    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(actualizarAnuncioDto._id) &&
      mongoose.isValidObjectId(actualizarAnuncioDto.perfil._id)
    ) {
      //Verifica el perfil que se envia en una peticion con el perfil del token autorization
      // let perfilVerificado = this.funcion.verificarPerfilToken(actualizarAnuncioDto.perfil._id, req.user.user.perfiles);

      // if (perfilVerificado) {
      try {
        const anuncios = await this.actualizarAnuncioService.actualizarAnuncio(
          actualizarAnuncioDto,
          actualizarAnuncioDto._id,
          codIdioma,
        );

        if (anuncios) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
            datos: anuncios,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } catch (e) {
        if (e?.codigoNombre) {
          const MENSAJE = await this.i18n.translate(
            codIdioma.concat(`.${e.codigoNombre}`),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: e.codigo,
            mensaje: MENSAJE,
          });
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
            mensaje: e.message,
          });
        }
      }
      // } else {

      //     const NO_PERMISO_ACCION = await this.i18n.translate(codIdioma.concat('.NO_PERMISO_ACCION'), {
      //         lang: codIdioma
      //     });
      //     return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.UNAUTHORIZED, mensaje: NO_PERMISO_ACCION })

      // }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
