import {
  Controller,
  Delete,
  Headers,
  HttpStatus,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { EliminarMensajesAnuncioService } from '../casos_de_uso/eliminar-mensaje-anuncio.service';

@ApiTags('Anuncios')
@Controller('api/anuncio/eliminar-mensaje')
export class EliminarMensajeAnuncioControlador {
  constructor(
    private readonly eliminarMensajesAnuncioService: EliminarMensajesAnuncioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Delete('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Elimina la información de un mensaje de anuncio' })
  @ApiResponse({
    status: 200, //type: ProyectoDto,
    description: 'Eliminacion correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al eliminar' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @UseGuards(AuthGuard('jwt'))
  public async eliminarMensajeAnuncio(
    @Headers() headers,
    @Query('idMensaje') idMensaje: string,
    @Query('idPerfil') perfil: string,
    @Query('admin') admin?: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(idMensaje) &&
      mongoose.isValidObjectId(perfil)
    ) {
      try {
        const anuncios = await this.eliminarMensajesAnuncioService.eliminarMensajesAnuncio(
          idMensaje,
          perfil,
          admin,
          codIdioma,
        );
        if (anuncios) {
          const ELIMINACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ELIMINACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        } else {
          const ERROR_ELIMINAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ELIMINAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } catch (e) {
        if (e?.codigoNombre) {
          const MENSAJE = await this.i18n.translate(
            codIdioma.concat(`.${e.codigoNombre}`),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: e.codigo,
            mensaje: MENSAJE,
          });
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
            mensaje: e.message,
          });
        }
      }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
