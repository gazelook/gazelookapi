import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerAnunciosPaginateService } from '../casos_de_uso/obtener-anuncios-paginate.service';

@ApiTags('Anuncios')
@Controller('api/anuncios-recientes')
export class ObtenerAnunciosPaginateControlador {
  constructor(
    private readonly obtenerAnunciosPaginateService: ObtenerAnunciosPaginateService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Devuelve la información de los anuncios mas recientes con paginacion',
  })
  @ApiResponse({
    status: 200, //type: ProyectoCompletoDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerAnunciosPaginate(
    @Headers() headers,
    @Query('perfil') perfil: string,
    // @Query('limite') limite: number,
    // @Query('pagina') pagina: number,
    // @Res() response: Response
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(perfil)
        // && !isNaN(limite) && !isNaN(pagina) && limite > 0 && pagina > 0
      ) {
        const anuncios = await this.obtenerAnunciosPaginateService.obtenerAnunciosPaginate(
          perfil,
          codIdioma,
        );

        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: anuncios,
        });
        // response.send(respuesta)
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        // response.send(respuesta)
        return respuesta;
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
