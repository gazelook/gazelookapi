import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ActualizarAnuncioService } from '../casos_de_uso/actualizar-anuncio.service';
import { ActivarAnuncioDto } from '../entidad/actualizar-anuncio.dto';

@ApiTags('Anuncios')
@Controller('api/activar-anuncio')
export class ActivarAnuncioControlador {
  constructor(
    private readonly actualizarAnuncioService: ActualizarAnuncioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Put('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: ActivarAnuncioDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Activa o desactiva un anuncio' })
  @ApiResponse({ status: 201, description: 'Creación correcta' })
  @ApiResponse({ status: 404, description: 'Error al crear' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async activarAnuncio(
    @Headers() headers,
    @Body() activarAnuncioDto: ActivarAnuncioDto,
  ) {
    console.log('activarAnuncioDto: ', activarAnuncioDto);
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(activarAnuncioDto.perfil._id) &&
        mongoose.isValidObjectId(activarAnuncioDto._id) &&
        activarAnuncioDto.tipo.codigo
      ) {
        const anuncio = await this.actualizarAnuncioService.activarAnuncio(
          activarAnuncioDto.perfil._id,
          activarAnuncioDto._id,
          activarAnuncioDto.activo,
          activarAnuncioDto.tipo.codigo,
        );

        if (anuncio) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
