import { Response } from 'express';
import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  Res,
  HttpStatus,
  Get,
  Headers,
  Query,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';
import { BuscarAnunciosRecientesPaginacionService } from '../casos_de_uso/buscar-anuncios-paginacion.service';

@ApiTags('Anuncios')
@Controller('api/anuncio')
export class BuscarAnunciosPaginacionControlador {
  constructor(
    private readonly buscarAnunciosRecientesPaginacionService: BuscarAnunciosRecientesPaginacionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/buscar-anuncios')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary:
      'Obtiene una lista de Intercambios, en un rango de fechas y filtros con paginacion',
  })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  // @UseGuards(AuthGuard('jwt'))
  public async buscarAnuncios(
    @Headers() headers,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Query('titulo') titulo: string,
    @Query('tipo') tipo: string,
    @Query('codigo') codigo: string,
    @Query('activo') activo: boolean,
    @Query('favorito') favorito: boolean,
    @Query('fecha') fecha: Date,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (!isNaN(limite) && !isNaN(pagina) && limite > 0 && pagina > 0) {
        const anuncios = await this.buscarAnunciosRecientesPaginacionService.buscarAnuncios(
          codIdioma,
          limite,
          pagina,
          tipo,
          titulo,
          codigo,
          activo,
          favorito,
          fecha,
          response,
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: anuncios,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
