import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';
import {
  Controller,
  HttpStatus,
  Get,
  Headers,
  UseGuards,
  Query,
  Res,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import * as mongoose from 'mongoose';
import { AuthGuard } from '@nestjs/passport';
import { Funcion } from 'src/shared/funcion';
import { Response } from 'express';
import { AnunciosPaginateService } from '../casos_de_uso/anuncios-paginate.service';

@ApiTags('Anuncios')
@Controller('api/anuncios')
export class AnunciosPaginateControlador {
  constructor(
    private readonly anunciosPaginateService: AnunciosPaginateService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({
    summary:
      'Devuelve la información de los anuncios mas recientes con paginacion',
  })
  @ApiResponse({
    status: 200, //type: ProyectoCompletoDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async anunciosPaginate(
    @Headers() headers,
    @Query('perfil') perfil: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(perfil) &&
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0
      ) {
        const anuncios = await this.anunciosPaginateService.anunciosPaginate(
          perfil,
          codIdioma,
          pagina,
          limite,
          response,
        );

        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: anuncios,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
