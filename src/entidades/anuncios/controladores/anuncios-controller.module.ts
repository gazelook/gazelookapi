import { forwardRef, Module } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { NotificacionServicesModule } from 'src/entidades/notificacion/casos_de_uso/notificacion-services.module';
import { AnunciosServicesModule } from '../casos_de_uso/anuncios-services.module';
import { CrearAnuncioControlador } from './crear-anuncio.controller';
import { ObtenerAnuncioUnicoControlador } from './obtener-anuncio-unico.controller';
import { ActualizarAnuncioControlador } from './actualizar-anuncio.controller';
import { ActivarAnuncioControlador } from './activar-anuncio.controller';
import { ObtenerAnunciosPaginateControlador } from './obtener-anuncios-paginate.controller';
import { AnunciosPaginateControlador } from './anuncios-paginate.controller';
import { EliminarAnuncioControlador } from './eliminar-anuncio.controller';
import { BuscarAnunciosPaginacionControlador } from './buscar-anuncios-paginacion.controller';
import { ObtenerAnunciosAdvertisingPaginateControlador } from './obtener-anuncios-advertising-paginate.controller';
import { CrearMensajeAnuncioControlador } from './crear-mensaje-anuncio.controller';
import { ObtenerMensajeAnunciosControlador } from './obtener-mensajes-anuncios.controller';
import { EliminarMensajeAnuncioControlador } from './eliminar-mensaje-anuncio.controller';

@Module({
  imports: [
    AnunciosServicesModule,
    forwardRef(() => NotificacionServicesModule),
  ],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    CrearAnuncioControlador,
    ObtenerAnuncioUnicoControlador,
    ActualizarAnuncioControlador,
    ActivarAnuncioControlador,
    ObtenerAnunciosPaginateControlador,
    AnunciosPaginateControlador,
    EliminarAnuncioControlador,
    BuscarAnunciosPaginacionControlador,
    ObtenerAnunciosAdvertisingPaginateControlador,
    CrearMensajeAnuncioControlador,
    EliminarMensajeAnuncioControlador,
    ObtenerMensajeAnunciosControlador,
  ],
})
export class AnunciosControllerModule {}
