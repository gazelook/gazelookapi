import {
  Controller,
  Delete,
  Headers,
  HttpStatus,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { EliminarAnuncioService } from '../casos_de_uso/eliminar-anuncio.service';

@ApiTags('Anuncios')
@Controller('api/anuncio')
export class EliminarAnuncioControlador {
  constructor(
    private readonly eliminarAnuncioService: EliminarAnuncioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Delete('/')
  @ApiSecurity('Authorization')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Elimina un anuncio' })
  @ApiResponse({
    status: 200, //type: ProyectoDto,
    description: 'Actualización correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al actualizar' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @UseGuards(AuthGuard('jwt'))
  public async eliminarAnuncio(
    @Headers() headers,
    @Query('anuncio') idAnuncio: string,
    @Query('perfil') idPerfil: string,
    @Req() req,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    if (
      mongoose.isValidObjectId(idPerfil) &&
      mongoose.isValidObjectId(idAnuncio)
    ) {
      //Verifica el perfil que se envia en una peticion con el perfil del token autorization
      // let perfilVerificado = this.funcion.verificarPerfilToken(idPerfil, req.user.user.perfiles);

      // if (perfilVerificado) {
      try {
        const anuncios = await this.eliminarAnuncioService.eliminarAnuncio(
          idPerfil,
          idAnuncio,
        );

        if (anuncios) {
          const ELIMINACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ELIMINACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
            datos: anuncios,
          });
        } else {
          const ERROR_ELIMINAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ELIMINAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } catch (e) {
        if (e?.codigoNombre) {
          const MENSAJE = await this.i18n.translate(
            codIdioma.concat(`.${e.codigoNombre}`),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: e.codigo,
            mensaje: MENSAJE,
          });
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
            mensaje: e.message,
          });
        }
      }
      // } else {
      //     const NO_PERMISO_ACCION = await this.i18n.translate(codIdioma.concat('.NO_PERMISO_ACCION'), {
      //         lang: codIdioma
      //     });
      //     return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.UNAUTHORIZED, mensaje: NO_PERMISO_ACCION })
      // }
    } else {
      const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
        codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
        {
          lang: codIdioma,
        },
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.NOT_ACCEPTABLE,
        mensaje: PARAMETROS_NO_VALIDOS,
      });
    }
  }
}
