import { Module } from '@nestjs/common';
import { AnunciosServicesModule } from './casos_de_uso/anuncios-services.module';
import { AnunciosControllerModule } from './controladores/anuncios-controller.module';

@Module({
  imports: [AnunciosControllerModule],
  providers: [AnunciosServicesModule],
  controllers: [],
})
export class AnunciosModule {}
