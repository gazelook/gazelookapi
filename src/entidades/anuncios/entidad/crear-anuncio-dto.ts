import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { CatalogoEstilosDto } from 'src/entidades/catalogos/entidad/catalogo-estilos.dto';
import { idPerfilDto } from 'src/entidades/perfil/entidad/crear-perfil.dto';
import { CodCatalogoTipoAnuncioDto } from './anuncio-dto';
import {
  CrearTematicaAnuncioDto,
  CrearTraduccionAnuncioDto,
} from './traduccion-anuncio-dto';

export class idConfiguracionAnuncioDto {
  @ApiProperty({ description: 'identificacion de la configuracion' })
  @IsString()
  @IsNotEmpty()
  _id: string;
}

export class estilosnAnuncioDto {
  @ApiProperty({ type: String, description: 'color exadecimal' })
  @IsString()
  @IsNotEmpty()
  color: string;

  @ApiProperty({ type: CatalogoEstilosDto })
  @IsString()
  @IsNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => CatalogoEstilosDto)
  tipo: CatalogoEstilosDto;
}

export class configuracionEstiloAnuncioDto {
  // @ApiProperty({type: CatalogoConfiguracionDto})
  // @IsNotEmpty()
  // tipo: CatalogoConfiguracionDto;

  @ApiProperty({ type: [idConfiguracionAnuncioDto] })
  @IsNotEmpty()
  estilos: Array<idConfiguracionAnuncioDto>;
}

export class CrearAnuncioDto {
  @ApiProperty({ type: idPerfilDto })
  perfil: idPerfilDto;

  @ApiProperty({ required: true, type: [CrearTraduccionAnuncioDto] })
  traducciones: Array<CrearTraduccionAnuncioDto>;

  @ApiProperty({ required: true, type: [CrearTematicaAnuncioDto] })
  tematica: Array<CrearTematicaAnuncioDto>;

  @ApiProperty({ type: CodCatalogoTipoAnuncioDto })
  tipo: CodCatalogoTipoAnuncioDto;

  @ApiProperty({ type: Boolean, example: true })
  activo: boolean;

  @ApiProperty({ required: true, type: [configuracionEstiloAnuncioDto] })
  configuraciones: Array<configuracionEstiloAnuncioDto>;

  @ApiProperty({ required: true, type: Number })
  orden: number;

  @ApiProperty({ type: Number })
  intervaloVisualizacion: number;

  @ApiProperty({ type: Number })
  tiempoVisualizacion: number;

  @ApiProperty({ type: Date })
  fechaInicio: Date;

  @ApiProperty({ type: Date })
  fechaFin: Date;

  @ApiProperty({ type: Date })
  horaInicio: Date;

  @ApiProperty({ type: Date })
  horaFin: Date;
}
