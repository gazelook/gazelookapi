import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { PerfilProyectoDto } from 'src/entidades/usuario/dtos/perfil.dto';
import { TraduccionAnuncioDto } from './traduccion-anuncio-dto';

export class AnnuncioDto {
  @ApiProperty({
    description: 'Identificador del perfil',
    example: '5f3173e892e08e19101271f8',
  })
  perfil: PerfilProyectoDto;
  @ApiProperty({
    description: 'Codigo del Tipo de proyecto',
    example: '5f3ebf1dd9d7099cad68c637',
  })
  tipo: string;
  @ApiProperty({
    type: [],
    description: 'Traducciones en diferentes idiomas del proyecto',
    example: '',
  })
  traducciones: [TraduccionAnuncioDto];
  @ApiProperty({ description: 'Si el anuncio esta activo', example: true })
  activo: boolean;
  @ApiProperty({ description: 'codigos de estados', example: '' })
  estado: string;
}
export class ProyectoUnicoDto {
  @ApiProperty()
  @IsString()
  _id: string;
}

export class ObtenerInfoproyectoDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
  @ApiProperty()
  @IsNotEmpty()
  perfil: PerfilProyectoDto;
}

export class ObtenerProyectosDto {
  @ApiProperty({
    required: true,
    description: 'Limite de datos que se muestran por pagina',
    example: 5,
  })
  @IsNotEmpty()
  limite: number;

  @ApiProperty({ required: true, description: 'Numero de paginas', example: 1 })
  @IsNotEmpty()
  pagina: number;

  @ApiProperty({
    required: true,
    description: 'Codigo del tipo de proyecto',
    example: 'CAT_TIPO_PROY_01',
  })
  @IsNotEmpty()
  filtro: any;

  @ApiProperty({
    required: true,
    description: 'Filtro requerido que se utiliza en la consulta',
    example: 'CAT_TIPO_PROY_01',
  })
  @IsNotEmpty()
  tipo: string;

  @ApiProperty({
    description: 'Codigo del estado del proyecto',
    example: 'EST_110',
  })
  estado: string;

  @ApiProperty({
    description: 'Identificador del perfil',
    example: '5f4528c05cf8ad362fdbb98f',
  })
  perfil: string;

  @ApiProperty({
    required: false,
    description: 'Fecha inicial para la busqueda',
    example: '2020-09-01',
  })
  fechaInicial: Date;

  @ApiProperty({
    required: false,
    description: 'Fecha final para la busqueda',
    example: '2020-09-03',
  })
  fechaFinal: Date;
}

// export class CatalogoLocalidadProyectoDto {

//     @ApiProperty({required:false, description: 'Codigo de la localidad del proyecto', example:'LOC_735'})
//     codigo: string;
// }

export class CodCatalogoTipoAnuncioDto {
  @ApiProperty({
    required: false,
    description: 'Codigo del tipo de anuncio',
    example: 'TIPANU_01',
  })
  codigo: string;
}

// export class BusquedaProyectoTituloDto {
//     @ApiProperty({description: 'titulo largo', example: "titulo largo"})
//     titulo: string;

//     @ApiProperty()
//     proyectos: ProyectoCompletoDto;
// }

// export class BusquedaProyectoRangoDto {

//     @ApiProperty()
//     proyectos: ProyectoCompletoDto;
// }

// export class fechaMaximaValorEstimadoDto {

//     @ApiProperty({description: 'Fecha maxima que se puede actualizar el valor estimado del proyecto', example: "2020-12-22T00:00:00.000Z"})
//     fecha:string;
// }
