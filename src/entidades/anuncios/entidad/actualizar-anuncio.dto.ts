import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { CatalogoEstilosDto } from 'src/entidades/catalogos/entidad/catalogo-estilos.dto';
import {
  PerfilAnuncioDto,
  PerfilProyectoDto,
} from 'src/entidades/usuario/dtos/perfil.dto';
import { CodCatalogoTipoAnuncioDto } from './anuncio-dto';
import { idConfiguracionAnuncioDto } from './crear-anuncio-dto';
import {
  CrearTematicaAnuncioDto,
  CrearTraduccionAnuncioDto,
} from './traduccion-anuncio-dto';

export class actualizarEstilosnAnuncioDto {
  @ApiProperty({ type: String })
  @IsNotEmpty()
  _id: string;

  @ApiProperty({ type: String, description: 'color exadecimal' })
  @IsString()
  @IsNotEmpty()
  color: string;

  @ApiProperty({ type: CatalogoEstilosDto })
  @IsString()
  @IsNotEmpty()
  tipo: CatalogoEstilosDto;
}

export class actualizarConfiguracionEstiloAnuncioDto {
  @ApiProperty({ type: String })
  @IsNotEmpty()
  _id: string;

  @ApiProperty({ type: [idConfiguracionAnuncioDto] })
  @IsNotEmpty()
  estilos: Array<idConfiguracionAnuncioDto>;
}

export class ActualizarAnuncioDto {
  @ApiProperty({ required: true, description: 'identificador del anuncio' })
  @IsString()
  _id: string;

  @ApiProperty({ type: PerfilAnuncioDto })
  perfil: PerfilAnuncioDto;

  @ApiProperty({ type: [CrearTraduccionAnuncioDto] })
  traducciones: [CrearTraduccionAnuncioDto];

  @ApiProperty({ required: true, type: [CrearTematicaAnuncioDto] })
  tematica: Array<CrearTematicaAnuncioDto>;

  @ApiProperty({ type: Number })
  orden: number;

  @ApiProperty({ type: Number })
  intervaloVisualizacion: number;

  @ApiProperty({ type: Number })
  tiempoVisualizacion: number;

  @ApiProperty({ type: Boolean })
  activo: boolean;

  @ApiProperty({ type: Boolean })
  favorito: boolean;

  @ApiProperty({
    required: true,
    type: [actualizarConfiguracionEstiloAnuncioDto],
  })
  configuraciones: Array<actualizarConfiguracionEstiloAnuncioDto>;

  @ApiProperty({ type: Date })
  fechaInicio: Date;

  @ApiProperty({ type: Date })
  fechaFin: Date;

  @ApiProperty({ type: Date })
  horaInicio: Date;

  @ApiProperty({ type: Date })
  horaFin: Date;
}

export class ActivarAnuncioDto {
  @ApiProperty({ required: true, description: 'identificador del anuncio' })
  @IsString()
  _id: string;

  @ApiProperty({ type: PerfilAnuncioDto })
  perfil: PerfilAnuncioDto;

  @ApiProperty({ type: CodCatalogoTipoAnuncioDto })
  tipo: CodCatalogoTipoAnuncioDto;

  @ApiProperty({ type: Boolean })
  activo: boolean;
}

export class EliminarAnuncioDto {
  @ApiProperty({ required: true, description: 'identificador del anuncio' })
  @IsString()
  _id: string;

  @ApiProperty({ type: PerfilAnuncioDto })
  perfil: PerfilAnuncioDto;
}

export class ActualizarEstadoProyectoDto {
  @ApiProperty({ required: true, description: 'identificador del proyecto' })
  @IsString()
  _id: string;

  @ApiProperty()
  perfil: PerfilProyectoDto;

  @ApiProperty({
    required: true,
    description:
      'Filtro que se utiliza para saber a que estado cambiar el proyecto',
    example:
      'activa, eliminado, preEstrategia, esperaFondos, revision, ejecucion, esperaDonacion, finalizado, foro, estrategia',
  })
  filtro: string;
}

export class RecomiendaProyectoDto {
  @ApiProperty({ required: true, description: 'identificador del proyecto' })
  @IsString()
  _id: string;

  @ApiProperty()
  perfil: PerfilProyectoDto;
}
