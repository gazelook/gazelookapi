import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { PerfilAnuncioDto } from 'src/entidades/usuario/dtos/perfil.dto';

export class CrearMensajesAnuncioDto {
  @ApiProperty({ type: PerfilAnuncioDto })
  @ValidateNested({ each: true })
  @IsNotEmpty()
  @Type(() => PerfilAnuncioDto)
  perfil: PerfilAnuncioDto;

  @ApiProperty({
    description: 'mensaje del anuncio',
    example: 'Mensaje del anuncio',
  })
  texto: string;
}
