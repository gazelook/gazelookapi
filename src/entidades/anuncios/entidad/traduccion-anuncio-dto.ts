import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';
import { CodCatalogoIdiomasDto } from 'src/entidades/catalogos/entidad/catalogo-idiomas.dto';

export class TraduccionAnuncioDto {
  @ApiProperty({
    description: 'enunciado que describe el proyecto en su totalidad',
    example: 'Esta es la descripción del proyecto',
  })
  @IsString()
  @IsNotEmpty()
  descripcion: string;
}

export class CrearTraduccionAnuncioDto {
  @ApiProperty({
    description: 'titulo del anuncio',
    example: 'titulo del anuncio',
  })
  @IsString()
  titulo: string;

  @ApiProperty({
    description: 'descripcion del anuncio',
    example: 'descripcion del anuncio',
  })
  @IsString()
  descripcion: string;

  @ApiProperty({ type: CodCatalogoIdiomasDto })
  idioma: CodCatalogoIdiomasDto;
}

export class CrearTematicaAnuncioDto {
  @ApiProperty({
    description: 'tematica del anuncio',
    example: 'tematica del anuncio',
  })
  @IsString()
  titulo: string;
}
