import { Connection } from 'mongoose';
import { AnuncioModelo } from 'src/drivers/mongoose/modelos/anuncios/anuncio.schema';
import { CatalogoTipoAnuncioModelo } from 'src/drivers/mongoose/modelos/catalogo_tipo_anuncio/catalogo-tipo-anuncio.schema';
import { ConfiguracionEstiloAnunciosModelo } from 'src/drivers/mongoose/modelos/configuracion_estilo_anuncios/configuracion-estilo_anuncios.schema';
import { EstiloAnuncioModelo } from 'src/drivers/mongoose/modelos/estilo_anuncio/estilo-anuncio.schema';
import { HistoricoModelo } from 'src/drivers/mongoose/modelos/historico/historico.schema';
import { MensajesAnuncioModelo } from 'src/drivers/mongoose/modelos/mensajes_anuncio/mensajes-anuncio.schema';
import { perfilModelo } from 'src/drivers/mongoose/modelos/perfil/perfil.schema';
import { TraduccionAnuncioModelo } from 'src/drivers/mongoose/modelos/traduccion_anuncio/traduccion-anuncio.schema';
import { TraduccionTematicaAnuncioModelo } from 'src/drivers/mongoose/modelos/traduccion_tematica_anuncio/traduccion-tematica-anuncio.schema';

export const anuncioProviders = [
  {
    provide: 'ANUNCIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('anuncio', AnuncioModelo, 'anuncio'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_ANUNCIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_anuncio',
        TraduccionAnuncioModelo,
        'traduccion_anuncio',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TEMATICA_ANUNCIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'tematica_anuncio',
        TraduccionTematicaAnuncioModelo,
        'tematica_anuncio',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_TIPO_ANUNCIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_tipo_anuncio',
        CatalogoTipoAnuncioModelo,
        'catalogo_tipo_anuncio',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'HISTORICO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('historico', HistoricoModelo, 'historico'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ESTILO_ANUNCIOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'estilos_anuncio',
        EstiloAnuncioModelo,
        'estilos_anuncio',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_ESTILO_ANUNCIOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_estilo_anuncios',
        ConfiguracionEstiloAnunciosModelo,
        'configuracion_estilo_anuncios',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'MENSAJES_ANUNCIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'mensajes_anuncio',
        MensajesAnuncioModelo,
        'mensajes_anuncio',
      ),
    inject: ['DB_CONNECTION'],
  },
];
