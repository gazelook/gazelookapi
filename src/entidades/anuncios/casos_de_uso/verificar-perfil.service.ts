import { codigosCatalogoRol } from '../../../shared/enum-sistema';
import { Injectable } from '@nestjs/common';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';

@Injectable()
export class VerificarPerfilAnuncioService {
  getProyectos: any;
  constructor(
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
  ) {}

  async verificarPerfilAdministradorAnuncios(idPerfil: string): Promise<any> {
    try {
      let getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
        idPerfil,
      );

      let adminAnunciosGazelook = false;
      //Verifica si el perfil que se envia es usuario administrador del sistema
      if (getUsuario && getUsuario.usuario) {
        if (getUsuario.usuario.rolSistema.length > 0) {
          for (const getRolSistema of getUsuario.usuario.rolSistema) {
            if (getRolSistema) {
              if (
                getRolSistema.rol == codigosCatalogoRol.administradorAnuncios ||
                getRolSistema.rol == codigosCatalogoRol.administrador ||
                getRolSistema.rol == codigosCatalogoRol.subAdministradorGeneral
                // getRolSistema.rol == codigosCatalogoRol.administradorTraductorAnuncios
              ) {
                adminAnunciosGazelook = true;
                break;
              }
            }
          }
        }
      }
      return adminAnunciosGazelook;
    } catch (error) {
      throw error;
    }
  }

  //Verifica que el perfil sea administrador de traductor de anuncios
  async verificarPerfilAdministradorTraductorAnuncios(
    idPerfil: string,
  ): Promise<any> {
    try {
      let getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
        idPerfil,
      );

      let adminAnunciosGazelook = false;
      //Verifica si el perfil que se envia es usuario administrador del sistema
      if (getUsuario.usuario) {
        if (getUsuario.usuario.rolSistema.length > 0) {
          for (const getRolSistema of getUsuario.usuario.rolSistema) {
            if (getRolSistema) {
              if (
                getRolSistema.rol ==
                codigosCatalogoRol.administradorTraductorAnuncios
              ) {
                adminAnunciosGazelook = true;
                break;
              }
            }
          }
        }
      }
      return adminAnunciosGazelook;
    } catch (error) {
      throw error;
    }
  }
}
