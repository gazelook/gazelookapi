import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { ConfiguracionEstiloAnuncios } from 'src/drivers/mongoose/interfaces/configuracion_estilo_anuncios/configuracion_estilo_anuncios.interface';
import { EstiloAnuncio } from 'src/drivers/mongoose/interfaces/estilos_anuncio/estilo-anuncio.interface';
import { TraduccionAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_anuncio/traduccion_anuncio.interface';
import { TraduccionTematicaAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_tematica_anuncio/traduccion_tematica_anuncio.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { erroresGeneral } from 'src/shared/enum-errores';
import {
  codigosCatalogoConfiguracion,
  estadosConfiguracionEstilosAnuncio,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
  rangoNumericoAnuncios,
} from 'src/shared/enum-sistema';
import { CrearAnuncioDto } from '../entidad/crear-anuncio-dto';
import { TraducirTematicaServiceSegundoPlano } from './traducir-tematica-segundo-plano.service';
import { VerificarPerfilAnuncioService } from './verificar-perfil.service';
const sw = require('stopword');
const mongoose = require('mongoose');

@Injectable()
export class CrearAnuncioService {
  constructor(
    @Inject('ANUNCIO_MODEL') private readonly anuncioModel: Model<Anuncio>,
    @Inject('TRADUCCION_ANUNCIO_MODEL')
    private readonly traduccionAnuncioModel: Model<TraduccionAnuncio>,
    @Inject('TEMATICA_ANUNCIO_MODEL')
    private readonly tematicaAnuncioModel: Model<TraduccionTematicaAnuncio>,
    @Inject('ESTILO_ANUNCIOS_MODEL')
    private readonly estiloAnunciosModel: Model<EstiloAnuncio>,
    @Inject('CONFIGURACION_ESTILO_ANUNCIOS_MODEL')
    private readonly configuracionEstiloAnunciosModel: Model<
      ConfiguracionEstiloAnuncios
    >,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private verificarPerfilAnuncioService: VerificarPerfilAnuncioService,
    private traducirTematicaServiceSegundoPlano: TraducirTematicaServiceSegundoPlano,
  ) {}

  async crearAnuncio(
    anuncioDto: CrearAnuncioDto,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Verifica si el perfil es un perfil administrador de anuncios de gazelook
      let adminAnunciosGazelook = await this.verificarPerfilAnuncioService.verificarPerfilAdministradorAnuncios(
        anuncioDto.perfil._id,
      );

      if (!adminAnunciosGazelook) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      //Obtiene la entidad anuncio
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.anuncio,
      );
      let getCodEntAnuncio = entidad.codigo;

      //Obtiene el estado de anuncio
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        getCodEntAnuncio,
      );
      let codEstado = estado.codigo;

      //Obtiene la entidad traduccion anuncio
      const entidadTA = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionAnuncio,
      );
      let codEntidadTAnuncio = entidadTA.codigo;

      //Obtiene el estado  activa de la entidad traduccion anuncio
      const estadoTA = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTAnuncio,
      );
      let codEstadoTAnuncio = estadoTA.codigo;

      //Obtiene la entidad tematica anuncio
      const entidadTematica = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.tematicaAnuncio,
      );
      let codEntidadTematicaAnuncio = entidadTematica.codigo;

      //Obtiene el estado  activa de la entidad traduccion anuncio
      const estadoTematica = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTematicaAnuncio,
      );
      let codEstadoTematica = estadoTematica.codigo;

      // //Obtiene el ultimo anuncio
      // const getUltimoAnuncio = await this.anuncioModel.findOne(
      //   {
      //     estado: codEstado,
      //     activo: true
      //   }
      // ).sort({ fechaActualizacion: -1 }).session(opts.session);

      // //Desactiva el ultimo anuncio
      // if (getUltimoAnuncio) {
      //   await this.anuncioModel.updateOne(
      //     { _id: getUltimoAnuncio._id },
      //     { $set: { activo: false, fechaActualizacion: new Date() } })
      // }

      //Objeto de traduccion anuncio
      const idAnuncio = new mongoose.mongo.ObjectId();

      let codTraduccionAnuncio = [];
      for (const traduccionAnunc of anuncioDto.traducciones) {
        let newTraduccionAnuncio = {
          titulo: traduccionAnunc.titulo,
          descripcion: traduccionAnunc.descripcion,
          idioma: traduccionAnunc.idioma.codigo,
          estado: codEstadoTAnuncio,
          anuncio: idAnuncio,
        };
        //Crea la traduccion del anuncio
        const crearTraduccionProyecto = await new this.traduccionAnuncioModel(
          newTraduccionAnuncio,
        ).save(opts);

        const dataTraduccion = JSON.parse(
          JSON.stringify(crearTraduccionProyecto),
        );
        delete dataTraduccion.fechaCreacion;
        delete dataTraduccion.fechaActualizacion;
        delete dataTraduccion.__v;

        let newHistoricoTraduccionAnuncio: any = {
          datos: dataTraduccion,
          usuario: anuncioDto.perfil._id,
          accion: getAccion,
          entidad: codEntidadTAnuncio,
        };

        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoTraduccionAnuncio,
        );
        codTraduccionAnuncio.push(dataTraduccion._id);
      }

      //detectar el idioma que se envia el texto

      let textoTraducido = await traducirTexto(
        codIdiom,
        anuncioDto.tematica[0].titulo,
      );
      let idiomaDetectado = textoTraducido.idiomaDetectado;

      //Obtiene el idioma por el codigo
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idiomaDetectado,
      );
      let codIdioma = idioma.codigo;

      let codTraduccionTematica = [];
      for (const traduccionTematica of anuncioDto.tematica) {
        let newTematicaAnuncio = {
          titulo: traduccionTematica.titulo,
          idioma: codIdioma,
          original: true,
          estado: codEstadoTematica,
          anuncio: idAnuncio,
        };
        //Crea la traduccion del anuncio
        const crearTraduccionTematica = await new this.tematicaAnuncioModel(
          newTematicaAnuncio,
        ).save(opts);
        const dataTematica = JSON.parse(
          JSON.stringify(crearTraduccionTematica),
        );
        delete dataTematica.fechaCreacion;
        delete dataTematica.fechaActualizacion;
        delete dataTematica.__v;

        let newHistoricoTraduccionTematica: any = {
          datos: dataTematica,
          usuario: anuncioDto.perfil._id,
          accion: getAccion,
          entidad: codEntidadTematicaAnuncio,
        };

        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoTraduccionTematica,
        );
        codTraduccionTematica.push(dataTematica._id);
      }

      // if(!anuncioDto.fechaInicio || !anuncioDto.fechaFin){
      //   anuncioDto.fechaInicio = null;
      //   anuncioDto.fechaFin = null;
      // }

      const getUltimoAnuncio = await this.anuncioModel
        .find()
        .sort({ fechaCreacion: -1 })
        .limit(1);
      let codigoUltAnuncio;
      //Obtiene el codigo del ultimo anuncio
      let codigoAnuncio;
      if (getUltimoAnuncio.length > 0) {
        codigoUltAnuncio = parseInt(getUltimoAnuncio[0].codigo);
        codigoUltAnuncio = codigoUltAnuncio + rangoNumericoAnuncios.inicio;
        codigoAnuncio = codigoUltAnuncio.toString();
      } else {
        codigoAnuncio = rangoNumericoAnuncios.inicio.toString();
      }

      let configuracionEstiloAnuncios = [];
      for (const configuracionAnuncion of anuncioDto.configuraciones) {
        let estiloAnuncios = [];
        for (const estilosAnuncio of configuracionAnuncion.estilos) {
          // let dataEstilosAnuncio = {
          //   media: null,
          //   color: estilosAnuncio.color,
          //   tipo: estilosAnuncio.tipo.codigo,
          //   estado: estadosEstilosAnuncio.activa
          // }
          // //Guarda el estilo del anuncio
          // const crearEstiloAnuncio = await new this.estiloAnunciosModel(dataEstilosAnuncio).save(opts);

          estiloAnuncios.push(estilosAnuncio._id);
        }
        let dataConfiguracionEstilosAnuncios = {
          estilos: estiloAnuncios,
          tipo: codigosCatalogoConfiguracion.personalizada,
          estado: estadosConfiguracionEstilosAnuncio.activa,
        };
        //Guarda la configuracion estilo del anuncio
        const crearConfiguracionEstiloAnuncio = await new this.configuracionEstiloAnunciosModel(
          dataConfiguracionEstilosAnuncios,
        ).save(opts);
        configuracionEstiloAnuncios.push(crearConfiguracionEstiloAnuncio._id);
      }

      let nuevoAnuncio = {
        _id: idAnuncio,
        codigo: codigoAnuncio,
        perfil: anuncioDto.perfil,
        tipo: anuncioDto.tipo.codigo,
        activo: anuncioDto.activo,
        favorito: false,
        traducciones: codTraduccionAnuncio,
        tematica: codTraduccionTematica,
        configuraciones: configuracionEstiloAnuncios,
        orden: anuncioDto?.orden || null,
        intervaloVisualizacion: anuncioDto?.intervaloVisualizacion || null,
        tiempoVisualizacion: anuncioDto?.tiempoVisualizacion || null,
        estado: codEstado,
        fechaInicio: anuncioDto.fechaInicio || null,
        fechaFin: anuncioDto.fechaFin || null,
        horaInicio: anuncioDto?.horaInicio || null,
        horaFin: anuncioDto?.horaFin || null,
        fechaCreacion: new Date(),
        fechaActualizacion: new Date(),
      };
      //Guarda el anuncio
      const crearAnuncio = await new this.anuncioModel(nuevoAnuncio).save(opts);

      const dataAnuncio = JSON.parse(JSON.stringify(crearAnuncio));
      delete dataAnuncio.__v;

      let newHistoricoAnuncio: any = {
        descripcion: dataAnuncio,
        usuario: anuncioDto.perfil._id,
        accion: getAccion,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistoricoAnuncio);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      await this.traducirTematicaServiceSegundoPlano.traducirTematicaSegundoPlano(
        crearAnuncio._id,
        anuncioDto.perfil._id,
        idiomaDetectado,
      );
      return crearAnuncio;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
