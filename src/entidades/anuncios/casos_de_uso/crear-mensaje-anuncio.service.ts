import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import {
  codigoEntidades,
  estadosMensajesAnuncio,
  nombreAcciones,
} from 'src/shared/enum-sistema';
import { CrearMensajesAnuncioDto } from '../entidad/mensajes-anuncio-dto';
import { MensajesAnuncio } from 'src/drivers/mongoose/interfaces/mensajes_anuncio/mensajes-anuncio.interface';
const sw = require('stopword');
const mongoose = require('mongoose');

@Injectable()
export class CrearMensajesAnuncioService {
  constructor(
    @Inject('MENSAJES_ANUNCIO_MODEL')
    private readonly crearMensajeAnuncioModel: Model<MensajesAnuncio>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
  ) {}

  async crearMensajesAnuncio(
    crearMensaje: CrearMensajesAnuncioDto,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      // //Verifica si el perfil es un perfil administrador de anuncios de gazelook
      // let adminAnunciosGazelook = await this.verificarPerfilAnuncioService.verificarPerfilAdministradorAnuncios(anuncioDto.perfil._id);

      // if (!adminAnunciosGazelook) {
      //   throw {
      //     codigo: HttpStatus.UNAUTHORIZED,
      //     codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
      //   };
      // }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let getAccion = accion.codigo;

      let nuevoMensajeAnuncio = {
        perfil: crearMensaje.perfil._id,
        texto: crearMensaje.texto,
        estado: estadosMensajesAnuncio.activa,
        fechaCreacion: new Date(),
        fechaActualizacion: new Date(),
      };
      //Guarda el anuncio
      const crearMensajeAnuncio = await new this.crearMensajeAnuncioModel(
        nuevoMensajeAnuncio,
      ).save(opts);

      const dataMensajeAnuncio = JSON.parse(
        JSON.stringify(crearMensajeAnuncio),
      );
      delete dataMensajeAnuncio.__v;

      let newHistoricoAnuncio: any = {
        descripcion: dataMensajeAnuncio,
        usuario: crearMensaje.perfil._id,
        accion: getAccion,
        entidad: codigoEntidades.mensajesAnuncio,
      };

      this.crearHistoricoService.crearHistoricoServer(newHistoricoAnuncio);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return crearMensajeAnuncio;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
