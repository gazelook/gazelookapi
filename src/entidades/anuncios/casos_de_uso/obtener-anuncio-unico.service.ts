import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { TraduccionAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_anuncio/traduccion_anuncio.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import {
  codidosEstadosAnuncio,
  codidosEstadosTraduccionAnuncio,
  codigosEstadosTematicaAnuncio,
  estadosConfiguracionEstilosAnuncio,
  estadosEstilosAnuncio,
} from 'src/shared/enum-sistema';

const mongoose = require('mongoose');

@Injectable()
export class ObtenerAnuncioUnicoService {
  proyecto: any;

  constructor(
    @Inject('ANUNCIO_MODEL') private readonly anuncioModel: Model<Anuncio>,
    @Inject('TRADUCCION_ANUNCIO_MODEL')
    private readonly traduccionAnuncioModel: Model<TraduccionAnuncio>,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async obtenerAnuncioUnico(
    idAnuncio: string,
    idPerfil: string,
    codIdioma: string,
  ): Promise<any> {
    console.log('**************************++');
    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codIdioma,
    );

    //Obtiene el codigo del idioma
    let codIdi = idioma.codigo;

    try {
      const getAnuncio = await this.anuncioModel
        .findOne({
          _id: idAnuncio,
          estado: codidosEstadosAnuncio.activa,
        })
        .populate({
          path: 'traducciones',
          match: {
            estado: codidosEstadosTraduccionAnuncio.activa,
            idioma: codIdi,
          },
        })
        .populate({
          path: 'tematica',
          match: {
            estado: codigosEstadosTematicaAnuncio.activa,
            idioma: codIdi,
          },
        })
        .populate({
          path: 'configuraciones',
          match: {
            estado: estadosConfiguracionEstilosAnuncio.activa,
          },
          populate: {
            path: 'estilos',
            match: {
              estado: estadosEstilosAnuncio.activa,
            },
          },
        });

      if (getAnuncio) {
        let traduccionAnuncios = [];
        if (getAnuncio.traducciones.length > 0) {
          let traduccion = {
            titulo: getAnuncio.traducciones[0]['titulo'],
            descripcion: getAnuncio.traducciones[0]['descripcion'],
            idioma: {
              codigo: getAnuncio.traducciones[0]['idioma'],
            },
          };
          traduccionAnuncios.push(traduccion);
        }

        let traduccionTematica = [];
        if (getAnuncio?.tematica && getAnuncio.tematica.length > 0) {
          let traduccion = {
            titulo: getAnuncio.tematica[0]['titulo'],
            idioma: {
              codigo: getAnuncio.tematica[0]['idioma'],
            },
          };
          traduccionTematica.push(traduccion);
        }

        let arrayConfiguracionEstilosAnuncio = [];
        if (
          getAnuncio?.configuraciones &&
          getAnuncio?.configuraciones.length > 0
        ) {
          for (const getConfiguracionAnuncio of getAnuncio.configuraciones) {
            let arrayEstilosAnuncio = [];
            if (getConfiguracionAnuncio['estilos'].length > 0) {
              for (const getEstilosAnuncios of getConfiguracionAnuncio[
                'estilos'
              ]) {
                let objEstilosAnuncios = {
                  _id: getEstilosAnuncios._id,
                  color: getEstilosAnuncios.color,
                  tipo: {
                    codigo: getEstilosAnuncios.tipo,
                  },
                  tamanioLetra: getEstilosAnuncios?.tamanioLetra || null,
                };
                arrayEstilosAnuncio.push(objEstilosAnuncios);
              }
            }
            let objConfiguracionEstilosAnuncio = {
              _id: getConfiguracionAnuncio['_id'],
              estilos: arrayEstilosAnuncio,
            };
            arrayConfiguracionEstilosAnuncio.push(
              objConfiguracionEstilosAnuncio,
            );
          }
        }

        let datos = {
          _id: getAnuncio._id,
          codigo: getAnuncio.codigo || null,
          traducciones: traduccionAnuncios,
          tematica: traduccionTematica,
          activo: getAnuncio.activo,
          favorito: getAnuncio.favorito,
          configuraciones: arrayConfiguracionEstilosAnuncio,
          orden: getAnuncio.orden || null,
          tiempoVisualizacion: getAnuncio?.tiempoVisualizacion || null,
          intervaloVisualizacion: getAnuncio?.intervaloVisualizacion || null,
          estado: {
            codigo: getAnuncio.estado,
          },
          fechaInicio: getAnuncio.fechaInicio,
          fechaFin: getAnuncio.fechaFin,
          horaInicio: getAnuncio.horaInicio,
          horaFin: getAnuncio.horaFin,
          fechaCreacion: getAnuncio.fechaCreacion,
          fechaActualizacion: getAnuncio.fechaActualizacion,
        };
        return datos;
      }
    } catch (error) {
      throw error;
    }
  }
}
