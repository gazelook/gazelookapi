import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { TraduccionTematicaAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_tematica_anuncio/traduccion_tematica_anuncio.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import {
  codigosEstadosTematicaAnuncio,
  estadosConfiguracionEstilosAnuncio,
  estadosEstilosAnuncio,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { TraducirTematicaService } from './traducir-tematica.service';

const mongoose = require('mongoose');

@Injectable()
export class BuscarAnunciosRecientesPaginacionService {
  constructor(
    @Inject('ANUNCIO_MODEL')
    private readonly anuncioModel: PaginateModel<Anuncio>,
    @Inject('TEMATICA_ANUNCIO_MODEL')
    private readonly tematicaAnuncioModel: PaginateModel<
      TraduccionTematicaAnuncio
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirTematicaService: TraducirTematicaService,
  ) {}

  async buscarAnuncios(
    codIdim: string,
    limite: number,
    pagina: number,
    tipo: string,
    titulo: any,
    codigo: string,
    activo: any,
    favorito: any,
    fecha: any,
    response: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const headerNombre = new HadersInterfaceNombres();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtener el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdim,
      );
      let codIdioma = idioma.codigo;

      //Obtiene la entidad traduccionAnuncio
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionAnuncio,
      );
      let codEntidadTraduccion = entidad.codigo;

      //Obtiene el estado activa de la entidad traduccion anuncio
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTraduccion,
      );
      let codEstadoTradAnuncio = estado.codigo;

      //Obtiene el codigo de la entidad anuncio
      const entidadAnuncio = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.anuncio,
      );
      let codEntidadAnuncio = entidadAnuncio.codigo;

      //Obtiene el estado activa de la entidad anuncio
      const estadoAnuncio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAnuncio,
      );
      let codEstadoAnuncio = estadoAnuncio.codigo;

      //Obtiene el codigo de la entidad tematica
      const entidadTematica = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.tematicaAnuncio,
      );
      let codEntidadTematicaAnuncio = entidadTematica.codigo;

      //Obtiene el estado activa de la entidad anuncio
      const estadoTematicaAnuncio = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTematicaAnuncio,
      );
      let regex1;
      let regex2;
      let regex3;
      let regex4;
      let regex5;
      let regex6;
      let regex7;

      if (titulo) {
        //Obtener ultimo digito
        let ultimoCaracter = titulo?.charAt(titulo.length - 1);
        //Obtener ultimo digito
        let penultimoCaracter = titulo?.charAt(titulo.length - 2);

        let ultDosCracteres = penultimoCaracter.concat(ultimoCaracter);
        console.log('ultDosCracteres: ', ultDosCracteres);

        let eliminar_caracteres = [
          'as',
          'es',
          'is',
          'os',
          'us',
          'AS',
          'ES',
          'IS',
          'OS',
          'US',
        ];
        let eliminarS = ['s', 'S'];
        let nuevoTitulo;
        //Elimina la plural de los dos ultimos caracteres de la palabra que se envia
        for (const recorreCaracter of eliminar_caracteres) {
          if (recorreCaracter === ultDosCracteres) {
            nuevoTitulo = titulo.substring(0, titulo.length - 1);
            nuevoTitulo = nuevoTitulo.substring(0, nuevoTitulo.length - 1);
          }
        }

        let nuevoTitulo1;
        //Elimina la s (S) del ultimo caracter de la palabra que se envia
        for (const recorreCaracter of eliminarS) {
          if (recorreCaracter === ultimoCaracter) {
            nuevoTitulo1 = titulo.substring(0, titulo.length - 1);
          }
        }

        console.log('nuevoTitulo: ', nuevoTitulo);
        //Expresion regular de solo busqueda por palabras  completas
        regex1 = '\\b'.concat(titulo).concat('\\b');
        console.log('regex1: ', regex1);

        if (nuevoTitulo) {
          //Expresion regular por fin de palabras
          regex2 = nuevoTitulo.concat('\\b');
          console.log('regex2: ', regex2);

          //Expresion regular de solo busqueda por palabras  completas
          regex3 = '\\b'.concat(nuevoTitulo).concat('\\b');
          console.log('regex3: ', regex3);

          //Expresion regular por inicio de palabras
          regex7 = ('\\b').concat(nuevoTitulo);
          console.log('regex7: ', regex7);

        } else {
          //Expresion regular por fin de palabras
          regex2 = titulo.concat('\\b');
          console.log('regex2: ', regex2);

          //Expresion regular de solo busqueda por palabras  completas
          regex3 = '\\b'.concat(titulo).concat('\\b');
          console.log('regex3: ', regex3);

          //Expresion regular por inicio de palabras
          regex7 = ('\\b').concat(titulo);
          console.log('regex2: ', regex7);
        }
        if (nuevoTitulo1) {
          //Expresion regular por fin de palabras
          regex5 = nuevoTitulo1.concat('\\b');
          console.log('regex2: ', regex5);

          //Expresion regular de solo busqueda por palabras  completas
          regex6 = '\\b'.concat(nuevoTitulo1).concat('\\b');
          console.log('regex3: ', regex6);

          //Expresion regular por inicio de palabras
          regex7 = ('\\b').concat(nuevoTitulo1);
          console.log('regex7: ', regex7);
        } else {
          //Expresion regular por fin de palabras
          regex5 = titulo.concat('\\b');
          console.log('regex2: ', regex5);

          //Expresion regular de solo busqueda por palabras  completas
          regex6 = '\\b'.concat(titulo).concat('\\b');
          console.log('regex3: ', regex6);

          //Expresion regular por inicio de palabras
          regex7 = ('\\b').concat(titulo);
          console.log('regex7: ', regex7);
        }

        //Expresion regular por fin de palabras
        regex4 = titulo.concat('\\b');
        console.log('regex2: ', regex4);
      }

      let I;
      if (fecha) {
        const date = fecha + 'T05:00:00.000Z';
        // 'T05:00:00.000+00:00';
        I = new Date(date);
      } else {
        fecha = null;
      }
      if (!codigo) {
        codigo = null;
      }

      const populateTraduccion = {
        path: 'traducciones',
        select: '-fechaActualizacion -fechaCreacion -__v',
        match: {
          // idioma: codIdioma,
          estado: codEstadoTradAnuncio,
        },
      };

      const populateTematica = {
        path: 'tematica',
        match: {
          estado: codigosEstadosTematicaAnuncio.activa,
          idioma: codIdioma,
        },
      };

      const populateConfiguraciones = {
        path: 'configuraciones',
        match: {
          estado: estadosConfiguracionEstilosAnuncio.activa,
        },
        populate: {
          path: 'estilos',
          match: {
            estado: estadosEstilosAnuncio.activa,
          },
        },
      };

      const populatePerfil = {
        path: 'perfil',
        select: '_id nombre nombreContacto',
      };

      const populateAnuncio = {
        path: 'anuncio',
        populate: [
          populatePerfil,
          populateTematica,
          populateTraduccion,
          populateConfiguraciones,
        ],
      };

      let totalAnuncios: any;

      if (
        tipo &&
        !fecha &&
        !codigo &&
        favorito === undefined &&
        activo === undefined &&
        !titulo
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        //Obtiene intercambios segun en rango de fecha
        totalAnuncios = await this.anuncioModel.paginate(
          { $and: [{ tipo: tipo }, { estado: codEstadoAnuncio }] },
          options,
        );

        // totalIntercambios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options);
      }

      if (
        !tipo &&
        !fecha &&
        !codigo &&
        favorito === undefined &&
        activo === undefined &&
        !titulo
      ) {
        const options = {
          session: session,
          lean: true,
          sort: 'codigo',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        //Obtiene intercambios segun en rango de fecha
        totalAnuncios = await this.anuncioModel.paginate(
          { $and: [{ estado: codEstadoAnuncio }] },
          options,
        );

        // totalIntercambios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options);
      }

      if (
        tipo &&
        fecha &&
        !codigo &&
        favorito === undefined &&
        activo === undefined &&
        !titulo
      ) {
        console.log('tipo');
        console.log('fecha: ', I);

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            tipo: tipo,
            estado: codEstadoAnuncio,
            fechaInicio: { $lte: I },
            fechaFin: { $gte: I },
          },
          options,
        );
        console.log(totalAnuncios.docs.length);
      }
      if (
        tipo &&
        fecha &&
        codigo &&
        favorito === undefined &&
        activo === undefined &&
        !titulo
      ) {
        console.log('tipo fecha codigo');
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            tipo: tipo,
            codigo: codigo,
            estado: codEstadoAnuncio,
            fechaInicio: { $lte: I },
            fechaFin: { $gte: I },
          },
          options,
        );
      }

      if (
        tipo &&
        fecha &&
        codigo &&
        favorito != undefined &&
        activo === undefined &&
        !titulo
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            tipo: tipo,
            codigo: codigo,
            favorito: favorito,
            estado: codEstadoAnuncio,
            fechaInicio: { $lte: I },
            fechaFin: { $gte: I },
          },
          options,
        );
      }

      if (
        tipo &&
        fecha &&
        codigo &&
        favorito != undefined &&
        activo != undefined &&
        !titulo
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            tipo: tipo,
            codigo: codigo,
            favorito: favorito,
            activo: activo,
            estado: codEstadoAnuncio,
            fechaInicio: { $lte: I },
            fechaFin: { $gte: I },
          },
          options,
        );
      }

      if (
        tipo &&
        fecha &&
        codigo &&
        favorito != undefined &&
        activo != undefined &&
        titulo
      ) {
        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { titulo: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateAnuncio],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getAnuncios = await this.anuncioModel.find({
          tipo: tipo,
          codigo: codigo,
          favorito: favorito,
          activo: activo,
          estado: codEstadoAnuncio,
          fechaInicio: { $lte: I },
          fechaFin: { $gte: I },
        });

        let arrayAnuncios = [];
        if (getAnuncios.length > 0) {
          for (const anuncio of getAnuncios) {
            arrayAnuncios.push(anuncio._id);
          }
        }

        let totalTematica = await this.tematicaAnuncioModel.find({
          $or: [
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex1, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex2, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex3, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex5, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex6, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex7, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayAnuncios = [];
        if (totalTematica.length > 0) {
          for (const tematica of totalTematica) {
            const getIdiomaTematica = await this.tematicaAnuncioModel.find({
              $and: [
                { anuncio: tematica.anuncio },
                { estado: codigosEstadosTematicaAnuncio.activa },
              ],
            });

            const find = getIdiomaTematica.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayAnuncios.push(find._id);
            }
          }
        }

        totalAnuncios = await this.tematicaAnuncioModel.paginate(
          {
            _id: { $in: arrayAnuncios },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataAnuncios = await this.obtenerTematicaAnuncio(
          totalAnuncios,
          codIdim,
          codIdioma,
          codigosEstadosTematicaAnuncio.activa,
          opts,
        );

        response.set(headerNombre.totalDatos, totalAnuncios.totalDocs);
        response.set(headerNombre.totalPaginas, totalAnuncios.totalPages);
        response.set(headerNombre.proximaPagina, totalAnuncios.hasNextPage);
        response.set(headerNombre.anteriorPagina, totalAnuncios.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataAnuncios;
      }

      if (
        !tipo &&
        fecha &&
        !codigo &&
        favorito === undefined &&
        activo === undefined &&
        !titulo
      ) {
        console.log('11111111');
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            estado: codEstadoAnuncio,
            fechaInicio: { $lte: I },
            fechaFin: { $gte: I },
          },
          options,
        );
      }

      if (
        !tipo &&
        !fecha &&
        codigo &&
        favorito === undefined &&
        activo === undefined &&
        !titulo
      ) {
        console.log('codigo');
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          { $and: [{ codigo: codigo }, { estado: codEstadoAnuncio }] },
          options,
        );
      }
      if (
        !tipo &&
        !fecha &&
        !codigo &&
        favorito != undefined &&
        activo === undefined &&
        !titulo
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };
        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          { $and: [{ favorito: favorito }, { estado: codEstadoAnuncio }] },
          options,
        );
      }

      if (
        !tipo &&
        !fecha &&
        !codigo &&
        favorito === undefined &&
        activo != undefined &&
        !titulo
      ) {
        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          { $and: [{ activo: activo }, { estado: codEstadoAnuncio }] },
          options,
        );
      }

      if (
        !tipo &&
        !fecha &&
        !codigo &&
        favorito === undefined &&
        activo === undefined &&
        titulo
      ) {
        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { titulo: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateAnuncio],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getAnuncios = await this.anuncioModel.find({
          $and: [{ estado: codEstadoAnuncio }],
        });

        let arrayAnuncios = [];
        if (getAnuncios.length > 0) {
          for (const anuncio of getAnuncios) {
            arrayAnuncios.push(anuncio._id);
          }
        }

        let totalTematica = await this.tematicaAnuncioModel.find({
          $or: [
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex1, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex2, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex3, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex5, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex6, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex7, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            }
          ],
        });

        arrayAnuncios = [];

        if (totalTematica.length > 0) {
          for (const tematica of totalTematica) {
            const getIdiomaTematica = await this.tematicaAnuncioModel.find({
              $and: [
                { anuncio: tematica.anuncio },
                { estado: codigosEstadosTematicaAnuncio.activa },
              ],
            });

            const find = getIdiomaTematica.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayAnuncios.push(find._id);
            }
          }
        }

        totalAnuncios = await this.tematicaAnuncioModel.paginate(
          {
            _id: { $in: arrayAnuncios },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataAnuncios = await this.obtenerTematicaAnuncio(
          totalAnuncios,
          codIdim,
          codIdioma,
          codigosEstadosTematicaAnuncio.activa,
          opts,
        );

        response.set(headerNombre.totalDatos, totalAnuncios.totalDocs);
        response.set(headerNombre.totalPaginas, totalAnuncios.totalPages);
        response.set(headerNombre.proximaPagina, totalAnuncios.hasNextPage);
        response.set(headerNombre.anteriorPagina, totalAnuncios.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataAnuncios;
      }

      if (
        !tipo &&
        fecha &&
        codigo &&
        favorito === undefined &&
        activo === undefined &&
        !titulo
      ) {
        console.log('fecha');
        console.log('codigo');

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            codigo: codigo,
            estado: codEstadoAnuncio,
            fechaInicio: { $lte: I },
            fechaFin: { $gte: I },
          },
          options,
        );
      }

      if (
        !tipo &&
        fecha &&
        !codigo &&
        favorito != undefined &&
        activo === undefined &&
        !titulo
      ) {
        console.log('fecha');
        console.log('favorito');

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            favorito: favorito,
            estado: codEstadoAnuncio,
            fechaInicio: { $lte: I },
            fechaFin: { $gte: I },
          },
          options,
        );
      }

      if (
        !tipo &&
        fecha &&
        !codigo &&
        favorito === undefined &&
        activo != undefined &&
        !titulo
      ) {
        console.log('fecha');
        console.log('activo');

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            activo: activo,
            estado: codEstadoAnuncio,
            fechaInicio: { $lte: I },
            fechaFin: { $gte: I },
          },
          options,
        );
      }

      if (
        !tipo &&
        fecha &&
        !codigo &&
        favorito === undefined &&
        activo === undefined &&
        titulo
      ) {
        console.log('fecha');
        console.log('titulo');

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { titulo: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateAnuncio],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getAnuncios = await this.anuncioModel.find({
          estado: codEstadoAnuncio,
          fechaInicio: { $lte: I },
          fechaFin: { $gte: I },
        });

        let arrayAnuncios = [];
        if (getAnuncios.length > 0) {
          for (const anuncio of getAnuncios) {
            arrayAnuncios.push(anuncio._id);
          }
        }

        let totalTematica = await this.tematicaAnuncioModel.find({
          $or: [
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex1, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex2, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex3, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex5, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex6, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex7, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayAnuncios = [];

        if (totalTematica.length > 0) {
          for (const tematica of totalTematica) {
            const getIdiomaTematica = await this.tematicaAnuncioModel.find({
              $and: [
                { anuncio: tematica.anuncio },
                { estado: codigosEstadosTematicaAnuncio.activa },
              ],
            });

            const find = getIdiomaTematica.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayAnuncios.push(find._id);
            }
          }
        }

        totalAnuncios = await this.tematicaAnuncioModel.paginate(
          {
            _id: { $in: arrayAnuncios },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataAnuncios = await this.obtenerTematicaAnuncio(
          totalAnuncios,
          codIdim,
          codIdioma,
          codigosEstadosTematicaAnuncio.activa,
          opts,
        );

        response.set(headerNombre.totalDatos, totalAnuncios.totalDocs);
        response.set(headerNombre.totalPaginas, totalAnuncios.totalPages);
        response.set(headerNombre.proximaPagina, totalAnuncios.hasNextPage);
        response.set(headerNombre.anteriorPagina, totalAnuncios.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataAnuncios;
      }
      if (
        !tipo &&
        !fecha &&
        codigo &&
        favorito != undefined &&
        activo === undefined &&
        !titulo
      ) {
        console.log('codigo');
        console.log('favorito');

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            $and: [
              { codigo: codigo },
              { favorito: favorito },
              { estado: codEstadoAnuncio },
            ],
          },
          options,
        );
      }

      if (
        !tipo &&
        !fecha &&
        codigo &&
        favorito === undefined &&
        activo != undefined &&
        !titulo
      ) {
        console.log('codigo');
        console.log('activo');

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            $and: [
              { codigo: codigo },
              { activo: activo },
              { estado: codEstadoAnuncio },
            ],
          },
          options,
        );
      }

      if (
        !tipo &&
        !fecha &&
        codigo &&
        favorito === undefined &&
        activo === undefined &&
        titulo
      ) {
        console.log('codigo');
        console.log('titulo');

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { titulo: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateAnuncio],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getAnuncios = await this.anuncioModel.find({
          $and: [{ estado: codEstadoAnuncio }, { codigo: codigo }],
        });

        let arrayAnuncios = [];
        if (getAnuncios.length > 0) {
          for (const anuncio of getAnuncios) {
            arrayAnuncios.push(anuncio._id);
          }
        }

        let totalTematica = await this.tematicaAnuncioModel.find({
          $or: [
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex1, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex2, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex3, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex5, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex6, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex7, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayAnuncios = [];

        if (totalTematica.length > 0) {
          for (const tematica of totalTematica) {
            const getIdiomaTematica = await this.tematicaAnuncioModel.find({
              $and: [
                { anuncio: tematica.anuncio },
                { estado: codigosEstadosTematicaAnuncio.activa },
              ],
            });

            const find = getIdiomaTematica.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayAnuncios.push(find._id);
            }
          }
        }

        totalAnuncios = await this.tematicaAnuncioModel.paginate(
          {
            _id: { $in: arrayAnuncios },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataAnuncios = await this.obtenerTematicaAnuncio(
          totalAnuncios,
          codIdim,
          codIdioma,
          codigosEstadosTematicaAnuncio.activa,
          opts,
        );

        response.set(headerNombre.totalDatos, totalAnuncios.totalDocs);
        response.set(headerNombre.totalPaginas, totalAnuncios.totalPages);
        response.set(headerNombre.proximaPagina, totalAnuncios.hasNextPage);
        response.set(headerNombre.anteriorPagina, totalAnuncios.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataAnuncios;
      }

      if (
        !tipo &&
        !fecha &&
        !codigo &&
        favorito != undefined &&
        activo != undefined &&
        !titulo
      ) {
        console.log('favorito');
        console.log('activo');

        const options = {
          session: session,
          lean: true,
          sort: '-fechaCreacion',
          select: ' -__v ',
          populate: [
            populateTraduccion,
            populatePerfil,
            populateTematica,
            populateConfiguraciones,
          ],
          page: Number(pagina),
          limit: Number(limite),
        };

        // totalAnuncios = await this.anuncioModel.paginate({ $and: [{ tipo: tipo }, { estado: codEstadoIntercambio }] }, options)
        totalAnuncios = await this.anuncioModel.paginate(
          {
            $and: [
              { favorito: favorito },
              { activo: activo },
              { estado: codEstadoAnuncio },
            ],
          },
          options,
        );
      }

      if (
        !tipo &&
        !fecha &&
        !codigo &&
        favorito != undefined &&
        activo === undefined &&
        titulo
      ) {
        console.log('favorito');
        console.log('titulo');

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { titulo: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateAnuncio],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getAnuncios = await this.anuncioModel.find({
          $and: [{ estado: codEstadoAnuncio }, { favorito: favorito }],
        });

        let arrayAnuncios = [];
        if (getAnuncios.length > 0) {
          for (const anuncio of getAnuncios) {
            arrayAnuncios.push(anuncio._id);
          }
        }

        let totalTematica = await this.tematicaAnuncioModel.find({
          $or: [
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex1, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex2, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex3, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex5, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex6, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex7, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayAnuncios = [];

        if (totalTematica.length > 0) {
          for (const tematica of totalTematica) {
            const getIdiomaTematica = await this.tematicaAnuncioModel.find({
              $and: [
                { anuncio: tematica.anuncio },
                { estado: codigosEstadosTematicaAnuncio.activa },
              ],
            });

            const find = getIdiomaTematica.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayAnuncios.push(find._id);
            }
          }
        }

        totalAnuncios = await this.tematicaAnuncioModel.paginate(
          {
            _id: { $in: arrayAnuncios },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataAnuncios = await this.obtenerTematicaAnuncio(
          totalAnuncios,
          codIdim,
          codIdioma,
          codigosEstadosTematicaAnuncio.activa,
          opts,
        );

        response.set(headerNombre.totalDatos, totalAnuncios.totalDocs);
        response.set(headerNombre.totalPaginas, totalAnuncios.totalPages);
        response.set(headerNombre.proximaPagina, totalAnuncios.hasNextPage);
        response.set(headerNombre.anteriorPagina, totalAnuncios.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataAnuncios;
      }

      if (
        !tipo &&
        !fecha &&
        !codigo &&
        favorito === undefined &&
        activo != undefined &&
        titulo
      ) {
        console.log('activo');
        console.log('titulo');

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { titulo: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateAnuncio],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getAnuncios = await this.anuncioModel.find({
          $and: [{ estado: codEstadoAnuncio }, { activo: activo }],
        });

        let arrayAnuncios = [];
        if (getAnuncios.length > 0) {
          for (const anuncio of getAnuncios) {
            arrayAnuncios.push(anuncio._id);
          }
        }

        let totalTematica = await this.tematicaAnuncioModel.find({
          $or: [
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex1, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex2, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex3, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex5, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex6, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex7, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayAnuncios = [];

        if (totalTematica.length > 0) {
          for (const tematica of totalTematica) {
            const getIdiomaTematica = await this.tematicaAnuncioModel.find({
              $and: [
                { anuncio: tematica.anuncio },
                { estado: codigosEstadosTematicaAnuncio.activa },
              ],
            });

            const find = getIdiomaTematica.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayAnuncios.push(find._id);
            }
          }
        }

        totalAnuncios = await this.tematicaAnuncioModel.paginate(
          {
            _id: { $in: arrayAnuncios },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataAnuncios = await this.obtenerTematicaAnuncio(
          totalAnuncios,
          codIdim,
          codIdioma,
          codigosEstadosTematicaAnuncio.activa,
          opts,
        );

        response.set(headerNombre.totalDatos, totalAnuncios.totalDocs);
        response.set(headerNombre.totalPaginas, totalAnuncios.totalPages);
        response.set(headerNombre.proximaPagina, totalAnuncios.hasNextPage);
        response.set(headerNombre.anteriorPagina, totalAnuncios.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataAnuncios;
      }

      if (
        !tipo &&
        !fecha &&
        !codigo &&
        favorito === undefined &&
        activo === undefined &&
        titulo
      ) {
        console.log('titulo');

        const options = {
          lean: true,
          session: opts.session,
          collation: { locale: 'en' },
          sort: { titulo: 1, fechaCreacion: -1 },
          select: ' -__v ',
          populate: [populateAnuncio],
          page: Number(pagina),
          limit: Number(limite),
        };

        let getAnuncios = await this.anuncioModel.find({
          $and: [{ estado: codEstadoAnuncio }],
        });

        let arrayAnuncios = [];
        if (getAnuncios.length > 0) {
          for (const anuncio of getAnuncios) {
            arrayAnuncios.push(anuncio._id);
          }
        }

        let totalTematica = await this.tematicaAnuncioModel.find({
          $or: [
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex1, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex2, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex3, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex5, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex6, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
            {
              $and: [
                { anuncio: { $in: arrayAnuncios } },
                { titulo: { $regex: regex7, $options: 'i' } },
                { estado: codigosEstadosTematicaAnuncio.activa },
                // { idioma: codIdioma }
              ],
            },
          ],
        });

        arrayAnuncios = [];

        if (totalTematica.length > 0) {
          for (const tematica of totalTematica) {
            const getIdiomaTematica = await this.tematicaAnuncioModel.find({
              $and: [
                { anuncio: tematica.anuncio },
                { estado: codigosEstadosTematicaAnuncio.activa },
              ],
            });

            const find = getIdiomaTematica.find(
              element => element.idioma === codIdioma,
            );
            if (find) {
              arrayAnuncios.push(find._id);
            }
          }
        }

        totalAnuncios = await this.tematicaAnuncioModel.paginate(
          {
            _id: { $in: arrayAnuncios },
          },
          options,
        );

        // totalIntercambios = await this.intercambioModel.paginate({ $and: [{ tipo: tipo }, { fechaCreacion: { $gt: I } }, { fechaCreacion: { $lt: F } }, { estado: codEstadoIntercambio }] }, options)
        let dataAnuncios = await this.obtenerTematicaAnuncio(
          totalAnuncios,
          codIdim,
          codIdioma,
          codigosEstadosTematicaAnuncio.activa,
          opts,
        );

        response.set(headerNombre.totalDatos, totalAnuncios.totalDocs);
        response.set(headerNombre.totalPaginas, totalAnuncios.totalPages);
        response.set(headerNombre.proximaPagina, totalAnuncios.hasNextPage);
        response.set(headerNombre.anteriorPagina, totalAnuncios.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataAnuncios;
      }

      if (totalAnuncios.docs.length > 0) {
        const arrayAnuncios = [];
        for (const getAnuncio of totalAnuncios.docs) {
          //Traduce el intercambio en caso de no existir en el idioma enviado
          let getTraducirTematica: any;

          const getIdiomaTematica = await this.tematicaAnuncioModel.find({
            $and: [
              { anuncio: getAnuncio._id },
              { estado: codigosEstadosTematicaAnuncio.activa },
            ],
          });

          if (getIdiomaTematica.length > 0) {
            const find = getIdiomaTematica.find(
              element => element.idioma === codIdioma,
            );
            if (!find) {
              getTraducirTematica = await this.traducirTematicaService.traducirTematica(
                getAnuncio._id,
                getAnuncio.perfil._id,
                codIdim,
                opts,
              );
            } else {
              getTraducirTematica = [
                {
                  titulo: find.titulo,
                  idioma: {
                    codigo: find.idioma,
                  },
                },
              ];
            }
          } else {
            getTraducirTematica = [];
          }

          let traduccionAnuncios = [];
          if (getAnuncio.traducciones.length > 0) {
            for (const getTraducciones of getAnuncio.traducciones) {
              let traduccion = {
                titulo: getTraducciones['titulo'],
                descripcion: getTraducciones['descripcion'],
                idioma: {
                  codigo: getTraducciones['idioma'],
                },
              };
              traduccionAnuncios.push(traduccion);
            }
          }

          let arrayConfiguracionEstilosAnuncio = [];
          if (
            getAnuncio?.configuraciones &&
            getAnuncio?.configuraciones.length > 0
          ) {
            for (const getConfiguracionAnuncio of getAnuncio.configuraciones) {
              let arrayEstilosAnuncio = [];
              if (getConfiguracionAnuncio['estilos'].length > 0) {
                for (const getEstilosAnuncios of getConfiguracionAnuncio[
                  'estilos'
                ]) {
                  let objEstilosAnuncios = {
                    _id: getEstilosAnuncios._id,
                    color: getEstilosAnuncios.color,
                    tipo: {
                      codigo: getEstilosAnuncios.tipo,
                    },
                    tamanioLetra: getEstilosAnuncios?.tamanioLetra || null,
                  };
                  arrayEstilosAnuncio.push(objEstilosAnuncios);
                }
              }
              let objConfiguracionEstilosAnuncio = {
                _id: getConfiguracionAnuncio['_id'],
                estilos: arrayEstilosAnuncio,
              };
              arrayConfiguracionEstilosAnuncio.push(
                objConfiguracionEstilosAnuncio,
              );
            }
          }

          let datos = {
            _id: getAnuncio._id,
            codigo: getAnuncio.codigo || null,
            traducciones: traduccionAnuncios,
            tematica: getTraducirTematica,
            activo: getAnuncio.activo,
            perfil: getAnuncio.perfil,
            tipo: {
              codigo: getAnuncio.tipo,
            },
            favorito: getAnuncio.favorito,
            configuraciones: arrayConfiguracionEstilosAnuncio,
            orden: getAnuncio?.orden || null,
            tiempoVisualizacion: getAnuncio?.tiempoVisualizacion || null,
            intervaloVisualizacion: getAnuncio?.intervaloVisualizacion || null,
            estado: {
              codigo: getAnuncio.estado,
            },
            fechaInicio: getAnuncio.fechaInicio,
            fechaFin: getAnuncio.fechaFin,
            horaInicio: getAnuncio.horaInicio,
            horaFin: getAnuncio.horaFin,
            fechaCreacion: getAnuncio.fechaCreacion,
            fechaActualizacion: getAnuncio.fechaActualizacion,
          };

          arrayAnuncios.push(datos);
        }
        response.set(headerNombre.totalDatos, totalAnuncios.totalDocs);
        response.set(headerNombre.totalPaginas, totalAnuncios.totalPages);
        response.set(headerNombre.proximaPagina, totalAnuncios.hasNextPage);
        response.set(headerNombre.anteriorPagina, totalAnuncios.hasPrevPage);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return arrayAnuncios;
      } else {
        response.set(headerNombre.totalDatos, totalAnuncios.totalDocs);
        response.set(headerNombre.totalPaginas, totalAnuncios.totalPages);
        response.set(headerNombre.proximaPagina, totalAnuncios.hasNextPage);
        response.set(headerNombre.anteriorPagina, totalAnuncios.hasPrevPage);
        //Finaliza la transaccion
        await session.endSession();

        return totalAnuncios.docs;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async obtenerTematicaAnuncio(
    anuncios: any,
    codIdim,
    codIdioma,
    codEstadoTematicaAnuncio,
    opts,
  ): Promise<any> {
    let arrayAnuncios = [];
    if (anuncios.docs.length > 0) {
      for (const getAnuncio of anuncios.docs) {
        //Traduce el intercambio en caso de no existir en el idioma enviado
        let getTraducirTematica: any;

        const getIdiomaTematica = await this.tematicaAnuncioModel.find({
          $and: [
            { anuncio: getAnuncio.anuncio['_id'] },
            { estado: codEstadoTematicaAnuncio },
          ],
        });
        console.log('getAnuncio.anuncio[_id]: ', getAnuncio.anuncio['_id']);
        console.log('codEstadoTematicaAnuncio: ', codEstadoTematicaAnuncio);
        if (getIdiomaTematica.length > 0) {
          const find = getIdiomaTematica.find(
            element => element.idioma === codIdioma,
          );
          if (!find) {
            getTraducirTematica = await this.traducirTematicaService.traducirTematica(
              getAnuncio.anuncio['_id'],
              getAnuncio.anuncio['perfil']['_id'],
              codIdim,
              opts,
            );
          } else {
            getTraducirTematica = [
              {
                titulo: find.titulo,
                idioma: {
                  codigo: find.idioma,
                },
              },
            ];
          }
        } else {
          getTraducirTematica = [];
        }

        let traduccionAnuncios = [];
        if (
          getAnuncio.anuncio.traducciones &&
          getAnuncio.anuncio.traducciones.length > 0
        ) {
          for (const getTraducciones of getAnuncio.anuncio.traducciones) {
            let traduccion = {
              titulo: getTraducciones['titulo'],
              descripcion: getTraducciones['descripcion'],
              idioma: {
                codigo: getTraducciones['idioma'],
              },
            };
            traduccionAnuncios.push(traduccion);
          }
        }

        let arrayConfiguracionEstilosAnuncio = [];
        if (
          getAnuncio?.configuraciones &&
          getAnuncio?.configuraciones.length > 0
        ) {
          for (const getConfiguracionAnuncio of getAnuncio.configuraciones) {
            let arrayEstilosAnuncio = [];
            if (getConfiguracionAnuncio['estilos'].length > 0) {
              for (const getEstilosAnuncios of getConfiguracionAnuncio[
                'estilos'
              ]) {
                let objEstilosAnuncios = {
                  _id: getEstilosAnuncios._id,
                  color: getEstilosAnuncios.color,
                  tipo: {
                    codigo: getEstilosAnuncios.tipo,
                  },
                  tamanioLetra: getEstilosAnuncios?.tamanioLetra || null,
                };
                arrayEstilosAnuncio.push(objEstilosAnuncios);
              }
            }
            let objConfiguracionEstilosAnuncio = {
              _id: getConfiguracionAnuncio['_id'],
              estilos: arrayEstilosAnuncio,
            };
            arrayConfiguracionEstilosAnuncio.push(
              objConfiguracionEstilosAnuncio,
            );
          }
        }

        let datos = {
          _id: getAnuncio.anuncio._id,
          codigo: getAnuncio.anuncio.codigo || null,
          traducciones: traduccionAnuncios,
          tematica: getTraducirTematica,
          activo: getAnuncio.anuncio.activo,
          perfil: getAnuncio.anuncio.perfil,
          tipo: {
            codigo: getAnuncio.anuncio.tipo,
          },
          favorito: getAnuncio.anuncio.favorito,
          configuraciones: arrayConfiguracionEstilosAnuncio,
          orden: getAnuncio.anuncio?.orden || null,
          tiempoVisualizacion: getAnuncio.anuncio?.tiempoVisualizacion || null,
          intervaloVisualizacion:
            getAnuncio.anuncio?.intervaloVisualizacion || null,
          estado: {
            codigo: getAnuncio.anuncio.estado,
          },
          fechaInicio: getAnuncio.anuncio.fechaInicio,
          fechaFin: getAnuncio.anuncio.fechaFin,
          horaInicio: getAnuncio.anuncio.horaInicio,
          horaFin: getAnuncio.anuncio.horaFin,
          fechaCreacion: getAnuncio.anuncio.fechaCreacion,
          fechaActualizacion: getAnuncio.anuncio.fechaActualizacion,
        };
        arrayAnuncios.push(datos);
      }
    }
    return arrayAnuncios;
  }
}
