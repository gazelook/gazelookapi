import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codIdiomas,
  codigoEntidades,
  codigosCatalogoAcciones,
  codigosEstadosTematicaAnuncio,
  idiomas,
} from 'src/shared/enum-sistema';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { TraduccionTematicaAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_tematica_anuncio/traduccion_tematica_anuncio.interface';
const mongoose = require('mongoose');

@Injectable()
export class TraducirTematicaServiceSegundoPlano {
  constructor(
    @Inject('ANUNCIO_MODEL') private readonly anuncioModel: Model<Anuncio>,
    @Inject('TEMATICA_ANUNCIO_MODEL')
    private readonly tematicaAnuncioModel: Model<TraduccionTematicaAnuncio>,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async traducirTematicaSegundoPlano(idAnuncio, perfil, idioma) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la traduccion en el idioma original
      const getTraductorOriginal = await this.tematicaAnuncioModel
        .findOne({
          anuncio: idAnuncio,
          original: true,
          estado: codigosEstadosTematicaAnuncio.activa,
        })
        .session(opts.session);

      if (getTraductorOriginal) {
        if (idioma != idiomas.ingles) {
          await this.traducirTematicaIngles(
            idAnuncio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.espanol) {
          await this.traducirTematicaEspaniol(
            idAnuncio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.italiano) {
          await this.traducirTematicaItaliano(
            idAnuncio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.frances) {
          await this.traducirTematicaFrances(
            idAnuncio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.aleman) {
          await this.traducirTematicaAleman(
            idAnuncio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }
        if (idioma != idiomas.portugues) {
          await this.traducirTematicaPortugues(
            idAnuncio,
            perfil,
            getTraductorOriginal,
            opts,
          );
        }

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return true;
      }

      //Finaliza la transaccion
      await session.endSession();
      return true;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  //TRADUCCION DE TEMATICA A ESPAÑOL
  async traducirTematicaEspaniol(
    idAnuncio,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.espanol,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      let newTraduccionTematica = {
        titulo: titulo,
        // tags: tags,
        idioma: codIdiomas.espanol,
        original: false,
        estado: codigosEstadosTematicaAnuncio.activa,
        anuncio: idAnuncio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.tematicaAnuncioModel(newTraduccionTematica);
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion del pensamiento
      let newHistoricoTraduccionproyecto: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTematicaAnuncio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionproyecto,
      );

      //Actualiza el array de id de traducciones
      await this.anuncioModel.updateOne(
        { _id: idAnuncio },
        { $push: { tematica: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE TEMATICA A ITALIANO
  async traducirTematicaItaliano(
    idAnuncio,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.italiano,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionTematica = {
        titulo: titulo,
        idioma: codIdiomas.italiano,
        original: false,
        estado: codigosEstadosTematicaAnuncio.activa,
        anuncio: idAnuncio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.tematicaAnuncioModel(newTraduccionTematica);
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion de la tematica
      let newHistoricoTraduccioTematica: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTematicaAnuncio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccioTematica,
      );

      //Actualiza el array de id de traducciones
      await this.anuncioModel.updateOne(
        { _id: idAnuncio },
        { $push: { tematica: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE TEMATICA A FRANCES
  async traducirTematicaFrances(idAnuncio, perfil, getTraductorOriginal, opts) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.frances,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionTematica = {
        titulo: titulo,
        // tags: tags,
        idioma: codIdiomas.frances,
        original: false,
        estado: codigosEstadosTematicaAnuncio.activa,
        anuncio: idAnuncio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.tematicaAnuncioModel(newTraduccionTematica);
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion de la tematica
      let newHistoricoTraduccionTematica: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTematicaAnuncio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionTematica,
      );

      //Actualiza el array de id de traducciones
      await this.anuncioModel.updateOne(
        { _id: idAnuncio },
        { $push: { tematica: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE TEMATICA A ALEMAN
  async traducirTematicaAleman(idAnuncio, perfil, getTraductorOriginal, opts) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.aleman,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionTematica = {
        titulo: titulo,
        // tags: tags,
        idioma: codIdiomas.aleman,
        original: false,
        estado: codigosEstadosTematicaAnuncio.activa,
        anuncio: idAnuncio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.tematicaAnuncioModel(newTraduccionTematica);
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion de la tematica
      let newHistoricoTraduccionTematica: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTematicaAnuncio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionTematica,
      );

      //Actualiza el array de id de traducciones
      await this.anuncioModel.updateOne(
        { _id: idAnuncio },
        { $push: { tematica: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE TEMATICA A PORTUGUES
  async traducirTematicaPortugues(
    idAnuncio,
    perfil,
    getTraductorOriginal,
    opts,
  ) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.portugues,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionTematica = {
        titulo: titulo,
        // tags: tags,
        idioma: codIdiomas.portugues,
        original: false,
        estado: codigosEstadosTematicaAnuncio.activa,
        anuncio: idAnuncio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.tematicaAnuncioModel(newTraduccionTematica);
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion de la tematica
      let newHistoricoTraduccionTematica: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTematicaAnuncio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionTematica,
      );

      //Actualiza el array de id de traducciones
      await this.anuncioModel.updateOne(
        { _id: idAnuncio },
        { $push: { tematica: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }

  //TRADUCCION DE TEMATICA A INGLES
  async traducirTematicaIngles(idAnuncio, perfil, getTraductorOriginal, opts) {
    try {
      let textoTrducidoTitulo = await traducirTexto(
        idiomas.ingles,
        getTraductorOriginal.titulo,
      );
      let titulo = textoTrducidoTitulo.textoTraducido;

      //datos para guardar la nueva traduccion

      let newTraduccionTematica = {
        titulo: titulo,
        // tags: tags,
        idioma: codIdiomas.ingles,
        original: false,
        estado: codigosEstadosTematicaAnuncio.activa,
        anuncio: idAnuncio,
      };
      //guarda la nueva traduccion
      const traduccion = new this.tematicaAnuncioModel(newTraduccionTematica);
      const crearTraduccion = await traduccion.save(opts);

      const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
      delete dataTraduccion.__v;

      //datos para guardar el historico de la traduccion de la tematica
      let newHistoricoTraduccionTematica: any = {
        datos: dataTraduccion,
        usuario: perfil,
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadTematicaAnuncio,
      };

      //guarda el historico de la nueva traduccion
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoTraduccionTematica,
      );

      //Actualiza el array de id de traducciones
      await this.anuncioModel.updateOne(
        { _id: idAnuncio },
        { $push: { tematica: dataTraduccion._id } },
        opts,
      );

      return true;
    } catch (error) {
      throw error;
    }
  }
}
