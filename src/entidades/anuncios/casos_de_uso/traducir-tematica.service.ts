import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codigoEntidades,
  codigosCatalogoAcciones,
  codigosEstadosTematicaAnuncio,
} from 'src/shared/enum-sistema';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { TraduccionTematicaAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_tematica_anuncio/traduccion_tematica_anuncio.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';

@Injectable()
export class TraducirTematicaService {
  constructor(
    @Inject('ANUNCIO_MODEL') private readonly anuncioModel: Model<Anuncio>,
    @Inject('TEMATICA_ANUNCIO_MODEL')
    private readonly tematicaAnuncioModel: Model<TraduccionTematicaAnuncio>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async traducirTematica(idAnuncio, perfil, idioma, opts) {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    // const session = await mongoose.startSession();
    // session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      // const opts = { session };

      //Obtiene la traduccion en el idioma original
      const getTraductorOriginal = await this.tematicaAnuncioModel
        .findOne({
          anuncio: idAnuncio,
          original: true,
          estado: codigosEstadosTematicaAnuncio.activa,
        })
        .session(opts.session);

      if (getTraductorOriginal) {
        //Obtiene el idioma por el codigo
        const getIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
          idioma,
        );
        let codIdioma = getIdioma.codigo;

        let textoTrducidoTitulo = await traducirTexto(
          idioma,
          getTraductorOriginal.titulo,
        );
        let titulo = textoTrducidoTitulo.textoTraducido;

        let newTraduccionTematica = {
          titulo: titulo,
          idioma: codIdioma,
          original: false,
          estado: codigosEstadosTematicaAnuncio.activa,
          anuncio: idAnuncio,
        };
        //guarda la nueva traduccion
        const traduccion = new this.tematicaAnuncioModel(newTraduccionTematica);
        const crearTraduccion = await traduccion.save(opts);

        const dataTraduccion = JSON.parse(JSON.stringify(crearTraduccion));
        delete dataTraduccion.__v;

        //datos para guardar el historico de la traduccion del pensamiento
        let newHistoricoTraduccionproyecto: any = {
          datos: dataTraduccion,
          usuario: perfil,
          accion: codigosCatalogoAcciones.crear,
          entidad: codigoEntidades.entidadTematicaAnuncio,
        };

        //guarda el historico de la nueva traduccion
        this.crearHistoricoService.crearHistoricoServer(
          newHistoricoTraduccionproyecto,
        );

        //Actualiza el array de id de traducciones
        await this.anuncioModel.updateOne(
          { _id: idAnuncio },
          { $push: { tematica: dataTraduccion._id } },
          opts,
        );

        //Confirma los cambios de la transaccion
        // await session.commitTransaction();
        // //Finaliza la transaccion
        // await session.endSession();
        let returnTematica = [
          {
            titulo: crearTraduccion.titulo,
            idioma: {
              codigo: crearTraduccion.idioma,
            },
          },
        ];
        return returnTematica;
      }

      //Finaliza la transaccion
      // await session.endSession();
      return true;
    } catch (error) {
      //Aborta la transaccion
      // await session.abortTransaction();
      // //Finaliza la transaccion
      // await session.endSession();
      throw error;
    }
  }
}
