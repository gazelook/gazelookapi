import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { MensajesAnuncio } from 'src/drivers/mongoose/interfaces/mensajes_anuncio/mensajes-anuncio.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { erroresGeneral } from 'src/shared/enum-errores';
import {
  codigoEntidades,
  estadosMensajesAnuncio,
  nombreAcciones,
} from 'src/shared/enum-sistema';
const sw = require('stopword');
const mongoose = require('mongoose');

@Injectable()
export class EliminarMensajesAnuncioService {
  constructor(
    @Inject('MENSAJES_ANUNCIO_MODEL')
    private readonly mensajeAnuncioModel: Model<MensajesAnuncio>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
  ) {}

  async eliminarMensajesAnuncio(
    idMensaje: string,
    idPerfil: string,
    admin: string,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      // //Verifica si el perfil es un perfil administrador de anuncios de gazelook
      // let adminAnunciosGazelook = await this.verificarPerfilAnuncioService.verificarPerfilAdministradorAnuncios(anuncioDto.perfil._id);

      // if (!adminAnunciosGazelook) {
      //   throw {
      //     codigo: HttpStatus.UNAUTHORIZED,
      //     codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
      //   };
      // }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      let getAccion = accion.codigo;

      let getMensaje;
      if (admin === 'true') {
        getMensaje = await this.mensajeAnuncioModel.findOne({
          _id: idMensaje,
          estado: estadosMensajesAnuncio.activa,
        });
      } else {
        getMensaje = await this.mensajeAnuncioModel.findOne({
          _id: idMensaje,
          perfil: idPerfil,
          estado: estadosMensajesAnuncio.activa,
        });
      }

      console.log('idMensaje: ', idMensaje);
      console.log('perfil: ', idPerfil);
      console.log('getMensaje: ', getMensaje);
      if (getMensaje) {
        await this.mensajeAnuncioModel.updateOne(
          {
            _id: idMensaje,
          },
          {
            $set: {
              estado: estadosMensajesAnuncio.eliminado,
              fechaActualizacion: new Date(),
            },
          },
          opts,
        );

        //Data para guardar en el historico
        let dataHis = {
          _id: idMensaje,
          descripcion: 'Cambio de estado a '.concat(
            estadosMensajesAnuncio.eliminado,
          ),
          estadoAnterior: estadosMensajesAnuncio.activa,
          estadoActual: estadosMensajesAnuncio.eliminado,
        };

        let newHistorico: any = {
          datos: dataHis,
          usuario: idPerfil,
          accion: getAccion,
          entidad: codigoEntidades.mensajesAnuncio,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistorico);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return true;
      } else {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
