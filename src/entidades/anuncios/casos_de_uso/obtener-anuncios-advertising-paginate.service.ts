import { Inject, Injectable } from '@nestjs/common';
import * as moment from 'moment';
import { Model, PaginateModel } from 'mongoose';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { TraduccionAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_anuncio/traduccion_anuncio.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import {
  codidosEstadosAnuncio,
  codidosEstadosTraduccionAnuncio,
  codigosCatalogoTipoAnuncio,
  codigosEstadosTematicaAnuncio,
  estadosConfiguracionEstilosAnuncio,
  estadosEstilosAnuncio,
} from 'src/shared/enum-sistema';

@Injectable()
export class ObtenerAnunciosAdvertisingPaginateService {
  constructor(
    @Inject('ANUNCIO_MODEL')
    private readonly anuncioModel: PaginateModel<Anuncio>,
    @Inject('TRADUCCION_ANUNCIO_MODEL')
    private readonly traduccionAnuncioModel: Model<TraduccionAnuncio>,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async obtenerAnunciosPaginate(
    idUsuario: string,
    codIdioma: string,
  ): Promise<any> {
    // const headerNombre = new HadersInterfaceNombres;

    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codIdioma,
    );

    //Obtiene el codigo del idioma
    let codIdi = idioma.codigo;

    try {
      let localTime = moment().format('YYYY-MM-DD'); // store localTime
      let fechaFormat = localTime + 'T05:00:00.000Z';
      let I = new Date(fechaFormat);
      console.log('FECHA DE HOY: ', I);

      const anuncio = await this.anuncioModel
        .find({
          estado: codidosEstadosAnuncio.activa,
          activo: true,
          fechaInicio: { $lte: I },
          fechaFin: { $gte: I },
          tipo: codigosCatalogoTipoAnuncio.ADVERTISEMENT,
        })
        .populate({
          path: 'traducciones',
          match: {
            estado: codidosEstadosTraduccionAnuncio.activa,
            idioma: codIdi,
          },
        })
        .populate({
          path: 'tematica',
          match: {
            estado: codigosEstadosTematicaAnuncio.activa,
            idioma: codIdi,
          },
        })
        .populate({
          path: 'configuraciones',
          match: {
            estado: estadosConfiguracionEstilosAnuncio.activa,
          },
          populate: {
            path: 'estilos',
            match: {
              estado: estadosEstilosAnuncio.activa,
            },
          },
        })
        .sort({ orden: 1 });

      let dataAnuncio = [];

      if (anuncio.length > 0) {
        for (const getAnuncio of anuncio) {
          let traduccionAnuncios = [];
          if (getAnuncio.traducciones.length > 0) {
            for (const getTraducciones of getAnuncio.traducciones) {
              let traduccion = {
                titulo: getTraducciones['titulo'],
                descripcion: getTraducciones['descripcion'],
                idioma: {
                  codigo: getTraducciones['idioma'],
                },
              };
              traduccionAnuncios.push(traduccion);
            }
          }

          let traduccionTematica = [];
          if (getAnuncio?.tematica && getAnuncio.tematica.length > 0) {
            for (const getTematica of getAnuncio.tematica) {
              const traduccion = {
                titulo: getTematica['titulo'],
                idioma: {
                  codigo: getTematica['idioma'],
                },
              };
              traduccionTematica.push(traduccion);
            }
          }
          let arrayConfiguracionEstilosAnuncio = [];
          if (
            getAnuncio?.configuraciones &&
            getAnuncio?.configuraciones.length > 0
          ) {
            for (const getConfiguracionAnuncio of getAnuncio.configuraciones) {
              let arrayEstilosAnuncio = [];
              if (getConfiguracionAnuncio['estilos'].length > 0) {
                for (const getEstilosAnuncios of getConfiguracionAnuncio[
                  'estilos'
                ]) {
                  console.log('ID ESTILOS: ', getEstilosAnuncios._id);
                  console.log(
                    'getEstilosAnuncios.tamanioLetra: ',
                    getEstilosAnuncios['tamanioLetra'],
                  );
                  let objEstilosAnuncios = {
                    _id: getEstilosAnuncios._id,
                    color: getEstilosAnuncios.color,
                    tipo: {
                      codigo: getEstilosAnuncios.tipo,
                    },
                    tamanioLetra: getEstilosAnuncios?.tamanioLetra || null,
                  };
                  arrayEstilosAnuncio.push(objEstilosAnuncios);
                }
              }
              let objConfiguracionEstilosAnuncio = {
                _id: getConfiguracionAnuncio['_id'],
                estilos: arrayEstilosAnuncio,
              };
              arrayConfiguracionEstilosAnuncio.push(
                objConfiguracionEstilosAnuncio,
              );
            }
          }

          let datos = {
            _id: getAnuncio._id,
            codigo: getAnuncio.codigo || null,
            traducciones: traduccionAnuncios,
            tematica: traduccionTematica,
            activo: getAnuncio.activo,
            favorito: getAnuncio.favorito,
            configuraciones: arrayConfiguracionEstilosAnuncio,
            orden: getAnuncio?.orden || null,
            tiempoVisualizacion: getAnuncio?.tiempoVisualizacion || null,
            intervaloVisualizacion: getAnuncio?.intervaloVisualizacion || null,
            tipo: {
              codigo: getAnuncio.tipo,
            },
            estado: {
              codigo: getAnuncio.estado,
            },
            fechaInicio: getAnuncio.fechaInicio,
            fechaFin: getAnuncio.fechaFin,
            horaInicio: getAnuncio.horaInicio,
            horaFin: getAnuncio.horaFin,
            fechaCreacion: getAnuncio.fechaCreacion,
            fechaActualizacion: getAnuncio.fechaActualizacion,
          };
          dataAnuncio.push(datos);
        }
        // response.set(headerNombre.totalDatos, anuncio.totalDocs);
        // response.set(headerNombre.totalPaginas, anuncio.totalPages);
        // response.set(headerNombre.proximaPagina, anuncio.hasNextPage);
        // response.set(headerNombre.anteriorPagina, anuncio.hasPrevPage);

        // return dataAnuncio;
      }
      return dataAnuncio;
    } catch (error) {
      throw error;
    }
  }
}
