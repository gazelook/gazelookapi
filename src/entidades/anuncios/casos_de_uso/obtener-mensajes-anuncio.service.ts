import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { estadosMensajesAnuncio } from 'src/shared/enum-sistema';
import { MensajesAnuncio } from 'src/drivers/mongoose/interfaces/mensajes_anuncio/mensajes-anuncio.interface';

@Injectable()
export class ObtenerMensajesAnuncioService {
  constructor(
    @Inject('MENSAJES_ANUNCIO_MODEL')
    private readonly mensajeAnuncioModel: Model<MensajesAnuncio>,
  ) {}
  async obtenerMensajesAnuncio(
    idPerfil: string,
    codIdiom: string,
  ): Promise<any> {
    try {
      // let adminAnunciosGazelook = await this.verificarPerfilAnuncioService.verificarPerfilAdministradorAnuncios(anuncioDto.perfil._id);

      // if (!adminAnunciosGazelook) {
      //   throw {
      //     codigo: HttpStatus.UNAUTHORIZED,
      //     codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
      //   };
      // }

      //Guarda el anuncio
      const mensajeAnuncio = await this.mensajeAnuncioModel
        .find({ estado: estadosMensajesAnuncio.activa })
        .populate({
          path: 'perfil',
        });

      let arrayMensajes = [];
      if (mensajeAnuncio.length > 0) {
        for (const mensaje of mensajeAnuncio) {
          let getMensajes = {
            _id: mensaje._id,
            perfil: {
              _id: mensaje.perfil['_id'],
              nombre: mensaje.perfil['nombre'],
            },
            texto: mensaje.texto,
          };
          arrayMensajes.push(getMensajes);
        }
      }
      return arrayMensajes;
    } catch (error) {
      throw error;
    }
  }
}
