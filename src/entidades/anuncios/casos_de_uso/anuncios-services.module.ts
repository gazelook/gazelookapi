import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { AlbumServiceModule } from 'src/entidades/album/casos_de_uso/album.services.module';
import { ArchivoServicesModule } from 'src/entidades/archivo/casos_de_uso/archivo-services.module';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoColoresService } from 'src/entidades/catalogos/casos_de_uso/catalogo-colores.service';
import { CatalogoConfiguracionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-configuracion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { ConfiguracionEventoService } from 'src/entidades/catalogos/casos_de_uso/configuracion-evento.service';
import { FormulaEventoService } from 'src/entidades/catalogos/casos_de_uso/formula_evento.service';
import { CatalogoProviders } from 'src/entidades/catalogos/drivers/catalogo.provider';
import { ComentariosServicesModule } from 'src/entidades/comentarios/casos_de_uso/comentarios-services.module';
import { DireccionServiceModule } from 'src/entidades/direccion/casos_de_uso/direccion.services.module';
import { EmailServicesModule } from 'src/entidades/emails/casos_de_uso/email-services.module';
import { MediaServiceModule } from 'src/entidades/media/casos_de_uso/media.services.module';
import { NotificacionServicesModule } from 'src/entidades/notificacion/casos_de_uso/notificacion-services.module';
import { ParticipanteProyectoServicesModule } from 'src/entidades/participante_proyecto/casos_de_uso/participante-proyecto-services.module';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { RolServiceModule } from 'src/entidades/rol/casos_de_uso/rol.services.module';
import { UsuarioServicesModule } from 'src/entidades/usuario/casos_de_uso/usuario-services.module';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { PaisServicesModule } from '../../pais/casos_de_uso/pais.services.module';
import { anuncioProviders } from '../drivers/anuncio.provider';
import { ActualizarAnuncioService } from './actualizar-anuncio.service';
import { AnunciosPaginateService } from './anuncios-paginate.service';
import { BuscarAnunciosRecientesPaginacionService } from './buscar-anuncios-paginacion.service';
import { CrearAnuncioService } from './crear-anuncio.service';
import { CrearMensajesAnuncioService } from './crear-mensaje-anuncio.service';
import { CronDesactivarAnuncioServices } from './cron-desactivar-anuncios.service';
import { EliminarAnuncioService } from './eliminar-anuncio.service';
import { EliminarMensajesAnuncioService } from './eliminar-mensaje-anuncio.service';
import { ObtenerAnuncioUnicoService } from './obtener-anuncio-unico.service';
import { ObtenerAnunciosAdvertisingPaginateService } from './obtener-anuncios-advertising-paginate.service';
import { ObtenerAnunciosPaginateService } from './obtener-anuncios-paginate.service';
import { ObtenerMensajesAnuncioService } from './obtener-mensajes-anuncio.service';
import { TraducirTematicaServiceSegundoPlano } from './traducir-tematica-segundo-plano.service';
import { TraducirTematicaService } from './traducir-tematica.service';
import { VerificarPerfilAnuncioService } from './verificar-perfil.service';

@Module({
  imports: [
    DBModule,
    forwardRef(() => UsuarioServicesModule),
    forwardRef(() => ParticipanteProyectoServicesModule),
    forwardRef(() => RolServiceModule),
    forwardRef(() => EmailServicesModule),
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => AlbumServiceModule),
    forwardRef(() => MediaServiceModule),
    forwardRef(() => ComentariosServicesModule),
    forwardRef(() => DireccionServiceModule),
    forwardRef(() => ArchivoServicesModule),
    forwardRef(() => NotificacionServicesModule),
    PaisServicesModule,
  ],
  providers: [
    ...anuncioProviders,
    ...CatalogoProviders,
    CatalogoIdiomasService,
    CatalogoEstadoService,
    CatalogoEntidadService,
    CatalogoAccionService,
    TraduccionEstaticaController,
    ConfiguracionEventoService,
    FormulaEventoService,
    Funcion,
    CatalogoColoresService,
    CatalogoEstilosService,
    CatalogoConfiguracionService,
    CrearAnuncioService,
    VerificarPerfilAnuncioService,
    ObtenerAnuncioUnicoService,
    ActualizarAnuncioService,
    ObtenerAnunciosPaginateService,
    AnunciosPaginateService,
    EliminarAnuncioService,
    TraducirTematicaServiceSegundoPlano,
    TraducirTematicaService,
    BuscarAnunciosRecientesPaginacionService,
    ObtenerAnunciosAdvertisingPaginateService,
    CronDesactivarAnuncioServices,
    CrearMensajesAnuncioService,
    ObtenerMensajesAnuncioService,
    EliminarMensajesAnuncioService,
  ],
  exports: [
    CrearAnuncioService,
    VerificarPerfilAnuncioService,
    ObtenerAnuncioUnicoService,
    ActualizarAnuncioService,
    ObtenerAnunciosPaginateService,
    AnunciosPaginateService,
    EliminarAnuncioService,
    TraducirTematicaServiceSegundoPlano,
    TraducirTematicaService,
    BuscarAnunciosRecientesPaginacionService,
    ObtenerAnunciosAdvertisingPaginateService,
    CronDesactivarAnuncioServices,
    CrearMensajesAnuncioService,
    ObtenerMensajesAnuncioService,
    EliminarMensajesAnuncioService,
  ],
  controllers: [],
})
export class AnunciosServicesModule {}
