import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { TraduccionAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_anuncio/traduccion_anuncio.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { erroresGeneral } from 'src/shared/enum-errores';
import {
  codidosEstadosAnuncio,
  codidosEstadosTraduccionAnuncio,
  codigoEntidades,
  codigosCatalogoAcciones,
} from '../../../shared/enum-sistema';
import { VerificarPerfilAnuncioService } from './verificar-perfil.service';

@Injectable()
export class EliminarAnuncioService {
  noticia: any;

  constructor(
    @Inject('ANUNCIO_MODEL') private readonly anuncioModel: Model<Anuncio>,
    @Inject('TRADUCCION_ANUNCIO_MODEL')
    private readonly traduccionAnuncioModel: Model<TraduccionAnuncio>,
    private crearHistoricoService: CrearHistoricoService,
    private verificarPerfilAnuncioService: VerificarPerfilAnuncioService,
  ) {}

  async eliminarAnuncio(perfil: any, idAnuncio: string): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Verifica si el perfil es un perfil administrador de anuncios de gazelook
      let adminAnunciosGazelook = await this.verificarPerfilAnuncioService.verificarPerfilAdministradorAnuncios(
        perfil,
      );

      if (!adminAnunciosGazelook) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }

      console.log('usuario admin anuncios');
      const anuncio = await this.anuncioModel
        .findOne({
          _id: idAnuncio,
          estado: codidosEstadosAnuncio.activa,
        })
        .populate({
          path: 'traducciones ',
          select: '-fechaActualizacion  -__v ',
          match: {
            estado: codidosEstadosTraduccionAnuncio.activa,
          },
        })
        .session(session);

      if (anuncio) {
        //Actualiza el estado a eliminado de las traducciones proyecto
        await this.traduccionAnuncioModel.updateMany(
          { anuncio: idAnuncio },
          {
            $set: {
              estado: codidosEstadosTraduccionAnuncio.eliminado,
              fechaActualizacion: new Date(),
            },
          },
          opts,
        );

        // let datosDelete = {
        //   perfil: perfil,
        //   traduccionAnuncio: idAnuncio
        // }
        // let newHistoricoTProyecto: any = {
        //   datos: datosDelete,
        //   usuario: perfil,
        //   accion: codigosCatalogoAcciones.eliminar,
        //   entidad: codigoEntidades.entidadTraduccionAnuncio,
        // }
        // //Crea el historico del borrado de la traduccion anuncio
        // await this.crearHistoricoService.crearHistoricoTransaccion(newHistoricoTProyecto, opts);

        //Actualiza el estado a eliminado del anuncio
        await this.anuncioModel.updateOne(
          { _id: idAnuncio },
          {
            $set: {
              estado: codidosEstadosAnuncio.eliminado,
              fechaActualizacion: new Date(),
            },
          },
          opts,
        );

        const getProyectElimi = await this.anuncioModel
          .findOne({ _id: idAnuncio })
          .session(opts.session);

        let newHistoricoProyecto: any = {
          datos: getProyectElimi,
          usuario: perfil,
          accion: codigosCatalogoAcciones.eliminar,
          entidad: codigoEntidades.entidadAnuncio,
        };
        //Crea el historico del borrado del anuncio
        this.crearHistoricoService.crearHistoricoServer(newHistoricoProyecto);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return true;
      }
      return false;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
