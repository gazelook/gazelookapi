import { Inject, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { codidosEstadosAnuncio } from './../../../shared/enum-sistema';

@Injectable()
export class CronDesactivarAnuncioServices {
  constructor(
    @Inject('ANUNCIO_MODEL') private readonly anuncioModel: Model<Anuncio>,
  ) {}

  // Se ejecuta cada dia 1am
  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  //Se crea el evento de gastos operativos
  async cronDesactivarAnuncios() {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };
      //Obtiene todos los anuncios activos
      let getAnuncios = await this.anuncioModel.find({
        estado: codidosEstadosAnuncio.activa,
        activo: true,
      });

      if (getAnuncios.length > 0) {
        let localTime = moment().format('YYYY-MM-DD'); // store localTime
        let fechaFormat = localTime + 'T05:00:00.000Z';
        let I = new Date(fechaFormat);
        console.log('FECHA DE HOY: ', I);

        //Recorre todos los anuncios
        for (const anuncios of getAnuncios) {
          // console.log('ID: ', anuncios._id)
          // console.log('FECHA FIN: ', anuncios.fechaFin)
          //Si la fecha de hoy es mayor a la fecha de fin del anuncio se desactiva
          if (I > anuncios.fechaFin) {
            console.log('se desactivo el anuncio: ', anuncios._id);
            await this.anuncioModel.updateOne(
              { _id: anuncios._id },
              { $set: { activo: false, fechaActualizacion: new Date() } },
              opts,
            );
          }
        }
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      } else {
        //Finaliza la transaccion
        await session.endSession();
      }

      return true;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
