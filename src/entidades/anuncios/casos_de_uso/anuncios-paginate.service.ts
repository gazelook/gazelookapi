import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import {
  codidosEstadosAnuncio,
  codidosEstadosTraduccionAnuncio,
  codigosEstadosTematicaAnuncio,
  estadosConfiguracionEstilosAnuncio,
  estadosEstilosAnuncio,
} from 'src/shared/enum-sistema';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';

const mongoose = require('mongoose');

@Injectable()
export class AnunciosPaginateService {
  constructor(
    @Inject('ANUNCIO_MODEL')
    private readonly anuncioModel: PaginateModel<Anuncio>,
    private catalogoIdiomasService: CatalogoIdiomasService,
  ) {}

  async anunciosPaginate(
    idPerfil: string,
    codIdioma: string,
    pagina: number,
    limite: number,
    response: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codIdioma,
    );

    //Obtiene el codigo del idioma
    let codIdi = idioma.codigo;

    try {
      const populateTraducciones = {
        path: 'traducciones',
        match: {
          estado: codidosEstadosTraduccionAnuncio.activa,
        },
      };

      const populateTematica = {
        path: 'tematica',
        match: {
          estado: codigosEstadosTematicaAnuncio.activa,
          idioma: codIdi,
        },
      };

      const populatePerfil = {
        path: 'perfil',
        select: 'nombre nombreContacto _id',
      };

      const populateConfiguraciones = {
        path: 'configuraciones',
        match: {
          estado: estadosConfiguracionEstilosAnuncio.activa,
        },
        populate: {
          path: 'estilos',
          match: {
            estado: estadosEstilosAnuncio.activa,
          },
        },
      };

      const options = {
        lean: true,
        sort: { orden: 1 },
        select: ' -__v ',
        populate: [
          populateTraducciones,
          populatePerfil,
          populateTematica,
          populateConfiguraciones,
        ],
        page: Number(pagina),
        limit: Number(limite),
      };
      const anuncio = await this.anuncioModel.paginate(
        {
          estado: codidosEstadosAnuncio.activa,
        },
        options,
      );

      let dataAnuncio = [];

      if (anuncio.docs.length > 0) {
        for (const getAnuncio of anuncio.docs) {
          let traduccionAnuncios = [];
          if (getAnuncio.traducciones.length > 0) {
            for (const getTraducciones of getAnuncio.traducciones) {
              let traduccion = {
                titulo: getTraducciones['titulo'],
                descripcion: getTraducciones['descripcion'],
                idioma: {
                  codigo: getTraducciones['idioma'],
                },
              };
              traduccionAnuncios.push(traduccion);
            }
          }

          let traduccionTematica = [];
          if (getAnuncio?.tematica && getAnuncio.tematica.length > 0) {
            for (const getTematica of getAnuncio.tematica) {
              const traduccion = {
                titulo: getTematica['titulo'],
                idioma: {
                  codigo: getTematica['idioma'],
                },
              };
              traduccionTematica.push(traduccion);
            }
          }

          let arrayConfiguracionEstilosAnuncio = [];
          if (
            getAnuncio?.configuraciones &&
            getAnuncio?.configuraciones.length > 0
          ) {
            for (const getConfiguracionAnuncio of getAnuncio.configuraciones) {
              let arrayEstilosAnuncio = [];
              if (getConfiguracionAnuncio['estilos'].length > 0) {
                for (const getEstilosAnuncios of getConfiguracionAnuncio[
                  'estilos'
                ]) {
                  let objEstilosAnuncios = {
                    _id: getEstilosAnuncios._id,
                    color: getEstilosAnuncios.color,
                    tipo: {
                      codigo: getEstilosAnuncios.tipo,
                    },
                    tamanioLetra: getEstilosAnuncios?.tamanioLetra || null,
                  };
                  arrayEstilosAnuncio.push(objEstilosAnuncios);
                }
              }
              let objConfiguracionEstilosAnuncio = {
                _id: getConfiguracionAnuncio['_id'],
                estilos: arrayEstilosAnuncio,
              };
              arrayConfiguracionEstilosAnuncio.push(
                objConfiguracionEstilosAnuncio,
              );
            }
          }

          let datos = {
            _id: getAnuncio._id,
            codigo: getAnuncio.codigo || null,
            traducciones: traduccionAnuncios,
            tematica: traduccionTematica,
            activo: getAnuncio.activo,
            perfil: getAnuncio.perfil,
            tipo: {
              codigo: getAnuncio.tipo,
            },
            favorito: getAnuncio.favorito,
            configuraciones: arrayConfiguracionEstilosAnuncio,
            orden: getAnuncio?.orden || null,
            tiempoVisualizacion: getAnuncio?.tiempoVisualizacion || null,
            intervaloVisualizacion: getAnuncio?.intervaloVisualizacion || null,
            estado: {
              codigo: getAnuncio.estado,
            },
            fechaInicio: getAnuncio.fechaInicio,
            fechaFin: getAnuncio.fechaFin,
            horaInicio: getAnuncio.horaInicio,
            horaFin: getAnuncio.horaFin,
            fechaCreacion: getAnuncio.fechaCreacion,
            fechaActualizacion: getAnuncio.fechaActualizacion,
          };

          dataAnuncio.push(datos);
        }
        response.set(headerNombre.totalDatos, anuncio.totalDocs);
        response.set(headerNombre.totalPaginas, anuncio.totalPages);
        response.set(headerNombre.proximaPagina, anuncio.hasNextPage);
        response.set(headerNombre.anteriorPagina, anuncio.hasPrevPage);

        return dataAnuncio;
      }

      return dataAnuncio;
    } catch (error) {
      throw error;
    }
  }
}
