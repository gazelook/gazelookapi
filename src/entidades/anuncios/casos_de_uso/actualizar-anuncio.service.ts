import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import * as mongoose from 'mongoose';
import {
  codidosEstadosAnuncio,
  codidosEstadosTraduccionAnuncio,
  codigoEntidades,
  codigosCatalogoTipoAnuncio,
  codigosEstadosTematicaAnuncio,
  codigosHibernadoEntidades,
  estadosProyecto,
  idiomas,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { Anuncio } from 'src/drivers/mongoose/interfaces/anuncios/anuncio.interface';
import { TraduccionAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_anuncio/traduccion_anuncio.interface';
import { VerificarPerfilAnuncioService } from './verificar-perfil.service';
import { ActualizarAnuncioDto } from '../entidad/actualizar-anuncio.dto';
import { erroresGeneral } from 'src/shared/enum-errores';
import { TraduccionTematicaAnuncio } from 'src/drivers/mongoose/interfaces/traduccion_tematica_anuncio/traduccion_tematica_anuncio.interface';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { TraducirTematicaServiceSegundoPlano } from './traducir-tematica-segundo-plano.service';
import { EstiloAnuncio } from 'src/drivers/mongoose/interfaces/estilos_anuncio/estilo-anuncio.interface';
import { ConfiguracionEstiloAnuncios } from 'src/drivers/mongoose/interfaces/configuracion_estilo_anuncios/configuracion_estilo_anuncios.interface';

@Injectable()
export class ActualizarAnuncioService {
  noticia: any;
  constructor(
    @Inject('ANUNCIO_MODEL') private readonly anuncioModel: Model<Anuncio>,
    @Inject('TRADUCCION_ANUNCIO_MODEL')
    private readonly traduccionAnuncioModel: Model<TraduccionAnuncio>,
    @Inject('TEMATICA_ANUNCIO_MODEL')
    private readonly tematicaAnuncioModel: Model<TraduccionTematicaAnuncio>,
    @Inject('ESTILO_ANUNCIOS_MODEL')
    private readonly estiloAnunciosModel: Model<EstiloAnuncio>,
    @Inject('CONFIGURACION_ESTILO_ANUNCIOS_MODEL')
    private readonly configuracionEstiloAnunciosModel: Model<
      ConfiguracionEstiloAnuncios
    >,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private verificarPerfilAnuncioService: VerificarPerfilAnuncioService,
    private traducirTematicaServiceSegundoPlano: TraducirTematicaServiceSegundoPlano,
  ) { }

  async actualizarAnuncio(
    actualizarAnuncioDto: ActualizarAnuncioDto,
    idAnuncio: string,
    codIdiom: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    const accionAc = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.modificar,
    );
    let getAccionAc = accionAc.codigo;

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Verifica si el perfil es un perfil administrador de anuncios de gazelook
      let adminAnunciosGazelook = await this.verificarPerfilAnuncioService.verificarPerfilAdministradorAnuncios(
        actualizarAnuncioDto.perfil._id,
      );

      //Verifica si el perfil es un perfil administrador traductor de anuncios de gazelook
      let adminTraductorAnunciosGazelook = await this.verificarPerfilAnuncioService.verificarPerfilAdministradorTraductorAnuncios(
        actualizarAnuncioDto.perfil._id,
      );

      const getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
        actualizarAnuncioDto.perfil._id,
      );

      const getAnuncio = await this.anuncioModel
        .findOne({
          _id: idAnuncio,
          estado: codidosEstadosAnuncio.activa,
        })
        .session(session);

      if (getAnuncio) {
        if (adminAnunciosGazelook) {
          console.log('usuario admin anuncios');

          if (
            actualizarAnuncioDto?.traducciones &&
            actualizarAnuncioDto.traducciones.length > 0
          ) {
            for (const traduccionAnunc of actualizarAnuncioDto.traducciones) {
              const updateTradu = await this.traduccionAnuncioModel.updateMany(
                {
                  anuncio: idAnuncio,
                  estado: codidosEstadosTraduccionAnuncio.activa,
                  idioma: traduccionAnunc.idioma.codigo,
                },
                {
                  $set: {
                    estado: codidosEstadosTraduccionAnuncio.eliminado,
                    fechaActualizacion: new Date(),
                  },
                },
                opts,
              );

              let newTraduccionAnuncio = {
                titulo: traduccionAnunc.titulo,
                descripcion: traduccionAnunc.descripcion,
                idioma: traduccionAnunc.idioma.codigo,
                estado: codidosEstadosTraduccionAnuncio.activa,
                anuncio: idAnuncio,
              };
              //Crea la traduccion del anuncio
              const crearTraduccionAnuncio = await new this.traduccionAnuncioModel(
                newTraduccionAnuncio,
              ).save(opts);

              // codTraduccionAnuncio.push(crearTraduccionProyecto._id)

              await this.anuncioModel.updateOne(
                { _id: idAnuncio },
                {
                  $push: { traducciones: crearTraduccionAnuncio._id },
                  fechaActualizacion: new Date(),
                },
                opts,
              );
            }
          }

          let idiomaDetectado;
          if (
            actualizarAnuncioDto?.tematica &&
            actualizarAnuncioDto.tematica.length > 0
          ) {
            for (const traduccionTematica of actualizarAnuncioDto.tematica) {
              //detectar el idioma que se envia el texto
              let textoTraducido = await traducirTexto(
                codIdiom,
                traduccionTematica.titulo,
              );
              idiomaDetectado = textoTraducido.idiomaDetectado;

              //Obtiene el idioma por el codigo
              const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
                idiomaDetectado,
              );
              let codIdioma = idioma.codigo;

              const updateTradu = await this.tematicaAnuncioModel.updateMany(
                {
                  anuncio: idAnuncio,
                  estado: codigosEstadosTematicaAnuncio.activa,
                },
                {
                  $set: {
                    estado: codigosEstadosTematicaAnuncio.eliminado,
                    fechaActualizacion: new Date(),
                  },
                },
                opts,
              );

              let newTraduccionTematica = {
                titulo: traduccionTematica.titulo,
                idioma: codIdioma,
                original: true,
                estado: codigosEstadosTematicaAnuncio.activa,
                anuncio: idAnuncio,
              };
              //Crea la traduccion de la tematica
              const crearTraduccionTematica = await new this.tematicaAnuncioModel(
                newTraduccionTematica,
              ).save(opts);

              // codTraduccionAnuncio.push(crearTraduccionProyecto._id)

              await this.anuncioModel.updateOne(
                { _id: idAnuncio },
                {
                  $push: { tematica: crearTraduccionTematica._id },
                  fechaActualizacion: new Date(),
                },
                opts,
              );
            }
          }
          if (
            actualizarAnuncioDto?.fechaInicio &&
            actualizarAnuncioDto?.fechaFin
          ) {
            await this.anuncioModel.updateOne(
              { _id: idAnuncio },
              {
                $set: {
                  fechaInicio: actualizarAnuncioDto.fechaInicio,
                  fechaFin: actualizarAnuncioDto.fechaFin,
                  fechaActualizacion: new Date(),
                },
              },
              opts,
            );
          }
          if (actualizarAnuncioDto?.horaInicio) {
            await this.anuncioModel.updateOne(
              { _id: idAnuncio },
              {
                $set: {
                  horaInicio: actualizarAnuncioDto.horaInicio,
                  fechaActualizacion: new Date(),
                },
              },
              opts,
            );
          }
          if (actualizarAnuncioDto?.horaFin) {
            await this.anuncioModel.updateOne(
              { _id: idAnuncio },
              {
                $set: {
                  horaFin: actualizarAnuncioDto.horaFin,
                  fechaActualizacion: new Date(),
                },
              },
              opts,
            );
          }

          if (
            actualizarAnuncioDto?.configuraciones &&
            actualizarAnuncioDto.configuraciones.length > 0
          ) {
            for (const configuracionAnuncion of actualizarAnuncioDto.configuraciones) {
              let arrayEsilosAnuncio = [];

              for (const estilosAnuncio of configuracionAnuncion.estilos) {
                arrayEsilosAnuncio.push(estilosAnuncio._id);
                // //Actualiza el estilo del anuncio
                // await this.estiloAnunciosModel.updateOne({ _id: estilosAnuncio._id },
                //   { $set: { : estilosAnuncio.color, tipo: estilosAnuncio.tipo.codigo, fechaActualizacion: new Date() } }, opts);
              }
              //Actualiza el estilo de la configuracion estilo anuncio
              await this.configuracionEstiloAnunciosModel.updateOne(
                { _id: configuracionAnuncion._id },
                {
                  $set: {
                    estilos: arrayEsilosAnuncio,
                    fechaActualizacion: new Date(),
                  },
                },
                opts,
              );
            }
          }
          if (actualizarAnuncioDto?.favorito != undefined) {
            await this.anuncioModel.updateOne(
              { _id: idAnuncio },
              {
                $set: {
                  favorito: actualizarAnuncioDto.favorito,
                  fechaActualizacion: new Date(),
                },
              },
              opts,
            );
          }
          if (actualizarAnuncioDto?.orden != undefined) {
            await this.anuncioModel.updateOne(
              { _id: idAnuncio },
              {
                $set: {
                  orden: actualizarAnuncioDto.orden,
                  fechaActualizacion: new Date(),
                },
              },
              opts,
            );
          }
          if (actualizarAnuncioDto?.intervaloVisualizacion != undefined) {
            await this.anuncioModel.updateOne(
              { _id: idAnuncio },
              {
                $set: {
                  intervaloVisualizacion:
                    actualizarAnuncioDto.intervaloVisualizacion,
                  fechaActualizacion: new Date(),
                },
              },
              opts,
            );
          }
          if (actualizarAnuncioDto?.tiempoVisualizacion != undefined) {
            await this.anuncioModel.updateOne(
              { _id: idAnuncio },
              {
                $set: {
                  tiempoVisualizacion: actualizarAnuncioDto.tiempoVisualizacion,
                  fechaActualizacion: new Date(),
                },
              },
              opts,
            );
          }
          let newHistoricoProyectoUpdate: any = {
            datos: actualizarAnuncioDto,
            usuario: actualizarAnuncioDto.perfil._id,
            accion: getAccionAc,
            entidad: codigoEntidades.entidadAnuncio,
          };

          this.crearHistoricoService.crearHistoricoServer(
            newHistoricoProyectoUpdate,
          );

          const returnAnuncio = await this.anuncioModel
            .findOne({
              _id: idAnuncio,
              estado: codidosEstadosAnuncio.activa,
            })
            .populate({
              path: 'traducciones ',
              select: '-fechaActualizacion  -__v ',
              match: {
                estado: codidosEstadosTraduccionAnuncio.activa,
              },
            })
            .session(session);
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          if (
            actualizarAnuncioDto?.tematica &&
            actualizarAnuncioDto.tematica.length > 0
          ) {
            await this.traducirTematicaServiceSegundoPlano.traducirTematicaSegundoPlano(
              idAnuncio,
              actualizarAnuncioDto.perfil._id,
              idiomaDetectado,
            );
          }
          return returnAnuncio;
        }

        if (adminTraductorAnunciosGazelook) {
          console.log('usuario admin traductor anuncios');
          if (
            actualizarAnuncioDto?.traducciones &&
            actualizarAnuncioDto.traducciones.length > 0
          ) {
            for (const traducciones of actualizarAnuncioDto.traducciones) {

              //elimina las traducciones segun el idioma
              const updateTradu = await this.traduccionAnuncioModel.updateMany(
                {
                  anuncio: idAnuncio,
                  idioma: traducciones.idioma.codigo,
                  estado: codidosEstadosTraduccionAnuncio.activa,
                },
                {
                  $set: {
                    estado: codidosEstadosTraduccionAnuncio.eliminado,
                    fechaActualizacion: new Date(),
                  },
                },
                opts,
              );


              let newTraduccionAnuncio = {
                titulo: traducciones.titulo,
                descripcion: traducciones.descripcion,
                idioma: traducciones.idioma.codigo,
                estado: codidosEstadosTraduccionAnuncio.activa,
                anuncio: idAnuncio,
              };
              //Crea la traduccion del anuncio
              const crearTraduccionAnuncio = await new this.traduccionAnuncioModel(
                newTraduccionAnuncio,
              ).save(opts);

              // codTraduccionAnuncio.push(crearTraduccionProyecto._id)

              await this.anuncioModel.updateOne(
                { _id: idAnuncio },
                {
                  $push: { traducciones: crearTraduccionAnuncio._id },
                  fechaActualizacion: new Date(),
                },
                opts,
              );
            }

          }

          let newHistoricoProyectoUpdate: any = {
            datos: actualizarAnuncioDto,
            usuario: actualizarAnuncioDto.perfil._id,
            accion: getAccionAc,
            entidad: codigoEntidades.entidadAnuncio,
          };
          this.crearHistoricoService.crearHistoricoServer(
            newHistoricoProyectoUpdate,
          );

          const returnAnuncio = await this.anuncioModel
            .findOne({ _id: idAnuncio, estado: codidosEstadosAnuncio.activa })
            .session(session)
            .populate({
              path: 'traducciones ',
              select: '-fechaActualizacion  -__v ',
              match: {
                estado: codidosEstadosTraduccionAnuncio.activa,
              },
            });
          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          return returnAnuncio;
        }
      } else {
        return null;
      }

      throw {
        codigo: HttpStatus.UNAUTHORIZED,
        codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
      };
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async activarAnuncio(
    perfil: string,
    idAnuncio: string,
    estadoActivo: boolean,
    tipoAnuncio: string,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let getAccionModificar = accion.codigo;

      //Verifica si el perfil es un perfil administrador de anuncios de gazelook
      let adminAnunciosGazelook = await this.verificarPerfilAnuncioService.verificarPerfilAdministradorAnuncios(
        perfil,
      );

      if (!adminAnunciosGazelook) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }
      // const anuncio = await this.anuncioModel.findOne({
      //   $and: [
      //     { _id: idAnuncio },
      //     { perfil: perfil },
      //     { estado: codidosEstadosAnuncio.activa }
      //   ]
      // });

      // console.log('anuncio ***************: ', anuncio)
      // if (!anuncio) {
      //   //Aborta la transaccion
      //   await session.abortTransaction();
      //   //Finaliza la transaccion
      //   await session.endSession();
      //   return false;
      // }

      // if (estadoActivo === true) {
      //Si el anuncio es de tipo INFORMATION  o ADVERTISEMENT se pueden tener varios anuncios activos al mismo tiempo
      // if (tipoAnuncio === codigosCatalogoTipoAnuncio.REFLECTION || tipoAnuncio === codigosCatalogoTipoAnuncio.FINANCE) {

      // const getUltimoAnuncio = await this.anuncioModel.findOne(
      //   {
      //     estado: codidosEstadosAnuncio.activa,
      //     activo: true,
      //     tipo: tipoAnuncio
      //   }
      // ).sort({ fechaActualizacion: -1 }).session(opts.session);

      // //Desactiva el ultimo anuncio
      // if (getUltimoAnuncio) {
      //   await this.anuncioModel.updateOne(
      //     { _id: getUltimoAnuncio._id },
      //     { $set: { activo: false, fechaActualizacion: new Date() } })
      // }
      // }

      // }

      await this.anuncioModel.updateOne(
        { _id: idAnuncio },
        { $set: { activo: estadoActivo, fechaActualizacion: new Date() } },
        opts,
      );

      const datosModificado = {
        perfil: perfil,
        _id: idAnuncio,
        anuncio: await this.anuncioModel
          .findOne({ _id: idAnuncio })
          .session(opts.session)
          .select('-fechaActualizacion -fechaCreacion -__v'),
      };

      let newHistoricoProyecto: any = {
        datos: datosModificado,
        usuario: perfil,
        accion: getAccionModificar,
        entidad: codigoEntidades.entidadAnuncio,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistoricoProyecto);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return true;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
