import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { DocumentosLegalesServicesModule } from '../casos_de_uso/documentos-legales-services.module';
import { TraduccionEstaticaController } from './../../../multiIdioma/controladores/traduccion-estatica-controller';
import { CrearDocumentosLegalesControlador } from './crear-documentos-legales.controller';
import { ObtenerDocumentosControlador } from './obtener-documentos.controller';

@Module({
  imports: [DocumentosLegalesServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    CrearDocumentosLegalesControlador,
    ObtenerDocumentosControlador,
  ],
})
export class DocumentosLegalesControllerModule {}
