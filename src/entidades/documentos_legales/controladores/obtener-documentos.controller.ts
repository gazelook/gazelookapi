import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { Funcion } from 'src/shared/funcion';
import { TraduccionEstaticaController } from './../../../multiIdioma/controladores/traduccion-estatica-controller';
import { HadersInterfaceNombres } from './../../../shared/header-response-interface';
import { ObneterDocumentosService } from './../casos_de_uso/obtener-documentos.service';
import { DocumentoLegalDto } from './../entidad/documentos-legales-dto';
// import {CrearSuscripcionAdaptador}from '../adaptador/v1/adapterCrearSuscripcion'

@ApiTags('DocumentosLegales')
@Controller('api/documentos-legales')
export class ObtenerDocumentosControlador {
  constructor(
    private readonly obneterDocumentosService: ObneterDocumentosService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}
  // Add User: /users/create
  @Get('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Listar documentos por rango de fecha(Opcional)' })
  @ApiResponse({ status: 201, type: DocumentoLegalDto, description: 'Creado' })
  @ApiResponse({
    status: 404,
    description: 'No se pudo obtener los documentos',
  })
  @ApiQuery({ name: 'fechaInicial', required: false })
  @ApiQuery({ name: 'fechaFinal', required: false })
  @UseGuards(AuthGuard('jwt'))
  public async listarDocumentos(
    @Headers() headers,
    @Query('origen') origen: string,
    @Query('fechaInicial') fechaInicial: string,
    @Query('fechaFinal') fechaFinal: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const headerNombre = new HadersInterfaceNombres();

    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

      const documentos = await this.obneterDocumentosService.listarRango(
        origen,
        limite,
        pagina,
        fechaInicial,
        fechaFinal,
        codIdioma,
        response,
      );

      if (documentos.length === 0) {
        //llama al metodo de traduccion estatica
        const COLECCION_BACIA = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'COLECCION_BACIA',
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: COLECCION_BACIA,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: documentos,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.set(respuesta);
      return respuesta;
    }
  }
}
