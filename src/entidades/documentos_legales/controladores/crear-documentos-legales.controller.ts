import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { CrearDocumentosLegalesService } from '../casos_de_uso/crear-documentos-legales.service';
import { DocumentoLegalDto } from './../entidad/documentos-legales-dto';
// import {CrearSuscripcionAdaptador}from '../adaptador/v1/adapterCrearSuscripcion'

@ApiTags('DocumentosLegales')
@Controller('api/documentos-legales')
export class CrearDocumentosLegalesControlador {
  constructor(
    private readonly crearDocumentosLegalesService: CrearDocumentosLegalesService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}
  // Add User: /users/create
  @Post('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Crea documento legal' })
  @ApiResponse({ status: 201, type: DocumentoLegalDto, description: 'Creado' })
  @ApiResponse({ status: 404, description: 'Error al crear documento' })
  @UseGuards(AuthGuard('jwt'))
  public async crearDocumento(
    @Body() documentoLegalDto: DocumentoLegalDto,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const documento = await this.crearDocumentosLegalesService.crear(
        documentoLegalDto,
        codIdioma,
      );

      if (documento) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: documento,
        });
      } else {
        //llama al metodo de traduccion estatica
        const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_CREACION',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: ERROR_CREACION,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
