import { ActualizarDocumentosLegalesService } from './actualizar-documentos-legales.service';
import { ActualizarMediaService } from 'src/entidades/media/casos_de_uso/actualizar-media.service';
import { ConfigService } from 'src/config/config.service';
import { ObtenerDatosUsuarioService } from './../../usuario/casos_de_uso/obtener-datos-usuario.service';
import { ActualizarUsuarioService } from './../../usuario/casos_de_uso/actualizar-usuario.service';
import { DocumentoLegalDto } from './../entidad/documentos-legales-dto';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import * as mongoose from 'mongoose';
import { pathServerM } from '../../../shared/enum-servidor-mantenimiento';

import { Inject, Injectable, HttpService } from '@nestjs/common';
import { Model } from 'mongoose';
import { DocumentosLegales } from 'src/drivers/mongoose/interfaces/documentos_legales/documentos_legales.interface';
import {
  codigosCatalogoOrigenDoc,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class CrearDocumentosLegalesService {
  constructor(
    @Inject('DOCUMENTOS_MODEL')
    private readonly documentosLegalesModel: Model<DocumentosLegales>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private actualizarMediaService: ActualizarMediaService,
    private obtenerDatosUsuarioService: ObtenerDatosUsuarioService,
    private httpService: HttpService,
    private config: ConfigService,
    private actualizarDocumentosLegalesService: ActualizarDocumentosLegalesService,
  ) {}
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async crear(documento: DocumentoLegalDto, idioma): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.documentosLegales,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );

      const documentoDto = {
        estado: estado.codigo,
        version: documento.version,
        origen: documento.origen.codigo,
        usuario: documento.usuario._id,
        adjuntos: documento.adjuntos,
      };

      // _________________medias_____________________
      const listaMedias = [];
      if (documentoDto.adjuntos.length > 0) {
        for (const media of documentoDto.adjuntos) {
          let idMedia = media._id;
          //cambia de estado sinAsignar a activa
          await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
            idMedia,
            documentoDto.usuario,
            idioma,
            null,
            false,
            opts,
          );
          //
          listaMedias.push(media._id);
        }
      }

      const documentosLegales = await this.documentosLegalesModel
        .find({ origen: documento.origen.codigo })
        .sort({ fechaCreacion: 'desc' });
      console.log(documentosLegales, 'documentosLegales');

      // si existe un documento anterior actualizar estado
      if (documentosLegales.length > 0) {
        await this.actualizarDocumentosLegalesService.actualizar(
          documentosLegales[0],
          opts,
        );
      }
      // crear documento legal
      documentoDto.adjuntos = listaMedias;
      const documentoLegal = await new this.documentosLegalesModel(
        documentoDto,
      ).save(opts);

      // si el origen es de terminos y condiciones
      // cambiar estado a todos los usuarios => AceptaTerminosYcondiciones = false
      if (
        documentoDto.origen === codigosCatalogoOrigenDoc.terminosCondiciones
      ) {
        // buscar media para obtener el link del archivo
        const media = await this.documentosLegalesModel
          .findOne({ _id: documentoLegal._id })
          .session(opts.session)
          .populate({
            path: 'adjuntos',
            select: '-fechaActualizacion -fechaCreacion -__v -nombre',
            populate: { path: 'principal' },
          });
        const adjunto: any = media.adjuntos[0];

        this.actualizarUsuarioService.actualizarTerminosYCondiciones(opts);
        // Obtener y enviar correos
        this.obtenerDatosUsuarioService
          .obtenerEmailUsuarios()
          .then(async data => {
            const arrayCorreos = [];
            for (const correo of data) {
              arrayCorreos.push(correo.email);
            }

            const correos = {
              emailDestinatarios: arrayCorreos,
              link: adjunto.principal.url, // link de documento adjunto
            };
            await this.enviarCorreos(correos);
          });
      }

      // datos para guardar el historico
      const newHistorico: any = {
        datos: documentoLegal,
        usuario: documento.usuario,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return documentoLegal;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async enviarCorreos(correos: any) {
    return new Promise((resolve, reject) => {
      const apiKey = this.config.get('API_KEY');
      const URL_SERVER_MANTENIMIENTO = this.config.get(
        'URL_SERVER_MANTENIMIENTO',
      );
      const headersRequest: any = {
        'Content-Type': 'application/json',
        apiKey: apiKey,
      };
      this.httpService
        .post(
          `${URL_SERVER_MANTENIMIENTO}${pathServerM.ACTUALIZACION_POLITICAS_LEGALES}`,
          correos,
          { headers: headersRequest },
        )
        .subscribe((data: any) => {
          resolve(data.data);
        });
    });
  }
}
