import { ObneterDocumentosService } from './obtener-documentos.service';
import { MediaServiceModule } from 'src/entidades/media/casos_de_uso/media.services.module';
import { EmailServicesModule } from 'src/entidades/emails/casos_de_uso/email-services.module';
import { documentosLegalesProviders } from './../drivers/documentos-legales.provider';
import { ActualizarDocumentosLegalesService } from './actualizar-documentos-legales.service';
import { CrearDocumentosLegalesService } from './crear-documentos-legales.service';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { Module, HttpModule } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { UsuarioServicesModule } from 'src/entidades/usuario/casos_de_uso/usuario-services.module';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    UsuarioServicesModule,
    EmailServicesModule,
    HttpModule,
    MediaServiceModule,
  ],
  providers: [
    ...documentosLegalesProviders,
    CrearDocumentosLegalesService,
    ActualizarDocumentosLegalesService,
    ObneterDocumentosService,
  ],
  exports: [
    CrearDocumentosLegalesService,
    ActualizarDocumentosLegalesService,
    ObneterDocumentosService,
    ...documentosLegalesProviders,
  ],
  controllers: [],
})
export class DocumentosLegalesServicesModule {}
