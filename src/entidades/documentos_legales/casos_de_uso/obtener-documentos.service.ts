import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { PaginateModel } from 'mongoose';
import { DocumentosLegales } from 'src/drivers/mongoose/interfaces/documentos_legales/documentos_legales.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import {
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { HadersInterfaceNombres } from '../../../shared/header-response-interface';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { TraduccionMediaService } from '../../media/casos_de_uso/traduccion-media.service';

@Injectable()
export class ObneterDocumentosService {
  constructor(
    @Inject('DOCUMENTOS_MODEL')
    private readonly documentosLegalesModel: PaginateModel<DocumentosLegales>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private traduccionMediaService: TraduccionMediaService,
  ) {}

  async listarRango(
    origen: string,
    limite: number,
    pagina: number,
    fechaInicial: any,
    fechaFinal: any,
    codIdioma,
    response: any,
  ): Promise<any> {
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      const headerNombre = new HadersInterfaceNombres();

      //Obtener el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdioma,
      );

      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.documentosLegales,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;
      //Obtiene el estado activo de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      const populateAdjuntos = {
        path: 'adjuntos',
        match: { estado: codEstadoMedia },
        //select: 'principal enlace miniatura catalogoMedia',
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: idioma.codigo },
          },
          {
            path: 'principal',
            select: ' url tipo duracion fileDefault fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: ' url tipo duracion fileDefault fechaActualizacion',
          },
        ],
      };

      const options = {
        lean: true,
        sort: { fechaCreacion: -1 },
        select: 'adjuntos version origen fechaCreacion',
        populate: [populateAdjuntos],
        page: Number(pagina),
        limit: Number(limite),
      };

      // si no se le pasa las fechas mostrar el el ultimo documento
      let documentos: any;

      if (fechaInicial && fechaFinal) {
        documentos = await this.documentosLegalesModel.paginate(
          {
            $and: [
              {
                fechaCreacion: {
                  $gte: new Date(
                    new Date(fechaInicial).setUTCHours(0, 0, 0, 0),
                  ),
                  $lt: new Date(
                    new Date(fechaFinal).setUTCHours(23, 59, 59, 999),
                  ),
                },
              },
              { origen: origen },
            ],
          },
          options,
        );
      } else if (fechaInicial && !fechaFinal) {
        // busca de la fecha dada todo el mes
        /* const fechaI = new Date(fechaInicial);
        const primerDia = new Date(fechaI.getFullYear(), fechaI.getMonth() + 1, 1);
        const ultimoDia = new Date(fechaI.getFullYear(), fechaI.getMonth() + 2, 0); */

        documentos = await this.documentosLegalesModel.paginate(
          {
            $and: [
              {
                fechaCreacion: {
                  $gte: new Date(
                    new Date(fechaInicial).setUTCHours(0, 0, 0, 0),
                  ),
                  $lt: new Date(
                    new Date(fechaInicial).setUTCHours(23, 59, 59, 999),
                  ),
                },
              },
              { origen: origen },
            ],
          },
          options,
        );
      } else {
        documentos = await this.documentosLegalesModel.paginate(
          { origen: origen },
          options,
        );

        /* if (documentos.docs) {
          if (documentos.docs.length > 0) {
            documentos.docs = documentos.docs[documentos.docs.length - 1];
            return documentos;
          }
        } */
      }
      let result = [];
      for (const doc of documentos.docs) {
        let adjunts = [];
        let data: any = {
          _id: doc._id,
          version: doc.version,
          fechaCreacion: doc.fechaCreacion,
          origen: {
            codigo: doc.origen,
          },
        };
        for (const media of doc.adjuntos) {
          console.log(media);

          let dataMedia: any = {
            _id: media._id,
            catalogoMedia: {
              codigo: media.catalogoMedia,
            },
            principal: {
              _id: media.principal._id,
              url: media.principal.url,
              tipo: {
                codigo: media.principal.tipo,
              },
              fileDefault: media.principal.fileDefault,
              fechaActualizacion: media.principal.fechaActualizacion,
            },
            fechaCreacion: media.fechaCreacion,
            fechaActualizacion: media.fechaActualizacion,
          };

          if (media.traducciones[0] === undefined) {
            console.log('debe traducir descripcion media');

            const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
              media._id,
              codIdioma,
              opts,
            );
            if (traduccionMedia) {
              const traducciones = [
                {
                  _id: traduccionMedia._id,
                  descripcion: traduccionMedia.descripcion,
                },
              ];

              dataMedia.traducciones = traducciones;
            } else {
              dataMedia.traducciones = [];
            }
          } else {
            let arrayTrad = [];
            arrayTrad.push(media.traducciones[0]);
            dataMedia.traducciones = arrayTrad;
          }
          if (media.principal.duracion) {
            dataMedia.principal.duracion = media.principal.duracion;
          }

          adjunts.push(dataMedia);
        }

        data.adjuntos = adjunts;

        result.push(data);
        console.log(result);
      }
      response.set(headerNombre.totalDatos, documentos.totalDocs);
      response.set(headerNombre.totalPaginas, documentos.totalPages);
      response.set(headerNombre.proximaPagina, documentos.hasNextPage);
      response.set(headerNombre.anteriorPagina, documentos.hasPrevPage);

      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return result;
    } catch (error) {
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
