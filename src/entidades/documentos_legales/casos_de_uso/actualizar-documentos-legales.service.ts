import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { DocumentosLegales } from 'src/drivers/mongoose/interfaces/documentos_legales/documentos_legales.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class ActualizarDocumentosLegalesService {
  constructor(
    @Inject('DOCUMENTOS_MODEL')
    private readonly documentosLegalesModel: Model<DocumentosLegales>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
  ) {}
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async actualizar(documentoDto: any, opts?: any): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.documentosLegales,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.eliminado,
        entidad.codigo,
      );
      const accionActualizar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      // actualizar version del documento
      const documento = await this.documentosLegalesModel.updateOne(
        { _id: documentoDto._id },
        { estado: estado.codigo },
        opts,
      );

      //datos para actuaizar el historico
      const newHistorico: any = {
        datos: documento,
        usuario: documentoDto.usuario,
        accion: accionActualizar.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      return documento;
    } catch (error) {
      throw error;
    }
  }
}
