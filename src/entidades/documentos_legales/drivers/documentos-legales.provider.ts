import { Connection } from 'mongoose';
import { DocumentosLegalesModelo } from './../../../drivers/mongoose/modelos/documentos_legales/documentos_legales.schema';

export const documentosLegalesProviders = [
  {
    provide: 'DOCUMENTOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'documentos_legales',
        DocumentosLegalesModelo,
        'documentos_legales',
      ),
    inject: ['DB_CONNECTION'],
  },
];
