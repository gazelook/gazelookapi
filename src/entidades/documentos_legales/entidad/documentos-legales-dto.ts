import { ApiProperty } from '@nestjs/swagger';
import { mediaId } from '../../media/entidad/archivo-resultado.dto';

export class UsuarioDocumento {
  @ApiProperty()
  _id: string;
}

export class CatalogoOrigenDoc {
  @ApiProperty()
  codigo: string;
}

export class DocumentoLegalDto {
  @ApiProperty()
  version: number;
  @ApiProperty({ type: CatalogoOrigenDoc })
  origen: CatalogoOrigenDoc;
  @ApiProperty({ type: [mediaId] })
  adjuntos: mediaId[];
  @ApiProperty({ type: UsuarioDocumento })
  usuario: UsuarioDocumento;
}

export class ActualizarDocumentoLegalDto {
  @ApiProperty()
  _id: number;
  @ApiProperty()
  version: number;
  @ApiProperty()
  origen: string;
  @ApiProperty()
  adjuntos: Array<string>;
  @ApiProperty()
  usuario: UsuarioDocumento;
}
