import { DocumentosLegalesControllerModule } from './controladores/documentos-legales-controller.module';
import { Module, HttpModule } from '@nestjs/common';

@Module({
  imports: [DocumentosLegalesControllerModule, HttpModule],
  providers: [],
  controllers: [],
})
export class DocumentosLegalesModule {}
