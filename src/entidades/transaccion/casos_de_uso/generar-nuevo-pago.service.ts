import { Injectable } from '@nestjs/common';
import { CrearPagoPaypalService } from './crear-pago-paypal.service';
import { CrearPagoStripeService } from './crear-pago-stripe.service';
import {
  catalogoOrigen,
  codigosMetodosPago,
  estadosTransaccion,
} from '../../../shared/enum-sistema';
import { ObtenerTransaccionService } from './obtener-transaccion.service';
import { ActualizarTransaccionService } from './actualizar-transaccion.service';
import {
  ActualizarTransaccionPagoDto,
  ActualizarInformacionPagoDto,
  NuevosDatosPagoDto,
} from '../entidad/actualizar-pago.dto';
import { TransaccionCuentaDto } from '../entidad/transaccion.dto';
import { Usuario } from '../../../drivers/mongoose/interfaces/usuarios/usuario.interface';
import { Transaccion } from '../../../drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CrearCuentaDto } from '../../cuenta/entidad/cuenta.dto';
import { CrearNuevaTransaccionService } from './crear-nueva-transaccion.service';
import { EliminarTransaccionCompletoService } from './eliminar-transaccion-completo.service';

// Se utiliza solo en la creacion de cuenta
// Generar Nuevo pago: este caso se da cuando el usuario cambia el metodo de pago o cuando surge un inconveniente al pagar
// (se quedo sin internet)

@Injectable()
export class GenerarNuevoPagoService {
  constructor(
    private crearPagoPaypalService: CrearPagoPaypalService,
    private crearPagoStripeService: CrearPagoStripeService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private crearNuevaTransaccionService: CrearNuevaTransaccionService,
    private eliminarTransaccionCompletoService: EliminarTransaccionCompletoService,
  ) {}

  async nuevoPagoPaypal(
    transaccionUsuario: Array<Transaccion>,
    dataCuenta: CrearCuentaDto,
    opts?: any,
  ): Promise<any> {
    try {
      let nuevoMonto = 0;
      for (const nuevaTransaccion of dataCuenta.transacciones) {
        nuevoMonto += nuevaTransaccion.monto;
      }

      const paypal = await this.crearPagoPaypalService.generarNuevoPagoPaypal(
        nuevoMonto,
      );

      // _____________________________actualizar transaccion__________________________________
      //1. si el usuario tiene el mismo numero de transacciones
      //2. si el usuario agrego un valor extra, se crea la nueva transaccion

      const actualizarInformacionPago: ActualizarInformacionPagoDto = {
        idPago: paypal.idPaypal,
      };

      const checkNuevaTransSuscr = dataCuenta.transacciones.find(
        data => data.origen.codigo === catalogoOrigen.suscripciones,
      );
      // actualizar la transaccion de tipo origen suscripcion
      if (checkNuevaTransSuscr) {
        const actualizarTransaccion: ActualizarTransaccionPagoDto = {
          monto: checkNuevaTransSuscr.monto,
          origen: checkNuevaTransSuscr.origen.codigo,
          metodoPago: dataCuenta.metodoPago.codigo,
        };

        const transaccion = transaccionUsuario.find(
          data => data.origen === checkNuevaTransSuscr.origen.codigo,
        );
        await this.actualizarTransaccionService.actualizarTransaccionNuevoPagoPaypal(
          transaccion._id,
          transaccion.informacionPago._id,
          actualizarTransaccion,
          actualizarInformacionPago,
          opts,
        );
      }

      const checkNuevaTransExtra = dataCuenta.transacciones.find(
        data => data.origen.codigo === catalogoOrigen.valor_extra,
      );

      // actualizar la transaccion de tipo origen VALOR EXTRA
      if (
        checkNuevaTransExtra &&
        dataCuenta.transacciones.length === transaccionUsuario.length
      ) {
        const actualizarTransaccion: ActualizarTransaccionPagoDto = {
          monto: checkNuevaTransExtra.monto,
          origen: checkNuevaTransExtra.origen.codigo,
          metodoPago: dataCuenta.metodoPago.codigo,
        };

        const transaccion = transaccionUsuario.find(
          data => data.origen === checkNuevaTransExtra.origen.codigo,
        );
        await this.actualizarTransaccionService.actualizarTransaccionNuevoPagoPaypal(
          transaccion._id,
          transaccion.informacionPago._id,
          actualizarTransaccion,
          actualizarInformacionPago,
          opts,
        );
      }

      // Registrar Nueva transaccion
      if (
        checkNuevaTransExtra &&
        dataCuenta.transacciones.length > transaccionUsuario.length
      ) {
        await this.crearNuevaTransaccionService.nuevaTransaccion(
          {
            transaccion: checkNuevaTransExtra,
            metodoPago: dataCuenta.metodoPago.codigo,
            usuario: transaccionUsuario[0].usuario.toString(),
            informacionPago: transaccionUsuario[0].informacionPago._id,
          },
          opts,
        );
      }

      // Eliminar transaccion (Cuando el usuario elimina el valor extra)
      const transaccionExtraAnterior = transaccionUsuario.find(
        data => data.origen === catalogoOrigen.valor_extra,
      );
      if (!checkNuevaTransExtra && transaccionExtraAnterior) {
        await this.eliminarTransaccionCompletoService.eliminarTransaccionCompletoById(
          transaccionExtraAnterior._id.toString(),
          opts,
        );
      }

      // retorna la transaccion de tipo suscripcion
      const transaccionUser = transaccionUsuario.find(
        data => data.origen === catalogoOrigen.suscripciones,
      );
      const result = {
        idPago: paypal.idPaypal,
        idTransaccion: transaccionUser._id,
      };

      return result;
    } catch (error) {
      throw error;
    }
  }

  async nuevoPagoStripe(
    usuarioRegistrado: Usuario,
    dataCuenta: CrearCuentaDto,
    opts?: any,
  ): Promise<any> {
    try {
      let total = 0;
      for (let transaccion of dataCuenta.transacciones) {
        total += transaccion.monto;
      }
      const datosPago: NuevosDatosPagoDto = {
        idPago: usuarioRegistrado.transacciones[0].informacionPago.idPago,
        nombres: dataCuenta.datosFacturacion.nombres,
        email: dataCuenta.datosFacturacion.email || usuarioRegistrado.email,
        telefono: dataCuenta.datosFacturacion.telefono,
        direccion:
          dataCuenta.datosFacturacion?.direccion ||
          usuarioRegistrado.direccion.traducciones[0].descripcion,
        monto: total,
      };

      const stripe = await this.crearPagoStripeService.generarNuevoPagoStripe(
        datosPago,
      );

      // _____________________________actualizar transaccion si el usuario tiene el mismo numero de transacciones__________________________________
      if (
        usuarioRegistrado.transacciones.length ===
        dataCuenta.transacciones.length
      ) {
        const actualizarInformacionPago: ActualizarInformacionPagoDto = {
          idPago: stripe.id,
          datos: {
            nombres: datosPago.nombres,
            direccion: datosPago.direccion,
            email: datosPago.email,
            telefono: datosPago.email,
          },
        };

        for (const nuevaTransaccion of dataCuenta.transacciones) {
          const actualizarTransaccion: ActualizarTransaccionPagoDto = {
            monto: nuevaTransaccion.monto,
            origen: nuevaTransaccion.origen.codigo,
            metodoPago: dataCuenta.metodoPago.codigo,
          };

          const transaccion = usuarioRegistrado.transacciones.find(
            data => data.origen === nuevaTransaccion.origen.codigo,
          );
          await this.actualizarTransaccionService.actualizarTransaccionNuevoPagoStripe(
            transaccion._id,
            transaccion.informacionPago._id,
            actualizarTransaccion,
            actualizarInformacionPago,
            opts,
          );
        }
      }

      const checkNuevaTransExtra = dataCuenta.transacciones.find(
        data => data.origen.codigo === catalogoOrigen.valor_extra,
      );
      // Registrar Nueva transaccion
      if (
        checkNuevaTransExtra &&
        dataCuenta.transacciones.length > usuarioRegistrado.transacciones.length
      ) {
        await this.crearNuevaTransaccionService.nuevaTransaccion(
          {
            transaccion: checkNuevaTransExtra,
            metodoPago: dataCuenta.metodoPago.codigo,
            usuario: usuarioRegistrado.transacciones[0].usuario.toString(),
            informacionPago:
              usuarioRegistrado.transacciones[0].informacionPago._id,
          },
          opts,
        );
      }

      // Eliminar transaccion (Cuando el usuario decide ya no abonar un valor extra)
      const transaccionExtraAnterior = usuarioRegistrado.transacciones.find(
        data => data.origen === catalogoOrigen.valor_extra,
      );
      if (!checkNuevaTransExtra && transaccionExtraAnterior) {
        await this.eliminarTransaccionCompletoService.eliminarTransaccionCompletoById(
          transaccionExtraAnterior._id.toString(),
          opts,
        );
      }

      const transaccionUser = usuarioRegistrado.transacciones.find(
        data => data.origen === catalogoOrigen.suscripciones,
      );
      const result = {
        idPago: stripe.client_secret,
        idTransaccion: transaccionUser._id,
      };

      return result;
    } catch (error) {
      throw error;
    }
  }
}
