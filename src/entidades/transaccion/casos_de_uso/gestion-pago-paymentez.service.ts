import { Injectable } from '@nestjs/common';

import { nombreEstadosPagos } from 'src/shared/enum-sistema';
import { PaymentIntentsService } from '../../../drivers/stripe/services/payment-intents.service';
import { PaymentMethodsService } from '../../../drivers/stripe/services/payment-methods.service';

@Injectable()
export class GestionPagoPaymentezService {
  constructor(
    private paymentIntentsService: PaymentIntentsService,
    private paymentMethodsService: PaymentMethodsService,
  ) {}

  calcularValorTotalComisionPaymentez(
    montoActualTransaccion: number,
    comisionPaymentez: number,
  ) {
    try {
      let valorComision = montoActualTransaccion * comisionPaymentez;
      let nuevoValor = montoActualTransaccion - valorComision;
      const resultData = {
        newFee: parseFloat(valorComision.toFixed(2)),
        newAmountReceived: parseFloat(nuevoValor.toFixed(2)),
      };
      return resultData;
    } catch (error) {
      throw error;
    }
  }
}
