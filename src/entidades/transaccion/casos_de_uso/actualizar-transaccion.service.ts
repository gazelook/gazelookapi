import { ActualizarSuscripcionService } from './../../suscripcion/casos_de_uso/actualizar-suscripcion.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import {
  ActualizarInformacionPagoDto,
  ActualizarTransaccionPagoDto,
} from '../entidad/actualizar-pago.dto';
import { UpdateTransaccionPagoCriptoDto, UpdateTransaccionPagoDto } from '../entidad/transaccion.dto';

@Injectable()
export class ActualizarTransaccionService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    @Inject('INFORMACION_PAGO_MODEL')
    private readonly informacionPagoModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async actualizarTransaccion(
    idTransaccion: string,
    opts: any,
    dataUpdate?: UpdateTransaccionPagoDto,
  ): Promise<Transaccion> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionMofificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let transaccion: Transaccion;
      if (dataUpdate) {
        const dataTransaccionUpdate = {
          ...dataUpdate,
          estado: estado.codigo,
        };
        transaccion = await this.transaccionModel.updateOne(
          { _id: idTransaccion },
          dataTransaccionUpdate,
          opts,
        );
      } else {
        transaccion = await this.transaccionModel.updateOne(
          { _id: idTransaccion },
          { estado: estado.codigo },
          opts,
        );
      }

      //datos para guardar el historico
      const newHistorico: any = {
        datos: transaccion,
        usuario: transaccion.usuario,
        accion: accionMofificar.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return transaccion;
    } catch (error) {
      throw error;
    }
  }

  async actualizarTransaccionCripto(
    idTransaccion: string,
    opts: any,
    dataUpdate?: UpdateTransaccionPagoCriptoDto,
  ): Promise<Transaccion> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionMofificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let transaccion: Transaccion;
      if (dataUpdate) {
        const dataTransaccionUpdate = {
          ...dataUpdate,
          estado: estado.codigo,
        };
        transaccion = await this.transaccionModel.updateOne(
          { _id: idTransaccion },
          dataTransaccionUpdate,
          opts,
        );
      } else {
        transaccion = await this.transaccionModel.updateOne(
          { _id: idTransaccion },
          { estado: estado.codigo },
          opts,
        );
      }

      //datos para guardar el historico
      const newHistorico: any = {
        datos: transaccion,
        usuario: transaccion.usuario,
        accion: accionMofificar.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return transaccion;
    } catch (error) {
      throw error;
    }
  }

  async crearTransaccionVerificacion(idTransaccion: any): Promise<Transaccion> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionMofificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      const transaccion = await this.transaccionModel.updateOne(
        { _id: idTransaccion },
        { estado: estado.codigo },
      );

      //datos para guardar el historico
      const newHistorico: any = {
        datos: transaccion,
        usuario: transaccion.usuario,
        accion: accionMofificar.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return transaccion;
    } catch (error) {
      throw error;
    }
  }

  // actualizamos de nuevo los datos de transaccion con un nuevo pago
  async actualizarTransaccionNuevoPagoStripe(
    idTransaccion: string,
    idInformacionPago: string,
    actualizarTransaccion: ActualizarTransaccionPagoDto,
    actualizarInformacionPago: ActualizarInformacionPagoDto,
    opts: any,
  ): Promise<Transaccion> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.pendiente,
        entidad.codigo,
      );
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      const actualizarTransaccionPago = {
        ...actualizarTransaccion,
        estado: estado.codigo,
      };

      const transaccion = await this.transaccionModel.updateOne(
        { _id: idTransaccion },
        actualizarTransaccionPago,
        opts,
      );
      const informacionPago = await this.informacionPagoModel.updateOne(
        { _id: idInformacionPago },
        actualizarInformacionPago,
        opts,
      );

      //datos para guardar el historico
      const newHistorico: any = {
        datos: transaccion,
        usuario: transaccion.usuario,
        accion: accionModificar.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return transaccion;
    } catch (error) {
      throw error;
    }
  }

  async actualizarTransaccionNuevoPagoPaypal(
    idTransaccion: string,
    idInformacionPago: string,
    actualizarTransaccion: ActualizarTransaccionPagoDto,
    actualizarInformacionPago: ActualizarInformacionPagoDto,
    opts: any,
  ): Promise<Transaccion> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.pendiente,
        entidad.codigo,
      );
      const accionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );

      const actualizarTransaccionPago = {
        ...actualizarTransaccion,
        estado: estado.codigo,
      };

      const transaccion = await this.transaccionModel.updateOne(
        { _id: idTransaccion },
        actualizarTransaccionPago,
        opts,
      );
      await this.informacionPagoModel.updateOne(
        { _id: idInformacionPago },
        actualizarInformacionPago,
        opts,
      );

      //datos para guardar el historico
      const newHistorico: any = {
        datos: transaccion,
        usuario: transaccion.usuario,
        accion: accionModificar.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return transaccion;
    } catch (error) {
      throw error;
    }
  }
}
