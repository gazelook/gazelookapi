import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { Devolucion } from 'src/drivers/mongoose/interfaces/devolucion/devolucion.interface';
import { InformacionPago } from 'src/drivers/mongoose/interfaces/informacion_pago/informacion-pago.interface';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { StripeRefundsService } from 'src/drivers/stripe/services/stripe-refunds.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { ActualizarUsuarioService } from 'src/entidades/usuario/casos_de_uso/actualizar-usuario.service';
import { EliminarInformacionCompleto } from 'src/entidades/usuario/casos_de_uso/eliminar-informacion-completo.service';
import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';
import { erroresGeneral } from 'src/shared/enum-errores';
import {
  codigoEntidades,
  codigosCatalogoAcciones,
  dias,
  diasMaximosDevolucion,
  estadosDevolucion,
  estadosUsuario,
  statusRefundStripe,
} from 'src/shared/enum-sistema';
import { EliminarTransaccionCompletoService } from './eliminar-transaccion-completo.service';

const mongoose = require('mongoose');

@Injectable()
export class CreaRefundStripeService {
  constructor(
    @Inject('INFORMACION_PAGO_MODEL')
    private readonly informacionPagoModel: Model<InformacionPago>,
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    @Inject('DEVOLUCION_MODEL')
    private readonly devolucionModel: Model<Devolucion>,
    private stripeRefundsService: StripeRefundsService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private eliminarInformacionCompleto: EliminarInformacionCompleto,
    private eliminarTransaccionCompletoService: EliminarTransaccionCompletoService,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async crearRefundStripe(idPago: string): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let getInformacionPago = await this.informacionPagoModel.findOne({
        idPago: idPago,
      });
      if (getInformacionPago) {
        let formatFechaActual = new Date();
        // formatFechaActual.setDate(new Date(fechaFormat).getDate());
        let fechaCreacionInformacionPago = getInformacionPago.fechaCreacion;
        fechaCreacionInformacionPago.setDate(
          new Date(fechaCreacionInformacionPago).getDate() +
            diasMaximosDevolucion.dias,
        );

        if (formatFechaActual > fechaCreacionInformacionPago) {
          throw {
            codigo: HttpStatus.UNAUTHORIZED,
            codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
          };
        }
        let idInformacionPago = getInformacionPago._id;

        let getTransaccion = await this.transaccionModel.find({
          informacionPago: idInformacionPago,
        });

        let arrayTransaccion = [];
        let idUsuario;
        let montoTotal = 0;
        if (getTransaccion.length > 0) {
          for (const transaccion of getTransaccion) {
            arrayTransaccion.push(transaccion._id);
            idUsuario = transaccion.usuario;
            montoTotal += transaccion.monto;
          }
          const refund = await this.stripeRefundsService.createRefund(idPago);

          let dataDevolucion: any = {
            transaccion: arrayTransaccion,
            usuario: idUsuario,
            montoTotal: montoTotal,
            idPago: idPago,
            idRefund: refund.id,
            fechaCreacion: new Date(),
            fechaActualizacion: new Date(),
          };
          if (refund.status === statusRefundStripe.succeeded) {
            dataDevolucion.estado = estadosDevolucion.success;

            const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
              idUsuario,
            );
            let formatFechaActual = new Date();
            let fechaCreacionUsuario = getUsuario.fechaCreacion;
            let direccion = getUsuario.direccion;
            fechaCreacionUsuario.setDate(
              new Date(fechaCreacionUsuario).getDate() + dias.año,
            );

            console.log('formatFechaActual: ', formatFechaActual);
            console.log('fechaCreacionUsuario: ', fechaCreacionUsuario);
            //Si el usuario fue creado hace mas de un año
            if (formatFechaActual >= fechaCreacionUsuario) {
              console.log('eliminar usuario logico');
              //Eliminar la transaccion logicamente
              await this.eliminarTransaccionCompletoService.eliminarTransaccionLogica(
                arrayTransaccion,
                opts,
              );
              //Actualiza el estado del usuario a bloqueado del sistema
              let actualizaUsuario = await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
                idUsuario,
                estadosUsuario.bloqueadoSistema,
                opts,
              );
            } else {
              //Si el usuario fue creado recien
              console.log('eliminar usuario completo');
              const eliminarUsuario = await this.eliminarInformacionCompleto.eliminarUsuarioPerfilCompleto(
                idUsuario,
                direccion,
                opts,
              );
            }
          } else {
            dataDevolucion.estado = estadosDevolucion.pendiente;
          }

          let crearDevolucion = await new this.devolucionModel(
            dataDevolucion,
          ).save(opts);

          const dataHistoricoUser: any = {
            datos: crearDevolucion,
            usuario: idUsuario,
            accion: codigosCatalogoAcciones.crear,
            entidad: codigoEntidades.devolucion,
          };

          // crear el historico de usuario
          this.crearHistoricoService.crearHistoricoServer(dataHistoricoUser);

          let actualizaUsuario = await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
            idUsuario,
            estadosUsuario.bloqueadoRefund,
            opts,
          );

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          const result = {
            refund: refund,
            idDevolucion: crearDevolucion._id,
          };

          return result;
        } else {
          //Finaliza la transaccion
          await session.endSession();
          return null;
        }
      }
      //Finaliza la transaccion
      await session.endSession();
      return null;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async obtenerRefundStripe(fechaInicio: any, fechaFin: any): Promise<any> {
    try {
      const refund = await this.stripeRefundsService.listRefunds(
        fechaInicio,
        fechaFin,
      );
      return refund;
    } catch (error) {
      throw error;
    }
  }

  // Se ejecuta el 15 de cada mes a la 1:30am
  @Cron('0 30 1 15 * *')
  async verificaDevoluciones(): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };
      const getDevoluciones = await this.devolucionModel.find({
        estado: estadosDevolucion.pendiente,
      });
      if (getDevoluciones.length > 0) {
        for (const devolucion of getDevoluciones) {
          const idRefund = devolucion.idRefund;
          const idUsuario = devolucion.usuario;
          const idDevolucion = devolucion._id;
          const transacciones = devolucion.transaccion;
          const refund = await this.stripeRefundsService.getRefundById(
            idRefund,
          );
          console.log('Refund: ', refund);
          const estadoRefoud = refund.status;
          console.log('estadoRefoud: ', estadoRefoud);
          if (estadoRefoud === statusRefundStripe.succeeded) {
            await this.devolucionModel.findByIdAndUpdate(
              idDevolucion,
              { estado: estadosDevolucion.success },
              opts,
            );

            const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
              idUsuario,
            );
            let formatFechaActual = new Date();
            let fechaCreacionUsuario = getUsuario.fechaCreacion;
            let direccion = getUsuario.direccion;
            fechaCreacionUsuario.setDate(
              new Date(fechaCreacionUsuario).getDate() + dias.año,
            );

            console.log('formatFechaActual: ', formatFechaActual);
            console.log('fechaCreacionUsuario: ', fechaCreacionUsuario);
            //Comprueba si el usuario es recien creacion o ya fue creado hace mas de un año
            if (formatFechaActual >= fechaCreacionUsuario) {
              console.log('eliminar usuario logico');
              //Eliminar la transaccion logicamente
              await this.eliminarTransaccionCompletoService.eliminarTransaccionLogica(
                transacciones,
                opts,
              );
              //Actualiza el estado del usuario a bloqueado del sistema
              let actualizaUsuario = await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
                idUsuario,
                estadosUsuario.bloqueadoSistema,
                opts,
              );
            } else {
              console.log('eliminar usuario completo');
              const eliminarUsuario = await this.eliminarInformacionCompleto.eliminarUsuarioPerfilCompleto(
                idUsuario,
                direccion,
                opts,
              );
            }
          }
        }
        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
      } else {
        console.log('ninguna devolucion');
        //Finaliza la transaccion
        await session.endSession();
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
