import { HttpStatus, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { openStdin } from 'process';
import { CoinpaymentezInfoTransactionService } from 'src/drivers/coinpaymentez/services/coinpayments-info-transaction.service';
import { RetornoCuentaDto } from 'src/entidades/cuenta/entidad/retorno-cuenta.dto';
import { CrearEmailBienvenidaService } from 'src/entidades/emails/casos_de_uso/crear-email-bienvenida.service';
import { CrearPagoPaymentezService } from 'src/entidades/transaccion/casos_de_uso/crear-pago-paymentez.service';
import { GestionPagoCoinpaymentsService } from 'src/entidades/transaccion/casos_de_uso/gestion-pago-coinpayments.service';
import { GestionPagoPaymentezService } from 'src/entidades/transaccion/casos_de_uso/gestion-pago-paymentez.service';
import { erroresCuenta } from '../../../shared/enum-errores';
import {
  catalogoOrigen,
  codigosCatalogoSuscripcion,
  codigosMetodosPago,
  estadosUsuario,
  porcentajeComisionCoinpaiments,
  porcentajeComisionPaymentez,
} from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoTipoEmailService } from '../../catalogos/casos_de_uso/catalogo-tipo-email.service';
import { CrearEmailMenorEdadService } from '../../emails/casos_de_uso/crear-email-menor-edad.service';
import { CrearEmailReciboPagoService } from '../../emails/casos_de_uso/crear-email-recibo-pago.service';
import { CrearEmailService } from '../../emails/casos_de_uso/crear-email.service';
import { LoginService } from '../../login/casos_de_uso/login.service';
import { ActualizarSuscripcionService } from '../../suscripcion/casos_de_uso/actualizar-suscripcion.service';
import { ActualizarConversionTransaccionService } from './actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from './actualizar-transaccion.service';
import { GestionPagoPaypalService } from './gestion-pago-paypal.service';
import { GestionPagoStripeService } from './gestion-pago-stripe.service';
import { ObtenerTransaccionService } from './obtener-transaccion.service';
import { UpdateTransaccionPagoCriptoDto, UpdateTransaccionPagoDto } from '../entidad/transaccion.dto';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';

@Injectable()
export class ValidarPagoCriptoService {
  inactivaPago = estadosUsuario.inactivaPago;
  activaNoVerificado = estadosUsuario.activaNoVerificado;
  activo = estadosUsuario.activa;
  codigoMetodoPagoStripe = codigosMetodosPago.stripe;
  // codigoMetodoPagoPaypal = codigosMetodosPago.paypal;

  constructor(
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private actualizarConversionTransaccionService: ActualizarConversionTransaccionService,
    private crearEmailReciboPagoService: CrearEmailReciboPagoService,
    private coinpaymentezInfoTransactionService: CoinpaymentezInfoTransactionService,
    private gestionPagoCoinpaymentsService: GestionPagoCoinpaymentsService
  ) { }

  async validarPago(
    idTransaccion,
    email
  ): Promise<any> {
    const session = await mongoose.startSession();

    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };

    let transaccionDatabase;
    const opts = { session };

    try {

      let cuentaOk: RetornoCuentaDto = {};

      const transaccionQuery: any = await this.obtenerTransaccionService.obtenerTransaccionById(
        idTransaccion,
        opts,
      );
      if (!transaccionQuery) {
        throw {
          codigo: HttpStatus.EXPECTATION_FAILED,
          codigoNombre: erroresCuenta.TRANSACCION_NO_VALIDA,
        };
      }

      const informacionPago = transaccionQuery.informacionPago;
      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithID(
        transaccionQuery.usuario._id,
        opts,
      );

      //data para consultar transaccion en coinpayments
      let objTransaccion = {
        txid: informacionPago.idPago,
        full: 1
      }

      //VALIDA TRAMSACCION EN COINPAYMENTS QUE ESTE OK
      const getTransaccion = await this.coinpaymentezInfoTransactionService.obtenerTransaction(objTransaccion)


      if (getTransaccion.status === 100) {
        // ___________________________________________ Verificar Pago OK____________________________________________

        // ______________________________ actualizar transaccion (dos origenes) ________________________________
        const transaccionUser: any = await this.obtenerTransaccionService.obtenerTransaccionByUsuarioByPago(
          transaccionQuery.usuario._id,
          informacionPago._id,
          opts,
        );
        for (const transaccion of transaccionUser.transacciones) {

          if (transaccion.origen === null) {
            const montoRecibidoCripto = getTransaccion.receivedf

            const dataUpdate: UpdateTransaccionPagoCriptoDto = {
              monto: montoRecibidoCripto, // valor neto recibido
            };

            // if (transaccion.informacionPago.idPago === transaccionQuery.informacionPago.idPago) {
            transaccionDatabase = await session.withTransaction(async () => {
              const updatetransaccion = await this.actualizarTransaccionService.actualizarTransaccionCripto(
                transaccion._id,
                opts,
                dataUpdate,
              );
              if (!updatetransaccion) {
                await session.abortTransaction();
                return;
              }
            }, transactionOptions);

          } else {

            //__________________Calcular valor total por transaccion________________
            const getNewValor = this.gestionPagoCoinpaymentsService.calcularValorTotalComisionCoinpayments(
              // detallePagoStripe.amount,
              transaccion.monto,
              porcentajeComisionCoinpaiments.porcentaje,
            );

            const dataUpdate: UpdateTransaccionPagoDto = {
              comisionTransferencia: getNewValor.newFee, // valor de la comision
              totalRecibido: getNewValor.newAmountReceived, // total recibido
              origenPais: null,
            };

            // if (transaccion.informacionPago.idPago === transaccionQuery.informacionPago.idPago) {
            transaccionDatabase = await session.withTransaction(async () => {
              const updatetransaccion = await this.actualizarTransaccionService.actualizarTransaccion(
                transaccion._id,
                opts,
                dataUpdate,
              );
              if (!updatetransaccion) {
                await session.abortTransaction();
                return;
              }
            }, transactionOptions);
            // }

            //__________________ Actualizar conversion transaccion ________________
            transaccionDatabase = await session.withTransaction(async () => {
              const updateConversion = await this.actualizarConversionTransaccionService.actualizarConversionStripe(
                transaccion._id,
                dataUpdate,
                opts,
              );
              if (!updateConversion) {
                await session.abortTransaction();
                return;
              }
            }, transactionOptions);

          }

        }

        const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(
          getUsuario.idioma,
        );

        //_______________________________ enviar EMAIL recibo pago ___________________________________
        const dataEmailPago = {
          idInformacionPago: informacionPago._id as string,
          usuario: getUsuario._id as string,
          nombre: getUsuario.perfiles[0].nombre as string,
          emailDestinatario: getUsuario.email as string,
          idioma: idiomaUsuario.codNombre as string,
        };
        await this.crearEmailReciboPagoService.crearEmailReciboPago(
          dataEmailPago,
        );

        // proceso exitoso
        return true;
      } else {
        await session.endSession();
        return null;
      }
    } catch (error) {
      throw error;
    }
  }

  validarCodigosMetodosPago(codigo): Boolean {
    const codigos = [
      codigosMetodosPago.stripe,
      codigosMetodosPago.payments1,
      codigosMetodosPago.payments2,
      codigosMetodosPago.panama,
    ];
    if (codigos.indexOf(codigo) !== -1) {
      return true;
    } else {
      return false;
    }
  }
}
