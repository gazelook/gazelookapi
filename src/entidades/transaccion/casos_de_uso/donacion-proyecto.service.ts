import { forwardRef, HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearCuentaDto } from 'src/entidades/cuenta/entidad/cuenta.dto';
import { RetornoCuentaCoinpaymentsDto, RetornoCuentaDto } from 'src/entidades/cuenta/entidad/retorno-cuenta.dto';
import { CrearEmailReciboPagoService } from 'src/entidades/emails/casos_de_uso/crear-email-recibo-pago.service';
import { ActualizaEstadoProyectoService } from 'src/entidades/proyectos/casos_de_uso/actualiza-estado-proyecto.service';
import { ObtenerProyetoIdService } from 'src/entidades/proyectos/casos_de_uso/obtener-proyecto-id.service';
import { DonacionProyectoDto } from 'src/entidades/proyectos/entidad/donacion-proyecto-dto';
import { CrearPagoStripeService } from 'src/entidades/transaccion/casos_de_uso/crear-pago-stripe.service';
import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';
import { listaMonedasCripto } from 'src/money/enum-lista-money';
import { erroresCuenta } from 'src/shared/enum-errores';
import {
  codigosMetodosPago, descripcionTransPagosGazelook, estadosProyecto, porcentajeComisionPaymentez
} from '../../../shared/enum-sistema';
import { UpdateTransaccionPagoDto } from '../entidad/transaccion.dto';
import { ActualizarConversionTransaccionService } from './actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from './actualizar-transaccion.service';
import { CrearPagoCoinpaymentezService } from './crear-pago-coinpaymentez.service';
import { CrearPagoPaymentezService } from './crear-pago-paymentez.service';
import { GestionPagoPaymentezService } from './gestion-pago-paymentez.service';
import { ObtenerBeneficiarioTransaccionService } from './obtener-beneficiario-transaccion.service';
import { ObtenerTransaccionService } from './obtener-transaccion.service';

const sw = require('stopword');

@Injectable()
export class DonacionProyectoService {


  constructor(
    // @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
    @Inject(forwardRef(() => CrearPagoStripeService)) private readonly crearPagoStripeService: CrearPagoStripeService,
    private readonly crearPagoPaymentezService: CrearPagoPaymentezService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private gestionPagoPaymentezService: GestionPagoPaymentezService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private actualizarConversionTransaccionService: ActualizarConversionTransaccionService,
    private crearPagoCoinpaymentezService: CrearPagoCoinpaymentezService,
    private crearEmailReciboPagoService: CrearEmailReciboPagoService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private obtenerBeneficiarioTransaccionService: ObtenerBeneficiarioTransaccionService,
    private obtenerProyetoIdService: ObtenerProyetoIdService,
    private actualizaEstadoProyectoService: ActualizaEstadoProyectoService
  ) { }

  async donacionProyecto(
    dataProyecto: DonacionProyectoDto
  ): Promise<any> {

    // Inicia proceso de transaccion
    const session = await mongoose.startSession();


    await session.startTransaction();

    try {

      const opts = { session };

      // _____________________ datos de pago _________________
      const tipoMonedaRegistro = dataProyecto.monedaRegistro.codNombre;
      let currency2
      if (dataProyecto.pagoCripto) {
        currency2 = dataProyecto.pagoCripto.simboloCripto
      }

      let datosPago: any = {
        email: dataProyecto.email,
        idUsuario: dataProyecto.idUsuario,
        metodoPago: dataProyecto.metodoPago.codigo,
        tipoMonedaRegistro: tipoMonedaRegistro,
        currency2: currency2,
        currency1: listaMonedasCripto.BTC
      };


      let total = 0;
      for (let transaccion of dataProyecto.transacciones) {
        total += transaccion.monto;
      }

      datosPago.nombres = dataProyecto.datosFacturacion.nombres;
      datosPago.telefono = dataProyecto.datosFacturacion.telefono;
      datosPago.direccion = dataProyecto.datosFacturacion?.direccion
      datosPago.monto = total;
      datosPago.transacciones = dataProyecto.transacciones;
      datosPago.creacionCuenta = true;
      datosPago.description = descripcionTransPagosGazelook.fondosReservados;
      datosPago.idProyecto = dataProyecto.idProyecto;
      datosPago.idPago = dataProyecto.datosFacturacion.idPago;
      datosPago.pagoCripto = dataProyecto.pagoCripto

      // ___________________________________________Crear Pago____________________________________________
      let cuentaOk: any = {};

      if (codigosMetodosPago.stripe === dataProyecto.metodoPago.codigo) {
        const pagoStripe = await this.crearPagoStripeService.crearPagoStripe(
          datosPago,
          opts,
          true
        );

        cuentaOk.idPago = pagoStripe.stripe.client_secret;
        cuentaOk.idTransaccion = pagoStripe.idTransaccion;
        console.log('TERMINAAAAAAAAAA PAGOOOOOOOOOOOOOOOOOOO STRIPE');

        // proceso exitoso
        await session.commitTransaction();
        session.endSession();
      }

      if (codigosMetodosPago.payments1 === dataProyecto.metodoPago.codigo
        || codigosMetodosPago.payments2 === dataProyecto.metodoPago.codigo) {
        const pagoPaymentez = await this.crearPagoPaymentezService.crearPagoPaymentez(
          datosPago,
          opts,
          true
        );
        cuentaOk.idPago = dataProyecto.datosFacturacion.idPago;
        cuentaOk.idTransaccion = pagoPaymentez.idTransaccion;


        const transaccionQuery: any = await this.obtenerTransaccionService.obtenerTransaccionById(
          cuentaOk.idTransaccion,
          opts,
        );

        if (!transaccionQuery) {
          throw {
            codigo: HttpStatus.EXPECTATION_FAILED,
            codigoNombre: erroresCuenta.TRANSACCION_NO_VALIDA,
          };
        }

        const informacionPago = transaccionQuery.informacionPago;

        // ______________________________actualizar transaccion (dos origenes)________________________________
        const transaccionUser: any = await this.obtenerTransaccionService.obtenerTransaccionByUsuarioByPago(
          dataProyecto.idUsuario,
          informacionPago._id,
          opts,
        );
        for (const transaccion of transaccionUser) {
          //__________________Calcular valor total por transaccion________________
          const getNewValor = this.gestionPagoPaymentezService.calcularValorTotalComisionPaymentez(
            // detallePagoStripe.amount,
            transaccion.monto,
            porcentajeComisionPaymentez.porcentaje,
          );

          const dataUpdate: UpdateTransaccionPagoDto = {
            comisionTransferencia: getNewValor.newFee, // valor de la comision
            totalRecibido: getNewValor.newAmountReceived, // total recibido
            origenPais: null,
          };


          // if (transaccion.informacionPago.idPago === transaccionQuery.informacionPago.idPago) {
          // transaccionDatabase = await session.withTransaction(async () => {
          const updatetransaccion = await this.actualizarTransaccionService.actualizarTransaccion(
            transaccion._id,
            opts,
            dataUpdate,
          );
          //   if (!updatetransaccion) {
          //     await session.abortTransaction();
          //     return;
          //   }
          // }, transactionOptions);
          // }

          //__________________Actualizar conversion transaccion________________
          // transaccionDatabase = await session.withTransaction(async () => {
          const updateConversion = await this.actualizarConversionTransaccionService.actualizarConversionStripe(
            transaccion._id,
            dataUpdate,
            opts,
          );
          //   if (!updateConversion) {
          //     await session.abortTransaction();
          //     return;
          //   }
          // }, transactionOptions);
        }

        const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithID(
          transaccionQuery.usuario._id,
          opts,
        );

        const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(
          getUsuario.idioma,
        );

        //_______________________________enviar recibo pago___________________________________
        const dataEmailPago = {
          idInformacionPago: informacionPago._id as string,
          usuario: getUsuario._id as string,
          nombre: getUsuario.perfiles[0].nombre as string,
          emailDestinatario: getUsuario.email as string,
          idioma: idiomaUsuario.codNombre as string,
          autorizacionCodePaymentez: dataProyecto.autorizacionCodePaymentez as string
        };
        // await this.crearEmailReciboPagoService.crearEmailReciboPago(
        //   dataEmailPago,
        // );

        // transaccionDatabase = await session.withTransaction(async () => {
        await this.crearEmailReciboPagoService.crearEmailReciboPago(
          dataEmailPago,
          opts
        );
        // }, transactionOptions);
        console.log('TERMINAAAAAAAAAA PAGOOOOOOOOOOOOOOOOOOO PAYMENTEZ');

        // proceso exitoso
        await session.commitTransaction();
        session.endSession();

        let getProyecto = await this.obtenerProyetoIdService.obtenerProyectoById(dataProyecto.idProyecto)
        let montoFaltanteProyecto = await this.obtenerBeneficiarioTransaccionService.obtenerMontoFaltanteEntregarProyecto(getProyecto._id, getProyecto.valorEstimado)
        
        //SI EL MONTO FALTANTE DEL PROYECTO ES IGUAL A CERO CAMBIA DE ESTADO A PRE ESTRATEGIA
        if (montoFaltanteProyecto === 0) {
          await this.actualizaEstadoProyectoService.actualizaEstadoProyecto(getProyecto._id, estadosProyecto.proyectoEnEsperaFondos, estadosProyecto.proyectoPreEstrategia)
        }

      }

      if (codigosMetodosPago.cripto === dataProyecto.metodoPago.codigo) {
        // let cuentaOk: RetornoCuentaCoinpaymentsDto = {};
        const pagoCoinpayments = await this.crearPagoCoinpaymentezService.crearPagoCoinpaymentez(
          datosPago,
          opts,
          true
        );

        cuentaOk.coinpayments = pagoCoinpayments.coinpayments;
        cuentaOk.idTransaccion = pagoCoinpayments.idTransaccion;

        console.log('TERMINAAAAAAAAAA PAGOOOOOOOOOOOOOOOOOOO CRIPTO');
        // proceso exitoso
        await session.commitTransaction();
        session.endSession();
      }

      return cuentaOk
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
