import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { Devolucion } from 'src/drivers/mongoose/interfaces/devolucion/devolucion.interface';
import { InformacionPago } from 'src/drivers/mongoose/interfaces/informacion_pago/informacion-pago.interface';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { StripeRefundsService } from 'src/drivers/stripe/services/stripe-refunds.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { ActualizarUsuarioService } from 'src/entidades/usuario/casos_de_uso/actualizar-usuario.service';
import { EliminarInformacionCompleto } from 'src/entidades/usuario/casos_de_uso/eliminar-informacion-completo.service';
import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';
import { erroresGeneral } from 'src/shared/enum-errores';
import {
  codigoEntidades,
  codigosCatalogoAcciones,
  dias,
  diasMaximosDevolucion,
  estadosDevolucion,
  estadosUsuario,
  statusRefundStripe,
} from 'src/shared/enum-sistema';
import { EliminarTransaccionCompletoService } from './eliminar-transaccion-completo.service';

const mongoose = require('mongoose');

@Injectable()
export class CreaRefundPaymentezService {
  constructor(
    @Inject('INFORMACION_PAGO_MODEL')
    private readonly informacionPagoModel: Model<InformacionPago>,
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    @Inject('DEVOLUCION_MODEL')
    private readonly devolucionModel: Model<Devolucion>,
    private stripeRefundsService: StripeRefundsService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private eliminarInformacionCompleto: EliminarInformacionCompleto,
    private eliminarTransaccionCompletoService: EliminarTransaccionCompletoService,
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async crearRefundPaymentez(idPago: string, idRefound: string): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let getInformacionPago = await this.informacionPagoModel.findOne({
        idPago: idPago,
      });
      if (getInformacionPago) {
        let formatFechaActual = new Date();
        // formatFechaActual.setDate(new Date(fechaFormat).getDate());
        let fechaCreacionInformacionPago = getInformacionPago.fechaCreacion;
        fechaCreacionInformacionPago.setDate(
          new Date(fechaCreacionInformacionPago).getDate() +
          diasMaximosDevolucion.dias,
        );

        if (formatFechaActual > fechaCreacionInformacionPago) {
          throw {
            codigo: HttpStatus.UNAUTHORIZED,
            codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
          };
        }
        let idInformacionPago = getInformacionPago._id;

        let getTransaccion = await this.transaccionModel.find({
          informacionPago: idInformacionPago,
        });

        let arrayTransaccion = [];
        let idUsuario;
        let montoTotal = 0;
        if (getTransaccion.length > 0) {
          for (const transaccion of getTransaccion) {
            arrayTransaccion.push(transaccion._id);
            idUsuario = transaccion.usuario;
            montoTotal += transaccion.monto;
          }
          // const refund = await this.stripeRefundsService.createRefund(idPago);

          let dataDevolucion: any = {
            transaccion: arrayTransaccion,
            usuario: idUsuario,
            montoTotal: montoTotal,
            idPago: idPago,
            idRefund: idRefound,
            fechaCreacion: new Date(),
            fechaActualizacion: new Date(),
          };
          // if (refund.status === statusRefundStripe.succeeded) {
            dataDevolucion.estado = estadosDevolucion.success;

            const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(
              idUsuario,
            );
            let formatFechaActual = new Date();
            let fechaCreacionUsuario = getUsuario.fechaCreacion;
            let direccion = getUsuario.direccion;
            fechaCreacionUsuario.setDate(
              new Date(fechaCreacionUsuario).getDate() + dias.año,
            );

            console.log('formatFechaActual: ', formatFechaActual);
            console.log('fechaCreacionUsuario: ', fechaCreacionUsuario);
            //Si el usuario fue creado hace mas de un año
            if (formatFechaActual >= fechaCreacionUsuario) {
              console.log('eliminar usuario logico');
              //Eliminar la transaccion logicamente
              await this.eliminarTransaccionCompletoService.eliminarTransaccionLogica(
                arrayTransaccion,
                opts,
              );
              //Actualiza el estado del usuario a bloqueado del sistema
              let actualizaUsuario = await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
                idUsuario,
                estadosUsuario.bloqueadoRefund,
                opts,
              );
            } else {
              //Si el usuario fue creado recien
              console.log('eliminar usuario completo');
              const eliminarUsuario = await this.eliminarInformacionCompleto.eliminarUsuarioPerfilCompleto(
                idUsuario,
                direccion,
                opts,
              );
            }
          // } 
          // else {
          //   dataDevolucion.estado = estadosDevolucion.pendiente;
          // }

          let crearDevolucion = await new this.devolucionModel(
            dataDevolucion,
          ).save(opts);

          const dataHistoricoUser: any = {
            datos: crearDevolucion,
            usuario: idUsuario,
            accion: codigosCatalogoAcciones.crear,
            entidad: codigoEntidades.devolucion,
          };

          // crear el historico de usuario
          this.crearHistoricoService.crearHistoricoServer(dataHistoricoUser);

          // let actualizaUsuario = await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(
          //   idUsuario,
          //   estadosUsuario.bloqueadoRefund,
          //   opts,
          // );

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          const result = {
            // refund: refund,
            idDevolucion: crearDevolucion._id,
          };

          return result;
        } else {
          //Finaliza la transaccion
          await session.endSession();
          return null;
        }
      }
      //Finaliza la transaccion
      await session.endSession();
      return null;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

}
