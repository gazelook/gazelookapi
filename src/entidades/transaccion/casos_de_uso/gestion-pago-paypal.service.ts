import { Injectable } from '@nestjs/common';

import { nombreEstadosPagos, pagoPaypal } from 'src/shared/enum-sistema';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class GestionPagoPaypalService {
  paypal = require('@paypal/checkout-server-sdk');
  private PAYPAL_CLIEN_ID: string;
  private PAYPAL_CLIENT_SECRET: string;

  constructor(private config: ConfigService) {
    this.PAYPAL_CLIEN_ID = config.get<string>('PAYPAL_CLIEN_ID');
    this.PAYPAL_CLIENT_SECRET = config.get<string>('PAYPAL_CLIENT_SECRET');
  }

  async obtenerEstadoPagoPaypal(orderId): Promise<Boolean> {
    const environment = new this.paypal.core.SandboxEnvironment(
      this.PAYPAL_CLIEN_ID,
      this.PAYPAL_CLIENT_SECRET,
    );
    //const client = new this.paypal.core.PayPalHttpClient(environment);
    const client = new this.paypal.core.PayPalHttpClient(environment);
    try {
      //let request = new this.paypal.orders.OrdersCaptureRequest(orderId);
      let request = new this.paypal.orders.OrdersGetRequest(orderId);

      //request.requestBody({});

      // Call API with your client and get a response for your call
      let response: any = await client.execute(request);

      // If call returns body in response, you can get the deserialized version from the result attribute of the response.

      if (response.result.status === nombreEstadosPagos.COMPLETED) {
        return true;
      }
      return false;
    } catch (error) {
      throw error;
    }
  }
}
