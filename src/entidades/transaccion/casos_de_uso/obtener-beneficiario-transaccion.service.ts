import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Beneficiario } from 'src/drivers/mongoose/interfaces/beneficiario/beneficiario.interface';

import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { estadosTransaccion } from 'src/shared/enum-sistema';

@Injectable()
export class ObtenerBeneficiarioTransaccionService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    @Inject('BENEFICIARIO_MODEL')
    private readonly beneficiarioModel: Model<Beneficiario>,
  ) { }

  //Obtiene el monto faltante que se le debe aun entregar al proyecto
  async obtenerMontoFaltanteEntregarProyecto(
    idProyecto,
    valorEstimado,
  ): Promise<any> {
    try {
      const beneficiario = await this.beneficiarioModel.find({
        proyecto: idProyecto,
      });

      let montoFaltanteProyecto: any;
      let montoBeneficiario = 0
      if (beneficiario.length > 0) {
        for (const benf of beneficiario) {
          let idBeneficiario = benf._id;
          const transaccion = await this.transaccionModel.findOne({
            beneficiario: idBeneficiario,
            estado: estadosTransaccion.activa
          });
          if (transaccion) {
            if(transaccion.metodoPago){
              montoBeneficiario += transaccion.totalRecibido
            }else{
              montoBeneficiario += transaccion.monto
            }
            
          }

        }
        montoFaltanteProyecto = valorEstimado - montoBeneficiario;
      }
      return montoFaltanteProyecto;
    } catch (error) {
      throw error;
    }
  }
}
