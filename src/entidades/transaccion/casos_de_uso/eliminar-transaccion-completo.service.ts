import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { EliminarSuscripcionCompletoService } from '../../suscripcion/casos_de_uso/eliminar-suscripcion-completo.service';
import {
  estadosTransaccion,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class EliminarTransaccionCompletoService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private eliminarSuscripcionCompletoService: EliminarSuscripcionCompletoService,
  ) {}

  async eliminarTransaccionCompleto(usuario, opts): Promise<any> {
    // //Obtiene el codigo de la accion a realizarse
    // const accion = await this.obtenerNombreAccionService.obtenerNombreAccion('crear')
    // const getAccion = accion['codigo'];
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.retiroUsuario,
        entidad.codigo,
      );
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      const transacciones = await this.transaccionModel.find({
        usuario: usuario,
      });
      if (transacciones.length > 0) {
        for (const trans of transacciones) {
          //await this.informacionPago.deleteOne({_id: trans.informacionPago})
          //await this.transaccionModel.deleteOne({_id: trans._id});
          const dataupdate = {
            estado: estado.codigo,
            usuario: null,
          };
          const updateTransaccion = await this.transaccionModel.findByIdAndUpdate(
            trans._id,
            dataupdate,
            opts,
          );
          await this.eliminarSuscripcionCompletoService.eliminarSuscripcionCompleto(
            trans._id,
            opts,
          );
        }
      }

      return true;
    } catch (error) {
      throw error;
    }
  }

  async eliminarTransaccionFisica(usuario, opts): Promise<any> {
    // //Obtiene el codigo de la accion a realizarse
    // const accion = await this.obtenerNombreAccionService.obtenerNombreAccion('crear')
    // const getAccion = accion['codigo'];
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.retiroUsuario,
        entidad.codigo,
      );
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      const transacciones = await this.transaccionModel.find({
        usuario: usuario,
      });
      if (transacciones.length > 0) {
        for (const trans of transacciones) {
          await this.eliminarTransaccionCompletoById(trans._id, opts);
          await this.eliminarSuscripcionCompletoService.eliminarSuscripcionFisica(
            trans._id,
            opts,
          );
        }
      }

      return true;
    } catch (error) {
      throw error;
    }
  }

  async eliminarTransaccionCompletoById(idTransaccion, opts): Promise<Boolean> {
    try {
      await this.transaccionModel.deleteOne({ _id: idTransaccion }, opts);

      return true;
    } catch (error) {
      throw error;
    }
  }

  async eliminarTransaccionLogica(transacciones, opts): Promise<Boolean> {
    try {
      if (transacciones.length > 0) {
        for (const transaccion of transacciones) {
          await this.transaccionModel.findByIdAndUpdate(
            transaccion,
            { estado: estadosTransaccion.eliminada },
            opts,
          );

          await this.eliminarSuscripcionCompletoService.eliminarSuscripcionCompleto(
            transaccion,
            opts,
          );
        }
      }

      return true;
    } catch (error) {
      throw error;
    }
  }
}
