import { Injectable } from '@nestjs/common';
import { CoinpaymentezRatesService } from 'src/drivers/coinpaymentez/services/coinpayments-rates.service';


@Injectable()
export class ObtenerRatesCoinpaymentezService {
  constructor(
    private coinpaymentezRatesService: CoinpaymentezRatesService
  ) { }

  async ObtenerRates(
  ): Promise<any> {
    try {
      const rates = await this.coinpaymentezRatesService.obtenerRates();

      return rates;
    } catch (error) {
      throw error;
    }
  }

}
