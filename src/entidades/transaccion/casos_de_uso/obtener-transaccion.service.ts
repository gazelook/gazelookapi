import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { estadosTransaccion } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerTransaccionService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
  ) { }

  async obtenerTransaccionById(idTransaccion, opts?): Promise<any> {
    try {
      if (opts) {
        const transaccion = await this.transaccionModel
          .findById(idTransaccion)
          .populate('informacionPago')
          .populate('usuario')
          .session(opts.session);

        return transaccion;
      } else {
        const transaccion = await this.transaccionModel
          .findById(idTransaccion)
          .populate('informacionPago')
          .populate('usuario');

        return transaccion;
      }
    } catch (error) {
      throw error;
    }
  }


  async obtenerTransaccionByUsuarioByPago(idUsuario, informacionPago, opts?): Promise<any> {
    try {
      if (opts) {
        const transaccion = await this.transaccionModel
          .find({
            usuario: idUsuario,
            informacionPago: informacionPago
          })
          .populate('informacionPago')
          .populate('usuario')
          .session(opts.session);

          
        return transaccion;
      } else {
        const transaccion = await this.transaccionModel
          .find({
            usuario: idUsuario,
            informacionPago: informacionPago
          })
          .populate('informacionPago')
          .populate('usuario');

        return transaccion;
      }
    } catch (error) {
      throw error;
    }
  }

  async obtenerTransaccionesUsuario(idUsuario: string): Promise<any> {
    try {
      const transaccion = await this.transaccionModel
        .find({ usuario: idUsuario, estado: estadosTransaccion.pendiente })
        .populate([
          {
            path: 'informacionPago',
          },
          {
            path: 'usuario',
          },
        ]);

      return transaccion;
    } catch (error) {
      throw error;
    }
  }

  async obtenerTransaccionesActivasUsuario(idUsuario: string): Promise<any> {
    try {
      //Obtener fecha actual
      let fechaActual = new Date();

      //Obtener fecha actual menos 15 dias
      let fecha = new Date();
      fecha.setDate(fecha.getDate() - 15);

      const transaccion = await this.transaccionModel
        .find(
          {
            $and: [
              { usuario: idUsuario },
              { estado: estadosTransaccion.activa },
              { fechaCreacion: { $gte: fecha } },//mayor o igual que
              { fechaCreacion: { $lte: fechaActual } },// menor o igual que
            ],
          })
        .populate([
          {
            path: 'informacionPago',
          },
          {
            path: 'usuario',
          },
          {
            path: 'conversionTransaccion',
          },
        ]);

      let arrayTransacciones = []
      if (transaccion.length > 0) {
        for (const trans of transaccion) {
          let conversionTransaccion = []
          if (trans.conversionTransaccion.length > 0) {
            for (const conversion of trans.conversionTransaccion) {
              let obtConversion = {
                _id: conversion._id,
                principal: conversion.principal,
                moneda: {
                  codNombre: conversion.moneda
                },
                monto: conversion.monto
              }
              conversionTransaccion.push(obtConversion)
            }
          }
          let objTransaccion = {
            _id: trans._id,
            metodoPago: {
              codigo: trans.metodoPago
            },
            informacionPago: {
              _id: trans.informacionPago._id,
              datos: trans.informacionPago.datos,
              idPago: trans.informacionPago.idPago,
            },
            monto: trans.monto,
            moneda: {
              codNombre: trans.moneda
            },
            conversionTransaccion: conversionTransaccion,
            fechaCreacion: trans.fechaCreacion
          }
          arrayTransacciones.push(objTransaccion)

        }
      }
      return arrayTransacciones;

    } catch (error) {
      throw error;
    }
  }

}
