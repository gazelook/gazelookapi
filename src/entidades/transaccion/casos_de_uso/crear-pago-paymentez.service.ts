import { Injectable } from '@nestjs/common';
import { CrearTransaccionMontoFaltanteProyectoService } from './crear-transaccion-monto-faltante-proyectos.service';
import { CrearTransaccionService } from './crear-transaccion.service';

@Injectable()
export class CrearPagoPaymentezService {
  constructor(private crearTransaccionService: CrearTransaccionService,
    private crearTransaccionMontoFaltanteProyectoService: CrearTransaccionMontoFaltanteProyectoService) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async crearPagoPaymentez(datosPago: any, opts?: any, donacion?: any): Promise<any> {
    try {
      const idPago = datosPago.idPago;

      // guardar transacción y suscripcon con estado pendiente
      const numeroRecibo = this.crearTransaccionService.generarNumeroRecibo();

      let transaccion
      if(donacion){
        
        transaccion = await this.crearTransaccionMontoFaltanteProyectoService.crearTransaccionMontoFaltanteProyectosService(
          datosPago,
          idPago,
          numeroRecibo,
          opts,
        );
        
      }else{
        transaccion = await this.crearTransaccionService.crearTransaccion(
          datosPago,
          idPago,
          numeroRecibo,
          opts,
        );
      }

      const result = {
        idTransaccion: transaccion._id,
      };

      return result;
    } catch (error) {
      throw error;
    }
  }
}
