import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Beneficiario } from 'src/drivers/mongoose/interfaces/beneficiario/beneficiario.interface';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
import {
  beneficiario,
  catalogoOrigen,
  codigoEntidades,
  codigosCatalogo,
  codigosCatalogoAcciones,
  codigosEstadosBeneficiario, descripcionTransPagosGazelook,
  estadosTransaccion,
  nombreAcciones, nombreEntidades
} from 'src/shared/enum-sistema';
import { ConversionTransaccionDto } from '../entidad/conversion-transaccion.dto';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearConversionTransaccionService } from './crear-conversion-transaccion.service';
import { CrearinformacionPagoService } from './crear-informacion-pago.service';
import { CrearTransaccionFondosProyectoService } from './crear-transaccion-fondos-proyectos.service';
import * as mongoose from 'mongoose';

@Injectable()
export class CrearTransaccionMontoFaltanteProyectoService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    @Inject('BENEFICIARIO_MODEL')
    private readonly beneficiarioModel: Model<Beneficiario>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private crearTransaccionFondosProyectoService: CrearTransaccionFondosProyectoService,
    private crearinformacionPagoService: CrearinformacionPagoService,
    private crearConversionTransaccionService: CrearConversionTransaccionService
  ) { }

  async crearTransaccionMontoFaltanteProyectosService(
    datosPago: any,
    idPago: any,
    numeroRecibo: string,
    opts: any,
  ): Promise<any> {
    // //Obtiene el codigo de la accion a realizarse

    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );


      let transaccion
      // guardar informacion de pago
      let datosInfoPago = {
        nombres: datosPago.nombres,
        telefono: datosPago.telefono,
        direcccion: datosPago.direccion,
        email: datosPago.email,
      };
      const infoPago = await this.crearinformacionPagoService.crearInformacionPago(
        datosInfoPago,
        idPago,
        opts,
      );

      //Objeto de beneficiario
      const beneficiarioDto = {
        tipo: beneficiario.proyecto,
        usuario: null,
        proyecto: datosPago.idProyecto,
        estado: codigosEstadosBeneficiario.activa,
      };

      //Se guarda el beneficiario
      let crearBeneficiario = await new this.beneficiarioModel(
        beneficiarioDto,
      ).save(opts);

      //datos para guardar el historico
      const newHistoricoBeneficiario: any = {
        datos: crearBeneficiario,
        usuario: '',
        accion: codigosCatalogoAcciones.crear,
        entidad: codigoEntidades.entidadBeneficiario,
        tipo: codigosCatalogo.reparticionFondos,
      };

      //Crear historico del beneficiario
      this.crearHistoricoService.crearHistoricoServer(
        newHistoricoBeneficiario,
      );

      for (const pagoCuenta of datosPago.transacciones) {
        const idTransaccion = new mongoose.mongo.ObjectId();

        console.log('idTransaccion: ', idTransaccion);
        // -------------------------------- Crear Conversion ---------------------------
        const dataConversion: ConversionTransaccionDto = {
          monedaUsuario: datosPago.tipoMonedaRegistro,
          monedaDefault: pagoCuenta.moneda.codNombre,
          montoDefault: pagoCuenta.monto,
          idTransaccion: idTransaccion.toHexString(),
        };
        const conversiones = await this.crearConversionTransaccionService.crearConversionTransaccion(
          dataConversion,
          opts,
        );

        const transaccionDto = {
          _id: idTransaccion,
          estado: estadosTransaccion.pendiente,
          monto: pagoCuenta.monto,
          moneda: pagoCuenta.moneda.codNombre,
          descripcion:
            descripcionTransPagosGazelook.fondosReservados,
          origen: catalogoOrigen.fondos_reservados,
          destino: catalogoOrigen.donacion,
          balance: [],
          beneficiario: crearBeneficiario._id,
          metodoPago: datosPago.metodoPago,
          informacionPago: infoPago._id,
          usuario: datosPago.idUsuario,
          conversionTransaccion: conversiones,
          numeroRecibo: numeroRecibo,
        };

        transaccion = await new this.transaccionModel(transaccionDto).save(
          opts,
        );

        //datos para guardar el historico
        const newHistorico: any = {
          datos: transaccion,
          usuario: '',
          accion: accionCrear.codigo,
          entidad: entidad.codigo,
        };

        //Crear historico para transacción
        this.crearHistoricoService.crearHistoricoServer(newHistorico);


      }

      return transaccion;
    } catch (error) {
      throw error;
    }
  }
}
