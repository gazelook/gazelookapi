import { Injectable } from '@nestjs/common';

@Injectable()


export class ObtenerDiaService {
  contador = 1

  constructor(
  ) { }

  async obtenerDia(
  ): Promise<any> {

    try {

      let dato
      if (this.contador % 2 === 0) {
        dato = 1
      } else {
        dato = 2
      }

      this.contador = this.contador + 1
      return dato

    } catch (error) {
      throw error;
    }
  }
}
