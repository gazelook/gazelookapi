import { CrearTransaccionService } from './crear-transaccion.service';
import { Injectable } from '@nestjs/common';

import Stripe from 'stripe';

import {
  descripcionTransPagosGazelook,
  pagoStripe,
} from 'src/shared/enum-sistema';
import { NuevosDatosPagoDto } from '../entidad/actualizar-pago.dto';
import { CustomersService } from '../../../drivers/stripe/services/payment-customers.service';
import { PaymentIntentsService } from '../../../drivers/stripe/services/payment-intents.service';
import { CrearTransaccionMontoFaltanteProyectoService } from './crear-transaccion-monto-faltante-proyectos.service';

@Injectable()
export class CrearPagoStripeService {
  constructor(
    private crearTransaccionService: CrearTransaccionService,
    private customersService: CustomersService,
    private paymentIntentsService: PaymentIntentsService,
    private crearTransaccionMontoFaltanteProyectoService: CrearTransaccionMontoFaltanteProyectoService
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async crearPagoStripe(datosPago: any, opts?: any, donacion?: any): Promise<any> {
    try {
      // Guardar cliente en stripe
      const nuevoCliente: any = {
        name: datosPago.nombres,
        description: datosPago.description || descripcionTransPagosGazelook.suscripcion,
        email: datosPago.email,
        phone: datosPago.telefono,
        address: {
          line1: datosPago.direccion,
          // country: dataUser.pais,
        },
        // currency: 'usd'
      };

      const customer = await this.customersService.createCustomer(nuevoCliente);

      const monto = Math.floor(datosPago.monto * 100);

      const paymentIntent = await this.paymentIntentsService.createPaymentsIntent(
        {
          amount: monto,
          userEmail: datosPago.email,
          idCustomer: customer.id,
        },
      );

      // guardar transacción y suscripcon con estado pendiente
      const numeroRecibo = this.crearTransaccionService.generarNumeroRecibo();

      let transaccion
      if(donacion){
        
        transaccion = await this.crearTransaccionMontoFaltanteProyectoService.crearTransaccionMontoFaltanteProyectosService(
          datosPago,
          paymentIntent.id,
          numeroRecibo,
          opts,
        );
        console.log('acaaaaaaaaaaaaaaaaaasasas: ', transaccion)
      }else{
        transaccion = await this.crearTransaccionService.crearTransaccion(
          datosPago,
          paymentIntent.id,
          numeroRecibo,
          opts,
        );
      }
      

      console.log('termina transaccionnnnnnnnnnnnnnnnnnnnnn');
      const result = {
        stripe: paymentIntent,
        idTransaccion: transaccion._id,
      };

      return result;
    } catch (error) {
      throw error;
    }
  }

  async generarNuevoPagoStripe(
    nuevoPago: NuevosDatosPagoDto,
  ): Promise<Stripe.PaymentIntent> {
    // const customer = await this.customersService.createCustomer(cliente);
    // crear intento de pago
    try {
      // actualizar el pago con el mismo cliente (id del cliente)
      //const pago = await this.stripeService.retrievePaymentIntents(nuevoPago.idPago);

      const nuevoCliente: any = {
        name: nuevoPago.nombres,
        description: descripcionTransPagosGazelook.suscripcion,
        email: nuevoPago.email,
        phone: nuevoPago.telefono,
        address: {
          line1: nuevoPago.direccion,
        },
      };

      const customer = await this.customersService.createCustomer(nuevoCliente);

      const monto = Math.floor(nuevoPago.monto * 100);
      console.log('datosPago.monto', nuevoPago.monto * 100);

      const paymentIntent = await this.paymentIntentsService.createPaymentsIntent(
        {
          amount: monto,
          userEmail: nuevoPago.email,
          idCustomer: customer.id,
          description: descripcionTransPagosGazelook.suscripcion,
        },
      );

      return paymentIntent;
    } catch (error) {
      throw error;
    }
  }
}
