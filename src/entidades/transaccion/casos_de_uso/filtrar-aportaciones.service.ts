import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { ConvertirMontoMonedaService } from 'src/entidades/moneda/casos_de_uso/convertir-monto-moneda.service';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
import { Funcion } from 'src/shared/funcion';
import {
  catalogoOrigen,
  filtroBusquedaGastoOperacional,
  nombrecatalogoEstados,
  nombreEntidades,
  numeroDias,
} from './../../../shared/enum-sistema';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

@Injectable()
export class FiltrarAportacionesService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private convertirMontoMonedaService: ConvertirMontoMonedaService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private readonly funcion: Funcion
  ) { }

  filtroBus = filtroBusquedaGastoOperacional;
  numeroDia = numeroDias;

  async FiltrarAportaciones(
    filtro: string,
    campo: string,
    fecha: any,
  ): Promise<any> {
    // valor total de mis aportaciones en Gazelook. por rango de fecha
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      let transaccionesSuscripcion: any;
      let transaccionesValorExtra: any;
      let aportacionesSuscripcionUsd = 0;
      let aportacionesSuscripcionEur = 0;
      let aportacionesValorExtraUsd = 0;
      let aportacionesValorExtraEur = 0;

      let transaccionesSuscripcionPais: any;
      let transaccionesValorExtraPais: any;
      let aportacionesSuscripcionUsdPais = 0;
      let aportacionesSuscripcionEurPais = 0;
      let aportacionesValorExtraUsdPais = 0;
      let aportacionesValorExtraEurPais = 0;
      let transaccionesGastosOperativosPais = 0;

      let transaccionesGastosOperativos: any;
      let transaccionesMontoSobrante: any;
      let gastosOperativosUsd = 0;
      let gastosOperativosEur = 0;

      let montoSobranteUsd = 0;
      let montoSobranteEur = 0;

      let arrayAportaciones = [];
      let objAportacionUsd: any = {
        aportaciones: 0,
        moneda: {
          codNombre: '',
        },
      };
      let objAportacionEur: any = {
        aportaciones: 0,
        moneda: {
          codNombre: '',
        },
      };

      let objAportacionUsdPais: any = {
        aportaciones: 0,
        moneda: {
          codNombre: '',
        },
      };
      let objAportacionEurPais: any = {
        aportaciones: 0,
        moneda: {
          codNombre: '',
        },
      };

      const filtroSel = this.seleccionarFiltro(filtro);

      let inicio;
      let final;
      //SI SE ENVIA LA FECHA
      if (fecha) {
        console.log('fecha: ', fecha)
        const getFecha = new Date(fecha);
        //Obtiene el dia de la fecha enviada
        let dia = getFecha.getUTCDay();
        //SI el dia de la fecha enviada es 6 (sabado) el dia se establece en 0 (domingo)
        // if (dia === this.numeroDia.sabado) {
        //   dia = this.numeroDia.domingo;
        // } else {
        //   //Caso contrario se le suma 1 al dia obtenido
        //   dia = dia + 1;
        // }
        // console.log('dia: ', dia)
        //Obtiene el dia
        const getDia = this.funcion.seleccionarDia(dia);

        // console.log('FECHA ENVIADA: ', getFecha);
        // console.log('getDia: ', getDia)
        //Establece la fecha final para la busqueda
        const fechaF = getFecha.setUTCDate(getFecha.getUTCDate() - getDia);

        const fecFinal = new Date(fechaF);

        // console.log('fecFinal: ', fecFinal)

        let formatFinal =
          fecFinal.getUTCFullYear() +
          '-' +
          (fecFinal.getUTCMonth() + 1) +
          '-' +
          fecFinal.getUTCDate();

        //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es mayor o igual a 10
        if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() >= 10) {
          formatFinal =
            fecFinal.getUTCFullYear() +
            '-0' +
            (fecFinal.getUTCMonth() + 1) +
            '-' +
            fecFinal.getUTCDate();
        }

        //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es igual a 32
        if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() === 32) {
          formatFinal =
            fecFinal.getUTCFullYear() +
            '-0' +
            (fecFinal.getUTCMonth() + 2) +
            '-01';
        }

        //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es menor a 10
        if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() < 10) {
          formatFinal =
            fecFinal.getUTCFullYear() +
            '-0' +
            (fecFinal.getUTCMonth() + 1) +
            '-0' +
            fecFinal.getUTCDate();
        }

        //Si el mes de la fecha final es mayor a 10 y el dia de la fecha final es menor a 10
        if (fecFinal.getUTCMonth() + 1 >= 10 && fecFinal.getUTCDate() < 10) {
          formatFinal =
            fecFinal.getUTCFullYear() +
            '-' +
            (fecFinal.getUTCMonth() + 1) +
            '-0' +
            fecFinal.getUTCDate();
        }


        // console.log('formatFinal: ', formatFinal)
        formatFinal = formatFinal + 'T23:59:59.999Z';
        //Fecha final
        final = new Date(formatFinal);
        console.log('fechaFin: ', final);

        //Fecha inicial
        // const fechaI = getFecha.setDate(getFecha.getDate() - 6);
        // inicio = new Date(fechaI);
        // console.log('fechaInicial: ', inicio)
      }

      console.log('estado.codigo: ', estado.codigo)
      //Obtener aportes por suscripcion
      transaccionesSuscripcion = await this.transaccionModel
        .find({
          $and: [
            // { fechaActualizacion: { $gte: inicio } },
            { fechaCreacion: { $lte: final } },
            { estado: estado.codigo },
            { origen: catalogoOrigen.suscripciones },
          ],
        })
        .populate({
          path: 'conversionTransaccion',
        });

      //Obtener aportes por valor extra
      transaccionesValorExtra = await this.transaccionModel
        .find({
          $and: [
            // { fechaActualizacion: { $gte: inicio } },
            { fechaCreacion: { $lte: final } },
            { estado: estado.codigo },
            { origen: catalogoOrigen.valor_extra },
          ],
        })
        .populate({
          path: 'conversionTransaccion',
        });

      //Gastos operativos
      transaccionesGastosOperativos = await this.transaccionModel
        .find({
          $and: [
            // { fechaActualizacion: { $gte: inicio } },
            { fechaCreacion: { $lte: final } },
            { estado: estado.codigo },
            { destino: catalogoOrigen.gastos_operativos },
          ],
        })
        .populate({
          path: 'conversionTransaccion',
        });

      //Monto sobrante
      transaccionesMontoSobrante = await this.transaccionModel
        .find({
          $and: [
            // { fechaActualizacion: { $gte: inicio } },
            { fechaCreacion: { $lte: final } },
            { estado: estado.codigo },
            { destino: catalogoOrigen.monto_sobrante },
          ],
        })
        .populate({
          path: 'conversionTransaccion',
        });

      console.log('transaccionesSuscripcion.length: ', transaccionesSuscripcion.length)
      if (transaccionesSuscripcion.length > 0) {
        for (const transaccionSuscripcion of transaccionesSuscripcion) {

          //Si no se ha ejecutado en ningun balance
          if (transaccionSuscripcion.balance.length === 0) {
            if (transaccionSuscripcion.conversionTransaccion.length > 0) {
              for (const conversion of transaccionSuscripcion.conversionTransaccion) {
                if (conversion.totalRecibido) {
                  if (conversion.moneda === listaCodigosMonedas.USD) {
                    aportacionesSuscripcionUsd += Number(
                      conversion.totalRecibido,
                    );

                    // objAportacionUsd.aportaciones = aportacionesSuscripcionUsd;
                    objAportacionUsd.moneda.codNombre = conversion.moneda;
                  }
                  if (conversion.moneda === listaCodigosMonedas.EUR) {
                    aportacionesSuscripcionEur += Number(
                      conversion.totalRecibido,
                    );
                    // objAportacionEur.aportaciones = aportacionesSuscripcionEur;
                    objAportacionEur.moneda.codNombre = conversion.moneda;
                  }
                }
              }
            }
          }

        }
      }

      if (transaccionesValorExtra.length > 0) {
        for (const transaccionValorExtra of transaccionesValorExtra) {

          //Si no se ha ejecutado en ningun balance
          if (transaccionValorExtra.balance.length === 0) {
            if (transaccionValorExtra.conversionTransaccion.length > 0) {
              for (const conversion of transaccionValorExtra.conversionTransaccion) {
                if (conversion.totalRecibido) {
                  if (conversion.moneda === listaCodigosMonedas.USD) {
                    aportacionesValorExtraUsd += Number(conversion.totalRecibido);
                    // objAportacionUsd.aportacionesValorExtra = aportacionesValorExtraUsd;
                    objAportacionUsd.moneda.codNombre = conversion.moneda;
                  }
                  if (conversion.moneda === listaCodigosMonedas.EUR) {
                    aportacionesValorExtraEur += Number(conversion.totalRecibido);
                    // objAportacionEur.aportacionesValorExtra = aportacionesValorExtraEur;
                    objAportacionEur.moneda.codNombre = conversion.moneda;
                  }
                }
              }
            }
          }

        }
      }

      if (transaccionesGastosOperativos.length > 0) {
        for (const transacciongastosOperativos of transaccionesGastosOperativos) {

          //Si no se ha ejecutado en ningun balance
          if (transacciongastosOperativos.balance.length === 0) {
            if (transacciongastosOperativos.conversionTransaccion.length > 0) {
              for (const conversion of transacciongastosOperativos.conversionTransaccion) {
                if (conversion.totalRecibido) {
                  if (conversion.moneda === listaCodigosMonedas.USD) {
                    gastosOperativosUsd += Number(conversion.totalRecibido);
                  }
                  if (conversion.moneda === listaCodigosMonedas.EUR) {
                    gastosOperativosEur += Number(conversion.totalRecibido);
                  }
                }
              }
            }
          }

        }
      }

      if (transaccionesMontoSobrante.length > 0) {
        for (const transaccionMontoSobrante of transaccionesMontoSobrante) {

          //Si no se ha ejecutado en ningun balance
          if (transaccionMontoSobrante.balance.length === 0) {
            if (transaccionMontoSobrante.conversionTransaccion.length > 0) {
              for (const conversion of transaccionMontoSobrante.conversionTransaccion) {

                if (conversion.monto) {
                  if (conversion.moneda === listaCodigosMonedas.USD) {
                    montoSobranteUsd += Number(conversion.monto);
                    objAportacionUsd.moneda.codNombre = conversion.moneda;
                  }
                  if (conversion.moneda === listaCodigosMonedas.EUR) {
                    montoSobranteEur += Number(conversion.monto);
                    objAportacionEur.moneda.codNombre = conversion.moneda;
                  }
                }
              }
            }
          }

        }
      }

      //Filtro Mundial
      if (filtroSel === 1) {
        let totalUsd =
          (aportacionesSuscripcionUsd - gastosOperativosUsd) +
          aportacionesValorExtraUsd + montoSobranteUsd
          ;

        //redondeo
        totalUsd = Math.round(totalUsd * 100) / 100;
        let totalUsd1 = parseFloat(totalUsd.toString()).toFixed(2);

        let totalEur =
          (aportacionesSuscripcionEur - gastosOperativosEur) +
          aportacionesValorExtraEur + montoSobranteEur
          ;
        //redondeo
        totalEur = Math.round(totalEur * 100) / 100;
        let totalEur1 = parseFloat(totalEur.toString()).toFixed(2);

        objAportacionUsd.aportaciones = parseFloat(totalUsd1);
        objAportacionEur.aportaciones = parseFloat(totalEur1);

        if (objAportacionUsd.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionUsd);
        }
        if (objAportacionEur.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionEur);
        }
        return arrayAportaciones;
      }
      //Filtro de Pais
      if (filtroSel === 2) {
        let totalUsd = aportacionesSuscripcionUsd + aportacionesValorExtraUsd + montoSobranteUsd;
        let totalEur = aportacionesSuscripcionEur + aportacionesValorExtraEur + montoSobranteEur;

        let getUsuarios = await this.obtenerIdUsuarioService.obtenerUsuarioByPais(
          campo,
        );

        if (getUsuarios.length > 0) {
          for (const usuario of getUsuarios) {
            transaccionesSuscripcionPais = await this.transaccionModel
              .find({
                $and: [
                  { usuario: usuario._id },
                  { fechaCreacion: { $lte: final } },
                  { estado: estado.codigo },
                  { origen: catalogoOrigen.suscripciones },
                ],
              })
              .populate({
                path: 'conversionTransaccion',
              });

            transaccionesValorExtraPais = await this.transaccionModel
              .find({
                $and: [
                  { usuario: usuario._id },
                  { fechaCreacion: { $lte: final } },
                  { estado: estado.codigo },
                  { origen: catalogoOrigen.valor_extra },
                ],
              })
              .populate({
                path: 'conversionTransaccion',
              });

            if (transaccionesSuscripcionPais.length > 0) {
              for (const transaccionSuscripcion of transaccionesSuscripcionPais) {
                if (transaccionSuscripcion.balance.length === 0) {
                  if (transaccionSuscripcion.conversionTransaccion.length > 0) {
                    for (const conversion of transaccionSuscripcion.conversionTransaccion) {
                      if (conversion.totalRecibido) {
                        if (conversion.moneda === listaCodigosMonedas.USD) {
                          aportacionesSuscripcionUsdPais += Number(
                            conversion.totalRecibido,
                          );

                          // objAportacionUsd.aportacionesSuscripcion = aportacionesSuscripcionUsd;
                          objAportacionUsdPais.moneda.codNombre =
                            conversion.moneda;
                        }
                        if (conversion.moneda === listaCodigosMonedas.EUR) {
                          aportacionesSuscripcionEurPais += Number(
                            conversion.totalRecibido,
                          );
                          // objAportacionEur.aportacionesSuscripcion = aportacionesSuscripcionEur;
                          objAportacionEurPais.moneda.codNombre =
                            conversion.moneda;
                        }
                      }
                    }
                  }
                }

              }
            }

            if (transaccionesValorExtraPais.length > 0) {
              for (const transaccionValorExtra of transaccionesValorExtraPais) {
                //comprueba que no se haya ejecutado ningun balance con esta transaccion
                if (transaccionValorExtra.balance.length === 0) {
                  if (transaccionValorExtra.conversionTransaccion.length > 0) {
                    for (const conversion of transaccionValorExtra.conversionTransaccion) {
                      if (conversion.totalRecibido) {
                        if (conversion.moneda === listaCodigosMonedas.USD) {
                          aportacionesValorExtraUsdPais += Number(
                            conversion.totalRecibido,
                          );
                          // objAportacionUsd.aportacionesValorExtra = aportacionesValorExtraUsd;
                          objAportacionUsdPais.moneda.codNombre =
                            conversion.moneda;
                        }
                        if (conversion.moneda === listaCodigosMonedas.EUR) {
                          aportacionesValorExtraEurPais += Number(
                            conversion.totalRecibido,
                          );
                          // objAportacionEur.aportacionesValorExtra = aportacionesValorExtraEur;
                          objAportacionEurPais.moneda.codNombre =
                            conversion.moneda;
                        }
                      }
                    }
                  }
                }

              }
            }
          }

          let totalPaisUsd =
            aportacionesSuscripcionUsdPais + aportacionesValorExtraUsdPais;
          totalPaisUsd = totalPaisUsd * 100;
          //Porcentaje que tiene de gastos operativos ese pais
          totalPaisUsd = totalPaisUsd / totalUsd;
          totalPaisUsd = totalPaisUsd / 100;
          //Total monto de gastos operativos de ese pais
          let totalGastoOperativoUsd = totalPaisUsd * gastosOperativosUsd;
          let montoTotalPaisUsd =
            aportacionesSuscripcionUsdPais - totalGastoOperativoUsd +
            aportacionesValorExtraUsdPais
            ;

          //redondeo
          montoTotalPaisUsd = Math.round(montoTotalPaisUsd * 100) / 100;
          let montoTotalPaisUsd1 = parseFloat(
            montoTotalPaisUsd.toString(),
          ).toFixed(2);

          objAportacionUsdPais.aportaciones = parseFloat(montoTotalPaisUsd1);

          let totalPaisEur =
            aportacionesSuscripcionEurPais + aportacionesValorExtraEurPais;
          totalPaisEur = totalPaisEur * 100;
          //Porcentaje que tiene de gastos operativos ese pais
          totalPaisEur = totalPaisEur / totalEur;
          totalPaisEur = totalPaisEur / 100;
          //Total monto de gastos operativos de ese pais
          let totalGastoOperativoEur = totalPaisEur * gastosOperativosEur;
          let montoTotalPaisEur =
            aportacionesSuscripcionEurPais - totalGastoOperativoEur +
            aportacionesValorExtraEurPais
            ;
          //redondeo
          montoTotalPaisEur = Math.round(montoTotalPaisEur * 100) / 100;
          let montoTotalPaisEur1 = parseFloat(
            montoTotalPaisEur.toString(),
          ).toFixed(2);

          objAportacionEurPais.aportaciones = parseFloat(montoTotalPaisEur1);

          if (objAportacionUsdPais.moneda.codNombre != '') {
            arrayAportaciones.push(objAportacionUsdPais);
          }
          if (objAportacionEurPais.moneda.codNombre != '') {
            arrayAportaciones.push(objAportacionEurPais);
          }
          // return arrayAportaciones;
        }
        return arrayAportaciones;
      }
    } catch (error) {
      throw error;
    }
  }

  seleccionarFiltro(filtro) {
    switch (filtro) {
      case this.filtroBus.MUNDIAL:
        return 1;
      case this.filtroBus.PAIS:
        return 2;
    }
  }

}
