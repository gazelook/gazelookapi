import { Injectable } from '@nestjs/common';


@Injectable()
export class GestionPagoCoinpaymentsService {
  constructor(
  ) {}

  calcularValorTotalComisionCoinpayments(
    montoActualTransaccion: number,
    comisionPaymentez: number,
  ) {
    try {
      let valorComision = montoActualTransaccion * comisionPaymentez;
      let nuevoValor = montoActualTransaccion - valorComision;
      const resultData = {
        newFee: parseFloat(valorComision.toFixed(2)),
        newAmountReceived: parseFloat(nuevoValor.toFixed(2)),
      };
      return resultData;
    } catch (error) {
      throw error;
    }
  }
}
