import { CrearinformacionPagoService } from './crear-informacion-pago.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import {
  descripcionTransPagosGazelook,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { datosNuevaTransaccionDto } from '../entidad/actualizar-pago.dto';

@Injectable()
export class CrearNuevaTransaccionService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private actualizarUsuarioService: ActualizarUsuarioService,
  ) {}

  async nuevaTransaccion(
    datosNuevaTransaccion: datosNuevaTransaccionDto,
    opts: any,
  ): Promise<Transaccion> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.pendiente,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );

      // obtener ultima suscripcion usuario

      const transaccionDto = {
        estado: estado.codigo,
        monto: datosNuevaTransaccion.transaccion.monto,
        moneda: datosNuevaTransaccion.transaccion.moneda.codNombre,
        descripcion: descripcionTransPagosGazelook.suscripcionDeAlta,
        origen: datosNuevaTransaccion.transaccion.origen.codigo,
        balance: [],
        metodoPago: datosNuevaTransaccion.metodoPago,
        informacionPago: datosNuevaTransaccion.informacionPago,
        usuario: datosNuevaTransaccion.usuario,
      };

      const transaccion = await new this.transaccionModel(transaccionDto).save(
        opts,
      );

      // Agregar referencia usuario
      await this.actualizarUsuarioService.agregarUsuarioTransaccion(
        datosNuevaTransaccion.usuario,
        transaccion._id,
        opts,
      );

      //datos para guardar el historico
      const newHistorico: any = {
        datos: transaccion,
        usuario: datosNuevaTransaccion.usuario,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return transaccion;
    } catch (error) {
      throw error;
    }
  }
}
