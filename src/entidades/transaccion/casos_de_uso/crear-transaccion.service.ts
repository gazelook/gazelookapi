import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import * as moment from 'moment';
import * as mongoose from 'mongoose';

import { CrearinformacionPagoService } from './crear-informacion-pago.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import {
  catalogoOrigen,
  descripcionTransPagosGazelook,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { CrearSuscripcionService } from '../../suscripcion/casos_de_uso/crear-suscripcion.service';
import { ObtenerSuscripcionService } from '../../suscripcion/casos_de_uso/obtener-suscripcion.service';
import { ActualizarSuscripcionService } from '../../suscripcion/casos_de_uso/actualizar-suscripcion.service';
import { Suscripcion } from '../../../drivers/mongoose/interfaces/suscripcion/suscripcion.interface';
import { CrearConversionTransaccionService } from './crear-conversion-transaccion.service';
import { ConversionTransaccionDto } from '../entidad/conversion-transaccion.dto';

@Injectable()
export class CrearTransaccionService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private crearinformacionPagoService: CrearinformacionPagoService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private crearSuscripcionService: CrearSuscripcionService,
    private obtenerSuscripcionService: ObtenerSuscripcionService,
    private actualizarSuscripcionService: ActualizarSuscripcionService,
    private crearConversionTransaccionService: CrearConversionTransaccionService,
  ) {}

  async crearTransaccion(
    datosPago: any,
    idPago: any,
    numeroRecibo: string,
    opts: any,
  ): Promise<any> {
    // //Obtiene el codigo de la accion a realizarse
    // const accion = await this.obtenerNombreAccionService.obtenerNombreAccion('crear')
    // const getAccion = accion['codigo'];
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.pendiente,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );

      // guardar informacion de pago
      let datosInfoPago = {
        nombres: datosPago.nombres,
        telefono: datosPago.telefono,
        direcccion: datosPago.direccion,
        email: datosPago.email,
      };
      const infoPago = await this.crearinformacionPagoService.crearInformacionPago(
        datosInfoPago,
        idPago,
        opts,
      );
      let transaccion,
        suscripcion,
        refSuscripcion,
        cont = 0;
      // obtener ultima suscripcion usuario
      const getUltimaSuscripcion: Suscripcion = await this.obtenerSuscripcionService.diasTerminaSuscripcion(
        datosPago.idUsuario,
      );

      console.log(
        'datosPago.transacciones.length:',
        datosPago.transacciones.length,
      );
      for (const pagoCuenta of datosPago.transacciones) {
        const idTransaccion = new mongoose.mongo.ObjectId();

        console.log('idTransaccion: ', idTransaccion);
        // -------------------------------- Crear Conversion ---------------------------
        const dataConversion: ConversionTransaccionDto = {
          monedaUsuario: datosPago.tipoMonedaRegistro,
          monedaDefault: pagoCuenta.moneda.codNombre,
          montoDefault: pagoCuenta.monto,
          idTransaccion: idTransaccion.toHexString(),
        };
        const conversiones = await this.crearConversionTransaccionService.crearConversionTransaccion(
          dataConversion,
          opts,
        );

        if (!pagoCuenta.destino) {
          pagoCuenta.destino.codigo = pagoCuenta.origen.codigo;
        }
        
        const transaccionDto = {
          _id: idTransaccion,
          estado: estado.codigo,
          monto: pagoCuenta.monto,
          moneda: pagoCuenta.moneda.codNombre,
          descripcion:
            pagoCuenta.origen.codigo == catalogoOrigen.valor_extra
              ? descripcionTransPagosGazelook.valorExtra
              : descripcionTransPagosGazelook.suscripcion,
          origen: pagoCuenta.origen.codigo,
          destino: pagoCuenta.destino.codigo,
          balance: [],
          metodoPago: datosPago.metodoPago,
          informacionPago: infoPago._id,
          usuario: datosPago.idUsuario,
          conversionTransaccion: conversiones,
          numeroRecibo: numeroRecibo,
        };

        transaccion = await new this.transaccionModel(transaccionDto).save(
          opts,
        );

        // origen suscripcion: crea una suscripcion por cada transaccion
        if (transaccionDto.origen === catalogoOrigen.suscripciones) {
          if (datosPago.creacionCuenta) {
            // ________________cuando se crea la cuenta__________________
            suscripcion = await this.crearSuscripcionService.crearSuscripcion(
              transaccion._id,
              datosPago.idUsuario,
              datosPago.tipoSuscripcion,
              opts,
            );
          } else if (datosPago.renovacionCuenta) {
            // _______________________renovacion de la suscripcion______________________

            // verificar si el usuario aun esta activo, por medio de la suscripcion
            const formatFechaActual = moment().format('YYYY-MM-DD');
            const fechaActualValidar = new Date(formatFechaActual);
            if (
              moment(fechaActualValidar) >
              moment(getUltimaSuscripcion.fechaFinalizacion)
            ) {
              // CASO 1: activar usuario y generar nueva suscripcion
              if (cont === 0) {
                suscripcion = await this.crearSuscripcionService.crearSuscripcion(
                  transaccion._id,
                  datosPago.idUsuario,
                  datosPago.tipoSuscripcion,
                  opts,
                );
                refSuscripcion = suscripcion;
                // actualizar estado usario activo
                //await this.actualizarUsuarioService.actualizarEstadoUsuarioValidacionCuenta(datosPago.idUsuario, estadosUsuario.activa, opts);
              } else if (cont > 0) {
                suscripcion = await this.crearSuscripcionService.crearNuevaSuscripcion(
                  transaccion._id,
                  datosPago.idUsuario,
                  datosPago.tipoSuscripcion,
                  refSuscripcion.fechaFinalizacion,
                  opts,
                );
                await this.actualizarSuscripcionService.addReferenciaSuscripcion(
                  refSuscripcion._id,
                  suscripcion._id,
                  opts,
                );

                refSuscripcion = suscripcion;
              }
            } else {
              // CASO 2: si suscripcion activa
              if (cont === 0) {
                refSuscripcion = getUltimaSuscripcion;
                suscripcion = await this.crearSuscripcionService.crearNuevaSuscripcion(
                  transaccion._id,
                  datosPago.idUsuario,
                  datosPago.tipoSuscripcion,
                  refSuscripcion.fechaFinalizacion,
                  opts,
                );
                await this.actualizarSuscripcionService.addReferenciaSuscripcion(
                  refSuscripcion._id,
                  suscripcion._id,
                  opts,
                );
                refSuscripcion = suscripcion;
              } else if (cont > 0) {
                suscripcion = await this.crearSuscripcionService.crearNuevaSuscripcion(
                  transaccion._id,
                  datosPago.idUsuario,
                  datosPago.tipoSuscripcion,
                  refSuscripcion.fechaFinalizacion,
                  opts,
                );
                await this.actualizarSuscripcionService.addReferenciaSuscripcion(
                  refSuscripcion._id,
                  suscripcion._id,
                  opts,
                );

                refSuscripcion = suscripcion;
              }
            }

            cont++;
          }
          // agregar referencia usuario suscripcion
          await this.actualizarUsuarioService.agregarUsuarioSuscripcion(
            datosPago.idUsuario,
            suscripcion._id,
            opts,
          );
        }

        // Agregar referencia usuario
        await this.actualizarUsuarioService.agregarUsuarioTransaccion(
          datosPago.idUsuario,
          transaccion._id,
          opts,
        );

        //datos para guardar el historico
        const newHistorico: any = {
          datos: transaccion,
          usuario: datosPago.idUsuario,
          accion: accionCrear.codigo,
          entidad: entidad.codigo,
        };

        //Crear historico para transacción
        this.crearHistoricoService.crearHistoricoServer(newHistorico);
      }
      if (datosPago.pagoCripto) {
        const idTransaccion = new mongoose.mongo.ObjectId();

        console.log('idTransaccion: ', idTransaccion);

        
        const transaccionDto = {
          _id: idTransaccion,
          estado: estado.codigo,
          monto: datosPago.pagoCripto.cantidad,
          moneda: datosPago.pagoCripto.simboloCripto,
          descripcion: descripcionTransPagosGazelook.montoCripto,
          origen: null,
          destino: null,
          balance: [],
          metodoPago: datosPago.metodoPago,
          informacionPago: infoPago._id,
          usuario: datosPago.idUsuario,
          conversionTransaccion: [],
          numeroRecibo: numeroRecibo,
        };

        transaccion = await new this.transaccionModel(transaccionDto).save(
          opts,
        );

        // Agregar referencia usuario
        await this.actualizarUsuarioService.agregarUsuarioTransaccion(
          datosPago.idUsuario,
          transaccion._id,
          opts,
        );

        //datos para guardar el historico
        const newHistorico: any = {
          datos: transaccion,
          usuario: datosPago.idUsuario,
          accion: accionCrear.codigo,
          entidad: entidad.codigo,
        };

        //Crear historico para transacción
        this.crearHistoricoService.crearHistoricoServer(newHistorico);
      }

      console.log('finnnnnnnnnnn transaccion');
      return transaccion;
    } catch (error) {
      throw error;
    }
  }

  generarNumeroRecibo() {
    try {
      const numGenerate = `${Math.floor(new Date().valueOf() * Math.random())}`;

      return numGenerate;
    } catch (error) {
      throw error;
    }
  }
}
