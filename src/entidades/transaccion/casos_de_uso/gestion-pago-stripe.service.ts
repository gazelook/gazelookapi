import { Injectable } from '@nestjs/common';

import { nombreEstadosPagos } from 'src/shared/enum-sistema';
import { PaymentIntentsService } from '../../../drivers/stripe/services/payment-intents.service';
import { PaymentMethodsService } from '../../../drivers/stripe/services/payment-methods.service';

@Injectable()
export class GestionPagoStripeService {
  constructor(
    private paymentIntentsService: PaymentIntentsService,
    private paymentMethodsService: PaymentMethodsService,
  ) {}

  async obtenerEstadoPagoStripe(idPago): Promise<Boolean> {
    try {
      const paymentIntents = await this.paymentIntentsService.retrievePaymentIntents(
        idPago,
      );
      if (paymentIntents.status === nombreEstadosPagos.succeeded) {
        return true;
      }
      return false;
    } catch (error) {
      throw error;
    }
  }

  async obtenerDetallePagoStripe(idPago) {
    try {
      const paymentIntents = await this.paymentIntentsService.retrievePaymentIntents(
        idPago,
      );
      const payBalance = await this.paymentIntentsService.getStripeBalanceTransaction(
        paymentIntents.charges.data[0].balance_transaction.toString(),
      );
      const payMethod = await this.paymentMethodsService.getPaymentMethod(
        paymentIntents.payment_method.toString(),
      );
      const result = {
        country: payMethod.card.country,
        fee: payBalance.fee,
        net: payBalance.net,
        amount: payBalance.amount,
      };
      return result;
    } catch (error) {
      throw error;
    }
  }

  calcularValorTotalComisionStripe(
    montoTotalStripe: number,
    montoActualTransaccion: number,
    comisionStripe: number,
  ) {
    try {
      let newFee = (montoActualTransaccion * comisionStripe) / montoTotalStripe;
      let nuevoValor = montoActualTransaccion - newFee;
      const resultData = {
        newFee: parseFloat(newFee.toFixed(2)),
        newAmountReceived: parseFloat(nuevoValor.toFixed(2)),
      };
      return resultData;
    } catch (error) {
      throw error;
    }
  }
}
