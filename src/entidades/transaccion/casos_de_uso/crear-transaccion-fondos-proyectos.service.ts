import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  nombreAcciones, nombreEntidades
} from 'src/shared/enum-sistema';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearinformacionPagoService } from './crear-informacion-pago.service';


@Injectable()
export class CrearTransaccionFondosProyectoService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private crearinformacionPagoService: CrearinformacionPagoService,
  ) {}

  async crearTransaccionFondosAsignadosProyectos(
    transaccionDto: any,
    opts: any,
  ): Promise<any> {
    // //Obtiene el codigo de la accion a realizarse

    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );

      let transaccion;
      transaccion = await new this.transaccionModel(transaccionDto).save(opts);

      //datos para guardar el historico
      const newHistorico: any = {
        datos: transaccion,
        usuario: '',
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);
      return transaccion;
    } catch (error) {
      throw error;
    }
  }
}
