import { HttpStatus, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { erroresCuenta } from '../../../shared/enum-errores';
import {
  codigosMetodosPago
} from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearEmailReciboPagoService } from '../../emails/casos_de_uso/crear-email-recibo-pago.service';
import { ActualizarConversionTransaccionService } from '../../transaccion/casos_de_uso/actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from '../../transaccion/casos_de_uso/actualizar-transaccion.service';
import { GestionPagoStripeService } from '../../transaccion/casos_de_uso/gestion-pago-stripe.service';
import { ObtenerTransaccionService } from '../../transaccion/casos_de_uso/obtener-transaccion.service';
import { UpdateTransaccionPagoDto } from '../../transaccion/entidad/transaccion.dto';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';

@Injectable()
export class ValidarPagoStripeService {

  codigoMetodoPagoStripe = codigosMetodosPago.stripe;

  constructor(
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private gestionPagoStripeService: GestionPagoStripeService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private actualizarConversionTransaccionService: ActualizarConversionTransaccionService,
    private crearEmailReciboPagoService: CrearEmailReciboPagoService,
  ) { }

  async validarCuenta(idTransaccion, idioma): Promise<any> {

    const session = await mongoose.startSession();

    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };

    let transaccionDatabase;
    const opts = { session };

    try {
      
      const transaccionQuery: any = await this.obtenerTransaccionService.obtenerTransaccionById(
        idTransaccion,
      );
      if (!transaccionQuery) {
        throw {
          codigo: HttpStatus.EXPECTATION_FAILED,
          codigoNombre: erroresCuenta.TRANSACCION_NO_VALIDA,
        };
      }

      const informacionPago = transaccionQuery.informacionPago;
      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithID(
        transaccionQuery.usuario._id,
      );

      
      // ___________________________________________ Verificar Pago OK____________________________________________
      // pago Stripe
      let verificarCuentaStripe;
      if (transaccionQuery.metodoPago === this.codigoMetodoPagoStripe) {
        console.log('informacionPago.idPago', informacionPago.idPago)
        verificarCuentaStripe = await this.gestionPagoStripeService.obtenerEstadoPagoStripe(
          informacionPago.idPago,
        );
        console.log('verificarCuentaStripe--', verificarCuentaStripe)
        if (!verificarCuentaStripe) {
          throw {
            codigo: HttpStatus.EXPECTATION_FAILED,
            codigoNombre: erroresCuenta.ERROR_REGISTRO_PAGO,
          };
        }

        console.log('__________________----------......-')
        if (verificarCuentaStripe) {
          const detallePagoStripe = await this.gestionPagoStripeService.obtenerDetallePagoStripe(
            informacionPago.idPago,
          );
          detallePagoStripe.amount = detallePagoStripe.amount / 100;
          detallePagoStripe.fee = detallePagoStripe.fee / 100;
          detallePagoStripe.net = detallePagoStripe.net / 100;


          // ______________________________actualizar transaccion (dos origenes)________________________________
          //__________________Calcular valor total por transaccion________________
          const getNewValor = this.gestionPagoStripeService.calcularValorTotalComisionStripe(
            detallePagoStripe.amount,
            transaccionQuery.monto,
            detallePagoStripe.fee,
          );

          const dataUpdate: UpdateTransaccionPagoDto = {
            comisionTransferencia: getNewValor.newFee,
            totalRecibido: getNewValor.newAmountReceived,
            origenPais: detallePagoStripe.country,
          };

          if (
            transaccionQuery.informacionPago.idPago ===
            transaccionQuery.informacionPago.idPago
          ) {
            transaccionDatabase = await session.withTransaction(async () => {
              const updatetransaccion = await this.actualizarTransaccionService.actualizarTransaccion(
                transaccionQuery._id,
                opts,
                dataUpdate,
              );
              if (!updatetransaccion) {
                await session.abortTransaction();
                return;
              }
            }, transactionOptions);
          }

          //__________________Actualizar conversion transaccion________________
          transaccionDatabase = await session.withTransaction(async () => {
            const updateConversion = await this.actualizarConversionTransaccionService.actualizarConversionStripe(
              transaccionQuery._id,
              dataUpdate,
              opts,
            );
            if (!updateConversion) {
              await session.abortTransaction();
              return;
            }
          }, transactionOptions);



          const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(
            getUsuario.idioma,
          );

          //_______________________________enviar recibo pago___________________________________
          const dataEmailPago = {
            idInformacionPago: informacionPago._id as string,
            usuario: getUsuario._id as string,
            nombre: getUsuario.perfiles[0].nombre as string,
            emailDestinatario: getUsuario.email as string,
            idioma: idiomaUsuario.codNombre as string,
          };
          await this.crearEmailReciboPagoService.crearEmailReciboPago(
            dataEmailPago,
          );

          // proceso exitoso
          return true;
        }
        
      }
      return false;
    } catch (error) {
      throw error;
    }
  }

  validarCodigosMetodosPago(codigo): Boolean {
    const codigos = [
      codigosMetodosPago.stripe,
      codigosMetodosPago.payments1,
      codigosMetodosPago.payments2,
      codigosMetodosPago.panama,
      codigosMetodosPago.cripto,
    ];
    if (codigos.indexOf(codigo) !== -1) {
      return true;
    } else {
      return false;
    }
  }
}
