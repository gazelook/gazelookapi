import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { ConvertirMontoMonedaService } from 'src/entidades/moneda/casos_de_uso/convertir-monto-moneda.service';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
import {
  catalogoOrigen,
  filtroBusquedaAportaciones,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from './../../../shared/enum-sistema';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

@Injectable()
export class ObtenerAportacionesService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private convertirMontoMonedaService: ConvertirMontoMonedaService,
  ) { }

  filtroBus = filtroBusquedaAportaciones;

  async ObtenerAportaciones(
    usuario: string,
    anio: any,
    // mes: any,
  ): Promise<any> {
    // valor total de mis aportaciones en Gazelook. por rango de fecha
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.ver,
      );

      let transaccionesSuscripcion: any;
      let transaccionesValorExtra: any;
      let transaccionesDonacionProyectos: any;

      let inicio;
      let final;
      // if (mes) {
      //   let fechaInicial = anio + '-' + mes + '-01';
      //   let fechaFinal = anio + '-' + mes + '-31';

      //   inicio = new Date(fechaInicial);
      //   fechaFinal = fechaFinal + 'T23:59:59.999Z';
      //   final = new Date(fechaFinal);
      // } else {
      let fechaInicial = anio + '-01-01';
      let fechaFinal = anio + '-12-31';

      inicio = new Date(fechaInicial);
      fechaFinal = fechaFinal + 'T23:59:59.999Z';
      final = new Date(fechaFinal);
      // }

      console.log('FECHAINI: ', inicio);
      console.log('FECHAFIN: ', final);
      //obtener aportes por suscripcion
      // if (filtroSel === 1) {
      console.log(estado.codigo);
      transaccionesSuscripcion = await this.transaccionModel
        .find({
          usuario: usuario,
          $and: [
            { fechaActualizacion: { $gte: inicio } },
            { fechaActualizacion: { $lte: final } },
            { estado: estado.codigo },
            { origen: catalogoOrigen.suscripciones },
          ],
        })
        .populate({
          path: 'conversionTransaccion',
        });
      // }

      //obtener aportes por valor extra

      transaccionesValorExtra = await this.transaccionModel
        .find({
          usuario: usuario,
          $and: [
            { fechaActualizacion: { $gte: inicio } },
            { fechaActualizacion: { $lte: final } },
            { estado: estado.codigo },
            { origen: catalogoOrigen.valor_extra },
          ],
        })
        .populate({
          path: 'conversionTransaccion',
        });

      //obtener aportes por donacion a proyectos

      transaccionesDonacionProyectos = await this.transaccionModel
        .find({
          usuario: usuario,
          $and: [
            { fechaActualizacion: { $gte: inicio } },
            { fechaActualizacion: { $lte: final } },
            { estado: estado.codigo },
            { destino: catalogoOrigen.donacion },
          ],
        })
        .populate({
          path: 'conversionTransaccion',
        });

      let aportacionesSuscripcionUsd = 0;
      let aportacionesSuscripcionEur = 0;

      let aportacionesValorExtraUsd = 0;
      let aportacionesValorExtraEur = 0;

      let aportacionesDonacionProyectosUsd = 0;
      let aportacionesDonacionProyectosEur = 0;

      let arrayAportaciones = [];
      let objAportacionUsd: any = {
        aportacionesSuscripcion: 0,
        aportacionesValorExtra: [],
        aportacionesDonacionProyectos: [],
        moneda: {
          codNombre: '',
        },
      };
      let objAportacionEur: any = {
        aportacionesSuscripcion: 0,
        aportacionesValorExtra: [],
        aportacionesDonacionProyectos: [],
        moneda: {
          codNombre: '',
        },
      };

      if (transaccionesSuscripcion.length > 0) {
        for (const transaccionSuscripcion of transaccionesSuscripcion) {
          if (transaccionSuscripcion.conversionTransaccion.length > 0) {
            for (const conversion of transaccionSuscripcion.conversionTransaccion) {
              if (conversion.totalRecibido) {
                if (conversion.moneda === listaCodigosMonedas.USD) {
                  aportacionesSuscripcionUsd += Number(
                    conversion.totalRecibido,
                  );

                  objAportacionUsd.aportacionesSuscripcion = aportacionesSuscripcionUsd;
                  objAportacionUsd.moneda.codNombre = conversion.moneda;
                }
                if (conversion.moneda === listaCodigosMonedas.EUR) {
                  aportacionesSuscripcionEur += Number(
                    conversion.totalRecibido,
                  );
                  objAportacionEur.aportacionesSuscripcion = aportacionesSuscripcionEur;
                  objAportacionEur.moneda.codNombre = conversion.moneda;
                }
              }
            }
          }
        }
      }

      if (transaccionesValorExtra.length > 0) {
        let arrayValoresExtraUsd = []
        let arrayValoresExtraEur = []
        for (const transaccionValorExtra of transaccionesValorExtra) {
          if (transaccionValorExtra.conversionTransaccion.length > 0) {
            for (const conversion of transaccionValorExtra.conversionTransaccion) {
              if (conversion.totalRecibido) {
                if (conversion.moneda === listaCodigosMonedas.USD) {
                  aportacionesValorExtraUsd = Number(conversion.totalRecibido);
                  let objValoresExtraUsd = {
                    monto: parseFloat(
                      aportacionesValorExtraUsd.toFixed(2)),
                    fechaCreacion: conversion.fechaCreacion
                  }
                  arrayValoresExtraUsd.push(objValoresExtraUsd)
                  objAportacionUsd.aportacionesValorExtra = arrayValoresExtraUsd;
                  objAportacionUsd.moneda.codNombre = conversion.moneda;
                }
                if (conversion.moneda === listaCodigosMonedas.EUR) {
                  aportacionesValorExtraEur = Number(conversion.totalRecibido);
                  let objValoresExtraEur = {
                    monto: parseFloat(
                      aportacionesValorExtraEur.toFixed(2)),
                    fechaCreacion: conversion.fechaCreacion
                  }
                  arrayValoresExtraEur.push(objValoresExtraEur)
                  objAportacionEur.aportacionesValorExtra = arrayValoresExtraEur;
                  objAportacionEur.moneda.codNombre = conversion.moneda;
                }
              }
            }
          }
        }
      }


      if (transaccionesDonacionProyectos.length > 0) {
        let arrayDonacionProyectosUsd = []
        let arrayDonacionProyectosEur = []
        for (const transaccionDonacionProyectos of transaccionesDonacionProyectos) {
          if (transaccionDonacionProyectos.conversionTransaccion.length > 0) {
            for (const conversion of transaccionDonacionProyectos.conversionTransaccion) {
              if (conversion.totalRecibido) {
                if (conversion.moneda === listaCodigosMonedas.USD) {
                  aportacionesDonacionProyectosUsd = Number(conversion.totalRecibido);
                  let objDonacionProyectosUsd = {
                    monto: parseFloat(
                      aportacionesDonacionProyectosUsd.toFixed(2)),
                    fechaCreacion: conversion.fechaCreacion
                  }
                  arrayDonacionProyectosUsd.push(objDonacionProyectosUsd)
                  objAportacionUsd.aportacionesDonacionProyectos = arrayDonacionProyectosUsd;
                  objAportacionUsd.moneda.codNombre = conversion.moneda;
                }
                if (conversion.moneda === listaCodigosMonedas.EUR) {
                  aportacionesDonacionProyectosEur = Number(conversion.totalRecibido);
                  let objDonacionProyectosEur = {
                    monto: parseFloat(
                      aportacionesDonacionProyectosEur.toFixed(2)),
                    fechaCreacion: conversion.fechaCreacion
                  }
                  arrayDonacionProyectosEur.push(objDonacionProyectosEur)
                  objAportacionEur.aportacionesDonacionProyectos = arrayDonacionProyectosEur;
                  objAportacionEur.moneda.codNombre = conversion.moneda;
                }
              }
            }
          }
        }
      }

      console.log('legueeeeeeeeeee aquiiiiiiiiii')
      if (objAportacionUsd.moneda.codNombre != '') {
        objAportacionUsd.aportacionesSuscripcion =
          objAportacionUsd.aportacionesSuscripcion * 0.8;
        objAportacionUsd.aportacionesSuscripcion = parseFloat(
          objAportacionUsd.aportacionesSuscripcion.toFixed(2),
        );
        // objAportacionUsd.aportacionesValorExtra = parseFloat(
        //   objAportacionUsd.aportacionesValorExtra.toFixed(2),
        // );
        arrayAportaciones.push(objAportacionUsd);
      }
      if (objAportacionEur.moneda.codNombre != '') {
        objAportacionEur.aportacionesSuscripcion =
          objAportacionEur.aportacionesSuscripcion * 0.8;
        objAportacionEur.aportacionesSuscripcion = parseFloat(
          objAportacionEur.aportacionesSuscripcion.toFixed(2),
        );
        // objAportacionEur.aportacionesValorExtra = parseFloat(
        //   objAportacionEur.aportacionesValorExtra.toFixed(2),
        // );
        arrayAportaciones.push(objAportacionEur);
      }

      //Obtiene el perfil del usuario
      // const perfil = await this.obtenerPerfilUsuarioService.obtenerPerfilesUsuario(
      //   usuario,
      // );

      return arrayAportaciones;
    } catch (error) {
      throw error;
    }
  }

  seleccionarFiltro(filtro) {
    switch (filtro) {
      case this.filtroBus.suscripcion:
        return 1;
      case this.filtroBus.valorExtra:
        return 2;
      default:
        return 1;
    }
  }
}
