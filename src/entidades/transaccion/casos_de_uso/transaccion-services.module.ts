import { forwardRef, Module } from '@nestjs/common';
import { CoinpaymentezModule } from 'src/drivers/coinpaymentez/coinpaymentez.module';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { StripeRefundsService } from 'src/drivers/stripe/services/stripe-refunds.service';
import { EmailServicesModule } from 'src/entidades/emails/casos_de_uso/email-services.module';
import { ConvertirMontoMonedaService } from 'src/entidades/moneda/casos_de_uso/convertir-monto-moneda.service';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { ProyectosServicesModule } from 'src/entidades/proyectos/casos_de_uso/proyectos-services.module';
import { Funcion } from 'src/shared/funcion';
import { StripeModule } from '../../../drivers/stripe/stripe.module';
import { MonedaServicesModule } from '../../moneda/casos_de_uso/moneda.services.module';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { transaccionProviders } from '../drivers/transaccion.provider';
import { CatalogosServiceModule } from './../../catalogos/casos_de_uso/catalogos-services.module';
import { SuscripcionServicesModule } from './../../suscripcion/casos_de_uso/suscripcion-services.module';
import { ActualizarConversionTransaccionService } from './actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from './actualizar-transaccion.service';
import { ObtenerDiaService } from './contador-metodo.service';
import { CrearConversionTransaccionService } from './crear-conversion-transaccion.service';
import { CrearinformacionPagoService } from './crear-informacion-pago.service';
import { CrearNuevaTransaccionService } from './crear-nueva-transaccion.service';
import { CrearPagoCoinpaymentezService } from './crear-pago-coinpaymentez.service';
import { CrearPagoPaymentezService } from './crear-pago-paymentez.service';
import { CrearPagoPaypalService } from './crear-pago-paypal.service';
import { CrearPagoStripeService } from './crear-pago-stripe.service';
import { CreaRefundPaymentezService } from './crear-refund-paymentez.service';
import { CreaRefundStripeService } from './crear-refund-stripe.service';
import { CrearTransaccionFondosProyectoService } from './crear-transaccion-fondos-proyectos.service';
import { CrearTransaccionMontoFaltanteProyectoService } from './crear-transaccion-monto-faltante-proyectos.service';
import { CrearTransaccionService } from './crear-transaccion.service';
import { DonacionProyectoService } from './donacion-proyecto.service';
import { EliminarTransaccionCompletoService } from './eliminar-transaccion-completo.service';
import { FiltrarAportacionesService } from './filtrar-aportaciones.service';
import { GenerarNuevoPagoService } from './generar-nuevo-pago.service';
import { GestionPagoCoinpaymentsService } from './gestion-pago-coinpayments.service';
import { GestionPagoPaymentezService } from './gestion-pago-paymentez.service';
import { GestionPagoPaypalService } from './gestion-pago-paypal.service';
import { GestionPagoStripeService } from './gestion-pago-stripe.service';
import { ObtenerAportacionesService } from './obtener-aportaciones.service';
import { ObtenerBeneficiarioTransaccionService } from './obtener-beneficiario-transaccion.service';
import { ObtenerRatesCoinpaymentezService } from './obtener-rates-coinpaymentez.service';
import { ObtenerTransaccionService } from './obtener-transaccion.service';
import { ValidarPagoCriptoService } from './validar-pago-cripto.service';
import { ValidarPagoStripeService } from './validar-pago-stripe.service';



@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    SuscripcionServicesModule,
    forwardRef(() => UsuarioServicesModule),
    PerfilServiceModule,
    MonedaServicesModule,
    StripeModule,
    CoinpaymentezModule,
    forwardRef(() => EmailServicesModule),
    ProyectosServicesModule
  ],
  providers: [
    ...transaccionProviders,
    CrearTransaccionService,
    CrearPagoStripeService,
    CrearPagoPaypalService,
    CrearinformacionPagoService,
    ObtenerTransaccionService,
    ActualizarTransaccionService,
    GestionPagoStripeService,
    GestionPagoPaypalService,
    GenerarNuevoPagoService,
    ObtenerAportacionesService,
    EliminarTransaccionCompletoService,
    ConvertirMontoMonedaService,
    CrearTransaccionFondosProyectoService,
    FiltrarAportacionesService,
    ObtenerBeneficiarioTransaccionService,
    CrearNuevaTransaccionService,
    CrearConversionTransaccionService,
    ActualizarConversionTransaccionService,
    CreaRefundStripeService,
    StripeRefundsService,
    CrearPagoPaymentezService,
    GestionPagoPaymentezService,
    CreaRefundPaymentezService,
    ObtenerRatesCoinpaymentezService,
    CrearPagoCoinpaymentezService,
    GestionPagoCoinpaymentsService,
    Funcion,
    CrearTransaccionMontoFaltanteProyectoService,
    ObtenerDiaService,
    DonacionProyectoService,
    ValidarPagoStripeService,
    ValidarPagoCriptoService
  ],
  exports: [
    CrearTransaccionService,
    CrearPagoStripeService,
    CrearPagoPaypalService,
    CrearinformacionPagoService,
    ObtenerTransaccionService,
    ActualizarTransaccionService,
    GestionPagoStripeService,
    GestionPagoPaypalService,
    GenerarNuevoPagoService,
    ObtenerAportacionesService,
    EliminarTransaccionCompletoService,
    ConvertirMontoMonedaService,
    CrearTransaccionFondosProyectoService,
    FiltrarAportacionesService,
    ObtenerBeneficiarioTransaccionService,
    CrearNuevaTransaccionService,
    CrearConversionTransaccionService,
    ActualizarConversionTransaccionService,
    CreaRefundStripeService,
    StripeRefundsService,
    CrearPagoPaymentezService,
    GestionPagoPaymentezService,
    CreaRefundPaymentezService,
    ObtenerRatesCoinpaymentezService,
    CrearPagoCoinpaymentezService,
    GestionPagoCoinpaymentsService,
    CrearTransaccionMontoFaltanteProyectoService,
    ObtenerDiaService,
    DonacionProyectoService,
    ValidarPagoStripeService,
    ValidarPagoCriptoService
  ],
  controllers: [],
})
export class TransaccionServicesModule { }
