import { CrearTransaccionService } from './crear-transaccion.service';
import { Injectable } from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { pagoPaypal } from 'src/shared/enum-sistema';
import { ConfigService } from '../../../config/config.service';

const paypal = require('@paypal/checkout-server-sdk');

@Injectable()
export class CrearPagoPaypalService {
  private PAYPAL_CLIEN_ID: string;
  private PAYPAL_CLIENT_SECRET: string;

  constructor(
    private crearTransaccionService: CrearTransaccionService,
    private config: ConfigService,
  ) {
    this.PAYPAL_CLIEN_ID = config.get<string>('PAYPAL_CLIEN_ID');
    this.PAYPAL_CLIENT_SECRET = config.get<string>('PAYPAL_CLIENT_SECRET');
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async crearPagoPaypal(datosPago: any, opts?: any): Promise<any> {
    try {
      console.log('datosPagoPaypal', datosPago);

      const monto = Number.parseFloat(datosPago.monto.toFixed(2));

      // Guardar cliente en paypal
      // This sample uses SandboxEnvironment. In production, use LiveEnvironment
      const environment = new paypal.core.SandboxEnvironment(
        this.PAYPAL_CLIEN_ID,
        this.PAYPAL_CLIENT_SECRET,
      );
      const client = new paypal.core.PayPalHttpClient(environment);

      // Construct a request object and set desired parameters
      // Here, OrdersCreateRequest() creates a POST request to /v2/checkout/orders
      const orderRequest = new paypal.orders.OrdersCreateRequest();

      orderRequest.prefer('return=representation');
      orderRequest.requestBody({
        intent: pagoPaypal.intent,
        purchase_units: [
          {
            amount: {
              currency_code: pagoPaypal.currency_code,
              value: monto, //pagoPaypal.value//
            },
          },
        ],
        application_context: {
          brand_name: pagoPaypal.brand_name,
          landing_page: 'NO_PREFERENCE', // por defecto
          user_action: 'PAY_NOW', // Accion para que en paypal muestre el monto del pago
          return_url: pagoPaypal.return_url, // url despues que el pago se ha realizado con exito
          cancel_url: pagoPaypal.cancel_url, // url despues de cancelar el pago
        },
      });

      const orderResponse = await client.execute(orderRequest);
      // If call returns body in response, you can get the deserialized version from the result attribute of the response.

      // // guardar transacción y suscripcon con estado pendiente
      const numeroRecibo = this.crearTransaccionService.generarNumeroRecibo();
      const transaccion: any = await this.crearTransaccionService.crearTransaccion(
        datosPago,
        orderResponse.result.id,
        numeroRecibo,
        opts,
      );

      const result = {
        idPaypal: orderResponse.result.id,
        idTransaccion: transaccion._id,
      };
      return result;
    } catch (error) {
      throw error;
    }
  }

  async generarNuevoPagoPaypal(nuevoMonto: number): Promise<any> {
    try {
      // Guardar cliente en paypal
      // This sample uses SandboxEnvironment. In production, use LiveEnvironment
      const environment = new paypal.core.SandboxEnvironment(
        this.PAYPAL_CLIEN_ID,
        this.PAYPAL_CLIENT_SECRET,
      );
      const client = new paypal.core.PayPalHttpClient(environment);
      const respuesta = new RespuestaInterface();

      // Construct a request object and set desired parameters
      // Here, OrdersCreateRequest() creates a POST request to /v2/checkout/orders
      const orderRequest = new paypal.orders.OrdersCreateRequest();

      nuevoMonto = Number.parseFloat(nuevoMonto.toFixed(2));

      orderRequest.prefer('return=representation');
      orderRequest.requestBody({
        intent: pagoPaypal.intent,
        purchase_units: [
          {
            amount: {
              currency_code: pagoPaypal.currency_code,
              value: nuevoMonto,
            },
          },
        ],
        application_context: {
          brand_name: pagoPaypal.brand_name,
          landing_page: 'NO_PREFERENCE', // por defecto
          user_action: 'PAY_NOW', // Accion para que en paypal muestre el monto del pago
          return_url: pagoPaypal.return_url, // url despues que el pago se ha realizado con exito
          cancel_url: pagoPaypal.cancel_url, // url despues de cancelar el pago
        },
      });

      const orderResponse = await client.execute(orderRequest);
      const result = {
        idPaypal: orderResponse.result.id,
      };
      return result;
    } catch (error) {
      throw error;
    }
  }
}
