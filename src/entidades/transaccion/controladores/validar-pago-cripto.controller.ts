import {
  Body, Controller, Headers, HttpStatus, Post, UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody, ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ValidarPagoCriptoService } from '../casos_de_uso/validar-pago-cripto.service';
import { ValidarPagoDto } from '../entidad/transaccion.dto';

@ApiTags('Proyectos')
@Controller('api/valida-pago-cripto')
export class ValidarPagoCriptoControlador {
  constructor(
    private readonly validarPagoCriptoService: ValidarPagoCriptoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) { }

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: ValidarPagoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Valida el pago hecho por coinpaiments (criptomoneda)' })
  @ApiResponse({
    status: 200,
    description: 'OK',
  })
  @ApiResponse({ status: 404, description: 'Error al crear' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async validaPago(
    @Headers() headers,
    @Body() validarPagoDto: ValidarPagoDto,
  ) {

    console.log('validarPagoDto: ', validarPagoDto)
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(validarPagoDto.idTransaccion)
      ) {
        const valida = await this.validarPagoCriptoService.validarPago(
          validarPagoDto.idTransaccion,
          codIdioma
        );

        if (valida) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: valida,
          });
        } else {
          const ERROR_REGISTRO_PAGO = await this.i18n.translate(
            codIdioma.concat('.ERROR_REGISTRO_PAGO'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_REGISTRO_PAGO,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
