import { Controller, Get, Headers, HttpStatus, Query } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
//Metodos de pago
import { Funcion } from 'src/shared/funcion';
import { ObtenerAportacionesService } from './../casos_de_uso/obtener-aportaciones.service';
import { AportacionesDto } from './../entidad/aportaciones-dto';

@ApiTags('Transacciones')
@Controller('api/aportaciones')
export class ObtenerAportacionesControlador {
  constructor(
    private readonly obtenerAportacionesService: ObtenerAportacionesService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/')
  @ApiOperation({
    summary:
      'Este método obtiene las aportaciones de un determinado usuario segun el tipo de aportacion (suscripcion, valorExtra)',
  })
  @ApiResponse({ status: 200, type: [AportacionesDto], description: 'OK' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async obtenerAportacionesRango(
    @Headers() headers,
    @Query('idUsuario') idUsuario: string,
    // @Query('mes') mes: string,
    @Query('anio') anio: Date,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (mongoose.isValidObjectId(idUsuario) && anio) {
        const aportaciones = await this.obtenerAportacionesService.ObtenerAportaciones(
          idUsuario,
          anio,
          // mes,
        );
        if (aportaciones) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: aportaciones,
          });
        } else {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_OBTENER,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
