import { ObtenerAportacionesControlador } from './obtener-aportaciones.controller';
import { CustomersService } from '../../../drivers/stripe/services/payment-customers.service';
import { Module, Delete } from '@nestjs/common';
import { CrearTransaccionControlador } from './crear-transaccion.controller';
import { TransaccionServicesModule } from '../casos_de_uso/transaccion-services.module';
import { Funcion } from 'src/shared/funcion';
import { FiltrarAportacionesControlador } from './filtrar-aportaciones.controller';
import { CrearRefundStripeControlador } from './refund-stripe.controller';
import { ObtenerTransaccionesActivasControlador } from './obtener-transacciones-activas.controller';
import { CrearRefundPaymentezControlador } from './refund-paymentez.controller';
import { ObtenerRatesCoinpaymentezControlador } from './obtener-monedas-coinpaymentez.controller';
import { VerificarIngresoControlador } from './verificar-ingreso.controller';
import { DonacionProyectoControlador } from './donacion-proyecto.controller';
import { ValidarPagoStripeControlador } from './validar-pago-stripe.controller';
import { ValidarPagoCriptoControlador } from './validar-pago-cripto.controller';

@Module({
  imports: [TransaccionServicesModule],
  providers: [CustomersService, Funcion],
  exports: [],
  controllers: [
    CrearTransaccionControlador,
    ObtenerAportacionesControlador,
    FiltrarAportacionesControlador,
    CrearRefundStripeControlador,
    ObtenerTransaccionesActivasControlador,
    CrearRefundPaymentezControlador,
    ObtenerRatesCoinpaymentezControlador,
    VerificarIngresoControlador,
    DonacionProyectoControlador,
    ValidarPagoStripeControlador,
    ValidarPagoCriptoControlador
  ],
})
export class TransaccionControllerModule {}
