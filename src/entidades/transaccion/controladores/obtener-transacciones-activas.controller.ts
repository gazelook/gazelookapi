import { Controller, Get, Headers, HttpStatus, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
//Metodos de pago
import { Funcion } from 'src/shared/funcion';
import { ObtenerTransaccionService } from '../casos_de_uso/obtener-transaccion.service';

@ApiTags('Transacciones')
@Controller('api/transaccion')
export class ObtenerTransaccionesActivasControlador {
  constructor(
    private readonly obtenerTransaccionService: ObtenerTransaccionService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/activas')
  @ApiOperation({
    summary:
      'Este método obtiene las transacciones activas de un usuario',
  })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async obtenerTransaccionesActivasUsuario(
    @Headers() headers,
    @Query('idUsuario') idUsuario: string
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (mongoose.isValidObjectId(idUsuario)) {
        const aportaciones = await this.obtenerTransaccionService.obtenerTransaccionesActivasUsuario(
          idUsuario
        );

        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: aportaciones,
        });

      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
