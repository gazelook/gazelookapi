import {
  Body,
  Controller,
  Get,
  Headers,
  HttpStatus,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CreaRefundStripeService } from '../casos_de_uso/crear-refund-stripe.service';
import { CrearRefundStripeDto } from '../entidad/devoluciones.dto';

@ApiTags('Transacciones')
@Controller('api/transaccion')
export class CrearRefundStripeControlador {
  constructor(
    private creaRefundStripeService: CreaRefundStripeService,
    private readonly funcion: Funcion,
    private readonly i18n: I18nService,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Post('/stripe-refund')
  @ApiBody({ type: CrearRefundStripeDto })
  @ApiOperation({
    summary: 'Este método crea una devolucion del pago que hace el cliente',
  })
  @ApiResponse({ status: 201, description: 'Creado' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async crearRefundStripe(
    @Body() crearRefundto: CrearRefundStripeDto,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (crearRefundto.idPago) {
        const pagoStripe = await this.creaRefundStripeService.crearRefundStripe(
          crearRefundto.idPago,
        );
        if (pagoStripe) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: pagoStripe,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (err) {
      // return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: err.message })
      if (err?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${err.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: err.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: err.message,
        });
      }
    }
  }

  @Get('/stripe-refund')
  @ApiSecurity('Authorization')
  @ApiOperation({
    summary:
      'Este método obtiene las devolucion del pago que han hecho los clientes por fecha',
  })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async obtenerRefundStripeById(
    @Query('fechaInicial') fechaInicial: number,
    @Query('fechaFinal') fechaFinal: number,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (fechaInicial && fechaFinal) {
        const refundStripe = await this.creaRefundStripeService.obtenerRefundStripe(
          fechaInicial,
          fechaFinal,
        );

        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: refundStripe,
        });
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
