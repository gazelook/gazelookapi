import { CrearPagoPaypalService } from './../casos_de_uso/crear-pago-paypal.service';
import { RespuestaInterface } from './../../../shared/respuesta-interface';

import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';

import { Controller, Post, Body, HttpStatus, Headers } from '@nestjs/common';

//Metodos de pago
import Stripe from 'stripe';
import { CrearPagoStripeService } from '../casos_de_uso/crear-pago-stripe.service';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Transacciones')
@Controller('api/transaccion')
export class CrearTransaccionControlador {
  paypal = require('@paypal/checkout-server-sdk');

  constructor(
    private crearPagoStripeService: CrearPagoStripeService,
    private crearPagoPaypalService: CrearPagoPaypalService,
    private readonly funcion: Funcion,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Post('/stripe-pago')
  @ApiOperation({
    summary:
      'Este método crea una sesión del cliente, como intento de pago y devuelve un "clientSecret" para confirmar un pago',
  })
  @ApiResponse({ status: 201, description: 'Creado' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async crearIntentoPago(@Body() dataUser: any, @Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const pagoStripe = await this.crearPagoStripeService.crearPagoStripe(
        dataUser,
      );
      if (pagoStripe.statusCode != 400) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: {
            idPago: pagoStripe.stripe.client_secret,
            idTransaccion: pagoStripe.idTransaccion,
          },
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: pagoStripe.statusCode,
          mensaje: pagoStripe.message,
        });
      }
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Post('/paypal-pago')
  @ApiOperation({
    summary:
      'Este proceso crea una orden de pago y devuelve un "orderId" para autorizar un pago',
  })
  @ApiResponse({ status: 201, description: 'Creado' })
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async crearpagoPaypal(@Body() dataUser?: any) {
    const respuesta = new RespuestaInterface();

    try {
      const pagoPaypal = await this.crearPagoPaypalService.crearPagoPaypal(
        dataUser,
      );

      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: {
          idPago: pagoPaypal.idPaypal,
          idTransaccion: pagoPaypal.idTransaccion,
        },
      });
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
