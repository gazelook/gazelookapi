import { Controller, Get, Headers, HttpStatus } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
//Metodos de pago
import { Funcion } from 'src/shared/funcion';
import { ObtenerDiaService } from '../casos_de_uso/contador-metodo.service';

@ApiTags('Transacciones')
@Controller('api/verificar-ingreso')
export class VerificarIngresoControlador {
  constructor(
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private obtenerDiaService: ObtenerDiaService,
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/')
  @ApiOperation({
    summary:
      'Este método devulve un 1 o 2 segun el numero de veces que se ejecuta el método',
  })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async verificarIngreso(
    @Headers() headers
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {

        const aportaciones = await this.obtenerDiaService.obtenerDia();

        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: aportaciones,
        });

    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
