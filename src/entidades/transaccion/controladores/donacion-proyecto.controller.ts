import {
  Body, Controller, Headers, HttpStatus, Post, UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody, ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { DonacionProyectoDto } from 'src/entidades/proyectos/entidad/donacion-proyecto-dto';
import { Funcion } from 'src/shared/funcion';
import { DonacionProyectoService } from '../casos_de_uso/donacion-proyecto.service';

@ApiTags('Proyectos')
@Controller('api/donacion-proyecto')
export class DonacionProyectoControlador {
  constructor(
    private readonly donacionProyectoService: DonacionProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) { }

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: DonacionProyectoDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Donacion a proyectos' })
  @ApiResponse({
    status: 201,
    description: 'Creación correcta',
  })
  @ApiResponse({ status: 404, description: 'Error al crear' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @UseGuards(AuthGuard('jwt'))
  public async donacionProyecto(
    @Headers() headers,
    @Body() donacionProyectoDto: DonacionProyectoDto,
  ) {
    console.log('donacionProyectoDto: ', donacionProyectoDto);

    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (
        mongoose.isValidObjectId(donacionProyectoDto.idUsuario) &&
        mongoose.isValidObjectId(donacionProyectoDto.idProyecto) &&
        donacionProyectoDto.metodoPago.codigo &&
        // donacionProyectoDto.direccion &&
        donacionProyectoDto.datosFacturacion &&
        donacionProyectoDto.transacciones.length > 0 &&
        donacionProyectoDto.monedaRegistro.codNombre &&
        donacionProyectoDto.email
      ) {
        const proyecto = await this.donacionProyectoService.donacionProyecto(
          donacionProyectoDto
        );

        if (proyecto) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
            datos: proyecto,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
