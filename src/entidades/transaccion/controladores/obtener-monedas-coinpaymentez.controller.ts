import {
  Controller, Get, Headers,
  HttpStatus
} from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse, ApiSecurity, ApiTags
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerRatesCoinpaymentezService } from '../casos_de_uso/obtener-rates-coinpaymentez.service';

@ApiTags('Transacciones')
@Controller('api/transaccion')
export class ObtenerRatesCoinpaymentezControlador {
  constructor(
    private obtenerRatesCoinpaymentezService: ObtenerRatesCoinpaymentezService,
    private readonly funcion: Funcion,
    private readonly i18n: I18nService,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/coinpayments-rates')
  @ApiSecurity('Authorization')
  @ApiOperation({
    summary: 'Este método obtiene todas las rates de coinpaymentez',
  })
  @ApiResponse({ status: 200, description: 'OK' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  // @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async obtenerRatesCoinPaymentez(
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      
        const rates = await this.obtenerRatesCoinpaymentezService.ObtenerRates();
        
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: rates,
          });
        
      
    } catch (err) {
      // return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: err.message })
      if (err?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${err.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: err.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: err.message,
        });
      }
    }
  }
  
}
