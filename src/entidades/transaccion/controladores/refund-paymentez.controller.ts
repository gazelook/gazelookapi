import {
  Body,
  Controller, Headers,
  HttpStatus,
  Post, UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse, ApiSecurity, ApiTags
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CreaRefundPaymentezService } from '../casos_de_uso/crear-refund-paymentez.service';
import { CrearRefundPaymentezDto } from '../entidad/devoluciones.dto';

@ApiTags('Transacciones')
@Controller('api/transaccion')
export class CrearRefundPaymentezControlador {
  constructor(
    private creaRefundPaymentezService: CreaRefundPaymentezService,
    private readonly funcion: Funcion,
    private readonly i18n: I18nService,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Post('/paymentez-refund')
  @ApiSecurity('Authorization')
  @ApiBody({ type: CrearRefundPaymentezDto })
  @ApiOperation({
    summary: 'Este método crea una devolucion del pago que hace el cliente en paymentez',
  })
  @ApiResponse({ status: 201, description: 'Creado' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async crearRefundPaymentez(
    @Body() crearRefundto: CrearRefundPaymentezDto,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (crearRefundto.idPago && crearRefundto.idRefound) {
        const pagoStripe = await this.creaRefundPaymentezService.crearRefundPaymentez(
          crearRefundto.idPago,
          crearRefundto.idRefound
        );
        if (pagoStripe) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: pagoStripe,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (err) {
      // return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: err.message })
      if (err?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${err.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: err.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: err.message,
        });
      }
    }
  }
  
}
