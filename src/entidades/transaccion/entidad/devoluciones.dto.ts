import { ApiProperty } from "@nestjs/swagger";

export class CrearRefundStripeDto {
  @ApiProperty({ required: true , type:String})
  idPago: string;
}

export class CrearRefundPaymentezDto {
  @ApiProperty({ required: true , type:String})
  idPago: string;
  @ApiProperty({ required: true , type:String})
  idRefound: string;
}
