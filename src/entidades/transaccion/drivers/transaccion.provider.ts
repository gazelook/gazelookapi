import { informacionPago } from './../../../drivers/mongoose/modelos/informacion_pago/informacion-pago.schema';
import { entidadesModelo } from './../../../drivers/mongoose/modelos/catalogoEntidades/catalogoEntidadesModelo';
import { TransaccionModelo } from './../../../drivers/mongoose/modelos/transaccion/transaccion.schema';
import { Connection } from 'mongoose';
import { BeneficiarioModelo } from 'src/drivers/mongoose/modelos/beneficiario/beneficiario.schema';
import { ConversionTransaccionModelo } from '../../../drivers/mongoose/modelos/convercion_transaccion/convercion-transaccion.schema';
import { DevolucionModelo } from 'src/drivers/mongoose/modelos/devolucion/devolucion.schema';

export const transaccionProviders = [
  {
    provide: 'TRANSACCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('transaccion', TransaccionModelo, 'transaccion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ESTADOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('catalogo_estados', entidadesModelo),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'INFORMACION_PAGO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('informacion_pago', informacionPago, 'informacion_pago'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'BENEFICIARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('beneficiario', BeneficiarioModelo, 'beneficiario'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONVERSION_TRANSACCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'conversion_transaccion',
        ConversionTransaccionModelo,
        'conversion_transaccion',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'DEVOLUCION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('devolucion', DevolucionModelo, 'devolucion'),
    inject: ['DB_CONNECTION'],
  },
];
