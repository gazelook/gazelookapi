import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
} from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
} from '@nestjs/common';
import { ObtenerPerfilesService } from '../casos_de_uso/obtener-perfiles.service';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ObtenerPerfilesDto } from '../entidad/obtenerPerfiles.dto';

import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';

@ApiTags('Catalogos')
@Controller('api/catalogos/tipo-perfiles')
export class ObtenerPerfilesControlador {
  constructor(
    private readonly obtenerPerfilesService: ObtenerPerfilesService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: ObtenerPerfilesDto,
    description: 'Lista de Perfiles',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener los perfiles' })
  @ApiOperation({
    summary:
      'Este metodo retorna la lista de todos los perfiles con la traduccion estatica. Se recibira el codigo del idioma. Si no se envia el codigo el idioma por defecto sera el ingles',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async ObtenerPerfiles(@Headers() headers) {
    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
      const perfiles = await this.obtenerPerfilesService.obtenerPerfiles(
        codIdioma,
      );

      if (!perfiles || perfiles.length === 0) {
        const ERROR_OBTENER = await this.i18n.translate(
          codIdioma.concat('.ERROR_OBTENER'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ERROR_OBTENER,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: perfiles,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
