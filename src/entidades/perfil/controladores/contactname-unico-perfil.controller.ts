import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiHeader,
  ApiSecurity,
} from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ContactnameUnicoPerfilService } from '../casos_de_uso/contactname-unico-perfil.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';

@ApiTags('Perfiles')
@Controller('api/perfil/nombre-contacto-unico')
export class ContactnameUnicoControlador {
  constructor(
    private readonly contactnameUnicoPerfilService: ContactnameUnicoPerfilService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  // Add User: /users/create
  @Get('/:nombre/:contactoTraducido')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'Nombre de contacto unico' })
  @ApiResponse({ status: 404, description: 'Error al consultar' })
  @ApiOperation({
    summary: 'Este metodo retorna OK si el nombre de contacto esta disponible',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  public async verificarNombreContacto(
    @Param('nombre') nombreContacto: string,
    @Headers() headers,
    @Param('contactoTraducido') contactoTraducido?: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const result = await this.contactnameUnicoPerfilService.nombreContactoUnico(
        nombreContacto,
        contactoTraducido,
      );
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: result,
      });
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
