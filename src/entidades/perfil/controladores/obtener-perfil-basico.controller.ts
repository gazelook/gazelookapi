import { ObtenerPerfilUsuarioService } from './../casos_de_uso/obtener-perfil-usuario.service';

import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
  ApiQuery,
} from '@nestjs/swagger';

import {
  Controller,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  Query,
  UseGuards,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ObtenerPerfilGeneralService } from '../casos_de_uso/obtener-perfil-general.service';
import * as mongoose from 'mongoose';
import { RetornoPerfilBasicoDto } from '../entidad/retorno-perfil-basico.dto';
import { ObtenerPerfilBasicoService } from '../casos_de_uso/obtener-perfil-basico.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Perfiles')
@Controller('api/perfil-basico/')
export class ObtenerPerfilBasicoControlador {
  constructor(
    private readonly obtenerPerfilBasicoService: ObtenerPerfilBasicoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Get('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: RetornoPerfilBasicoDto,
    description: 'Datos basicos del usuario',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener el perfil' })
  @ApiOperation({
    summary:
      'Este metodo retorna el perfil del usuario(foto de perfil, nombre de contacto) y pensamientos',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiQuery({ name: 'perfilVisitante', required: false })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerPerfilBasico(
    @Headers() headers,
    @Query('idPerfil') idPerfil: string,
    @Query('perfilVisitante') perfilVisitante: string,
  ) {
    const respuesta = new RespuestaInterface();
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      let perfilDatos;
      if (
        mongoose.isValidObjectId(idPerfil) ||
        mongoose.isValidObjectId(perfilVisitante)
      ) {
        perfilDatos = await this.obtenerPerfilBasicoService.obtenerPerfilBasico(
          idPerfil,
          perfilVisitante,
          codIdioma,
        );
        if (!perfilDatos) {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_OBTENER,
          });
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: perfilDatos,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
