import { ObtenerPerfilUsuarioService } from './../casos_de_uso/obtener-perfil-usuario.service';

import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
  ApiQuery,
} from '@nestjs/swagger';

import {
  Controller,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  Query,
  UseGuards,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ObtenerPerfilGeneralService } from '../casos_de_uso/obtener-perfil-general.service';
import * as mongoose from 'mongoose';
import {
  ResultFotoPerfil,
  ResultPerfilGeneralDto,
} from '../entidad/perfil-general.dto';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';
import { LocalAuthGuard } from 'src/entidades/login/drivers/local-auth.guard';
import { ObtenerAlbumPerfilService } from '../casos_de_uso/obtener-album-perfil.service';

@ApiTags('Perfiles')
@Controller('api/perfil/album')
export class ObtenerAlbumPerfilController {
  constructor(
    private readonly obtenerAlbumPerfilService: ObtenerAlbumPerfilService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Get('/:idPerfil')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: ResultFotoPerfil,
    description: 'Obtiene el nombre de contacto y la foto de perfil',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener los datos' })
  @ApiOperation({ summary: 'Retorna la foto de perfil y el nombre contacto' })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiQuery({ name: 'perfilOtros', required: false })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerPerfilGeneral(
    @Headers() headers,
    @Param('idPerfil') idPerfil: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      let perfilDatos;
      if (mongoose.isValidObjectId(idPerfil)) {
        perfilDatos = await this.obtenerAlbumPerfilService.obtenerAlbumPerfil(
          idPerfil,
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: perfilDatos,
        });
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
