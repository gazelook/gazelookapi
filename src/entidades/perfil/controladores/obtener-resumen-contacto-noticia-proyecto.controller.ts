import { ObtenerPerfilUsuarioService } from '../casos_de_uso/obtener-perfil-usuario.service';

import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
  ApiQuery,
} from '@nestjs/swagger';

import {
  Controller,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  Query,
  UseGuards,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ObtenerPerfilGeneralService } from '../casos_de_uso/obtener-perfil-general.service';
import * as mongoose from 'mongoose';
import { ResultPerfilGeneralDto } from '../entidad/perfil-general.dto';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';
import { LocalAuthGuard } from 'src/entidades/login/drivers/local-auth.guard';
import { ResumenContactoNoticiaProyectoService } from '../casos_de_uso/obtener-resumen-contacto-noticia-proyecto.service';

@ApiTags('Perfiles')
@Controller('api/resumen-entidad/')
export class ResumenContactoNoticiaProyectoControlador {
  constructor(
    private readonly resumenContactoNoticiaProyectoService: ResumenContactoNoticiaProyectoService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Get('/')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'Datos del resumen' })
  @ApiResponse({ status: 406, description: 'Parametros no validos' })
  @ApiOperation({
    summary:
      'Este metodo retorna el resumen de un contacto perfil, noticia o proyecto',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  public async resumenEntidad(
    @Headers() headers,
    @Query('codigoEntidad') codigoEntidad: string,
    @Query('idEntidad') idEntidad: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      let resumen;
      if (mongoose.isValidObjectId(idEntidad)) {
        resumen = await this.resumenContactoNoticiaProyectoService.obtenerResumen(
          idEntidad,
          codigoEntidad,
          codIdioma,
        );
        if (!resumen || resumen.length === 0) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: {},
          });
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: resumen,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
