import { ObtenerPerfilUsuarioService } from './../casos_de_uso/obtener-perfil-usuario.service';

import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';

import {
  Controller,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  Post,
  Put,
  Patch,
  Delete,
  UseGuards,
  Req,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ActivarPerfilService } from '../casos_de_uso/activar-perfil.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Perfiles')
@Controller('api/perfil/activar')
export class activarPerfilControlador {
  constructor(
    private readonly activarPerfilService: ActivarPerfilService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Put('/:idPerfil')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'Perfil activado' })
  @ApiResponse({ status: 404, description: 'Error al activar el perfil' })
  @ApiOperation({
    summary:
      'Este metodo activa un perfil con sus albunes, noticias, proyectos, pensamientos',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  public async activarPerfil(
    @Param('idPerfil') idPerfil: string,
    @Headers() headers,
    @Req() req: any,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    const dataToken = req.user;

    try {
      if (mongoose.isValidObjectId(idPerfil)) {
        const perfil = await this.activarPerfilService.activarPerfil(
          idPerfil,
          headers.idioma,
          dataToken.dispositivo,
        );

        if (perfil) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
            datos: perfil,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
