import { ObtenerPerfilUsuarioService } from './../casos_de_uso/obtener-perfil-usuario.service';

import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
  ApiQuery,
  ApiBearerAuth,
} from '@nestjs/swagger';

import {
  Controller,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  Query,
  UseGuards,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ObtenerPerfilGeneralService } from '../casos_de_uso/obtener-perfil-general.service';
import * as mongoose from 'mongoose';
import { ResultPerfilGeneralDto } from '../entidad/perfil-general.dto';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';
import { LocalAuthGuard } from 'src/entidades/login/drivers/local-auth.guard';

@ApiTags('Perfiles')
@Controller('api/perfil-general/')
export class ObtenerPerfilGeneralControlador {
  constructor(
    private readonly obtenerPerfilGeneralService: ObtenerPerfilGeneralService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Get('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: ResultPerfilGeneralDto,
    description: 'Datos del Pefil de Usuario',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener el perfil' })
  @ApiOperation({
    summary:
      'Este metodo retorna el perfil del usuario, noticias, proyectos, contactos, pensamientos',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiQuery({ name: 'perfilOtros', required: false })
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  public async obtenerPerfilGeneral(
    @Headers() headers,
    @Query('perfil') perfil: string,
    @Query('perfilOtros') perfilOtros: string,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      let perfilDatos;
      if (
        mongoose.isValidObjectId(perfil) ||
        mongoose.isValidObjectId(perfilOtros)
      ) {
        perfilDatos = await this.obtenerPerfilGeneralService.obtenerPerfilGeneral(
          perfil,
          perfilOtros,
          codIdioma,
        );
        if (!perfilDatos || perfilDatos.length === 0) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: {},
          });
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: perfilDatos,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
