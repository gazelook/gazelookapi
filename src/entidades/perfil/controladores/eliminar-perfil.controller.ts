import {
  Body,
  Controller,
  Delete,
  Headers,
  HttpStatus,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { EliminarPerfilService } from '../casos_de_uso/eliminar-perfil.service';
import { EliminarPerfilDto } from '../entidad/eliminar-perfil.dto';

@ApiTags('Perfiles')
@Controller('api/perfil')
export class EliminarPerfilControlador {
  constructor(
    private readonly eliminarPerfilService: EliminarPerfilService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Delete('/')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'Perfil eliminado' })
  @ApiResponse({ status: 404, description: 'Error al eliminar el perfil' })
  @ApiOperation({
    summary:
      'Este metodo elimina un perfil con sus albunes, noticias, proyectos, pensamientos',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  public async eliminarPerfil(
    @Body() dataPerfil: EliminarPerfilDto,
    @Headers() headers,
    @Req() req: any,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const dataToken = req.user;
      //console.log("dispositivoToken", dataToken);
      dataPerfil.dispositivo = dataToken.dispositivo;

      if (mongoose.isValidObjectId(dataPerfil._id)) {
        const perfil = await this.eliminarPerfilService.eliminarPerfil(
          dataPerfil,
          codIdioma,
        );

        if (perfil) {
          // si se hiberno correctamente; estado: true
          if (perfil.estado) {
            const PERFIL_HIBERNADO = await this.i18n.translate(
              codIdioma.concat('.PERFIL_HIBERNADO'),
              {
                lang: codIdioma,
              },
            );
            return this.funcion.enviarRespuestaOptimizada({
              codigoEstado: HttpStatus.OK,
              mensaje: PERFIL_HIBERNADO,
            });
          } else {
            const ELIMINACION_CORRECTA = await this.i18n.translate(
              codIdioma.concat('.ELIMINACION_CORRECTA'),
              {
                lang: codIdioma,
              },
            );
            return this.funcion.enviarRespuestaOptimizada({
              codigoEstado: HttpStatus.OK,
              mensaje: ELIMINACION_CORRECTA,
            });
          }
        } else {
          const ERROR_ELIMINAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ELIMINAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
