import { ObtenerPerfilUsuarioService } from './../casos_de_uso/obtener-perfil-usuario.service';

import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';

import {
  Controller,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  Post,
  UseGuards,
  Req,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { PerfilUsuarioDto } from '../entidad/perfil-usuario.dto';
import { NuevoPerfilService } from '../casos_de_uso/nuevo-perfil.service';
import { CrearPerfilDto } from '../entidad/crear-perfil.dto';
import { RetornoCrearPerfilDto } from '../entidad/retorno-crear-perfil.dto';
import { AuthGuard } from '@nestjs/passport';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';

@ApiTags('Perfiles')
@Controller('api/perfil')
export class NuevoPerfilControlador {
  constructor(
    private readonly nuevoPerfilService: NuevoPerfilService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Post('/:idUsuario')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: RetornoCrearPerfilDto,
    description: 'Perfil creado con éxito',
  })
  @ApiResponse({ status: 404, description: 'Error al crear el perfil' })
  @ApiOperation({
    summary:
      'Este metodo crea un nuevo perfil con los correspondientes albunes(medias)',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  public async crearNuevoPerfil(
    @Body() dataPerfil: CrearPerfilDto,
    @Req() req,
    @Param('idUsuario') idUsuario: string,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    const dataToken = req.user;
    console.log('dispositivoToken', dataToken);
    try {
      if (mongoose.isValidObjectId(idUsuario)) {
        const perfil = await this.nuevoPerfilService.nuevoPerfil(
          idUsuario,
          dataPerfil,
          codIdioma,
          dataToken.dispositivo,
        );

        if (perfil) {
          const CREACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.CREACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            mensaje: CREACION_CORRECTA,
            datos: perfil,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
