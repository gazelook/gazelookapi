import { ObtenerPerfilUsuarioService } from './../casos_de_uso/obtener-perfil-usuario.service';

import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';

import {
  Controller,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  UseGuards,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { PerfilUsuarioDto } from '../entidad/perfil-usuario.dto';

import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Perfiles')
@Controller('api/perfil')
export class ObtenerPerfilUsuarioControlador {
  constructor(
    private readonly obtenerPerfileService: ObtenerPerfilUsuarioService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Get('/:usuario')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: PerfilUsuarioDto,
    description: 'Lista de Pefiles de Usuario',
  })
  @ApiResponse({ status: 404, description: 'Error al obtener el perfil' })
  @ApiOperation({
    summary:
      'Este metodo retorna los perfiles que el usuario escogio con la traduccion estatica. Se recibira el codigo del idioma. Si no se envia el codigo el idioma por defecto sera el ingles',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  public async obtenerPerfilesUsuario(
    @Param('usuario') usuario: string,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (mongoose.isValidObjectId(usuario)) {
        const perfil = await this.obtenerPerfileService.obtenerPerfilUsuario(
          usuario,
          codIdioma,
        );

        if (!perfil || perfil.length === 0) {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_OBTENER,
          });
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: perfil,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
    }
  }
}
