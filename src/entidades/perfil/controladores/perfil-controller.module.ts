import { ObtenerPerfilesControlador } from './obtener-perfiles.controller';
import { Module } from '@nestjs/common';
import { PerfilServiceModule } from '../casos_de_uso/perfil.services.module';
import { ObtenerPerfilUsuarioControlador } from './obtener-perfil-usuario.controller';
import { NuevoPerfilControlador } from './nuevo-perfil.controller';
import { ActualizarPerfilControlador } from './actualizar-perfil.controller';
import { ObtenerAllDatosPerfilUsuarioControlador } from './obtener-all-datos-perfil.controller';
import { EliminarPerfilControlador } from './eliminar-perfil.controller';
import { BuscarPerfilesControlador } from './buscar-perfil.controller';
import { ContactnameUnicoControlador } from './contactname-unico-perfil.controller';
import { activarPerfilControlador } from './activar-perfil.controller';
import { ObtenerPerfilGeneralControlador } from './obtener-perfil-general.controller';
import { ObtenerPerfilBasicoControlador } from './obtener-perfil-basico.controller';
import { Funcion } from 'src/shared/funcion';
import { ResumenContactoNoticiaProyectoControlador } from './obtener-resumen-contacto-noticia-proyecto.controller';
import { ObtenerAlbumPerfilController } from './obtener-album-perfil.controller';
import { BuscarContactosPerfilController } from './buscar-contactos-perfil.controller';

@Module({
  imports: [PerfilServiceModule],
  providers: [Funcion],
  exports: [PerfilServiceModule],
  controllers: [
    ObtenerPerfilUsuarioControlador,
    ObtenerPerfilesControlador,
    NuevoPerfilControlador,
    ActualizarPerfilControlador,
    ObtenerAllDatosPerfilUsuarioControlador,
    EliminarPerfilControlador,
    BuscarPerfilesControlador,
    ContactnameUnicoControlador,
    activarPerfilControlador,
    ObtenerPerfilGeneralControlador,
    ObtenerPerfilBasicoControlador,
    ResumenContactoNoticiaProyectoControlador,
    ObtenerAlbumPerfilController,
    BuscarContactosPerfilController,
  ],
})
export class PerfilControllerModule {}
