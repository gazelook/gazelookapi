import { ObtenerPerfilUsuarioService } from './../casos_de_uso/obtener-perfil-usuario.service';

import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
} from '@nestjs/swagger';

import {
  Controller,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  Post,
  Put,
  Patch,
  UseGuards,
  Req,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { ActualizarPerfilService } from '../casos_de_uso/actualizar-perfil.service';
import { RetornoCrearPerfilDto } from '../entidad/retorno-crear-perfil.dto';
import { ActualizarPerfilDto } from '../entidad/actualizar-perfil.dto';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Perfiles')
@Controller('api/perfil')
export class ActualizarPerfilControlador {
  constructor(
    private readonly actualizarPerfilService: ActualizarPerfilService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Patch('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: RetornoCrearPerfilDto,
    description: 'Perfil actualizado con éxito',
  })
  @ApiResponse({ status: 404, description: 'Error al actualizar el perfil' })
  @ApiOperation({
    summary:
      'Este metodo actualiza un perfil con los correspondientes albunes(medias)',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  public async actualizarPerfil(
    @Body() dataPerfil: ActualizarPerfilDto,
    @Req() req,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      // verificar que el token pertenece al usuario
      // const perfilVerificado = this.funcion.verificarPerfilToken(dataPerfil._id, req.user.user.perfiles);
      // if (!perfilVerificado) {
      //     const NO_PERMISO_ACCION = await this.i18n.translate(codIdioma.concat('.NO_PERMISO_ACCION'), {
      //         lang: codIdioma
      //     });
      //     return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.UNAUTHORIZED, mensaje: NO_PERMISO_ACCION })
      // }

      if (mongoose.isValidObjectId(dataPerfil._id)) {
        const perfil = await this.actualizarPerfilService.actualizarPerfil(
          dataPerfil,
          codIdioma,
        );

        if (perfil) {
          const ACTUALIZACION_CORRECTA = await this.i18n.translate(
            codIdioma.concat('.ACTUALIZACION_CORRECTA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ACTUALIZACION_CORRECTA,
            datos: perfil,
          });
        } else {
          const ERROR_ACTUALIZAR = await this.i18n.translate(
            codIdioma.concat('.ERROR_ACTUALIZAR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CONFLICT,
            mensaje: ERROR_ACTUALIZAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
