import { RespuestaInterface } from 'src/shared/respuesta-interface';
import {
  ApiResponse,
  ApiTags,
  ApiOperation,
  ApiSecurity,
  ApiHeader,
} from '@nestjs/swagger';

import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  Headers,
  Query,
  UseGuards,
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { BuscarPerfilesService } from '../casos_de_uso/buscar-perfiles.service';

import * as mongoose from 'mongoose';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';

import { Response } from 'express';
import { ObtenerUsuariosSistemaDto } from '../entidad/obtenerPerfiles.dto';
import { AuthGuard } from '@nestjs/passport';
import { Funcion } from 'src/shared/funcion';
import { BuscarContactosPerfilService } from '../casos_de_uso/buscar-contactos-perfil.service';

@ApiTags('Perfiles')
@Controller('api/buscar-contactos-perfil')
export class BuscarContactosPerfilController {
  constructor(
    private readonly i18n: I18nService,
    private readonly buscarContactosPerfilService: BuscarContactosPerfilService,
  ) {}

  funcion = new Funcion(this.i18n);

  @Get('/')
  @ApiSecurity('Authorization')
  @ApiResponse({
    status: 200,
    type: ObtenerUsuariosSistemaDto,
    description: 'OK',
  })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({
    status: 404,
    description: 'No se ha podido obtener los datos',
  })
  @ApiResponse({ status: 406, description: 'Parámetros no validos' })
  @ApiOperation({
    summary:
      'Este metodo retorna los contactos de un perfil según parámetros de busqueda como: nombre del perfil o nombreContacto',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @UseGuards(AuthGuard('jwt'))
  public async buscarContactosPerfil(
    @Query('busqueda') buscar: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Headers() headers,
    @Res() response: Response,
    @Query('perfil') perfilId: string,
  ) {
    let codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(perfilId) &&
        !isNaN(limite) &&
        !isNaN(pagina) &&
        limite > 0 &&
        pagina > 0
      ) {
        const busqueda = await this.buscarContactosPerfilService.bucarContactosPerfil(
          buscar,
          limite,
          pagina,
          response,
          perfilId,
        );

        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: busqueda,
        });
        response.send(respuesta);
        return respuesta;
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
