import { ObtenerPerfilUsuarioService } from './../casos_de_uso/obtener-perfil-usuario.service';

import {
  ApiResponse,
  ApiTags,
  ApiHeader,
  ApiOperation,
  ApiSecurity,
  ApiBearerAuth,
} from '@nestjs/swagger';

import {
  Controller,
  Body,
  Headers,
  Res,
  HttpStatus,
  HttpException,
  Param,
  Get,
  UseGuards,
} from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { PerfilUsuarioDto } from '../entidad/perfil-usuario.dto';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';
import * as mongoose from 'mongoose';
import { ObtenerAllDatosPerfilService } from '../casos_de_uso/obtener-all-datos-perfil.service';

@ApiTags('Perfiles')
@Controller('api/perfil/datos')
export class ObtenerAllDatosPerfilUsuarioControlador {
  constructor(
    private readonly obtenerAllDatosPerfilService: ObtenerAllDatosPerfilService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}
  @Get('/:idPerfil')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'Datos del Pefil de Usuario' })
  @ApiResponse({ status: 404, description: 'Error al obtener el perfil' })
  @ApiOperation({
    summary: 'Este metodo retorna el detalle de los parametros de un perfil',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  public async obtenerPerfilesUsuario(
    @Param('idPerfil') idPerfil: string,
    @Headers() headers,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (mongoose.isValidObjectId(idPerfil)) {
        const perfil = await this.obtenerAllDatosPerfilService.obtenerAllDatosPerfil(
          idPerfil,
          codIdioma,
        );

        if (!perfil || perfil.length === 0) {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_OBTENER,
          });
        } else {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: perfil,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(
          codIdioma.concat(`.${e.codigoNombre}`),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: e.codigo,
          mensaje: MENSAJE,
        });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message,
        });
      }
    }
  }
}
