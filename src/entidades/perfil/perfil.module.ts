import { Module } from '@nestjs/common';
import { PerfilControllerModule } from './controladores/perfil-controller.module';

@Module({
  imports: [PerfilControllerModule],
})
export class PerfilModule {}
