import { Connection } from 'mongoose';
import { perfilModelo } from '../../../drivers/mongoose/modelos/perfil/perfil.schema';
import { tipoPerfilModelo } from '../../../drivers/mongoose/modelos/catalogo-tipo-perfil/catalogo-tipo-perfil.schema';
import { traduccionTipoPerfilModelo } from '../../../drivers/mongoose/modelos/traduccion_catalogo_tipo_perfil/traduccion-catalogo-tipo-perfil.schema';
import { usuarioModelo } from 'src/drivers/mongoose/modelos/usuarios/usuario.schema';
import { ParticipanteAsociacionModelo } from 'src/drivers/mongoose/modelos/participante_asociacion/participante-asociacion.schema';
import { CatalogoPaisModelo } from '../../../drivers/mongoose/modelos/catalogo_pais/catalogo-pais.schema';
import { CatalogoLocalidadModelo } from '../../../drivers/mongoose/modelos/catalogo_localidad/catalogo-localidad.schema';

export const PerfilProviders = [
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION'],
  },

  {
    provide: 'TIPO_PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_tipo_perfil',
        tipoPerfilModelo,
        'catalogo_tipo_perfil',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_TIPO_PERFIL_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_catalogo_tipo_perfil',
        traduccionTipoPerfilModelo,
        'traduccion_catalogo_tipo_perfil',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'USUARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('usuario', usuarioModelo, 'usuario'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PARTICIPANTE_ASOCIACION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'participante_asociacion',
        ParticipanteAsociacionModelo,
        'participante_asociacion',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_PAIS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('catalogo_pais', CatalogoPaisModelo, 'catalogo_pais'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_LOCALIDAD_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_localidad',
        CatalogoLocalidadModelo,
        'catalogo_localidad',
      ),
    inject: ['DB_CONNECTION'],
  },
];
