import {
  Matches,
  IsOptional,
  Equals,
  IsBoolean,
  MaxLength,
  ArrayNotEmpty,
  ArrayContains,
  IsMongoId,
  IsInstance,
  IsObject,
  IsIn,
  IsNotEmpty,
} from 'class-validator';
import { IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class MediaArchivo {
  @ApiProperty({ description: 'id de la media' })
  @IsOptional()
  @IsMongoId()
  _id: string;
}
export class TipoPerfil {
  @ApiProperty({
    description: 'Código catálogo tipo perfil',
    required: true,
    example: 'TIPERFIL_2 | TIPERFIL_1 ',
  })
  @IsOptional()
  codigo: string;
}
export class Idioma {
  @ApiProperty({
    description: 'Código catálogo idiomas',
    required: true,
    example: 'IDI_1 español | IDI_2 ingles ',
  })
  @IsOptional()
  codigo: string;
}
export class MetodoPago {
  @ApiProperty({
    description: 'Código catálogo método de pago',
    required: true,
    example: 'METPAG_1:stripe | METPAG_3:paypal ',
  })
  @IsOptional()
  codigo: string;
}
export class Localidad {
  @ApiProperty({
    description: 'Código catálgo Localidad',
    required: true,
    example: 'LOC_745',
  })
  @IsOptional()
  codigo: string;
}
export class Pais {
  @ApiProperty({
    description: 'Código catálogo país',
    required: true,
    example: 'PAI_31',
  })
  @IsOptional()
  codigo: string;
}
export class TipoAlbumActualizar {
  @ApiProperty({
    description: 'Código catálogo tipo album',
    required: true,
    example: 'CATALB_1 | CATALB_2',
  })
  @IsOptional()
  codigo: string;
}

export class traduccionDireccion {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  descripcion: string;
}

export class Direccion {
  @ApiProperty({ description: 'id de la direccion' })
  @IsMongoId()
  _id?: string;

  @ApiProperty()
  latitud?: number;

  @ApiProperty()
  longitud?: number;

  @ApiProperty({ type: [traduccionDireccion], required: true })
  @IsNotEmpty()
  traducciones?: Array<traduccionDireccion>;

  @ApiProperty({ type: Pais })
  @IsOptional()
  pais?: Pais;
}

export class Telefono {
  @ApiProperty({ description: 'id del telefono' })
  @IsMongoId()
  _id?: string;

  @ApiProperty()
  @IsOptional()
  numero: string;
  @ApiProperty({ type: Pais })
  @IsOptional()
  pais: Pais;
}

export class AlbumPerfil {
  @ApiProperty({ required: false })
  nombre?: string;
  @ApiProperty({ type: TipoAlbumActualizar })
  @IsOptional()
  tipo: TipoAlbumActualizar;
  @ApiProperty({ type: [MediaArchivo] })
  @IsOptional() //@ArrayNotEmpty()
  media: MediaArchivo[];
  @ApiProperty({ type: MediaArchivo })
  @IsOptional()
  portada: MediaArchivo;
}

export class ActualizarPerfilDto {
  @ApiProperty({ description: 'id del perfil' })
  @IsMongoId()
  _id: string;

  @ApiProperty()
  @IsOptional()
  nombreContacto: string;

  @ApiProperty()
  @IsOptional()
  nombreContactoTraducido: string;

  @ApiProperty()
  @IsOptional()
  nombre: string;

  @ApiProperty({ type: [Direccion] })
  @IsOptional()
  @ArrayNotEmpty()
  direcciones: Direccion[];

  @ApiProperty({ type: [Telefono] })
  @IsOptional()
  @ArrayNotEmpty()
  telefonos: Telefono[];
}
