import { ApiProperty } from '@nestjs/swagger';
import { Archivo } from '../../media/entidad/archivo-resultado.dto';
import { TipoPerfil } from './actualizar-perfil.dto';
import { TipoAlbum } from './crear-perfil.dto';

export class EstadoResultPg {
  @ApiProperty()
  codigo: string;
}

export class MediaResult {
  @ApiProperty()
  principal: Archivo;
}

export class ResultAlbumDto {
  @ApiProperty()
  _id: string;
  @ApiProperty({ type: TipoAlbum })
  tipo: TipoAlbum;
  @ApiProperty({ type: MediaResult })
  portada: MediaResult;
}

// proy not
export class TraduccionesProNot {
  @ApiProperty()
  tituloCorto: string;
}

// NOTICIA
export class ResultNoticias {
  @ApiProperty()
  _id: string;
  @ApiProperty({ type: TraduccionesProNot })
  traducciones: TraduccionesProNot;
  @ApiProperty({ type: Date })
  fechaActualizacion: Date;
  @ApiProperty({ type: ResultAlbumDto })
  adjuntos: ResultAlbumDto;
}

// PROYECTO

export class ResultAdjuntosPro {
  @ApiProperty()
  tituloCorto: string;
}

export class ResultProyectos {
  @ApiProperty()
  _id: string;
  @ApiProperty({ type: TraduccionesProNot })
  traducciones: TraduccionesProNot;
  @ApiProperty({ type: Date })
  fechaActualizacion: Date;
  @ApiProperty({ type: ResultAlbumDto })
  adjuntos: ResultAlbumDto;
}

// PENSAMIENTOS
export class TraduccionesPens {
  @ApiProperty()
  texto: string;
}

export class ResultPensamientos {
  @ApiProperty()
  _id: string;
  @ApiProperty({ type: TraduccionesPens })
  traducciones: TraduccionesPens;
  @ApiProperty({ type: Date })
  fechaActualizacion: Date;
}

// CONTACTOS
export class ResultAsocia {
  @ApiProperty()
  _id: string;
}

// perfil
export class ResultPerfilCont {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  nombreContacto: string;
  @ApiProperty({ type: ResultAlbumDto })
  album: ResultAlbumDto;
}

// participante
export class ResultParticipante {
  @ApiProperty()
  _id: string;
  @ApiProperty({ type: ResultPerfilCont })
  perfil: ResultPerfilCont;
}

export class ResultAsociaciones {
  @ApiProperty({ description: 'id del participanteAsociacion' })
  _id: string;
  @ApiProperty({})
  nombre: string;
  @ApiProperty({ type: [ResultParticipante] })
  participantes: ResultParticipante[];
}

export class ResultPerfilGeneralDto {
  @ApiProperty({ description: 'id del perfil' })
  _id: string;
  @ApiProperty()
  nombreContacto: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty({ type: TipoPerfil })
  tipoPerfil: TipoPerfil;
  @ApiProperty({ type: [ResultAlbumDto] })
  album: ResultAlbumDto[];
  @ApiProperty({ type: [ResultProyectos] })
  proyectos: ResultProyectos;
  @ApiProperty({ type: [ResultNoticias] })
  noticias: ResultNoticias;
  @ApiProperty({ type: [ResultPensamientos] })
  pensamientos: ResultPensamientos;
  @ApiProperty({ type: ResultAsociaciones })
  asociaciones: ResultAsociaciones;
}

export class ResultFotoPerfil {
  @ApiProperty()
  nombreContacto: string;
  @ApiProperty()
  album: ResultAlbumDto;
}
