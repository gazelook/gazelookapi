import { ApiProperty } from '@nestjs/swagger';

export class PerfilUsuarioDto {
  @ApiProperty()
  perfil: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  descripcion: string;
  @ApiProperty()
  idioma: string;
}

export class CoautorPerfilDto {
  @ApiProperty({
    description: 'identificador del perfil',
    example: '5f5274e6bcb45e29f3407d41',
  })
  _id: string;
  @ApiProperty({ description: 'Nombre de contacto del perfil', example: 'leo' })
  nombreContacto: string;
  @ApiProperty({ description: 'Nombre del perfil', example: 'Leonardo' })
  nombre: string;
}

export class ObtenerPerfilesSistemaDto {
  @ApiProperty({
    description: 'identificador del perfil',
    example: '5f5274e5bcb45e29f3407d3f',
  })
  _id: string;
  @ApiProperty({
    description: 'Nombre de contacto del perfil',
    example: 'vpanchojs',
  })
  nombreContacto: string;
  @ApiProperty({ description: 'Nombre del perfil', example: 'Victor Jumbo' })
  nombre: string;
}

export class ObtenerPerfilesNoticiaPerfilDto {
  @ApiProperty({
    description: 'identificador del perfil',
    example: '5f5274e5bcb45e29f3407d3f',
  })
  _id: string;
  // @ApiProperty({description:'Nombre de contacto del perfil',example:'vpanchojs'})
  // nombreContacto: string;
  @ApiProperty({
    description: 'Nombre del perfil',
    example: 'Roberto Carlos Garces',
  })
  nombre: string;
}

export class ContactoUnicoDto {
  @ApiProperty({ type: Array })
  nombreContacto: Array<any>;
}
