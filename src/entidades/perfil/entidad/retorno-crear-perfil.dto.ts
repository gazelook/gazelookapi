import {
  Matches,
  IsNotEmpty,
  IsOptional,
  Equals,
  IsBoolean,
  MaxLength,
  ArrayNotEmpty,
  ArrayContains,
  IsMongoId,
  IsInstance,
  IsObject,
  IsIn,
} from 'class-validator';
import { IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { FindValueSubscriber } from 'rxjs/internal/operators/find';

export class Media {
  @ApiProperty({ description: 'id de la media' })
  _id: string;
  @ApiProperty({ type: {} })
  principal: {
    _id: string;
    url: string;
  };
  @ApiProperty({ type: {} })
  miniatura: {
    _id: string;
    url: string;
  };
  @ApiProperty()
  enlace: string;
}
export class TipoPerfil {
  @ApiProperty({
    description: 'Código catálogo tipo perfil',
    required: true,
    example: 'TIPERFIL_2 | TIPERFIL_1 ',
  })
  codigo: string;
}
export class Idioma {
  @ApiProperty({
    description: 'Código catálogo idiomas',
    required: true,
    example: 'IDI_1 español | IDI_2 ingles ',
  })
  codigo: string;
}
export class MetodoPago {
  @ApiProperty({
    description: 'Código catálogo método de pago',
    required: true,
    example: 'METPAG_1:stripe | METPAG_3:paypal ',
  })
  @IsNotEmpty()
  codigo: string;
}
export class Localidad {
  @ApiProperty({
    description: 'Código catálgo Localidad',
    required: true,
    example: 'LOC_745',
  })
  @IsNotEmpty()
  codigo: string;
}
export class Pais {
  @ApiProperty({
    description: 'Código catálgo país',
    required: true,
    example: 'PAI_31',
  })
  @IsNotEmpty()
  codigo: string;
}
export class TipoAlbum {
  @ApiProperty({
    description: 'Código catálogo tipo album',
    required: true,
    example: 'CATALB_1 | CATALB_2',
  })
  @IsNotEmpty()
  codigo: string;
}
export class Direccion {
  @ApiProperty()
  latitud: number;
  @ApiProperty()
  longitud: number;
  @ApiProperty({ description: '', required: true })
  @IsNotEmpty()
  descripcion: string;
  @ApiProperty({ type: Localidad })
  @IsNotEmpty()
  localidad: Localidad;
}
export class Telefono {
  @ApiProperty()
  @IsNotEmpty()
  numero: string;
  @ApiProperty({ type: Pais })
  @IsNotEmpty()
  pais: Pais;
}

export class Album {
  @ApiProperty({ required: false })
  nombre?: string;
  @ApiProperty({ type: TipoAlbum })
  @IsNotEmpty()
  tipo: TipoAlbum;
  @ApiProperty({ type: [Media] })
  @IsNotEmpty()
  media: Media[];
  @ApiProperty({ type: Media })
  portada: Media;
}

export class RetornoCrearPerfilDto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  nombreContacto: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty({ type: TipoPerfil })
  tipoPerfil: TipoPerfil;
  @ApiProperty({ type: [Album] })
  album: Album[];
}
