import { ApiProperty } from '@nestjs/swagger';
import { albumPerfilSistemaDto } from 'src/entidades/album/entidad/album-dto';
import { ObtenerPerfilesSistemaDto } from './perfil-usuario.dto';

export class ObtenerPerfilesDto {
  @ApiProperty()
  codigo: string;
  @ApiProperty()
  traducciones: string;
}

export class ObtenerUsuariosSistemaDto {
  // @ApiProperty({description:'identificador del perfil',example:'5f5274e5bcb45e29f3407d3f'})
  // _id: string;
  // @ApiProperty({type:[ObtenerPerfilesSistemaDto]})
  // perfiles: [ObtenerPerfilesSistemaDto]
  @ApiProperty({
    description: 'identificador del perfil',
    example: '5f5274e5bcb45e29f3407d3f',
  })
  _id: string;
  @ApiProperty({
    description: 'Nombre de contacto del perfil',
    example: 'vpanchojs',
  })
  nombreContacto: string;
  @ApiProperty({ description: 'Nombre del perfil', example: 'Alex Pedroso' })
  nombre: string;
  @ApiProperty({ type: [albumPerfilSistemaDto] })
  album: [albumPerfilSistemaDto];
}
