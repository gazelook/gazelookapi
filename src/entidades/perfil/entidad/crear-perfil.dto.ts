import {
  Matches,
  IsNotEmpty,
  IsOptional,
  Equals,
  IsBoolean,
  MaxLength,
  ArrayNotEmpty,
  ArrayContains,
  IsMongoId,
  IsInstance,
  IsObject,
  IsIn,
} from 'class-validator';
import { IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class Media {
  @ApiProperty({ description: 'id de la media' })
  @IsNotEmpty()
  @IsMongoId()
  _id: string;
}
export class TipoPerfil {
  @ApiProperty({
    description: 'Código catálogo tipo perfil',
    required: true,
    example: 'TIPERFIL_2 | TIPERFIL_1 ',
  })
  @IsNotEmpty()
  codigo: string;
}

export class NuevaLocalidad {
  @ApiProperty({
    description: 'Código catálgo Localidad',
    required: true,
    example: 'LOC_745',
  })
  @IsNotEmpty()
  codigo: string;
}
export class Pais {
  @ApiProperty({
    description: 'Código catálgo país',
    required: true,
    example: 'PAI_31',
  })
  @IsNotEmpty()
  codigo: string;
}
export class TipoAlbum {
  @ApiProperty({
    description: 'Código catálogo tipo album',
    required: true,
    example: 'CATALB_1 | CATALB_2',
  })
  @IsNotEmpty()
  codigo: string;
}
export class NuevaDireccion {
  @ApiProperty()
  latitud: number;
  @ApiProperty()
  longitud: number;
  @ApiProperty({ description: '', required: true })
  @IsNotEmpty()
  descripcion: string;
  @ApiProperty({ type: NuevaLocalidad })
  @IsNotEmpty()
  localidad: NuevaLocalidad;
}
export class Telefono {
  @ApiProperty()
  @IsNotEmpty()
  numero: string;
  @ApiProperty({ type: Pais })
  @IsNotEmpty()
  pais: Pais;
}

export class AlbumNuevo {
  @ApiProperty({ required: false })
  nombre?: string;
  @ApiProperty({ type: TipoAlbum })
  @IsNotEmpty()
  tipo: TipoAlbum;
  @ApiProperty({ type: [Media] })
  @IsOptional()
  media: Media[];
  @IsOptional()
  @ApiProperty({ type: Media })
  portada: Media;
  @ApiProperty()
  predeterminado: boolean;
}

export class CrearPerfilDto {
  @ApiProperty()
  @IsNotEmpty()
  nombreContacto: string;
  @ApiProperty()
  @IsNotEmpty()
  nombre: string;
  @ApiProperty({ type: TipoPerfil })
  @IsNotEmpty()
  tipoPerfil: TipoPerfil;
  @ApiProperty({ type: [AlbumNuevo] })
  @IsOptional()
  album: AlbumNuevo[];
  @ApiProperty({ type: [Telefono] })
  @IsOptional() //@ArrayNotEmpty()
  telefonos: Telefono[];
  @ApiProperty({ type: [NuevaDireccion] })
  @IsNotEmpty()
  @ArrayNotEmpty()
  direcciones: NuevaDireccion[];
}

export class idPerfilDto {
  @ApiProperty({
    required: true,
    description: 'Identificador del perfil',
    example: '5f57a7a1e446d64158ef85f1',
  })
  @IsNotEmpty()
  _id: string;
}
