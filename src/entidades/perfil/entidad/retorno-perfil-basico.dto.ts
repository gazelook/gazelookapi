import {
  Matches,
  IsNotEmpty,
  IsOptional,
  Equals,
  IsBoolean,
  MaxLength,
  ArrayNotEmpty,
  ArrayContains,
  IsMongoId,
  IsInstance,
  IsObject,
  IsIn,
} from 'class-validator';
import { IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { FindValueSubscriber } from 'rxjs/internal/operators/find';
import { Media, TipoAlbum, TipoPerfil } from './crear-perfil.dto';
import { ResultPensamientos } from './perfil-general.dto';
import { MediaAlbum } from '../../album/entidad/retorno-media-album.dto';

export class AlbumPerfBasico {
  @ApiProperty({ required: false })
  nombre?: string;
  @ApiProperty({ type: TipoAlbum })
  tipo: TipoAlbum;
  @ApiProperty({ type: MediaAlbum })
  portada: MediaAlbum;
}

export class RetornoPerfilBasicoDto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  nombreContacto: string;
  @ApiProperty()
  nombre: string;
  @ApiProperty({ type: TipoPerfil })
  tipoPerfil: TipoPerfil;
  @ApiProperty({ type: [AlbumPerfBasico] })
  album: AlbumPerfBasico[];
  @ApiProperty({ type: [ResultPensamientos] })
  pensamientos: ResultPensamientos[];
}
