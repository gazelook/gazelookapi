import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CrearPerfilCuentaDto {
  @ApiProperty()
  @IsNotEmpty()
  nombreContacto: string;

  @ApiProperty()
  nombreContactoTraducido: string;

  @ApiProperty()
  @IsNotEmpty()
  nombre: string;
  @ApiProperty()
  @IsNotEmpty()
  usuario: string;
  @ApiProperty()
  @IsNotEmpty()
  tipoPerfil: string;
}
