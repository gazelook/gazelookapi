import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { Dispositivo } from '../../../drivers/mongoose/interfaces/dispositivo/dispositivo.interface';

export class EstadoPerfil {
  @ApiProperty({
    description: 'codigo del perfil',
    example: 'hibernado = EST_19 || eliminado = EST_89',
  })
  @IsNotEmpty()
  codigo: string;
}

export class EliminarPerfilDto {
  @ApiProperty({ description: 'id del perfil' })
  @IsMongoId()
  _id: string;
  @ApiProperty({ type: EstadoPerfil })
  @IsNotEmpty()
  estado: EstadoPerfil;

  dispositivo?: Dispositivo;
}
