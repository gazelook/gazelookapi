import { CatalogoIdiomasService } from './../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerIdUsuarioService } from './../../usuario/casos_de_uso/obtener-id-usuario.service';
import { Injectable, Inject, HttpStatus, forwardRef } from '@nestjs/common';
import { Model } from 'mongoose';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { TraducionTipoPerfil } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_perfil/traduccion-catalogo-tipo-perfil.interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import * as mongoose from 'mongoose';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { ObtenerCatalogoPaisService } from '../../pais/casos_de_uso/obtener-catalogo-pais.service';
import { ObtenerCatalogoLocalidadService } from '../../pais/casos_de_uso/obtener-catalogo-localidad.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import {
  estadosPerfil,
  filtroBusqueda,
  idiomas,
  codigosCatalogoTipoProyecto,
  estadosProyecto,
  codigosArchivosSistema,
  codigosCatalogoAlbum,
  codigosTiposPerfil,
  nombreEntidades,
  nombrecatalogoEstados,
  codigosEstadosPartAsociacion,
  codigosTipoAsociacion,
  estadosPartAso,
  codigoEntidades,
} from '../../../shared/enum-sistema';
import { TraduccionMediaService } from '../../media/casos_de_uso/traduccion-media.service';
import { ObtenerProyetoPerfilService } from '../../proyectos/casos_de_uso/obtener-proyecto-perfil.service';
import { ListaArchivosDefaultTipoService } from '../../archivo/casos_de_uso/lista-archivos-default-tipo.service';
import { ObtenerAlbumService } from '../../album/casos_de_uso/obtener-album.service';
import { ObtenerNoticiasPerfilService } from '../../noticia/casos_de_uso/obtener-noticias-perfil.service';
import { GetFotoPerfilService } from './obtener-album-perfil-general.service';
import { ObtenerPensamientosPerfilService } from '../../pensamientos/casos_de_uso/obtener-pensamientos-perfil.service';
import { ObtenerContactosPerfilService } from '../../asociacion_participante/casos_de_uso/obtener-contactos-perfil.service';
import { AsociacionEntrePerfilesService } from '../../asociacion_participante/casos_de_uso/asociacion-entre-perfiles.service';
import { CalcularDiasProyectoService } from 'src/entidades/proyectos/casos_de_uso/carcular-dias-proyecto.service';
import { ObtenerPortadaService } from 'src/entidades/noticia/casos_de_uso/obtener-portada-predeterminada.service';
import { ObtenerEntidadNotificacionService } from 'src/entidades/notificacion/casos_de_uso/obtener-entidad-notificacion.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from 'src/entidades/proyectos/casos_de_uso/obtener-portada-predeterminada-proyecto-resumen.service';
import {
  populateGetMediaTraduciones,
  populateGetPortadaTraduccion,
} from '../../../shared/enum-query-populate';
import { ObtenerResumenProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-resumen-proyecto.service';
import { ObtenerResumenNoticiasService } from 'src/entidades/noticia/casos_de_uso/obtener-resumen-noticia.service';

@Injectable()
export class ResumenContactoNoticiaProyectoService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private readonly catalogoIdiomasModelat: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private obtenerProyetoPerfilService: ObtenerProyetoPerfilService,
    private listaArchivosDefaultTipoService: ListaArchivosDefaultTipoService,
    private obtenerAlbumService: ObtenerAlbumService,
    private obtenerNoticiasPerfilService: ObtenerNoticiasPerfilService,
    private obtenerPensamientosPerfilService: ObtenerPensamientosPerfilService,
    private obtenerContactosPerfilService: ObtenerContactosPerfilService,
    private asociacionEntrePerfilesService: AsociacionEntrePerfilesService,
    private getFotoPerfilService: GetFotoPerfilService,
    private readonly calcularDiasProyectoService: CalcularDiasProyectoService,
    private obtenerPortada: ObtenerPortadaService,
    private obtenerEntidadNotificacionService: ObtenerEntidadNotificacionService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
    private traduccionMediaService: TraduccionMediaService,
    private obtenerResumenProyectoService: ObtenerResumenProyectoService,
    private obtenerResumenNoticiasService: ObtenerResumenNoticiasService,
  ) {}

  // retorna perfiles de usuario con noticias, pensamientos, noticias
  async obtenerResumen(idEntidad: string, codigoEntidad, idioma): Promise<any> {
    //controlar idioma
    let catalogoIdioma;
    catalogoIdioma = await this.catalogoIdiomasModelat.obtenerIdiomaByCodigoNombre(
      idioma,
    );

    // ______________________estado media____________________________
    //Obtiene el codigo de la entidad
    const entidadMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    const codEntidadMedia = entidadMedia.codigo;

    //Obtiene el estado
    const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadMedia,
    );
    const codEstadoMedia = estadoMedia.codigo;

    // ______________________estado album____________________________
    //Obtiene el codigo de la entidad
    const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.album,
    );
    const codEntidadAlbum = entidadAlbum.codigo;

    //Obtiene el estado
    const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadAlbum,
    );
    const codEstadoAlbum = estadoAlbum.codigo;

    //_____________________estado traduccion media___________________________
    //Obtiene el codigo de la entidad
    const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    const codEntidadTradMedia = entidadTradMedia.codigo;
    //Obtiene el estado
    const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradMedia,
    );
    const codEstadoTradMedia = estadoTradMedia.codigo;
    // ______________________estado perfiles____________________________
    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.perfiles,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      let opts = { session };

      //Cuando el codigo de la entidad es perfiles
      if (codigoEntidad == codigoEntidades.entidadPerfiles) {
        //_________obtener perfil ______________
        const perfil = await this.perfilModel
          .findOne({
            $and: [
              // {
              //     $or: [
              //         { estado: codEstado },
              //         { estado: estadoHibernado }
              //     ],
              // },
              { _id: idEntidad },
            ],
          })
          .select('-fechaCreacion -fechaActualizacion -__v')
          .populate([
            {
              path: 'album',
              match: {
                estado: codEstadoAlbum,
              },
              select: '-fechaCreacion -fechaActualizacion -__v',
              populate: [
                populateGetMediaTraduciones(
                  codEstadoMedia,
                  codEstadoTradMedia,
                  catalogoIdioma.codigo,
                ),
                populateGetPortadaTraduccion(
                  codEstadoTradMedia,
                  catalogoIdioma.codigo,
                ),
              ],
            },
            {
              path: 'direcciones',
              select: '-fechaCreacion -fechaActualizacion -__v',
            },
          ]);

        if (!perfil) {
          return null;
        }
        let result: any = {
          _id: perfil._id,
          nombre: perfil.nombre,
          nombreContacto: perfil.nombreContacto,
          nombreContactoTraducido: perfil?.nombreContactoTraducido || null,
          tipoPerfil: { codigo: perfil.tipoPerfil },
          estado: { codigo: perfil.estado },
        };
        // ALBUM PERFIL Y GENERAL
        const albumPerfilGeneralFoto = await this.getFotoPerfilService.getFotoPerfil(
          idEntidad,
          true,
        );
        let albumGeneral = albumPerfilGeneralFoto.objAlbumTipoGeneral;

        // Retornar las medias del album

        for (const album of perfil.album) {
          if (album['tipo'] === codigosCatalogoAlbum.catAlbumGeneral) {
            if (album['media']?.length > 0) {
              albumGeneral._id = album['_id'];
              albumGeneral.media = await this.getMediasAlbum(
                album['media'],
                idioma,
                opts,
              );
              break;
            }
          }
        }

        result.album = [
          albumPerfilGeneralFoto.objAlbumTipoPerfil,
          albumGeneral,
        ];
        await session.commitTransaction();
        await session.endSession();
        return result;
      }

      //Si el codigo de la entidad es proyectos
      if (codigoEntidad == codigoEntidades.entidadProyectos) {
        let proyecto = await this.obtenerResumenProyectoService.obtenerResumenProyecto(
          idioma,
          idEntidad,
          opts,
        );

        await session.commitTransaction();
        await session.endSession();
        return proyecto;
      }

      //Si el codigo de la entidad es noticias
      if (codigoEntidad == codigoEntidades.entidadNoticias) {
        let getNoticia = await this.obtenerResumenNoticiasService.obtenerResumenNoticia(
          idEntidad,
          idioma,
          opts,
        );

        await session.commitTransaction();
        await session.endSession();
        return getNoticia;
      }
    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      throw error;
    }
  }
  calcularDias(fechaActual, fechaActualizacion) {
    let dias =
      (fechaActual.getTime() - fechaActualizacion.getTime()) /
      (60 * 60 * 24 * 1000);
    if (dias <= 3) {
      return true;
    } else return false;
  }

  async getMediasAlbum(listaMedias, idioma, opts): Promise<any> {
    let listaMedia = [];
    for (const media of listaMedias) {
      let dataMedia: any = {
        _id: media['_id'],
      };
      if (media['principal']) {
        dataMedia.principal = {
          _id: media['principal']._id,
          url: media['principal'].url,
          duracion: media['principal']?.duracion,
          fechaActualizacion: media['principal']?.fechaActualizacion,
        };
      }
      if (media['miniatura']) {
        dataMedia.miniatura = {
          _id: media['miniatura']._id,
          url: media['miniatura'].url,
          duracion: media['principal']?.duracion,
        };
      }

      if (media['traducciones'][0]) {
        dataMedia.traducciones = [
          {
            _id: media['traducciones'][0]._id,
            descripcion: media['traducciones'][0].descripcion,
          },
        ];
      }

      //traducir descripcion de la media
      if (media['traducciones'][0] === undefined) {
        const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
          media['_id'],
          idioma,
          opts,
        );
        if (traduccionMedia) {
          dataMedia.traducciones = [
            {
              _id: traduccionMedia._id,
              descripcion: traduccionMedia.descripcion,
            },
          ];
        }
      }
      listaMedia.push(dataMedia);
    }
    return listaMedia;
  }
}
