import { Injectable, Inject } from '@nestjs/common';
import { Model, PaginateModel } from 'mongoose';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { GetFotoPerfilService } from './obtener-album-perfil-general.service';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import {
  codigoEstadosAsociacion,
  codigosTipoAsociacion,
  estadosPartAso,
  estadosPerfil,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';

@Injectable()
export class BuscarContactosPerfilService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: PaginateModel<Perfil>,
    @Inject('PARTICIPANTE_ASOCIACION_MODEL')
    private readonly participanteAsociacionModel: Model<Participante>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private getFotoPerfilService: GetFotoPerfilService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );

  async bucarContactosPerfil(
    busqueda: string,
    limite: number,
    pagina: number,
    response: any,
    idPerfil?: string,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();
    try {
      if (!busqueda) {
        return [];
      }

      let options: any;

      // const populatePerfil = ({
      //     path: 'contactoDe',
      //     select: 'nombre nombreContacto',
      //     match: {
      //         $or: [
      //             {
      //                 $or: [
      //                     {
      //                         $and: [
      //                             { nombreContacto: { $regex: busqueda, $options: 'i' } },
      //                             { estado: estadosPerfil.activa }
      //                         ]
      //                     }
      //                 ],
      //             }
      //         ]
      //     }
      // });

      // const asociacion = ({
      //     path: 'asociacion',
      //     select: 'nombre tipo estado',
      //     match: {
      //         tipo: codigosTipoAsociacion.contacto,
      //         estado: codigoEstadosAsociacion.activa
      //     }
      // })

      options = {
        lean: true,
        collation: { locale: 'en' },
        sort: { nombreContacto: 1 },
        select: 'nombre nombreContacto',
        page: Number(pagina),
        limit: Number(limite),
      };

      const getParticAsociacion = await this.participanteAsociacionModel
        .find({
          perfil: idPerfil,
          estado: estadosPartAso.contacto,
        })
        .select('contactoDe asociacion -_id')
        .populate({
          path: 'asociacion',
          select: 'nombre tipo estado',
          match: {
            tipo: codigosTipoAsociacion.contacto,
            estado: codigoEstadosAsociacion.activa,
          },
        });

      let arrayContactoDe = [];

      if (getParticAsociacion.length > 0) {
        for (const asoc of getParticAsociacion) {
          if (asoc.asociacion) {
            arrayContactoDe.push(asoc.contactoDe.toString());
          }
        }
      }

      const getAsociacion = await this.perfilModel.paginate(
        {
          $or: [
            {
              $or: [
                {
                  $and: [
                    {
                      nombreContacto: {
                        $regex: '^'.concat(busqueda),
                        $options: 'i',
                      },
                    },
                    { estado: estadosPerfil.activa },
                    { _id: { $in: arrayContactoDe } },
                  ],
                },
                // {
                //     $and: [
                //         { nombre: { $regex: "^".concat(busqueda), $options: 'i' } },
                //         { estado: estadosPerfil.activa },
                //         { _id: { $in: arrayContactoDe } }
                //     ]
                // }
              ],
            },
          ],
        },
        options,
      );

      let dataPerfiles = [];

      if (getAsociacion.docs.length > 0) {
        for (const parAsociacion of getAsociacion.docs) {
          //Llama al metodo de obtener foto de perfil
          let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
            parAsociacion._id,
            true,
          );
          let dataAlbum = [];
          let arrayAsociaciones = [];
          dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);

          const getAsociacion = await this.participanteAsociacionModel.findOne({
            perfil: idPerfil,
            contactoDe: parAsociacion._id,
            estado: estadosPartAso.contacto,
          });

          if (getAsociacion) {
            let objAsociacion = {
              _id: getAsociacion.asociacion,
            };
            arrayAsociaciones.push(objAsociacion);
          }

          //Data para el mapeo de perfil
          let objPerfil = {
            _id: parAsociacion._id,
            nombreContacto: parAsociacion.nombreContacto,
            nombreContactoTraducido: parAsociacion.nombreContactoTraducido,
            nombre: parAsociacion.nombre,
            album: dataAlbum,
            asociaciones: arrayAsociaciones,
          };
          dataPerfiles.push(objPerfil);
          // }
        }

        // dataPerfiles.sort(function (a, b) {
        //     if (a.nombreContacto > b.nombreContacto) {
        //         return 1;
        //     }
        //     if (a.nombreContacto < b.nombreContacto) {
        //         return -1;
        //     }
        //     return 0;
        // });

        response.set(headerNombre.totalDatos, getAsociacion.totalDocs);
        response.set(headerNombre.totalPaginas, getAsociacion.totalPages);
        response.set(headerNombre.proximaPagina, getAsociacion.hasNextPage);
        response.set(headerNombre.anteriorPagina, getAsociacion.hasPrevPage);

        return dataPerfiles;
      } else {
        response.set(headerNombre.totalDatos, getAsociacion.totalDocs);
        response.set(headerNombre.totalPaginas, getAsociacion.totalPages);
        response.set(headerNombre.proximaPagina, getAsociacion.hasNextPage);
        response.set(headerNombre.anteriorPagina, getAsociacion.hasPrevPage);
        return getAsociacion.docs;
      }
    } catch (error) {
      throw error;
    }
  }
}
