import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { FBNotificacion } from '../../../drivers/firebase/models/fb-notificacion.interface';
import { FirebaseNotificacionService } from '../../../drivers/firebase/services/firebase-notificacion.service';
import { erroresGeneral, erroresPerfil } from '../../../shared/enum-errores';
import {
  populateGetMediaActiva,
  populatePortada,
} from '../../../shared/enum-query-populate';
import {
  accionNotificacionFirebase,
  codigoEntidades,
  estadoNotificacionesFirebase,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
  tipoNotificacionFirebase,
} from '../../../shared/enum-sistema';
import { ActualizarAlbumService } from '../../album/casos_de_uso/actualizar-album.service';
import { ActivarParticipanteAsociacionService } from '../../asociacion_participante/casos_de_uso/activar-participante-asociacion.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { ActualizarDireccionService } from '../../direccion/casos_de_uso/actualizar-direccion.service';
import { ObtenerDispositivoUsuarioService } from '../../dispositivos/casos_de_uso/obtener-dispositivo-usuario.service';
import { ActualizarNoticiaUnicaService } from '../../noticia/casos_de_uso/actualizar-noticia-unica.service';
import { ActualizacionPensamientoService } from '../../pensamientos/casos_de_uso/actualizacion-pensamiento.service';
import { ActualizarProyectoService } from '../../proyectos/casos_de_uso/actualizar-proyecto.service';
import { ObtenerProyetoPerfilService } from '../../proyectos/casos_de_uso/obtener-proyecto-perfil.service';
@Injectable()
export class ActivarPerfilService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('TIPO_PERFIL_MODEL')
    private readonly tipoPerfilModelo: Model<TipoPerfil>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private actualizarAlbumService: ActualizarAlbumService,
    private actualizarDireccionService: ActualizarDireccionService,
    private actualizarProyectoService: ActualizarProyectoService,
    private actualizacionPensamientoService: ActualizacionPensamientoService,
    private actualizarNoticiaUnicaService: ActualizarNoticiaUnicaService,
    private obtenerProyetoPerfilService: ObtenerProyetoPerfilService,
    private activarParticipanteAsociacionService: ActivarParticipanteAsociacionService,
    private firebaseNotificacionService: FirebaseNotificacionService,
    private obtenerDispositivoUsuarioService: ObtenerDispositivoUsuarioService,
  ) {}

  async activarPerfil(idPerfil, idioma, dispositivo: any): Promise<any> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const opts = { session };

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.perfiles,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      //______verificar perfil__________
      const existePerfil: any = await this.perfilModel.findOne({
        _id: idPerfil,
      });
      if (!existePerfil) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresPerfil.PERFIL_NO_EXISTE,
        };
      }

      if (existePerfil.usuario.toString() !== dispositivo.usuario.toString()) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }

      const datosPerfil = await this.allDatosPerfilActualizarEstado(idPerfil);

      // _________________album_____________________
      if (datosPerfil.album) {
        for (const albm of datosPerfil.album) {
          const dataAlbum = {
            _id: albm['_id'],
            perfil: existePerfil._id,
          };
          await this.actualizarAlbumService.activarAlbum(dataAlbum, opts);
        }
      }

      // _________________telefono_____________________
      if (datosPerfil.telefonos) {
      }

      // _________________direcciones_____________________
      if (datosPerfil.direcciones) {
        for (const telefono of datosPerfil.telefonos) {
          const dataDireccion = {
            _id: telefono['_id'],
            usuario: existePerfil._id,
          };
          await this.actualizarDireccionService.activarDireccion(
            dataDireccion,
            opts,
          );
        }
      }

      // _________________proyectos_____________________
      const getProyectos = await this.obtenerProyetoPerfilService.obtenerProyectosPerfil(
        idPerfil,
        idioma,
      );
      if (getProyectos) {
        for (const proyecto of getProyectos) {
          await this.actualizarProyectoService.activarProyecto(
            idPerfil,
            proyecto['_id'],
            opts,
          );
        }
      }
      // _________________pensamientos_____________________
      await this.actualizacionPensamientoService.activarPensamiento(
        idPerfil,
        opts,
      );

      // _________________noticias_____________________
      await this.actualizarNoticiaUnicaService.activarNoticiaUnica(
        idPerfil,
        opts,
      );
      // _________________participante asociacion_____________________
      await this.activarParticipanteAsociacionService.activarParticipante(
        idPerfil,
        opts,
      );

      // _______________perfil___________________
      const dataPerfil = {
        estado: codEstado,
      };

      await this.perfilModel.findByIdAndUpdate(idPerfil, dataPerfil, opts);
      const updatePerfil = await this.perfilModel
        .findById(idPerfil)
        .session(opts.session);
      // _______________historico___________________
      const dataPerfilHistorico = JSON.parse(JSON.stringify(updatePerfil));
      //eliminar parametro no necesarios
      delete dataPerfilHistorico.fechaCreacion;
      delete dataPerfilHistorico.fechaActualizacion;
      delete dataPerfilHistorico.__v;

      const dataHistoricoPerfil: any = {
        datos: dataPerfilHistorico,
        usuario: existePerfil.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoPerfil);

      // notificar cambio de estado de perfil
      const dataPerfilNotificar = {
        _id: updatePerfil._id.toString(),
        nombre: updatePerfil.nombre,
        nombreContacto: updatePerfil.nombreContacto,
        estado: { codigo: updatePerfil.estado },
        usuario: {
          _id: updatePerfil.usuario.toString(),
        },
      };

      // lista de dispositivos
      let dataDispNotif,
        listDispositivos = [];
      const getDispositivos: any = await this.obtenerDispositivoUsuarioService.getDispositivosUsuario(
        existePerfil.usuario.toString(),
      );
      for (const disp of getDispositivos) {
        if (disp._id.toString() === dispositivo._id.toString()) {
          dataDispNotif = {
            idDispositivo: disp._id.toString(),
            leido: true,
          };
        } else {
          dataDispNotif = {
            idDispositivo: disp._id.toString(),
            leido: false,
          };
        }
        listDispositivos = [...listDispositivos, dataDispNotif];
      }

      const dataNotificacionPerfil: FBNotificacion = {
        codEntidad: codigoEntidades.entidadPerfiles,
        idEntidad: updatePerfil._id.toString(),
        data: dataPerfilNotificar,
        leido: false,
        estado: estadoNotificacionesFirebase.activa,
        accion: accionNotificacionFirebase.ver,
        tipo: tipoNotificacionFirebase.sistema,
        query: `${codigoEntidades.entidadPerfiles}-${false}`,
        dispositivos: listDispositivos,
      };

      this.firebaseNotificacionService.createDataNotificacion(
        dataNotificacionPerfil,
        codigoEntidades.entidadUsuarios,
        updatePerfil.usuario.toString(),
      );

      // termina el proceso
      await session.commitTransaction();
      session.endSession();

      return true;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }

  async allDatosPerfilActualizarEstado(idPerfil) {
    const perfil = await this.perfilModel
      .findOne({ _id: idPerfil })
      .select('-fechaCreacion -fechaActualizacion -__v')
      .populate([
        {
          path: 'album',
          select: '-fechaCreacion -fechaActualizacion -__v',
          populate: [populateGetMediaActiva, populatePortada],
        },
        {
          path: 'direcciones',
          select: 'estado',
        },
        {
          path: 'telefonos',
          select: 'estado',
        },
      ]);
    return perfil;
  }
}
