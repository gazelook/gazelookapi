import { CatalogoIdiomasService } from './../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerIdUsuarioService } from './../../usuario/casos_de_uso/obtener-id-usuario.service';
import { Injectable, Inject, HttpStatus, forwardRef } from '@nestjs/common';
import { Model } from 'mongoose';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { TraducionTipoPerfil } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_perfil/traduccion-catalogo-tipo-perfil.interface';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import * as mongoose from 'mongoose';
import { CatalogoIdiomas } from 'src/drivers/mongoose/interfaces/catalogo_idiomas/catalogo-idiomas.interface';
import { ObtenerDatosUsuarioService } from '../../usuario/casos_de_uso/obtener-datos-usuario.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { ObtenerCatalogoPaisService } from '../../pais/casos_de_uso/obtener-catalogo-pais.service';
import { ObtenerCatalogoLocalidadService } from '../../pais/casos_de_uso/obtener-catalogo-localidad.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import {
  codidosEstadosTraduccionDireccion,
  codigosHibernadoEntidades,
  estadosPerfil,
  idiomas,
  nombrecatalogoEstados,
  nombreEntidades,
} from '../../../shared/enum-sistema';
import { TraduccionMediaService } from '../../media/casos_de_uso/traduccion-media.service';
import { erroresPerfil } from '../../../shared/enum-errores';
import {
  populateGetMediaTraduciones,
  populateGetPortadaTraduccion,
} from '../../../shared/enum-query-populate';

@Injectable()
export class ObtenerAllDatosPerfilService {
  constructor(
    @Inject('TIPO_PERFIL_MODEL')
    private readonly catalogoTipoPerfilModelo: Model<TipoPerfil>,
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('TRADUCCION_TIPO_PERFIL_MODEL')
    private readonly traduccionTipoPerfilModelo: Model<TraducionTipoPerfil>,
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private readonly catalogoIdiomasModelat: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private obtenerCatalogoPaisService: ObtenerCatalogoPaisService,
    private obtenerCatalogoLocalidadService: ObtenerCatalogoLocalidadService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traduccionMediaService: TraduccionMediaService,
  ) {}

  async obtenerAllDatosPerfil(idPerfil: string, idioma): Promise<any> {
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      // ______________________estado media____________________________
      //Obtiene el codigo de la entidad
      const entidadMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      const codEntidadMedia = entidadMedia['codigo'];

      //Obtiene el estado
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      const codEstadoMedia = estadoMedia['codigo'];
      //_____________________estado traduccion media___________________________
      //Obtiene el codigo de la entidad
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      const codEntidadTradMedia = entidadTradMedia['codigo'];
      //Obtiene el estado
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      const codEstadoTradMedia = estadoTradMedia['codigo'];
      // ______________________estado perfiles____________________________
      const catalogoIdioma = await this.catalogoIdiomasModelat.obtenerIdiomaByCodigoNombre(
        idioma,
      );
      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.perfiles,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      //Obtiene l codigo de la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activo de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      let estadoMediaCheck;
      const checkPerfil = await this.perfilModel.findById(idPerfil);

      estadoMediaCheck =
        checkPerfil.estado === estadosPerfil.hibernado
          ? codigosHibernadoEntidades.mediaHibernado
          : codEstadoMedia;
      console.log('estadoMediaCheck', estadoMediaCheck);

      //_________obtener perfil______________
      const perfil = await this.perfilModel
        .findOne({
          $and: [
            {
              $or: [{ estado: codEstado }, { estado: estadosPerfil.hibernado }],
            },
            { _id: idPerfil },
          ],
        })
        .select('-fechaCreacion -fechaActualizacion -__v')
        .populate([
          {
            path: 'album',
            match: {
              $or: [
                { estado: codEstadoAlbum },
                { estado: codigosHibernadoEntidades.albumHibernado },
              ],
            },
            select: '-fechaCreacion -fechaActualizacion -__v',
            populate: [
              populateGetMediaTraduciones(
                estadoMediaCheck,
                codEstadoTradMedia,
                catalogoIdioma.codigo,
              ),
              populateGetPortadaTraduccion(
                codEstadoTradMedia,
                catalogoIdioma.codigo,
              ),
            ],
          },
          {
            path: 'direcciones',
            select: '-fechaCreacion -fechaActualizacion -__v',
            populate: {
              path: 'traducciones',
              select: 'descripcion traducciones idioma original estado',
              match: {
                estado: codidosEstadosTraduccionDireccion.activa,
                original: true,
              },
            },
          },
          {
            path: 'telefonos',
            select: '-fechaCreacion -fechaActualizacion -__v',
          },
        ]);
      let listaAlbum: any = [];
      let listaMedia: any = [];
      const listaDirecciones = [];
      const listaTelefonos = [];
      if (!perfil) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresPerfil.PERFIL_NO_EXISTE,
        };
      }
      let dataAlbum: any = {};
      let result: any = {
        _id: perfil._id,
        nombre: perfil.nombre,
        nombreContacto: perfil.nombreContacto,
        nombreContactoTraducido: perfil?.nombreContactoTraducido || null,
        tipoPerfil: { codigo: perfil.tipoPerfil },
        estado: { codigo: perfil.estado },
      };
      console.log('1111111111111111111111111111111');
      // datos album
      if (perfil.album) {
        for (let album of perfil.album) {
          dataAlbum = {};
          dataAlbum._id = album['_id'];
          dataAlbum.tipo = { codigo: album['tipo'] };

          if (album['portada']) {
            dataAlbum.portada = {
              _id: album['portada']._id,
              principal: {
                _id: album['portada'].principal._id,
                url: album['portada'].principal.url,
                duracion: album['portada'].principal?.duracion,
                fechaActualizacion:
                  album['portada'].principal?.fechaActualizacion,
              },
            };

            // verificar si hay traduccion
            if (album['portada'].traducciones[0]) {
              const traducciones = [
                {
                  _id: album['portada'].traducciones._id,
                  descripcion: album['portada'].traducciones.descripcion,
                },
              ];
              dataAlbum.portada.traducciones = traducciones;
            }

            //traducir descripcion de la media
            if (album['portada'].traducciones[0] === undefined) {
              const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                album['portada']._id,
                idioma,
                opts,
              );
              if (traduccionMedia) {
                const traducciones = [
                  {
                    _id: traduccionMedia._id,
                    descripcion: traduccionMedia.descripcion,
                  },
                ];
                dataAlbum.portada.traducciones = traducciones;
              }
            }
          }
          //datos media
          if (album['media']) {
            listaMedia = [];
            for (const media of album['media']) {
              let dataMedia: any = {
                _id: media['_id'],
              };
              if (media['principal']) {
                dataMedia.principal = {
                  _id: media['principal']._id,
                  url: media['principal'].url,
                  duracion: media['principal']?.duracion,
                  fechaActualizacion: media['principal']?.fechaActualizacion,
                };
              }
              if (media['miniatura']) {
                dataMedia.miniatura = {
                  _id: media['miniatura']._id,
                  url: media['miniatura'].url,
                  duracion: media['principal']?.duracion,
                };
              }

              if (media['traducciones'][0]) {
                dataMedia.traducciones = [
                  {
                    _id: media['traducciones'][0]._id,
                    descripcion: media['traducciones'][0].descripcion,
                  },
                ];
              }

              //traducir descripcion de la media
              if (media['traducciones'][0] === undefined) {
                const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                  media['_id'],
                  idioma,
                  opts,
                );
                if (traduccionMedia) {
                  dataMedia.traducciones = [
                    {
                      _id: traduccionMedia._id,
                      descripcion: traduccionMedia.descripcion,
                    },
                  ];
                }
              }
              listaMedia.push(dataMedia);
            }
            dataAlbum.media = listaMedia;
          }
          listaAlbum.push(dataAlbum);
        }
      }

      // datos direcciones
      if (perfil.direcciones) {
        for (const direccion of perfil.direcciones) {
          //const localidad = await this.obtenerCatalogoLocalidadService.obtenerLocalidad(direccion['localidad']);
          //const pais = await this.obtenerCatalogoPaisService.obtenerPaisByCodigo(localidad.catalogoPais, idioma)
          const pais = await this.obtenerCatalogoPaisService.obtenerPaisByCodigo(
            direccion['pais'],
            idioma,
          );

          const dataDirecciones = {
            _id: direccion['_id'],
            traducciones: direccion['traducciones'],
            pais: {
              codigo: pais?.codigo,
              traducciones: [{ nombre: pais?.traducciones[0]?.nombre }],
            },
            /* localidad: {
                            codigo: localidad.codigo,
                            nombre: localidad.nombre,
                            codigoPostal: localidad.codigoPostal,
                            catalogoPais: {
                                codigo: pais.codigo,
                                traducciones: [{ nombre: pais.traducciones[0].nombre }]
                            }
                        } */
          };
          listaDirecciones.push(dataDirecciones);
        }
        result.direcciones = listaDirecciones;
      }

      if (perfil.telefonos) {
        for (const telefono of perfil.telefonos) {
          const pais = await this.obtenerCatalogoPaisService.obtenerPaisByCodigo(
            telefono['pais'],
            idioma,
          );

          const dataTelefono = {
            _id: telefono['_id'],
            numero: telefono['numero'],
            pais: {
              codigo: pais.codigo,
              codigoTelefono: pais.codigoTelefono,
              codigoNombre: pais.codigoNombre,
              traducciones: [
                {
                  nombre: pais.traducciones[0].nombre,
                },
              ],
            },
          };
          listaTelefonos.push(dataTelefono);
        }
        result.telefonos = listaTelefonos;
      }

      result.album = listaAlbum;
      await session.commitTransaction();
      await session.endSession();

      return result;
    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      throw error;
    }
  }
}
