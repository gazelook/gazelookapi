import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { CrearPerfilService } from './crear-perfil.service';
import { CrearAlbumService } from '../../album/casos_de_uso/crear-album.service';
import { CrearDireccionService } from '../../direccion/casos_de_uso/crear-direccion.service';
import { CrearTelefonoService } from '../../direccion/casos_de_uso/crear-telefono.service';
import * as mongoose from 'mongoose';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { ObtenerPerfilUsuarioService } from './obtener-perfil-usuario.service';
import { estadoLocalidad, estadosPerfil } from '../../../shared/enum-sistema';
import { ObtenerAllDatosPerfilService } from './obtener-all-datos-perfil.service';
import { erroresPerfil } from '../../../shared/enum-errores';
import { CrearLocalidadService } from '../../pais/casos_de_uso/crear-localidad.service';
import { paisProviders } from '../../pais/drivers/pais.provider';
@Injectable()
export class NuevoPerfilService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('TIPO_PERFIL_MODEL')
    private readonly tipoPerfilModelo: Model<TipoPerfil>,
    private crearPerfilService: CrearPerfilService,
    private crearAlbumService: CrearAlbumService,
    private crearDireccionService: CrearDireccionService,
    private crearTelefonoService: CrearTelefonoService,
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private obtenerAllDatosPerfilService: ObtenerAllDatosPerfilService,
    private crearLocalidadService: CrearLocalidadService,
  ) {}

  async nuevoPerfil(
    idUsuario: string,
    data: any,
    idioma: string,
    dispositivo: any,
  ): Promise<any> {
    let saveAlbum;
    let idAlbunes = [];
    const idPerfiles = [];
    const idDireccion = [];
    const idTelefono = [];
    let savePerfil;
    let saveDireccion;
    let saveTelefono;

    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      const opts = { session };

      //______verificar usuario__________
      // obtener perfiles de usuario activos
      const user = await this.obtenerIdUsuarioService.obtenerPerfilesActivosFromUsuarioById(
        idUsuario,
      );

      if (!user) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresPerfil.USUARIO_NO_REGISTRADO,
        };
      }

      //__________verificar perfil_____________________
      //const perfilesUsuario: [] = await this.obtenerPerfilUsuarioService.obtenerPerfilesUsuario(idUsuario)
      const listaTipoPerfil = [];
      listaTipoPerfil.push(data.tipoPerfil.codigo);
      user.perfiles.forEach((element: any) => {
        listaTipoPerfil.push(element.tipoPerfil);
      });

      const perfilUnico = await this.crearPerfilService.verificarPerfilUnico(
        listaTipoPerfil,
      );

      if (!perfilUnico) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresPerfil.PERFIL_NO_VALIDO,
        };
      }
      //_______verificar nombre contacto_________
      const existeNombreContacto: any = await this.perfilModel.findOne({
        $and: [
          {
            $or: [
              { estado: estadosPerfil.activa },
              { estado: estadosPerfil.hibernado },
            ],
          },
          { nombreContacto: data.nombreContacto },
        ],
      });
      //  nombreContacto: data.nombreContacto });
      if (existeNombreContacto) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresPerfil.ERROR_NOMBRE_CONTACTO,
        };
      }

      const idPerfil = new mongoose.mongo.ObjectId();

      //______________album______________________
      if (data.album) {
        const listaAlbum = data.album;
        for (const album of listaAlbum) {
          const dataAlbum = {
            nombre: album.nombre, // para guardar en la traduccion del album
            tipo: album.tipo.codigo,
            media: album.media,
            portada: album.portada,
            usuario: idPerfil,
            idioma: idioma,
          };

          // guardar album
          saveAlbum = await this.crearAlbumService.crearAlbum(dataAlbum, opts);
          idAlbunes.push(saveAlbum._id);
        }
      }
      //datos guardar telefonos
      // _________________direccion_____________________
      const listaDireccion = data.direcciones;
      for (const direccionPerfil of listaDireccion) {
        let dataDireccionPerfil;

        if (
          direccionPerfil.localidad?.estado?.codigo ===
          estadoLocalidad.enRevision
        ) {
          const nuevaLocalidad = await this.crearLocalidadService.crearLocalidad(
            direccionPerfil.localidad.nombre,
            direccionPerfil.localidad.catalogoPais.codigo,
            idPerfil,
            opts,
          );
          dataDireccionPerfil = {
            latitud: null,
            longitud: null,
            traducciones: direccionPerfil.traducciones,
            localidad: nuevaLocalidad.codigo,
            usuario: idPerfil,
          };
        } else {
          dataDireccionPerfil = {
            latitud: null,
            longitud: null,
            traducciones: direccionPerfil.traducciones,
            //localidad: direccionPerfil.localidad.codigo,
            pais: direccionPerfil?.pais?.codigo,
            usuario: idPerfil,
          };
        }
        saveDireccion = await this.crearDireccionService.crearDireccion(
          dataDireccionPerfil,
          opts,
        );
        idDireccion.push(saveDireccion._id);
      }

      // _______________telefono____________________
      if (data.telefonos) {
        const listaTelefono = data.telefonos;

        for (let telefono of listaTelefono) {
          const dataTelefono = {
            numero: telefono.numero,
            pais: telefono.pais.codigo,
            usuario: idPerfil,
          };

          saveTelefono = await this.crearTelefonoService.crearTelefono(
            dataTelefono,
            opts,
          );
          idTelefono.push(saveTelefono._id);
        }
      }
      //datos guardar direcciones

      // datos para guardar el perfil
      const dataPerfil = {
        _id: idPerfil,
        nombreContacto: data.nombreContacto,
        nombreContactoTraducido: data.nombreContactoTraducido,
        nombre: data.nombre,
        tipoPerfil: data.tipoPerfil.codigo,
        album: idAlbunes,
        direcciones: idDireccion,
        telefonos: idTelefono,
        usuario: idUsuario,
      };
      savePerfil = await this.crearPerfilService.crearPerfil(
        dataPerfil,
        dispositivo,
        opts,
      );
      await this.actualizarUsuarioService.agregarPerfilUsuario(
        idUsuario,
        savePerfil._id,
        opts,
      );

      // termina el proceso
      await session.commitTransaction();
      session.endSession();
      const result = await this.obtenerAllDatosPerfilService.obtenerAllDatosPerfil(
        savePerfil._id,
        idioma,
      );
      return result;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
