import { UsuarioServicesModule } from './../../usuario/casos_de_uso/usuario-services.module';
import { Module, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CrearPerfilService } from './crear-perfil.service';
import { PerfilProviders } from '../drivers/perfil.provider';
import { CatalogosServiceModule } from './../../catalogos/casos_de_uso/catalogos-services.module';
import { ObtenerPerfilUsuarioService } from './obtener-perfil-usuario.service';
import { ObtenerPerfilesService } from './obtener-perfiles.service';
import { NuevoPerfilService } from './nuevo-perfil.service';
import { AlbumServiceModule } from '../../album/casos_de_uso/album.services.module';
import { EliminarPerfilService } from './eliminar-perfil.service';
import { DireccionServiceModule } from '../../direccion/casos_de_uso/direccion.services.module';
import { ActualizarPerfilService } from './actualizar-perfil.service';
import { BuscarPerfilesService } from './buscar-perfiles.service';
import { PaisServicesModule } from '../../pais/casos_de_uso/pais.services.module';
import { ProyectosServicesModule } from '../../proyectos/casos_de_uso/proyectos-services.module';
import { PensamientoServicesModule } from '../../pensamientos/casos_de_uso/pensamientos-services.module';
import { NoticiaServicesModule } from '../../noticia/casos_de_uso/noticia-services.module';
import { AsociacionServicesModule } from '../../asociaciones/casos_de_uso/asociacion-services.module';
import { ContactnameUnicoPerfilService } from './contactname-unico-perfil.service';
import { ActivarPerfilService } from './activar-perfil.service';
import { MediaServiceModule } from '../../media/casos_de_uso/media.services.module';
import { AgregarReferenciaPerfilService } from './agregar-referencia-perfil.service';
import { ListaArchivosDefaultTipoService } from 'src/entidades/archivo/casos_de_uso/lista-archivos-default-tipo.service';
import { ArchivoServicesModule } from 'src/entidades/archivo/casos_de_uso/archivo-services.module';
import { GetFotoPerfilService } from './obtener-album-perfil-general.service';
import { ParticipanteAsociacionServicesModule } from '../../asociacion_participante/casos_de_uso/participante-asociacion-services.module';
import { ObtenerPerfilGeneralService } from './obtener-perfil-general.service';
import { EliminarPerfilCompletoService } from './eliminar-perfil-completo.service';
import { ObtenerPerfilBasicoService } from './obtener-perfil-basico.service';
import { ObtenerAllDatosPerfilService } from './obtener-all-datos-perfil.service';
import { NotificacionServicesModule } from 'src/entidades/notificacion/casos_de_uso/notificacion-services.module';
import { ResumenContactoNoticiaProyectoService } from './obtener-resumen-contacto-noticia-proyecto.service';
import { FirebaseModule } from '../../../drivers/firebase/firebase.module';
import { ObtenerAlbumPerfilService } from './obtener-album-perfil.service';
import { DispositivoServicesModule } from '../../dispositivos/casos_de_uso/dispositivo-services.module';
import { BuscarContactosPerfilService } from './buscar-contactos-perfil.service';
import { IntercambioServicesModule } from 'src/entidades/intercambio/casos_de_uso/intercambio-services.module';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    forwardRef(() => UsuarioServicesModule),
    AlbumServiceModule,
    forwardRef(() => DireccionServiceModule),
    PaisServicesModule,
    DireccionServiceModule,
    forwardRef(() => ProyectosServicesModule),
    NoticiaServicesModule,
    AsociacionServicesModule,
    forwardRef(() => MediaServiceModule),
    forwardRef(() => ArchivoServicesModule),
    forwardRef(() => ParticipanteAsociacionServicesModule),
    forwardRef(() => PensamientoServicesModule),
    forwardRef(() => NotificacionServicesModule),
    forwardRef(() => IntercambioServicesModule),
    FirebaseModule,
    DispositivoServicesModule,
  ],
  providers: [
    ...PerfilProviders,
    CrearPerfilService,
    ObtenerPerfilesService,
    ObtenerPerfilUsuarioService,
    NuevoPerfilService,
    ActualizarPerfilService,
    EliminarPerfilService,
    BuscarPerfilesService,
    ContactnameUnicoPerfilService,
    ActivarPerfilService,
    AgregarReferenciaPerfilService,
    GetFotoPerfilService,
    ObtenerPerfilGeneralService,
    EliminarPerfilCompletoService,
    ObtenerPerfilBasicoService,
    ObtenerAllDatosPerfilService,
    ResumenContactoNoticiaProyectoService,
    ObtenerAlbumPerfilService,
    BuscarContactosPerfilService,
  ],
  exports: [
    CrearPerfilService,
    ObtenerPerfilesService,
    ObtenerPerfilUsuarioService,
    NuevoPerfilService,
    ActualizarPerfilService,
    EliminarPerfilService,
    BuscarPerfilesService,
    ContactnameUnicoPerfilService,
    ActivarPerfilService,
    AgregarReferenciaPerfilService,
    GetFotoPerfilService,
    ObtenerPerfilGeneralService,
    EliminarPerfilCompletoService,
    ObtenerPerfilBasicoService,
    ObtenerAllDatosPerfilService,
    ResumenContactoNoticiaProyectoService,
    ObtenerAlbumPerfilService,
    BuscarContactosPerfilService,
  ],
  controllers: [],
})
export class PerfilServiceModule {}
