import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { ObtenerProyetoPerfilService } from '../../proyectos/casos_de_uso/obtener-proyecto-perfil.service';
import { EliminarAlbumCompletoService } from '../../album/casos_de_uso/eliminar-album-completo.service';
import { EliminarDireccionCompletoService } from '../../direccion/casos_de_uso/eliminar-direccion-completo.service';
import { EliminarProyectoCompletoService } from 'src/entidades/proyectos/casos_de_uso/eliminar-proyecto-completo.service';
import { EliminarPensamientoCompletoService } from '../../pensamientos/casos_de_uso/eliminar-pensamiento-completo.service';
import { EliminarParticipanteAsociacionCompletoService } from '../../asociacion_participante/casos_de_uso/eliminar-participante-asociacion-completo.service';
import { EliminarNoticiaCompletaService } from 'src/entidades/noticia/casos_de_uso/eliminar-noticia-completa.service';
import { populateGetMedia } from '../../../shared/enum-query-populate';
import { EliminarTelefonoCompletoService } from '../../direccion/casos_de_uso/eliminar-telefono-completo.service';

@Injectable()
export class EliminarPerfilCompletoService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('TIPO_PERFIL_MODEL')
    private readonly tipoPerfilModelo: Model<TipoPerfil>,
    private obtenerProyetoPerfilService: ObtenerProyetoPerfilService,
    private eliminarAlbumCompletoService: EliminarAlbumCompletoService,
    private eliminarDireccionCompletoService: EliminarDireccionCompletoService,
    private eliminarProyectoCompletoService: EliminarProyectoCompletoService,
    private eliminarPensamientoCompletoService: EliminarPensamientoCompletoService,
    private eliminarParticipanteAsociacionCompletoService: EliminarParticipanteAsociacionCompletoService,
    private eliminarNoticiaCompletaService: EliminarNoticiaCompletaService,
    private eliminarTelefonoCompletoService: EliminarTelefonoCompletoService,
  ) {}

  async eliminarPerfilCompleto(usuario, opts): Promise<any> {
    try {
      //Obtiene el codigo de la accion a realizarse
      // const accion = await this.catalogoAccionService.obtenerNombreAccion('eliminar');

      //Obtiene el codigo de la entidad
      //const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(nombreEntidades.perfiles);
      //Obtiene el estado
      //const estado = await this.catalogoEstadoService.obtenerNombreEstado(nombrecatalogoEstados.activa, entidad.codigo);

      const perfiles = await this.getPerilesEliminar(usuario);

      const listaProyectosEliminar = [];

      for (const iterPerfil of perfiles) {
        // ___________ verificar proyecto __________
        const checkEstadoProyecto = await this.obtenerProyetoPerfilService.obtenerProyectoNoEliminar(
          iterPerfil._id,
        );

        if (checkEstadoProyecto.length > 0) {
          console.error('Operacion No Permitida: Proyecto en ejecución');
          throw {
            message: 'Operacion No Permitida: Proyectos activos o en ejecución',
          };
        }
      }

      for (const iterPerfil of perfiles) {
        console.log(
          '*******************************************************************',
        );
        console.log('ID PERFIL: ', iterPerfil._id);

        // console.log("perfiles....", perfiles);

        // _________________album_____________________
        if (iterPerfil.album.length > 0) {
          console.log('NUMERO DE ALBUNES: ', iterPerfil.album.length);
          for (const albm of iterPerfil.album) {
            await this.eliminarAlbumCompletoService.eliminarAlbumCompleto(
              albm['_id'],
              opts,
            );
          }
        }

        // _________________telefono_____________________
        if (iterPerfil.telefonos.length > 0) {
          for (const telefono of iterPerfil.telefonos) {
            await this.eliminarTelefonoCompletoService.eliminarTelefonoCompleto(
              telefono['_id'],
              opts,
            );
          }
        }

        // _________________direcciones_____________________
        if (iterPerfil.direcciones.length > 0) {
          for (const direccion of iterPerfil.direcciones) {
            await this.eliminarDireccionCompletoService.eliminarDireccionCompleto(
              direccion['_id'],
              opts,
            );
          }
        }

        // _________________proyectos_____________________
        const eliminarProyectos = await this.eliminarProyectoCompletoService.eliminarProyectoCompleto(
          iterPerfil._id,
          opts,
        );
        listaProyectosEliminar.push(eliminarProyectos);

        console.log('proyectos');
        // _________________pensamientos_____________________
        await this.eliminarPensamientoCompletoService.eliminarPensamientoCompleto(
          iterPerfil._id,
          opts,
        );

        //____________________mensajes, asociacion, participante_________________________
        await this.eliminarParticipanteAsociacionCompletoService.eliminarParticipanteAsocCompleto(
          iterPerfil._id,
          opts,
        );

        // _________________noticias_____________________

        const eliminarNoticias = await this.eliminarNoticiaCompletaService.eliminarNoticiaCompleta(
          iterPerfil._id,
          opts,
        );
      }

      // eliminar todos los perfiles del usuario
      await this.perfilModel.deleteMany({ usuario: usuario }, opts);

      return listaProyectosEliminar;
    } catch (error) {
      throw error;
    }
  }

  async getPerilesEliminar(idUsuario) {
    const perfiles = await this.perfilModel
      .find({ usuario: idUsuario })
      .select('-fechaCreacion -fechaActualizacion -__v')
      .populate([
        {
          path: 'album',
          select: '-fechaCreacion -fechaActualizacion -__v',
          // match:codigoEstadoAlbum.activa,
          populate: [populateGetMedia],
        },
        {
          path: 'direcciones',
          select: 'estado',
        },
        {
          path: 'telefonos',
          select: 'estado',
        },
      ]);
    return perfiles;
  }
}
