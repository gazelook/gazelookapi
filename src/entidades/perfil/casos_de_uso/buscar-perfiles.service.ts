import { Inject, Injectable } from '@nestjs/common';
import { Model, PaginateModel } from 'mongoose';
import { Participante } from 'src/drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import {
  codigoEstadosAsociacion,
  codigosCatalogoRol,
  codigosTipoAsociacion,
  estadosPartAso, estadosUsuario,
  nombrecatalogoEstados,
  nombreEntidades
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { GetFotoPerfilService } from './obtener-album-perfil-general.service';
// import utf8 from 'utf8';

const utf8 = require('utf8');

@Injectable()
export class BuscarPerfilesService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: PaginateModel<Perfil>,
    @Inject('PARTICIPANTE_ASOCIACION_MODEL')
    private readonly participanteAsociacionModel: Model<Participante>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private getFotoPerfilService: GetFotoPerfilService,
  ) { }

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );

  async bucarPerfilSistema(
    busqueda: string,
    limite: number,
    pagina: number,
    response: any,
    idPerfil?: string,
  ): Promise<any> {

    const headerNombre = new HadersInterfaceNombres();
    try {
      if (!busqueda) {
        return [];
      }
      const catalogoEstadoEntidadPerfilActiva = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.perfiles,
        nombrecatalogoEstados.activa,
      );
      const codEstadoPerfiles =
        catalogoEstadoEntidadPerfilActiva.catalogoEstado.codigo;

      let options: any;

      options = {
        lean: true,
        collation: { locale: 'en' },
        sort: { nombreContacto: 1 },
        select:
          'nombre nombreContacto nombreContactoTraducido tipoPerfil album asociaciones',
        // populate: [
        //     populateAsociaciones
        // ],
        page: Number(pagina),
        limit: Number(limite),
      };

      const getPerfiles = await this.perfilModel.find(
        {
          $or: [
            {
              $or: [
                {
                  $and: [
                    {
                      nombreContacto: {
                        $regex: '\\b'.concat(
                          this.funcion.diacriticSensitiveRegex(busqueda),
                        ),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
                {
                  $and: [
                    {
                      nombreContacto: {
                        $regex: '\\b'
                          .concat(
                            this.funcion.diacriticSensitiveRegex(busqueda),
                          )
                          .concat('\\b'),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
                {
                  $and: [
                    {
                      nombreContactoTraducido: {
                        $regex: '\\b'.concat(
                          this.funcion.diacriticSensitiveRegex(busqueda),
                        ),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
                {
                  $and: [
                    {
                      nombreContactoTraducido: {
                        $regex: '\\b'
                          .concat(
                            this.funcion.diacriticSensitiveRegex(busqueda),
                          )
                          .concat('\\b'),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
                {
                  $and: [
                    {
                      nombreContacto: {
                        $regex: '\\b'.concat(busqueda).concat('\\b'),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
                {
                  $and: [
                    {
                      nombreContacto: {
                        $regex: '\\b'.concat(busqueda),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
                {
                  $and: [
                    {
                      nombreContactoTraducido: {
                        $regex: '\\b'.concat(busqueda).concat('\\b'),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
                {
                  $and: [
                    {
                      nombreContactoTraducido: {
                        $regex: '\\b'.concat(busqueda),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
                {
                  $and: [
                    {
                      nombreContacto: {
                        $regex: '^'.concat(busqueda),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
                {
                  $and: [
                    {
                      nombreContactoTraducido: {
                        $regex: '^'.concat(busqueda),
                        $options: 'i',
                      },
                    },
                    { estado: codEstadoPerfiles },
                  ],
                },
              ],
            },
          ],
        },
        // options,
      ).populate({
        path: 'usuario',
        populate: 'rolSistema'
      })

      const listaPerfiles = []
      if (getPerfiles.length > 0) {
        for (const perfil of getPerfiles) {

          if (perfil.usuario['estado'] === estadosUsuario.activa
            || perfil.usuario['estado'] === estadosUsuario.activaNoVerificado) {

            if (perfil.usuario['rolSistema'].length > 0) {
              if (perfil.usuario['rolSistema'][0] != null) {
                for (const roles of perfil.usuario['rolSistema']) {
                  if (roles.rol === codigosCatalogoRol.usuario) {
                    listaPerfiles.push(perfil._id)
                    break
                  }
                }

              }else{
                listaPerfiles.push(perfil._id)
                
              }

            }else{
              listaPerfiles.push(perfil._id)
            }


          }
        }
      }

      const getBusqueda = await this.perfilModel.paginate(
        {
          _id: { $in: listaPerfiles }
        },
        options,
      )

      let dataPerfiles = [];

      if (getBusqueda.docs.length > 0) {
        for (let itemPerfil of getBusqueda.docs) {
          //Llama al metodo de obtener foto de perfil
          let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
            itemPerfil._id,
            true,
          );
          let dataAlbum = [];
          let arrayAsociaciones = [];
          dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);

          //Data para el mapeo de perfil
          let objPerfil: any;
          if (idPerfil) {
            const getAsociacion = await this.participanteAsociacionModel
              .findOne({
                perfil: itemPerfil._id,
                contactoDe: idPerfil,
                estado: estadosPartAso.contacto,
              })
              .populate({
                path: 'asociacion',
                select: 'nombre tipo estado',
                match: {
                  tipo: codigosTipoAsociacion.contacto,
                  estado: codigoEstadosAsociacion.activa,
                },
              });

            if (getAsociacion) {
              if (getAsociacion.asociacion) {
                let objAsociacion = {
                  _id: getAsociacion.asociacion['_id'],
                };
                arrayAsociaciones.push(objAsociacion);
              }
            }

            //Data para el mapeo de perfil
            objPerfil = {
              _id: itemPerfil._id,
              nombreContacto: itemPerfil.nombreContacto,
              nombreContactoTraducido:
                itemPerfil?.nombreContactoTraducido || null,
              nombre: itemPerfil.nombre,
              album: dataAlbum,
              asociaciones: arrayAsociaciones,
            };
            dataPerfiles.push(objPerfil);
          } else {
            //Data para el mapeo de perfil
            objPerfil = {
              _id: itemPerfil._id,
              nombreContacto: itemPerfil.nombreContacto,
              nombreContactoTraducido:
                itemPerfil?.nombreContactoTraducido || null,
              nombre: itemPerfil.nombre,
              album: dataAlbum,
            };
            dataPerfiles.push(objPerfil);
          }
        }
      } else {
        response.set(headerNombre.totalDatos, getBusqueda.totalDocs);
        response.set(headerNombre.totalPaginas, getBusqueda.totalPages);
        response.set(headerNombre.proximaPagina, getBusqueda.hasNextPage);
        response.set(headerNombre.anteriorPagina, getBusqueda.hasPrevPage);

        return getBusqueda.docs;
      }
      response.set(headerNombre.totalDatos, getBusqueda.totalDocs);
      response.set(headerNombre.totalPaginas, getBusqueda.totalPages);
      response.set(headerNombre.proximaPagina, getBusqueda.hasNextPage);
      response.set(headerNombre.anteriorPagina, getBusqueda.hasPrevPage);

      return dataPerfiles;
    } catch (error) {
      throw error;
    }
  }
}
