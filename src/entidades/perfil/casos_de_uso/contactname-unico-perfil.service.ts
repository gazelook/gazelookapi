import { CatalogoIdiomasService } from './../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerIdUsuarioService } from './../../usuario/casos_de_uso/obtener-id-usuario.service';
import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { TraducionTipoPerfil } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_perfil/traduccion-catalogo-tipo-perfil.interface';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import * as mongoose from 'mongoose';
import { CatalogoIdiomas } from 'src/drivers/mongoose/interfaces/catalogo_idiomas/catalogo-idiomas.interface';
import { ObtenerDatosUsuarioService } from '../../usuario/casos_de_uso/obtener-datos-usuario.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { ObtenerCatalogoPaisService } from '../../pais/casos_de_uso/obtener-catalogo-pais.service';
import { ObtenerCatalogoLocalidadService } from '../../pais/casos_de_uso/obtener-catalogo-localidad.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import {
  estadosPerfil,
  estadosUsuario,
  idiomas,
} from '../../../shared/enum-sistema';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { Funcion } from 'src/shared/funcion';
import { RollbackCreacionCuentaPaymentezService } from 'src/entidades/usuario/casos_de_uso/rollback-creacion-cuenta-paymentez.service';

@Injectable()
export class ContactnameUnicoPerfilService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private rollbackCreacionCuentaPaymentezService: RollbackCreacionCuentaPaymentezService,
    private readonly funcion: Funcion,
  ) { }

  async nombreContactoUnico(
    nombreContacto,
    contactoTraducido?: string,
  ): Promise<any> {
    try {
      let returnData: any;
      let userInactivoPago;
      if (contactoTraducido === 'false') {
        const perfil = await this.perfilModel.findOne({
          $and: [
            {
              $or: [
                { estado: estadosPerfil.activa },
                { estado: estadosPerfil.hibernado },
              ],
            },
            {
              $or: [
                {
                  nombreContacto: {
                    $regex: '^'
                      .concat(
                        this.funcion.diacriticSensitiveRegex(nombreContacto),
                      )
                      .concat('$'),
                    $options: 'i',
                  },
                },
                {
                  nombreContacto: this.funcion.diacriticSensitiveRegex(
                    nombreContacto,
                  ),
                },
              ],
            },
          ],
        });

        if (perfil) {
          const idUsuario = perfil.usuario;
          userInactivoPago = await this.userModel.findOne({
            $and: [{ _id: idUsuario }, { estado: estadosUsuario.inactivaPago }],
          });
          if (userInactivoPago) {
            //_______________ elimina usuario que este como inactivo pago ________________________
            this.rollbackCreacionCuentaPaymentezService.rollBackCrearCuenta(
              userInactivoPago.email,
            );
            // return true;
          }
        }

        // let getPrimerosCaracteres = this.funcion.obtenerPrimerosCaracteres(nombreContacto, 3);
        // let primerCaracter = nombreContacto.charAt(0);
        // let segundoCaracter = nombreContacto.charAt(1);
        // let tercerCaracter = nombreContacto.charAt(2);

        //Expresion regular para validar  solo abecedario latino, numero y simbolos _.-
        let exp = this.funcion.expReg;
        let verificaIdiom: any;
        // if (tercerCaracter) {
        //   if (
        //     primerCaracter.match(exp) === null ||
        //     segundoCaracter.match(exp) === null ||
        //     tercerCaracter.match(exp) === null
        //   ) {
        //     verificaIdiom = false;
        //   } else {
        //     verificaIdiom = true;
        //   }
        // } else {
        //   if (
        //     primerCaracter.match(exp) === null ||
        //     segundoCaracter.match(exp) === null
        //   ) {
        //     verificaIdiom = false;
        //   } else {
        //     verificaIdiom = true;
        //   }
        // }

        //Verifica en toda la cadena que contenga solo abecedario en los 6 idiomas permitidos
        //numeros y simbolos _ .-
        if (nombreContacto.match(exp) === null) {
          verificaIdiom = false
        } else {
          verificaIdiom = true
        }

        if (perfil) {
          if (userInactivoPago) {
            returnData = {
              nombreContactoUnico: true,
              verificaIdioma: verificaIdiom,
            };
          } else {
            returnData = {
              nombreContactoUnico: false,
              verificaIdioma: verificaIdiom,
            };
          }
        } else {
          returnData = {
            nombreContactoUnico: true,
            verificaIdioma: verificaIdiom,
          };
        }
      } else {
        const perfil = await this.perfilModel.findOne({
          $and: [
            {
              $or: [
                { estado: estadosPerfil.activa },
                { estado: estadosPerfil.hibernado },
              ],
            },
            {
              $or: [
                {
                  nombreContacto: {
                    $regex: '^'
                      .concat(
                        this.funcion.diacriticSensitiveRegex(nombreContacto),
                      )
                      .concat('$'),
                    $options: 'i',
                  },
                },
                {
                  nombreContactoTraducido: {
                    $regex: '^'
                      .concat(
                        this.funcion.diacriticSensitiveRegex(nombreContacto),
                      )
                      .concat('$'),
                    $options: 'i',
                  },
                },
                {
                  nombreContacto: this.funcion.diacriticSensitiveRegex(
                    nombreContacto,
                  ),
                },
                {
                  nombreContactoTraducido: this.funcion.diacriticSensitiveRegex(
                    nombreContacto,
                  ),
                },
              ],
            },
          ],
        });

        if (perfil) {
          const idUsuario = perfil.usuario;
          userInactivoPago = await this.userModel.findOne({
            $and: [{ _id: idUsuario }, { estado: estadosUsuario.inactivaPago }],
          });
          if (userInactivoPago) {
            //_______________ elimina usuario que este como inactivo pago ________________________
            this.rollbackCreacionCuentaPaymentezService.rollBackCrearCuenta(
              userInactivoPago.email,
            );
            // return true;
          }
        }

        //Expresion regular para validar  solo abecedario latino, numero y simbolos _.-
        let exp = this.funcion.expReg;
        let verificaIdiom: any;

        //Verifica en toda la cadena que contenga solo abecedario latino, numero y simbolos _ .-
        if (nombreContacto.match(exp) === null) {
          verificaIdiom = false;
        } else {
          verificaIdiom = true;
        }

        if (perfil) {
          if (userInactivoPago) {
            returnData = {
              nombreContactoUnico: true,
              verificaIdioma: verificaIdiom,
            };
          } else {
            returnData = {
              nombreContactoUnico: false,
              verificaIdioma: verificaIdiom,
            };
          }
        } else {
          returnData = {
            nombreContactoUnico: true,
            verificaIdioma: verificaIdiom,
          };
        }
      }

      return returnData;
    } catch (error) {
      throw error;
    }
  }
}
