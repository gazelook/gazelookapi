import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { TraducionTipoPerfil } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_perfil/traduccion-catalogo-tipo-perfil.interface';
import { CatalogoIdiomas } from 'src/drivers/mongoose/interfaces/catalogo_idiomas/catalogo-idiomas.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { idiomas } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerPerfilesService {
  constructor(
    @Inject('TIPO_PERFIL_MODEL')
    private readonly tipoPerfilModelo: Model<TipoPerfil>,
    @Inject('TRADUCCION_TIPO_PERFIL_MODEL')
    private readonly traduccionTipoPerfilModelo: Model<TraducionTipoPerfil>,

    private readonly catalogoIdiomasModel: CatalogoIdiomasService,
  ) {}

  async obtenerPerfiles(idiomaPerfil: string): Promise<any> {
    try {
      const perfiles = await this.tipoPerfilModelo.find();
      const idioma = await this.catalogoIdiomasModel.obtenerIdiomaByCodigoNombre(
        idiomaPerfil,
      );

      const listaPerfiles: TraducionTipoPerfil[] = [];
      for (let i = 0; i < perfiles.length; i++) {
        const codigoTipoPerfil: any = perfiles[i].codigo;
        const perfilTraducion = await this.traduccionTipoPerfilModelo.find(
          {
            $and: [
              { idioma: idioma.codigo },
              { codigoTipoPerfil: codigoTipoPerfil },
            ],
          },
          { _id: 0, nombre: 1, descripcion: 1, idioma: 1, codigoTipoPerfil: 1 },
        );
        listaPerfiles.push(perfilTraducion[0]);
      }
      const listaResult = [];
      for (let i = 0; i < listaPerfiles.length; i++) {
        const data = {
          codigo: listaPerfiles[i].codigoTipoPerfil,
          traducciones: [
            {
              nombre: listaPerfiles[i].nombre,
              descripcion: listaPerfiles[i].descripcion,
              idioma: listaPerfiles[i].idioma,
            },
          ],
        };
        listaResult.push(data);
      }

      return listaResult;
    } catch (error) {
      throw error;
    }
  }
}
