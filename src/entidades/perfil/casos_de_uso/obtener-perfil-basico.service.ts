import { CatalogoIdiomasService } from './../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerIdUsuarioService } from './../../usuario/casos_de_uso/obtener-id-usuario.service';
import { Injectable, Inject, HttpStatus, forwardRef } from '@nestjs/common';
import { Model } from 'mongoose';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import * as mongoose from 'mongoose';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import {
  estadosPerfil,
  filtroBusqueda,
  idiomas,
  codigosCatalogoTipoProyecto,
  estadosProyecto,
  codigosArchivosSistema,
  codigosCatalogoAlbum,
  codigosTiposPerfil,
  nombreEntidades,
  nombrecatalogoEstados,
} from '../../../shared/enum-sistema';
import { GetFotoPerfilService } from './obtener-album-perfil-general.service';
import { ObtenerPensamientosPerfilService } from '../../pensamientos/casos_de_uso/obtener-pensamientos-perfil.service';
import { AsociacionEntrePerfilesService } from '../../asociacion_participante/casos_de_uso/asociacion-entre-perfiles.service';

@Injectable()
export class ObtenerPerfilBasicoService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private readonly catalogoIdiomasModelat: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private obtenerPensamientosPerfilService: ObtenerPensamientosPerfilService,
    private getFotoPerfilService: GetFotoPerfilService,
    private asociacionEntrePerfilesService: AsociacionEntrePerfilesService,
  ) {}

  // retorna perfiles de usuario con noticias, pensamientos, noticias
  async obtenerPerfilBasico(
    idPerfil: string,
    perfilVisitante: string,
    idioma,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      const catalogoIdioma = await this.catalogoIdiomasModelat.obtenerIdiomaByCodigoNombre(
        idioma,
      );

      // ______________________estado perfiles____________________________
      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.perfiles,
      );
      const codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado.codigo;
      //_________obtener perfil______________
      const perfil = await this.perfilModel
        .findOne({
          $and: [{ estado: codEstado }, { _id: idPerfil }],
        })
        .select('-fechaCreacion -fechaActualizacion -__v');

      let result: any = {
        _id: perfil._id,
        nombre: perfil.nombre,
        nombreContacto: perfil.nombreContacto,
        nombreContactoTraducido: perfil?.nombreContactoTraducido || null,
        tipoPerfil: { codigo: perfil.tipoPerfil },
        estado: { codigo: perfil.estado },
      };

      // ALBUM PERFIL Y GENERAL
      const albumPerfilGeneralFoto = await this.getFotoPerfilService.getFotoPerfil(
        idPerfil,
        true,
      );
      result.album = [albumPerfilGeneralFoto.objAlbumTipoPerfil];

      //_________________________________pensamientos________________________________

      const getPensamientos = await this.obtenerPensamientosPerfilService.obtenerPerfilPensamientosRecientes(
        idPerfil,
        catalogoIdioma.codNombre,
        6,
        perfilVisitante,
        opts,
      );

      result.pensamientos = getPensamientos;

      // verificar si tiene una asociacion con el otro perfil
      if (perfilVisitante) {
        const invitacionContacto = await this.asociacionEntrePerfilesService.asociacionEntrePerfiles(
          idPerfil,
          perfilVisitante,
        );
        if (invitacionContacto) {
          result.participanteAsociacion = invitacionContacto;
        }
      }

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return result;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async obtenerPerfilSimple(idPerfil: string): Promise<any> {
    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.perfiles,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    //_________obtener perfil______________
    const perfil = await this.perfilModel
      .findOne({
        $and: [{ estado: codEstado }, { _id: idPerfil }],
      })
      .select('-fechaCreacion -fechaActualizacion -__v');

    let result: any = {
      _id: perfil._id,
      nombre: perfil.nombre,
      nombreContacto: perfil.nombreContacto,
      tipoPerfil: { codigo: perfil.tipoPerfil },
      estado: { codigo: perfil.estado },
    };
    // ALBUM PERFIL Y GENERAL
    const albumPerfilGeneralFoto = await this.getFotoPerfilService.getFotoPerfil(
      idPerfil,
      true,
    );
    result.album = [albumPerfilGeneralFoto.objAlbumTipoPerfil];

    return result;
  }
}
