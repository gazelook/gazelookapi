import { Inject, Injectable } from '@nestjs/common';
import { Model, startSession } from 'mongoose';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import {
  accionNotificacionFirebase,
  codigoEntidades,
  codigosTiposPerfil,
  estadoNotificacionesFirebase,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
  tipoNotificacionFirebase,
} from 'src/shared/enum-sistema';
import { ObtenerDispositivoUsuarioService } from 'src/entidades/dispositivos/casos_de_uso/obtener-dispositivo-usuario.service';
import { FBNotificacion } from 'src/drivers/firebase/models/fb-notificacion.interface';
import { FirebaseNotificacionService } from 'src/drivers/firebase/services/firebase-notificacion.service';
import { CrearPerfilCuentaDto } from '../entidad/crear-perfil-cuenta.dto';

@Injectable()
export class CrearPerfilService {
  CODIGO_TIPO_PERFIL_GRUPO = codigosTiposPerfil.tipoPerfilGrupo;

  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('TIPO_PERFIL_MODEL')
    private readonly tipoPerfilModelo: Model<TipoPerfil>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private obtenerDispositivoUsuarioService: ObtenerDispositivoUsuarioService,
    private firebaseNotificacionService: FirebaseNotificacionService,
  ) {}

  async crearPerfil(
    dataPerfil: CrearPerfilCuentaDto,
    dispositivo,
    opts: any,
  ): Promise<Perfil> {
    //Obtiene el codigo de la accion a realizarse
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    const getAccion = accion['codigo'];

    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.perfiles,
    );
    const codEntidad = entidad['codigo'];

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado['codigo'];

    try {
      //crea perfil
      const dataPerfilGuardar = {
        ...dataPerfil,
        estado: codEstado,
      };
      const perfil = new this.perfilModel(dataPerfilGuardar);
      await perfil.save(opts);

      const dataPerfilHistorico = JSON.parse(JSON.stringify(perfil));
      //eliminar parametro no necesarios
      delete dataPerfilHistorico.fechaCreacion;
      delete dataPerfilHistorico.fechaActualizacion;
      delete dataPerfilHistorico.__v;

      const dataHistoricoPerfil: any = {
        datos: dataPerfilHistorico,
        usuario: dataPerfil.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoPerfil);

      // notificar la creacion del perfil
      const dataPerfilNotificar = {
        _id: perfil._id.toString(),
        nombre: perfil.nombre,
        nombreContacto: perfil.nombreContacto,
        estado: { codigo: perfil.estado },
        usuario: {
          _id: perfil.usuario.toString(),
        },
      };

      let dataDispNotif,
        listDispositivos = [];
      if (dispositivo) {
        const getDispositivos: any = await this.obtenerDispositivoUsuarioService.getDispositivosUsuario(
          perfil.usuario.toString(),
        );
        for (const disp of getDispositivos) {
          if (disp._id.toString() === dispositivo._id.toString()) {
            dataDispNotif = {
              idDispositivo: disp._id.toString(),
              leido: true,
            };
          } else {
            dataDispNotif = {
              idDispositivo: disp._id.toString(),
              leido: false,
            };
          }
          listDispositivos = [...listDispositivos, dataDispNotif];
        }

        const dataNotificacionPerfil: FBNotificacion = {
          codEntidad: codigoEntidades.entidadPerfiles,
          idEntidad: perfil._id.toString(),
          data: dataPerfilNotificar,
          leido: false,
          estado: estadoNotificacionesFirebase.activa,
          accion: accionNotificacionFirebase.ver,
          tipo: tipoNotificacionFirebase.sistema,
          query: `${codigoEntidades.entidadPerfiles}-${false}`,
          dispositivos: listDispositivos,
        };

        this.firebaseNotificacionService.createDataNotificacion(
          dataNotificacionPerfil,
          codigoEntidades.entidadUsuarios,
          perfil.usuario.toString(),
        );
      }

      return perfil;
    } catch (error) {
      throw error;
    }
  }

  async verificarPerfilUnico(listaPerfil: any[]): Promise<Boolean> {
    let bandera = true;
    if (await this.existePerfil(listaPerfil)) {
      if (
        (listaPerfil.indexOf(this.CODIGO_TIPO_PERFIL_GRUPO) != -1 &&
          listaPerfil.length > 1) ||
        listaPerfil.length > 3
      ) {
        bandera = false;
      }
      if (await this.elementosRepetidos(listaPerfil)) {
        bandera = false;
      }
    } else {
      bandera = false;
    }
    return bandera;
  }

  async elementosRepetidos(listaPerfil: any[]): Promise<Boolean> {
    return listaPerfil.some(function(v, i) {
      return listaPerfil.indexOf(v, i + 1) > -1;
    });
  }

  async existePerfil(listaPerfil: any[]): Promise<Boolean> {
    const perfiles = await this.tipoPerfilModelo.find();

    for (let i = 0; i < listaPerfil.length; i++) {
      const result = perfiles.findIndex(
        element => element.codigo === listaPerfil[i],
      );
      if (result === -1) {
        return false;
      }
    }
    return true;
  }
}
