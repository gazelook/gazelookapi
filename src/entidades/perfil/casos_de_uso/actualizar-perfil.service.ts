import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { erroresPerfil } from '../../../shared/enum-errores';
import {
  estadosPerfil,
  nombreAcciones,
  nombreEntidades,
  propiedadesPerfil,
} from '../../../shared/enum-sistema';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { ActualizarDireccionService } from '../../direccion/casos_de_uso/actualizar-direccion.service';
import { ActualizarTelefonoService } from '../../direccion/casos_de_uso/actualizar-telefono.service';
import { CrearDireccionService } from '../../direccion/casos_de_uso/crear-direccion.service';
import { CrearTelefonoService } from '../../direccion/casos_de_uso/crear-telefono.service';
import { ActualizarPerfilDto } from '../entidad/actualizar-perfil.dto';
import { AgregarReferenciaPerfilService } from './agregar-referencia-perfil.service';
import { ObtenerAllDatosPerfilService } from './obtener-all-datos-perfil.service';

@Injectable()
export class ActualizarPerfilService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private crearDireccionService: CrearDireccionService,
    private crearTelefonoService: CrearTelefonoService,
    private actualizarDireccionService: ActualizarDireccionService,
    private agregarReferenciaPerfilService: AgregarReferenciaPerfilService,
    private obtenerAllDatosPerfilService: ObtenerAllDatosPerfilService,
    private actualizarTelefonoService: ActualizarTelefonoService,
  ) {}

  async actualizarPerfil(
    data: ActualizarPerfilDto,
    idioma: string,
  ): Promise<any> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      const opts = { session };

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.perfiles,
      );
      const codEntidad = entidad['codigo'];

      const idPerfil = data._id;

      //______verificar perfil__________
      const existePerfil: any = await this.perfilModel
        .findOne({ _id: idPerfil })
        .populate('album');
      if (!existePerfil) {
        throw {
          codigo: HttpStatus.OK,
          codigoNombre: erroresPerfil.PERFIL_NO_EXISTE,
        };
      }
      //_______verificar nombre contacto_________
      const existeNombreContacto: any = await this.perfilModel.findOne({
        $and: [
          {
            $or: [
              { estado: estadosPerfil.activa },
              { estado: estadosPerfil.hibernado },
            ],
          },
          { nombreContacto: data.nombreContacto },
        ],
      });
      // nombreContacto: data.nombreContacto });
      if (existeNombreContacto) {
        console.log(
          'existePerfil._id.toString(): ',
          existePerfil._id.toString(),
        );
        console.log(
          'existeNombreContacto._id.toString(): ',
          existeNombreContacto._id.toString(),
        );

        if (
          existePerfil._id.toString() !== existeNombreContacto._id.toString()
        ) {
          if (existeNombreContacto.nombreContacto === data.nombreContacto) {
            console.log('acaaaaaaaaaaaaaaaaaaaa*********************');
            throw {
              codigo: HttpStatus.OK,
              codigoNombre: erroresPerfil.ERROR_NOMBRE_CONTACTO,
            };
          }
        }
      }

      // _________________album_____________________
      let nuevaDireccion, nuevoTelefono;

      // _________________direccion_____________________
      if (data.direcciones) {
        for (const direccion of data.direcciones) {
          let dataDireccion: any = {
            latitud: direccion.latitud,
            longitud: direccion.longitud,
            traducciones: direccion.traducciones,
            //localidad: direccion.localidad.codigo,
            usuario: existePerfil.usuario,
            pais: direccion.pais.codigo,
          };
          if (direccion?._id) {
            dataDireccion._id = direccion._id;
            await this.actualizarDireccionService.actualizarDireccion(
              dataDireccion,
              opts,
            );
          } else {
            //crear la nueva direccion
            nuevaDireccion = await this.crearDireccionService.crearDireccion(
              dataDireccion,
              opts,
            );
            await this.agregarReferenciaPerfilService.agregarReferencia(
              idPerfil,
              propiedadesPerfil.direcciones,
              nuevaDireccion._id,
              opts,
            );
            /* this.perfilModel.updateOne({ _id: idPerfil },
              { $push: { direcciones: nuevaDireccion._id } }); */
          }
        }
      }

      // ____________telefono_________________
      const listaTelefono = data?.telefonos;
      if (listaTelefono && listaTelefono.length > 0) {
        for (const telefono of listaTelefono) {
          console.log(telefono);
          const dataTelefono: any = {
            numero: telefono.numero,
            pais: telefono.pais.codigo,
            usuario: existePerfil.usuario,
          };
          if (telefono?._id) {
            // actualizar telefono
            dataTelefono._id = telefono._id;
            await this.actualizarTelefonoService.actualizarTelefono(
              dataTelefono,
              opts,
            );
          } else {
            // crear telefono
            nuevoTelefono = await this.crearTelefonoService.crearTelefono(
              dataTelefono,
              opts,
            );
            await this.agregarReferenciaPerfilService.agregarReferencia(
              idPerfil,
              propiedadesPerfil.telefonos,
              nuevoTelefono._id,
              opts,
            );
          }
        }
      } else {
        await this.perfilModel.findByIdAndUpdate(
          existePerfil._id,
          { telefonos: [] },
          opts,
        );
      }

      // _______________ perfil ___________________
      const dataPerfil = {
        nombreContacto: data.nombreContacto,
        nombreContactoTraducido: data.nombreContactoTraducido,
        nombre: data.nombre,
      };
      const updatePerfil = await this.perfilModel.findByIdAndUpdate(
        idPerfil,
        dataPerfil,
        opts,
      );
      // _______________historico___________________
      const dataPerfilHistorico = JSON.parse(JSON.stringify(updatePerfil));
      //eliminar parametro no necesarios
      delete dataPerfilHistorico.fechaCreacion;
      delete dataPerfilHistorico.fechaActualizacion;
      delete dataPerfilHistorico.__v;

      const dataHistoricoPerfil: any = {
        datos: dataPerfilHistorico,
        usuario: existePerfil.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoPerfil);

      // termina el proceso
      await session.commitTransaction();
      session.endSession();
      const result = await this.obtenerAllDatosPerfilService.obtenerAllDatosPerfil(
        idPerfil,
        idioma,
      );

      // actualizar en todos los perfiles datos compartidos(direccion, nombreContacto, nombre)
      const dataActualizarOtrosPerfiles = {
        telefonos: data.telefonos,
        direcciones: data.direcciones,
      };
      this.actualizarInfoCompartidaPerfiles(
        idPerfil.toString(),
        existePerfil.usuario.toString(),
        dataActualizarOtrosPerfiles,
      )
        .then(() => {
          console.log('Perfiles actualizados');
        })
        .catch(error => console.log('No se actualizaron los perfiles', error));

      return result;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }

  async actualizarInfoCompartidaPerfiles(
    idPeril,
    usuario,
    data: any,
  ): Promise<any> {
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      let nuevaDireccion, nuevoTelefono;
      const getPerfiles = await this.perfilModel.find({
        $and: [
          { usuario: usuario },
          { estado: { $ne: estadosPerfil.eliminado } },
        ],
      });

      for (const perfil of getPerfiles) {
        if (perfil.id !== idPeril) {
          // _________________direccion_____________________
          if (data.direcciones) {
            for (const direccion of data.direcciones) {
              let dataDireccion: any = {
                latitud: direccion.latitud,
                longitud: direccion.longitud,
                traducciones: direccion.traducciones,
                //localidad: direccion.localidad.codigo,
                pais: direccion.pais.codigo,
                usuario: usuario,
              };

              dataDireccion._id = perfil.direcciones[0].toString();
              console.log('actualizaDir', dataDireccion);
              await this.actualizarDireccionService.actualizarDireccion(
                dataDireccion,
                opts,
              );
            }
          }

          // ____________telefono_________________
          const listaTelefono = data?.telefonos;
          if (listaTelefono && listaTelefono.length > 0) {
            for (let telefono of listaTelefono) {
              const dataTelefono: any = {
                numero: telefono.numero,
                pais: telefono.pais.codigo,
                usuario: usuario,
              };
              // actualizar telefono
              if (perfil.telefonos.length > 0) {
                dataTelefono._id = perfil.telefonos[0].toString();
                console.log('actualizaTelf', dataTelefono);
                await this.actualizarTelefonoService.actualizarTelefono(
                  dataTelefono,
                  opts,
                );
              } else {
                // crear telefono
                nuevoTelefono = await this.crearTelefonoService.crearTelefono(
                  dataTelefono,
                  opts,
                );
                await this.agregarReferenciaPerfilService.agregarReferencia(
                  perfil._id,
                  propiedadesPerfil.telefonos,
                  nuevoTelefono._id,
                  opts,
                );
              }
            }
          }
        }
      }
      await session.commitTransaction();
      session.endSession();

      return true;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
