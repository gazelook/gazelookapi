import { CatalogoIdiomasService } from './../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerIdUsuarioService } from './../../usuario/casos_de_uso/obtener-id-usuario.service';
import { Injectable, Inject, HttpStatus, forwardRef } from '@nestjs/common';
import { Model } from 'mongoose';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { TraducionTipoPerfil } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_perfil/traduccion-catalogo-tipo-perfil.interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import * as mongoose from 'mongoose';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { ObtenerCatalogoPaisService } from '../../pais/casos_de_uso/obtener-catalogo-pais.service';
import { ObtenerCatalogoLocalidadService } from '../../pais/casos_de_uso/obtener-catalogo-localidad.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import {
  estadosPerfil,
  filtroBusqueda,
  idiomas,
  codigosCatalogoTipoProyecto,
  estadosProyecto,
  codigosArchivosSistema,
  codigosCatalogoAlbum,
  codigosTiposPerfil,
  nombreEntidades,
  nombrecatalogoEstados,
  codigosEstadosPartAsociacion,
  codigosTipoAsociacion,
  estadosPartAso,
} from '../../../shared/enum-sistema';
import { TraduccionMediaService } from '../../media/casos_de_uso/traduccion-media.service';
import { ObtenerProyetoPerfilService } from '../../proyectos/casos_de_uso/obtener-proyecto-perfil.service';
import { ListaArchivosDefaultTipoService } from '../../archivo/casos_de_uso/lista-archivos-default-tipo.service';
import { ObtenerAlbumService } from '../../album/casos_de_uso/obtener-album.service';
import { ObtenerNoticiasPerfilService } from '../../noticia/casos_de_uso/obtener-noticias-perfil.service';
import { GetFotoPerfilService } from './obtener-album-perfil-general.service';
import { ObtenerPensamientosPerfilService } from '../../pensamientos/casos_de_uso/obtener-pensamientos-perfil.service';
import { ObtenerContactosPerfilService } from '../../asociacion_participante/casos_de_uso/obtener-contactos-perfil.service';
import { AsociacionEntrePerfilesService } from '../../asociacion_participante/casos_de_uso/asociacion-entre-perfiles.service';
import { CalcularDiasProyectoService } from 'src/entidades/proyectos/casos_de_uso/carcular-dias-proyecto.service';
import { ObtenerPortadaService } from 'src/entidades/noticia/casos_de_uso/obtener-portada-predeterminada.service';
import { ObtenerEntidadNotificacionService } from 'src/entidades/notificacion/casos_de_uso/obtener-entidad-notificacion.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from 'src/entidades/proyectos/casos_de_uso/obtener-portada-predeterminada-proyecto-resumen.service';
import {
  populateGetMediaTraduciones,
  populateGetPortadaTraduccion,
} from '../../../shared/enum-query-populate';
import { CalcularDiasNoticiaService } from '../../noticia/casos_de_uso/carcular-dias-noticia.service';
import { erroresPerfil } from '../../../shared/enum-errores';

@Injectable()
export class ObtenerPerfilGeneralService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private readonly catalogoIdiomasModelat: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private obtenerProyetoPerfilService: ObtenerProyetoPerfilService,
    private listaArchivosDefaultTipoService: ListaArchivosDefaultTipoService,
    private obtenerAlbumService: ObtenerAlbumService,
    private obtenerNoticiasPerfilService: ObtenerNoticiasPerfilService,
    private obtenerPensamientosPerfilService: ObtenerPensamientosPerfilService,
    private obtenerContactosPerfilService: ObtenerContactosPerfilService,
    private asociacionEntrePerfilesService: AsociacionEntrePerfilesService,
    private getFotoPerfilService: GetFotoPerfilService,
    private readonly calcularDiasProyectoService: CalcularDiasProyectoService,
    private obtenerPortada: ObtenerPortadaService,
    private obtenerEntidadNotificacionService: ObtenerEntidadNotificacionService,
    private obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
    private traduccionMediaService: TraduccionMediaService,
    private calcularDiasNoticiaService: CalcularDiasNoticiaService,
  ) {}

  // retorna perfiles de usuario con noticias, pensamientos, noticias
  async obtenerPerfilGeneral(
    idPerfil: string,
    idPerfilOtros,
    idioma,
  ): Promise<any> {
    const perfilOwner = await this.perfilModel.findOne({ _id: idPerfil });
    if (!perfilOwner) {
      return null;
    }

    if (perfilOwner.estado === estadosPerfil.hibernado) {
      throw {
        codigo: HttpStatus.CONFLICT,
        codigoNombre: erroresPerfil.PERFIL_HIBERNADO,
      };
    }

    const usuarioOwner: any = await this.obtenerIdUsuarioService.obtenerUsuarioById(
      perfilOwner.usuario,
    );

    let perfilOtros, usuarioOtros;
    if (idPerfilOtros) {
      perfilOtros = await this.perfilModel.findOne({ _id: idPerfilOtros });
      if (perfilOtros) {
        usuarioOtros = await this.obtenerIdUsuarioService.obtenerUsuarioById(
          perfilOtros.usuario,
        );
      } else {
        return null;
      }
    }
    //controlar idioma
    let catalogoIdioma;
    let estadoHibernado;

    catalogoIdioma = await this.catalogoIdiomasModelat.obtenerIdiomaByCodigoNombre(
      idioma,
    );
    estadoHibernado = estadosPerfil.hibernado;
    // }

    // ______________________estado media____________________________
    //Obtiene el codigo de la entidad
    const entidadMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    const codEntidadMedia = entidadMedia.codigo;

    //Obtiene el estado
    const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadMedia,
    );
    const codEstadoMedia = estadoMedia.codigo;

    // ______________________estado album____________________________
    //Obtiene el codigo de la entidad
    const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.album,
    );
    const codEntidadAlbum = entidadAlbum.codigo;

    //Obtiene el estado
    const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadAlbum,
    );
    const codEstadoAlbum = estadoAlbum.codigo;

    //_____________________estado traduccion media___________________________
    //Obtiene el codigo de la entidad
    const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.traduccionMedia,
    );
    const codEntidadTradMedia = entidadTradMedia.codigo;
    //Obtiene el estado
    const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadTradMedia,
    );
    const codEstadoTradMedia = estadoTradMedia.codigo;

    // ______________________estado perfiles____________________________
    //Obtiene el codigo de la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.perfiles,
    );
    const codEntidad = entidad.codigo;

    //Obtiene el estado
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidad,
    );
    const codEstado = estado.codigo;

    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      let opts = { session };

      //_________obtener perfil______________
      const perfil = await this.perfilModel
        .findOne({
          $and: [
            {
              $or: [{ estado: codEstado }, { estado: estadoHibernado }],
            },
            { _id: idPerfil },
          ],
        })
        .select('-fechaCreacion -fechaActualizacion -__v')
        .populate([
          {
            path: 'album',
            match: {
              estado: codEstadoAlbum,
            },
            select: '-fechaCreacion -fechaActualizacion -__v',
            populate: [
              populateGetMediaTraduciones(
                codEstadoMedia,
                codEstadoTradMedia,
                catalogoIdioma.codigo,
              ),
              populateGetPortadaTraduccion(
                codEstadoTradMedia,
                catalogoIdioma.codigo,
              ),
            ],
          },
          {
            path: 'direcciones',
            select: '-fechaCreacion -fechaActualizacion -__v',
          },
        ]);

      console.log('perfil', perfil);
      if (!perfil) {
        return null;
      }
      let result: any = {
        _id: perfil._id,
        nombre: perfil.nombre,
        nombreContacto: perfil.nombreContacto,
        nombreContactoTraducido: perfil?.nombreContactoTraducido || null,
        tipoPerfil: { codigo: perfil.tipoPerfil },
        estado: { codigo: perfil.estado },
      };

      // ALBUM PERFIL Y GENERAL
      const albumPerfilGeneralFoto = await this.getFotoPerfilService.getFotoPerfil(
        idPerfil,
        true,
      );
      let albumGeneral = albumPerfilGeneralFoto.objAlbumTipoGeneral;

      // Retornar las medias del album si se visita a otro perfil
      if (perfil.album && perfilOtros) {
        for (const album of perfil.album) {
          if (album['tipo'] === codigosCatalogoAlbum.catAlbumGeneral) {
            if (album['media']?.length > 0) {
              albumGeneral._id = album['_id'];
              albumGeneral.media = await this.getMediasAlbum(
                album['media'],
                idioma,
                opts,
              );
              break;
            }
          }
        }
      }

      result.album = [albumPerfilGeneralFoto.objAlbumTipoPerfil, albumGeneral];

      console.log('antes de ingresar a proyectos recientes')
      //______________________proyectos________________________________
      const getProyectos = await this.obtenerProyetoPerfilService.obtenerProyectosRecientesPerfil(
        idPerfil,
        catalogoIdioma.codNombre,
        idPerfilOtros,
        opts,
      );
      let proyectos = [];
      for (const proyecto of getProyectos) {
        let actu = false;
        console.log('proyecto', proyecto);

        if (
          proyecto.fechaCreacion.toString() ==
          proyecto.fechaActualizacionSistema.toString()
        ) {
          actu = false;
        } else {
          let dias = this.calcularDiasProyectoService.calcularDias(
            new Date(),
            proyecto.fechaActualizacionSistema,
          );
          if (dias) {
            actu = true;
          }
        }

        let notificacion: any;
        let notif: any;
        if (idPerfil == proyecto.perfil) {
          //Obtiene si el proyecto tiene notificaciones entregadas pero no leidas
          notificacion = this.obtenerEntidadNotificacionService.obtenerIdEntidadNotificacion(
            proyecto._id,
          );
          if (notificacion.length > 0) {
            notif = true;
          } else {
            notificacion = [];
            notif = false;
          }
        }

        let dataProyectos: any = {
          _id: proyecto._id,
          traducciones: [
            {
              titulo: proyecto.traducciones[0].titulo,
              tituloCorto: proyecto.traducciones[0].tituloCorto,
            },
          ],
          actualizado: actu, //Si el proyecto ha sido actualizado recientemente =true
          listaNotificacion: notificacion,
          notificacion: notif,
          fechaCreacion: proyecto.fechaCreacion,
          fechaActualizacionSistema: proyecto.fechaActualizacionSistema,
        };

        dataProyectos.adjuntos = await this.obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
          proyecto.adjuntos,
          idioma,
          opts,
        );

        proyectos.push(dataProyectos);
      }

      result.proyectos = proyectos;

      //_________________________________noticias________________________________

      const getNoticias = await this.obtenerNoticiasPerfilService.obtenerNoticiasRecientesPerfil(
        idPerfil,
        catalogoIdioma.codNombre,
        idPerfilOtros,
        opts,
      );
      let noticias = [];
      for (const noticia of getNoticias) {
        let actu = false;
        if (
          noticia.fechaCreacion.toString() ==
          noticia.fechaActualizacion.toString()
        ) {
          actu = false;
        } else {
          let dias = this.calcularDiasNoticiaService.calcularDias(
            new Date(),
            noticia.fechaActualizacion,
          );
          if (dias) actu = true;
        }

        let dataNoticias: any = {
          _id: noticia._id,
          traducciones: [
            {
              titulo: noticia.traducciones[0]?.titulo,
              tituloCorto: noticia.traducciones[0]?.tituloCorto,
            },
          ],
          fechaActualizacion: noticia.fechaActualizacion,
          fechaCreacion: noticia.fechaCreacion,
        };
        let album = await this.obtenerPortada.obtenerPortada(
          noticia.adjuntos,
          idioma,
          opts,
        );

        dataNoticias.adjuntos = album;
        dataNoticias.actualizado = actu;
        noticias.push(dataNoticias);
      }
      result.noticias = noticias;

      //_________________________________pensamientos________________________________

      const getPensamientos = await this.obtenerPensamientosPerfilService.obtenerPerfilPensamientosRecientes(
        idPerfil,
        catalogoIdioma.codNombre,
        4,
        idPerfilOtros,
        opts,
      );

      result.pensamientos = getPensamientos;

      //_________________________________contactos________________________________

      let getContactos;
      let asociaciones = [];
      let servicioPeticion = {
        estado: codigosEstadosPartAsociacion.contacto,
      };
      if (perfilOtros && perfilOwner) {
        // si esta visitando otro perfil presentacion de sus contactos
        getContactos = await this.obtenerContactosPerfilService.obtenerContactosPerfil(
          idPerfil,
          'fecha',
          5,
          1,
          servicioPeticion,
        );
        console.log('getContactos', getContactos);

        if (getContactos?.length > 0) {
          for (const participanteAsocs of getContactos) {
            if (
              participanteAsocs.asociacion.tipo.codigo ===
              codigosTipoAsociacion.contacto
            ) {
              const dataAsociacion = {
                _id: participanteAsocs.asociacion._id,
                nombre: participanteAsocs.asociacion.nombre,
                participantes: [
                  {
                    _id: participanteAsocs._id,
                    perfil: participanteAsocs.perfil,
                  },
                ],
              };
              asociaciones.push(dataAsociacion);
            }
          }
        }
      } else {
        console.log('entraaaa x acaaa------');
        // si el perfil es propio se presentan las invitaciones de contacto
        servicioPeticion.estado = codigosEstadosPartAsociacion.enviada;
        getContactos = await this.obtenerContactosPerfilService.obtenerContactosPerfil(
          idPerfil,
          'fecha',
          5,
          1,
          servicioPeticion,
        );

        if (getContactos?.length > 0) {
          for (const participanteAsocs of getContactos) {
            if (
              participanteAsocs.asociacion.tipo.codigo ===
                codigosTipoAsociacion.contacto &&
              estadosPartAso.enviada === participanteAsocs.estado.codigo
            ) {
              const dataAsociacion = {
                _id: participanteAsocs.asociacion._id,
                nombre: participanteAsocs.asociacion.nombre,
                participantes: [participanteAsocs.invitadoPor],
              };
              asociaciones.push(dataAsociacion);
            }
          }
        }
      }
      result.asociaciones = asociaciones;

      //_________________________________si el perfil visitante tiene una invitacion pendiente________________________________
      if (perfilOtros) {
        const invitacionContacto = await this.asociacionEntrePerfilesService.asociacionEntrePerfiles(
          idPerfil,
          idPerfilOtros,
        );
        if (invitacionContacto) {
          result.participanteAsociacion = invitacionContacto;
        }
      }
      await session.commitTransaction();
      await session.endSession();

      return result;
    } catch (error) {
      console.log(error);
      await session.abortTransaction();
      await session.endSession();
      throw error;
    }
  }

  async getMediasAlbum(listaMedias, idioma, opts): Promise<any> {
    let listaMedia = [];
    for (const media of listaMedias) {
      let dataMedia: any = {
        _id: media['_id'],
      };
      if (media['principal']) {
        dataMedia.principal = {
          _id: media['principal']._id,
          url: media['principal'].url,
          duracion: media['principal']?.duracion,
          fechaActualizacion: media['principal']?.fechaActualizacion,
        };
      }
      if (media['miniatura']) {
        dataMedia.miniatura = {
          _id: media['miniatura']._id,
          url: media['miniatura'].url,
          duracion: media['principal']?.duracion,
        };
      }

      if (media['traducciones'][0]) {
        dataMedia.traducciones = [
          {
            _id: media['traducciones'][0]._id,
            descripcion: media['traducciones'][0].descripcion,
          },
        ];
      }

      //traducir descripcion de la media
      if (media['traducciones'][0] === undefined) {
        const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
          media['_id'],
          idioma,
          opts,
        );
        if (traduccionMedia) {
          dataMedia.traducciones = [
            {
              _id: traduccionMedia._id,
              descripcion: traduccionMedia.descripcion,
            },
          ];
        }
      }
      listaMedia.push(dataMedia);
    }
    return listaMedia;
  }
}
