import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import * as mongoose from 'mongoose';
import {
  estadosPerfil,
  nombreAcciones,
  nombreEntidades,
  nombrecatalogoEstados,
  codigosEstadosMedia,
  codigoEntidades,
  estadoNotificacionesFirebase,
  accionNotificacionFirebase,
  tipoNotificacionFirebase,
} from '../../../shared/enum-sistema';
import { EliminarAlbumService } from '../../album/casos_de_uso/eliminar-album.service';
import { EliminarProyectoUnicoService } from '../../proyectos/casos_de_uso/eliminar-proyecto-unico.service';
import { EliminarDireccionService } from '../../direccion/casos_de_uso/eliminar-direccion.service';
import { EliminarPensamientoService } from '../../pensamientos/casos_de_uso/eliminar-pensamiento.service';
import { EliminarNoticiasUnicaService } from '../../noticia/casos_de_uso/eliminar-noticia-unica.service';
import { ObtenerProyetoPerfilService } from '../../proyectos/casos_de_uso/obtener-proyecto-perfil.service';
import { ObtenerPensamientosPerfilService } from '../../pensamientos/casos_de_uso/obtener-pensamientos-perfil.service';
import { ObtenerNoticiasPerfilService } from '../../noticia/casos_de_uso/obtener-noticias-perfil.service';
import { EliminarParticipanteAsociacionService } from '../../asociacion_participante/casos_de_uso/eliminar-participante-asociacion.service';
import { ObtenerTodosParticipantePerfilService } from '../../asociacion_participante/casos_de_uso/obtener-todos-participante-perfil.service';
import { erroresPerfil, erroresGeneral } from '../../../shared/enum-errores';
import { FirebaseNotificacionService } from '../../../drivers/firebase/services/firebase-notificacion.service';
import { Dispositivo } from '../../../drivers/mongoose/interfaces/dispositivo/dispositivo.interface';
import { ObtenerDispositivoUsuarioService } from '../../dispositivos/casos_de_uso/obtener-dispositivo-usuario.service';
import { ObtenerIntercambioPerfilService } from 'src/entidades/intercambio/casos_de_uso/obtener-intercambio-perfil.service';
import { EliminarIntercambioUnicoService } from 'src/entidades/intercambio/casos_de_uso/eliminar-intercambio-unico.service';
import { FBNotificacion } from 'src/drivers/firebase/models/fb-notificacion.interface';
import { EliminarTelefonoService } from '../../direccion/casos_de_uso/eliminar-telefono.service';
import { EliminarPerfilDto } from '../entidad/eliminar-perfil.dto';

@Injectable()
export class EliminarPerfilService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private eliminarAlbumService: EliminarAlbumService,
    private eliminarDireccionService: EliminarDireccionService,
    private eliminarProyectoUnicoService: EliminarProyectoUnicoService,
    private eliminarPensamientoService: EliminarPensamientoService,
    private eliminarNoticiasUnicaService: EliminarNoticiasUnicaService,
    private obtenerProyetoPerfilService: ObtenerProyetoPerfilService,
    private obtenerPensamientosPerfilService: ObtenerPensamientosPerfilService,
    private obtenerNoticiasPerfilService: ObtenerNoticiasPerfilService,
    private eliminarParticipanteAsociacionService: EliminarParticipanteAsociacionService,
    private obtenerTodosParticipantePerfilService: ObtenerTodosParticipantePerfilService,
    private firebaseNotificacionService: FirebaseNotificacionService,
    private obtenerDispositivoUsuarioService: ObtenerDispositivoUsuarioService,
    private obtenerIntercambioPerfilService: ObtenerIntercambioPerfilService,
    private eliminarIntercambioUnicoService: EliminarIntercambioUnicoService,
    private eliminarTelefonoService: EliminarTelefonoService,
  ) {}

  async eliminarPerfil(
    data: EliminarPerfilDto,
    idioma,
    opts?: any,
  ): Promise<any> {
    let session;
    let optsLocal = false;
    if (!opts) {
      // Inicia proceso de transaccion
      session = await mongoose.startSession();
      await session.startTransaction();
      optsLocal = true;
      opts = { session };
    }

    try {
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.eliminar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.perfiles,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      const idPerfil = data._id;

      //______verificar perfil__________
      const existePerfil = await this.perfilModel.findOne({ _id: idPerfil });
      if (!existePerfil) {
        throw {
          codigo: HttpStatus.NOT_FOUND,
          codigoNombre: erroresPerfil.PERFIL_NO_EXISTE,
        };
      }

      if (
        data.dispositivo &&
        existePerfil.usuario.toString() !== data.dispositivo.usuario.toString()
      ) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }

      if (!this.verificarCodigos(data.estado.codigo)) {
        return null;
      }
      // ___________ verificar proyecto __________
      const checkEstadoProyecto = await this.obtenerProyetoPerfilService.obtenerProyectoNoEliminar(
        idPerfil,
      );

      if (checkEstadoProyecto.length > 0) {
        throw {
          codigo: HttpStatus.NOT_ACCEPTABLE,
          codigoNombre: erroresGeneral.ERROR_ELIMINAR,
        };
      }
      let hibernado: any = false;

      if (estadosPerfil.hibernado === data.estado.codigo) {
        hibernado = true;
      }

      const datosPerfil = await this.allDatosPerfilEliminar(idPerfil);

      // _________________album_____________________
      if (datosPerfil.album) {
        for (const albm of datosPerfil.album) {
          const dataAlbum = {
            _id: albm['_id'],
            usuario: existePerfil.usuario,
          };
          await this.eliminarAlbumService.eliminarAlbum(
            dataAlbum,
            hibernado,
            opts,
          );
        }
      }

      // _________________telefono_____________________
      if (datosPerfil.telefonos) {
        for (const telefono of datosPerfil.telefonos) {
          const dataTelefono = {
            _id: telefono['_id'],
            usuario: existePerfil.usuario,
          };
          await this.eliminarTelefonoService.eliminarTelefono(
            dataTelefono,
            hibernado,
            opts,
          );
        }
      }

      // _________________direcciones_____________________
      if (datosPerfil.direcciones) {
        for (const direccion of datosPerfil.direcciones) {
          const dataDireccion = {
            _id: direccion['_id'],
            usuario: existePerfil.usuario,
          };
          await this.eliminarDireccionService.eliminarDireccion(
            dataDireccion,
            hibernado,
            opts,
          );
        }
      }

      // _________________proyectos_____________________
      const getProyectos = await this.obtenerProyetoPerfilService.obtenerProyectosPerfil(
        idPerfil,
        idioma,
      );
      if (getProyectos) {
        for (const proyecto of getProyectos) {
          if (hibernado) {
            await this.eliminarProyectoUnicoService.hibernarProyecto(
              idPerfil,
              proyecto['_id'],
              opts,
            );
          } else {
            await this.eliminarProyectoUnicoService.eliminarProyectoUnico(
              idPerfil,
              proyecto['_id'],
              opts,
            );
          }
        }
      }

      // _________________ intercambios _____________________
      const getIntercambios = await this.obtenerIntercambioPerfilService.obtenerIntercambiosPerfil(
        idPerfil,
        idioma,
      );
      if (getIntercambios) {
        for (const intercambio of getIntercambios) {
          await this.eliminarIntercambioUnicoService.eliminarIntercambioUnico(
            idPerfil,
            intercambio['_id'],
            opts,
          );
        }
      }

      // _________________pensamientos_____________________
      const getPensamientos = await this.obtenerPensamientosPerfilService.obtenerPensamientosPorPerfil(
        idPerfil,
      );
      if (getPensamientos) {
        for (const pensamiento of getPensamientos) {
          if (hibernado) {
            await this.eliminarPensamientoService.hibernarPensamiento(
              pensamiento['_id'],
              opts,
            );
          } else {
            await this.eliminarPensamientoService.eliminarPensamiento(
              pensamiento['_id'],
              opts,
            );
          }
        }
      }

      // _________________noticias_____________________
      const getNoticias = await this.obtenerNoticiasPerfilService.obtenerNoticiasPerfil(
        idPerfil,
      );
      if (getNoticias) {
        for (const noticia of getNoticias) {
          if (hibernado) {
            await this.eliminarNoticiasUnicaService.hibernarNoticia(
              idPerfil,
              noticia['_id'],
              opts,
            );
          } else {
            await this.eliminarNoticiasUnicaService.eliminarNoticiaUnica(
              idPerfil,
              noticia['_id'],
              opts,
            );
          }
        }
      }
      // _________________participante asociacion_____________________
      if (hibernado) {
        await this.eliminarParticipanteAsociacionService.hibernarParticipante(
          idPerfil,
          opts,
        );
      } else {
        //______________eliminar_______________________
        await this.eliminarParticipanteAsociacionService.eliminarParticipante(
          idPerfil,
          opts,
        );
      }

      // _______________perfil___________________
      const dataPerfil = {
        estado:
          estadosPerfil.eliminado === data.estado.codigo
            ? estadosPerfil.eliminado
            : estadosPerfil.hibernado,
      };

      await this.perfilModel.findByIdAndUpdate(idPerfil, dataPerfil, opts);
      const updatePerfil = await this.perfilModel
        .findById(idPerfil)
        .session(opts.session);

      // _______________historico___________________
      const dataPerfilHistorico = JSON.parse(JSON.stringify(updatePerfil));
      //eliminar parametro no necesarios
      delete dataPerfilHistorico.fechaCreacion;
      delete dataPerfilHistorico.fechaActualizacion;
      delete dataPerfilHistorico.__v;

      const dataHistoricoPerfil: any = {
        datos: dataPerfilHistorico,
        usuario: existePerfil.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoPerfil);

      // _____________notificar el cambio del estado del perfil a firebase_____________________
      if (optsLocal) {
        const dataPerfilNotificar = {
          _id: updatePerfil._id.toString(),
          nombre: updatePerfil.nombre,
          nombreContacto: updatePerfil.nombreContacto,
          estado: { codigo: updatePerfil.estado },
          usuario: {
            _id: updatePerfil.usuario.toString(),
          },
        };

        let dataDispNotif,
          listDispositivos = [];
        const getDispositivos: any = await this.obtenerDispositivoUsuarioService.getDispositivosUsuario(
          existePerfil.usuario.toString(),
        );
        for (const disp of getDispositivos) {
          if (disp._id.toString() === data.dispositivo._id.toString()) {
            dataDispNotif = {
              idDispositivo: disp._id.toString(),
              leido: true,
            };
          } else {
            dataDispNotif = {
              idDispositivo: disp._id.toString(),
              leido: false,
            };
          }
          listDispositivos = [...listDispositivos, dataDispNotif];
        }

        const dataNotificacionPerfil: FBNotificacion = {
          codEntidad: codigoEntidades.entidadPerfiles,
          idEntidad: updatePerfil._id.toString(),
          data: dataPerfilNotificar,
          leido: false,
          estado: estadoNotificacionesFirebase.activa,
          accion: accionNotificacionFirebase.ver,
          tipo: tipoNotificacionFirebase.sistema,
          query: `${codigoEntidades.entidadPerfiles}-${false}`,
          dispositivos: listDispositivos,
        };

        this.firebaseNotificacionService.createDataNotificacion(
          dataNotificacionPerfil,
          codigoEntidades.entidadUsuarios,
          updatePerfil.usuario.toString(),
        );
      }

      if (optsLocal) {
        await session.commitTransaction();
        await session.endSession();
      }

      return { estado: hibernado };
    } catch (error) {
      if (optsLocal) {
        await session.abortTransaction();
        session.endSession();
      }
      throw error;
    }
  }

  verificarCodigos(codigo) {
    let codigos = [estadosPerfil.eliminado, estadosPerfil.hibernado];
    if (codigos.indexOf(codigo) !== -1) {
      return true;
    } else {
      return false;
    }
  }

  async allDatosPerfilEliminar(idPerfil) {
    const perfil = await this.perfilModel
      .findOne({ _id: idPerfil })
      .select('-fechaCreacion -fechaActualizacion -__v')
      .populate([
        {
          path: 'album',
          select: '-fechaCreacion -fechaActualizacion -__v',
          populate: [
            {
              path: 'media',
              select: '-fechaCreacion -fechaActualizacion -__v',
              match: { estado: codigosEstadosMedia.activa },
              populate: [
                {
                  path: 'principal',
                  select: 'url',
                },
                {
                  path: 'miniatura',
                  select: 'url',
                },
                {
                  path: 'traducciones',
                  select: 'nombre',
                },
              ],
            },
            {
              path: 'portada',
              select: '-fechaCreacion -fechaActualizacion -__v',
              populate: [
                {
                  path: 'principal',
                  select: 'url',
                },
                {
                  path: 'miniatura',
                  select: 'url',
                },
              ],
            },
          ],
        },
        {
          path: 'direcciones',
          select: 'estado',
        },
        {
          path: 'telefonos',
          select: 'estado',
        },
        {
          path: 'proyectos',
          select: 'estado',
        },
        {
          path: 'pensamientos',
          select: 'estado',
        },
        {
          path: 'noticias',
          select: 'estado',
        },
        {
          path: 'asociaciones',
          select: 'estado',
        },
      ]);
    return perfil;
  }
}
