import { Inject, Injectable, HttpStatus } from '@nestjs/common';
import { Model, startSession } from 'mongoose';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import * as mongoose from 'mongoose';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
  propiedadesPerfil,
} from 'src/shared/enum-sistema';

@Injectable()
export class AgregarReferenciaPerfilService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('TIPO_PERFIL_MODEL')
    private readonly tipoPerfilModelo: Model<TipoPerfil>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async agregarReferencia(
    idPerfil,
    propiedad,
    idPropiedad,
    opts: any,
  ): Promise<any> {
    try {
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion['codigo'];

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.perfiles,
      );
      const codEntidad = entidad['codigo'];

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );
      const codEstado = estado['codigo'];

      //verificar id
      if (!mongoose.isValidObjectId(idPerfil)) {
        throw {
          codigoEstado: HttpStatus.NOT_FOUND,
          message: 'id no valido',
        };
      }
      //______verificar perfil__________
      const existePerfil: any = await this.perfilModel
        .findOne({ _id: idPerfil })
        .populate('album');
      if (!existePerfil) {
        console.error('Error no existe el perfil');
        throw {
          message: 'Error no existe el perfil',
        };
      }
      // _______________perfil___________________
      let updatePerfil;

      switch (propiedad) {
        case propiedadesPerfil.pensamientos:
          updatePerfil = await this.perfilModel.updateOne(
            { _id: idPerfil },
            { $push: { pensamientos: idPropiedad } },
            opts,
          );
          break;
        case propiedadesPerfil.noticias:
          updatePerfil = await this.perfilModel.updateOne(
            { _id: idPerfil },
            { $push: { noticias: idPropiedad } },
            opts,
          );
          break;
        case propiedadesPerfil.proyectos:
          updatePerfil = await this.perfilModel.updateOne(
            { _id: idPerfil },
            { $push: { proyectos: idPropiedad } },
            opts,
          );
          break;
        case propiedadesPerfil.asociaciones:
          updatePerfil = await this.perfilModel.updateOne(
            { _id: idPerfil },
            { $push: { asociaciones: idPropiedad } },
            opts,
          );
          break;
        case propiedadesPerfil.telefonos:
          updatePerfil = await this.perfilModel.updateOne(
            { _id: idPerfil },
            { $push: { telefonos: idPropiedad } },
            opts,
          );
          break;
        case propiedadesPerfil.direcciones:
          updatePerfil = await this.perfilModel.updateOne(
            { _id: idPerfil },
            { $push: { direcciones: idPropiedad } },
            opts,
          );
          break;
        case propiedadesPerfil.album:
          updatePerfil = await this.perfilModel.updateOne(
            { _id: idPerfil },
            { $push: { album: idPropiedad } },
            opts,
          );
          break;
      }

      // _______________historico___________________
      const dataPerfilHistorico = JSON.parse(JSON.stringify(updatePerfil));
      //eliminar parametro no necesarios
      delete dataPerfilHistorico.fechaCreacion;
      delete dataPerfilHistorico.fechaActualizacion;
      delete dataPerfilHistorico.__v;

      const dataHistoricoPerfil: any = {
        datos: dataPerfilHistorico,
        usuario: existePerfil.usuario,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de usuario
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoPerfil);

      const result = await this.perfilModel
        .findById({ _id: idPerfil })
        .session(opts.session);
      return result;
    } catch (error) {
      throw error;
    }
  }
}
