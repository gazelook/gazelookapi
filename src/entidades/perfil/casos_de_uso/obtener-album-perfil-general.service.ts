import { Injectable, Inject, Res } from '@nestjs/common';
import { Model } from 'mongoose';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { ListaArchivosDefaultTipoService } from 'src/entidades/archivo/casos_de_uso/lista-archivos-default-tipo.service';
import {
  codigosArchivosSistema,
  codigosCatalogoAlbum,
  codigosCatalogoMedia,
  codigosTiposPerfil,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class GetFotoPerfilService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly PerfilModel: Model<Perfil>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private listaArchivosDefaultTipoService: ListaArchivosDefaultTipoService,
  ) {}

  async getFotoPerfil(
    idPerfil: string,
    albumPerfilGeneral?: boolean,
  ): Promise<any> {
    try {
      //Obtiene la entidad perfiles
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.perfiles,
      );
      let codEntidadPerfiles = entidad.codigo;

      //Obtiene el estado activa de la entidad perfiles
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadPerfiles,
      );
      let codEstadoPerfiles = estado.codigo;

      //Obtiene la entidad album
      const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.album,
      );
      let codEntidadAlbum = entidadAlbum.codigo;

      //Obtiene el estado activa de la entidad album
      const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadAlbum,
      );
      let codEstadoAlbum = estadoAlbum.codigo;

      //Obtiene la entidad media
      const entidadMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadMedia.codigo;

      //Obtiene el estado activa de la entidad media
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene la entidad archivo
      const entidadArchivo = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.archivo,
      );
      let codEntidadArchivo = entidadArchivo.codigo;

      //Obtiene el estado activa de la entidad archivo
      const estadoArchivo = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadArchivo,
      );
      let codEstadoArchivo = estadoArchivo.codigo;

      const getPerfil = await this.PerfilModel.findOne({
        _id: idPerfil,
        estado: codEstadoPerfiles,
      })
        .select('nombre nombreContacto tipoPerfil album')
        .populate({
          path: 'album',
          select: 'portada tipo',
          match: {
            estado: codEstadoAlbum,
          },
          populate: {
            path: 'portada',
            select: 'principal miniatura catalogoMedia',
            match: {
              estado: codEstadoMedia,
            },
            populate: [
              {
                path: 'principal',
                select: 'url fileDefault duracion fechaActualizacion',
                match: {
                  estado: codEstadoArchivo,
                },
              },
              {
                path: 'miniatura',
                select: 'url fileDefault duracion fechaActualizacion',
                match: {
                  estado: codEstadoArchivo,
                },
              },
            ],
          },
        });
      if (getPerfil) {
        let tipPerfil = getPerfil.tipoPerfil;
        let tipoAlbumGeneral = false;
        let tipoAlbumPerfil = false;
        let getPortadaAlbumGeneral: any;
        let getPortadaAlbumPerfil: any;
        let getUrlAlbumPerfil: any;
        let getUrlAlbumGeneral: any;
        let getCatMediaAlbumGeneral: any;
        let getCatMediaAlbumPerfil: any;
        let getTipoAlbumGeneral: any;
        let getTipoAlbumPerfil: any;
        let getArchivoDefaultAlbGeneral: any;
        let getArchivoDefaultAlbPerfil: any;
        let getFileDefault: any;

        if (albumPerfilGeneral) {
          // BUSCAR PERFIL Y GENERAL
          const listaAlbum = getPerfil.album.filter(
            element =>
              element['tipo'] == codigosCatalogoAlbum.catAlbumPerfil ||
              element['tipo'] == codigosCatalogoAlbum.catAlbumGeneral,
          );

          if (listaAlbum.length) {
            for (const album of listaAlbum) {
              if (album['tipo'] == codigosCatalogoAlbum.catAlbumPerfil) {
                tipoAlbumPerfil = true;
                getPortadaAlbumPerfil = album['portada'];
                getTipoAlbumPerfil = album['tipo'];

                if (getPortadaAlbumPerfil) {
                  getCatMediaAlbumPerfil = album['portada'].catalogoMedia;
                  getUrlAlbumPerfil = album['portada'].principal.url;
                  getArchivoDefaultAlbPerfil =
                    album['portada'].principal.fileDefault;
                }
              }

              if (album['tipo'] == codigosCatalogoAlbum.catAlbumGeneral) {
                tipoAlbumGeneral = true;
                getPortadaAlbumGeneral = album['portada'];
                getTipoAlbumGeneral = album['tipo'];
                if (getPortadaAlbumGeneral) {
                  getCatMediaAlbumGeneral = album['portada'].catalogoMedia;
                  getUrlAlbumGeneral = album['portada'].principal.url;
                  getArchivoDefaultAlbGeneral =
                    album['portada'].principal.fileDefault;
                }
              }
            }

            let objAlbumTipoPerfil;
            let objAlbumTipoGeneral;

            if (tipoAlbumPerfil) {
              objAlbumTipoPerfil = await this.obtenerImagenAlbumPerfil(
                tipPerfil,
                idPerfil,
                getPortadaAlbumPerfil,
                getTipoAlbumPerfil,
                getCatMediaAlbumPerfil,
                getUrlAlbumPerfil,
                getArchivoDefaultAlbPerfil,
              );
            } else {
              objAlbumTipoPerfil = await this.obtenerImagenDefectoAlbumPerfil();
            }

            if (tipoAlbumGeneral) {
              objAlbumTipoGeneral = await this.obtenerImagenAlbumGeneral(
                getPortadaAlbumGeneral,
                getTipoAlbumGeneral,
                getCatMediaAlbumGeneral,
                getUrlAlbumGeneral,
                getArchivoDefaultAlbGeneral,
              );
            } else {
              objAlbumTipoGeneral = await this.obtenerImagenDefectoAlbumGeneral();
            }
            return {
              objAlbumTipoPerfil,
              objAlbumTipoGeneral,
            };
          } else {
            return {
              objAlbumTipoPerfil: await this.obtenerImagenDefectoAlbumPerfil(),
              objAlbumTipoGeneral: await this.obtenerImagenDefectoAlbumGeneral(),
            };
          }
        } else {
          const albumPerfil = getPerfil.album.find(
            element => element['tipo'] == codigosCatalogoAlbum.catAlbumPerfil,
          );

          if (albumPerfil) {
            tipoAlbumPerfil = true;
            getPortadaAlbumPerfil = albumPerfil['portada'];
            getTipoAlbumPerfil = albumPerfil['tipo'];
            if (getPortadaAlbumPerfil) {
              getCatMediaAlbumPerfil = getPortadaAlbumPerfil.catalogoMedia;
              getUrlAlbumPerfil = getPortadaAlbumPerfil.principal['url'];
              getArchivoDefaultAlbPerfil =
                getPortadaAlbumPerfil.principal['fileDefault'];
            }
            return {
              objAlbumTipoPerfil: await this.obtenerImagenAlbumPerfil(
                tipPerfil,
                idPerfil,
                getPortadaAlbumPerfil,
                getTipoAlbumPerfil,
                getCatMediaAlbumPerfil,
                getUrlAlbumPerfil,
                getArchivoDefaultAlbPerfil,
              ),
            };
          } else {
            return {
              objAlbumTipoPerfil: await this.obtenerImagenDefectoAlbumPerfil(),
            };
          }
        }
      } else {
        return null;
      }
    } catch (error) {
      throw error;
    }
  }

  async getFotoAleatoriaListMedias(idPerfil: string): Promise<any> {
    //Obtiene la entidad album
    const entidadAlbum = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.album,
    );
    let codEntidadAlbum = entidadAlbum.codigo;

    //Obtiene el estado activa de la entidad album
    const estadoAlbum = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadAlbum,
    );
    let codEstadoAlbum = estadoAlbum.codigo;

    //Obtiene la entidad media
    const entidadMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.media,
    );
    let codEntidadMedia = entidadMedia.codigo;

    //Obtiene el estado activa de la entidad media
    const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadMedia,
    );
    let codEstadoMedia = estadoMedia.codigo;

    //Obtiene la entidad archivo
    const entidadArchivo = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.archivo,
    );
    let codEntidadArchivo = entidadArchivo.codigo;

    //Obtiene el estado activa de la entidad archivo
    const estadoArchivo = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      codEntidadArchivo,
    );
    let codEstadoArchivo = estadoArchivo.codigo;

    const getPerfil = await this.PerfilModel.findOne({ _id: idPerfil })
      .select('nombre nombreContacto tipoPerfil album')
      .populate({
        path: 'album',
        select: 'portada tipo',
        match: {
          estado: codEstadoAlbum,
        },
        populate: {
          path: 'media',
          select: 'principal miniatura',
          match: {
            estado: codEstadoMedia,
          },
          populate: [
            {
              path: 'principal',
              select: 'url fileDefault',
              match: {
                estado: codEstadoArchivo,
              },
            },
            {
              path: 'miniatura',
              select: 'url fileDefault',
              match: {
                estado: codEstadoArchivo,
              },
            },
          ],
        },
      });

    let tipoAlbumPerfil: any;
    let getUrlPrincipal: any;
    let getFileDefault: any;
    let media = [];
    for (let album of getPerfil.album) {
      let tipoAlbum = album['tipo'];
      if (tipoAlbum === codigosCatalogoAlbum.catAlbumPerfil) {
        tipoAlbumPerfil = true;
        if (album['media'].length > 0) {
          let count = album['media'].length;
          count = count - 1;

          //Generar aleatorio de medias
          let alet = Math.round(Math.random() * count);
          let getMediaAlet = album['media'][alet];
          getUrlPrincipal = getMediaAlet.principal.url;
          getFileDefault = getMediaAlet.principal.fileDefault;
          break;
        }
      }
    }
    let objArchivo = {
      getUrlPrincipal: getUrlPrincipal,
      getFileDefault: getFileDefault,
    };
    return objArchivo;
  }

  async obtenerImagenDefectoAlbumPerfil() {
    const getArchivoFotoPerfil = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
      codigosArchivosSistema.archivo_default_foto_perfil,
    );

    return {
      tipo: {
        codigo: codigosCatalogoAlbum.catAlbumPerfil,
      },
      portada: {
        catalogoMedia: getArchivoFotoPerfil.catalogoMedia,
        principal: {
          url: getArchivoFotoPerfil.principal.url,
          fileDefault: getArchivoFotoPerfil.principal.fileDefault,
        },
      },
    };
  }

  async obtenerImagenDefectoAlbumGeneral() {
    const getArchivoAlbumGeneral = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
      codigosArchivosSistema.archivo_default_album_general,
    );

    return {
      tipo: {
        codigo: codigosCatalogoAlbum.catAlbumGeneral,
      },
      portada: {
        catalogoMedia: getArchivoAlbumGeneral.catalogoMedia,
        principal: {
          url: getArchivoAlbumGeneral.principal.url,
          fileDefault: getArchivoAlbumGeneral.principal.fileDefault,
        },
      },
    };
  }

  async obtenerImagenAlbumPerfil(
    tipPerfil,
    idPerfil,
    getPortadaAlbumPerfil,
    getTipoAlbumPerfil,
    getCatMediaAlbumPerfil,
    getUrlAlbumPerfil,
    getArchivoDefaultAlbPerfil,
  ) {
    //2.1) Para el album de tipo perfil:
    // - Si el perfil es de tipo Ludico:
    if (tipPerfil === codigosTiposPerfil.tipoPerfilLudico) {
      //- Se debe establecer como portada del album en el objeto de respuesta, una foto aleatoria de la lista de medias del album.
      const getUrlAlbumPerfil = await this.getFotoAleatoriaListMedias(idPerfil);
      return {
        tipo: {
          codigo: codigosCatalogoAlbum.catAlbumPerfil,
        },
        portada: {
          catalogoMedia: {
            codigo: codigosCatalogoMedia.catMediaSimple,
          },
          principal: {
            url: getUrlAlbumPerfil.getUrlPrincipal,
            fileDefault: getUrlAlbumPerfil.getFileDefault,
          },
        },
      };
      //Si el perfil es clasico, grupo o substituto:
    } else {
      //- Si el album no tiene portada definida
      if (!getPortadaAlbumPerfil) {
        //se debe establecer una de origen aleatorio a partir de los archivos por defecto de tipo fotoperfil (la imagen de la cara)
        const getArchivoFotoPerfil = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
          codigosArchivosSistema.archivo_default_foto_perfil,
        );

        //Data para el mapeo del album solo con foto principal
        return {
          tipo: {
            codigo: codigosCatalogoAlbum.catAlbumPerfil,
          },
          portada: {
            catalogoMedia: getArchivoFotoPerfil.catalogoMedia,
            principal: {
              url: getArchivoFotoPerfil.principal.url,
              fileDefault: getArchivoFotoPerfil.principal.fileDefault,
            },
          },
        };
        //Si el abum si tiene portada definida
      } else {
        //Data para el mapeo del album solo con foto
        return {
          tipo: {
            codigo: getTipoAlbumPerfil,
          },
          portada: {
            catalogoMedia: {
              codigo: getCatMediaAlbumPerfil,
            },
            principal: {
              url: getUrlAlbumPerfil,
              fileDefault: getArchivoDefaultAlbPerfil,
            },
          },
        };
      }
    }
  }

  async obtenerImagenAlbumGeneral(
    getPortadaAlbumGeneral,
    getTipoAlbumGeneral,
    getCatMediaAlbumGeneral,
    getUrlAlbumGeneral,
    getArchivoDefaultAlbGeneral,
  ) {
    // 3.2) Para el album de tipo general:
    //- Si el album no tiene portada definida
    if (!getPortadaAlbumGeneral) {
      //se debe establecer una de origen aleatorio a partir de los archivos por defecto de tipo album general
      const getArchivoAlbumGeneral = await this.listaArchivosDefaultTipoService.getArchivoDefaultTipoAleatorio(
        codigosArchivosSistema.archivo_default_album_general,
      );

      return {
        tipo: {
          codigo: codigosCatalogoAlbum.catAlbumGeneral,
        },
        portada: {
          catalogoMedia: getArchivoAlbumGeneral.catalogoMedia,
          principal: {
            url: getArchivoAlbumGeneral.principal.url,
            fileDefault: getArchivoAlbumGeneral.principal.fileDefault,
          },
        },
      };
      //Si el album si tiene portada definida
    } else {
      return {
        tipo: {
          codigo: getTipoAlbumGeneral,
        },
        portada: {
          catalogoMedia: {
            codigo: getCatMediaAlbumGeneral,
          },
          principal: {
            url: getUrlAlbumGeneral,
            fileDefault: getArchivoDefaultAlbGeneral,
          },
        },
      };
    }
  }
}
