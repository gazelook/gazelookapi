import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { Perfil } from '../../../drivers/mongoose/interfaces/perfil/perfil.interface';
import { GetFotoPerfilService } from './obtener-album-perfil-general.service';

@Injectable()
export class ObtenerAlbumPerfilService {
  constructor(
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    private getFotoPerfilService: GetFotoPerfilService,
  ) {}

  async obtenerAlbumPerfil(idPerfil: string): Promise<any> {
    try {
      const getPerfil = await this.perfilModel.findOne({ _id: idPerfil });
      const albumPerfilGeneralFoto = await this.getFotoPerfilService.getFotoPerfil(
        idPerfil,
        true,
      );
      console.log('albumPerfilGeneralFoto: ', albumPerfilGeneralFoto);
      let albumPerfil = albumPerfilGeneralFoto.objAlbumTipoPerfil;

      let result = {
        nombreContacto: getPerfil.nombreContacto,
        album: albumPerfil,
      };
      return result;
    } catch (error) {
      throw error;
    }
  }
}
