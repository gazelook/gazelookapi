import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CrearAsociacionService } from './../casos_de_uso/crear-asociacion.service';
import { AsociacionDto } from './../entidad/asociacion-dto';

@ApiTags('Asociacion')
@Controller('api/asociacion')
export class CrearAsociacionControlador {
  constructor(
    private readonly crearAsociacionService: CrearAsociacionService,
    private readonly i18n: I18nService,
  ) {}

  funcion = new Funcion(this.i18n);

  @Post('/')
  @ApiBody({ type: AsociacionDto })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
  @ApiOperation({ summary: 'Crea una nueva asociacion' })
  @ApiResponse({ status: 201, description: 'Creación correcta' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @ApiResponse({ status: 406, description: 'Error al crear la asociacion' })
  @UseGuards(AuthGuard('jwt'))
  public async crearAsociacion(
    @Headers() headers,
    @Body() createAsoDTO: AsociacionDto,
  ) {
    let codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const asociacion = await this.crearAsociacionService.crearAsociacion(
        createAsoDTO,
      );

      if (asociacion) {
        return this.funcion.enviarRespuesta(
          HttpStatus.CREATED,
          await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'CREACION_CORRECTA',
          ),
          asociacion,
        );
      } else {
        return this.funcion.enviarRespuesta(
          HttpStatus.CONFLICT,
          await this.funcion.obtenerTraduccionEstatica(
            codIdioma,
            'ERROR_CREACION',
          ),
        );
      }
    } catch (e) {
      return this.funcion.enviarRespuesta(
        HttpStatus.INTERNAL_SERVER_ERROR,
        e.message,
      );
    }
  }
}
