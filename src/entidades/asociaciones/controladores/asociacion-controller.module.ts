import { AsociacionServicesModule } from './../casos_de_uso/asociacion-services.module';
import { Module } from '@nestjs/common';
import { CrearAsociacionControlador } from './crear-asociacion.controller';

@Module({
  imports: [AsociacionServicesModule],
  providers: [],
  exports: [],
  controllers: [CrearAsociacionControlador],
})
export class AsociacionControllerModule {}
