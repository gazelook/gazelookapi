import { Module } from '@nestjs/common';
import { AsociacionControllerModule } from './controladores/asociacion-controller.module';

@Module({
  imports: [AsociacionControllerModule],
  providers: [],
  controllers: [],
  exports: [],
})
export class AsociacionModule {}
