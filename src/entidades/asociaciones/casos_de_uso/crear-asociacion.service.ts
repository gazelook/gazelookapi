import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Asociacion } from 'src/drivers/mongoose/interfaces/asociacion/asociacion.interface';
import { CatalogoTipoAsociacion } from 'src/drivers/mongoose/interfaces/catalogo_tipo_asociacion/catalogo-tipo-asociacion.interface';
import { ObtenerParticipanteAsociacionService } from 'src/entidades/asociacion_participante/casos_de_uso/obtener-participante-asociacion.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  accionNotificacionFirebase,
  codigoEntidades,
  estadoNotificacionesFirebase,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
  nombreTipoAsociacion,
} from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';
import { FBNotificacion } from '../../../drivers/firebase/models/fb-notificacion.interface';
import { FirebaseNotificacionService } from '../../../drivers/firebase/services/firebase-notificacion.service';
import { GetFotoPerfilService } from '../../perfil/casos_de_uso/obtener-album-perfil-general.service';
import { ConocerExistenciaAsociacionService } from './../../asociacion_participante/casos_de_uso/conocer-existencia-asociacion.service';
import { CrearParticipanteAsociacionService } from './../../asociacion_participante/casos_de_uso/crear-participante-asociacion.service';
import { ModificiarParticipanteAsociacionService } from './../../asociacion_participante/casos_de_uso/modificar-participante-asociacion.service';
import { CrearConversacionService } from './../../conversacion/casos_de_uso/crear-conversacion.service';
import { AsociacionDto } from './../entidad/asociacion-dto';

@Injectable()
export class CrearAsociacionService {
  constructor(
    @Inject('ASOCIACION_MODEL')
    private readonly asociacionModel: Model<Asociacion>,
    @Inject('TIPO_ASO_MODEL')
    private readonly catalogoTipoAsociacionModel: Model<CatalogoTipoAsociacion>,
    // private conversacionModel: CrearConversacionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
    private particpanteService: CrearParticipanteAsociacionService,
    private conversacion: CrearConversacionService,
    private exitenciaParti: ConocerExistenciaAsociacionService,
    private particiAso: ObtenerParticipanteAsociacionService,
    private modifParticiaso: ModificiarParticipanteAsociacionService,
    private firebaseNotificacionService: FirebaseNotificacionService,
    private getFotoPerfilService: GetFotoPerfilService,
  ) {}

  funcion = new Funcion(
    null,
    this.catalogoEntidadService,
    this.catalogoEstadoService,
  );

  async crearAsociacion(crearAsoDto: AsociacionDto): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const catalogoAccionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      let codAccionCrear = catalogoAccionCrear.codigo;

      const catalogoAccionModificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const catalogoEstadoEntidadAsociacionActiva = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.asociacion,
        nombrecatalogoEstados.activa,
      );
      const catalogoEstadoEntidadConversacion = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.conversacion,
        nombrecatalogoEstados.activa,
      );

      const catalogoEstadoEntidadPartAsocEnviada = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.participanteAsociacion,
        nombrecatalogoEstados.enviada,
      );
      const catalogoEstadoEntidadPartAsocAceptada = await this.funcion.obtenerCatalogoEstadoEntidad(
        nombreEntidades.participanteAsociacion,
        nombrecatalogoEstados.aceptada,
      );

      const catalogoTipoAsociacion = await this.catalogoTipoAsociacionModel.findOne(
        { nombre: nombreTipoAsociacion.contacto },
      );
      const codTipAsoC = catalogoTipoAsociacion.codigo;

      if (crearAsoDto.tipo.codigo === codTipAsoC) {
        let idConversacion = new mongoose.mongo.ObjectId();

        let participantesAso = [];
        for (const iterator of crearAsoDto.participantes) {
          participantesAso.push(iterator['perfil']._id);
        }

        const idPerfilInvita = participantesAso[0];
        const idPerfilRecibeInvitacion = participantesAso[1];

        const conocerAso = await this.exitenciaParti.conocerExistenciaAsociacion(
          participantesAso,
        );

        if (!conocerAso) {
          // Si no existe la asociación con estos participantes
          const idParticipante1 = new mongoose.mongo.ObjectId();
          const idParticipante2 = new mongoose.mongo.ObjectId();

          let listaIdPartAsoc = [idParticipante1, idParticipante2];

          let crearAsoci = {
            _id: idConversacion,
            estado: catalogoEstadoEntidadAsociacionActiva.catalogoEstado.codigo,
            tipo: codTipAsoC,
            nombre: crearAsoDto.nombre,
            participantes: listaIdPartAsoc,
            conversacion: idConversacion,
          };

          const crearAsociacion = await new this.asociacionModel(
            crearAsoci,
          ).save(opts);

          let newHistoricoCrearTraduc: any = {
            datos: crearAsociacion,
            usuario: idPerfilInvita,
            accion: codAccionCrear,
            entidad:
              catalogoEstadoEntidadAsociacionActiva.catalogoEntidad.codigo,
          };
          this.crearHistoricoService.crearHistoricoServer(
            newHistoricoCrearTraduc,
          );

          const createPartici1 = {
            _id: idParticipante1,
            estado: catalogoEstadoEntidadPartAsocAceptada.catalogoEstado.codigo,
            perfil: idPerfilInvita,
            asociacion: crearAsociacion._id,
            invitadoPor: null,
            contactoDe: idPerfilRecibeInvitacion,
          };

          const crearParticipante1 = await this.particpanteService.crearParticipanteAsociacion(
            createPartici1,
            opts,
          );

          const createPartici2 = {
            _id: idParticipante2,
            estado: catalogoEstadoEntidadPartAsocEnviada.catalogoEstado.codigo,
            perfil: idPerfilRecibeInvitacion,
            asociacion: crearAsociacion._id,
            invitadoPor: idParticipante1,
            contactoDe: idPerfilInvita,
          };

          const crearParticipante2 = await this.particpanteService.crearParticipanteAsociacion(
            createPartici2,
            opts,
          );

          const crearConversacion = {
            _id: idConversacion,
            asociacion: crearAsociacion._id,
            estado: catalogoEstadoEntidadConversacion.catalogoEstado.codigo,
          };

          const conversacion = await this.conversacion.crearConversacion(
            crearConversacion,
            opts,
          );
          if (conversacion) {
            // ___________________________enviar notificacion______________________________
            await this.notificarParticipante(
              idPerfilRecibeInvitacion,
              idPerfilInvita,
              crearAsociacion._id,
              opts,
            );

            //Confirma los cambios de la transaccion
            await session.commitTransaction();
            //Finaliza la transaccion
            await session.endSession();

            const dataAsociacion = {
              _id: crearAsociacion._id,
            };
            return dataAsociacion;
          }
          /*  //Confirma los cambios de la transaccion
                    await session.commitTransaction();
                    //Finaliza la transaccion
                    await session.endSession();
                    return false; */
        } else {
          // Si existe ya la asociacion con estos participantes

          // actualizar el estado de la asociacion a activa
          await this.asociacionModel.findByIdAndUpdate(
            conocerAso.asociacion,
            {
              estado:
                catalogoEstadoEntidadAsociacionActiva.catalogoEstado.codigo,
            },
            opts,
          );

          let newHistoricoUpdateAso: any = {
            datos: catalogoAccionModificar.codigo,
            usuario: idPerfilInvita,
            accion: codAccionCrear,
            entidad:
              catalogoEstadoEntidadAsociacionActiva.catalogoEntidad.codigo,
          };

          this.crearHistoricoService.crearHistoricoServer(
            newHistoricoUpdateAso,
          );

          const partAsocInvita = await this.particiAso.obtenerParticipanteAsociacion(
            {
              conversacion: { _id: conocerAso.asociacion },
              perfil: { _id: idPerfilInvita },
            },
            opts,
          );

          const crearParticipante1 = await this.modifParticiaso.modificiarParticipanteAsociacion(
            {
              _id: partAsocInvita._id,
              estado:
                catalogoEstadoEntidadPartAsocAceptada.catalogoEstado.codigo,
              perfil: idPerfilInvita,
              asociacion: conocerAso.asociacion,
              invitadoPor: null,
              contactoDe: idPerfilRecibeInvitacion,
            },
            opts,
          );

          const partAsocRecibeInvitacion = await this.particiAso.obtenerParticipanteAsociacion(
            {
              conversacion: { _id: conocerAso.asociacion },
              perfil: { _id: idPerfilRecibeInvitacion },
            },
            opts,
          );

          const crearParticipante2 = await this.modifParticiaso.modificiarParticipanteAsociacion(
            {
              _id: partAsocRecibeInvitacion._id,
              estado:
                catalogoEstadoEntidadPartAsocEnviada.catalogoEstado.codigo,
              perfil: idPerfilRecibeInvitacion,
              asociacion: conocerAso.asociacion,
              invitadoPor: partAsocInvita._id,
              contactoDe: idPerfilInvita,
            },
            opts,
          );

          // ___________________________enviar notificacion______________________________
          await this.notificarParticipante(
            idPerfilRecibeInvitacion,
            idPerfilInvita,
            conocerAso.asociacion.toString(),
            opts,
          );

          //Confirma los cambios de la transaccion
          await session.commitTransaction();
          //Finaliza la transaccion
          await session.endSession();

          const dataAsociacion = {
            _id: conocerAso.asociacion,
          };
          return dataAsociacion;
        }
      }
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return false;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async notificarParticipante(idPerfil, idPerfilInvita, asociacion, opts) {
    let formatData,
      listParticipantes = [];
    let dataAlbum = [];
    const getAsociacion = await this.asociacionModel
      .findById(asociacion)
      .populate({
        path: 'participantes',
        select: 'perfil estado',
        populate: {
          path: 'perfil',
          select: 'nombreContacto',
        },
      })
      .select('estado tipo participantes')
      .session(opts.session);
    console.log('getAsociacion.participantes', getAsociacion.participantes);

    formatData = {
      id: getAsociacion._id.toString(),
      estado: {
        codigo: getAsociacion.estado,
      },
      tipo: {
        codigo: getAsociacion.tipo,
      },
    };
    const getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
      idPerfilInvita,
    );
    dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);

    for (const participante of getAsociacion.participantes) {
      if (participante['perfil']._id.toString() === idPerfilInvita.toString()) {
        const dataParticipante = {
          id: participante['_id'].toString(),
          estado: {
            codigo: participante['estado'],
          },
          perfil: {
            _id: participante['perfil']._id.toString(),
            nombreContacto: participante['perfil'].nombreContacto,
            album: dataAlbum,
          },
        };
        listParticipantes.push(dataParticipante);
      }
    }
    listParticipantes.length > 0
      ? (formatData.participantes = listParticipantes)
      : false;

    const dataNotificarParticipante: FBNotificacion = {
      idEntidad: getAsociacion._id.toString(),
      leido: false,
      codEntidad: codigoEntidades.entidadAsociacion,
      data: formatData,
      estado: estadoNotificacionesFirebase.activa,
      accion: accionNotificacionFirebase.ver,
      query: `${codigoEntidades.entidadAsociacion}-${false}`,
    };
    this.firebaseNotificacionService.createDataNotificacion(
      dataNotificarParticipante,
      codigoEntidades.entidadPerfiles,
      idPerfil,
    );
  }
}
