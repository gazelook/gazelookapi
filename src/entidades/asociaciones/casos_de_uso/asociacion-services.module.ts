import { ConversacionServicesModule } from './../../conversacion/casos_de_uso/conversacion-services.module';
import { ParticipanteAsociacionServicesModule } from './../../asociacion_participante/casos_de_uso/participante-asociacion-services.module';
import { CrearAsociacionService } from './crear-asociacion.service';
import { asociacionProviders } from './../drivers/asociacion.provider';
import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { EliminarAsociacionService } from './eliminar-asociacion.service';
import { ActualizarAsociacionService } from './actualizar-asociacion.service';
import { FirebaseModule } from '../../../drivers/firebase/firebase.module';
import { PerfilServiceModule } from '../../perfil/casos_de_uso/perfil.services.module';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    ParticipanteAsociacionServicesModule,
    ConversacionServicesModule,
    FirebaseModule,
    forwardRef(() => PerfilServiceModule),
  ],
  providers: [
    ...asociacionProviders,
    CrearAsociacionService,
    EliminarAsociacionService,
    ActualizarAsociacionService,
  ],
  exports: [
    CrearAsociacionService,
    EliminarAsociacionService,
    ActualizarAsociacionService,
  ],
  controllers: [],
})
export class AsociacionServicesModule {}
