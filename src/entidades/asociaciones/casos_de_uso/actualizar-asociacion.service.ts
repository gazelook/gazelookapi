import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { Asociacion } from '../../../drivers/mongoose/interfaces/asociacion/asociacion.interface';
import { nombreAcciones, nombreEntidades } from '../../../shared/enum-sistema';

@Injectable()
export class ActualizarAsociacionService {
  constructor(
    @Inject('ASOCIACION_MODEL')
    private readonly asociacionModel: Model<Asociacion>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async actualizarEstadoAsociacion(
    idAsociacion: string,
    codigoEstado: string,
    perfil: string,
    opts?: any,
  ): Promise<any> {
    try {
      const getAsociacion = await this.asociacionModel
        .findOne({ _id: idAsociacion })
        .session(opts.session);
      if (!getAsociacion) {
        return null;
      }

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      const getAccion = accion.codigo;

      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.asociacion,
      );
      const codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerEstadoByCodigo(
        codigoEstado,
      );
      const codEstado = estado.codigo;

      //actualizar estado
      const dataAsociacion = {
        estado: codEstado,
      };
      //console.log("getAsociacion", getAsociacion);

      await this.asociacionModel.findByIdAndUpdate(
        getAsociacion._id,
        dataAsociacion,
        opts,
      );

      const asociacionUpdated = await this.asociacionModel
        .findById(getAsociacion._id)
        .session(opts.session);

      const dataAlbumHistorico = JSON.parse(JSON.stringify(asociacionUpdated));
      //eliminar parametro no necesarios
      delete dataAlbumHistorico.fechaCreacion;
      delete dataAlbumHistorico.fechaActualizacion;
      delete dataAlbumHistorico.__v;

      const dataHistoricoAlbum: any = {
        datos: dataAlbumHistorico,
        usuario: perfil,
        accion: getAccion,
        entidad: codEntidad,
      };
      // crear el historico de asociacion
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoAlbum);

      return asociacionUpdated;
    } catch (error) {
      throw error;
    }
  }
}
