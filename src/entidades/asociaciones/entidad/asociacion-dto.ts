import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { PerfilIdDto } from '../../usuario/dtos/perfil.dto';
import { TipoAsociacionDto } from './tipo-asociacion.dto';

export class AsociacionDto {
  @ApiProperty()
  @IsNotEmpty()
  tipo: TipoAsociacionDto;

  @ApiProperty()
  @IsNotEmpty()
  nombre: string;

  @ApiProperty({
    description:
      'array de participantes con su perfil _id, la primera posicion es quien envia y la segunda quien recepta',
  })
  @IsOptional()
  participantes: Array<PerfilIdDto>;
}

export class AsociacionIdDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
}

export class AsociacionNombreDto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  nombre: string;
}
