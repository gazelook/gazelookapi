import { Connection } from 'mongoose';
import { AsociacionModelo } from '../../../drivers/mongoose/modelos/asociaciones/asociacion.schema';
import { CatalogoTipoAsociacionModelo } from './../../../drivers/mongoose/modelos/catalogo_tipo_asociacion/catalogo-tipo-asociacion.schema';

export const asociacionProviders = [
  {
    provide: 'ASOCIACION_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('asociacion', AsociacionModelo, 'asociacion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TIPO_ASO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_tipo_asociacion',
        CatalogoTipoAsociacionModelo,
        'catalogo_tipo_asociacion',
      ),
    inject: ['DB_CONNECTION'],
  },
];
