import { ApiProperty } from '@nestjs/swagger';
import { bool } from 'aws-sdk/clients/signer';

export class ComentarioDto {
  @ApiProperty()
  estado: string;
  @ApiProperty()
  tipo: string;
  @ApiProperty()
  adjuntos: [];
  @ApiProperty()
  coautor: string;
  @ApiProperty()
  importante: bool;
  @ApiProperty()
  traducciones: [];
}

export class identComentarioDto {
  @ApiProperty()
  _id: string;
}

export class ListaComentariosDto {
  @ApiProperty({ type: [identComentarioDto] })
  listaComentarios: Array<identComentarioDto>;
  // @ApiProperty({type:Number})
  // limite: number;
  // @ApiProperty({type:Number})
  // pagina: number;
}
