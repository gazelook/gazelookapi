import { ApiProperty } from '@nestjs/swagger';
import { MediaAdjuntos } from 'src/entidades/album/entidad/retorno-media-adjuntos.dto';
import { CoautorComentarioCompleto, Traduccion } from './crear-comentario-dto';

class Perfil {
  @ApiProperty()
  _id: string;
}
class ParticipanteProyectoDTO {
  @ApiProperty()
  coautor: Perfil;
}
class Coautor {
  @ApiProperty()
  participanteProyecto: ParticipanteProyectoDTO;
}

class Proyecto {
  @ApiProperty()
  _id: string;
  @ApiProperty()
  tipo: string;
}

class TipoComentario {
  @ApiProperty()
  nombre: string;
  @ApiProperty()
  codigo: string;
}

export class RespuestaComentarioDto {
  // @ApiProperty()
  // estado: string
  @ApiProperty({ type: [], example: ['5f9af1fa8e2c4f0724a7c434'] })
  adjuntos: [string];
  @ApiProperty({
    description: 'Participante que publico el comentario (id de perfil)',
    example: '5f9af1fa8e2c4f0724a7c434',
  })
  coautor: string;
  @ApiProperty({ description: 'Indica la prioridad del comentario' })
  importante: boolean;
  @ApiProperty({
    description: 'Catalogo tipo comentario',
    example: 'CATIPCOM_2',
  })
  tipo: string;
  @ApiProperty({
    description: 'Traducciones en diferentes idiomas del comentario',
    type: [],
    example: ['5f9af1fa8e2c4f0724a7c434'],
  })
  traducciones: [string];
  @ApiProperty({
    description: 'Id del proyecto al que pertenece el comentario',
    example: '5f9af1fa8e2c4f0724a7c434',
  })
  proyecto: string;
}

export class ObtencionComentarioDto {
  @ApiProperty({
    description: 'Identificador del comentario)',
    example: '5f9af1fa8e2c4f0724a7c434',
  })
  _id: string;
  @ApiProperty({ type: [MediaAdjuntos] })
  adjuntos: [MediaAdjuntos];
  @ApiProperty({ type: [Traduccion] })
  traducciones: [Traduccion];
  @ApiProperty({ type: [CoautorComentarioCompleto] })
  coautor: CoautorComentarioCompleto;
  @ApiProperty({
    description: 'Indica la prioridad del comentario',
    example: true,
  })
  importante: boolean;
  // @ApiProperty({type: TipoComentario})
  // tipo: TipoComentario;
  // @ApiProperty({ description: 'Id del proyecto al que pertenece el comentario', example:'5f9af1fa8e2c4f0724a7c434'})
  // proyecto: string;
  @ApiProperty({
    description: 'fecha que se creo el comentario',
    example: '2020-10-29T14:34:24.495Z',
  })
  fechaCreacion: Date;
  @ApiProperty({
    description: 'fecha que se actualizo el comentario',
    example: '2020-10-29T14:34:24.495Z',
  })
  fechaActualizacion: Date;
}
