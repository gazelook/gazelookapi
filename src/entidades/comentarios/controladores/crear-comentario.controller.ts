import {
  Body,
  Controller,
  Headers,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { CrearComentarioDto } from '../entidad/crear-comentario-dto';
import { RespuestaComentarioDto } from '../entidad/respuesta-comentario-dto';
import { CrearComentarioService } from './../casos_de_uso/crear-comentario.service';

@ApiTags('Comentarios')
@Controller('api/comentario')
export class CrearComentarioControlador {
  constructor(
    private readonly crearComentarioService: CrearComentarioService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}
  @Post('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Crea comentario en un proyecto' })
  @ApiResponse({
    status: 201,
    type: RespuestaComentarioDto,
    description: 'Creado',
  })
  @ApiResponse({ status: 404, description: 'Error al crear comentario' })
  @UseGuards(AuthGuard('jwt'))
  public async crearComentario(
    @Headers() headers,
    @Body() comentarioDto: CrearComentarioDto,
  ) {
    console.log('comentarioDto: ', comentarioDto);
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    let paramValidos = false;

    comentarioDto?.traducciones || comentarioDto?.adjuntos
      ? (paramValidos = true)
      : false;
    comentarioDto?.traducciones && comentarioDto?.adjuntos
      ? (paramValidos = false)
      : false;

    try {
      if (paramValidos) {
        if (
          mongoose.isValidObjectId(comentarioDto.proyecto._id) &&
          mongoose.isValidObjectId(comentarioDto.coautor.coautor._id)
        ) {
          const comentario = await this.crearComentarioService.comentar(
            comentarioDto,
            codIdioma,
          );
          if (comentario) {
            return this.funcion.enviarRespuestaOptimizada({
              codigoEstado: HttpStatus.CREATED,
              datos: comentario,
            });
          } else {
            const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(
              codIdioma,
              'ERROR_CREACION',
            );
            return this.funcion.enviarRespuestaOptimizada({
              codigoEstado: HttpStatus.NOT_FOUND,
              mensaje: ERROR_CREACION,
            });
          }
        } else {
          const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'PARAMETROS_NO_VALIDOS',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_ACCEPTABLE,
            mensaje: PARAMETROS_NO_VALIDOS,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
    }
  }
}
