import { ObtenerComentariosHistoricoControlador } from './obtener-comentarios-historico.controller';
import { EliminarMiComentarioControlador } from './eliminar-mi-comentario.controller';
import { ObtenerComentariosProyectoControlador } from './obtener-comentarios-proyecto.controller';
import { CrearComentarioControlador } from './crear-comentario.controller';
import { Module } from '@nestjs/common';
import { ComentariosServicesModule } from '../casos_de_uso/comentarios-services.module';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { EliminarComentarioControlador } from './eliminar-comentario.controller';
import { Funcion } from 'src/shared/funcion';
import { ObtenerComentariosByIdControlador } from './obtener-comentarios-by-id.controller';

@Module({
  imports: [ComentariosServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    CrearComentarioControlador,
    EliminarMiComentarioControlador,
    EliminarComentarioControlador,
    ObtenerComentariosProyectoControlador,
    ObtenerComentariosHistoricoControlador,
    ObtenerComentariosByIdControlador,
  ],
})
export class ComentariosControllerModule {}
