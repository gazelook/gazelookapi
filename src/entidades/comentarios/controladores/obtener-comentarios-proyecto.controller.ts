import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import * as mongoose from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { ObtenerComentariosService } from './../casos_de_uso/obtener-comentarios-proyecto.service';
import { ObtencionComentarioDto } from './../entidad/respuesta-comentario-dto';

@ApiTags('Comentarios')
@Controller('api/comentario')
export class ObtenerComentariosProyectoControlador {
  constructor(
    private readonly obtenerComentariosService: ObtenerComentariosService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}

  @Get('/')
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiOperation({ summary: 'Devuelve una lista de cometarios de un proyecto' })
  @ApiResponse({ status: 200, type: ObtencionComentarioDto, description: 'OK' })
  @ApiResponse({ status: 404, description: 'El proyecto no tiene comentarios' })
  @UseGuards(AuthGuard('jwt'))
  public async listarComentariosProyecto(
    @Headers() headers,
    @Query('idProyecto') idProyecto: string,
    @Query('limite') limite: number,
    @Query('pagina') pagina: number,
    @Res() response: Response,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      if (mongoose.isValidObjectId(idProyecto)) {
        const comentarios = await this.obtenerComentariosService.obtenerComentariosProyecto(
          idProyecto,
          codIdioma,
          limite,
          pagina,
          response,
        );

        if (comentarios) {
          //llama al metodo de traduccion estatica
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: comentarios,
          });
          response.send(respuesta);
          return respuesta;
        } else {
          const respuesta = this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: [],
          });
          response.send(respuesta);
          return respuesta;
        }
      } else {
        //llama al metodo de traduccion estatica
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_FOUND,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: e.message,
      });
      response.send(respuesta);
      return respuesta;
    }
  }
}
