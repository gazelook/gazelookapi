import { Controller, Delete, Headers, HttpStatus, Query } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { isValidObjectId } from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { EliminarMiComentarioService } from './../casos_de_uso/eliminar-mi-comentario.service';

@ApiTags('Comentarios')
@Controller('api/mi-comentario')
export class EliminarMiComentarioControlador {
  constructor(
    private readonly eliminarComentarioService: EliminarMiComentarioService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}
  @Delete('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Elimina mi comentario realizado en un proyecto' })
  @ApiResponse({ status: 200, description: 'Eliminación correcta' })
  @ApiResponse({ status: 406, description: 'El usuario no tiene comentarios' })
  // @ApiQuery({ name: 'idComentario', required: false})
  public async crearComentario(
    @Headers() headers,
    @Query('idComentario') idComentario: string,
    @Query('idCoautor') idCoautor?: string,
  ) {
    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

      if (isValidObjectId(idComentario)) {
        const eliminarMiCom = await this.eliminarComentarioService.eliminarComentario(
          idComentario,
          idCoautor,
        );
        if (eliminarMiCom) {
          const ELIMINACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ELIMINACION_CORRECTA',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        } else {
          const ERROR_ELIMINAR = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ERROR_ELIMINAR',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_ELIMINAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
    }
  }
}
