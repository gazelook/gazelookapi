import { Controller, Delete, Headers, HttpStatus, Query } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { EliminarComentarioService } from '../casos_de_uso/eliminar-comentario.service';

@ApiTags('Comentarios')
@Controller('api/comentario-proyecto')
export class EliminarComentarioControlador {
  constructor(
    private readonly eliminarComentarioService: EliminarComentarioService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion,
  ) {}
  @Delete('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({
    summary:
      'Elimina un comentario realizado a mi proyecto. Los comentarios eliminados se cambian a un estado historico ',
  })
  @ApiResponse({ status: 200, description: 'Eliminación correcta' })
  @ApiResponse({ status: 406, description: 'El usuario no tiene comentarios' })
  public async crearComentario(
    @Headers() headers,
    @Query('idProyecto') idProyecto: string,
    @Query('coautor') coautor: string,
    @Query('idComentario') idComentario: string,
  ) {
    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

      if (
        mongoose.isValidObjectId(idProyecto) &&
        mongoose.isValidObjectId(idComentario) &&
        mongoose.isValidObjectId(coautor)
      ) {
        const eliminarComentario = await this.eliminarComentarioService.eliminarComentario(
          idProyecto,
          coautor,
          idComentario,
        );
        if (eliminarComentario) {
          const ELIMINACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'ELIMINACION_CORRECTA',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ELIMINACION_CORRECTA,
          });
        } else {
          const FALLO_ELIMINAR = await this.traduccionEstaticaController.traduccionEstatica(
            codIdioma,
            'FALLO_ELIMINAR',
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: FALLO_ELIMINAR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'PARAMETROS_NO_VALIDOS',
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
    }
  }
}
