import { ComentariosControllerModule } from './controladores/comentarios-controller.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [ComentariosControllerModule],
  providers: [],
  controllers: [],
})
export class ComentariosModule {}
