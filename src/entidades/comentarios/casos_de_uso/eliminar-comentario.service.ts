import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Comentario } from 'src/drivers/mongoose/interfaces/comentario/comentario.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import {
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class EliminarComentarioService {
  constructor(
    @Inject('COMENTARIOS_MODEL')
    private readonly comentarioModel: Model<Comentario>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
  ) {}

  async eliminarComentario(
    idProyecto: string,
    coautor: string,
    idComentario: string,
    opts?: any,
  ): Promise<any> {
    try {
      const comentario = await this.comentarioModel.findOne({
        _id: idComentario,
      });

      if (comentario) {
        // entidad traduccion comentario
        const traduccionComentario = await this.catalogoEntidadService.obtenerNombreEntidad(
          nombreEntidades.traduccionComentario,
        );
        // const estadoTraduccion = await this.catalogoEstadoService.obtenerNombreEstado(
        //   'eliminado',
        //   traduccionComentario.codigo,
        // );

        // Entitad comentario
        const entidadComentario = await this.catalogoEntidadService.obtenerNombreEntidad(
          nombreEntidades.comentarios,
        );
        const estadoComentario = await this.catalogoEstadoService.obtenerNombreEstado(
          nombrecatalogoEstados.historico,
          entidadComentario.codigo,
        );
        const accionEliminar = await this.catalogoAccionService.obtenerNombreAccion(
          nombreAcciones.eliminar,
        );

        // const nuevoComentario: any = {
        //   estado: estado.codigo,
        // }

        // const traducionesEliminadas = await this.traduccionComentarioModel.updateMany(
        //   { referencia: idComentario },
        //   { $set: { estado: estadoTraduccion.codigo } },
        // );

        // const historicoTraduccion: any = {
        //   datos: traducionesEliminadas,
        //   usuario: comentario.coautor,
        //   accion: accionEliminar.codigo,
        //   entidad: traduccionComentario.codigo,
        //   tipo: '',
        // };
        // await this.crearHistoricoService.crearHistorico(historicoTraduccion);

        const comentarioEliminado = await this.comentarioModel.updateOne(
          {
            $and: [
              { _id: idComentario },
              { proyecto: idProyecto },
              { coautor: coautor },
            ],
          },
          { $set: { estado: estadoComentario.codigo } },
          opts,
        );

        //datos para guardar el historico
        const newHistorico: any = {
          datos: comentarioEliminado,
          usuario: comentarioEliminado.coautor,
          accion: accionEliminar.codigo,
          entidad: entidadComentario.codigo,
        };
        this.crearHistoricoService.crearHistoricoServer(newHistorico);
        return comentarioEliminado;
      } else {
        return false;
      }
    } catch (error) {
      throw error;
    }
  }
}
