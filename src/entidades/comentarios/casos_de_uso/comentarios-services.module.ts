import { ObtenerParticipanteProyectoService } from './../../participante_proyecto/casos_de_uso/obtener-participante-proyecto.service';
import { ParticipanteProyectoModule } from './../../participante_proyecto/participante-proyecto.module';
import { RolServiceModule } from './../../rol/casos_de_uso/rol.services.module';
import { participanteProyectoProviders } from './../../participante_proyecto/drivers/participante-proyecto.provider';
import { CrearParticipanteProyectoService } from './../../participante_proyecto/casos_de_uso/crear-participante-proyecto.service';
import { ObtenerComentariosHistoricoService } from './obtener-comentarios-historico.service';
import { ObtenerComentariosService } from './obtener-comentarios-proyecto.service';
import { CatalogoTipoComentarioService } from './../../catalogos/casos_de_uso/catalogo-tipo-comentario.service';
import { CrearComentarioService } from './crear-comentario.service';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { comentariosProviders } from '../drivers/comentarios.provider';
import { TraducirComentarioService } from './traducir-comentario.service';
import { EliminarMiComentarioService } from './eliminar-mi-comentario.service';
import { EliminarComentarioService } from './eliminar-comentario.service';
import { EliminarComentarioCompletoService } from './eliminar-comentario-completo.service';
import { AlbumServiceModule } from 'src/entidades/album/casos_de_uso/album.services.module';
import { GetFotoPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil-general.service';
import { PerfilServiceModule } from 'src/entidades/perfil/casos_de_uso/perfil.services.module';
import { Funcion } from 'src/shared/funcion';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { MediaServiceModule } from 'src/entidades/media/casos_de_uso/media.services.module';
import { ObtenerEstadoProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-estado-proyecto.service';
import { CatalogoProviders } from 'src/entidades/catalogos/drivers/catalogo.provider';
import { CatalogoColoresService } from 'src/entidades/catalogos/casos_de_uso/catalogo-colores.service';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { CatalogoConfiguracionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-configuracion.service';
import { ObtenerMediasProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-medias-proyecto.service';
import { ObtenerComentarioUnicoService } from './obtener-comentarios-unico-proyecto.service';
import { ArchivoServicesModule } from 'src/entidades/archivo/casos_de_uso/archivo-services.module';
import { ObtenerComentariosByIdService } from './obtener-comentarios-by-id.service';
import { FirebaseModule } from 'src/drivers/firebase/firebase.module';
import { TraducirComentarioSegundoPlanoService } from './traducir-comentario-segundo-plano.service';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    RolServiceModule,
    forwardRef(() => AlbumServiceModule),
    forwardRef(() => PerfilServiceModule),
    forwardRef(() => MediaServiceModule),
    forwardRef(() => ArchivoServicesModule),
    FirebaseModule,
  ],
  providers: [
    ...comentariosProviders,
    ...participanteProyectoProviders,
    ...CatalogoProviders,
    CrearComentarioService,
    EliminarMiComentarioService,
    EliminarComentarioService,
    TraducirComentarioService,
    CatalogoTipoComentarioService,
    ObtenerComentariosService,
    ObtenerComentariosHistoricoService,
    CrearParticipanteProyectoService,
    ObtenerParticipanteProyectoService,
    EliminarComentarioCompletoService,
    Funcion,
    ObtenerEstadoProyectoService,
    CatalogoColoresService,
    CatalogoEstilosService,
    CatalogoConfiguracionService,
    ObtenerMediasProyectoService,
    ObtenerComentarioUnicoService,
    ObtenerComentariosByIdService,
    TraducirComentarioSegundoPlanoService,
  ],
  exports: [
    ...comentariosProviders,
    ...participanteProyectoProviders,
    CrearComentarioService,
    EliminarMiComentarioService,
    EliminarComentarioService,
    TraducirComentarioService,
    CatalogoTipoComentarioService,
    ObtenerComentariosService,
    ObtenerComentariosHistoricoService,
    CrearParticipanteProyectoService,
    ObtenerParticipanteProyectoService,
    EliminarComentarioCompletoService,
    ObtenerComentarioUnicoService,
    ObtenerComentariosByIdService,
    TraducirComentarioSegundoPlanoService,
  ],
  controllers: [],
})
export class ComentariosServicesModule {}
