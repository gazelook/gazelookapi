import { TraduccionComentario } from 'src/drivers/mongoose/interfaces/traduccion_comentario/traduccion-comentario.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Comentario } from 'src/drivers/mongoose/interfaces/comentario/comentario.interface';
import { Model } from 'mongoose';
import { EliminarAlbumCompletoService } from 'src/entidades/album/casos_de_uso/eliminar-album-completo.service';

@Injectable()
export class EliminarComentarioCompletoService {
  constructor(
    @Inject('COMENTARIOS_MODEL')
    private readonly comentarioModel: Model<Comentario>,
    @Inject('TRADUCCION_COMENTARIO_MODEL')
    private readonly traduccionComentarioModel: Model<TraduccionComentario>,
    private eliminarAlbumCompletoService: EliminarAlbumCompletoService,
  ) {}

  async eliminarComentarioCompleto(comentario: any, opts): Promise<any> {
    try {
      let idComentario = comentario._id;
      // const comentario = await this.comentarioModel.findOne({ _id: idComentario });
      //Elimina el comentario
      await this.comentarioModel.deleteOne({ _id: idComentario }, opts);
      // if (comentario) {
      if (comentario.adjuntos.length > 0) {
        //Elimina todos los adjuntos uno a uno
        for (const getAdjuntos of comentario.adjuntos) {
          await this.eliminarAlbumCompletoService.eliminarAlbumCompleto(
            getAdjuntos,
            opts,
          );
        }
      }
      if (comentario.traducciones.length > 0) {
        //console.log('comentario.traducciones.length: ', comentario.traducciones.length)
        for (const traducciones of comentario.traducciones) {
          if (traducciones) {
            //Elimina todas las traducciones del comentario
            await this.traduccionComentarioModel.deleteOne(
              { _id: traducciones._id },
              opts,
            );
          }
        }
      }

      console.log('DESPUES DE ELIMINAR TRADUCCION COMENTARIOS');

      // }
      return true;
    } catch (error) {
      throw error;
    }
  }
}
