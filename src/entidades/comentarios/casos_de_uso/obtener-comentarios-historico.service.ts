import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model, PaginateModel } from 'mongoose';
import { Comentario } from 'src/drivers/mongoose/interfaces/comentario/comentario.interface';
import { TraduccionComentario } from 'src/drivers/mongoose/interfaces/traduccion_comentario/traduccion-comentario.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoColoresService } from 'src/entidades/catalogos/casos_de_uso/catalogo-colores.service';
import { CatalogoConfiguracionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-configuracion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { GetFotoPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil-general.service';
import { ObtenerMediasProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-medias-proyecto.service';
import {
  codIdiomas,
  codigoestadosTraaduccionComentario,
  idiomas,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { TraducirComentarioService } from './traducir-comentario.service';

@Injectable()
export class ObtenerComentariosHistoricoService {
  constructor(
    @Inject('COMENTARIOS_MODEL')
    private readonly comentarioModelo: PaginateModel<Comentario>,
    @Inject('TRADUCCION_COMENTARIO_MODEL')
    private readonly traduccionComentarioModel: Model<TraduccionComentario>,
    @Inject('PROYECTO_MODEL')
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private traducirComentarioService: TraducirComentarioService,
    private traduccionMediaService: TraduccionMediaService,
    private getFotoPerfilService: GetFotoPerfilService,
    private catalogoColoresService: CatalogoColoresService,
    private catalogoEstilosService: CatalogoEstilosService,
    private obtenerMediasProyectoService: ObtenerMediasProyectoService,
    private catalogoConfiguracionService: CatalogoConfiguracionService,
  ) {}

  async obtenerComentariosProyecto(
    proyecto: string,
    codigoEstado: string,
    idioma: string,
    limit: number,
    page: number,
    fechaInicial: any,
    fechaFinal: any,
    response?: any,
  ): Promise<any> {
    const headerNombre = new HadersInterfaceNombres();

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarios,
      );

      const entidadProyecto = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );

      const estadoProyecto = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidadProyecto.codigo,
      );
      const codIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );

      //Obtiene la entidad traduccion pensamientos
      const entidadTraducion = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionComentario,
      );

      //Obtiene el estado de activa traduccion pensamientos
      const estadoTraduccion = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidadTraducion.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad proyectos
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      //Obtiene el codigo de la entidad participante proyecto
      const entidadConfigEstilo = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.configuracionEstilo,
      );
      let codEntidadConfigEstilo = entidadConfigEstilo.codigo;

      //Obtiene el estado activo de la entidad participante proyecto
      const estadoConfigEstilo = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadConfigEstilo,
      );
      let codEstadoConfigEstilo = estadoConfigEstilo.codigo;

      const populateTraduccion = {
        path: 'traducciones',
        select: 'texto idioma original',
        match: {
          idioma: codIdioma.codigo,
        },
      };
      //estado: estadoTraduccion.codigo
      const adjuntos = {
        path: 'adjuntos',
        select: 'principal enlace miniatura catalogoMedia traducciones',
        match: { estado: codEstadoMedia },
        populate: [
          {
            path: 'traducciones',
            select: 'descripcion',
            match: { idioma: codIdioma.codigo, estado: codEstadoTradMedia },
          },
          {
            path: 'principal',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
          {
            path: 'miniatura',
            select: 'url tipo duracion fileDefault fechaActualizacion',
          },
        ],
      };

      const coautor = {
        path: 'coautor',
        select: 'coautor configuraciones',
        populate: [
          {
            path: 'coautor',
            select: 'nombre nombreContacto tipoPerfil album',
          },
          {
            path: 'configuraciones',
            select: 'codigo estilos tonoNotificacion tipo',
            match: { estado: codEstadoConfigEstilo },
            populate: [
              {
                path: 'estilos',
                select: 'codigo media color tipo',
                populate: [
                  {
                    path: 'media',
                    match: { estado: codEstadoMedia },
                    select:
                      'principal enlace miniatura traducciones catalogoMedia',
                    populate: [
                      {
                        path: 'traducciones',
                        select: 'descripcion',
                        match: {
                          idioma: codIdioma,
                          estado: codEstadoTradMedia,
                        },
                      },
                      {
                        path: 'principal',
                        select:
                          ' url tipo duracion fileDefault fechaActualizacion',
                      },
                      {
                        path: 'miniatura',
                        select:
                          ' url tipo duracion fileDefault fechaActualizacion',
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      };

      const populateIdPerfilRespuesta = {
        path: 'idPerfilRespuesta',
        select: 'nombre nombreContacto tipoPerfil album',
      };

      const options = {
        session: opts.session,
        lean: true,
        sort: { fechaCreacion: 1 },
        select: ' -__v  -estado -proyecto -tipo',
        populate: [
          populateTraduccion,
          adjuntos,
          coautor,
          populateIdPerfilRespuesta,
        ],
        page: Number(page),
        limit: Number(limit),
      };

      const inicio = new Date(fechaInicial);
      fechaFinal = fechaFinal + 'T23:59:59.999Z';
      const final = new Date(fechaFinal);

      // const comentariospruebas = await this.proyectoModel.paginate({_id: proyecto, estado: estadoProyecto.codigo });
      const comentarios = await this.comentarioModelo.paginate(
        {
          $and: [
            { fechaActualizacion: { $gte: inicio } },
            { fechaActualizacion: { $lte: final } },
            { estado: codigoEstado },
            { proyecto: proyecto },
          ],
        },
        options,
      );

      if (comentarios.docs.length > 0) {
        let dataComentarios = [];
        let objPerfil: any;
        let objPartProyec: any;
        for (let getComentarios of comentarios.docs) {
          let getTraduccion = [];

          if (getComentarios.traducciones.length === 0) {
            let tradIdi = await this.traducirComentarioService.traducir(
              getComentarios._id,
              idioma,
              opts,
              true,
            );

            if (tradIdi) {
              getTraduccion.push(tradIdi);
              if (tradIdi.idioma.codigo != codIdiomas.ingles) {
                let getTraduccionIngles = await this.traduccionComentarioModel
                  .findOne({
                    referencia: getComentarios._id,
                    idioma: codIdiomas.ingles,
                    estado: codigoestadosTraaduccionComentario.activa,
                  })
                  .select('texto idioma original');

                if (getTraduccionIngles) {
                  let trad = {
                    _id: getTraduccionIngles._id,
                    texto: getTraduccionIngles.texto,
                    idioma: {
                      codigo: getTraduccionIngles.idioma,
                    },
                    original: getTraduccionIngles.original,
                  };
                  getTraduccion.push(trad);
                } else {
                  let tradIdiIngles = await this.traducirComentarioService.traducir(
                    getComentarios._id,
                    idiomas.ingles,
                    opts,
                    true,
                  );
                  getTraduccion.push(tradIdiIngles);
                }
              }
            }
          } else {
            let tradOri = {
              _id: getComentarios.traducciones[0]['_id'],
              texto: getComentarios.traducciones[0]['texto'],
              idioma: {
                codigo: getComentarios.traducciones[0]['idioma'],
              },
              original: getComentarios.traducciones[0]['original'],
            };
            getTraduccion.push(tradOri);

            if (getComentarios.traducciones[0]['idioma'] != codIdiomas.ingles) {
              let getTraduccionIngles = await this.traduccionComentarioModel
                .findOne({
                  referencia: getComentarios._id,
                  idioma: codIdiomas.ingles,
                  estado: codigoestadosTraaduccionComentario.activa,
                })
                .select('texto idioma original');

              if (getTraduccionIngles) {
                let trad = {
                  _id: getTraduccionIngles._id,
                  texto: getTraduccionIngles.texto,
                  idioma: {
                    codigo: getTraduccionIngles.idioma,
                  },
                  original: getTraduccionIngles.original,
                };
                getTraduccion.push(trad);
              } else {
                let tradIdiIngles = await this.traducirComentarioService.traducir(
                  getComentarios._id,
                  idiomas.ingles,
                  opts,
                  true,
                );
                getTraduccion.push(tradIdiIngles);
              }
            }
          } //Fin de traducciones

          if (getComentarios.traducciones.length === 0) {
            getTraduccion = await this.traducirComentarioService.traducir(
              getComentarios._id,
              idioma,
              opts,
            );
          } else {
            getTraduccion.push(getComentarios.traducciones[0]);
          }

          let media = [];
          //Verifica tiene adjuntos (medias)

          if (getComentarios.adjuntos.length > 0) {
            for (const getMedia of getComentarios.adjuntos) {
              let obMiniatura: any;

              //Verifica si la media tiene miniatura
              if (getMedia['miniatura']) {
                //Objeto de tipo miniatura (archivo)
                obMiniatura = {
                  _id: getMedia['miniatura']._id,
                  url: getMedia['miniatura'].url,
                  tipo: {
                    codigo: getMedia['miniatura'].tipo,
                  },
                  fileDefault: getMedia['miniatura'].fileDefault,
                  fechaActualizacion: getMedia['miniatura'].fechaActualizacion,
                };
              }
              //Objeto de tipo media
              let objMedia: any = {
                _id: getMedia['_id'],
                catalogoMedia: {
                  codigo: getMedia['catalogoMedia'],
                },
                principal: {
                  _id: getMedia['principal']._id,
                  url: getMedia['principal'].url,
                  tipo: {
                    codigo: getMedia['principal'].tipo,
                  },
                  fileDefault: getMedia['principal'].fileDefault,
                  fechaActualizacion: getMedia['principal'].fechaActualizacion,
                },
                miniatura: obMiniatura,
              };
              if (getMedia['principal'].duracion) {
                objMedia.principal.duracion = getMedia['principal'].duracion;
              }
              //Verifica si existe la traduccion de la media

              if (getMedia['traducciones'][0] === undefined) {
                const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                  getMedia['_id'],
                  idioma,
                  opts,
                );
                console.log('traduce: ', traduccionMedia);

                if (traduccionMedia) {
                  let arrayTrad = [];
                  const traducciones = {
                    _id: traduccionMedia._id,
                    descripcion: traduccionMedia.descripcion,
                  };
                  arrayTrad.push(traducciones);
                  objMedia.traducciones = arrayTrad;
                } else {
                  objMedia.traducciones = [];
                }
              } else {
                let arrayTraducciones = [];
                arrayTraducciones.push(getMedia['traducciones'][0]);
                objMedia.traducciones = arrayTraducciones;
              }

              media.push(objMedia);
            }
          }

          //Llama al metodo de obtener foto de perfil
          let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
            getComentarios.coautor['coautor']._id,
          );

          let dataAlbum = [];
          dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);

          //Data para el mapeo de perfil
          objPerfil = {
            _id: getComentarios.coautor['coautor']._id,
            nombreContacto: getComentarios.coautor['coautor']['nombreContacto'],
            nombre: getComentarios.coautor['coautor']['nombre'],
            album: dataAlbum,
          };

          let arrayConfigPartic = [];
          //Verifica si el participante tiene configuraciones
          if (getComentarios.coautor['configuraciones'].length > 0) {
            for (const confParticipante of getComentarios.coautor[
              'configuraciones'
            ]) {
              let arrayEstilosConfigPartic = [];
              //Verifica si tiene estilos
              if (confParticipante.estilos.length > 0) {
                for (const estilosParticipante of confParticipante.estilos) {
                  //Obtiene el color
                  const getColor = await this.catalogoColoresService.obtenerCatalogoColoresByCodigo(
                    estilosParticipante.color,
                  );

                  //Objeto de tipo catalogo estilos
                  //Obtiene el catalogo estilos
                  const getCatEstilos = await this.catalogoEstilosService.obtenerCatalogoEstilosByCodigo(
                    estilosParticipante.tipo,
                  );

                  //Verifica si el estilo tiene media
                  let mediaEstiloPartic: any;
                  if (estilosParticipante.media) {
                    //Data de medias
                    //Llama al metodo del obtener medias
                    mediaEstiloPartic = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(
                      estilosParticipante.media,
                      idioma,
                      opts,
                    );
                  }
                  let objEstiloPartic = {
                    _id: estilosParticipante._id,
                    codigo: estilosParticipante.codigo,
                    color: getColor,
                    tipo: getCatEstilos,
                    media: mediaEstiloPartic,
                  };
                  arrayEstilosConfigPartic.push(objEstiloPartic);
                }
              }

              //Verifica si la configuracion tiene tono de notificacion
              let objMediaTonoNotificacion: any;
              if (confParticipante.tonoNotificacion) {
                //Data de medias
                //Llama al metodo del obtener medias
                objMediaTonoNotificacion = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(
                  confParticipante.tonoNotificacion,
                  idioma,
                  opts,
                );
              }

              //Obtiene el catalogo configuracion segun el codigo
              const getCatConfiguracion = await this.catalogoConfiguracionService.obtenerCatalogoConfiguracionByCodigo(
                confParticipante.tipo,
              );

              let objConfigPartic = {
                _id: confParticipante._id,
                codigo: confParticipante.codigo,
                tonoNotificacion: objMediaTonoNotificacion,
                estilos: arrayEstilosConfigPartic,
                tipo: getCatConfiguracion,
              };
              arrayConfigPartic.push(objConfigPartic);
            }
          }
          //Data para el mapeo de participante proyecto
          objPartProyec = {
            _id: getComentarios.coautor['_id'],
            coautor: objPerfil,
            configuraciones: arrayConfigPartic,
          };

          let objIdPerfilRespuesta: any;
          //Verificca si tiene el comentario un perfil de respuesta
          if (getComentarios.idPerfilRespuesta) {
            //Llama al metodo de obtener foto de perfil
            let getFotoIdPerfilRespuesta = await this.getFotoPerfilService.getFotoPerfil(
              getComentarios.idPerfilRespuesta['_id'],
            );
            let dataAlbumIdPerfilRespuesta = [];
            dataAlbumIdPerfilRespuesta.push(
              getFotoIdPerfilRespuesta.objAlbumTipoPerfil,
            );

            //Data para el mapeo de perfil respuesta
            objIdPerfilRespuesta = {
              _id: getComentarios.idPerfilRespuesta['_id'],
              nombreContacto:
                getComentarios.idPerfilRespuesta['nombreContacto'],
              nombre: getComentarios.idPerfilRespuesta['nombre'],
              album: dataAlbumIdPerfilRespuesta,
            };
          }
          let objComentarios = {
            _id: getComentarios._id,
            adjuntos: media,
            traducciones: getTraduccion,
            coautor: objPartProyec,
            importante: getComentarios.importante,
            idPerfilRespuesta: objIdPerfilRespuesta || null,
            fechaCreacion: getComentarios.fechaCreacion,
            fechaActualizacion: getComentarios.fechaActualizacion,
          };
          dataComentarios.push(objComentarios);

          response.set(
            headerNombre.totalDatos,
            comentarios.totalDocs.toString(),
          );
          response.set(
            headerNombre.totalPaginas,
            comentarios.totalPages.toString(),
          );
          response.set(
            headerNombre.proximaPagina,
            comentarios.hasNextPage.toString(),
          );
          response.set(
            headerNombre.anteriorPagina,
            comentarios.hasPrevPage.toString(),
          );
        }

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();

        return dataComentarios;
      } else {
        return comentarios.docs;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
