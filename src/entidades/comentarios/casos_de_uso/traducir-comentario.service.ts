import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Comentario } from 'src/drivers/mongoose/interfaces/comentario/comentario.interface';
import { TraduccionComentario } from 'src/drivers/mongoose/interfaces/traduccion_comentario/traduccion-comentario.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  codigoestadosTraaduccionComentario,
  nombreAcciones,
  nombreEntidades,
} from 'src/shared/enum-sistema';

@Injectable()
export class TraducirComentarioService {
  constructor(
    @Inject('COMENTARIOS_MODEL')
    private readonly comentarioModel: Model<Comentario>,
    @Inject('TRADUCCION_COMENTARIO_MODEL')
    private readonly traduccionComentarioModel: Model<TraduccionComentario>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) {}

  async traducir(
    idComentario: string,
    idioma: string,
    opts?: any,
    returnObj?: any,
  ): Promise<any> {
    //Obtiene el codigo del idioma
    const getCodIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      idioma,
    );
    let codIdioma = getCodIdioma.codigo;

    //Obtiene la accion crear
    const accion = await this.catalogoAccionService.obtenerNombreAccion(
      nombreAcciones.crear,
    );
    let getAccion = accion.codigo;

    //Obtiene la entidad comentarios
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.comentarios,
    );

    try {
      let comentario = await this.comentarioModel.findOne({
        _id: idComentario,
      });

      //Obtiene la traduccion en el idioma original
      const getTraductorOriginal = await this.traduccionComentarioModel.findOne(
        { referencia: idComentario, original: true },
      );

      if (getTraductorOriginal) {
        //llama al metodo de traduccion dinamica
        const comentarioTraducido = await traducirTexto(
          idioma,
          getTraductorOriginal.texto,
        );

        //datos para guardar la nueva traduccion
        let newTraduccionComentario = {
          texto: comentarioTraducido.textoTraducido,
          idioma: codIdioma,
          original: false,
          referencia: idComentario,
          estado: codigoestadosTraaduccionComentario.activa,
        };

        //guarda la nueva traduccion
        const traduccion = await new this.traduccionComentarioModel(
          newTraduccionComentario,
        ).save(opts);

        //datos para guardar el historico de la traduccion del pensamiento
        let historicoTraduccion: any = {
          datos: traduccion,
          usuario: comentario.coautor,
          accion: getAccion,
          entidad: entidad.codigo,
        };

        //guarda el historico de la nueva traduccion
        this.crearHistoricoService.crearHistoricoServer(historicoTraduccion);

        //Actualiza el array de id de traducciones
        await this.comentarioModel.updateOne(
          { _id: idComentario },
          { $push: { traducciones: traduccion._id } },
          opts,
        );

        // return true;
        let returnTraduccion = [];
        let trad = {
          _id: traduccion._id,
          texto: traduccion.texto,
          idioma: {
            codigo: traduccion.idioma,
          },
          original: traduccion.original,
        };
        returnTraduccion.push(trad);
        if (returnObj) {
          return trad;
        }
        return returnTraduccion;
      } else {
        if (returnObj) {
          return null;
        }
        let returnTraduccion = [];
        return returnTraduccion;
      }
    } catch (error) {
      throw error;
    }
  }
}
