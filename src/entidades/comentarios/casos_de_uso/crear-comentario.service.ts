import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model, mongo } from 'mongoose';
import { FBNotificacion } from 'src/drivers/firebase/models/fb-notificacion.interface';
import { FirebaseNotificacionService } from 'src/drivers/firebase/services/firebase-notificacion.service';
import { Comentario } from 'src/drivers/mongoose/interfaces/comentario/comentario.interface';
import { ParticipanteProyecto } from 'src/drivers/mongoose/interfaces/participante_proyecto/participante_proyecto.interface';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { ActualizarMediaService } from 'src/entidades/media/casos_de_uso/actualizar-media.service';
import { ObtenerPerfilUsuarioService } from 'src/entidades/perfil/casos_de_uso/obtener-perfil-usuario.service';
import { GestionRolService } from 'src/entidades/rol/casos_de_uso/gestion-rol.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import {
  accionNotificacionFirebase,
  codIdiomas,
  codigoEntidades,
  codigosRol,
  estadoNotificacionesFirebase,
  estadosProyecto,
  idiomas,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreCatComentario,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { CrearComentarioDto } from '../entidad/crear-comentario-dto';
import { ConfiguracionEstilo } from './../../../drivers/mongoose/interfaces/configuracion_estilo/configuracion_estilo.interface';
import { TraduccionComentario } from './../../../drivers/mongoose/interfaces/traduccion_comentario/traduccion-comentario.interface';
import { CatalogoTipoComentarioService } from './../../catalogos/casos_de_uso/catalogo-tipo-comentario.service';
import { CrearParticipanteProyectoService } from './../../participante_proyecto/casos_de_uso/crear-participante-proyecto.service';
import { ObtenerParticipanteProyectoService } from './../../participante_proyecto/casos_de_uso/obtener-participante-proyecto.service';
import { ObtenerComentariosService } from './obtener-comentarios-proyecto.service';
import { ObtenerComentarioUnicoService } from './obtener-comentarios-unico-proyecto.service';
import { TraducirComentarioSegundoPlanoService } from './traducir-comentario-segundo-plano.service';

@Injectable()
export class CrearComentarioService {
  constructor(
    @Inject('COMENTARIOS_MODEL')
    private readonly comentarioModel: Model<Comentario>,
    @Inject('TRADUCCION_COMENTARIO_MODEL')
    private readonly traduccionComentarioModel: Model<TraduccionComentario>,
    @Inject('PROYECTO_MODEL')
    private readonly proyectoModel: Model<Proyecto>,
    @Inject('PARTICIPANTE_PROYECTO_MODEL')
    private readonly participanteProyectoModelo: Model<ParticipanteProyecto>,
    @Inject('CONFIGURACION_ESTILO_MODEL')
    private readonly configuracionEstiloModelo: Model<ConfiguracionEstilo>,

    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoTipoComentarioService: CatalogoTipoComentarioService,
    private crearParticipanteProyectoService: CrearParticipanteProyectoService,
    private obtenerParticipanteProyecto: ObtenerParticipanteProyectoService,
    private gestionRolService: GestionRolService,
    private obtenerComentariosService: ObtenerComentariosService,
    private obtenerComentarioUnicoService: ObtenerComentarioUnicoService,
    private actualizarMediaService: ActualizarMediaService,
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private firebaseNotificacionService: FirebaseNotificacionService,
    private traducirComentarioSegundoPlanoService: TraducirComentarioSegundoPlanoService,
  ) {}

  async comentar(
    comentarioDto: CrearComentarioDto,
    codIdioma: any,
  ): Promise<any> {
    try {
      const saveComentario = await this.procesarComentario(
        comentarioDto,
        codIdioma,
      );

      if (saveComentario) {
        const getComentario = saveComentario.getComentario;
        const perfilPropietario = saveComentario.perfilPropietario;

        this.firebaseNotificacionService.createDataComenatario(
          getComentario,
          comentarioDto.proyecto._id.toString(),
        );

        // setTimeout(() => {
        //_______________ Notificar comentario al propietario del proyecto ____________________

        let formatData = {
          proyecto: {
            _id: comentarioDto.proyecto._id.toString(),
          },
          perfil: {
            _id: comentarioDto.coautor.coautor._id.toString(),
          },
        };

        const dataNotificarComentario: FBNotificacion = {
          idEntidad: getComentario._id.toString(),
          leido: false,
          codEntidad: codigoEntidades.entidadComentarios,
          data: formatData,
          estado: estadoNotificacionesFirebase.activa,
          accion: accionNotificacionFirebase.ver,
          query: `${codigoEntidades.entidadComentarios}-${false}`,
          queryEntidad: `${comentarioDto.proyecto._id.toString()}`,
        };

        await this.firebaseNotificacionService.createDataNotificacion(
          dataNotificarComentario,
          codigoEntidades.entidadPerfiles,
          perfilPropietario,
        );
        // })
        return getComentario;
      }

      return null;
    } catch (error) {
      throw error;
    }
  }
  async procesarComentario(
    comentarioDto: CrearComentarioDto,
    codIdioma: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();

    const transactionOptions: any = {
      readPreference: 'primary',
      readConcern: { level: 'local' },
      writeConcern: { w: 'majority' },
    };

    let transaccion;

    //await session.startTransaction(transactionOptions);

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarios,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      //Obtiene la entidad proyecto
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadP = entidadP.codigo;

      //Obtiene el estado que de foro del proyecto
      const estadoForoProy = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.foro,
        codEntidadP,
      );
      let codEstadoForoProyecto = estadoForoProy.codigo;

      let idiomaDetectado;
      let conTraduccion;
      if (
        comentarioDto?.traducciones &&
        comentarioDto?.traducciones.length > 0
      ) {
        conTraduccion = true;
        let textoTraducido = await traducirTexto(
          codIdioma,
          comentarioDto.traducciones[0].texto,
        );
        idiomaDetectado = textoTraducido.idiomaDetectado;
      } else {
        conTraduccion = false;
        idiomaDetectado = codIdioma;
      }

      //Obtiene el idioma por el codigo
      // const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(idiomaDetectado)
      // let codIdioma = idioma.codigo;

      const getproyecto = await this.proyectoModel
        .findOne({ _id: comentarioDto.proyecto._id })
        .populate({
          path: 'comentarios',
          select: 'coautor',
          match: {
            estado: estado.codigo,
          },
          populate: {
            path: 'coautor',
            select: 'coautor roles',
            populate: {
              path: 'roles',
              select: 'rol',
            },
          },
        });

      let find: any;
      let perfilComento = false;
      let proyectoEstadoForo = false;
      let crearComent: any;
      if (getproyecto) {
        if (
          getproyecto.estado === codEstadoForoProyecto ||
          getproyecto.estado === estadosProyecto.proyectoPreEstrategia
        ) {
          proyectoEstadoForo = true;
          //Verifica si el proyecto tiene comentarios
          if (getproyecto.comentarios.length > 0) {
            for (const getComentarios of getproyecto.comentarios) {
              const getCouator = getComentarios['coautor'].coautor;
              //Verifica si el perfil que comenta ya ha comentado antes en el proyecto
              if (getCouator == comentarioDto.coautor.coautor._id) {
                perfilComento = true;
                break;
              }
            }
          }
        }
        //Verifica si el proyecto esta en estado de estrategia
        if (getproyecto.estado === estadosProyecto.proyectoEnEstrategia) {
          proyectoEstadoForo = true;
          //Verifica si el proyecto tiene comentarios
          if (getproyecto.comentarios.length > 0) {
            for (const getComentarios of getproyecto.comentarios) {
              const getCouator = getComentarios['coautor'].coautor;
              const getRol = getComentarios['coautor'].roles;
              //Verifica si el perfil que comenta ya ha comentado antes en el proyecto
              if (getCouator == comentarioDto.coautor.coautor._id) {
                if (getRol.length > 0) {
                  for (const iteraRol of getRol) {
                    const rol = iteraRol.rol;
                    //Verifica si el rol que comento es de tipo estratega
                    if (rol == codigosRol.cod_rol_coautor_estratega) {
                      perfilComento = true;
                      break;
                    }
                  }
                }
              }
            }
          }
        }

        //Verifica si el proyecto esta en estado de foro

        if (proyectoEstadoForo) {
          // _________________________________________Inicia transaccion muiti-document_________________________

          transaccion = await session.withTransaction(async () => {
            //Si el perfil ya ha comentado antes en el proyecto lo deja comentar
            if (perfilComento) {
              console.log('Si puede comentar');
              crearComent = await this.crearComentario(
                comentarioDto,
                idiomaDetectado,
                proyectoEstadoForo,
                opts,
              );

              if (!crearComent) {
                await session.abortTransaction();
                return;
              }
            } else {
              console.log('No puede comentar');
              //Finaliza la transaccion
              await session.endSession();
              return null;
            }
          }, transactionOptions);

          if (transaccion) {
            let getComentario = await this.obtenerComentarioUnicoService.obtenerComentarioUnicoProyecto(
              crearComent._id,
              codIdioma,
              conTraduccion,
            );

            return {
              getComentario: getComentario,
              perfilPropietario: getproyecto.perfil.toString(),
            };
          }
        } else {
          // _________________________________________Inicia transaccion muiti-document_________________________
          transaccion = await session.withTransaction(async () => {
            crearComent = await this.crearComentario(
              comentarioDto,
              idiomaDetectado,
              proyectoEstadoForo,
              opts,
            );

            if (!crearComent) {
              await session.abortTransaction();
              return;
            }
          }, transactionOptions);

          if (transaccion) {
            let getComentario = await this.obtenerComentarioUnicoService.obtenerComentarioUnicoProyecto(
              crearComent._id,
              codIdioma,
              conTraduccion,
            );
            return {
              getComentario: getComentario,
              perfilPropietario: getproyecto.perfil.toString(),
            };
          }
        }
      } else {
        //Finaliza la transaccion
        await session.endSession();
        return null;
      }
    } catch (error) {
      await session.endSession();
      throw error;
    } finally {
      await session.endSession();
    }
  }

  async crearComentario(
    comentarioDto: CrearComentarioDto,
    codIdioma: any,
    proyectoEstadoForo: any,
    opts: any,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarios,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );
      const codigoIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        codIdioma,
      );
      const entidadTraduccion = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionComentario,
      );
      const estadoTradComen = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidadTraduccion.codigo,
      );
      const getEstadoActivaTradComent = estadoTradComen.codigo;

      let tipoComentario: any;

      const entidadParticipantePro = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.participanteProyecto,
      );
      const estadoParticipantePro = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidadParticipantePro.codigo,
      );

      //Obtiene la entidad proyecto
      const entidadP = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.proyectos,
      );
      let codEntidadP = entidadP.codigo;

      //Obtiene el estado activa de la entidad proyecto
      const estadoProy = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadP,
      );
      let codEstadoProyecto = estadoProy.codigo;

      //Obtiene el estado que de foro del proyecto
      const estadoForoProy = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.foro,
        codEntidadP,
      );
      let codEstadoForoProyecto = estadoForoProy.codigo;

      // Crear nueevo ObjectId para traduccion comentario
      const idComentario = new mongo.ObjectId();

      // crear participante proyecto, con rol tipo coautor
      // validar si es la primera vez que comenta se crea y si es el propietario

      const existeParticipante = await this.obtenerParticipanteProyecto.ObtenerParticipanteProyecto(
        comentarioDto.coautor.coautor._id,
        comentarioDto.proyecto._id,
      );

      let participanteProyecto: any;
      if (existeParticipante) {
        participanteProyecto = existeParticipante;
      } else {
        const rolEntidad = await this.gestionRolService.obtenerRolEntidadByCodEntRol(
          codigosRol.cod_rol_ent_co_autor,
          entidad.codigo,
        );
        // random de colores para participante proyecto
        const configEstilo = await this.ObtenerConfiguracionEstilo(
          entidad.codigo,
        );

        const participanteProyectoDto: any = {
          estado: estadoParticipantePro.codigo,
          coautor: comentarioDto.coautor.coautor._id,
          roles: [rolEntidad._id],
          configuraciones: [configEstilo], // configuraciones de estilos
          comentarios: [],
          proyecto: comentarioDto.proyecto._id,
          totalComentarios: 0,
        };

        participanteProyecto = await this.crearParticipanteProyectoService.crearParticipanteProyecto(
          participanteProyectoDto,
          opts,
        );
      }

      if (proyectoEstadoForo) {
        tipoComentario = await this.catalogoTipoComentarioService.obtenerTipoComentario(
          nombreCatComentario.foro,
        );
      } else {
        tipoComentario = await this.catalogoTipoComentarioService.obtenerTipoComentario(
          nombreCatComentario.normal,
        );
      }

      let fechaCreacionFirebase = new Date();
      // console.log('fechaCreacion.getTime(): ', fechaCreacionFirebase.getTime())
      const nuevoComentario: any = {
        _id: idComentario,
        estado: estado.codigo,
        coautor: participanteProyecto._id,
        adjuntos: [],
        importante: comentarioDto.importante,
        tipo: tipoComentario.codigo,
        proyecto: comentarioDto.proyecto._id,
        fechaActualizacion: new Date(),
        fechaCreacionFirebase: fechaCreacionFirebase.getTime(),
      };

      let traducciones = [];
      let traduccionComentario;
      //Verifica si tiene traducciones

      if (
        comentarioDto?.traducciones &&
        comentarioDto?.traducciones.length > 0
      ) {
        //Objeto de traduccion comentario
        let objTraduccionComentario = {
          texto: comentarioDto.traducciones[0].texto,
          idioma: codigoIdioma.codigo,
          original: true,
          referencia: idComentario,
          estado: getEstadoActivaTradComent,
        };

        //Guarda la traduccion del comentario
        traduccionComentario = await new this.traduccionComentarioModel(
          objTraduccionComentario,
        ).save(opts);

        let historicoTraduccion: any = {
          datos: traduccionComentario,
          usuario: comentarioDto.coautor.coautor._id,
          accion: accionCrear.codigo,
          entidad: entidadTraduccion.codigo,
        };

        //Guarda el historico de la traduccion del comentario
        this.crearHistoricoService.crearHistoricoServer(historicoTraduccion);

        //Lista de traducciones comentarios

        traducciones.push(traduccionComentario._id);
        nuevoComentario.traducciones = traducciones;
      }
      if (comentarioDto.idPerfilRespuesta) {
        nuevoComentario.idPerfilRespuesta = comentarioDto.idPerfilRespuesta._id;
      } else {
        nuevoComentario.idPerfilRespuesta = null;
      }
      const comentario = await new this.comentarioModel(nuevoComentario).save(
        opts,
      );

      if (comentario.traducciones.length > 0) {
        //Si es diferente de idioma español
        if (codigoIdioma.codigo != codIdiomas.espanol) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.espanol,
            opts,
          );
        }
        //Si es diferente de idioma ingles
        if (codigoIdioma.codigo != codIdiomas.ingles) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.ingles,
            opts,
          );
        }
        //Si es diferente de idioma italiano
        if (codigoIdioma.codigo != codIdiomas.italiano) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.italiano,
            opts,
          );
        }
        //Si es diferente de idioma aleman
        if (codigoIdioma.codigo != codIdiomas.aleman) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.aleman,
            opts,
          );
        }
        //Si es diferente de idioma frances
        if (codigoIdioma.codigo != codIdiomas.frances) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.frances,
            opts,
          );
        }
        //Si es diferente de idioma portugues
        if (codigoIdioma.codigo != codIdiomas.portugues) {
          await this.traducirComentarioSegundoPlanoService.traducirComentario(
            comentario._id,
            comentarioDto.coautor.coautor._id,
            comentarioDto.traducciones[0].texto,
            idiomas.portugues,
            opts,
          );
        }
      }

      const getUsuarioPerfil = await this.obtenerPerfilUsuarioService.obtenerPerfilById(
        comentarioDto.coautor.coautor._id,
      );

      //Verifica si tiene adjuntos
      // _________________medias_____________________
      if (comentarioDto?.adjuntos && comentarioDto?.adjuntos.length > 0) {
        for (const media of comentarioDto.adjuntos) {
          let idMedia = media._id;

          ////cambia de estado sinAsignar a activa
          await this.actualizarMediaService.actualizarMediaAsignadoDescripcion(
            idMedia,
            getUsuarioPerfil._id,
            codIdioma,
            null,
            false,
            opts,
          );
          //Actualiza el array de medias
          await this.comentarioModel.updateOne(
            { _id: comentario._id },
            { $push: { adjuntos: idMedia } },
            opts,
          );
        }
      }

      //datos para guardar el historico
      const newHistorico: any = {
        datos: comentario,
        usuario: comentarioDto.coautor.coautor._id,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      // actualizar participante y comentarios al proyecto
      let proyectoActualizado: any;

      if (existeParticipante) {
        proyectoActualizado = await this.proyectoModel.findOneAndUpdate(
          { _id: comentarioDto.proyecto._id },
          { $push: { comentarios: comentario._id } },
          opts,
        );
      } else {
        proyectoActualizado = await this.proyectoModel.findOneAndUpdate(
          { _id: comentarioDto.proyecto._id },
          {
            $push: {
              comentarios: comentario._id,
              participantes: participanteProyecto._id,
            },
          },
          opts,
        );
      }

      const historicoActualizarProyecto: any = {
        datos: proyectoActualizado,
        usuario: comentarioDto.coautor.coautor._id,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };
      this.crearHistoricoService.crearHistoricoServer(
        historicoActualizarProyecto,
      );

      const totalComentarios = await this.obtenerComentariosService.totalComentariosCoautor(
        comentarioDto.proyecto._id,
        comentarioDto.coautor.coautor._id,
        opts,
      );

      const participanteProyectoActualizado = await this.participanteProyectoModelo.findOneAndUpdate(
        { _id: participanteProyecto._id },
        {
          $push: { comentarios: comentario._id },
          $set: { totalComentarios: totalComentarios },
        },
        opts,
      );

      // let getComentario = await this.obtenerComentarioUnicoService.obtenerComentarioUnicoProyecto(comentario._id, )
      return comentario;
    } catch (error) {
      throw error;
    }
  }

  async ObtenerConfiguracionEstilo(
    codigoEntidadComentarioIntercambio: string,
  ): Promise<any> {
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.configuracionEstilo,
      );
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      // random de colores para participante proyecto

      const confiEstilos = await this.configuracionEstiloModelo
        .find({
          entidad: codigoEntidadComentarioIntercambio,
          estado: estado.codigo,
        })
        .populate({
          path: 'estilos',
          //populate: { path: 'tipo', match: { nombre: 'fondo' } }, // fondo o letra
        });

      const arrayEstilos = [];
      for (const config of confiEstilos) {
        arrayEstilos.push(config);
      }

      arrayEstilos.map((conf, index) => {
        arrayEstilos[index].estilos = conf.estilos.filter(
          estilo => estilo.tipo != null,
        );
      });

      const random = Math.floor(Math.random() * arrayEstilos.length);

      return arrayEstilos[random];
    } catch (error) {
      throw error;
    }
  }
}
