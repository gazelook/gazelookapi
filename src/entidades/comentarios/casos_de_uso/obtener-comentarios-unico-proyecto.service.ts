import { Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model, PaginateModel } from 'mongoose';
import { Comentario } from 'src/drivers/mongoose/interfaces/comentario/comentario.interface';
import { TraduccionComentario } from 'src/drivers/mongoose/interfaces/traduccion_comentario/traduccion-comentario.interface';
import { CatalogoColoresService } from 'src/entidades/catalogos/casos_de_uso/catalogo-colores.service';
import { CatalogoConfiguracionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-configuracion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEstilosService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estilos.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { TraduccionMediaService } from 'src/entidades/media/casos_de_uso/traduccion-media.service';
import { GetFotoPerfilService } from 'src/entidades/perfil/casos_de_uso/obtener-album-perfil-general.service';
import { ObtenerEstadoProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-estado-proyecto.service';
import { ObtenerMediasProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-medias-proyecto.service';
import {
  codigoestadosTraaduccionComentario,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { TraducirComentarioService } from './traducir-comentario.service';

@Injectable()
export class ObtenerComentarioUnicoService {
  constructor(
    @Inject('COMENTARIOS_MODEL')
    private readonly comentarioModelo: PaginateModel<Comentario>,
    @Inject('TRADUCCION_COMENTARIO_MODEL')
    private readonly traduccionComentarioModel: Model<TraduccionComentario>,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private traducirComentarioService: TraducirComentarioService,
    private getFotoPerfilService: GetFotoPerfilService,
    private traduccionMediaService: TraduccionMediaService,
    private obtenerEstadoProyectoService: ObtenerEstadoProyectoService,
    private catalogoColoresService: CatalogoColoresService,
    private catalogoEstilosService: CatalogoEstilosService,
    private obtenerMediasProyectoService: ObtenerMediasProyectoService,
    private catalogoConfiguracionService: CatalogoConfiguracionService,
  ) {}

  async obtenerComentarioUnicoProyecto(
    idComentario: string,
    idioma: string,
    conTraduccion: any,
  ): Promise<any> {
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.comentarios,
      );

      //Obtiene el estado activo
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      const codIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
        idioma,
      );

      //Obtiene el codigo de la entidad media
      const entidadmedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.media,
      );
      let codEntidadMedia = entidadmedia.codigo;

      //Obtiene el estado activo de la entidad proyectos
      const estadoMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadMedia,
      );
      let codEstadoMedia = estadoMedia.codigo;

      //Obtiene el codigo de la entidad traduccion media
      const entidadTradMedia = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.traduccionMedia,
      );
      let codEntidadTradMedia = entidadTradMedia.codigo;

      //Obtiene el estado activo de la entidad traduccion media
      const estadoTradMedia = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadTradMedia,
      );
      let codEstadoTradMedia = estadoTradMedia.codigo;

      //Obtiene el codigo de la entidad participante proyecto
      const entidadConfigEstilo = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.configuracionEstilo,
      );
      let codEntidadConfigEstilo = entidadConfigEstilo.codigo;

      //Obtiene el estado activo de la entidad participante proyecto
      const estadoConfigEstilo = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidadConfigEstilo,
      );
      let codEstadoConfigEstilo = estadoConfigEstilo.codigo;

      const getComentario = await this.comentarioModelo
        .findOne({ _id: idComentario, estado: estado.codigo })
        .populate({
          path: 'traducciones',
          match: {
            // idioma: codIdioma.codigo,
            estado: codigoestadosTraaduccionComentario.activa,
          },
          select: 'texto idioma original',
        })
        .populate({
          path: 'adjuntos',
          select: 'principal enlace miniatura catalogoMedia traducciones',
          match: { estado: codEstadoMedia },
          populate: [
            {
              path: 'traducciones',
              select: 'descripcion',
              match: { idioma: codIdioma.codigo, estado: codEstadoTradMedia },
            },
            {
              path: 'principal',
              select: 'url tipo duracion fileDefault fechaActualizacion',
            },
            {
              path: 'miniatura',
              select: 'url tipo duracion fileDefault fechaActualizacion',
            },
          ],
        })
        .populate({
          path: 'idPerfilRespuesta',
          select: 'nombre nombreContacto tipoPerfil album',
        })
        .populate({
          path: 'coautor',
          select: 'coautor configuraciones',
          populate: [
            {
              path: 'coautor',
              select: 'nombre nombreContacto tipoPerfil album',
            },
            {
              path: 'configuraciones',
              select: 'codigo estilos tonoNotificacion tipo',
              match: { estado: codEstadoConfigEstilo },
              populate: [
                {
                  path: 'estilos',
                  select: 'codigo media color tipo',
                  populate: [
                    {
                      path: 'media',
                      match: { estado: codEstadoMedia },
                      select:
                        'principal enlace miniatura traducciones catalogoMedia',
                      populate: [
                        {
                          path: 'traducciones',
                          select: 'descripcion',
                          match: {
                            idioma: codIdioma,
                            estado: codEstadoTradMedia,
                          },
                        },
                        {
                          path: 'principal',
                          select:
                            ' url tipo duracion fileDefault fechaActualizacion',
                        },
                        {
                          path: 'miniatura',
                          select:
                            ' url tipo duracion fileDefault fechaActualizacion',
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        });

      if (getComentario) {
        let objPerfil: any;
        let objPartProyec: any;
        // let getTraduccion = []
        // if (getComentario.traducciones.length > 0) {
        //   getTraduccion.push(getComentario.traducciones[0])
        // } else {
        //   getTraduccion = await this.traducirComentarioService.traducir(getComentario._id, idioma, opts);
        // }

        //////////////////////////////////////////////////////////////////
        let getTraduccion = [];
        // if (conTraduccion) {

        //Inicio de traducciones

        if (getComentario.traducciones.length > 0) {
          for (const traducciones of getComentario.traducciones) {
            let tradOri = {
              _id: traducciones['_id'].toString(),
              texto: traducciones['texto'],
              idioma: {
                codigo: traducciones['idioma'],
              },
              original: traducciones['original'],
            };
            getTraduccion.push(tradOri);
          }
        }

        // let tradIdi = await this.traducirComentarioService.traducir(
        //   getComentario._id,
        //   idioma,
        //   opts,
        //   true
        // );
        // getTraduccion.push(tradIdi);

        // if (tradIdi.idioma.codigo != codIdiomas.ingles) {
        //   let getTraduccionIngles = await this.traduccionComentarioModel
        //     .findOne({
        //       referencia: getComentario._id,
        //       idioma: codIdiomas.ingles,
        //       estado: codigoestadosTraaduccionComentario.activa
        //     }).select('texto idioma original');

        //   if (getTraduccionIngles) {
        //     let trad = {
        //       _id: getTraduccionIngles._id,
        //       texto: getTraduccionIngles.texto,
        //       idioma: {
        //         codigo: getTraduccionIngles.idioma
        //       },
        //       original: getTraduccionIngles.original
        //     }
        //     getTraduccion.push(trad);
        //   } else {
        //     let tradIdiIngles = await this.traducirComentarioService.traducir(
        //       getComentario._id,
        //       idiomas.ingles,
        //       opts,
        //       true
        //     );
        //     getTraduccion.push(tradIdiIngles);
        //   }
        // }

        // } else {
        //   let tradOri = {
        //     _id: getComentario.traducciones[0]['_id'],
        //     texto: getComentario.traducciones[0]['texto'],
        //     idioma: {
        //       codigo: getComentario.traducciones[0]['idioma']
        //     },
        //     original: getComentario.traducciones[0]['original']
        //   }
        //   getTraduccion.push(tradOri)

        //   if (getComentario.traducciones[0]['idioma'] != codIdiomas.ingles) {

        //     let getTraduccionIngles = await this.traduccionComentarioModel
        //       .findOne({
        //         referencia: getComentario._id,
        //         idioma: codIdiomas.ingles,
        //         estado: codigoestadosTraaduccionComentario.activa
        //       }).select('texto idioma original');

        //     if (getTraduccionIngles) {

        //       let trad = {
        //         _id: getTraduccionIngles._id,
        //         texto: getTraduccionIngles.texto,
        //         idioma: {
        //           codigo: getTraduccionIngles.idioma
        //         },
        //         original: getTraduccionIngles.original
        //       }
        //       getTraduccion.push(trad);
        //     } else {
        //       let tradIdiIngles = await this.traducirComentarioService.traducir(
        //         getComentario._id,
        //         idiomas.ingles,
        //         opts,
        //         true
        //       );
        //       getTraduccion.push(tradIdiIngles);
        //     }
        //   }

        // }//Fin de traducciones

        let media = [];
        //Verifica tiene adjuntos (medias)
        if (getComentario.adjuntos.length > 0) {
          for (const getMedia of getComentario.adjuntos) {
            let obMiniatura: any;

            //Verifica si la media tiene miniatura
            if (getMedia['miniatura']) {
              //Objeto de tipo miniatura (archivo)
              obMiniatura = {
                _id: getMedia['miniatura']._id.toString(),
                url: getMedia['miniatura'].url,
                tipo: {
                  codigo: getMedia['miniatura'].tipo.toString(),
                },
                fileDefault: getMedia['miniatura'].fileDefault,
                fechaActualizacion: getMedia['miniatura'].fechaActualizacion,
              };
            }
            //Objeto de tipo media
            let objMedia: any = {
              _id: getMedia['_id'].toString(),
              catalogoMedia: {
                codigo: getMedia['catalogoMedia'],
              },
              principal: {
                _id: getMedia['principal']._id.toString(),
                url: getMedia['principal'].url,
                tipo: {
                  codigo: getMedia['principal'].tipo,
                },
                fileDefault: getMedia['principal'].fileDefault,
                fechaActualizacion: getMedia['principal'].fechaActualizacion,
              },
              miniatura: obMiniatura || null,
            };
            if (getMedia['principal'].duracion) {
              objMedia.principal.duracion = getMedia['principal'].duracion;
            }
            //Verifica si existe la traduccion de la media

            if (getMedia['traducciones'][0] === undefined) {
              const traduccionMedia: any = await this.traduccionMediaService.traducirDescripcionMedia(
                getMedia['_id'],
                idioma,
                opts,
              );

              if (traduccionMedia) {
                let arrayTrad = [];
                const traducciones = {
                  _id: traduccionMedia._id.toString(),
                  descripcion: traduccionMedia.descripcion,
                };
                arrayTrad.push(traducciones);
                objMedia.traducciones = arrayTrad;
              } else {
                objMedia.traducciones = [];
              }
            } else {
              let arrayTraducciones = [];
              arrayTraducciones.push(getMedia['traducciones'][0]);
              objMedia.traducciones = arrayTraducciones;
            }

            media.push(objMedia);
          }
        }

        //Llama al metodo de obtener foto de perfil
        let getFotoPerfil = await this.getFotoPerfilService.getFotoPerfil(
          getComentario.coautor['coautor']._id,
        );

        let dataAlbum = [];
        dataAlbum.push(getFotoPerfil.objAlbumTipoPerfil);

        //Data para el mapeo de perfil
        objPerfil = {
          _id: getComentario.coautor['coautor']._id.toString(),
          nombreContacto: getComentario.coautor['coautor']['nombreContacto'],
          nombre: getComentario.coautor['coautor']['nombre'],
          album: dataAlbum,
        };

        let arrayConfigPartic = [];
        //Verifica si el participante tiene configuraciones
        if (getComentario.coautor['configuraciones'].length > 0) {
          for (const confParticipante of getComentario.coautor[
            'configuraciones'
          ]) {
            let arrayEstilosConfigPartic = [];
            //Verifica si tiene estilos
            if (confParticipante.estilos.length > 0) {
              for (const estilosParticipante of confParticipante.estilos) {
                //Obtiene el color
                const getColor = await this.catalogoColoresService.obtenerCatalogoColoresByCodigo(
                  estilosParticipante.color,
                );

                //Objeto de tipo catalogo estilos
                //Obtiene el catalogo estilos
                const getCatEstilos = await this.catalogoEstilosService.obtenerCatalogoEstilosByCodigo(
                  estilosParticipante.tipo,
                );

                //Verifica si el estilo tiene media
                let mediaEstiloPartic: any;
                if (estilosParticipante.media) {
                  //Data de medias
                  //Llama al metodo del obtener medias
                  mediaEstiloPartic = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(
                    estilosParticipante.media,
                    idioma,
                    opts,
                  );
                }
                let objEstiloPartic = {
                  _id: estilosParticipante._id.toString(),
                  codigo: estilosParticipante.codigo,
                  color: getColor,
                  tipo: getCatEstilos,
                  media: mediaEstiloPartic || null,
                };
                arrayEstilosConfigPartic.push(objEstiloPartic);
              }
            }

            //Verifica si la configuracion tiene tono de notificacion
            let objMediaTonoNotificacion: any;
            if (confParticipante.tonoNotificacion) {
              //Data de medias
              //Llama al metodo del obtener medias
              objMediaTonoNotificacion = await this.obtenerMediasProyectoService.obtenerMediaUnicaProyecto(
                confParticipante.tonoNotificacion,
                idioma,
                opts,
              );
            }

            //Obtiene el catalogo configuracion segun el codigo
            const getCatConfiguracion = await this.catalogoConfiguracionService.obtenerCatalogoConfiguracionByCodigo(
              confParticipante.tipo,
            );

            let objConfigPartic = {
              _id: confParticipante._id.toString(),
              codigo: confParticipante.codigo,
              tonoNotificacion: objMediaTonoNotificacion || null,
              estilos: arrayEstilosConfigPartic,
              tipo: getCatConfiguracion,
            };
            arrayConfigPartic.push(objConfigPartic);
          }
        }
        //Data para el mapeo de participante proyecto
        objPartProyec = {
          _id: getComentario.coautor['_id'].toString(),
          coautor: objPerfil,
          configuraciones: arrayConfigPartic,
        };

        let objIdPerfilRespuesta: any;
        if (getComentario.idPerfilRespuesta) {
          //Llama al metodo de obtener foto de perfil
          let getFotoIdPerfilRespuesta = await this.getFotoPerfilService.getFotoPerfil(
            getComentario.idPerfilRespuesta['_id'].toString(),
          );
          let dataAlbumIdPerfilRespuesta = [];
          dataAlbumIdPerfilRespuesta.push(
            getFotoIdPerfilRespuesta.objAlbumTipoPerfil,
          );

          //Data para el mapeo de perfil respuesta
          objIdPerfilRespuesta = {
            _id: getComentario.idPerfilRespuesta['_id'].toString(),
            nombreContacto: getComentario.idPerfilRespuesta['nombreContacto'],
            nombre: getComentario.idPerfilRespuesta['nombre'],
            album: dataAlbumIdPerfilRespuesta,
          };
        }

        let objComentarios = {
          _id: getComentario._id.toString(),
          adjuntos: media,
          traducciones: getTraduccion,
          coautor: objPartProyec,
          importante: getComentario.importante,
          idPerfilRespuesta: objIdPerfilRespuesta || null,
          tipo: {
            codigo: getComentario.tipo,
          },
          estado: {
            codigo: getComentario.estado,
          },
          fechaCreacion: getComentario.fechaCreacion,
          fechaActualizacion: getComentario.fechaActualizacion,
          fechaCreacionFirebase: getComentario.fechaCreacionFirebase,
        };

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        // //Finaliza la transaccion
        await session.endSession();

        return objComentarios;
      } else {
        return null;
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      // //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }
}
