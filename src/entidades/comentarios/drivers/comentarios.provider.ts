import { CatalogoEstilosModelo } from './../../../drivers/mongoose/modelos/catalogo_estilos/catalogo-estilos.schema';
import { EstiloModelo } from './../../../drivers/mongoose/modelos/estilo/estilo.schema';
import { ConfiguracionEstiloModelo } from './../../../drivers/mongoose/modelos/configuracion_estilo/configuracion-estilo.schema';
import { TipoComentarioModelo } from './../../../drivers/mongoose/modelos/catalogo_tipo_comentario/catalogo-tipo-comentario.schema';
import { TraduccionComentarioModelo } from './../../../drivers/mongoose/modelos/traduccion_comentario/traduccion-comentario.schema';
import { ComentarioModelo } from './../../../drivers/mongoose/modelos/comentarios/comentario.schema';
import { Connection } from 'mongoose';
import { ProyectoModelo } from 'src/drivers/mongoose/modelos/proyectos/proyecto.schema';

export const comentariosProviders = [
  {
    provide: 'PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('proyecto', ProyectoModelo, 'proyecto'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'COMENTARIOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('comentarios', ComentarioModelo, 'comentarios'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_COMENTARIO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'traduccion_comentario',
        TraduccionComentarioModelo,
        'traduccion_comentario',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_TIPO_COMENTARIO',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_tipo_comentario',
        TipoComentarioModelo,
        'catalogo_tipo_comentario',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'PROYECTO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('proyecto', ProyectoModelo, 'proyecto'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CONFIGURACION_ESTILO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'configuracion_estilo',
        ConfiguracionEstiloModelo,
        'configuracion_estilo',
      ),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'ESTILO_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('estilos', EstiloModelo, 'estilos'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_ESTILOS_MODEL',
    useFactory: (connection: Connection) =>
      connection.model(
        'catalogo_estilos',
        CatalogoEstilosModelo,
        'catalogo_estilos',
      ),
    inject: ['DB_CONNECTION'],
  },
];
