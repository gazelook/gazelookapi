import { DocumentosLegalesModule } from './entidades/documentos_legales/documentos-legales.module';
import { FinanzasModule } from './entidades/finanzas/finanzas.module';
import { EmailModule } from './entidades/emails/email.module';
import { ConfigModule } from './config/config.module';
import { ConversacionModule } from './entidades/conversacion/conversacion.module';

import { ParticipanteAsociacionModule } from './entidades/asociacion_participante/participante-asociacion.module';
import { AsociacionModule } from './entidades/asociaciones/asociacion.module';
import { ComentariosModule } from './entidades/comentarios/comentarios.module';
import { APP_FILTER, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { AuthModule } from './auth/auth.module';
import { HttpErrorFilter } from './shared/filters/http-error.filter';
import { LoggingInterceptor } from './shared/logging.interceptor';
import { PensamientoModule } from './entidades/pensamientos/pensamientos.module';
import { DispositivosModule } from './entidades/dispositivos/dispositivos.module';
import { LoginModule } from './entidades/login/login.module';
import { TransaccionModule } from './entidades/transaccion/transaccion.module';
import {
  Module,
  ValidationPipe,
  MiddlewareConsumer,
  RequestMethod,
  forwardRef,
  HttpModule,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsuarioModule } from './entidades/usuario/usuario.module';
import { DBModule } from './drivers/db_conection/db.module';
import { HistoricoModule } from './entidades/historico/historico.module';
import { StorageModule } from './drivers/amazon_s3/storage.module';
import { ArchivoModule } from './entidades/archivo/archivo.module';
import { I18nModule, I18nJsonParser } from 'nestjs-i18n';
import * as path from 'path';

import { PaisModule } from './entidades/pais/pais.module';
import { CatalogosModule } from './entidades/catalogos/catalogos.module';
import { PerfilModule } from './entidades/perfil/perfil.module';
import { CuentaModule } from './entidades/cuenta/cuenta.module';
import { AlbumModule } from './entidades/album/album.module';
import { MediaModule } from './entidades/media/media.module';
import { AuthMiddleware } from './shared/middleware/auth.middleware';
import { NoticiaModule } from './entidades/noticia/noticia.module';
import { DireccionModule } from './entidades/direccion/direccion.module';
import { MultiIdiomaModule } from './multiIdioma/multiIdioma.module';
import { ProyectosModule } from './entidades/proyectos/proyectos.module';
import { RolModule } from './entidades/rol/rol.module';

import { ScheduleModule } from '@nestjs/schedule';

import { MongooseModule } from '@nestjs/mongoose';
import { ConfigService } from './config/config.service';
import { MonedaModule } from './entidades/moneda/catalogos.module';
import { EventosModule } from './entidades/eventos/eventos.module';
import { NotificacionModule } from './entidades/notificacion/notificacion.module';
import { FirebaseModule } from './drivers/firebase/firebase.module';
import { IntercambioModule } from './entidades/intercambio/intercambio.module';
import { ErrorsGateway } from './shared/errors.gateway';
import { AnunciosModule } from './entidades/anuncios/anuncios.module';
import { ComentariosIntercambioModule } from './entidades/comentarios_intercambio/comentarios-intercambio.module';
import { ParticipanteIntercambioModule } from './entidades/participante_intercambio/participante-intercambio.module';
import { DocumentoUsuarioModule } from './entidades/documentos_usuario/documento-usuario.module';
import { StripeModule } from './drivers/stripe/stripe.module';
import { LogSistemaModule } from './entidades/log_sistema/log-sistema.module';
import { idiomas } from './shared/enum-sistema';
import { CoinpaymentezModule } from './drivers/coinpaymentez/coinpaymentez.module';
import { CoinMarketCapModule } from './drivers/coinMarketCap/coinMarketCap.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule,
    MultiIdiomaModule,
    UsuarioModule,
    HistoricoModule,
    PensamientoModule,
    DocumentosLegalesModule,
    DBModule,
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGO_URI'),
      }),
      inject: [ConfigService],
    }),
    LoginModule,
    TransaccionModule,
    DispositivosModule,
    I18nModule.forRoot({
      fallbackLanguage: idiomas.ingles,
      fallbacks: {
        fr: idiomas.frances,
        es: idiomas.espanol,
        pt: idiomas.portugues,
        it: idiomas.italiano,
        de: idiomas.aleman,
      },
      parser: I18nJsonParser,
      parserOptions: {
        path: path.join(__dirname, '/i18n/'),
      },
    }),
    StorageModule,
    ArchivoModule,
    DispositivosModule,
    PaisModule,
    CatalogosModule,
    PerfilModule,
    CuentaModule,
    AlbumModule,
    MediaModule,
    AuthModule,
    NoticiaModule,
    DireccionModule,
    ProyectosModule,
    ComentariosModule,
    RolModule,
    AsociacionModule,
    ParticipanteAsociacionModule,
    ConversacionModule,
    EmailModule,
    FinanzasModule,
    MonedaModule,
    EventosModule,
    NotificacionModule,
    FirebaseModule,
    IntercambioModule,
    HttpModule,
    AnunciosModule,
    ParticipanteIntercambioModule,
    ComentariosIntercambioModule,
    DocumentoUsuarioModule,
    StripeModule,
    LogSistemaModule,
    CoinpaymentezModule,
    CoinMarketCapModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpErrorFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    ErrorsGateway,
  ],
  exports: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude(
        'api/email/confirmar-email(.*)',
        'api/transaccion(.*)',
        'api/actualizar-contrasenia(.*)',
        'api/email/confirmar-peticion-datos(.*)',
        //'api/obtener-participante-asociacion(.*)',
        'api/email/confirmar-peticion-eliminacion-datos(.*)',
        'api/perfil/nombre-contacto-unico(.*)',
      )
      .forRoutes('');
  }
}
