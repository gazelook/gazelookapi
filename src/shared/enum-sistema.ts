export enum filtroBusqueda {
  NUEVOS_PROYECTO = 'nuevos_proyectos',
  MENOS_VOTADOS = 'menos_votados',
  MAS_VOTADOS = 'mas_votados',
  VOTO = 'voto',
  FECHA = 'fecha',
  ALFA = 'alfa',
}

export enum filtrosCambioEstadoProyecto {
  activa = 'activa',
  eliminado = 'eliminado',
  preEstrategia = 'preEstrategia',
  esperaFondos = 'esperaFondos',
  revision = 'revision',
  ejecucion = 'ejecucion',
  esperaDonacion = 'esperaDonacion',
  finalizado = 'finalizado',
  foro = 'foro',
  estrategia = 'estrategia',
}

export enum idiomas {
  espanol = 'es',
  ingles = 'en',
  aleman = 'de',
  frances = 'fr',
  italiano = 'it',
  portugues = 'pt',
}

export enum allIdiomas {
  afrikaans = 'af',
  arabe = 'ar',
  asames = 'as',
  bengali = 'bn',
  bosnioLatino = 'bs',
  bulgaro = 'bg',
  cantonesTradicional = 'yue',
  catalan = 'ca',
  chinoSimplificado = 'zh-Hans',
  chinoTradicional = 'zh-Hant',
  croata = 'hr',
  checo = 'cs',
  dari = 'prs',
  danes = 'da',
  neerlandes = 'nl',
  ingles = 'en',
  estonio = 'et',
  fiyiano = 'fj',
  filipino = 'fil',
  finés = 'fi',
  frances = 'fr',
  alemán = 'de',
  griego = 'el',
  gujarati = 'gu',
  criolloHaitiano = 'ht',
  hebreo = 'he',
  hindi = 'hi',
  hmongDaw = 'mww',
  hungaro = 'hu',
  islandés = 'is',
  indonesio = 'id',
  irlandés = 'ga',
  italiano = 'it',
  japonés = 'ja',
  canarés = 'kn',
  kazajo = 'kk',
  klingon = 'tlh-Latn',
  klingonPlqaD = 'tlh-Piqd',
  coreano = 'ko',
  kurdoCentral = 'ku',
  kurdoNorte = 'kmr',
  leton = 'lv',
  lituano = 'lt',
  malgache = 'mg',
  malayo = 'ms',
  malayalam = 'ml',
  maltes = 'mt',
  maori = 'mi',
  maratí = 'mr',
  moruego = 'nb',
  odia = 'or',
  pastún = 'ps',
  persa = 'fa',
  polaco = 'pl',
  portuguésBrasil = 'pt-b',
  portuguésPortugal = 'pt-pt',
  punjabi = 'pa',
  otomiQueretaro = 'otq',
  rumano = 'ro',
  ruso = 'ru',
  samoano = 'sm',
  serbioCirílico = 'sr-Cyrl',
  serbioLatino = 'sr-Latn',
  eslovaco = 'sk',
  esloveno = 'sl',
  espaniol = 'es',
  swahili = 'sw',
  sueco = 'sv',
  tahitiano = 'ty',
  tamil = 'ta',
  telugu = 'te',
  tailandes = 'th',
  tongano = 'to',
  turco = 'tr',
  ucraniano = 'uk',
  urdu = 'ur',
  vietnamita = 'vi',
  gales = 'cy',
  mayaYucateco = 'yua',
}

export enum codIdiomas {
  espanol = 'IDI_1',
  ingles = 'IDI_2',
  aleman = 'IDI_3',
  portugues = 'IDI_4',
  frances = 'IDI_5',
  italiano = 'IDI_6',
}

export enum estadosPartAso {
  enviada = 'EST_96',
  eliminado = 'EST_93',
  rechazada = 'EST_95',
  cancelada = 'EST_94',
  contacto = 'EST_159',
}

export enum formulasEvento {
  TOTAL_VOTOS = 0,
  PORCENTAJE_VOTO_GAZELOOK = 0.16,
}

//Codigos de formulas de eventos
export enum codigosFormulasEvento {
  fondos_balance_usuarios = 'FOREVT_2',
  fondos_balance_gazelook = 'FOREVT_3',
  fecha_maxima_foro_proyecto = 'FOREVT_4',
  monto_por_porcentaje = 'FOREVT_7',
  pre_estrategia = 'FOREVT_5',
  esperaFondos = 'FOREVT_6',
  fondos_disponibles_tipo_proyecto = 'FOREVT_8',
  semanasxdias = 'FOREVT_9',
}

//Codigos de configuracion de estilos
export enum codigosConfiguracionEstilo {
  CONFEST6 = 'CONFEST6',
  CONFEST12 = 'CONFEST12',
}

//Codigos de configuracion de estilos
export enum perfilFalso {
  no_existe = 'Usuario no disponible',
}

//Codigos de roles
export enum codigosRol {
  //Codigo de rol de propietario
  cod_rol_ent_propietario = 'CATROL_4',
  //Codigo de rol de CoAutor
  cod_rol_ent_co_autor = 'CATROL_2',
  //Codigo de rol de usuario del sistema
  cod_rol_usuario_sistema = 'CATROL_5',
  //Codigo rol del coordinador gazelook
  cod_rol_coordinador_gazelook = 'CATROL_3',
  //Codigo rol de coautor de rol estratega
  cod_rol_coautor_estratega = 'CATROL_6',
  //Codigo rol de coautor de comentario intercambio
  cod_rol_coautor_comentario_intercambio = 'CATROL_11',
}

//codigo de los roles del sistema
export enum codigosRolSistema {
  cod_rol_sis_gazelook = 'CATROL_3',
}

//Codigos de las configuraciones de eventos
export enum codigosConfEvento {
  cod_configuracion_evento_proyecto = 'CONFEVT_1',
  cod_configuracion_evento_gasto_operacional = 'CONFEVT_2',
}

//Codigos de las catalogo tipo votos
export enum codigosCatTipoVoto {
  //Voto normal no administrador
  cod_cat_tipo_voto_normal = 'CTVOTO_1',
  //Voto foro no administrador
  cod_cat_tipo_voto_foro = 'CTVOTO_2',
  //Voto normal administrador
  cod_cat_tipo_voto_normal_admin = 'CTVOTO_3',
  //Voto foro administrador
  cod_cat_tipo_voto_foro_admin = 'CTVOTO_4',
}

//Codigos de TipoPerfil
export enum estadosPerfil {
  //Codigo
  hibernado = 'EST_19',
  activa = 'EST_20',
  creado = 'EST_88',
  eliminado = 'EST_89',
}

export enum estadosUsuario {
  //Codigo
  activa = 'EST_1',
  inactivaPago = 'EST_2',
  bloqueado = 'EST_3',
  bloqueoContrasena = 'EST_4',
  activaNoVerificado = 'EST_5',
  eliminado = 'EST_6',
  bloqueadoSistema = 'EST_7',
  rechazadoResponsable = 'EST_196',
  bloqueadoRefund = 'EST_282',
  noPermitirAcceso = 'EST_283',
  cripto = 'EST_284',
  inactivaPagoPaymentez = 'EST_291'
}

export enum estadosTransaccion {
  //Codigo
  activa = 'EST_8',
  eliminada = 'EST_9',
  cancelada = 'EST_10',
  rechazada = 'EST_11',
  pendiente = 'EST_80',
}

export enum codigosMetodosPago {
  //Codigo
  stripe = 'METPAG_1',
  payments1 = 'METPAG_3',
  payments2 = 'METPAG_6',
  panama = 'METPAG_7',
  cripto = 'METPAG_5',
}

export enum codigosArchivosSistema {
  //Codigo
  archivo_default_album_general = 'CAT_ARC_DEFAULT_01',
  archivo_default_album_perfil = 'CAT_ARC_DEFAULT_02',
  archivo_default_proyectos = 'CAT_ARC_DEFAULT_03',
  archivo_default_noticias = 'CAT_ARC_DEFAULT_04',
  archivo_default_contactos = 'CAT_ARC_DEFAULT_05',
  archivo_default_foto_perfil = 'CAT_ARC_DEFAULT_06',
  archivo_default_recursos_done = 'CAT_ARC_DEFAULT_07',
  archivo_default_recursos_demo = 'CAT_ARC_DEFAULT_08',
  archivo_default_demo_lista_contactos = 'CAT_ARC_DEFAULT_09',
  archivo_default_portada_anuncios = 'CAT_ARC_DEFAULT_10',
  archivo_default_tercios = 'CAT_ARC_DEFAULT_11',
}

export enum codigoCatalogosTipoEmail {
  //Codigo
  validacion_correo_normal = 'CTEM_1',
  validacion_correo_responsable = 'CTEM_2',
  transferir_proyecto = 'CTEM_3',
  recuperar_contrasenia = 'CTEM_4',
  actualizacion_contrasenia = 'CTEM_5',
  peticion_datos = 'CTEM_6',
  eliminacion_datos = 'CTEM_7',
  menor_edad_cambio_email = 'CTEM_8',
  descargar_informacion_usuario = 'CTEM_9',
  confirmar_actualizacion_email = 'CTEM_12',
  enviar_recibo_pago_email = 'CTEM_13',
  contact_gazelook = 'CTEM_14',
  correo_bienvenida = 'CTEM_15',
}

export enum estadoEvento {
  activa = 'EST_147',
  eliminado = 'EST_148',
  pendiente = 'EST_175',
  ejecutado = 'EST_176',
}

export enum catalogoOrigen {
  suscripciones = 'CATORI_1',
  gastos_operativos = 'CATORI_2',
  donacion = 'CATORI_3',
  ganancia_proyecto = 'CATORI_4',
  asignacion_fondo = 'CATORI_5',
  valor_extra = 'CATORI_6',
  fondos_reservados = 'CATORI_7',
  monto_sobrante = 'CATORI_8',
  fondos_reservados_fiscalizacion_proyectos = 'CATORI_9',
  fondos_reservados_gazelook = 'CATORI_10',
}

export enum catalogoTipoMensaje {
  texto = 'CTMSJ_1',
  archivo = 'CTMSJ_2',
  llamada = 'CTMSJ_3',
  video_llamada = 'CTMSJ_4',
  audio = 'CTMSJ_5',
  imagen = 'CTMSJ_6',
  video = 'CTMSJ_7',
  compartir_proyecto = 'CTMSJ_8',
  transferir_proyecto = 'CTMSJ_9',
  compartir_contacto = 'CTMSJ_10',
  compartir_noticia = 'CTMSJ_11',
}
export enum catalogoEstatusMensaje {
  aceptado = 'CTM_4',
  rechazado = 'CTM_5',
  enviado = 'CTM_1',
  entregado = 'CTM_2',
}

//Codigos de Catalogo tipo balance
export enum catalogoTipoBalance {
  fondos_gazelook = 'BAL_02',
  fondos_usuario = 'BAL_01',
}

// leidos de mensajes
export enum estadoDeMensaje {
  enviado = 'enviado',
  entregado = 'entregado',
  leido = 'leido',
  todos = 'todos',
}

export enum propiedadesPerfil {
  //Codigo
  pensamientos = 'pensamientos',
  noticias = 'noticias',
  proyectos = 'proyectos',
  asociaciones = 'asociaciones',
  telefonos = 'telefonos',
  direcciones = 'direcciones',
  album = 'album',
}

//Codigos de tipos de perfiles
export enum codigosTiposPerfil {
  //Codigo de perfil clasico
  tipoPerfilClasico = 'TIPERFIL_1',
  //Codigo de perfil Lúdico
  tipoPerfilLudico = 'TIPERFIL_2',
  //Codigo de perfil Sustituto
  tipoPerfilSustituto = 'TIPERFIL_3',
  //Codigo de perfil Grupo
  tipoPerfilGrupo = 'TIPERFIL_4',
}

export enum estadosProyecto {
  proyectoActivo = 'EST_110',
  proyectoEliminado = 'EST_111',
  proyectoPreEstrategia = 'EST_112',
  proyectoEnEsperaFondos = 'EST_113',
  proyectoEnEstrategia = 'EST_114',
  proyectoEnRevision = 'EST_115',
  proyectoEnEjecucion = 'EST_116',
  proyectoEnEsperaDonacion = 'EST_117',
  proyectoFinalizado = 'EST_118',
  proyectoForo = 'EST_195',
  preseleccionado = 'EST_292',
}

export enum estadosNoticia {
  noticiaActiva = 'EST_53',
  noticiaEliminada = 'EST_54',
}

//Codigos de catalogo Album
export enum codigosCatalogoAlbum {
  //Codigo del catalogo album general
  catAlbumGeneral = 'CATALB_1',
  //Codigo del catalogo album perfil
  catAlbumPerfil = 'CATALB_2',
  //Codigo del catalogo album links
  catAlbumLinks = 'CATALB_3',
  //Codigo del catalogo album audios
  catAlbumAudios = 'CATALB_4',
}

//Codigos de catalogo media
export enum codigosCatalogoMedia {
  //Codigo del catalogo media Link
  catMediaLink = 'CATMED_1',
  //Codigo del catalogo media compuesto
  catMediaCompuesto = 'CATMED_2',
  //Codigo del catalogo media simple
  catMediaSimple = 'CATMED_3',
}

//Codigos de catalogo Tipo Proyecto
export enum codigosCatalogoTipoProyecto {
  // mundial
  mundial = 'CAT_TIPO_PROY_01',
  // local
  local = 'CAT_TIPO_PROY_02',
  // Network
  network = 'CAT_TIP_PROY_03',
  // pais
  pais = 'CAT_TIP_PROY_04',
}

//Numero de Tipos de Proyectos que existen
export enum numeroTipoProyectos {
  total = 4,
}

//Numero de ejecuciones repartición de fondos
export enum numeroEjecucionesReparticionFondos {
  total = 15,
}

//Codigos de Tipo Moneda
export enum tipoMoneda {
  dolar = 'TIPMON_1',
  euro = 'TIPMON_2',
}

//tipo beneficiario
export enum beneficiario {
  usuario = 'BENEF_1',
  proyecto = 'BENEF_2',
}

//Codigos de catalogo historico
export enum codigosCatalogo {
  default = 'CATHIS_01',
  solicitudInformacion = 'CATHIS_02',
  solicitudRetiro = 'CATHIS_03',
  reparticionFondos = 'CATHIS_04',
}

//nombre catalogo tipo media
export enum codigoCatalogoTipoMedia {
  codImage = 'CATTIPMED_1',
  codVideo = 'CATTIPMED_2',
  codAudio = 'CATTIPMED_3',
  codFiles = 'CATTIPMED_4',
  codEnlace = 'CATTIPMED_5',
}
//Nombre de entidades
export enum nombreEntidades {
  usuarios = 'usuarios',
  proyectos = 'proyectos',
  noticias = 'noticias',
  pensamientos = 'pensamientos',
  comunicacion = 'comunicacion',
  transaccion = 'transaccion',
  compras = 'compras',
  perfiles = 'perfiles',
  contactos = 'contactos',
  comentarios = 'comentarios',
  busquedas = 'busquedas',
  media = 'media',
  dispositivo = 'dispositivo',
  email = 'email',
  metodoPago = 'metodoPago',
  tipoMoneda = 'tipoMoneda',
  origen = 'origen',
  beneficiario = 'beneficiario',
  archivo = 'archivo',
  telefono = 'telefono',
  direccion = 'direccion',
  traduccionDireccion = 'traduccionDireccion',
  asociacion = 'asociacion',
  participanteAsociacion = 'participanteAsociacion',
  conversacion = 'conversacion',
  mensaje = 'mensaje',
  configuracion = 'configuracion',
  votoProyecto = 'votoProyecto',
  participanteProyecto = 'participanteProyecto',
  estrategia = 'estrategia',
  configuracionEvento = 'configuracionEvento',
  evento = 'evento',
  formulaEvento = 'formulaEvento',
  suscripcion = 'suscripcion',
  catalogoSuscripcion = 'catalogoSuscripcion',
  catalogoTipoMedia = 'catalogoTipoMedia',
  album = 'album',
  traduccionMedia = 'traduccionMedia',
  catalogoTipoEmail = 'catalogoTipoEmail',
  catalogoPais = 'catalogoPais',
  catalogoLocalidad = 'catalogoLocalidad',
  traduccionNoticia = 'traduccionNoticia',
  catalogoMedia = 'catalogoMedia',
  catalogoArchivoDefault = 'catalogoArchivoDefault',
  catalogoTipoPerfil = 'catalogoTipoPerfil',
  votoNoticia = 'votoNoticia',
  traduccionVotoNoticia = 'traduccionVotoNoticia',
  catalogoTipoProyecto = 'catalogoTipoProyecto',
  traduccionCatalogoTipoProyecto = 'traduccionCatalogoTipoProyecto',
  traduccionProyecto = 'traduccionProyecto',
  catalogoAlbum = 'catalogoAlbum',
  catalogoIdiomas = 'catalogoIdiomas',
  traduccionPensamientos = 'traduccionPensamientos',
  catalogoTipoAsociacion = 'catalogoTipoAsociacion',
  traduccionMensajes = 'traduccionMensajes',
  catalogoMensaje = 'catalogoMensaje',
  catalogoTipoComentario = 'catalogoTipoComentario',
  traduccionComentario = 'traduccionComentario',
  traduccionVotoProyecto = 'traduccionVotoProyecto',
  balance = 'balance',
  rolSistema = 'rolSistema',
  catalogoTipoRol = 'catalogoTipoRol',
  catalogoConfiguracion = 'catalogoConfiguracion',
  catalogoTipoVoto = 'catalogoTipoVoto',
  catalogoRol = 'catalogoRol',
  proyectoEjecucion = 'proyectoEjecucion',
  traduccionProyectoEjecución = 'traduccionProyectoEjecución',
  catalogoColores = 'catalogoColores',
  catalogoTipoColores = 'catalogoTipoColores',
  catalogoEvento = 'catalogoEvento',
  rolEntidad = 'rolEntidad',
  configuracionEstilo = 'configuracionEstilo',
  configuracionPersonalizada = 'configuracionPersonalizada',
  configuracionPredeterminada = 'configuracionPredeterminada',
  estilos = 'estilos',
  catalogoEstilos = 'catalogoEstilos',
  CatalogoPaticipanteConfiguracion = 'CatalogoPaticipanteConfiguracion',
  configuracionParticipante = 'configuracionParticipante',
  tokenUsuario = 'tokenUsuario',
  documentosLegales = 'documentosLegales',
  catalogoOrigenDocumento = 'catalogoOrigenDocumento',
  catalogoHistorico = 'catalogoHistorico',
  catalogoTipoBalance = 'catalogoTipoBalance',
  catalogoEventoNotificacion = 'catalogoEventoNotificacion',
  notificacion = 'notificacion',
  bonificacion = 'bonificacion',
  intercambio = 'Intercambio',
  traduccionIntercambio = 'TraduccionIntercambio',
  traduccionCatalogoTipoIntercambio = 'TraduccionCatalogoTipoIntercambio',
  catalogoTipoIntercambio = 'CatalogoTipoIntercambio',
  anuncio = 'anuncio',
  traduccionAnuncio = 'traduccionAnuncio',
  catalogoTipoAnuncio = 'catalogoTipoAnuncio',
  tematicaAnuncio = 'tematicaAnuncio',
  comentarioIntercambio = 'comentarioIntercambio',
  participanteIntercambio = 'participanteIntercambio',
  traduccionComentarioIntercambio = 'traduccionComentarioIntercambio',
  documentoUsuario = 'documentoUsuario',
}

//Nombre de acciones
export enum nombreAcciones {
  ver = 'ver',
  modificar = 'modificar',
  eliminar = 'eliminar',
  crear = 'crear',
}

//Nombre de catalogo Estados
export enum nombrecatalogoEstados {
  desactivado = 'desactivado',
  activa = 'activa',
  inactivaPago = 'inactivaPago',
  bloqueado = 'bloqueado',
  bloqueoContrasena = 'bloqueoContrasena',
  activaNoVerificado = 'activaNoVerificado',
  eliminado = 'eliminado',
  bloqueadoSistema = 'bloqueadoSistema',
  rechazadoResponsable = 'rechazadoResponsable',
  hibernado = 'hibernado',
  historico = 'historico',
  retiroUsuario = 'retiroUsuario',
  pendiente = 'pendiente',
  contacto = 'contacto',
  enviada = 'enviada',
  aceptada = 'aceptada',
  sinAsignar = 'sinAsignar',
  foro = 'foro',
  foroSegundaEtapa = 'foroSegundaEtapa',
  noRenovada = 'noRenovada',
}

export enum codigoEstadosAsociacion {
  activa = 'EST_35',
  eliminado = 'EST_36',
  hibernado = 'EST_201',
}

export enum codigosEstadosPartAsociacion {
  enviada = 'EST_96',
  eliminado = 'EST_93',
  rechazada = 'EST_95',
  aceptada = 'EST_92',
  cancelada = 'EST_94',
  contacto = 'EST_159',
  hibernado = 'EST_193',
}

export enum codigosEstadosConversacion {
  activa = 'EST_97',
  eliminado = 'EST_98',
  hibernado = 'EST_194',
}
// codigos de catalogo historico
export enum codigosCatalogoHistorico {
  default = 'CATHIS_01',
  solicitudInformacion = 'CATHIS_02',
  solicitudRetiro = 'CATHIS_03',
}

// codigos estados de hibernado de entidades
export enum codigosHibernadoEntidades {
  asociacionHibernado = 'EST_201',
  conversacionHibernado = 'EST_194',
  participanteAsocHibernado = 'EST_193',
  noticiaHibernado = 'EST_192',
  pensamientoHibernado = 'EST_191',
  proyectoHibernado = 'EST_190',
  direccionHibernado = 'EST_189',
  albumHibernado = 'EST_188',
  mediaHibernado = 'EST_187',
  telefonoHibernado = 'EST_238',
}

export enum codigoEntidades {
  entidadUsuarios = 'ENT_1',
  entidadProyectos = 'ENT_2',
  entidadNoticias = 'ENT_3',
  entidadPensamientos = 'ENT_4',
  entidadTransaccion = 'ENT_6',
  entidadPerfiles = 'ENT_8',
  entidadcontactos = 'ENT_9',
  entidadComentarios = 'ENT_10',
  entidadMedia = 'ENT_12',
  entidadDispositivo = 'ENT_13',
  entidadBeneficiario = 'ENT_18',
  entidadTelefono = 'ENT_20',
  entidadDireccion = 'ENT_21',
  entidadAsociacion = 'ENT_22',
  entidadParticipanteAsociacion = 'ENT_23',
  entidadConversacion = 'ENT_24',
  entidadMensaje = 'ENT_25',
  entidadVotoProyecto = 'ENT_31',
  entidadParticipanteProyecto = 'ENT_32',
  entidadEstrategia = 'ENT_34',
  entidadSuscripcion = 'ENT_38',
  entidadAlbum = 'ENT_42',
  entidadCatalogoLocalidad = 'ENT_46',
  entidadTraduccionNoticia = 'ENT_47',
  entidadVotoNoticia = 'ENT_51',
  entidadTraduccionProyecto = 'ENT_55',
  fondosFinanciamiento = 'ENT_95',
  fondosTipoProyecto = 'ENT_96',
  entidadBonificacion = 'ENT_97',
  intercambio = 'ENT_100',
  traduccionIntercambio = 'ENT_101',
  catalogoTipoIntercambio = 'ENT_102',
  traduccionCatalogoTipoIntercambio = 'ENT_103',
  entidadNotificacionesFirefase = 'ENT_107',
  entidadAnuncio = 'ENT_111',
  entidadTraduccionAnuncio = 'ENT_112',
  entidadCatalogoTipoAnuncio = 'ENT_113',
  entidadTematicaAnuncio = 'ENT_114',
  comentarioIntercambio = 'ENT_115',
  participanteIntercambio = 'ENT_116',
  traduccionComentarioIntercambio = 'ENT_117',
  mensajesAnuncio = 'ENT_122',
  traduccionPensamiento = 'ENT_58',
  devolucion = 'ENT_125',
}

// nombres del catalogo tipo comentario
export enum nombreCatComentario {
  foro = 'foro',
  normal = 'normal',
  estrategia = 'estrategia',
}

// codigos del catalogo tipo comentario
export enum codigosCatComentario {
  foro = 'CATIPCOM_1',
  normal = 'CATIPCOM_2',
  estrategia = 'CATIPCOM_3',
}

// nombres de estados transaccion pagos
export enum nombreEstadosPagos {
  succeeded = 'succeeded',
  COMPLETED = 'COMPLETED',
}

// costo suscripcion gazelook
export enum costoSuscripcionGazelook {
  costoLanzamiento = 18,
}

// descripcion transaccion pagos gazelook
export enum descripcionTransPagosGazelook {
  suscripcionDeAlta = 'Suscripción de alta',
  suscripcion = 'Suscripción',
  gastoOperacional = 'Gasto operacional',
  valorExtra = 'Valor extra',
  fondosReservados = 'Fondos reservados',
  montoSobrante = 'Monto sobrante de la última repartición de fondos',
  fondosReservadosFiscalizacionProyectos = 'Fondos reservados para la fizcalizacion de los proyectos',
  montoCripto = 'Monto ingresado con Criptomonedas'
}

// datos pago stripe
export enum pagoStripe {
  currency = 'USD',
  payment_method_types = 'card',
  apiVersion = '2020-03-02',
}

// datos pago paypal
export enum pagoPaypal {
  currency_code = 'USD',
  value = '18.00',
  intent = 'CAPTURE',
  return_url = 'https://gazelook.com/pago-exitoso',
  cancel_url = 'https://gazelook.com/pago-cancelado',
  brand_name = 'Gazelook.com',
}

// tipo de suscripcion
export enum tipoSuscripcion {
  anual = 'anual',
}

export enum codigosEstadoSuscripcion {
  activa = 'EST_37',
  finalizada = 'EST_210',
  retiroUsuario = 'EST_184',
  pendiente = 'EST_81',
  noRenovada = 'EST_40',
  cancelada = 'EST_39',
  eliminada = 'EST_38',
}

export enum codigosCatalogoSuscripcion {
  anual = 'CATSUS_1',
  semestral = 'CATSUS_2',
  mensual = 'CATSUS_3',
}

// codigos de la configuracion eventos
export enum codigosConfiguracionEvento {
  //finanzas
  finanzasCONFEVT_2 = 'CONFEVT_2',
  //rango de foro
  eventoForoProyectosCONFEVT_3 = 'CONFEVT_3',
  //inicio de foro
  eventoForoProyectosCONFEVT_4 = 'CONFEVT_4',
  // bloquear cuenta responsable - email
  eventoBloquearResponsableEmail = 'CONFEVT_5',
  // Obtener monto por porcentaje
  montoPorPorcentaje = 'CONFEVT_8',
  // Obtener configuracion de pre-estrategia
  preEstrategia = 'CONFEVT_6',
  // Obtener configuracion de espera de fondos
  esperaFondos = 'CONFEVT_7',
}

// nombre catalogo tipo email
export enum nombreCatalogoTipoEmail {
  validacion = 'validacion',
  validacionResponsable = 'validacionResponsable',
  trasferirProyecto = 'trasferirProyecto',
  recuperarContrasenia = 'recuperarContrasenia',
  actualizacionContrasenia = 'actualizacionContrasenia',
  peticionDatos = 'peticionDatos',
  eliminacionDatos = 'eliminacionDatos',
  notificacionUsuario = 'notificacionUsuario',
}

// cod dispositivo
export enum codigoDispositivo {
  codigo = 'DIS',
}

// catalogo dispositivo
export enum estadosDispositivo {
  activo = 'EST_45',
  eliminado = 'EST_46',
}

// estados participantes asociacion
export enum estadosParticipanteAsociacion {
  ok = '1',
}
// estados catalogo tipo asociacion
export enum codigosTipoAsociacion {
  contacto = 'CTAS_1',
  grupo = 'CTAS_2',
}

// nombre de catalogo tipo asociacion
export enum nombreTipoAsociacion {
  contacto = 'contacto',
  grupo = 'grupo',
}

// tiempos de duracion eventos
export enum duracionEventos {
  limiteMaximoEstimacionProyecto = 15,
}

// intervalo de eventos (sera llamado desde la BD)
export enum intervaloEventos {
  intervalo = '',
}

//Codigos de catalogo evento notificacion
export enum codigosCatalogoEventoNotificacion {
  eliminarMensaje = 'CATEVTNOT_1',
  trasferirProyecto = 'CATEVTNOT_2',
  mensajeNoLeido = 'CATEVTNOT_3',
  crearComentario = 'CATEVTNOT_4',
  llamadaDeAudio = 'CATEVTNOT_5',
  videoLlamada = 'CATEVTNOT_6',
}

export enum codigosCatalogoStatusNotificacion {
  entregada = 'CATSTANOT_1',
  leida = 'CATSTANOT_2',
}
export enum codigosAccionNotificacion {
  llamando = 'ACN_1',
  aceptar = 'ACN_2',
  rechazar = 'ACN_3',
}

export enum codigosEstadosNotificacion {
  activa = 'EST_204',
  eliminado = 'EST_205',
}

export enum codigosEstadosMedia {
  activa = 'EST_24',
  eliminado = 'EST_25',
  sinAsignar = 'EST_26',
}

export enum codigosCatalogoOrigenDoc {
  terminosCondiciones = 'CATORIDOC1',
  cuentaBancaria = 'CATORIDOC2',
}

export enum codigosCatalogoAcciones {
  modificar = 'ACC_1',
  crear = 'ACC_4',
  ver = 'ACC_1',
  eliminar = 'ACC_3',
}

export enum codigosEstadosTraduccionProyecto {
  activa = 'EST_76',
  eliminado = 'EST_77',
}

export enum codigosEstadosTraduccionNoticia {
  activa = 'EST_59',
  eliminado = 'EST_60',
}

export enum codigosEstadosCatalogoPorcentajeProyecto {
  activa = 'EST_211',
  eliminado = 'EST_212',
}

export enum codigosEstadosCatalogoPorcentajeFinanciacion {
  activa = 'EST_213',
  eliminado = 'EST_214',
}

export enum codigosEstadosBonificacion {
  activa = 'EST_219',
  eliminado = 'EST_220',
}

export enum codigosEstadosCatalogoPorcentajeEsperaFondos {
  activa = 'EST_221',
  eliminado = 'EST_222',
}

export enum codigosEstadosFondosTipoProyecto {
  activa = 'EST_217',
  eliminado = 'EST_218',
}

export enum codigosEstadosFondosFinanciacion {
  activa = 'EST_215',
  eliminado = 'EST_216',
}

export enum codigosEstadosBeneficiario {
  activa = 'EST_16',
  eliminado = 'EST_18',
}

export enum filtroBusquedaGastoOperacional {
  MUNDIAL = 'mundial',
  PAIS = 'pais',
  LOCALIDAD = 'localidad',
}

export enum filtroBusquedaAportaciones {
  suscripcion = 'suscripcion',
  valorExtra = 'valorExtra',
}

export enum codigosEstadosFinanciamientoEsperaFondos {
  activa = 'EST_223',
  eliminado = 'EST_224',
}

//Codigo de balance
export enum codigoDeBalance {
  bal_ = 'BAL_',
}

//Codigos estados traduccion intercambio
export enum codigosEstadosTraduccionIntercambio {
  activa = 'EST_231',
  eliminado = 'EST_232',
}

//Codigos catalogo estatus intercambio
export enum codigosCatalogoEstatusIntercambio {
  habilitada = 'CATESTINTER_1',
  deshabilitada = 'CATESTINTER_2',
}

//Codigos catalogo rol
export enum codigosCatalogoRol {
  administrador = 'CATROL_1',
  coautor = 'CATROL_2',
  coordinadorGazelook = 'CATROL_3',
  propietario = 'CATROL_4',
  usuario = 'CATROL_5',
  estrategas = 'CATROL_6',
  lector = 'CATROL_7',
  escritor = 'CATROL_8',
  administradorAnuncios = 'CATROL_9',
  administradorTraductorAnuncios = 'CATROL_10',
  administradorGeneralAnuncios = 'CATROL_12',
  subAdministradorGeneral = 'CATROL_13',
  contabilidad = 'CATROL_14',
  administradorFinca = 'CATROL_15',
  administradorProgramadores = 'CATROL_16',
  subAdministradorProgramadores = 'CATROL_17',
  administradorVoluntariado = 'CATROL_18',
  alojamientoPersonalOficina = 'CATROL_19',
  usuarioResponsableProyecto = 'CATROL_20',
  usuarioEquipoProyecto = 'CATROL_21',
}

//Codigos de estados de estrategia
export enum estadosEstrategia {
  aprobada = 'EST_123',
  rechazada = 'EST_124',
  eliminado = 'EST_125',
  enEspera = 'EST_126',
  caducada = 'EST_127',
}

export enum estadoLocalidad {
  activa = 'EST_57',
  eliminado = 'EST_58',
  enRevision = 'EST_235',
}
//Codigos de catalogo tipo proyecto
export enum codigosTipoProyecto {
  proyectoMundial = 'CAT_TIPO_PROY_01',
  proyectoLocal = 'CAT_TIPO_PROY_02',
  proyectoDeRed = 'CAT_TIPO_PROY_03',
  proyectoPais = 'CAT_TIPO_PROY_04',
}

//Estados del catalogo repartir fondos
export enum codigosEstadosCatalogoRepartirFondos {
  activa = 'EST_236',
  eliminado = 'EST_237',
}

export enum dimensionesImagen {
  miniatura = `650x650`,
}

export enum codigoestadosTraaduccionComentario {
  activa = 'EST_155',
  eliminado = 'EST_156',
}

export enum codigosCatalogoPorcentajeFinanciacion {
  preEstrategia = 'CATPORCFIN_1',
  esperaFondos = 'CATPORCFIN_2',
}

export enum accionNotificacionFirebase {
  ver = 'ACCNOTFIR_1',
  actualizarTokenDispositivos = 'ACCNOTFIR_2',
  cerrarSesion = 'ACCNOTFIR_3',
}

export enum estadoNotificacionesFirebase {
  activa = 'EST_241',
  eliminado = 'EST_242',
}

export enum tipoNotificacionFirebase {
  usuario = 'TIPNOTFIR_1',
  sistema = 'TIPNOTFIR_2',
}

export enum codigoEstadoAlbum {
  activa = 'EST_47',
  eliminado = 'EST_48',
}

//Codigo de estados comentarios
export enum codigoEstadosComentario {
  activa = 'EST_105',
  eliminado = 'EST_106',
  historico = 'EST_107',
}

export enum limiteArchivo {
  audio = 52428800, //Bytes to 40 MB
  general = 52428800, //Bytes to 20 MB
}

export enum expiracionTokenDispositivo {
  dias = 5,
}
export enum expiracionToken {
  dias = '10d',
  segundos = '432000s',
}

export enum codidosEstadosTraduccionDireccion {
  activa = 'EST_247',
  eliminado = 'EST_248',
}

export enum codidosEstadosAnuncio {
  activa = 'EST_249',
  eliminado = 'EST_250',
}
export enum codidosEstadosTraduccionAnuncio {
  activa = 'EST_251',
  eliminado = 'EST_252',
}

export enum codidosEstadosCatalogoTipoAnuncio {
  activa = 'EST_253',
  eliminado = 'EST_254',
}

export enum codigosCatalogoTipoAnuncio {
  ADVERTISEMENT = 'TIPANU_01',
  INFORMATION = 'TIPANU_02',
  REFLECTION = 'TIPANU_03',
  FINANCE = 'TIPANU_04',
}

export enum codigosCatalogoTipoIntercambio {
  cultural = 'CATIPOINTER_1',
  objetosProductos = 'CATIPOINTER_2',
  alojamiento = 'CATIPOINTER_3',
  habilidades = 'CATIPOINTER_4',
}

//Anuncios
export enum rangoNumericoAnuncios {
  inicio = 1,
  fin = 500,
}

export enum rangoAlfabeticoAnuncios {
  inicio = 'A',
  fin = 'Z',
}

export enum codigosEstilosAnuncios {
  fondo = 'CATEST3',
  letra = 'CATEST4',
}

export enum codigosEstadosIntercambio {
  activa = 'EST_229',
  eliminado = 'EST_230',
}

export enum codigosEstadosTematicaAnuncio {
  activa = 'EST_255',
  eliminado = 'EST_256',
}

export enum codigosEstadosComentarioIntercambio {
  activa = 'EST_257',
  eliminado = 'EST_258',
}

export enum codigosEstadosParticipantentercambio {
  activa = 'EST_259',
  eliminado = 'EST_260',
}

export enum codigosEstadosTraduccionComentarioIntercambio {
  activa = 'EST_261',
  eliminado = 'EST_262',
}

export enum codigosEstadosCatalogoTipoDocumentoUsuario {
  activa = 'EST_263',
  eliminado = 'EST_264',
}

export enum codigosCatalogoTipoDocumentoUsuario {
  usuario = 'TIPDOCUSER_01',
  representante = 'TIPDOCUSER_02',
}

export enum codigosEstadosDocumentoUsuario {
  activa = 'EST_265',
  eliminado = 'EST_266',
}

export enum estadosEstilosAnuncio {
  activa = 'EST_270',
  eliminado = 'EST_271',
}

export enum estadosConfiguracionEstilosAnuncio {
  activa = 'EST_268',
  eliminado = 'EST_269',
}

export enum codigosCatalogoConfiguracion {
  predeterminada = 'CATCONF1',
  personalizada = 'CATCONF2',
}

export enum numeroDias {
  domingo = 0,
  lunes = 1,
  martes = 2,
  miercoles = 3,
  jueves = 4,
  viernes = 5,
  sabado = 6,
}

export enum listMonedaConversionDefault {
  EUR = 'EUR',
  USD = 'USD',
}
export enum estadosMensajesAnuncio {
  activa = 'EST_272',
  eliminado = 'EST_273',
}

export enum estadosCatalogoPorcentajeFondosReservados {
  activa = 'EST_274',
  eliminado = 'EST_275',
}

export enum estadoMetodosPago {
  activa = 'EST_14',
  eliminado = 'EST_15',
}

export enum estadosTraduccionMetodosPago {
  activa = 'EST_276',
  eliminado = 'EST_277',
}

export enum estadosTraduccionPensamiento {
  activa = 'EST_86',
  eliminado = 'EST_87',
}

export enum estadosDevolucion {
  activa = 'EST_278',
  eliminado = 'EST_279',
  pendiente = 'EST_280',
  success = 'EST_281',
}

export enum diasMaximosDevolucion {
  dias = 3,
}

export enum statusRefundStripe {
  succeeded = 'succeeded',
}

export enum dias {
  año = 365,
}

export enum codigosEstadosMensajes {
  activa = 'EST_99',
  eliminado = 'EST_100',
}

export enum porcentajeComisionPaymentez {
  porcentaje = 0.045,
}

export enum porcentajeComisionCoinpaiments {
  porcentaje = 0.05,
}

export enum estadosConfiguracionPreestrategia {
  activa = 'EST_285',
  inactivo = 'EST_286',
}

export enum estadosConfiguracionForo {
  activa = 'EST_287',
  inactivo = 'EST_288',
}

export enum estadosConfiguracionBalance {
  activa = 'EST_289',
  inactivo = 'EST_290',
}
export enum codigosConfiguracionPreestrategia {
  CONFPRE_1 = 'CONFPRE_1',
  CONFPRE_2 = 'CONFPRE_2',
  CONFPRE_3 = 'CONFPRE_3',
}
export enum codigosConfiguracionForo {
  CONFORO_1 = 'CONFORO_1',
  CONFORO_2 = 'CONFORO_2',
  CONFORO_3 = 'CONFORO_3',
}

export enum codigosConfiguracionBalance {
  CONFBAL_1 = 'CONFBAL_1',
  CONFBAL_2 = 'CONFBAL_2',
  CONFBAL_3 = 'CONFBAL_3',
}