import { codigosEstadosMedia } from './enum-sistema';

export const populateGetMedia = {
  path: 'media',
  select: '-fechaCreacion -fechaActualizacion -__v',
  populate: [
    {
      path: 'principal',
      select: 'url',
    },
    {
      path: 'miniatura',
      select: 'url',
    },
    {
      path: 'traducciones',
      select: 'nombre',
    },
  ],
};
export const populateGetMediaActiva = {
  path: 'media',
  select: '-fechaCreacion -fechaActualizacion -__v',
  match: { estado: codigosEstadosMedia.activa },
  populate: [
    {
      path: 'principal',
      select: 'url',
    },
    {
      path: 'miniatura',
      select: 'url',
    },
    {
      path: 'traducciones',
      select: 'nombre',
    },
  ],
};

export function populateGetMediaTraduciones(
  estadoMedia,
  estadoTraduccion,
  codigoIdioma,
) {
  return {
    path: 'media',
    select: '-fechaCreacion -fechaActualizacion -__v',
    match: { estado: estadoMedia },
    populate: [
      {
        path: 'principal',
        select: 'url fileDefault duracion fechaActualizacion',
      },
      {
        path: 'miniatura',
        select: 'url',
      },
      {
        path: 'traducciones',
        match: {
          idioma: codigoIdioma,
          estado: estadoTraduccion,
        },
      },
    ],
  };
}

export function populateGetPortadaTraduccion(estadoTraduccion, codigoIdioma) {
  return {
    path: 'portada',
    select: '-fechaCreacion -fechaActualizacion -__v',
    populate: [
      {
        path: 'principal',
        select: 'url duracion fechaActualizacion',
      },
      {
        path: 'miniatura',
        select: 'url duracion',
      },
      {
        path: 'traducciones',
        match: {
          idioma: codigoIdioma,
          estado: estadoTraduccion,
        },
      },
    ],
  };
}

export const populatePortada = {
  path: 'portada',
  select: '-fechaCreacion -fechaActualizacion -__v',
  populate: [
    {
      path: 'principal',
      select: 'url',
    },
    {
      path: 'miniatura',
      select: 'url',
    },
  ],
};
