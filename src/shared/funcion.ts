import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import {
  estadoDeMensaje,
  estadosPartAso,
  idiomas,
  numeroDias,
  rangoAlfabeticoAnuncios,
  rangoNumericoAnuncios,
} from './enum-sistema';
import { RespuestaInterface } from './respuesta-interface';
import { I18nService } from 'nestjs-i18n';
import { Mensaje } from 'src/drivers/mongoose/interfaces/mensaje/mensaje.interface';
import { ObtenerPortadaPredeterminadaProyectoService } from 'src/entidades/proyectos/casos_de_uso/obtener-portada-predeterminada-proyecto.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerNoticiaIdService } from 'src/entidades/noticia/casos_de_uso/obtener-noticia-id.service';
import { ObtenerPortadaService } from 'src/entidades/noticia/casos_de_uso/obtener-portada-predeterminada.service';
import { ObtenerProyetoIdService } from 'src/entidades/proyectos/casos_de_uso/obtener-proyecto-id.service';
import { ObtenerPortadaPredeterminadaProyectoResumenService } from 'src/entidades/proyectos/casos_de_uso/obtener-portada-predeterminada-proyecto-resumen.service';

export class Funcion {
  //ALfabeto mayusculo
  abecedario = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I',
    'J',
    'K',
    'L',
    'M',
    'N',
    'Ñ',
    'O',
    'P',
    'Q',
    'R',
    'S',
    'T',
    'U',
    'V',
    'W',
    'X',
    'Y',
    'Z',
  ];

  //Expresion regular para validar  solo abecedario, numero y simbolos _ - y espacios en blanco
  expReg = '^[a-zA-ZÀ-ÖØ-öø-ÿÂ âÆ æÇ çÉ éÈ èÊ êË ëÎ îÏ ïÔ ôŒ œÙ ùÛ ûÜ üŸ ÿ0-9_ .-]+$';

  //codigos de roles administradores
  rolesAdmin = [
    'CATROL_1',
    'CATROL_9',
    'CATROL_10',
    'CATROL_12',
    'CATROL_13',
    'CATROL_14',
    'CATROL_15',
    'CATROL_16',
    'CATROL_17',
    'CATROL_18',
    'CATROL_19',
  ];

  
  constructor(
    private readonly i18n?: I18nService,
    private readonly catalogoEntidadService?: CatalogoEntidadService,
    private readonly catalogoEstadoService?: CatalogoEstadoService,
  ) { }

  numeroDia = numeroDias;
  
  obtenerIdiomaDefecto(idioma) {
    if (idioma) {
      return idioma;
    }
    return idiomas.ingles;
  }

  //Verifica idiomas por defecto del sistema
  /*obtenerIdiomasDefecto(idioma) {
        if (idioma === idiomas.ingles) {
            return true;
        }
        if (idioma === idiomas.espanol) {
            return true;
        }
        if (idioma === idiomas.aleman) {
            return true;
        }
        if (idioma === idiomas.portugues) {
            return true;
        }
        if (idioma === idiomas.italiano) {
            return true;
        }
        if (idioma === idiomas.frances) {
            return true;
        }
        return false;

    }*/

  enviarRespuesta(codigoEstado, mensaje?, datos?) {
    const respuesta = new RespuestaInterface();

    respuesta.codigoEstado = codigoEstado;
    respuesta.respuesta = {
      mensaje: mensaje,
      datos: datos,
    };

    return respuesta;
  }
  enviarRespuestaOptimizada(resp) {
    const respuesta = new RespuestaInterface();
    respuesta.codigoEstado = resp.codigoEstado;
    if (resp.mensaje && !resp.datos) {
      respuesta.respuesta = {
        mensaje: resp.mensaje,
      };
    }
    if (resp.datos && !resp.mensaje) {
      respuesta.respuesta = {
        datos: resp.datos,
      };
    }
    if (resp.datos && resp.mensaje) {
      respuesta.respuesta = {
        mensaje: resp.mensaje,
        datos: resp.datos,
      };
    }
    if (resp.mensaje && resp.token) {
      respuesta.respuesta = {
        mensaje: resp.mensaje,
        token: resp.token,
      };
    }
    return respuesta;
  }

  seleccionarFiltroMensaje(filtro) {
    switch (filtro) {
      case estadoDeMensaje.enviado:
        return 'CTM_1';
      case estadoDeMensaje.entregado:
        return 'CTM_2';
      case estadoDeMensaje.leido:
        return 'CTM_3';
      case estadoDeMensaje.todos:
        return 1;
      default:
        return 1;
    }
  }

  verificarEstadoAsociacion(filtro) {
    switch (filtro) {
      case estadosPartAso.cancelada:
        return true;
      case estadosPartAso.enviada:
        return true;
      case estadosPartAso.contacto:
        return true;
      case estadosPartAso.rechazada:
        return true;
      case estadosPartAso.eliminado:
        return true;
      default:
        return false;
    }
  }

  async obtenerTraduccionEstatica(codigoIdioma, codigoMensaje) {
    return await this.i18n.translate(
      codigoIdioma.concat('.').concat(codigoMensaje),
      {
        lang: codigoIdioma,
      },
    );
  }

  async obtenerCatalogoEstadoEntidad(nombreEntidad, nombreEstado) {
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidad,
    );
    const estado = await this.catalogoEstadoService.obtenerNombreEstado(
      nombreEstado,
      entidad.codigo,
    );

    return {
      catalogoEntidad: entidad,
      catalogoEstado: estado,
    };
  }

  getListaPopulateMensaje() {
    const populateConversacion = {
      path: 'conversacion',
      select: '_id',
    };

    const populateTraducciones = {
      path: 'traducciones',
      select: '-_id contenido',
    };

    const populateAdjuntos = {
      path: 'adjuntos',
      select: 'principal miniatura catalogoMedia fechaActualizacion',
      populate: [
        {
          path: 'principal',
          select: 'url tipo filename fileDefault duracion fechaActualizacion',
        },
        {
          path: 'miniatura',
          select: 'filename url tipo',
        },
      ],
    };

    const populateMedia = {
      path: 'media',
      select:
        'principal miniatura catalogoMedia fechaActualizacion traducciones',
      populate: [
        {
          path: 'principal',
          select: 'url tipo filename fileDefault duracion fechaActualizacion',
        },
        {
          path: 'miniatura',
          select: 'filename url tipo',
        },
      ],
    };

    const populateReferenciaEntidadCompartida = {
      path: 'referenciaEntidadCompartida',
      select: '_id traducciones adjuntos fechaActualizacion',
      populate: [
        {
          path: 'traducciones',
          select: 'idioma titulo tituloCorto',
        },
        {
          path: 'adjuntos',
          populate: [
            populateMedia,
            {
              path: 'portada',
              populate: populateMedia,
            },
          ],
        },
      ],
    };

    const populatePropietario = {
      path: 'propietario',
      select: 'perfil',
      populate: { path: 'perfil', select: 'nombreContacto nombre' },
    };

    return [
      populateConversacion,
      populateTraducciones,
      populatePropietario,
      populateAdjuntos,
      //populateReferenciaEntidadCompartida
    ];
  }

  async formatoMensaje(
    mensaje: any,
    codIdioma: string,
    catalogoIdiomasService: CatalogoIdiomasService,
    obtenerPortadaPredeterminadaProyectoResumenService: ObtenerPortadaPredeterminadaProyectoResumenService,
    obtenerProyetoIdService: ObtenerProyetoIdService,
    obtenerNoticiaIdService: ObtenerNoticiaIdService,
    obtenerPortadaService: ObtenerPortadaService,
    opts,
  ) {
    const idioma = await catalogoIdiomasService.obtenerIdiomaByCodigoNombre(
      codIdioma,
    );

    mensaje.estado = {
      codigo: mensaje.estado,
    };

    mensaje.estatus = {
      codigo: mensaje.estatus,
    };

    mensaje.tipo = {
      codigo: mensaje.tipo,
    };

    mensaje.entidadCompartida = {
      codigo: mensaje.entidadCompartida,
    };

    var listaAdjuntos = mensaje.adjuntos;
    for (let adjunto of listaAdjuntos) {
      adjunto['catalogoMedia'] = {
        codigo: adjunto['catalogoMedia'],
      };

      if (
        adjunto['miniatura'] &&
        typeof adjunto['miniatura']['tipo'] !== 'object'
      ) {
        adjunto['miniatura']['tipo'] = {
          codigo: adjunto['miniatura']['tipo'],
        };
      }

      if (typeof adjunto['principal']['tipo'] !== 'object') {
        adjunto['principal']['tipo'] = {
          codigo: adjunto['principal']['tipo'],
        };
      }
    }

    if (mensaje.referenciaEntidadCompartida) {
      if (typeof mensaje.entidadCompartida !== 'object') {
        mensaje.entidadCompartida = {
          codigo: mensaje.entidadCompartida,
        };
      }

      if (mensaje.entidadCompartida.codigo == 'ENT_2') {
        // ENTIDAD PROYECTO
        mensaje.referenciaEntidadCompartida = await obtenerProyetoIdService.obtenerProyectoByIdPersonalizado(
          mensaje.referenciaEntidadCompartida,
        );
        let listaTraduccion = [];

        for (let traduccion of mensaje.referenciaEntidadCompartida[
          'traducciones'
        ]) {
          if (traduccion['idioma'] == idioma.codigo) {
            listaTraduccion.push(traduccion);
          }
        }

        mensaje.referenciaEntidadCompartida['traducciones'] = listaTraduccion;

        mensaje.referenciaEntidadCompartida[
          'adjuntos'
        ] = await obtenerPortadaPredeterminadaProyectoResumenService.obtenerPortadaPredeterminadaProyectoResumen(
          mensaje.referenciaEntidadCompartida['adjuntos'],
          codIdioma,
          opts,
        );
      } else if (mensaje.entidadCompartida.codigo == 'ENT_3') {
        // ENTIDAD NOTICIA
        mensaje.referenciaEntidadCompartida = await obtenerNoticiaIdService.obtenerNoticiaByIdPersonalizado(
          mensaje.referenciaEntidadCompartida,
        );

        let listaTraduccion = [];

        for (let traduccion of mensaje.referenciaEntidadCompartida[
          'traducciones'
        ]) {
          if (traduccion['idioma'] == idioma.codigo) {
            listaTraduccion.push(traduccion);
          }
        }

        mensaje.referenciaEntidadCompartida['traducciones'] = listaTraduccion;

        mensaje.referenciaEntidadCompartida[
          'adjuntos'
        ] = await obtenerPortadaService.obtenerPortada(
          mensaje.referenciaEntidadCompartida['adjuntos'],
          codIdioma,
          opts,
        );
      }
    }
  }

  formatoNotificacion(notificacion: any) {
    delete notificacion.__v;
    delete notificacion.fechaCreacion;
    delete notificacion.fechaActualizacion;
    delete notificacion.estado;

    notificacion.evento = {
      codigo: notificacion.evento,
    };
    notificacion.entidad = {
      codigo: notificacion.entidad,
    };
    notificacion.status = {
      codigo: notificacion.status,
    };
    notificacion.accion = {
      codigo: notificacion.accion,
    };
    notificacion.idEntidad = {
      _id: notificacion.idEntidad,
    };
    let arrayListaPerfiles = [];
    for (let perfilId of notificacion.listaPerfiles) {
      let objPerfilId: any = {
        _id: perfilId,
      };
      arrayListaPerfiles.push(objPerfilId);
    }
    notificacion.listaPerfiles = arrayListaPerfiles;

    return notificacion;
  }

  //Verifica que el perfil que se envia en una peticion con el perfil del token autorization sean iguales
  async verificarPerfilToken(perfilPeticion, perfilesToken) {
    let perfilVerificado = false;
    for (const userPerfil of perfilesToken) {
      console.log('perfil del token: ', userPerfil._id.toString());
      console.log('perfilPeticion.toString(): ', perfilPeticion.toString());
      if (perfilPeticion.toString() === userPerfil._id.toString()) {
        perfilVerificado = true;
        break;
      }
    }
    return perfilVerificado;
  }

  codigoAutoincrementable(getUltimoAnuncio) {
    let codigoUltAnuncio;
    //Obtiene el codigo del ultimo anuncio
    let codigoAnuncio;
    if (getUltimoAnuncio && getUltimoAnuncio[0].codigo) {
      codigoUltAnuncio = getUltimoAnuncio[0].codigo;
      //Obtencion de solo numeros en la cadena
      let regex = /(\d+)/g;
      let ultimoNumeroAnuncio = codigoUltAnuncio.match(regex);
      ultimoNumeroAnuncio = parseInt(ultimoNumeroAnuncio[0]);
      console.log('ultimoNumeroAnuncio: ', ultimoNumeroAnuncio);

      //Obtencion de solo letras de la A-Z en la cadena
      let expresion = /[A-Z]/gi;
      let letrasAnuncio = codigoUltAnuncio.match(expresion);
      console.log('letrasAnuncio: ', letrasAnuncio);

      //Si el ultimo numero del anuncio es igual al limite (500) regresar al inicio (1)
      if (ultimoNumeroAnuncio === rangoNumericoAnuncios.fin) {
        ultimoNumeroAnuncio = rangoNumericoAnuncios.inicio;
        codigoAnuncio = ultimoNumeroAnuncio.toString();

        if (letrasAnuncio) {
          let ultimaLetraAnuncio = letrasAnuncio[letrasAnuncio.length - 1];
          let indexAbecedario = this.abecedario.indexOf(ultimaLetraAnuncio);
          let busquedaAbecedario = this.abecedario[indexAbecedario];

          if (
            busquedaAbecedario === rangoAlfabeticoAnuncios.fin &&
            letrasAnuncio.length === 1
          ) {
            ultimaLetraAnuncio = this.abecedario[0].concat(this.abecedario[1]);
          } else {
            console.log('**************');
            indexAbecedario = indexAbecedario + 1;
            busquedaAbecedario = this.abecedario[indexAbecedario];
            console.log('indexAbecedario: ', indexAbecedario);
            console.log('busquedaAbecedario: ', busquedaAbecedario);
            if (letrasAnuncio) {
              for (const abcedario of letrasAnuncio) {
                codigoAnuncio = codigoAnuncio.concat(abcedario);
              }
            }
          }
        } else {
          letrasAnuncio = rangoAlfabeticoAnuncios.inicio;
          codigoAnuncio = codigoAnuncio.concat(letrasAnuncio);
        }
      } else {
        ultimoNumeroAnuncio =
          ultimoNumeroAnuncio + rangoNumericoAnuncios.inicio;
        codigoAnuncio = ultimoNumeroAnuncio.toString();
        if (letrasAnuncio) {
          for (const abcedario of letrasAnuncio) {
            codigoAnuncio = codigoAnuncio.concat(abcedario);
          }
        }
      }
    } else {
      codigoAnuncio = rangoNumericoAnuncios.inicio.toString();
    }
    return codigoAnuncio;
  }

  codigoAutoincrementableCatalogoRol(getUltimoCatRol) {
    let codigoUltAnuncio;
    //Obtiene el codigo del ultimo anuncio
    let codigoAnuncio;
    if (getUltimoCatRol && getUltimoCatRol[0].codigo) {
      codigoUltAnuncio = getUltimoCatRol[0].codigo;
      //Obtencion de solo numeros en la cadena
      let regex = /(\d+)/g;
      let ultimoNumeroAnuncio = codigoUltAnuncio.match(regex);
      ultimoNumeroAnuncio = parseInt(ultimoNumeroAnuncio[0]);
      console.log('ultimoNumeroAnuncio: ', ultimoNumeroAnuncio);

      codigoAnuncio = 'CATROL_'.concat(ultimoNumeroAnuncio);
    }
  }

  diacriticSensitiveRegex(texto: string) {
    return texto
      .replace(/a/g, '[a,á,à,ä,â]')
      .replace(/e/g, '[e,é,ë,è,ê]')
      .replace(/i/g, '[i,í,ï,ì,î]')
      .replace(/o/g, '[o,ó,ö,ò,ô]')
      .replace(/u/g, '[u,ü,ú,ù,û]')
      .replace(/A/g, '[A,Ä,Á,À,Â]')
      .replace(/E/g, '[E,Ë,É,È,Ê]')
      .replace(/I/g, '[I,Ï,Í,Ì,Î]')
      .replace(/O/g, '[O,Ö,Ó,Ò,Ô]')
      .replace(/U/g, '[U,Ü,Ú,Ù,Û]');
  }

  obtenerPrimerosCaracteres(texto: string, numero: number) {
    let arrayTexto = [];
    for (let i = 0; i <= numero; i++) {
      arrayTexto.push(texto.charAt(i));
    }
    return arrayTexto;
  }

  decimalAdjust(type, value, exp) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? +value[1] - exp : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? +value[1] + exp : exp));
  }

  round(num) {
    var m = Number((Math.abs(num) * 100).toPrecision(15));
    return Math.round(m) / 100 * Math.sign(num);
  }


  seleccionarDia(numeroDia) {
    switch (numeroDia) {

      case this.numeroDia.lunes:
        return 0;
      case this.numeroDia.martes:
        return 1;
      case this.numeroDia.miercoles:
        return 2;
      case this.numeroDia.jueves:
        return 3;
      case this.numeroDia.viernes:
        return 4;
      case this.numeroDia.sabado:
        return 5;
      case this.numeroDia.domingo:
        return 6;
      default:
        return 0;
    }
  }

}
