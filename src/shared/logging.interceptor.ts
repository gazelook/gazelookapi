import {
  CallHandler,
  ExecutionContext,
  Injectable,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { appendFile } from 'fs';
import { I18nService } from 'nestjs-i18n';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigService } from '../config/config.service';
import { ErrorsGateway } from './errors.gateway';
import { pathFile } from './path';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  apiKey = '';
  headersRequest: any;
  URL_SERVER_MANTENIMIENTO: string;
  EMAIL_MANTENIMIENTO: string;

  constructor(
    private readonly i18n: I18nService,
    private config: ConfigService,
    //private httpService: HttpService,
    private errorsGateway: ErrorsGateway,
  ) {
    this.apiKey = this.config.get('API_KEY');
    this.URL_SERVER_MANTENIMIENTO = this.config.get('URL_SERVER_MANTENIMIENTO');
    this.EMAIL_MANTENIMIENTO = this.config.get('EMAIL_MANTENIMIENTO');

    this.headersRequest = {
      'Content-Type': 'application/json',
      apiKey: this.apiKey,
    };
  }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    // get the request and log it to the console
    const ctx = context.switchToHttp();
    const request = ctx.getRequest();
    const response = ctx.getResponse();
    const statusCode = response.statusCode;
    const httpRequest = {
      headers: {
        'Content-Type': request.get('Content-Type'),
        Referer: request.get('referer'),
        'User-Agent': request.get('User-Agent'),
      },
    };
    const url = request.originalUrl;
    const browser = httpRequest.headers['User-Agent'];
    const ip = request.ip;
    const referer = request.get('referer');
    const params = JSON.stringify(request.params);
    const query = JSON.stringify(request.query);
    const method = request.method;
    const now = Date.now();
    const date = new Date();
    return next.handle().pipe(
      map(content => {
        if (
          (content.codigoEstado >= 200 && content.codigoEstado <= 207) ||
          content.codigoEstado === 401
        ) {
          Logger.log(
            `Request recibida a ${url} de IP => ${ip}, Browser => ${browser}, Referer => ${referer}, Method => ${method}, Query => ${query}, Params => ${params}, StatusCode => ${
              content.codigoEstado
            },  Response => ${JSON.stringify(
              content,
            )} - Tiempo petición => ${Date.now() - now}ms`,
          );
          appendFile(
            pathFile,
            `\n[${date}] Request recibida a ${url} de IP => ${ip}, Browser => ${browser}, Referer => ${referer}, Method => ${method}, Query => ${query}, Params => ${params}, StatusCode => ${
              content.codigoEstado
            },  Response => ${JSON.stringify(
              content,
            )} - Tiempo petición => ${Date.now() - now}ms`,
            error => {
              if (error) {
                throw error;
              }
            },
          );
        } else {
          const dataNotificar = {
            date: date,
            method: method as string,
            status: content.codigoEstado as number,
            url: url as string,
            ip: ip as string,
            browser: browser as string,
            content: content.respuesta.mensaje,
            timeRequest: Date.now() - now,
          };
          console.log(dataNotificar);
          //_______NOTIFICAR ERROR EMAIL________
          /* this.httpService.post(`${this.URL_SERVER_MANTENIMIENTO}${pathServerM.NOTIFICAR_ERROR}`, dataNotificar, { headers: this.headersRequest }).subscribe(data => {
              console.log("Se ha notificado el error al email: ");
            }, error => {
              console.log("No se ha podido notificar el error por email");
            }
            ); */
          //_______NOTIFICAR ERROR POR SOCKETS________
          this.errorsGateway.notificarError(dataNotificar);

          Logger.error(
            `[EXCEPTION] [${
              content.codigoEstado
            }] [${method}] Request recibida a ${url}, IP => ${ip}, Browser => ${browser}, ${JSON.stringify(
              content,
            )} - Tiempo petición => ${Date.now() - now}ms`,
          );
          appendFile(
            pathFile,
            `\n[${date}] [${'EXCEPTION'}] [${
              content.codigoEstado
            }] [${method}] Request recibida a ${url}, IP => ${ip}, Browser => ${browser}, Referer => ${referer}, Method => ${method}, Query => ${query}, Params => ${params}, StatusCode => ${
              content.codigoEstado
            }, Response => ${JSON.stringify(
              content,
            )} - Tiempo petición => ${Date.now() - now}ms`,
            { mode: 0o666, flag: 'a+' },
            error => {
              if (error) {
                throw error;
              }
            },
          );
        }
        return content;
      }),
    );
  }
}
