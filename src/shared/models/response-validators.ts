export interface ResponseValidators {
  statusCode?: number;
  message?: any;
  error?: string;
}
