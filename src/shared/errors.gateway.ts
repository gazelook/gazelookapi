import {
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { AuthService } from '../auth/auth.service';
import { Server, Socket } from 'socket.io';
import { Logger } from '@nestjs/common';
import { CrearLogSistemaService } from '../entidades/log_sistema/casos_de_uso/crear-log-sistema.service';
import { LogSistemaDto } from '../entidades/log_sistema/dtos/log.dto';

@WebSocketGateway()
export class ErrorsGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private authService: AuthService,
    private logSistema: CrearLogSistemaService,
  ) {}

  @WebSocketServer() server: Server;
  private logger: Logger = new Logger('ErrorsGateway');

  /* @SubscribeMessage('msgToServer')
  handleMessage(client: Socket, payload: string): void {
    this.server.emit('msgToClient', payload);
  } */

  afterInit(server: Server) {
    this.logger.log('Init');
    console.log('Sockect de Notificacion de Errores iniciado');
  }
  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  handleConnection(client: Socket, ...args: any[]) {
    this.logger.log(`Client connected: ${client.id}`);
  }

  @SubscribeMessage('mensaje')
  handleMessage(client: any, payload: any) {
    console.log('recibido: ', payload);
  }

  @SubscribeMessage('errors')
  async notificarError(@MessageBody() data: LogSistemaDto) {
    console.log('Se ha notificado el error por sockets');
    this.server.sockets.emit('notificacionError', data);
    await this.logSistema.crearLogSistema(data);
  }
}
