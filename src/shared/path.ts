import { resolve } from 'path';
//nombre de arquivo logger autogenerado
var log = '.log';
let date = new Date();
let fecha =
  date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getDate();

//path para crear archivo log desde shared
const dirPath = resolve(__dirname, `../../logs/development-`);
export const pathFile = dirPath.concat(fecha).concat(log);

//path para crear archivo log desde main
const dirPathMain = resolve(__dirname, `../../logs/development-`);
export const pathFileMain = dirPathMain.concat(fecha).concat(log);
