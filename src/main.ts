import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { Logger } from '@nestjs/common';
import { pathFileMain } from './shared/path';
import { appendFile, existsSync } from 'fs';
import { NestExpressApplication } from '@nestjs/platform-express';
import { resolve } from 'path';
import { ValidationPipe422 } from './shared/myvalidator';
const ffmpeg = require('fluent-ffmpeg');

import { ValidationExceptionFilter } from './shared/filters/validator-exception.filter';
import * as basicAuth from 'express-basic-auth';

try {
  async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule, {
      cors: true,
    });

    app.useStaticAssets(resolve('./src/public'));
    app.setBaseViewsDir(resolve('./src/views'));
    app.setViewEngine('hbs');

    const dirffmpegWin = resolve(__dirname, `C:/ffmpeg/bin/ffmpeg.exe`);
    const dirffmpegLin = resolve(__dirname, `/usr/bin/ffmpeg`);
    const dirffmpegMac = resolve(__dirname, `/usr/local/bin/ffmpeg`);
    if (existsSync(dirffmpegLin)) {
      ffmpeg.setFfprobePath('/usr/bin');
      ffmpeg.setFfmpegPath(dirffmpegLin);
    }
    if (existsSync(dirffmpegMac)) {
      ffmpeg.setFfprobePath('/usr/bin');
      ffmpeg.setFfmpegPath(dirffmpegMac);
    }
    if (existsSync(dirffmpegWin)) {
      ffmpeg.setFfprobePath('C:/ffmpeg/bin');
      ffmpeg.setFfmpegPath(dirffmpegWin);
    }

    app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header(
        'Access-Control-Allow-Headers',
        'apiKey, idioma, Authorization',
      );
      res.header(
        'Access-Control-Expose-Headers',
        'totalDatos, totalPaginas, proximaPagina, anteriorPagina',
      );
      res.header(
        'Access-Control-Allow-Methods',
        'GET, POST, OPTIONS, PUT, DELETE, PATCH',
      );
      res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE, PATCH');
      next();
    });
    app.enableCors({
      // origin: true,
      // methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
      // credentials: true,
    });

    const port = process.env.PORT;
    const date = new Date();

    // app.useGlobalPipes(new ValidationPipe());
    app.useGlobalPipes(new ValidationPipe422());
    app.useGlobalFilters(new ValidationExceptionFilter());

    appendFile(
      pathFileMain,
      `\n[${date}] Server running on http://localhost:${port}`,
      error => {
        if (error) {
          throw error;
        }
      },
    );

    // Swagger
    // solo para pruebas en produccion no se va a ver la documentacion
    const SWAGGER_ENVS = ['qa', 'dev', 'staging'];
    if (SWAGGER_ENVS.includes(process.env.NODE_ENV)) {
      app.use(
        ['/docs', '/docs-json'],
        basicAuth({
          challenge: true,
          users: {
            [process.env.SWAGGER_USER]: process.env.SWAGGER_PASSWORD,
          },
        }),
      );

      const config = new DocumentBuilder()
        .setTitle('GAZELOOK API Docs')
        .setDescription('Main API Documentation')
        .setVersion('1.0')
        .addBearerAuth()
        .build();

      const document = SwaggerModule.createDocument(app, config);
      SwaggerModule.setup('docs', app, document);
    }

    await app.listen(port);
    Logger.log(`Server running on http://localhost:${port}`, 'Bootstrap');
  }
  bootstrap();
} catch (error) {
  throw error;
}
