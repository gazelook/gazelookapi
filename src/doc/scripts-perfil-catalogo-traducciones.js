//TRADUCCIONES

//5.
//catalogo traduccion tipo perfil
// traduccion espaniol ES
db.traduccion_catalogo_tipo_perfil.insert([
    {
        nombre: 'CLÁSICO',
        descripcion:
            'EN ESTE PERFIL, ELEGIRÁ LAS FOTOS QUE DESEE DE USTED MISMO, SU NOMBRE DE CONTACTO, NOMBRE E INFORMACIÓN QUE CORRESPONDA.',
        idioma: 'IDI_1',
        original: true,
        codigoTipoPerfil: 'TIPERFIL_1',
        fechaCreacion: new Date(),
        fechaActualizacion: null,
    },
    {
        nombre: 'LÚDICO',
        descripcion:
            'UN PERFIL ENTRETENIDO E INTRIGANTE. SUBA FOTOS MIRANDO A LA CÁMARA DE CUANDO ERA UN BEBÉ O UN NIÑO. OPCIONALMENTE, PUEDE SUBIR MIRADAS INOCENTES DE SI MISMO COMO ADULTO. CADA VEZ QUE ABRA GAZELOOK, APARECERÁ AL AZAR UNA FOTO DE LAS ELEGIDAS; LA VIDA NOS CAMBIA Y LA INOCENCIA ES REFRESCANTE EN CUALQUIER MOMENTO. INVENTE SU NOMBRE DE CONTACTO Y PARTICIPE CON SU PERFIL LÚDICO',
        idioma: 'IDI_1',
        original: true,
        codigoTipoPerfil: 'TIPERFIL_2',
        fechaCreacion: new Date(),
        fechaActualizacion: null,
    },
    {
        nombre: 'SUSTITUTO',
        descripcion:
            '1- LE DA LA POSIBILIDAD DE CREAR UN PERFIL DE ALGUIEN A QUIEN DESEE SUSTITUIR. POR EJEMPLO, EL DE ALGÚN INFANTE O EL DE UN SER QUERIDO QUE NO PUEDE HACERLO O QUE LO HA DEJADO. 2- ¡TAMBIÉN PUEDE SER UN PERFIL CON ALGÚN PERSONAJE INVENTADO Y UN BUEN NOMBRE DE CONTACTO!',
        idioma: 'IDI_1',
        original: true,
        codigoTipoPerfil: 'TIPERFIL_3',
        fechaCreacion: new Date(),
        fechaActualizacion: null,
    },
    {
        nombre: 'GRUPO',
        descripcion:
            'ESTE PERFIL ES ÚTIL PARA ASOCIACIONES, GRUPOS, FAMILIAS Y AMIGOS QUE DESEEN TENER UN PROYECTO EN COMÚN. PUEDES SUBIR TUS FOTOS Y UN NOMBRE DE CONTACTO REPRESENTATIVO DE TODO EL GRUPO.',
        idioma: 'IDI_1',
        original: true,
        codigoTipoPerfil: 'TIPERFIL_4',
        fechaCreacion: new Date(),
        fechaActualizacion: null,
    },
]);


//catalogo traduccion tipo perfil
// Ingles EN
db.traduccion_catalogo_tipo_perfil.insert([
    {
        nombre: "CLASSIC",
        descripcion: "IN THIS PROFILE, YOU WILL CHOOSE THE PHOTOS YOU WANT FROM YOURSELF, YOUR NAME AND INFORMATION THAT CORRESPONDS.",
        idioma: "IDI_2",
        original: true,
        codigoTipoPerfil: "TIPERFIL_1",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "PLAYFUL",
        descripcion: "A FUN AND INTRIGUING PROFILE. UPLOAD PHOTOS OF YOU LOOKING AT THE CAMERA FROM THE TIME WHEN YOU WERE A BABY OR A KID. OPTIONALLY, YOU CAN UPLOAD INNOCENT LOOKS OF YOURSELF AS AN ADULT. EVERY TIME YOU OPEN GAZELOOK, A RANDOM PHOTO OF THE CHOSEN ONES WILL APPEAR; LIFE CHANGES US AND INNOCENCE IS REFRESHING AT ANY MOMENT. MAKE UP YOUR CONTACT NAME AND PARTICIPATE WITH YOUR PLAYFUL PROFILE.",
        idioma: "IDI_2",
        original: true,
        codigoTipoPerfil: "TIPERFIL_2",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "SUBSTITUTE",
        descripcion: '1- IT GIVES YOU THE OPTION OF CREATING THE PROFILE OF SOMEONE YOU WANT TO SUBSTITUTE, FOR EXAMPLE, THE PROFILE OF A CHILD OR FROM A LOVED ONE WHO CANNOT DO IT OR WHO HAS LEFT YOU. 2- IT CAN ALSO BE A PROFILE OF SOME INVENTED CHARACTER AND A GREAT CONTACT NAME!',
        idioma: "IDI_2",
        original: true,
        codigoTipoPerfil: "TIPERFIL_3",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "GROUP",
        descripcion: "THIS PROFILE IS USEFUL FOR ASSOCIATIONS, GROUPS, FAMILIES AND FRIENDS WHO WISH TO HAVE A PROJECT IN COMMON. YOU CAN UPLOAD YOUR PHOTOS AND ONE CONTACT NAME REPRESENTATIVE OF THE WHOLE GROUP.",
        idioma: "IDI_2",
        original: true,
        codigoTipoPerfil: "TIPERFIL_4",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    }
])

// Frances FR
db.traduccion_catalogo_tipo_perfil.insert([
    {
        nombre: "CLASSIQUE",
        descripcion: "DANS CE PROFIL, VOUS CHOISIREZ VOS POPRES PHOTOS, VOTRE NOM DE CONTACT, NOM ET LES INFORMATIONS CORRESPONDANTES.",
        idioma: "IDI_5",
        original: true,
        codigoTipoPerfil: "TIPERFIL_1",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "LUDIQUE",
        descripcion: "UN PROFIL DIVERTISSANT ET INTRIGUANT. TELECHARGEZ DES PHOTOS DE VOTRE ENFANCE EN REGARDANT L'APPAREIL PHOTO. VOUS POUVEZ AUSSI TÉLÉCHARGER DES REGARDS INNOCENTS DE VOUS-MEME EN TANT QU'ADULTE. CHAQUE FOIS QUE VOUS OUVRIREZ GAZELOOK, UNE PHOTO ALEATOIRE DES CHOISIES APPARAITRA ; LA VIE NOUS CHANGE ET L'INNOCENCE EST RAFRAICHISSANTE A TOUT MOMENT. INVENTEZ VOTRE NOM DE CONTACT ET PARTICIPEZ AVEC VOTRE PROFIL LUDIQUE.",
        idioma: "IDI_5",
        original: true,
        codigoTipoPerfil: "TIPERFIL_2",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "DE REMPLACEMENT",
        descripcion: "1- VOUS DONNE LA POSSIBILITÉ DE CRÉER UN PROFIL DE QUELQU'UN QUE VOUS SOUHAITEZ REMPLACER. PAR EXEMPLE, CELUI D´UN NOURRISSON OU D'UN PROCHE QUI NE PEUT PAS LE FAIRE OU QUI VOUS A QUITTÉ. 2- IL PEUT AUSSI S'AGIR D'UN PROFIL AVEC UN PERSONNAGE INVENTÉ ET UN NOM DE CONTACT SYMPA !",
        idioma: "IDI_5",
        original: true,
        codigoTipoPerfil: "TIPERFIL_3",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "DE GROUPE",
        descripcion: "CE PROFIL EST UTILE AUX ASSOCIATIONS, GROUPES, FAMILLES ET AMIS QUI SOUHAITENT AVOIR UN PROJET EN COMMUN. VOUS POUVEZ TÉLÉCHARGER VOS PHOTOS ET UN NOM DE CONTACT REPRESENTATIF DE L'ENSEMBLE DU GROUPE.",
        idioma: "IDI_5",
        original: true,
        codigoTipoPerfil: "TIPERFIL_4",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    }
])

// Aleman AN
db.traduccion_catalogo_tipo_perfil.insert([
    {
        nombre: "KLASSISCH",
        descripcion: "IN DIESEM PROFIL WÄHLEN SIE DIE FOTOS, DIE SIE VON SICH SELBST WOLLEN, IHREN NAMEN UND DIE GELTENDEN INFORMATIONEN.",
        idioma: "IDI_3",
        original: true,
        codigoTipoPerfil: "TIPERFIL_1",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "SPIELERISCH",
        descripcion: "EIN UNTERHALTENDES UND INTRIGIERENDES PROFIL. Laden Sie Fotos hoch, die die Kamera betrachten, wenn es sich um ein Baby oder ein Kind handelt. OPTIONAL KÖNNEN SIE UNSCHULDIGE AUSSEHEN ALS ERWACHSENE HOCHLADEN (JEDES MAL, WENN SIE GAZELOOK ÖFFNEN, WIRD ZUFÄLLIG EIN FOTO DER AUSGEWÄHLTEN ERSCHEINEN). Wählen Sie dann einen Spitznamen und haben Sie Spaß daran, die spielerischen Informationen anzubringen, die Sie zufrieden stellen.",
        idioma: "IDI_3",
        original: true,
        codigoTipoPerfil: "TIPERFIL_2",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "ERSATZ",
        descripcion: "1- GIBT IHNEN DIE MÖGLICHKEIT, EIN PROFIL VON JEMANDEM ZU ERSTELLEN, DEN SIE ERSETZEN MÖCHTEN. Zum Beispiel das von einigen Säuglingen oder von einem geliebten Menschen, der es nicht kann oder der es verlassen hat. 2- ES KANN AUCH EIN PROFIL MIT EINEM ERFINDETEN CHARAKTER UND EINEM GROSSEN NAMEN SEIN!",
        idioma: "IDI_3",
        original: true,
        codigoTipoPerfil: "TIPERFIL_3",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "GRUPPE",
        descripcion: "DIESES PROFIL IST NÜTZLICH FÜR VERBÄNDE, GRUPPEN, FAMILIEN UND FREUNDE, DIE EIN GEMEINSAMES PROJEKT HABEN möchten. MEHRERE FOTOS UND EIN EINZELNER NAME ODER EIN NAME FÜR DIE GANZE GRUPPE WIRD AKZEPTIERT.",
        idioma: "IDI_3",
        original: true,
        codigoTipoPerfil: "TIPERFIL_4",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    }
])

// Portugues PT
db.traduccion_catalogo_tipo_perfil.insert([
    {
        nombre: "CLÁSSICO",
        descripcion: "NESTE PERFIL, VOCÊ ESCOLHERÁ AS FOTOS DE VOCÊ, SEU NOME E AS INFORMAÇÕES APLICÁVEIS.",
        idioma: "IDI_4",
        original: true,
        codigoTipoPerfil: "TIPERFIL_1",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "BRINCALHÃO",
        descripcion: "UM PERFIL DIVERTIDO E INTRIGANTE. Carregar fotos olhando para a câmera quando era um bebê ou uma criança. Opcionalmente, você pode fazer upload de olhares inocentes de si mesmo como um adulto (toda vez que você abre o GAZELOOK, UMA FOTO DO ESCOLHIDO APARECE COM ALEGRIA). Em seguida, escolha um apelido e divirta-se anexando as informações divertidas que o agradam.",
        idioma: "IDI_4",
        original: true,
        codigoTipoPerfil: "TIPERFIL_2",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "SUBSTITUTO",
        descripcion: "1- DÁ A POSSIBILIDADE DE CRIAR UM PERFIL DE ALGUÉM QUE VOCÊ QUER SUBSTITUIR. Por exemplo, aquele de um bebê ou um ente querido que não pode fazê-lo ou que o deixou. 2- PODE TAMBÉM SER UM PERFIL COM ALGUNS PERSONAGENS INVENTADOS E UM GRANDE NOME!",
        idioma: "IDI_4",
        original: true,
        codigoTipoPerfil: "TIPERFIL_3",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "GRUPO",
        descripcion: "ESTE PERFIL É ÚTIL PARA ASSOCIAÇÕES, GRUPOS, FAMÍLIAS E AMIGOS QUE DESEJAM TER UM PROJETO COMUM. VÁRIAS FOTOS E UM NOME OU NOME PARA O GRUPO INTEIRO É ACEITO.",
        idioma: "IDI_4",
        original: true,
        codigoTipoPerfil: "TIPERFIL_4",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    }
])


// Italiano IT
db.traduccion_catalogo_tipo_perfil.insert([
    {
        nombre: "CLASSICO",
        descripcion: "IN QUESTO PROFILO, SCEGLIerai LE FOTO CHE DESIDERI DI TE, IL TUO NOME E LE INFORMAZIONI CHE SI APPLICANO.",
        idioma: "IDI_6",
        original: true,
        codigoTipoPerfil: "TIPERFIL_1",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "GIOCOSO",
        descripcion: "UN PROFILO DIVERTENTE E INTRIGANTE. CARICA LE FOTO CHE GUARDANO LA FOTOCAMERA QUANDO ERA UN BAMBINO O UN BAMBINO. OPZIONALMENTE, PUOI CARICARE TUO LO SGUARDO INNOCENTE COME ADULTO (OGNI VOLTA CHE APRIRE GAZELOOK, UNA FOTO DELLA SCELTA APPARIRÀ A CASO). Quindi scegli un nickname e divertiti ad attaccare le informazioni divertenti che ti soddisfano.",
        idioma: "IDI_6",
        original: true,
        codigoTipoPerfil: "TIPERFIL_2",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "SOSTITUIRE",
        descripcion: "1- TI DÀ LA POSSIBILITÀ DI CREARE UN PROFILO DI QUALUNQUE CHE VUOI SOSTITUIRE. ESEMPIO, QUELLO DI ALCUNI BAMBINI O QUELLO DI UN AMATO CHE NON PUO 'FARLO O CHE LO HA LASCIATO. 2- PU CAN ESSERE ANCHE UN PROFILO CON ALCUNI PERSONAGGI INVENTATI E UN GRANDE NOME!",
        idioma: "IDI_6",
        original: true,
        codigoTipoPerfil: "TIPERFIL_3",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    },
    {
        nombre: "GRUPPO",
        descripcion: "QUESTO PROFILO È UTILE PER ASSOCIAZIONI, GRUPPI, FAMIGLIE E ​​AMICI CHE DESIDERANO AVERE UN PROGETTO COMUNE. Sono accettate più foto e un singolo nome o nome per l'intero gruppo.",
        idioma: "IDI_6",
        original: true,
        codigoTipoPerfil: "TIPERFIL_4",
        fechaCreacion: new Date(),
        fechaActualizacion: null
    }
])