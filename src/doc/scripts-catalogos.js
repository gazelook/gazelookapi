//1.
//catalogo entidades

db.catalogo_entidades.insert([
  {
    codigo: 'ENT_1',
    nombre: 'usuarios',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_2',
    nombre: 'proyectos',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_3',
    nombre: 'noticias',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_4',
    nombre: 'pensamientos',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_5',
    nombre: 'comunicacion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_6',
    nombre: 'transaccion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_7',
    nombre: 'compras',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_8',
    nombre: 'perfiles',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_9',
    nombre: 'contactos',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_10',
    nombre: 'comentarios',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_11',
    nombre: 'busquedas',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_12',
    nombre: 'media',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_13',
    nombre: 'dispositivo',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_14',
    nombre: 'email',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_15',
    nombre: 'metodoPago',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_16',
    nombre: 'tipoMoneda',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_17',
    nombre: 'origen',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_18',
    nombre: 'beneficiario',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_19',
    nombre: 'archivo',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_20',
    nombre: 'telefono',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_21',
    nombre: 'direccion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_22',
    nombre: 'asociacion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_23',
    nombre: 'participanteAsociacion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_24',
    nombre: 'conversacion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_25',
    nombre: 'mensaje',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_26',
    nombre: 'configuracion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // {
  //   codigo: 'ENT_30',
  //   nombre: 'proyecto',
  //   fechaCreacion: new Date(),
  //   fechaActualizacion: new Date(),
  // },
  {
    codigo: 'ENT_31',
    nombre: 'votoProyecto',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_32',
    nombre: 'participanteProyecto',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // {
  //   codigo: 'ENT_33',
  //   nombre: 'comentario',
  //   fechaCreacion: new Date(),
  //   fechaActualizacion: new Date(),
  // },
  {
    codigo: 'ENT_34',
    nombre: 'estrategia',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_35',
    nombre: 'configuracionEvento',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_36',
    nombre: 'evento',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_37',
    nombre: 'formulaEvento',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_38',
    nombre: 'suscripcion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_39',
    nombre: 'catalogoSuscripcion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_40',
    nombre: 'catalogo_tipo_archivo',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // catalogo tipo media
  {
    codigo: 'ENT_41',
    nombre: 'catalogoTipoMedia',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_42',
    nombre: 'album',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_43',
    nombre: 'traduccionMedia',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_44',
    nombre: 'catalogoTipoEmail',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_45',
    nombre: 'catalogoPais',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_46',
    nombre: 'catalogoLocalidad',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  {
    codigo: 'ENT_47',
    nombre: 'traduccionNoticia',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  {
    codigo: 'ENT_48',
    nombre: 'catalogoMedia',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogo archivo default --------
  {
    codigo: 'ENT_49',
    nombre: 'catalogoArchivoDefault',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_50',
    nombre: 'catalogoTipoPerfil',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_51',
    nombre: 'votoNoticia',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'ENT_52',
    nombre: 'traduccionVotoNoticia',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogo tipo proyecto --------
  {
    codigo: 'ENT_53',
    nombre: 'catalogoTipoProyecto',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad traduccion catalogo tipo proyecto --------
  {
    codigo: 'ENT_54',
    nombre: 'traduccionCatalogoTipoProyecto',
    fechaCreacion: new Date(),

    fechaActualizacion: new Date(),
  },
  // ------- entidad traduccionProyecto --------
  {
    codigo: 'ENT_55',
    nombre: 'traduccionProyecto',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogoAlbum --------
  {
    codigo: 'ENT_56',
    nombre: 'catalogoAlbum',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogoIdiomas --------
  {
    codigo: 'ENT_57',
    nombre: 'catalogoIdiomas',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad traduccionPensamientos --------
  {
    codigo: 'ENT_58',
    nombre: 'traduccionPensamientos',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogoTipoAsociacion--------
  {
    codigo: 'ENT_59',
    nombre: 'catalogoTipoAsociacion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad traduccion mensajes ------
  {
    codigo: 'ENT_60',
    nombre: 'traduccion_mensajes',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogomensajes ------
  {
    codigo: 'ENT_61',
    nombre: 'catalogoMensaje',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad catalogoTipoComentario--------
  {
    codigo: 'ENT_62',
    nombre: 'catalogoTipoComentario',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad traducción comentario--------
  {
    codigo: 'ENT_63',
    nombre: 'traduccionComentario',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad traduccion voto proyecto--------
  {
    codigo: 'ENT_64',
    nombre: 'traduccionVotoProyecto',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad balance--------
  {
    codigo: 'ENT_65',
    nombre: 'balance',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad rolSistema--------
  {
    codigo: 'ENT_66',
    nombre: 'rolSistema',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad catalogo tipo rol--------
  {
    codigo: 'ENT_67',
    nombre: 'catalogoTipoRol',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogo configuracion --------
  {
    codigo: 'ENT_68',
    nombre: 'catalogoConfiguracion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogo tipo voto --------
  {
    codigo: 'ENT_69',
    nombre: 'catalogoTipoVoto',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad catalogo rol--------
  {
    codigo: 'ENT_70',
    nombre: 'catalogoRol',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad proyectoEjecucion--------
  {
    codigo: 'ENT_71',
    nombre: 'proyectoEjecucion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad traduccionproyectoEjecucion--------
  {
    codigo: 'ENT_72',
    nombre: 'traduccionProyectoEjecución',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad catalogoColores
  {
    codigo: 'ENT_73',
    nombre: 'catalogoColores',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogoTipoColores
  {
    codigo: 'ENT_74',
    nombre: 'catalogoTipoColores',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad catalogoEvento
  {
    codigo: 'ENT_75',
    nombre: 'catalogoEvento',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad rolEntidad
  {
    codigo: 'ENT_76',
    nombre: 'rolEntidad',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad configuracion estilo
  {
    codigo: 'ENT_77',
    nombre: 'configuracionEstilo',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad configuración personalizada
  {
    codigo: 'ENT_78',
    nombre: 'configuracionPersonalizada',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad configuración predeterminada
  {
    codigo: 'ENT_79',
    nombre: 'configuracionPredeterminada',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad estilo
  {
    codigo: 'ENT_80',
    nombre: 'estilos',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad catalogo estilo
  {
    codigo: 'ENT_81',
    nombre: 'catalogoEstilos',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad CatalogoPaticipanteConfiguracion
  {
    codigo: 'ENT_82',
    nombre: 'CatalogoPaticipanteConfiguracion',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad configuracionParticipante
  {
    codigo: 'ENT_83',
    nombre: 'configuracionParticipante',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad Tokens
  {
    codigo: 'ENT_84',
    nombre: 'tokenUsuario',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  // ------- entidad documentosLegales
  {
    codigo: 'ENT_85',
    nombre: 'documentosLegales',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },

  // ------- entidad catalogoOrigenDocumento
  {
    codigo: 'ENT_86',
    nombre: 'catalogoOrigenDocumento',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
]);

//2.
//catalogo acciones

db.catalogo_acciones.insert([
  {
    _id: ObjectId('5f48435d6b14c316f5e9cd48'),
    codigo: 'ACC_1',
    nombre: 'ver',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    _id: ObjectId('5f48435d6b14c316f5e9cd49'),
    codigo: 'ACC_2',
    nombre: 'modificar',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    _id: ObjectId('5f48435d6b14c316f5e9cd4a'),
    codigo: 'ACC_3',
    nombre: 'eliminar',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    _id: ObjectId('5f48435d6b14c316f5e9cd4b'),
    codigo: 'ACC_4',
    nombre: 'crear',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//3.
//catalogo estados

db.catalogo_estados.insert([
  //-------  estado usuario --------
  {
    codigo: 'EST_1',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_1',
  },
  {
    codigo: 'EST_2',
    nombre: 'inactivaPago',
    descripcion: 'Falta de pago de la subscripción',
    entidad: 'ENT_1',
  },
  {
    codigo: 'EST_3',
    nombre: 'bloqueado',
    descripcion: 'Bloqueado por el adminstrador del sistema',
    entidad: 'ENT_1',
  },
  {
    codigo: 'EST_4',
    nombre: 'bloqueoContrasena',
    descripcion: 'bloqueado por olvido de contraseña',
    entidad: 'ENT_1',
  },
  {
    codigo: 'EST_5',
    nombre: 'activaNoVerificado',
    descripcion: 'activa a la espera de que verifique su email',
    entidad: 'ENT_1',
  },
  {
    codigo: 'EST_6',
    nombre: 'eliminado',
    descripcion: 'Cuando el usuario esta desactivado por el administrador',
    entidad: 'ENT_1',
  },
  {
    codigo: 'EST_7',
    nombre: 'bloqueadoResposable',
    descripcion: 'Cuando la cuenta del usuario es bloqueado por el responsable',
    entidad: 'ENT_1',
  },
  // ------estado transaccion------
  {
    codigo: 'EST_8',
    nombre: 'activa',
    descripcion: 'Activa',
    entidad: 'ENT_6',
  },
  {
    codigo: 'EST_9',
    nombre: 'eliminada',
    descripcion: 'Cuando la atransaccion fue desactivada por el administrador',
    entidad: 'ENT_6',
  },
  {
    codigo: 'EST_10',
    nombre: 'cancelada',
    descripcion: 'La transaccion es cancelada por el usuario',
    entidad: 'ENT_6',
  },
  {
    codigo: 'EST_11',
    nombre: 'rechazada',
    descripcion: 'La transaccion fue rechazada por el usuario',
    entidad: 'ENT_6',
  },
  {
    codigo: 'EST_80',
    nombre: 'pendiente',
    descripcion: 'Cuando aún no se a hecho efecto el pago',
    entidad: 'ENT_6',
  },

  {
    codigo: 'EST_11',
    nombre: 'rechazada',
    descripcion: 'La transaccion fue rechazada por el usuario',
    entidad: 'ENT_6',
  },

  // -------- estado tipo_moneda --------
  {
    codigo: 'EST_12',
    nombre: 'activa',
    descripcion: 'Activa',
    entidad: 'ENT_16',
  },
  {
    codigo: 'EST_13',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo tipo moneda esta desactivado por el administrador',
    entidad: 'ENT_16',
  },

  // ------- estado metodo de pago --------
  {
    codigo: 'EST_14',
    nombre: 'activa',
    descripcion: 'Catálogo activa metodo de pago activo',
    entidad: 'ENT_15',
  },
  {
    codigo: 'EST_15',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo METODO PAGO esta desactivado por el administrador',
    entidad: 'ENT_15',
  },

  // ------- estado beneficiario --------
  {
    codigo: 'EST_16',
    nombre: 'activa',
    descripcion: 'Activa',
    entidad: 'ENT_18',
  },
  {
    codigo: 'EST_18',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo tipo beneficiario esta desactivado por el administrador',
    entidad: 'ENT_18',
  },
  // ------- estado perfil --------
  {
    codigo: 'EST_19',
    nombre: 'hibernado',
    descripcion: 'Cuando el usuario inabilita su perfil',
    entidad: 'ENT_8',
  },
  {
    codigo: 'EST_20',
    nombre: 'activa',
    descripcion: 'Cuando el perfil esta bien',
    entidad: 'ENT_8',
  },

  // ------- estado pensamiento --------
  {
    codigo: 'EST_22',
    nombre: 'activa',
    descripcion: 'Pensamiento se visualiza correctamente',
    entidad: 'ENT_4',
  },
  {
    codigo: 'EST_23',
    nombre: 'eliminado',
    descripcion: 'Cuando el pensamiento esta desactivado por el administrador',
    entidad: 'ENT_4',
  },
  // ------- estado Media --------
  {
    codigo: 'EST_24',
    nombre: 'activa',
    descripcion: 'La media se visualiza correctamente',
    entidad: 'ENT_12',
  },
  {
    codigo: 'EST_25',
    nombre: 'eliminado',
    descripcion: 'La media esta desactivada por el administrador ',
    entidad: 'ENT_12',
  },
  {
    codigo: 'EST_26',
    nombre: 'sinAsignar',
    descripcion: 'Media subido correctamente pero sin asignar',
    entidad: 'ENT_12',
  },

  // ------- estado archivo --------
  {
    codigo: 'EST_27',
    nombre: 'activa',
    descripcion: 'el archivo se visualiza correctamente',
    entidad: 'ENT_19',
  },
  {
    codigo: 'EST_28',
    nombre: 'eliminado',
    descripcion: 'El archivo esta desactivada por el administrador ',
    entidad: 'ENT_19',
  },

  // ------- estado telefono --------
  {
    codigo: 'EST_29',
    nombre: 'activa',
    descripcion: 'telefono activa',
    entidad: 'ENT_20',
  },
  {
    codigo: 'EST_30',
    nombre: 'eliminado',
    descripcion: 'telefono desactivada por el administrador ',
    entidad: 'ENT_20',
  },
  {
    codigo: 'EST_31',
    nombre: 'predeterminado',
    descripcion: 'Cuando el item es que aparecera por defecto ',
    entidad: 'ENT_20',
  },

  // ------- estado direccion --------
  {
    codigo: 'EST_32',
    nombre: 'activa',
    descripcion: 'direccion activa',
    entidad: 'ENT_21',
  },
  {
    codigo: 'EST_33',
    nombre: 'eliminado',
    descripcion: 'Cuando el direccion esta desactivado por el administrador',
    entidad: 'ENT_21',
  },
  {
    codigo: 'EST_34',
    nombre: 'predeterminado',
    descripcion: 'Cuando el item es que aparecera por defecto ',
    entidad: 'ENT_21',
  },

  // ------- estado asociacion --------
  {
    codigo: 'EST_35',
    nombre: 'activa',
    descripcion: 'direccion activa',
    entidad: 'ENT_22',
  },
  {
    codigo: 'EST_36',
    nombre: 'eliminado',
    descripcion: 'Cuando la asociacion esta desactivado por el administrador',
    entidad: 'ENT_22',
  },

  // ------- estado suscripcion --------
  {
    codigo: 'EST_37',
    nombre: 'activa',
    descripcion: 'suscripcion activa',
    entidad: 'ENT_38',
  },
  {
    codigo: 'EST_38',
    nombre: 'eliminada',
    descripcion: 'Cuando la suscripcion esta desactivado por el administrador',
    entidad: 'ENT_38',
  },
  {
    codigo: 'EST_39',
    nombre: 'cancelada',
    descripcion: 'usuario cancelo suscripcion',
    entidad: 'ENT_38',
  },
  {
    codigo: 'EST_40',
    nombre: 'noRenovada',
    descripcion: 'sistema cancelo la suscripcion',
    entidad: 'ENT_38',
  },
  {
    codigo: 'EST_81',
    nombre: 'pendiente',
    descripcion: 'Cuando aún no se a hecho efecto el pago',
    entidad: 'ENT_38',
  },
  // ------- estado catalogo suscripcion --------
  {
    codigo: 'EST_41',
    nombre: 'activa',
    descripcion: 'catalogo uscripcion activa',
    entidad: 'ENT_39',
  },
  {
    codigo: 'EST_42',
    nombre: 'eliminado',
    descripcion:
      'Cuando la catalogo suscripcion esta desactivado por el administrador',
    entidad: 'ENT_39',
  },
  // ------- estado catalogo email --------
  {
    codigo: 'EST_43',
    nombre: 'activa',
    descripcion: 'link email activo',
    entidad: 'ENT_14',
  },
  {
    codigo: 'EST_44',
    nombre: 'bloqueado',
    descripcion: 'link email bloqueado',
    entidad: 'ENT_14',
  },

  // ------- estado catalogo tipo archivo --------
  {
    codigo: 'EST_43',
    nombre: 'activa',
    descripcion: 'tipo archivo activa',
    entidad: 'ENT_40',
  },
  {
    codigo: 'EST_44',
    nombre: 'eliminado',
    descripcion: '',
    entidad: 'ENT_40',
  },
  // ------- estado dispositivo --------
  {
    codigo: 'EST_45',
    nombre: 'activa',
    descripcion: 'dispositivo activo',
    entidad: 'ENT_13',
  },
  {
    codigo: 'EST_46',
    nombre: 'eliminado',
    descripcion: 'Cuando el disposivo esta desactivado por el administrador',
    entidad: 'ENT_13',
  },

  // ------- estado dispositivo --------
  {
    codigo: 'EST_45',
    nombre: 'activa',
    descripcion: 'dispositivo activo',
    entidad: 'ENT_13',
  },
  {
    codigo: 'EST_46',
    nombre: 'eliminado',
    descripcion: 'Cuando el disposivo esta desactivado por el administrador',
    entidad: 'ENT_13',
  },
  {
    codigo: 'EST_47',
    nombre: 'activa',
    descripcion: 'Album activo',
    entidad: 'ENT_42',
  },
  {
    codigo: 'EST_48',
    nombre: 'eliminado',
    descripcion: 'Cuando el usuario elimina su album',
    entidad: 'ENT_42',
  },
  {
    codigo: 'EST_49',
    nombre: 'activa',
    descripcion: 'Traduccion de la descripcion del archivo activo',
    entidad: 'ENT_43',
  },
  {
    codigo: 'EST_50',
    nombre: 'eliminado',
    descripcion: 'Traduccion de la descripcion del archivo eliminado',
    entidad: 'ENT_43',
  },

  // ------- estado catalogo tipo email --------
  {
    codigo: 'EST_51',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_44',
  },
  {
    codigo: 'EST_52',
    nombre: 'eliminado',
    descripcion: 'eliminado por el administrador',
    entidad: 'ENT_44',
  },

  // ------- estado noticias --------
  {
    codigo: 'EST_53',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_3',
  },
  {
    codigo: 'EST_54',
    nombre: 'eliminado',
    descripcion: 'eliminado por el administrador',
    entidad: 'ENT_3',
  },
  // ------- estado catalogo pais --------
  {
    codigo: 'EST_55',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_45',
  },
  {
    codigo: 'EST_56',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo pais esta desactivado por el administrador',
    entidad: 'ENT_45',
  },
  {
    codigo: 'EST_57',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_46',
  },
  {
    codigo: 'EST_58',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo de la localidad esta desactivado por el administrador',
    entidad: 'ENT_46',
  },
  // ------- estado catalogo noticia --------
  {
    codigo: 'EST_59',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_47',
  },
  {
    codigo: 'EST_60',
    nombre: 'eliminado',
    descripcion:
      'Cuando la traduccion noticia esta desactivado por el administrador',
    entidad: 'ENT_47',
  },
  // ------- estado catalogo media --------
  {
    codigo: 'EST_61',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_48',
  },
  {
    codigo: 'EST_62',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo media esta desactivado por el administrador',
    entidad: 'ENT_48',
  },
  // ---------- Estado catalogo metodo de pago --------
  {
    codigo: 'EST_63',
    nombre: 'pendiente',
    descripcion:
      'Cuando el catalogo metodo de pago esta pendiente de desarrollo',
    entidad: 'ENT_15',
  },
  // ------- catalogo estado (catalogo archivos default) --------
  {
    codigo: 'EST_64',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_49',
  },
  {
    codigo: 'EST_65',
    nombre: 'eliminado',
    descripcion: 'eliminado',
    entidad: 'ENT_49',
  },

  // ------- estado catalogo tipo perfil --------
  {
    codigo: 'EST_66',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_50',
  },
  {
    codigo: 'EST_67',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo tipo perfil esta desactivado por el administrador',
    entidad: 'ENT_50',
  },

  {
    codigo: 'EST_68',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_51',
  },
  {
    codigo: 'EST_69',
    nombre: 'eliminado',
    descripcion: 'Cuando el voto noticiaesta desactivado por el administrador',
    entidad: 'ENT_51',
  },

  {
    codigo: 'EST_70',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_52',
  },
  {
    codigo: 'EST_71',
    nombre: 'eliminado',
    descripcion: 'Cuando el voto noticia esta desactivado por el administrador',
    entidad: 'ENT_52',
  },
  // ------- catalogo estado (catalogo tipo proyecto) --------
  {
    codigo: 'EST_72',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_53',
  },
  {
    codigo: 'EST_73',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo tipo proyecto esta desactivado por el administrador',
    entidad: 'ENT_53',
  },
  // ------- catalogo estado (traduccion catalogo tipo proyecto) --------
  {
    codigo: 'EST_74',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_54',
  },
  {
    codigo: 'EST_75',
    nombre: 'eliminado',
    descripcion:
      'Cuando la traduccion catalogo tipo proyecto esta desactivado por el administrador',
    entidad: 'ENT_54',
  },
  // ------- catalogo estado (traduccion proyecto) --------
  {
    codigo: 'EST_76',
    nombre: 'activa',
    descripcion: 'activo por el administrador',
    entidad: 'ENT_55',
  },
  {
    codigo: 'EST_77',
    nombre: 'eliminado',
    descripcion:
      'Cuando la traduccion proyecto esta desactivado por el administrador',
    entidad: 'ENT_55',
  },
  // ------- catalogo estado origen --------
  {
    codigo: 'EST_78',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_17',
  },
  {
    codigo: 'EST_79',
    nombre: 'eliminado',
    descripcion: 'eliminado',
    entidad: 'ENT_17',
  },

  // ----------------- estado de catalogo album-----------------------
  {
    codigo: 'EST_82',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_56',
  },
  {
    codigo: 'EST_83',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo ALBUM esta desactivado por el administrador',
    entidad: 'ENT_56',
  },
  // ----------------- estado de catalogo de idiomas-----------------------
  {
    codigo: 'EST_84',
    nombre: 'activa',
    descripcion: 'Cuando el catalogo de idiomas esta activo',
    entidad: 'ENT_57',
  },
  {
    codigo: 'EST_85',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo idiomas esta desactivado por el administrador',
    entidad: 'ENT_57',
  },
  // ----------------- estado de traduccion pensamientos-----------------------
  {
    codigo: 'EST_86',
    nombre: 'activa',
    descripcion: 'Cuando la traduccion de pensamientos esta activa',
    entidad: 'ENT_58',
  },
  {
    codigo: 'EST_87',
    nombre: 'eliminado',
    descripcion:
      'Cuando la traduccion de pensamientos esta inactiva o eliminada',
    entidad: 'ENT_58',
  },
  // add estado perfiles
  {
    codigo: 'EST_88',
    nombre: 'creado',
    descripcion: 'Cuando el perfil esta creado',
    entidad: 'ENT_8',
  },
  {
    codigo: 'EST_89',
    nombre: 'eliminado',
    descripcion: 'Cuando el perfil está inactivo o eliminado',
    entidad: 'ENT_8',
  },
  // ----------------- estado de catalogo tipo de asociacion-----------------------
  {
    codigo: 'EST_90',
    nombre: 'Cuando el catalogo tipo de asociacion esta activo',
    descripcion: 'activa',
    entidad: 'ENT_59',
  },
  {
    codigo: 'EST_91',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo tipo de asociacion esta desactivado por el administrador',
    entidad: 'ENT_59',
  },

  // ----------------- estado de particpante asociacion-----------------------
  {
    codigo: 'EST_92',
    nombre: 'aceptada',
    descripcion: 'La invitacion fue aceptada por el receptor',
    entidad: 'ENT_23',
  },
  {
    codigo: 'EST_93',
    nombre: 'eliminado',
    descripcion: 'Cuando el participante esta desactivado por el administrador',
    entidad: 'ENT_23',
  },
  {
    codigo: 'EST_94',
    nombre: 'cancelada',
    descripcion: 'La persona que invita la cancelo la invitacion',
    entidad: 'ENT_23',
  },
  {
    codigo: 'EST_95',
    nombre: 'rechazada',
    descripcion: 'La persona que fue invitada rechaza la invitacion',
    entidad: 'ENT_23',
  },
  {
    codigo: 'EST_96',
    nombre: 'enviada',
    descripcion: 'La invitacion aun no ha sido contestada',
    entidad: 'ENT_23',
  },
  // ----------------- estado de conversacion-----------------------
  {
    codigo: 'EST_97',
    nombre: 'activa',
    descripcion: 'Cuando la conversacion esta activa',
    entidad: 'ENT_24',
  },
  {
    codigo: 'EST_98',
    nombre: 'eliminado',
    descripcion: 'se ha elimiando la conversacion',
    entidad: 'ENT_24',
  },
  // ----------------- estado de mensaje-----------------------
  {
    codigo: 'EST_99',
    nombre: 'activa',
    descripcion: 'Cuando el mensaje esta activa',
    entidad: 'ENT_25',
  },
  {
    codigo: 'EST_100',
    nombre: 'eliminado',
    descripcion: 'se ha elimiando el mensaje',
    entidad: 'ENT_25',
  },
  // ----------------- estado de traduccion mensaje-----------------------
  {
    codigo: 'EST_101',
    nombre: 'activa',
    descripcion: 'Cuando la traducc mensaje esta activa',
    entidad: 'ENT_60',
  },
  {
    codigo: 'EST_102',
    nombre: 'eliminado',
    descripcion: 'se ha eliminado  traducc mensaje',
    entidad: 'ENT_60',
  },
  // ----------------- estado de catalogo mensajes-----------------------
  {
    codigo: 'EST_103',
    nombre: 'activa',
    descripcion: 'Cuando la traducc mensaje esta activa',
    entidad: 'ENT_61',
  },
  {
    codigo: 'EST_104',
    nombre: 'eliminado',
    descripcion: 'se ha eliminado  traducc mensaje',
    entidad: 'ENT_61',
  },

  // ----------------- estado de catalogo comentario -----------------------
  {
    codigo: 'EST_105',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_10',
  },
  {
    codigo: 'EST_106',
    nombre: 'eliminado',
    descripcion: 'Cuando el comentario esta desactivado por el administrador',
    entidad: 'ENT_10',
  },
  {
    codigo: 'EST_107',
    nombre: 'historico',
    descripcion: 'El comentario entra en un historial',
    entidad: 'ENT_10',
  },
  // ----------------- estado de catalogo tipo comentario -----------------------
  {
    codigo: 'EST_108',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_62',
  },
  {
    codigo: 'EST_109',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo tipo comentario esta desactivado por el administrador',
    entidad: 'ENT_62',
  },
  // ----------------- estado de entidad proyecto -----------------------
  {
    codigo: 'EST_110',
    nombre: 'activa',
    descripcion: 'Cuando el proyecto esta creado o proyecto nuevo',
    entidad: 'ENT_2',
  },
  {
    codigo: 'EST_111',
    nombre: 'eliminado',
    descripcion: 'Cuando el proyecto esta desactivado por el administrador',
    entidad: 'ENT_2',
  },
  {
    codigo: 'EST_112',
    nombre: 'preEstrategia',
    descripcion:
      'Cuando ha sido seleccionado pero no esta elegido las estrategas y esta Financiado en su totalidad (Gazelook, donaciones)',
    entidad: 'ENT_2',
  },
  {
    codigo: 'EST_113',
    nombre: 'enEsperaFondos',
    descripcion: 'Cuando es financiado parcialmente por gazelook',
    entidad: 'ENT_2',
  },
  {
    codigo: 'EST_114',
    nombre: 'enEstrategia',
    descripcion: 'Cuando los estrategas estan seleccionados',
    entidad: 'ENT_2',
  },
  {
    codigo: 'EST_115',
    nombre: 'enRevision',
    descripcion:
      'Cuando se finalizo la estrategia y se esta en espera del veredicto del coordinador (costo real del proyecto)',
    entidad: 'ENT_2',
  },
  {
    codigo: 'EST_116',
    nombre: 'ejecucion',
    descripcion: 'Cuando el coordinar acepto el proyecto e inicio su ejecucion',
    entidad: 'ENT_2',
  },
  {
    codigo: 'EST_117',
    nombre: 'esperaDonacion',
    descripcion:
      'Cuando el proyecto tuvo buena votacion, pero se debe financiar unicamente por donaciones.',
    entidad: 'ENT_2',
  },
  {
    codigo: 'EST_118',
    nombre: 'finalizado',
    descripcion: 'Cuando el proyecto finalizo exitosamente',
    entidad: 'ENT_2',
  },

  // ----------------- estado voto proyecto -----------------------
  {
    codigo: 'EST_119',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_31',
  },
  {
    codigo: 'EST_120',
    nombre: 'eliminado',
    descripcion: 'Cuando el voto esta desactivado por el administrador',
    entidad: 'ENT_31',
  },
  // ----------------- estado traduccion voto proyecto -----------------------
  {
    codigo: 'EST_121',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_64',
  },
  {
    codigo: 'EST_122',
    nombre: 'eliminado',
    descripcion:
      'Cuando la traducción del  voto proyecto está desactivado por el administrador',
    entidad: 'ENT_64',
  },
  // ----------------- estado catálogo
  {
    codigo: 'EST_123',
    nombre: 'aprobada',
    descripcion: 'El proyecto pasa a ejecucion',
    entidad: 'ENT_34',
  },
  {
    codigo: 'EST_124',
    nombre: 'rechazada',
    descripcion:
      'El proyecto vuelve al estado de creado a la espera de un nuevo foro',
    entidad: 'ENT_34',
  },
  {
    codigo: 'EST_125',
    nombre: 'eliminado',
    descripcion: 'Cuando la estrategia está desactivada por el administrador',
    entidad: 'ENT_34',
  },

  {
    codigo: 'EST_126',
    nombre: 'enEspera',
    descripcion: 'Cuando esta esperando por la decisión del coordinador',
    entidad: 'ENT_34',
  },
  {
    codigo: 'EST_127',
    nombre: 'caducada',
    descripcion:
      'Cuando despues de un tiempo no se concluye la estrategia (Se utiliza para liberar los fondos) en admin del sistema lo ingresa al estado',
    entidad: 'ENT_34',
  },
  // ----------------- estado balance -----------------------
  {
    codigo: 'EST_128',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_65',
  },
  {
    codigo: 'EST_129',
    nombre: 'eliminado',
    descripcion: 'Cuando el balance está desactivado por el administrador',
    entidad: 'ENT_65',
  },
  {
    codigo: 'EST_130',
    nombre: 'inactivo',
    descripcion: 'Cuando el balance no es actual',
    entidad: 'ENT_65',
  },
  // ----------------- estado rol sistema -----------------------
  {
    codigo: 'EST_131',
    nombre: 'activa',
    descripcion: 'Cuando el rol del sistema esta activo',
    entidad: 'ENT_66',
  },
  {
    codigo: 'EST_132',
    nombre: 'eliminado',
    descripcion: 'cuando el rol sistema fue desactivado por el administrador',
    entidad: 'ENT_66',
  },
  // ----------------- estado catalogoTipoRol -----------------------
  {
    codigo: 'EST_133',
    nombre: 'activa',
    descripcion: 'activa',
    entidad: 'ENT_67',
  },
  {
    codigo: 'EST_134',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo TIPO ROL esta desactivado por el administrador',
    entidad: 'ENT_67',
  },
  // ----------------- estado catalogoTipoVoto -----------------------
  {
    codigo: 'EST_135',
    nombre: 'activa',
    descripcion: 'Cuando el catalogo tipo voto esta activo',
    entidad: 'ENT_69',
  },
  {
    codigo: 'EST_136',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo tipo voto esta desactivado por el administrador',
    entidad: 'ENT_69',
  },

  // ----------------- estado catalogoRol -----------------------
  {
    codigo: 'EST_137',
    nombre: 'activa',
    descripcion: 'Cuando el rol esta activo',
    entidad: 'ENT_70',
  },
  {
    codigo: 'EST_138',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo roles esta desactivado por el administrador',
    entidad: 'ENT_70',
  },
  // ----------------- estado formulaEvento-----------------------
  {
    codigo: 'EST_139',
    nombre: 'activa',
    descripcion: 'Cuando la Formula Evento esta activo',
    entidad: 'ENT_37',
  },
  {
    codigo: 'EST_140',
    nombre: 'eliminado',
    descripcion:
      'Cuando la formula evento esta desactivado por el administrador',
    entidad: 'ENT_37',
  },
  // ----------------- estado proyectoEjecucion -----------------------
  {
    codigo: 'EST_141',
    nombre: 'activa',
    descripcion: 'Cuando el proyecto ejecucion esta activo',
    entidad: 'ENT_71',
  },
  {
    codigo: 'EST_142',
    nombre: 'eliminado',
    descripcion:
      'Cuando el proyecto ejecucion esta desactivado por el administrador',
    entidad: 'ENT_71',
  },
  // ----------------- estado traduccionProyectoEjecucion-----------------------
  {
    codigo: 'EST_143',
    nombre: 'activa',
    descripcion: 'Cuando la traduccion proyecto ejecucion esta activo',
    entidad: 'ENT_72',
  },
  {
    codigo: 'EST_144',
    nombre: 'eliminado',
    descripcion:
      'Cuando la traduccion proyecto ejecucion esta desactivado por el administrador',
    entidad: 'ENT_72',
  },
  // ----------------- estado catalogoConfiguracion
  {
    codigo: 'EST_145',
    nombre: 'activa',
    descripcion: 'Cuando la el catalogo de configuracion esta activo',
    entidad: 'ENT_68',
  },
  {
    codigo: 'EST_146',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo de configuracion desactivado por el administrador',
    entidad: 'ENT_68',
  },

  // ----------------- estado evento
  {
    codigo: 'EST_147',
    nombre: 'activa',
    descripcion: 'Cuando el evento esta activo',
    entidad: 'ENT_36',
  },
  {
    codigo: 'EST_148',
    nombre: 'eliminado',
    descripcion: 'Cuando el evento esta desactivado por el administrador',
    entidad: 'ENT_36',
  },
  // ----------------- estado configuracion evento
  {
    codigo: 'EST_149',
    nombre: 'activa',
    descripcion: 'Cuando la configuracion del evento esta activo',
    entidad: 'ENT_35',
  },
  {
    codigo: 'EST_150',
    nombre: 'eliminado',
    descripcion:
      'Cuando la configuracion del evento esta desactivado por el administrador',
    entidad: 'ENT_35',
  },

  // ----------------- estado catalogoEvento
  {
    codigo: 'EST_151',
    nombre: 'activa',
    descripcion: 'Cuando el catalogo evento está activo',
    entidad: 'ENT_75',
  },
  {
    codigo: 'EST_152',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo evento está desactivado por el administrador',
    entidad: 'ENT_75',
  },
  // ----------------- estado participanteProyecto
  {
    codigo: 'EST_153',
    nombre: 'activa',
    descripcion: 'Cuando el participante proyecto esta activo',
    entidad: 'ENT_32',
  },
  {
    codigo: 'EST_154',
    nombre: 'eliminado',
    descripcion:
      'Cuando el participante proyecto esta desactivado por el administrador',
    entidad: 'ENT_32',
  },
  // ----------------- estado traduccionComentario
  {
    codigo: 'EST_155',
    nombre: 'activa',
    descripcion: 'Cuando la traducción del comentario está activa',
    entidad: 'ENT_63',
  },
  {
    codigo: 'EST_156',
    nombre: 'eliminado',
    descripcion:
      'Cuando la traducción del comentario esta desactivado por el administrador',
    entidad: 'ENT_63',
  },

  // ----------------- estado rolEntidad
  {
    codigo: 'EST_157',
    nombre: 'activa',
    descripcion: 'Cuando el rol entidad está activa',
    entidad: 'ENT_76',
  },
  {
    codigo: 'EST_158',
    nombre: 'eliminado',
    descripcion: 'Cuando el rol entidad fue desactivado por el administrador',
    entidad: 'ENT_76',
  },
  {
    codigo: 'EST_159',
    nombre: 'contacto',
    descripcion: 'Dos perfiles son contactos en participante asociacion',
    entidad: 'ENT_23',
  },
  // ----------------- estado configuracionEstilo
  {
    codigo: 'EST_160',
    nombre: 'activa',
    descripcion: 'Cuando la entidad configuracionEstilo está activa',
    entidad: 'ENT_77',
  },
  {
    codigo: 'EST_161',
    nombre: 'eliminado',
    descripcion:
      'Cuando la entidad configuracionEstilo desactivado por el administrador',
    entidad: 'ENT_77',
  },
  {
    codigo: 'EST_162',
    nombre: 'inactiva',
    descripcion: 'Cuando la configuracion no sera utilizada',
    entidad: 'ENT_77',
  },

  // ----------------- estado catalgoParticipanteConfiguracion
  {
    codigo: 'EST_163',
    nombre: 'activa',
    descripcion:
      'Cuando la entidad catalogoParticipanteConfiguracion está activa',
    entidad: 'ENT_82',
  },
  {
    codigo: 'EST_164',
    nombre: 'eliminado',
    descripcion:
      'Cuando la entidad catalogoParticipanteConfiguracion desactivado por el administrador',
    entidad: 'ENT_82',
  },

  // ----------------- estado estilo----------
  {
    codigo: 'EST_165',
    nombre: 'activa',
    descripcion: 'Cuando la entidad estilo está activa',
    entidad: 'ENT_80',
  },
  {
    codigo: 'EST_166',
    nombre: 'eliminado',
    descripcion: 'Cuando el estilo esta desactivado por el administrador',
    entidad: 'ENT_80',
  },

  // ----------------- estado catalogo colores
  {
    codigo: 'EST_167',
    nombre: 'activa',
    descripcion: 'Cuando la entidad está activa',
    entidad: 'ENT_73',
  },
  {
    codigo: 'EST_168',
    nombre: 'eliminado',
    descripcion: 'Cuando la entidad esta desactivada por el administrador',
    entidad: 'ENT_73',
  },

  // ----------------- estado catalogo tipo colores
  {
    codigo: 'EST_169',
    nombre: 'activa',
    descripcion: 'Cuando la entidad estilo está activa',
    entidad: 'ENT_74',
  },
  {
    codigo: 'EST_170',
    nombre: 'eliminado',
    descripcion: 'Cuando la esta desactivada por el administrador',
    entidad: 'ENT_74',
  },

  // ----------------- estado configuración participante
  {
    codigo: 'EST_171',
    nombre: 'activa',
    descripcion: 'Cuando la entidad estilo está activa',
    entidad: 'ENT_83',
  },
  {
    codigo: 'EST_172',
    nombre: 'eliminado',
    descripcion: 'Cuando la esta desactivada por el administrador',
    entidad: 'ENT_83',
  },

  // ----------------- estado catalogo estilos
  {
    codigo: 'EST_173',
    nombre: 'activa',
    descripcion: 'Cuando la entidad catalogo estilos  está activa',
    entidad: 'ENT_81',
  },
  {
    codigo: 'EST_174',
    nombre: 'eliminado',
    descripcion:
      'Cuando el catalogo estilos esta desactivado por el administrador',
    entidad: 'ENT_81',
  },
  {
    codigo: 'EST_175',
    nombre: 'pendiente',
    descripcion: 'Cuando el evento esta pendiente por ejecutarse',
    entidad: 'ENT_36',
  },
  {
    codigo: 'EST_176',
    nombre: 'ejecutado',
    descripcion: 'Cuando se ejecutó el job o el',
    entidad: 'ENT_36',
  },

    // ----------------- estado documentos legales
    {
      codigo: 'EST_177',
      nombre: 'activa',
      descripcion: 'Cuando la entidad documentos legales está activa',
      entidad: 'ENT_85',
    },
    {
      codigo: 'EST_178',
      nombre: 'eliminado',
      descripcion: 'Cuando la entidad documentos legales esta desactivada por el administrador',
      entidad: 'ENT_85',
    },

       // ----------------- estado catalogo origen documentos 
       {
        codigo: 'EST_179',
        nombre: 'activa',
        descripcion: 'Cuando la entidad catalogo origen documentos está activa',
        entidad: 'ENT_86',
      },
      {
        codigo: 'EST_180',
        nombre: 'eliminado',
        descripcion: 'Cuando la entidad catalogo origen documentos esta desactivada por el administrador',
        entidad: 'ENT_86',
      },
]);

//4.
//catalogo de idiomas

db.catalogo_idiomas.insert([
  {
    codigo: 'IDI_1',
    nombre: 'español',
    codNombre: 'es',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
    estado: 'EST_84',
  },
  {
    codigo: 'IDI_2',
    nombre: 'ingles',
    codNombre: 'en',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
    estado: 'EST_84',
  },
  {
    codigo: 'IDI_3',
    nombre: 'aleman',
    codNombre: 'de',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
    estado: 'EST_84',
  },
  {
    codigo: 'IDI_4',
    nombre: 'portugues',
    codNombre: 'pt',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
    estado: 'EST_84',
  },
  {
    codigo: 'IDI_5',
    nombre: 'frances',
    codNombre: 'fr',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
    estado: 'EST_84',
  },
  {
    codigo: 'IDI_6',
    nombre: 'italiano',
    codNombre: 'it',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
    estado: 'EST_84',
  },
]);

//5.
//catalogo traduccion tipo perfil
// traduccion espaniol ES
db.traduccion_catalogo_tipo_perfil.insert([
  {
    nombre: 'CLÁSICO',
    descripcion:
      'EN ESTE PERFIL, ELEGIRÁ LAS FOTOS QUE DESEE DE USTED MISMO, SU NOMBRE E INFORMACIÓN QUE CORRESPONDA.',
    idioma: 'IDI_1',
    original: true,
    codigoTipoPerfil: 'TIPERFIL_1',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    nombre: 'LÚDICO',
    descripcion:
      'UN PERFIL ENTRETENIDO E INTRIGANTE. SUBA FOTOS MIRANDO A LA CÁMARA DE CUANDO ERA UN BEBÉ O UN NIÑO. OPCIONALMENTE, PUEDE SUBIR MIRADAS INOCENTES DE SI MISMO COMO ADULTO (CADA VEZ QUE ABRA GAZELOOK, APARECERÁ AL AZAR UNA FOTO DE LAS ELEGIDAS). LUEGO ELIJA UN APODO Y DIVIÉRTASE ADJUNTANDO LA INFORMACIÓN LÚDICA QUE LE SATISFAGA.',
    idioma: 'IDI_1',
    original: true,
    codigoTipoPerfil: 'TIPERFIL_2',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    nombre: 'SUSTITUTO',
    descripcion:
      '1- LE DA LA POSIBILIDAD DE CREAR UN PERFIL DE ALGUIEN A QUIEN DESEE SUSTITUIR. POR EJEMPLO, EL DE ALGÚN INFANTE O EL DE UN SER QUERIDO QUE NO PUEDE HACERLO O QUE LO HA DEJADO. 2- ¡TAMBIÉN PUEDE SER UN PERFIL CON ALGÚN PERSONAJE INVENTADO Y UN GRAN APODO!',
    idioma: 'IDI_1',
    original: true,
    codigoTipoPerfil: 'TIPERFIL_3',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    nombre: 'GRUPO',
    descripcion:
      'ESTE PERFIL ES ÚTIL PARA ASOCIACIONES, GRUPOS, FAMILIAS Y AMIGOS QUE DESEEN TENER UN PROYECTO EN COMÚN. SE ACEPTAN MÚLTIPLES FOTOS Y UN SOLO NOMBRE O APODO PARA TODO EL GRUPO. ',
    idioma: 'IDI_1',
    original: true,
    codigoTipoPerfil: 'TIPERFIL_4',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//6.
//catalogo tipo perfil

db.catalogo_tipo_perfil.insert([
  {
    codigo: 'TIPERFIL_1',
    estado: 'EST_66',
  },
  {
    codigo: 'TIPERFIL_2',
    estado: 'EST_66',
  },
  {
    codigo: 'TIPERFIL_3',
    estado: 'EST_66',
  },
  {
    codigo: 'TIPERFIL_4',
    estado: 'EST_66',
  },
]);

//7.
//catalogo suscripción

db.catalogo_suscripcion.insert([
  {
    codigo: 'CATSUS_1',
    nombre: 'anual',
    duracion: 365,
    costo: 0,
    estado: 'EST_37',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATSUS_2',
    nombre: 'semestral',
    duracion: 182,
    costo: 0,
    estado: 'EST_37',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATSUS_3',
    nombre: 'mensual',
    duracion: 30,
    costo: 0,
    estado: 'EST_37',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//8.
//catalogo origen

db.catalogo_origen.insert([
  {
    codigo: 'CATORI_1',
    nombre: 'Suscripciones',
    descripcion: 'Suscripciones del usuario',
    ingreso: true,
    estado: 'EST_78',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATORI_2',
    nombre: 'Gastos operativos',
    descripcion: 'Gastos operativos',
    ingreso: false,
    estado: 'EST_78',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATORI_3',
    nombre: 'Donacion',
    descripcion: 'Donacion',
    ingreso: true,
    estado: 'EST_78',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATORI_4',
    nombre: 'Ganancia proyecto',
    descripcion: 'Ganancia proyecto',
    ingreso: true,
    estado: 'EST_78',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATORI_5',
    nombre: 'Asignacion fondo',
    descripcion: 'Asignacion fondo',
    ingreso: false,
    estado: 'EST_78',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//9.
//catalogo roles

db.catalogo_rol.insert([
  {
    codigo: 'CATROL_1',
    nombre: 'administrador',
    descripcion: 'Persona que crea una conversacion o proyecto',
    estado: 'EST_137',
    tipo: 'CATIPROL_2',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATROL_2',
    nombre: 'coautor',
    descripcion: 'Persona que emite un comentario en un proyecto',
    estado: 'EST_137',
    tipo: 'CATIPROL_2',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATROL_3',
    nombre: 'coordinador gazelook',
    descripcion: 'coordinador gazelook',
    estado: 'EST_137',
    tipo: 'CATIPROL_1',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATROL_4',
    nombre: 'propietario',
    descripcion: 'propietario',
    estado: 'EST_137',
    tipo: 'CATIPROL_2',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATROL_5',
    nombre: 'usuario',
    descripcion: 'Usuario registrado',
    estado: 'EST_137',
    tipo: 'CATIPROL_1',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATROL_6',
    nombre: 'estrategas',
    descripcion: 'estrategas',
    estado: 'EST_137',
    tipo: 'CATIPROL_2',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATROL_7',
    nombre: 'lector',
    descripcion: 'lector',
    estado: 'EST_137',
    tipo: 'CATIPROL_2',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATROL_8',
    nombre: 'escritor',
    descripcion: 'escritor',
    estado: 'EST_137',
    tipo: 'CATIPROL_2',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//10.
//catalogo tipo rol

db.catalogo_tipo_rol.insert([
  {
    codigo: 'CATIPROL_1',
    nombre: 'Sistema',
    estado: 'EST_133',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATIPROL_2',
    nombre: 'Usuario',
    estado: 'EST_133',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// 11. catalogo album
db.catalogo_album.insert([
  {
    codigo: 'CATALB_1',
    nombre: 'general',
    descripcion: 'Album público',
    estado: 'EST_82',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATALB_2',
    nombre: 'perfil',
    descripcion: 'Album privado',
    estado: 'EST_82',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATALB_3',
    nombre: 'link',
    descripcion: 'Album de links',
    estado: 'EST_82',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATALB_4',
    nombre: 'audios',
    descripcion: 'Album de audios',
    estado: 'EST_82',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//12. Catalogo tipo media
db.catalogo_tipo_media.insert([
  {
    codigo: 'CATTIPMED_1',
    nombre: 'image',
    id_catalogoEstado: 'ENT_41',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATTIPMED_2',
    nombre: 'video',
    id_catalogoEstado: 'ENT_41',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATTIPMED_3',
    nombre: 'audio',
    id_catalogoEstado: 'ENT_41',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATTIPMED_4',
    nombre: 'files',
    estado: 'ENT_41',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// Catalogo media
db.catalogo_media.insert([
  {
    codigo: 'CATMED_1',
    nombre: 'link',
    estado: 'EST_61',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATMED_2',
    nombre: 'compuesto',
    estado: 'EST_61',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATMED_3',
    nombre: 'simple',
    estado: 'EST_61',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// 11
// catálogo tipo moneda

db.catalogo_tipo_moneda.insert([
  {
    codigo: 'TIPMON_1',
    nombre: 'dolares',
    predeterminado: true,
    estado: 'EST_12',
  },
  {
    codigo: 'TIPMON_2',
    nombre: 'euros',
    predeterminado: false,
    estado: 'EST_12',
  },
]);

// 12
// catálogo metodo pago

db.catalogo_metodo_pago.insert([
  {
    codigo: 'METPAG_1',
    traducciones: [ObjectId('5f31ca85285532b988a46979')],
    estado: 'EST_13',
    icono: ObjectId('5f2d866d5353a96b789722e5'),
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'METPAG_2',
    traducciones: [ObjectId('5f31ca85285532b988a4697a')],
    estado: 'EST_13',
    icono: ObjectId('5f2d85a05353a96b789722d9'),
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'METPAG_3',
    traducciones: [ObjectId('5f31ca85285532b988a4697b')],
    estado: 'EST_13',
    icono: ObjectId('5f3eecb66bdb581d3466c462'),
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'METPAG_4',
    traducciones: [ObjectId('5f31ca85285532b988a4697c')],
    estado: 'EST_13',
    icono: ObjectId('5f2d85a05353a96b789722d9'),
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'METPAG_5',
    traducciones: [ObjectId('5f31ca85285532b988a4697d')],
    estado: 'EST_13',
    icono: ObjectId('5f2d85a05353a96b789722d9'),
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// Traduccion catalogo metodo de pago
// traduccion espaniol ES
db.traduccion_catalogo_metodo_pago.insert([
  {
    codigo: 'METPAG_1',
    nombre: 'tarjeta',
    descripcion: 'tarjeta de crédito o débito',
    idioma: 'IDI_1',
    original: true,
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'METPAG_2',
    nombre: 'transferencia',
    descripcion: 'transferencia bancaría',
    idioma: 'IDI_1',
    original: true,
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'METPAG_3',
    nombre: 'paypal',
    descripcion: 'Paypal',
    idioma: 'IDI_1',
    original: true,
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'METPAG_4',
    nombre: 'trueque',
    descripcion: 'trueque',
    idioma: 'IDI_1',
    original: true,
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'METPAG_5',
    nombre: 'bitcoin',
    descripcion: 'Bitcoin',
    idioma: 'IDI_1',
    original: true,
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// 13
// catálogo tipo beneficiario

db.catalogo_tipo_beneficiario.insert([
  {
    codigo: 'BENEF_1',
    nombre: 'usuario',
    descripcion: '',
    estado: 'EST_16',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'BENEF_2',
    nombre: 'proyecto',
    descripcion: '',
    estado: 'EST_16',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//db.catalogo_tipo_archivo.insert
//([
//{
//codigo: "CATIPARC_1",
//nombre: "portada",
//descripcion: "portada del album",
//estado: "EST_43",
//fechaCreacion: new Date(),
//fechaActualizacion: null
//},
//{
//codigo: "CATIPARC_2",
//nombre: "principal",
//descripcion: "foto simple",
//estado: "EST_43",
//fechaCreacion: new Date(),
//fechaActualizacion: null
//},
//{
//codigo: "CATIPARC_3",
//nombre: "miniatura",
//descripcion: "foto compuesta",
//estado: "EST_43",
//fechaCreacion: new Date(),
//fechaActualizacion: null
//},
//{
//codigo: "CATIPARC_4",
//nombre: "link",
//descripcion: "link",
//estado: "EST_43",
//fechaCreacion: new Date(),
//fechaActualizacion: null
//}
//])

db.catalogo_tipo_email.insert([
  {
    codigo: 'CTEM_1',
    nombre: 'validacion',
    descripcion: 'validar correo normal',
    estado: 'EST_51',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CTEM_2',
    nombre: 'validacionResponsable',
    descripcion: 'validar correo por el responsable',
    estado: 'EST_51',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CTEM_3',
    nombre: 'trasferirProyecto',
    descripcion: 'Validar correo para transferir el proyecto a otro usuario',
    estado: 'EST_51',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CTEM_4',
    nombre: 'recuperarContrasenia',
    descripcion: 'Validar correo para recuperar la contraseña de un usuario',
    estado: 'EST_51',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
},
{
    codigo: 'CTEM_5',
    nombre: 'actualizacionContrasenia',
    descripcion: 'Envia correo al usuario para confirmarle que cambio su contraseña',
    estado: 'EST_51',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
},
{
  codigo: 'CTEM_6',
  nombre: 'peticionDatos',
  descripcion: 'Envia correo al usuario para confirmación de solicitud de la información que tiene la aplicación',
  estado: 'EST_51',
  fechaCreacion: new Date(),
  fechaActualizacion: null,
},
{
  codigo: 'CTEM_7',
  nombre: 'eliminacionDatos',
  descripcion: 'Envia correo al usuario para confirmación de solicitud de Eliminación de la información que tiene la aplicación',
  estado: 'EST_51',
  fechaCreacion: new Date(),
  fechaActualizacion: null,
},


]);
// ------- catalogo archivo default --------
db.catalogo_archivo_default.insert([
  {
    codigo: 'CAT_ARC_DEFAULT_01',
    nombre: 'album_general',
    estado: 'EST_64',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CAT_ARC_DEFAULT_02',
    nombre: 'album_perfil',
    estado: 'EST_64',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CAT_ARC_DEFAULT_03',
    nombre: 'proyectos',
    estado: 'EST_64',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CAT_ARC_DEFAULT_04',
    nombre: 'noticias',
    estado: 'EST_64',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CAT_ARC_DEFAULT_05',
    nombre: 'contactos',
    estado: 'EST_64',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);
// ------- traduccion catalogo tipo proyecto --------
db.traduccion_catalogo_tipo_proyecto.insert([
  //traduccion en idioma español
  {
    referencia: 'CAT_TIPO_PROY_01',
    nombre: 'Mundial',
    descripcion: 'Mundial',
    idioma: 'IDI_1',
    original: true,
    estado: 'EST_74',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    referencia: 'CAT_TIPO_PROY_02',
    nombre: 'Local',
    descripcion: 'Local',
    idioma: 'IDI_1',
    original: true,
    estado: 'EST_74',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CAT_TIP_PROY_03',
    nombre: 'Network',
    descripcion: 'Network',
    idioma: 'IDI_1',
    original: true,
    estado: 'EST_74',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CAT_TIP_PROY_04',
    nombre: 'País',
    descripcion: 'País',
    idioma: 'IDI_1',
    original: true,
    estado: 'EST_74',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);
// ------- catalogo tipo proyecto --------
db.catalogo_tipo_proyecto.insert([
  {
    codigo: 'CAT_TIPO_PROY_01',
    traducciones: [ObjectId('5f3eaacfd9d7099cad68c632')],
    estado: 'EST_72',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CAT_TIPO_PROY_02',
    traducciones: [ObjectId('5f3eac47a256a91bc02e4387')],
    estado: 'EST_72',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CAT_TIPO_PROY_03',
    traducciones: [ObjectId('5f3ebdb5d9d7099cad68c635')],
    estado: 'EST_72',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CAT_TIPO_PROY_04',
    traducciones: [ObjectId('5f3ebe39d9d7099cad68c636')],
    estado: 'EST_72',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// ------- catalogo tipo asociacion default --------
db.catalogo_tipo_asociacion.insert([
  {
    codigo: 'CTAS_1',
    nombre: 'contacto',
    descripcion: 'contacto entre dos perfiles',
    estado: 'EST_90',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CTAS_2',
    nombre: 'grupo',
    descripcion: 'grupo entre varios perfiles',
    estado: 'EST_90',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
]);

// ------- catalogo tipo mensaje default --------
db.catalogo_tipo_mensaje.insert([
  {
    codigo: 'CTMSJ_1',
    nombre: 'texto',
    descripcion: ' cadena de caracteres ',
    estado: 'EST_101',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CTMSJ_2',
    nombre: 'archivo',
    descripcion: 'archivos pdf, excel etc',
    estado: 'EST_101',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CTMSJ_3',
    nombre: 'llamada',
    descripcion: 'llamada datos',
    estado: 'EST_101',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CTMSJ_4',
    nombre: 'videollamada',
    descripcion: 'videollamada de datos',
    estado: 'EST_101',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
]);

// ------- catalogoTipoVoto --------
db.catalogo_tipo_voto.insert([
  {
    codigo: 'CTVOTO_1',
    nombre: 'normal',
    votoAdmin: false,
    estado: 'EST_135',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CTVOTO_2',
    nombre: 'foro',
    votoAdmin: false,
    estado: 'EST_135',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CTVOTO_3',
    nombre: 'normal',
    votoAdmin: true,
    estado: 'EST_135',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CTVOTO_4',
    nombre: 'foro',
    votoAdmin: true,
    estado: 'EST_135',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
]);

//catalogo tipo comentario

db.catalogo_tipo_comentario.insert([
  {
    codigo: 'CATIPCOM_1',
    nombre: 'foro',
    estado: 'EST_108',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATIPCOM_2',
    nombre: 'normal',
    estado: 'EST_108',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATIPCOM_3',
    nombre: 'estrategia',
    estado: 'EST_108',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//catalogo evento

db.catalogo_evento.insert([
  {
    codigo: 'CATEVT_1',
    nombre: 'foro',
    configuraciones: '',
    formulas: '',
    estado: 'EST_151',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATEVT_2',
    nombre: 'loteria',
    configuraciones: '',
    formulas: '',
    estado: 'EST_151',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATEVT_3',
    nombre: 'estrategia',
    configuraciones: '',
    formulas: '',
    estado: 'EST_151',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATEVT_4',
    nombre: 'esperaFondos',
    configuraciones: '',
    formulas: '',
    estado: 'EST_151',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATEVT_5',
    nombre: 'VotoProyectoGazelook',
    configuraciones: ['CONFEVT_1'],
    formulas: ['FOREVT_1'],
    estado: 'EST_151',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    codigo: 'CATEVT_6',
    nombre: 'gastoOperacional',
    configuraciones: null,
    formulas: null,
    estado: 'EST_151',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//FormulaEvento

db.formula_evento.insert([
  {
    codigo: 'FOREVT_1',
    formula: '${TOTAL_VOTOS}*${PORCENTAJE_VOTO_GAZELOOK}',
    descripcion: 'Formula para calcular los votos de gazelook a un proyecto',
    catalogoEvento: 'CATEVT_5',
    prioridad: 1,
    estado: 'EST_139',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

//configuracionEvento

db.configuracion_evento.insert([
  {
    codigo: 'CONFEVT_1',
    intervalo: '',
    duracion: '',
    ciclico: false,
    formulas: ['FOREVT_1'],
    catalogoEvento: ['CATEVT_5'],
    estado: 'EST_149',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// -------------SCRIPT ROLES ENTIDAD---------

db.rol_entidad.insert([
  //------usuario normal-------
  // proyectos
  {
    _id: ObjectId('5f4ea9c600744cdbbd2c17ea'),
    nombre: 'Usuario normal: Proyectos',
    rol: 'CATROL_5',
    entidad: 'ENT_2',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd49'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  // noticias
  {
    _id: ObjectId('5f4ea9df00744cdbbd2c17eb'),
    nombre: 'Usuario normal: Noticias',
    rol: 'CATROL_5',
    entidad: 'ENT_3',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd49'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  // comentarios
  {
    _id: ObjectId('5f4ea9e400744cdbbd2c17ec'),
    nombre: 'Usuario normal: Comentarios',
    rol: 'CATROL_5',
    entidad: 'ENT_10',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd49'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  // pensamiento
  {
    _id: ObjectId('5f4ea9e800744cdbbd2c17ed'),
    nombre: 'Usuario normal: Pensamiento',
    rol: 'CATROL_5',
    entidad: 'ENT_4',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd49'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  //------usuario gazelook-------
  //proyectoEjecucion
  {
    _id: ObjectId('5f4ea9ec00744cdbbd2c17ee'),
    nombre: 'Coordinador Gazelook: ProyectoEjecucion',
    rol: 'CATROL_3',
    entidad: 'ENT_71',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  //Estrategia
  {
    _id: ObjectId('5f4ea9f000744cdbbd2c17ef'),
    nombre: 'Coordinador Gazelook: Estrategia',
    rol: 'CATROL_3',
    entidad: 'ENT_34',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  //comentarios
  {
    _id: ObjectId('5f4ea9f400744cdbbd2c17f0'),
    nombre: 'Coordinador Gazelook: Comentarios',
    rol: 'CATROL_3',
    entidad: 'ENT_10',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },

  //------usuario coautor | rol especifico-------
  // comentarios
  {
    _id: ObjectId('5f4fe4e6452d60bfdd392eb4'),
    nombre: 'usuario coautor: Comentarios',
    rol: 'CATROL_2',
    entidad: 'ENT_10',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },

  //------usuario propietario | rol especifico-------
  // comentarios
  {
    _id: ObjectId('5f4fe4ec452d60bfdd392eb5'),
    nombre: 'usuario propietario: Comentarios',
    rol: 'CATROL_4',
    entidad: 'ENT_10',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd49'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },

  //------usuario propietario | rol especifico-------
  // proyectos
  {
    _id: ObjectId('5f4fe960452d60bfdd392eb6'),
    nombre: 'usuario propietario: Proyectos',
    rol: 'CATROL_4',
    entidad: 'ENT_2',
    acciones: [
      ObjectId('5f48435d6b14c316f5e9cd48'),
      ObjectId('5f48435d6b14c316f5e9cd49'),
      ObjectId('5f48435d6b14c316f5e9cd4b'),
      ObjectId('5f48435d6b14c316f5e9cd4a'),
    ],
    estado: 'EST_157',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// ___________ROLES SISTEMA________
db.rol_sistema.insert([
  //------usuario normal-------
  {
    _id: ObjectId('5f4eb41200744cdbbd2c17fe'),
    nombre: 'Usuario normal',
    rol: 'CATROL_5',
    rolesEspecificos: [
      ObjectId('5f4ea9e800744cdbbd2c17ed'),
      ObjectId('5f4ea9e400744cdbbd2c17ec'),
      ObjectId('5f4ea9df00744cdbbd2c17eb'),
      ObjectId('5f4ea9c600744cdbbd2c17ea'),
    ],
    estado: 'EST_131',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  //------coordinador gazelook-------
  {
    _id: ObjectId('5f4eb41800744cdbbd2c17ff'),
    nombre: 'Coordinador Gazelook',
    rol: 'CATROL_3',
    rolesEspecificos: [
      ObjectId('5f4ea9ec00744cdbbd2c17ee'),
      ObjectId('5f4ea9f000744cdbbd2c17ef'),
      ObjectId('5f4ea9f400744cdbbd2c17f0'),
    ],
    estado: 'EST_131',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// ___________ catalogo tipo color________
db.catalogo_tipo_colores.insert([
  {
    _id: ObjectId('5f5165969b3cd047e13a1afa'),
    codigo: 'CATIPCOL1',
    nombre: 'HEXADECIMAL',
    estado: 'EST_169',
    formula: 'color',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    _id: ObjectId('5f5165969b3cd047e13a1afb'),
    codigo: 'CATIPCOL2',
    nombre: 'RGBA',
    estado: 'EST_169',
    formula: 'rgba (a,b,c,d)',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    _id: ObjectId('5f5165969b3cd047e13a1afc'),
    codigo: 'CATIPCOL3',
    nombre: 'HSB',
    estado: 'EST_169',
    formula: 'a.b.c',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// ___________ catalogo colores ________
db.catalogo_colores.insert([
  {
    _id: ObjectId('5f5166929b3cd047e13a1afd'),
    codigo: 'ffc93c',
    tipo: ObjectId('5f5165969b3cd047e13a1afa'),
    estado: 'EST_167',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },

  {
    _id: ObjectId('5f5166929b3cd047e13a1afe'),
    codigo: 'fddb3a',
    tipo: ObjectId('5f5165969b3cd047e13a1afa'),
    estado: 'EST_167',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    _id: ObjectId('5f5166929b3cd047e13a1aff'),
    codigo: '07689f',
    tipo: ObjectId('5f5165969b3cd047e13a1afa'),
    estado: 'EST_167',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    _id: ObjectId('5f5166929b3cd047e13a1b00'),
    codigo: 'e94560',
    tipo: ObjectId('5f5165969b3cd047e13a1afa'),
    estado: 'EST_167',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    _id: ObjectId('5f5166929b3cd047e13a1b01'),
    codigo: '776d8a',
    tipo: ObjectId('5f5165969b3cd047e13a1afa'),
    estado: 'EST_167',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
  {
    _id: ObjectId('5f5166929b3cd047e13a1b02'),
    codigo: '158467',
    tipo: ObjectId('5f5165969b3cd047e13a1afa'),
    estado: 'EST_167',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);
// ___________ catalogo estilos ________
db.catalogo_estilos.insert([
  {
    _id: ObjectId('5f5166be9b3cd047e13a1b03'),
    codigo: 'CATEST1',
    nombre: 'fondo',
    estado: 'EST_173',
    descripcion: 'Fondo para comentarios',
    fechaCreacion: new Date(),
    fechaActualizacion: null,
  },
]);

// ___________  estilo ________
db.estilos.insert([
  {
    _id: ObjectId('5f514fffb63b3efb055490e7'),
    codigo: 'ESTILO1',
    media: null,
    estado: 'EST_165',
    color: ObjectId('5f5166929b3cd047e13a1afd'),
    tipo: ObjectId('5f5166be9b3cd047e13a1b03'),
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    _id: ObjectId('5f51a4829b3cd047e13a1b07'),
    codigo: 'ESTILO2',
    media: null,
    estado: 'EST_165',
    color: ObjectId('5f5166929b3cd047e13a1afe'),
    tipo: ObjectId('5f5166be9b3cd047e13a1b03'),
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    _id: ObjectId('5f51a4829b3cd047e13a1b08'),
    codigo: 'ESTILO3',
    media: null,
    estado: 'EST_165',
    color: ObjectId('5f5166929b3cd047e13a1aff'),
    tipo: ObjectId('5f5166be9b3cd047e13a1b03'),
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    _id: ObjectId('5f51a4829b3cd047e13a1b09'),
    codigo: 'ESTILO4',
    media: null,
    estado: 'EST_165',
    color: ObjectId('5f5166929b3cd047e13a1b00'),
    tipo: ObjectId('5f5166be9b3cd047e13a1b03'),
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    _id: ObjectId('5f51a4829b3cd047e13a1b0a'),
    codigo: 'ESTILO5',
    media: null,
    estado: 'EST_165',
    color: ObjectId('5f5166929b3cd047e13a1b01'),
    tipo: ObjectId('5f5166be9b3cd047e13a1b03'),
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    _id: ObjectId('5f51a4829b3cd047e13a1b0b'),
    codigo: 'ESTILO6',
    media: null,
    estado: 'EST_165',
    color: ObjectId('5f5166929b3cd047e13a1b02'),
    tipo: ObjectId('5f5166be9b3cd047e13a1b03'),
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
]);

// ___________ configuracion estilo ________ falta por hacer
db.configuracion_estilo.insert([
  // estilo:{color: azul: tipo fondo} | tipo: predeterminada
  {
    _id: ObjectId('5f5171869b3cd047e13a1b06'),
    codigo: 'CONFEST1',
    estado: 'EST_160',
    entidad: 'ENT_10',
    silenciada: false,
    tonoNotificacion: null,
    estilos: [
      ObjectId('5f514fffb63b3efb055490e7'),
      ObjectId('5f51a4829b3cd047e13a1b07'),
      ObjectId('5f51a4829b3cd047e13a1b08'),
      ObjectId('5f51a4829b3cd047e13a1b09'),
      ObjectId('5f51a4829b3cd047e13a1b0a'),
      ObjectId('5f51a4829b3cd047e13a1b0b'),
    ],
    tipo: ObjectId('5f5168699b3cd047e13a1b04'),
    catalogoParticipante: null,
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  
]);

// ___________ catalogo configuracion ________
db.catalogo_configuracion.insert([
  {
    _id: ObjectId('5f5168699b3cd047e13a1b04'),
    codigo: 'CATCONF1',
    estado: 'EST_145',
    nombre: 'predeterminada',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    _id: ObjectId('5f5168699b3cd047e13a1b05'),
    codigo: 'CATCONF2',
    estado: 'EST_145',
    nombre: 'personalizada',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
]);


// ___________ catalogo estados del mensaje ________
db.catalogo_mensajes.insert([
  {
    codigo: 'CTM_1',
    estado: 'EST_99',
    nombre: 'enviado',
    descripcion: 'el mensaje ha sido enviado al destinatario',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CTM_2',
    estado: 'EST_99',
    nombre: 'entregado',
    descripcion: 'el mensaje ha sido entregado al destinatario',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CTM_3',
    estado: 'EST_99',
    nombre: 'leido',
    descripcion: 'el mensaje ha sido leido por el destinatario',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
]);

// ___________ catalogo origen documentacion ________
db.catalogo_origen_documento.insert([
  {
    codigo: 'CATORIDOC1',
    estado: 'EST_179',
    nombre: 'terminosCondiciones',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
  {
    codigo: 'CATORIDOC2',
    estado: 'EST_179',
    nombre: 'cuentaFiduciaria',
    fechaCreacion: new Date(),
    fechaActualizacion: new Date(),
  },
]);