import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { CoinPaymentsDetalleTransactionController } from './controllers/coinpaymentez-detalle-transaction.controller';
import { CoinPaymentsTransactionController } from './controllers/coinpaymentez-transaction.controller';
import { ConverCoinPaymentsController } from './controllers/conver-coinpaymentez.controller';
import { CoinPaymentsRatesController } from './controllers/payment-intents.controller';
import { CoinpaymentezInfoTransactionService } from './services/coinpayments-info-transaction.service';
import { CoinpaymentezRatesService } from './services/coinpayments-rates.service';
import { CoinpaymentezTransactionService } from './services/coinpayments-transaction.service';
import { ConverCoinpaymentezService } from './services/conver-coinpayments.service';

@Module({
  providers: [CoinpaymentezRatesService, CoinpaymentezTransactionService, ConverCoinpaymentezService, CoinpaymentezInfoTransactionService, Funcion],
  exports: [CoinpaymentezRatesService, CoinpaymentezTransactionService, ConverCoinpaymentezService, CoinpaymentezInfoTransactionService],
  controllers: [CoinPaymentsRatesController, ConverCoinPaymentsController, CoinPaymentsTransactionController, CoinPaymentsDetalleTransactionController],
})
export class CoinpaymentezModule {}
