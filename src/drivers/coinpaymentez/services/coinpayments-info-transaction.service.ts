import { Injectable } from '@nestjs/common';
import Coinpayments from 'coinpayments';
import { CoinpaymentsCredentials } from 'coinpayments/dist/types/base';
import { CoinpaymentsCreateTransactionOpts, CoinpaymentsGetTxOpts } from 'coinpayments/dist/types/options';
import { ConfigService } from '../../../config/config.service';


@Injectable()
export class CoinpaymentezInfoTransactionService {
  private coinpayments: Coinpayments;
  private coinpaymentsCredentials: CoinpaymentsCredentials;
  private COINPAYMENTEZ_KEY: string;
  private COINPAYMENTEZ_SECRET: string;

  constructor(private config: ConfigService
  ) {
    this.COINPAYMENTEZ_KEY = this.config.get<string>('COINPAYMENTEZ_KEY');
    this.COINPAYMENTEZ_SECRET = this.config.get<string>('COINPAYMENTEZ_SECRET');
    this.coinpaymentsCredentials = {
      key: this.COINPAYMENTEZ_KEY,
      secret: this.COINPAYMENTEZ_SECRET
    }
    this.coinpayments = new Coinpayments(this.coinpaymentsCredentials);

  }

  //metodo que crea una transaccion en Coinpaymentez
  async obtenerTransaction(data: CoinpaymentsGetTxOpts): Promise<any> {
    try {

      let transaction: CoinpaymentsGetTxOpts = {
        txid: data.txid,
        full: data.full
      }

      const getTransaction = await this.coinpayments.getTx(
        transaction
      );
      // const getFecha= new Date(getTransaction.time_expires*1000)
      // console.log('getFecha: ', getFecha)

      return getTransaction;

    } catch (error) {
      throw error;
    }
  }

}
