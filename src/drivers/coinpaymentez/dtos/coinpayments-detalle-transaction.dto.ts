import { ApiProperty } from "@nestjs/swagger";

export class CoinpaymentsDetalleTransactionDto {
  @ApiProperty({ type: String })
  readonly txid: string

  @ApiProperty({ type: Number })
  readonly full: number

}
