import { ApiProperty } from "@nestjs/swagger";

export class CoinpaymentsTransactionDto {
  @ApiProperty({type:String})
  readonly currency1: string;

  @ApiProperty({type:String})
  readonly currency2: string;

  @ApiProperty({type:Number})
  readonly amount: number;

  @ApiProperty({type:String})
  readonly buyer_email: string;

  @ApiProperty({type:String})
  readonly address?: string;

  @ApiProperty({type:String})
  readonly buyer_name?: string;

  @ApiProperty({type:String})
  readonly item_name?: string;

  @ApiProperty({type:String})
  readonly item_number?: string;

  @ApiProperty({type:String})
  readonly invoice?: string;

  @ApiProperty({type:String})
  readonly custom?: string;

  @ApiProperty({type:String})
  readonly ipn_url?: string;

  @ApiProperty({type:String})
  readonly success_url?: string;

  @ApiProperty({type:String})
  readonly cancel_url?: string;
}
