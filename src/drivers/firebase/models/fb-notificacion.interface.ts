export interface FBNotificacion {
  tipo?: string;
  idEntidad: string;
  codEntidad: string;
  estado: string;
  accion: string;
  leido: boolean;
  data: any;
  fechaCreacion?: any;
  query: any;
  queryEntidad?: any;
  dispositivos?: any;
}
