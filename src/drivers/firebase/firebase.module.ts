import { Module } from '@nestjs/common';
import { firebaseProvider } from './drivers/firebase.provider';
import { FirebaseMensajesService } from './services/firebase-mensajes.service';
import { FirebaseNotificacionService } from './services/firebase-notificacion.service';
import { FirebaseProyectosService } from './services/firebase-proyectos.service';

@Module({
  providers: [
    ...firebaseProvider,
    FirebaseNotificacionService,
    FirebaseProyectosService,
    FirebaseMensajesService,
  ],
  exports: [
    FirebaseNotificacionService,
    FirebaseProyectosService,
    FirebaseMensajesService,
  ],
})
export class FirebaseModule {}
