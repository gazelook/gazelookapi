import { Inject, Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';

import { FBNotificacion } from '../models/fb-notificacion.interface';
import {
  codigoEntidades,
  estadoNotificacionesFirebase,
} from '../../../shared/enum-sistema';

@Injectable()
export class FirebaseNotificacionService {
  fireDatabase: admin.database.Database;
  notificacion: FBNotificacion;

  constructor(@Inject('FIREBASE_PROVIDER') private readonly firebase) {
    this.fireDatabase = this.firebase.database();
  }

  enviarNotificacion(token, mensaje, opcion) {
    //this.createDataNotificacion(mensaje);
    return admin.messaging().sendToDevice(token, mensaje, opcion);
  }

  async createDataNotificacion(
    data: FBNotificacion,
    nivel: string,
    idEntidadNivel: string,
  ) {
    data.fechaCreacion = admin.database.ServerValue.TIMESTAMP;

    if (data.codEntidad === codigoEntidades.entidadComentarios) {
      // ordena y consulta en toda la lista por el idEntidad y verifica que sea igual al id
      const getNotificacion = await this.fireDatabase
        .ref(`notificaciones/${nivel}/${idEntidadNivel}`)
        .orderByChild('queryEntidad')
        .equalTo(data.data.proyecto._id)
        .get();

      if (getNotificacion.val()) {
        const idNotificacion = Object.keys(getNotificacion.val())[0];

        const notificationRef = this.fireDatabase
          .ref()
          .child(`notificaciones/${nivel}/${idEntidadNivel}/${idNotificacion}`);
        // se modifica solo el estado de dicho documento encontrado
        notificationRef.set(data);
      } else {
        const notificationRef = this.fireDatabase.ref().child('notificaciones');
        notificationRef
          .child(nivel)
          .child(idEntidadNivel)
          .push(data);
      }
    } else if (data.codEntidad === codigoEntidades.comentarioIntercambio) {
      console.log('********************************************************');
      console.log(
        '********************************************************',
        nivel,
      );
      console.log(
        '********************************************************',
        idEntidadNivel,
      );
      // ordena y consulta en toda la lista por el idEntidad y verifica que sea igual al id
      const getNotificacion = await this.fireDatabase
        .ref(`notificaciones/${nivel}/${idEntidadNivel}`)
        .orderByChild('queryEntidad')
        .equalTo(data.data.intercambio._id)
        .get();

      if (getNotificacion.val()) {
        const idNotificacion = Object.keys(getNotificacion.val())[0];

        const notificationRef = this.fireDatabase
          .ref()
          .child(`notificaciones/${nivel}/${idEntidadNivel}/${idNotificacion}`);
        // se modifica solo el estado de dicho documento encontrado
        notificationRef.set(data);
      } else {
        console.log('++++++++++++++++++++++++++++++++++++++++++++++');
        const notificationRef = this.fireDatabase.ref().child('notificaciones');
        notificationRef
          .child(nivel)
          .child(idEntidadNivel)
          .push(data);
      }
    } else {
      const notificationRef = this.fireDatabase.ref().child('notificaciones');
      notificationRef
        .child(nivel)
        .child(idEntidadNivel)
        .push(data);
    }
  }

  createDataConversacion(dataConversacion, idConversacion: string) {
    const conversacionRef = this.fireDatabase.ref().child('conversaciones');
    dataConversacion.fechaCreacion = admin.database.ServerValue.TIMESTAMP;
    conversacionRef.child(idConversacion).set(dataConversacion);
  }

  // registrar tokens a un topic(perfil)
  addTopic(idPerfil, tokens) {
    admin
      .messaging()
      .subscribeToTopic(tokens, idPerfil)
      .then(res => {
        console.log('token suscrito', res);
      })
      .catch(error => {
        console.log('error al suscribir el token');
      });
  }

  createDataComenatario(data: any, idProyecto: string) {
    data.fechaCreacionFirebase = admin.database.ServerValue.TIMESTAMP;
    data.idProyecto = idProyecto;
    // console.log('fechaCreacionFirebase: ', data.comentario.fechaCreacionFirebase);

    const comentario = this.fireDatabase.ref().child('comentarios');
    comentario
      .child(idProyecto)
      .child(data._id)
      .set(data);
  }

  createDataComenatarioIntercambio(data: any, idIntercambio: string) {
    data.fechaCreacionFirebase = admin.database.ServerValue.TIMESTAMP;
    // console.log('fechaCreacionFirebase: ', data.comentario.fechaCreacionFirebase);

    const comentario = this.fireDatabase.ref().child('comentariosIntercambio');
    comentario
      .child(idIntercambio)
      .child(data._id)
      .set(data);
  }

  async eliminarNotificacion(
    nivel: string,
    idEntidadNivel,
    codEntidad: string,
    idEntidad: string,
  ) {
    // ordena y consulta en toda la lista por el idEntidad y verifica que sea igual al id
    const getNotificacion = await this.fireDatabase
      .ref(`notificaciones/${nivel}/${idEntidadNivel}`)
      .orderByChild('idEntidad')
      .equalTo(idEntidad)
      .get();

    console.log('getNotificacion', getNotificacion);
    console.log('getNotificacion.key', getNotificacion.key);
    console.log('getNotificacion.val', getNotificacion.val());

    const dataNotificacion = getNotificacion.val();

    if (dataNotificacion) {
      const idNotificacion = Object.keys(dataNotificacion)[0];
      console.log('idNotificacion', idNotificacion);

      const notificationRef = this.fireDatabase
        .ref()
        .child(`notificaciones/${nivel}/${idEntidadNivel}/${idNotificacion}`);
      // se modifica solo el estado de dicho documento encontrado
      notificationRef
        .child('estado')
        .set(estadoNotificacionesFirebase.eliminado);
    }
  }
}
