import { Inject, Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';
import { codigoEstadosComentario } from '../../../shared/enum-sistema';

@Injectable()
export class FirebaseProyectosService {
  fireDatabase: admin.database.Database;

  constructor(@Inject('FIREBASE_PROVIDER') private readonly firebase) {
    this.fireDatabase = admin.database();
  }

  // elimina completamente comentarios que ha hecho el usuario en otros proyectos en firebase
  async eliminarComentario(idProyecto: string, idComentario: string) {
    // ordena y consulta en toda la lista por el idEntidad y verifica que sea igual al id
    const getComentario = await this.fireDatabase
      .ref(`comentarios/${idProyecto}/${idComentario}`)
      .get();

    /* console.log("getComentario", getComentario);
        console.log("getComentario.key", getComentario.key);
        console.log("getComentario.val", getComentario.val()); */

    const dataComentario = getComentario.val();

    if (dataComentario) {
      // eliminar comentario
      await this.fireDatabase
        .ref(`comentarios/${idProyecto}/${idComentario}`)
        .set(null);
    }
  }

  // Elimina completamente todos los comentarios del proyecto
  async eliminarComentariosProyecto(idProyecto: string) {
    // ordena y consulta en toda la lista por el idEntidad y verifica que sea igual al id
    const getComentario = await this.fireDatabase
      .ref(`comentarios/${idProyecto}`)
      .get();

    /* console.log("getComentario", getComentario);
        console.log("getComentario.key", getComentario.key);
        console.log("getComentario.val", getComentario.val()); */

    const dataComentario = getComentario.val();

    if (dataComentario) {
      // eliminar comentarios del proyecto
      await this.fireDatabase.ref(`comentarios/${idProyecto}`).set(null);
    }
  }

  // Elimina Logicamente comentarios que ha hecho el usuario en otros proyectos en firebase
  async eliminarComentarioEST(idProyecto: string, idComentario: string) {
    // ordena y consulta en toda la lista por el idEntidad y verifica que sea igual al id
    const getComentario = await this.fireDatabase
      .ref(`comentarios/${idProyecto}/${idComentario}`)
      .get();

    /* console.log("getComentario1", getComentario);
        console.log("getComentario.key1", getComentario.key);
        console.log("getComentario.val1", getComentario.val()); */

    const dataComentario = getComentario.val();

    if (dataComentario) {
      dataComentario.estado.codigo = codigoEstadosComentario.eliminado;
      // eliminar comentario
      await this.fireDatabase
        .ref(`comentarios/${idProyecto}/${idComentario}`)
        .set(dataComentario);
    }
  }

  // Elimina logicamente todos los comentarios del proyecto
  async eliminarComentariosProyectoEST(idProyecto: string) {
    let listComentarios = [];

    await this.fireDatabase
      .ref(`comentarios/${idProyecto}`)
      .once('value', function(snapshot) {
        snapshot.forEach(function(ChildSnapshot) {
          listComentarios = [...listComentarios, ChildSnapshot.val()];
        });
      });

    for (const comentario of listComentarios) {
      //console.log("comentario: ", comentario);
      if (comentario) {
        let dataComentario = comentario;
        dataComentario.estado.codigo = codigoEstadosComentario.eliminado;
        // eliminar comentario
        await this.fireDatabase
          .ref(`comentarios/${idProyecto}/${comentario._id}`)
          .set(dataComentario);
      }
    }
  }
}
