import { Inject, Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import * as admin from 'firebase-admin';
import {
  catalogoTipoMensaje,
  codigoEstadosComentario,
  codigosEstadosMensajes,
} from '../../../shared/enum-sistema';

@Injectable()
export class FirebaseMensajesService {
  fireDatabase: admin.database.Database;

  constructor(@Inject('FIREBASE_PROVIDER') private readonly firebase) {
    this.fireDatabase = admin.database();
  }

  // Obtener mensajes por tipo
  async obtenerMensajeTipo(fechaActual) {
    console.log('fechaActual: ', fechaActual);
    let listTransferencias = [];

    await this.fireDatabase
      .ref(`transferencias-activas`)
      .once('value', function(snapshot) {
        snapshot.forEach(function(ChildSnapshot) {
          //Si la transferencia esta activa (true)
          if (ChildSnapshot.val().activa) {
            if (ChildSnapshot.val().fechaCreacion != undefined) {
              const fechaCreacion = new Date(ChildSnapshot.val().fechaCreacion);
              fechaCreacion.setDate(new Date(fechaCreacion).getDate() + 5);

              //Si una vez creada la transferencia y pasan 5 dias se debe eliminar el mensaje
              //y actualizar el estado de la transferencia-activa a false
              if (fechaActual > fechaCreacion) {
                listTransferencias = [
                  ...listTransferencias,
                  ChildSnapshot.val(),
                ];
              }
            }
          }
        });
      });

    for (const transferencia of listTransferencias) {
      if (transferencia) {
        let conversacion = transferencia.conversacion;
        let mensaje = transferencia.mensaje;

        if (conversacion && mensaje) {
          const getMensaje = await this.fireDatabase
            .ref(`mensajes/${conversacion}/${mensaje}`)
            .get();
          let dataMensaje = getMensaje.val();
          dataMensaje.estado.codigo = codigosEstadosMensajes.eliminado;

          // eliminar mensaje
          await this.fireDatabase
            .ref(`mensajes/${conversacion}/${mensaje}`)
            .set(dataMensaje);
        }
      }
    }
  }
}
