import * as mongoose from 'mongoose';

import { ConfigService } from 'src/config/config.service';

export const dbProviders = [
  {
    provide: 'DB_CONNECTION',
    inject: [ConfigService],

    useFactory: async (config: ConfigService): Promise<typeof mongoose> =>
      await mongoose.connect(config.get('MONGO_URI'), {
        useFindAndModify: false,
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
      }),
  },
];
