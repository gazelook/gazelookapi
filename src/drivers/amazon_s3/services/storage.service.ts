import { S3 } from 'aws-sdk';
import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { extname } from 'path';
import { ConfigService } from '../../../config/config.service';
import { StorageArchivoDto } from '../dtos/storage-archivo.dto';

@Injectable()
export class StorageService {
  private FOLDER;
  private FOLDER_FILE_DEFAULT;
  private URL_BASE;
  private PATH_BASE;
  private BUCKET;
  private ACCESS_KEY_ID: string;
  private SECRET_ACCESS_KEY: string;
  private URL_CLOUDFRONT: string;

  private readonly s3 = new S3();

  constructor(private config: ConfigService) {
    this.ACCESS_KEY_ID = this.config.get<string>('S3_ACCESS_KEY');
    this.SECRET_ACCESS_KEY = this.config.get<string>('S3_SECTRET_ACCESS_KEY');
    this.URL_CLOUDFRONT = this.config.get<string>('URL_CLOUDFRONT');
    this.BUCKET = config.get<string>('S3_BUCKET');
    this.FOLDER = config.get<string>('S3_FOLDER');
    this.FOLDER_FILE_DEFAULT = config.get<string>('S3_FOLDER_FILE_DEFAULT');

    this.s3 = new S3({
      accessKeyId: this.ACCESS_KEY_ID,
      secretAccessKey: this.SECRET_ACCESS_KEY,
    });

    this.URL_BASE = `${this.URL_CLOUDFRONT}/${this.FOLDER}`;
    this.PATH_BASE = `s3://${this.BUCKET}/${this.FOLDER}`;
  }

  async cargarArchivo(
    archivo: Express.Multer.File,
    fileDafault?: boolean,
  ): Promise<StorageArchivoDto> {
    if (fileDafault) {
      this.FOLDER = this.FOLDER_FILE_DEFAULT;

      this.URL_BASE = `${this.URL_CLOUDFRONT}/${this.FOLDER}`;
      this.PATH_BASE = `s3://${this.BUCKET}/${this.FOLDER}`;
    }

    const filename = `${uuidv4().toString()}${extname(archivo.originalname)}`;
    const bucket = {
      Bucket: `${this.BUCKET}/${this.FOLDER}`,
      Key: filename,
      Body: archivo.buffer,
      ContentType: archivo.mimetype,
    };

    try {
      const result = await this.s3.putObject(bucket).promise();

      const data: StorageArchivoDto = {
        url: `${this.URL_BASE}/${filename}`,
        etag: result.ETag,
        filename: filename,
        path: `${this.PATH_BASE}/${filename}`,
        size: archivo.size,
      };

      return data;
    } catch (err) {
      throw err;
    }
  }

  async getArchivo(filename: string): Promise<S3.GetObjectOutput> {
    const bucket = {
      Bucket: `${this.BUCKET}/${this.FOLDER}`,
      Key: `${filename}`,
    };

    try {
      const file = await this.s3.getObject(bucket).promise();

      return file;
    } catch (err) {
      throw err;
    }
  }

  async eliminarArchivo(filename: string) {
    const bucket = {
      Bucket: `${this.BUCKET}/${this.FOLDER}`,
      Key: filename,
    };

    try {
      const result = await this.s3.deleteObject(bucket).promise();
      return result;
    } catch (err) {
      throw err;
    }
  }

  async verificarArchivoStorage(filename: string) {
    const bucket = {
      Bucket: `${this.BUCKET}/${this.FOLDER}`,
      Key: filename,
    };

    try {
      const result = await this.s3.headObject(bucket).promise();
      return result;
    } catch (err) {
      //throw err;
      console.log('Error, el archivo no existe en el Storage: ', err);
      return false;
    }
  }
}
