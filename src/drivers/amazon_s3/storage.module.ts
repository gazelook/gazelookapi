import { Module } from '@nestjs/common';
import { StorageDocumentosUsuarioService } from './services/storage-documentos-usuario.service';
import { StorageService } from './services/storage.service';

@Module({
  providers: [StorageService, StorageDocumentosUsuarioService],
  exports: [StorageService, StorageDocumentosUsuarioService],
})
export class StorageModule {}
