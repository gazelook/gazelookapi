export class StorageArchivoDto {
  url: string;
  etag: string;
  filename: string;
  path: string;
  size: number;
}
