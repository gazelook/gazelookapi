import { Injectable } from '@nestjs/common';

import Stripe from 'stripe';
import { ConfigService } from '../../../config/config.service';
import { pagoStripe } from '../../../shared/enum-sistema';
@Injectable()
export class CustomersService {
  private readonly stripe: Stripe;
  private readonly STRIPE_KEY: string;

  constructor(private config: ConfigService) {
    this.STRIPE_KEY = this.config.get<string>('STRIPE_KEY');
    this.stripe = new Stripe(this.STRIPE_KEY, {
      apiVersion: pagoStripe.apiVersion,
    });
  }

  async findAllCustomers(): Promise<Stripe.ApiList<Stripe.Customer>> {
    try {
      const customers = await this.stripe.customers.list();
      return customers;
    } catch (error) {
      throw error;
    }
  }

  async findCustomer(
    id: string,
  ): Promise<Stripe.Customer | Stripe.DeletedCustomer> {
    try {
      const customer = await this.stripe.customers.retrieve(id);
      return await customer;
    } catch (error) {
      throw error;
    }
  }

  async createCustomer(customerInfo: any): Promise<Stripe.Customer> {
    try {
      const customer = await this.stripe.customers.create(customerInfo);
      return customer;
    } catch (error) {
      throw error;
    }
  }

  async updateCustomer(
    id: string,
    customerInfo: any,
  ): Promise<Stripe.Customer> {
    try {
      const customer = await this.stripe.customers.update(id, customerInfo);
      return customer;
    } catch (error) {
      throw error;
    }
  }

  async deleteCustomer(id: string): Promise<Stripe.DeletedCustomer> {
    try {
      const deletetionConfirmation = await this.stripe.customers.del(id);
      return deletetionConfirmation;
    } catch (error) {
      throw error;
    }
  }
}
