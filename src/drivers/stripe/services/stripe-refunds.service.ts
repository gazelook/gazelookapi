import { Injectable } from '@nestjs/common';
import Stripe from 'stripe';
import { ConfigService } from '../../../config/config.service';
import { pagoStripe } from '../../../shared/enum-sistema';

@Injectable()
export class StripeRefundsService {
  private readonly stripe: Stripe;
  private readonly STRIPE_KEY: string;

  constructor(private config: ConfigService) {
    this.STRIPE_KEY = this.config.get<string>('STRIPE_KEY');
    this.stripe = new Stripe(this.STRIPE_KEY, {
      apiVersion: pagoStripe.apiVersion,
    });
  }

  async createRefund(idPaymentIntent: string): Promise<Stripe.Refund> {
    try {
      const paramsRefund = {
        payment_intent: idPaymentIntent,
      };
      const createRefund = await this.stripe.refunds.create(paramsRefund);

      return createRefund;
    } catch (error) {
      throw error;
    }
  }
  async getRefundById(idRefund: string): Promise<Stripe.Refund> {
    try {
      const refund = await this.stripe.refunds.retrieve(idRefund);

      return refund;
    } catch (error) {
      throw error;
    }
  }

  async listRefunds(
    fechaInicio: number,
    fechaFin: number,
  ): Promise<Stripe.ApiList<Stripe.Refund>> {
    try {
      const listRefund = await this.stripe.refunds.list({
        created: { gte: fechaInicio, lte: fechaFin },
      });

      return listRefund;
    } catch (error) {
      throw error;
    }
  }
}
