import { Injectable } from '@nestjs/common';
import Stripe from 'stripe';
import { ConfigService } from '../../../config/config.service';
import { pagoStripe } from '../../../shared/enum-sistema';
import { PaymentMethodsDto } from '../dtos/payment-methods.dto';

@Injectable()
export class PaymentMethodsService {
  private readonly stripe: Stripe;
  private readonly STRIPE_KEY: string;

  constructor(private config: ConfigService) {
    this.STRIPE_KEY = this.config.get<string>('STRIPE_KEY');
    this.stripe = new Stripe(this.STRIPE_KEY, {
      apiVersion: pagoStripe.apiVersion,
    });
  }

  async createPaymentMethods(
    dataPayMethod: PaymentMethodsDto,
  ): Promise<Stripe.PaymentMethod> {
    try {
      const payMethod = await this.stripe.paymentMethods.create({
        type: 'card',
        card: {
          cvc: dataPayMethod.cvc,
          number: dataPayMethod.cardNumber,
          exp_month: dataPayMethod.expMonth,
          exp_year: dataPayMethod.expYear,
        },
      });

      return payMethod;
    } catch (error) {
      throw error;
    }
  }

  async getPaymentMethod(idPaymentMethod): Promise<Stripe.PaymentMethod> {
    try {
      const payMethod = await this.stripe.paymentMethods.retrieve(
        idPaymentMethod,
      );

      return payMethod;
    } catch (error) {
      throw error;
    }
  }
}
