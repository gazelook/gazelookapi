import { Body, Controller, Get, Module, Param, Post } from '@nestjs/common';
import { PaymentIntentsDto } from '../dtos/payment-intents.dto';
import { PaymentIntentResponse } from '../models/payment-intents.interface';
import { PaymentIntentsService } from '../services/payment-intents.service';

@Controller('payment_intents')
export class PaymentIntentsController {
  constructor(private readonly paymentIntentsService: PaymentIntentsService) {}

  @Post('/')
  async createPaymentIntents(
    @Body() paymentIntentDto: PaymentIntentsDto,
  ): Promise<any> {
    return this.paymentIntentsService.createPaymentsIntent(paymentIntentDto);
  }

  @Post(':id/confirm')
  async confirmPaymentIntents(
    @Param('id') paymentIntentID: string,
  ): Promise<PaymentIntentResponse> {
    return this.paymentIntentsService.confirmPaymentsIntent(paymentIntentID);
  }

  @Get(':id')
  async retrievePaymentIntents(
    @Param('id') paymentIntentID: string,
  ): Promise<PaymentIntentResponse> {
    return this.paymentIntentsService.retrievePaymentIntents(paymentIntentID);
  }
}

@Module({
  controllers: [PaymentIntentsController],
  providers: [PaymentIntentsService],
})
export class PaymentIntentsModule {}
