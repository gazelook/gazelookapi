import { Body, Controller, Module, Post } from '@nestjs/common';
import { PaymentMethodsDto } from '../dtos/payment-methods.dto';
import { PaymentMethodsService } from '../services/payment-methods.service';

@Controller('payment_methods')
export class PaymentMethodsController {
  constructor(private readonly paymentMethodsService: PaymentMethodsService) {}
  @Post()
  async createPaymentMethods(
    @Body() paymentMethodsDto: PaymentMethodsDto,
  ): Promise<any> {
    return this.paymentMethodsService.createPaymentMethods(paymentMethodsDto);
  }
}

@Module({
  controllers: [PaymentMethodsController],
  providers: [PaymentMethodsService],
})
export class PaymentMethodsModule {}
