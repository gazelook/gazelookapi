import * as stripe from 'stripe';

// tslint:disable-next-line: no-namespace
export namespace Stripe {
  export type PaymentIntents = stripe.Stripe.PaymentIntentsResource; //stripe.paymentIntents.IPaymentIntent;
  export type PaymentMethods = stripe.Stripe.PaymentMethodsResource; //  stripe.paymentMethods.IPaymentMethod;
}
