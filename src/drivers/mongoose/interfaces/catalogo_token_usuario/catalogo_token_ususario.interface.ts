import { Document } from 'mongoose';

export interface CatalogoTokenUsuarioLocalidad extends Document {
  readonly codigoTipo: string;
  readonly nombre: string;
  readonly estado: string;
  readonly descripcion: string;
}
