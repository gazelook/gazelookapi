import { Document } from 'mongoose';

export interface EstiloAnuncio extends Document {
  readonly media: string;
  readonly color: string;
  readonly tamanioLetra: string;
  readonly tipo: string;
  readonly estado: string;
}
