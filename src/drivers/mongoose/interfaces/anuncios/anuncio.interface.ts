import { Document } from 'mongoose';

export interface Anuncio extends Document {
  readonly codigo: string;
  readonly perfil: string;
  readonly tipo: string;
  readonly traducciones: Array<string>;
  readonly tematica: Array<string>;
  readonly estado: string;
  readonly activo: boolean;
  readonly favorito: boolean;
  readonly configuraciones: Array<string>;
  readonly orden: number;
  readonly intervaloVisualizacion: number;
  readonly tiempoVisualizacion: number;
  readonly fechaInicio: Date;
  readonly fechaFin: Date;
  readonly horaInicio: Date;
  readonly horaFin: Date;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
