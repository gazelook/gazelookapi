import { Document } from 'mongoose';

export interface CatalogoTipoColores extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly formula: string;
  readonly estado: string;
}
