import { Document } from 'mongoose';

export interface Beneficiario extends Document {
  readonly tipo: string;
  readonly usuario: number;
  readonly proyecto: number;
  readonly estado: string;
}
