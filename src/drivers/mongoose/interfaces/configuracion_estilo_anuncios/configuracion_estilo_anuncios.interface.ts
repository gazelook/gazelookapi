import { Document } from 'mongoose';

export interface ConfiguracionEstiloAnuncios extends Document {
  readonly estado: string;
  readonly estilos: Array<string>;
  readonly tipo: string;
}
