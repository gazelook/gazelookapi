import { Document } from 'mongoose';

export interface DocumentoUsuario extends Document {
  readonly usuario: string;
  readonly estado: string;
  readonly tipo: string;
  readonly archivo: string;
}
