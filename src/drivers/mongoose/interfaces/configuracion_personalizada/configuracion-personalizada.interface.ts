import { Document } from 'mongoose';

export interface ConfiguracionPredeterminada extends Document {
  readonly configuracion: string;
}
