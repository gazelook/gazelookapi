import { Document } from 'mongoose';

export interface TokenInterface extends Document {
  readonly emailUsuario: string;
  readonly usuario: string;
  readonly token: string;
  readonly tokenRefresh: string;
  readonly ultimoAcceso: Date;
}
