import { Document } from 'mongoose';

export interface Asociacion extends Document {
  readonly participantes: Array<string>;
  estado: string;
  tipo: any;
  readonly foto: string;
  readonly nombre: string;
  readonly conversacion: string;
  readonly privado: boolean;
}
