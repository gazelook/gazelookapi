import { Document } from 'mongoose';

export interface Participante extends Document {
  readonly estado: string;
  readonly invitadoPor: string;
  readonly perfil: string;
  readonly asociacion: string;
  readonly sobrenombre: string;
  readonly roles: Array<string>;
  readonly configuraciones: Array<string>;
  readonly contactoDe: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
