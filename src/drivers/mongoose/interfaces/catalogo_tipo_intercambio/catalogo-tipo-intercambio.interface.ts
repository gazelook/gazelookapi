import { Document } from 'mongoose';

export interface CatalogoTipoIntercambio extends Document {
  readonly codigo: string;
  readonly traducciones: Array<string>;
  readonly estado: string;
}
