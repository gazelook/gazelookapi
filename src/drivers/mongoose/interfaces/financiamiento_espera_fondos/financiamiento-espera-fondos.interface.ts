import { Document } from 'mongoose';

export interface FinanciamientoEsperaFondos extends Document {
  readonly catalogoPorcentajeEsperaFondos: string;
  readonly montoIngreso: number;
  readonly montoTotal: number;
  readonly montoFinal: number;
  readonly referencia: string;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
