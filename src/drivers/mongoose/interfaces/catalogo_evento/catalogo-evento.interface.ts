import { Document } from 'mongoose';

export interface CatalogoEvento extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly configuraciones: Array<string>;
  readonly formulas: Array<string>;
  readonly estado: string;
}
