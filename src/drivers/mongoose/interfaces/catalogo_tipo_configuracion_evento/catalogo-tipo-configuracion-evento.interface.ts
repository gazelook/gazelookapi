import { Document } from 'mongoose';

export interface CatalogoTipoConfiguracionEvento extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
}
