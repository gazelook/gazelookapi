import { Document } from 'mongoose';

export interface CatalogoTipoMensaje extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly descripcion: string;
  readonly estado: string;
}
