import { Document } from 'mongoose';

export interface ConfiguracionPreestrategia extends Document {
  readonly orden: number;
  readonly semanas: number;
  readonly configuracion_evento: string;
  readonly estado: string;
  readonly codigo: string;
}
