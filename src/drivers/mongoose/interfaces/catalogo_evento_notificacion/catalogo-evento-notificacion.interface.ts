import { Document } from 'mongoose';

export interface CatalogoEventoNotificacion extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly descripcion: string;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
