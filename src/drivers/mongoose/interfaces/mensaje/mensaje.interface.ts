import { Document } from 'mongoose';

export interface Mensaje extends Document {
  estado: any;
  tipo: any;
  estatus: any;
  readonly traducciones: Array<string>;
  readonly importante: Boolean;
  readonly adjuntos: Array<string>;
  readonly conversacion: string;
  readonly propietario: string;
  readonly entregadoA: Array<string>;
  readonly leidoPor: Array<string>;
  readonly eliminadoPor: Array<string>;
  readonly identificadorTemporal: number;
  readonly fechaCreacion: Date;
  referenciaEntidadCompartida: string;
  entidadCompartida: any;
}
