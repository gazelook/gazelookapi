import { Document } from 'mongoose';

export interface Estilo extends Document {
  readonly codigo: string;
  readonly media: string;
  readonly color: string;
  readonly tipo: string;
  readonly estado: string;
}
