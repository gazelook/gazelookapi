import { Document } from 'mongoose';

export interface ComentariosIntercambio extends Document {
  readonly estado: string;
  readonly adjuntos: Array<string>;
  readonly coautor: string;
  readonly importante: string;
  readonly traducciones: Array<string>;
  readonly tipo: string;
  readonly intercambio: string;
  readonly idPerfilRespuesta: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
  readonly fechaCreacionFirebase: string;
}
