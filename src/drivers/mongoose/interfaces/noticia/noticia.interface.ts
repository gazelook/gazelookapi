import { Document } from 'mongoose';

export interface Noticia extends Document {
  readonly autor: string;
  readonly adjuntos: Array<string>;
  readonly perfil: string;
  readonly votos: Array<string>;
  readonly totalVotos: number;
  readonly traducciones: Array<string>;
  readonly direccion: string;
  readonly estado: string;
  readonly medias: Array<string>;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
