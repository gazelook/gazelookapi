import { Document } from 'mongoose';

export interface TraduccionComentario extends Document {
  readonly texto: string;
  readonly idioma: string;
  readonly original: boolean;
  readonly referencia: string;
}
