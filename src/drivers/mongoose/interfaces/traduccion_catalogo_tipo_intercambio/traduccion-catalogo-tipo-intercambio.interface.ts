import { Document } from 'mongoose';

export interface TraduccionCatalogoTipoIntercambio extends Document {
  readonly catalogoTipoIntercambio: string;
  readonly nombre: string;
  readonly descripcion: string;
  readonly idioma: string;
  readonly original: boolean;
  readonly estado: string;
}
