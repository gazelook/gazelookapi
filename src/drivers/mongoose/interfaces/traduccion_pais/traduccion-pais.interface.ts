import { Document } from 'mongoose';

export interface TraduccionPais extends Document {
  readonly nombre: string;
  readonly idioma: string;
  readonly original: boolean;
  readonly pais: string;
}
