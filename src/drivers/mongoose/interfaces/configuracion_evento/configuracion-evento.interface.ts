import { Document } from 'mongoose';

export interface ConfiguracionEvento extends Document {
  readonly codigo: string;
  readonly intervalo: number;
  readonly duracion: number;
  readonly ciclico: boolean;
  readonly formulas: Array<string>;
  readonly fechaInicio: Date;
  readonly fechaProximo: Date;
  readonly catalogoEvento: Array<string>;
  readonly estado: string;
  readonly eventos: Array<string>;
}
