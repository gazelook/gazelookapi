import { Document } from 'mongoose';

export interface CatalogoRepartirFondos extends Document {
  readonly codigo: string;
  readonly porcentaje: number;
  readonly numeroEjecuciones: number;
  readonly estado: string;
  readonly fechaCreaacion: Date;
  readonly fechaActualizacion: Date;
}
