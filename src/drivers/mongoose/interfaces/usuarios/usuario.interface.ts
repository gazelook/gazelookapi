import { Document } from 'mongoose';
import { Direccion } from '../direccion/direccion.interface';
import { Dispositivo } from '../dispositivo/dispositivo.interface';
import { Transaccion } from '../transaccion/transaccion.interface';

export interface Usuario extends Document {
  readonly email: string;
  readonly contrasena: string;
  readonly codigo: string;
  readonly fechaNacimiento: Date;
  readonly aceptoTerminosCondiciones: boolean;
  readonly perfilGrupo: boolean;
  readonly emailVerificado: boolean;
  readonly menorEdad: boolean;
  readonly emailResponsable: string;
  readonly responsableVerificado: boolean;
  readonly nombreResponsable: string;
  readonly direccionDomiciliaria: String;
  readonly documentoIdentidad: String;
  readonly idioma: string;
  readonly estado: string;
  readonly perfiles: Array<any>;
  readonly transacciones: Array<Transaccion>;
  readonly suscripciones: Array<string>;
  dispositivos: Array<Dispositivo>;
  readonly rolSistema: Array<string>;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
  readonly direccion: Direccion;
  readonly cuentaRenovada: boolean;
  readonly documentosUsuario: Array<string>;
}
