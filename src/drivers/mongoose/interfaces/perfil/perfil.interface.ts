import { Document } from 'mongoose';

export interface Perfil extends Document {
  readonly tipoPerfil: string;
  readonly usuario: string;
  readonly album: Array<string>;
  readonly nombreContacto: string;
  readonly nombreContactoTraducido: string;
  readonly nombre: string;
  readonly estado: string;
  readonly direcciones: Array<string>;
  readonly telefonos: Array<string>;
  readonly proyectos: Array<string>;
  readonly pensamientos: Array<string>;
  readonly noticias: Array<string>;
  readonly asociaciones: Array<string>;
}
