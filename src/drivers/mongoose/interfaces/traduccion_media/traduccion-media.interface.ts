import { Document } from 'mongoose';

export interface TraduccionMedia extends Document {
  readonly descripcion: string;
  readonly idioma: string;
  readonly original: boolean;
  readonly estado: string;
  readonly media: string;
}
