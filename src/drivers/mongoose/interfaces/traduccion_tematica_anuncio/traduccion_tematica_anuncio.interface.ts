import { Document } from 'mongoose';

export interface TraduccionTematicaAnuncio extends Document {
  readonly titulo: string;
  readonly idioma: string;
  readonly original: boolean;
  readonly estado: string;
  readonly anuncio: string;
}
