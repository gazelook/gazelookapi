import { Document } from 'mongoose';

export interface CatalogoTipoBalance extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
  readonly descripcion: string;
}
