import { Document } from 'mongoose';

export interface VotoNoticia extends Document {
  readonly estado: string;
  readonly perfil: string;
  readonly noticia: string;
  readonly traducciones: Array<string>;
}
