import { Document } from 'mongoose';

export interface DocumentosLegales extends Document {
  readonly estado: string;
  readonly origen: string;
  readonly version: number;
  readonly usuario: string;
  readonly adjuntos: Array<string>;
}
