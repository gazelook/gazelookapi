import { Document } from 'mongoose';

export interface TraduccionCatalogoTipoProyecto extends Document {
  readonly referencia: string;
  readonly nombre: string;
  readonly descripcion: string;
  readonly idioma: string;
  readonly original: boolean;
  readonly estado: string;
}
