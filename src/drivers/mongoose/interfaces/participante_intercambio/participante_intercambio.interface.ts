import { Document } from 'mongoose';

export interface ParticipanteIntercambio extends Document {
  readonly coautor: string;
  readonly intercambio: string;
  readonly roles: Array<string>;
  readonly estado: string;
  readonly totalComentarios: number;
  readonly configuraciones: Array<string>;
  readonly comentarios: Array<string>;
}
