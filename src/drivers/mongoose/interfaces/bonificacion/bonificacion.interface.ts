import { Document } from 'mongoose';

export interface Bonificacion extends Document {
  readonly proyecto: string;
  readonly montoCubierto: number;
  readonly montoFaltante: number;
  readonly fondosFinanciamiento: number;
  readonly fondosReservados: string;
  readonly esperaFondos: string;
  readonly transacciones: Array<string>;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
