import { Document } from 'mongoose';

export interface Conversacion extends Document {
  readonly asociacion: string;
  readonly estado: string;
  readonly ultimoMensaje: string;
  readonly listaUltimoMensaje: Array<string>;
}
