import { Document } from 'mongoose';

export interface ProyectoEjecucion extends Document {
  readonly proyecto: string;
  readonly traducciones: Array<string>;
  readonly estado: string;
}
