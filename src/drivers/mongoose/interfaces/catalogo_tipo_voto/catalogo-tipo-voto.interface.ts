import { Document } from 'mongoose';

export interface CatalogoTipoVoto extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly votoAdmin: boolean;
  readonly estado: string;
}
