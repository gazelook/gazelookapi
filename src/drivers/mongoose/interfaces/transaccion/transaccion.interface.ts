import { Document } from 'mongoose';
import { InformacionPago } from '../informacion_pago/informacion-pago.interface';

export interface Transaccion extends Document {
  estado: string;
  monto: number;
  moneda: string;
  descripcion: string;
  origen: string;
  balance: Array<any>;
  beneficiario: string;
  metodoPago: string;
  informacionPago: InformacionPago;
  usuario: string;
  conversionTransaccion: Array<any>;
  destino: string;
  numeroRecibo: string;
  comisionTransferencia: number;
  totalRecibido: number;
  origenPais: string;
  fechaCreacion: Date;
  fechaActualizacion: Date;
}
