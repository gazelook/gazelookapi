import { Document } from 'mongoose';

export interface ConfiguracionPersonalizada extends Document {
  readonly codigo: String;
  readonly entidad: String;
  readonly silenciada: Boolean;
  readonly tonoNotificacion: String;
  readonly estilos: Array<string>;
  readonly tipo: String;
  readonly estado: String;
}
