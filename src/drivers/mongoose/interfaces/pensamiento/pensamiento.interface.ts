import { Document } from 'mongoose';

export interface Pensamiento extends Document {
  readonly perfil: string;
  readonly traducciones: Array<string>;
  readonly publico: boolean;
  readonly estado: string;
  readonly fechaActualizacion: Date;
}
