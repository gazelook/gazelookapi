import { Document } from 'mongoose';

export interface ConfiguracionBalance extends Document {
  readonly orden: number;
  readonly semanas: number;
  readonly configuracion_evento: string;
  readonly estado: string;
  readonly codigo: string;
}
