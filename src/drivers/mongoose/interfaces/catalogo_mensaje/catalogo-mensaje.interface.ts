import { Document } from 'mongoose';

export interface CatalogoMensaje extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly descripcion: string;
  readonly estado: string;
}
