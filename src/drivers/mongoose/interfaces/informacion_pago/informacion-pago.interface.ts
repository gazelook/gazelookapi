import { Document } from 'mongoose';

export interface InformacionPago extends Document {
  idPago: string;
  datos: object;
  fechaCreacion: Date;
  fechaActualizacion: Date;
}
