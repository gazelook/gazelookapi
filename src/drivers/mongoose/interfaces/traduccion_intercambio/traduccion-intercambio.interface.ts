import { Document } from 'mongoose';

export interface TraduccionIntercambio extends Document {
  readonly titulo: string;
  readonly tituloCorto: string;
  readonly descripcion: string;
  readonly idioma: string;
  readonly original: Boolean;
  readonly estado: string;
  readonly intercambio: string;
}
