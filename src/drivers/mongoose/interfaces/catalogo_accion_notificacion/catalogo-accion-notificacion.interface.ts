import { Document } from 'mongoose';

export interface CatalogoAccionNotificacion extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
