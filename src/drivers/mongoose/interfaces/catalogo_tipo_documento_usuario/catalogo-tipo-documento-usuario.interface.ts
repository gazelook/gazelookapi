import { Document } from 'mongoose';

export interface CatalogoTipoDocumentoUsuario extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
}
