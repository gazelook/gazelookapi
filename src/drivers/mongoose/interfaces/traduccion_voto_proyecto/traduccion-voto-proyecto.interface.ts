import { Document } from 'mongoose';

export interface TraduccionesVotoProyecto extends Document {
  readonly descripcion: string;
  readonly idioma: string;
  readonly estado: string;
  readonly original: boolean;
  readonly votoProyecto: string;
}
