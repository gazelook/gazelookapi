import { Document } from 'mongoose';

export interface CatalogoIdiomas extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly codNombre: string;
  readonly idiomaSistema: boolean;
  readonly estado: string;
}

export class CatalogoIdioma {
  codigo: string;
  nombre: string;
  codNombre: string;
  idiomaSistema: boolean;
  estado: string;
}
