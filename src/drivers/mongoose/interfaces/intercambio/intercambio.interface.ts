import { Document } from 'mongoose';

export interface Intercambio extends Document {
  readonly perfil: string;
  readonly tipo: string;
  readonly tipoIntercambiar: string;
  readonly direccion: string;
  readonly adjuntos: Array<string>;
  readonly traducciones: Array<string>;
  readonly participantes: Array<string>;
  readonly comentarios: Array<string>;
  readonly status: string;
  readonly email: string;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
