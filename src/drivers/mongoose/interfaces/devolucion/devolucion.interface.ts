import { Document } from 'mongoose';

export interface Devolucion extends Document {
  readonly usuario: string;
  readonly transaccion: Array<string>;
  readonly montoTotal: number;
  readonly idPago: string;
  readonly idRefund: string;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
