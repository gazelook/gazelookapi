import { Document } from 'mongoose';

export interface ConversionTransaccion extends Document {
  moneda: string;
  monto: number;
  principal: boolean;
  comisionTransferencia: number;
  totalRecibido: number;
  transaccion: string;
}
