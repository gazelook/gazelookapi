import { Document } from 'mongoose';

export interface CatalogoPorcentajeFondosReservados extends Document {
  readonly codigo: string;
  readonly porcentaje: number;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
