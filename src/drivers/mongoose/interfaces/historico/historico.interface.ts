import { Document } from 'mongoose';

export interface Historico extends Document {
  readonly datos: object;
  readonly usuario: string;
  readonly accion: string;
  readonly entidad: string;
  //readonly registro: string
  readonly fechaCreacion: Date;
  readonly tipo: string;
}
