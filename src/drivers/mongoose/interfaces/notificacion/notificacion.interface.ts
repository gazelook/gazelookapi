import { Document } from 'mongoose';

export interface Notificacion extends Document {
  entidad: any;
  evento: any;
  accion: any;
  readonly data: any;
  listaPerfiles: Array<string>;
  estado: string;
  status: string;
  idEntidad: string;
  fechaCreacion: Date;
  fechaActualizacion: Date;
}
