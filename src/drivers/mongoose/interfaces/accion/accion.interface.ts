import { Document } from 'mongoose';

export interface CatalogoAccion extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
}
