import { Document } from 'mongoose';

export interface CatalogoArchivoDefault extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
}
