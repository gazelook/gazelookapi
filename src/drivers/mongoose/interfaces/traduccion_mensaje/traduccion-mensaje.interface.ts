import { Document } from 'mongoose';

export interface TraduccionMensaje extends Document {
  readonly contenido: string;
  readonly idioma: string;
  readonly estado: string;
  readonly original: boolean;
  readonly referencia: string;
}
