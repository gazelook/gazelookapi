import { Document } from 'mongoose';

export interface TraduccionPensamiento extends Document {
  readonly pensamiento: string;
  readonly texto: string;
  readonly idioma: string;
  readonly original: boolean;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
