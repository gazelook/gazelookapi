import { Document } from 'mongoose';

export interface CatalogoConfiguracion extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
}
