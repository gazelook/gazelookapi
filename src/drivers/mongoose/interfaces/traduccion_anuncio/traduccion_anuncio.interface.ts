import { Document } from 'mongoose';

export interface TraduccionAnuncio extends Document {
  readonly titulo: string;
  readonly descripcion: string;
  readonly idioma: string;
  readonly estado: string;
  readonly anuncio: string;
}
