import { Document } from 'mongoose';

export interface MensajesAnuncio extends Document {
  readonly estado: string;
  readonly perfil: string;
  readonly texto: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
