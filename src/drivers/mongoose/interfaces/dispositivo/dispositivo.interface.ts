import { Document } from 'mongoose';

export interface Dispositivo extends Document {
  readonly nombre: string;
  readonly codigo: string;
  readonly tipo: string;
  readonly ciudad: string;
  readonly pais: string;
  readonly ipDispositivo: string;
  readonly usuario: string;
  readonly estado: string;
  readonly localidad: string;
  readonly tokenUsuario: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
