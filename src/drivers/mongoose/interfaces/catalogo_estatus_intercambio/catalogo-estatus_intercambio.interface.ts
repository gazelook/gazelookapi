import { Document } from 'mongoose';

export interface CatalogoEstatusIntercambio extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
  readonly fechaCreaacion: Date;
  readonly fechaActualizacion: Date;
}
