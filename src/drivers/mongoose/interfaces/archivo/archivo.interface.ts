import { Document } from 'mongoose';

export interface Archivo extends Document {
  readonly filename: string;
  readonly filenameStorage: string;
  readonly tipo: string;
  readonly url: string;
  readonly path: string;
  readonly peso: number;
  readonly relacionAspecto: string;
  readonly estado: string;
  readonly fileDefault: boolean;
  readonly catalogoArchivoDefault: string;
  readonly duracion: string;
}
