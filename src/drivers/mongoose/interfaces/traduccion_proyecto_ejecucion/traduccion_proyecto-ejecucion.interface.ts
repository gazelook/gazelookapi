import { Document } from 'mongoose';

export interface TraduccionProyectoEjecucion extends Document {
  readonly titulo: string;
  readonly tituloCorto: string;
  readonly descripcion: string;
  readonly tags: Array<string>;
  readonly idioma: string;
  readonly original: boolean;
  readonly estado: string;
  readonly proyectoEjecucion: string;
}
