import { Document } from 'mongoose';

export interface CatalogoTipoMoneda extends Document {
  readonly estado: string;
  readonly codigo: string;
  readonly codNombre: string;
  readonly nombre: string;
  readonly idioma: string;
  readonly predeterminado: boolean;
}
