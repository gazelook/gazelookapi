import { Document } from 'mongoose';

export interface CatalogoTipoComentario extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
}
