import { Document } from 'mongoose';

export interface Media extends Document {
  readonly principal: string;
  readonly miniatura: string;
  readonly catalogoMedia: string;
  readonly estado: string;
  readonly traducciones: Array<object>;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
