import { Document } from 'mongoose';

export interface Balance extends Document {
  readonly estado: string;
  readonly valorActual: number;
  readonly totalIngreso: number;
  readonly totalEgreso: number;
  readonly reparticionFondos: boolean;
  readonly totalIngresosExtra: number;
  readonly totalMontoSobrante: number;
  readonly proviene: number;
  readonly transaciones: Array<string>;
  readonly codigo: string;
}
