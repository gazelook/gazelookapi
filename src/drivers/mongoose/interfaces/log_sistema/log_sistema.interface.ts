import { Document } from 'mongoose';

export interface LogSistema extends Document {
  readonly method: string;
  readonly date: Date;
  readonly status: number;
  readonly url: string;
  readonly ip: string;
  readonly browser: string;
  readonly content: any;
  readonly timeRequest: number;
}
