import { Document } from 'mongoose';

export interface CatalogoColores extends Document {
  readonly codigo: string;
  readonly tipo: string;
  readonly estado: string;
}
