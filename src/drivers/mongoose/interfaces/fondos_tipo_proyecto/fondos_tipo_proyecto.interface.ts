import { Document } from 'mongoose';

export interface FondosTipoProyecto extends Document {
  readonly catalogoPorcentajeProyecto: string;
  readonly monto: number;
  readonly balance: string;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
