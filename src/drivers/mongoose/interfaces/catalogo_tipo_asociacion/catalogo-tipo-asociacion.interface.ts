import { Document } from 'mongoose';

export interface CatalogoTipoAsociacion extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly descripcion: string;
  readonly estado: string;
}
