import { Document } from 'mongoose';

export interface CatalogoTipoArchivo extends Document {
  readonly codigo: string;
  readonly nombre: string;
  readonly estado: string;
  readonly descripcion: string;
}
