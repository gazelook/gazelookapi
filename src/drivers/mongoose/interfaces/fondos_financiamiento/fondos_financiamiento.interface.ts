import { Document } from 'mongoose';

export interface FondosFinanciamiento extends Document {
  readonly catalogoPorcentajeFinanciacion: string;
  readonly monto: number;
  readonly fondosTipoProyecto: number;
  readonly estado: string;
  readonly fechaCreacion: Date;
  readonly fechaActualizacion: Date;
}
