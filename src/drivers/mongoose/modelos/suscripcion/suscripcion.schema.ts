import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const SuscripcionModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    tipo: {
      ref: 'catalogo_suscripcion',
      type: Schema.Types.String,
    },

    transaccion: {
      ref: 'transaccion',
      type: Schema.Types.ObjectId,
    },

    usuario: {
      ref: 'usuario',
      type: Schema.Types.ObjectId,
    },

    referencia: {
      ref: 'suscripcion',
      type: Schema.Types.ObjectId,
    },

    fechaFinalizacion: Date,
  },
  schemaOptions,
);
