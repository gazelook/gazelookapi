import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TraduccionProyectoModelo = new Schema(
  {
    titulo: String,
    tituloCorto: String,
    descripcion: String,
    tags: [],
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    proyecto: {
      ref: 'proyecto',
      type: Schema.Types.ObjectId,
    }, //referencia del id del proyecto
  },
  schemaOptions,
);
TraduccionProyectoModelo.plugin(mongoosePaginate);
