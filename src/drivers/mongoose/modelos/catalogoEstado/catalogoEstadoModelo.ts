import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const estadosModelo = new Schema(
  {
    codigo: String,
    nombre: String,
    descripcion: String,
    entidad: {
      ref: 'catalogo_entidades',
      type: Schema.Types.String,
    },
    accion: {
      ref: 'catalogo_accion',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);

//autoIncrement.initialize(mongoose.connection)

//campos.plugin(autoIncrement.plugin, {model:'catalogo_estado',field:'codigo'});
// const CatalogoEstadoModelo = mongoose.model<mongoose.Document>('catalogo_estados', campos)
// export default CatalogoEstadoModelo
