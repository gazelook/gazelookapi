import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};
export const EstiloAnuncioModelo = new Schema(
  {
    media: {
      ref: 'media',
      type: Schema.Types.ObjectId,
    },
    color: String,
    tamanioLetra: String,
    tipo: {
      ref: 'catalogo_estilos',
      type: Schema.Types.String,
    },
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
