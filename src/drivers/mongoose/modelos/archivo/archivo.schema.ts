import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const archivoModelo = new Schema(
  {
    filename: String,
    filenameStorage: String,
    tipo: {
      ref: 'catalogo_tipo_media',
      type: Schema.Types.String,
    },
    url: String,
    path: String,
    peso: Number,
    relacionAspecto: String,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    fileDefault: {
      type: Boolean,
      default: false,
    },
    catalogoArchivoDefault: {
      ref: 'catalogo_archivo_default',
      type: Schema.Types.String,
    },
    duracion: String,
  },
  schemaOptions,
);
