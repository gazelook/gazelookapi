import { Schema } from 'mongoose';
const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const ParticipanteIntercambioModelo = new Schema(
  {
    coautor: {
      ref: 'perfil',
      type: Schema.Types.ObjectId,
    },
    intercambio: {
      ref: 'intercambio',
      type: Schema.Types.ObjectId,
    },
    roles: [
      {
        ref: 'rol_entidad',
        type: Schema.Types.ObjectId,
      },
    ],
    comentarios: [
      {
        ref: 'comentarios_intercambio',
        type: Schema.Types.ObjectId,
      },
    ],
    configuraciones: [
      {
        ref: 'configuracion_estilo',
        type: Schema.Types.ObjectId,
      },
    ],
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    totalComentarios: Number,
  },
  schemaOptions,
);
