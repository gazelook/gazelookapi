import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: {
    createdAt: 'fechaCreacion',
    updatedAt: 'fechaActualizacionSistema',
  },
};

export const AnuncioModelo = new Schema(
  {
    codigo: String,
    perfil: {
      ref: 'perfil',
      type: Schema.Types.ObjectId,
    },
    tipo: {
      ref: 'catalogo_tipo_anuncio',
      type: Schema.Types.String,
    },
    traducciones: [
      {
        ref: 'traduccion_anuncio',
        type: Schema.Types.ObjectId,
      },
    ],
    tematica: [
      {
        ref: 'tematica_anuncio',
        type: Schema.Types.ObjectId,
      },
    ],
    activo: Boolean,
    favorito: Boolean,
    configuraciones: [
      {
        ref: 'configuracion_estilo_anuncios',
        type: Schema.Types.ObjectId,
      },
    ],
    orden: Number,
    intervaloVisualizacion: Number,
    tiempoVisualizacion: Number,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    fechaInicio: Date,
    fechaFin: Date,
    horaInicio: Date,
    horaFin: Date,
    fechaActualizacion: Date,
  },
  schemaOptions,
);
AnuncioModelo.plugin(mongoosePaginate);
