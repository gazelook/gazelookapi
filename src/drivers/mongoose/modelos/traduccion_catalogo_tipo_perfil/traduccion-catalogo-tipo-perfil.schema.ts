import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

//var collectionName=''
export const traduccionTipoPerfilModelo = new Schema(
  {
    nombre: String,
    descripcion: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    codigoTipoPerfil: {
      ref: 'catalogo_tipo_perfil',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);

// export const CatalogoTraduccionTipoPerfilModelo = mongoose.model<mongoose.Document>('traduccion_catalogo_tipo_perfil', traduccionTipoPerfilModelo, 'traduccion_catalogo_tipo_perfil')
