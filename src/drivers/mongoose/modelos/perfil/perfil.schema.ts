import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const perfilModelo = new Schema(
  {
    tipoPerfil: {
      ref: 'catalogo_tipo_perfil',
      type: Schema.Types.String,
    },
    usuario: {
      ref: 'usuario',
      type: Schema.Types.ObjectId,
    },
    album: [
      {
        ref: 'album',
        type: Schema.Types.ObjectId,
      },
    ],
    nombreContacto: String,
    nombreContactoTraducido: String,
    nombre: String,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    direcciones: [
      {
        ref: 'direccion',
        type: Schema.Types.ObjectId,
      },
    ],
    telefonos: [
      {
        ref: 'telefono',
        type: Schema.Types.ObjectId,
      },
    ],
    proyectos: [
      {
        ref: 'proyecto',
        type: Schema.Types.ObjectId,
      },
    ],
    pensamientos: [
      {
        ref: 'pensamiento',
        type: Schema.Types.ObjectId,
      },
    ],
    noticias: [
      {
        ref: 'noticia',
        type: Schema.Types.ObjectId,
      },
    ],
    asociaciones: [
      {
        ref: 'asociacion',
        type: Schema.Types.ObjectId,
      },
    ],
  },
  schemaOptions,
);
perfilModelo.plugin(mongoosePaginate);
