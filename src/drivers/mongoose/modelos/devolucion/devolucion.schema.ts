import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: {
    createdAt: 'fechaCreacion',
    updatedAt: 'fechaActualizacionSistema',
  },
};

export const DevolucionModelo = new Schema(
  {
    usuario: {
      ref: 'usuario',
      type: Schema.Types.ObjectId,
    },
    transaccion: [
      {
        ref: 'transaccion',
        type: Schema.Types.ObjectId,
      },
    ],
    montoTotal: Number,
    idPago: String,
    idRefund: String,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    fechaActualizacion: Date,
  },
  schemaOptions,
);
DevolucionModelo.plugin(mongoosePaginate);
