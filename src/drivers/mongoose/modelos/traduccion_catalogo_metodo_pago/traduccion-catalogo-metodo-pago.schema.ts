import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

//var collectionName=''
export const TraduccionCatalogoMetodoPagoModelo = new Schema(
  {
    codigo: String,
    nombre: String,
    descripcion: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
