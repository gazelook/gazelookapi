import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const RolSistemaModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    rolesEspecificos: [
      {
        ref: 'rol_entidad',
        type: Schema.Types.ObjectId,
      },
    ],
    rol: {
      ref: 'catalogo_rol',
      type: Schema.Types.String,
    },
    nombre: String,
  },
  schemaOptions,
);
