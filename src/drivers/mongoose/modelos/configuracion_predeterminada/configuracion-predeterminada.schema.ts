import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const ConfiguracionPredeterminada = new Schema(
  {
    configuracion: {
      ref: 'configuracion',
      type: Schema.Types.ObjectId,
    },
    version: Number,
  },
  schemaOptions,
);
