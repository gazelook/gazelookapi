import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const tipoPerfilModelo = new Schema(
  {
    codigo: String,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    traducciones: [
      {
        ref: 'traduccion_catalogo_tipo_perfil',
        type: Schema.Types.ObjectId,
      },
    ],
  },
  schemaOptions,
);

//export const CatalogoTipoPerfilModelo = mongoose.model<mongoose.Document>('catalogo_tipo_perfil', tipoPerfilModelo, 'catalogo_tipo_perfil')
