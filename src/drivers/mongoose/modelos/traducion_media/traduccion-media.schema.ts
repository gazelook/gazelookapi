import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TraduccionMediaModelo = new Schema(
  {
    descripcion: String,
    idioma: String,
    original: Boolean,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    media: {
      ref: 'media',
      type: Schema.Types.ObjectId,
    },
  },
  schemaOptions,
);
