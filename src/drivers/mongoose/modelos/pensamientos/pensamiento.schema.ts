import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: {
    createdAt: 'fechaCreacion',
    updatedAt: 'fechaActualizacionSistema',
  },
};

export const PensamientoModelo = new Schema(
  {
    perfil: {
      ref: 'perfil',
      type: Schema.Types.ObjectId,
    },
    traducciones: [
      {
        ref: 'traduccion_pensamiento',
        type: Schema.Types.ObjectId,
      },
    ],
    publico: Boolean,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    fechaActualizacion: Date,
  },
  schemaOptions,
);
PensamientoModelo.plugin(mongoosePaginate);
