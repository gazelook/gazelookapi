import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const FondosTipoProyectoModelo = new Schema(
  {
    catalogoPorcentajeProyecto: {
      ref: 'catalogo_porcentaje_proyecto',
      type: Schema.Types.String,
    },
    monto: Number,
    balance: {
      ref: 'balance',
      type: Schema.Types.ObjectId,
    },
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
