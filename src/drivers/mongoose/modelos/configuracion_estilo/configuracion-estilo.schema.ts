import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const ConfiguracionEstiloModelo = new Schema(
  {
    codigo: String,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    entidad: {
      ref: 'catalogo_entidad',
      type: Schema.Types.String,
    },
    silenciada: Boolean,
    tonoNotificacion: {
      ref: 'media',
      type: Schema.Types.ObjectId,
    },
    estilos: [
      {
        ref: 'estilos',
        type: Schema.Types.String,
      },
    ],
    tipo: {
      ref: 'catalogo_configuracion',
      type: Schema.Types.String,
    },
    catalogoParticipante: {
      ref: 'catalogo_participante',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
