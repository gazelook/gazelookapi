import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const MensajesAnuncioModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    perfil: {
      ref: 'perfil',
      type: Schema.Types.ObjectId,
    },
    texto: String,
  },
  schemaOptions,
);
MensajesAnuncioModelo.plugin(mongoosePaginate);
