import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TraduccionProyectoEjecucionModelo = new Schema(
  {
    titulo: String,
    tituloCorto: String,
    descripcion: String,
    tags: [],
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    proyectoEjecucion: {
      ref: 'proyecto_ejecucion',
      type: Schema.Types.ObjectId,
    }, //referencia del id del proyecto ejecucion
  },
  schemaOptions,
);
