import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const FormulaEventoModelo = new Schema(
  {
    codigo: String,
    formula: String,
    descripcion: String,
    catalogoEvento: {
      ref: 'catalogo_evento',
      type: Schema.Types.String,
    },
    prioridad: Number,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
