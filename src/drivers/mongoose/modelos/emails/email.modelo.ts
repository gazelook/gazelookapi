import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const emailSchema = new Schema(
  {
    codigo: String,
    emailDestinatario: String,
    fechaCreacion: Date,
    fechaValidacion: Date,
    tokenEmail: String,
    usuario: {
      ref: 'usuario',
      type: Schema.Types.ObjectId,
    },
    perfil: {
      ref: 'perfil',
      type: Schema.Types.ObjectId,
    },
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    enviado: Boolean,
    descripcionEnvio: String,
    datosProceso: Schema.Types.Mixed,
  },
  schemaOptions,
);
