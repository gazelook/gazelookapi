import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const traduccionMensajeModelo = new Schema(
  {
    contenido: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    original: Boolean,
    referencia: String,
  },
  schemaOptions,
);

// const PensamientoModelo = mongoose.model<mongoose.Document>('pensamiento', campos)

// export default PensamientoModelo
