import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TipoMonedaModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    codigo: String,
    codNombre: String,
    nombre: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    predeterminado: Boolean,
  },
  schemaOptions,
);
