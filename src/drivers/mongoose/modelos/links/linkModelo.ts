import mongoose from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

const campos = new mongoose.Schema(
  {
    nombre: String,
    url: String,
    estado: String,
  },
  schemaOptions,
);

const LinkModelo = mongoose.model<mongoose.Document>('links', campos);

export default LinkModelo;
