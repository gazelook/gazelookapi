import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const BalanceModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    valorActual: Number,
    totalIngreso: Number,
    totalEgreso: Number,
    totalIngresosExtra: Number,
    totalMontoSobrante: Number,
    reparticionFondos: Boolean,
    proviene: {
      ref: 'balance',
      type: Schema.Types.ObjectId,
    },
    tipo: {
      ref: 'catalogo_tipo_balance',
      type: Schema.Types.String,
    },
    transacciones: [
      {
        ref: 'transacciones',
        type: Schema.Types.ObjectId,
      },
    ],
    codigo: String,
  },
  schemaOptions,
);
