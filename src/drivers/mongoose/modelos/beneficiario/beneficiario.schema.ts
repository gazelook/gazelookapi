import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const BeneficiarioModelo = new Schema(
  {
    usuario: {
      ref: 'usuario',
      type: Schema.Types.ObjectId,
    },
    tipo: {
      ref: 'catalogo_tipo_beneficiario',
      type: Schema.Types.String,
    },
    proyecto: {
      ref: 'proyecto',
      type: Schema.Types.ObjectId,
    },
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
