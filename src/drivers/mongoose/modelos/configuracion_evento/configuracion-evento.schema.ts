import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const ConfiguracionEventoModelo = new Schema(
  {
    codigo: String,
    intervalo: Number,
    duracion: Number,
    ciclico: Boolean,
    formulas: [
      {
        ref: 'formula_evento',
        type: Schema.Types.String,
      },
    ],
    catalogoEvento: [
      {
        ref: 'catalogo_evento',
        type: Schema.Types.String,
      },
    ],
    fechaInicio: Date,
    fechaProximo: Date,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
