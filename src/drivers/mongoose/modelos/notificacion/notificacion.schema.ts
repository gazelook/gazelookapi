import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const NotificacionModelo = new Schema(
  {
    entidad: {
      ref: 'catalogo_entidad',
      type: Schema.Types.String,
    },
    evento: {
      ref: 'catalogo_evento_notificacion',
      type: Schema.Types.String,
    },
    accion: {
      ref: 'catalogo_accion_notificacion',
      type: Schema.Types.String,
    },
    data: Object,
    listaPerfiles: {
      ref: 'perfil',
      type: Schema.Types.Array,
    },
    status: {
      ref: 'catalogo_status_notificacion',
      type: Schema.Types.String,
    },
    idEntidad: {
      type: Schema.Types.ObjectId,
    },
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
