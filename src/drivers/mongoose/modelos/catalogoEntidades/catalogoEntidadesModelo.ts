import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const entidadesModelo = new Schema(
  {
    codigo: String,
    nombre: String,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
//const CatalogoEntidadesModelo = mongoose.model<mongoose.Document>('catalogo_entidades', campos)
//export default CatalogoEntidadesModelo
