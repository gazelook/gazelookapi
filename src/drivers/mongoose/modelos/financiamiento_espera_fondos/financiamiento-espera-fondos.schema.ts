import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const FinanciamientoEsperaFondosModelo = new Schema(
  {
    catalogoPorcentajeEsperaFondos: {
      ref: 'catalogo_porcentaje_espera_fondos',
      type: Schema.Types.String,
    },
    montoIngreso: Number,
    montoTotal: Number,
    montoFinal: Number,
    referencia: {
      ref: 'fondos_financiamiento',
      type: Schema.Types.ObjectId,
    },
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
