import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const ConfiguracionPreestrategiaModelo = new Schema(
  {
    codigo: String,
    orden: Number,
    semanas: Number,
    configuracion_evento: {
      ref: 'configuracion_evento',
      type: Schema.Types.String,
    },
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
