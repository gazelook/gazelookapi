import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TraduccionAnuncioModelo = new Schema(
  {
    titulo: String,
    descripcion: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    anuncio: {
      ref: 'anuncio',
      type: Schema.Types.ObjectId,
    }, //referencia del id del proyecto
  },
  schemaOptions,
);
TraduccionAnuncioModelo.plugin(mongoosePaginate);
