import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const albumModelo = new Schema(
  {
    media: [
      {
        ref: 'media',
        type: Schema.Types.ObjectId,
      },
    ],
    tipo: {
      ref: 'catalogo_album',
      type: Schema.Types.String,
    },
    portada: {
      ref: 'media',
      type: Schema.Types.ObjectId,
    },
    traducciones: [
      {
        ref: 'traduccion_album',
        type: Schema.Types.ObjectId,
      },
    ],
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    predeterminado: { type: Boolean, default: false },
  },
  schemaOptions,
);
