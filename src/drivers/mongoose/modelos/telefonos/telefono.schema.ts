import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TelefonoModelo = new Schema(
  {
    pais: {
      ref: 'catalogo_pais',
      type: Schema.Types.String,
    },
    numero: String,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
