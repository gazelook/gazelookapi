import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const RolEntidadModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    acciones: [
      {
        ref: 'catalogo_acciones',
        type: Schema.Types.ObjectId,
      },
    ],
    entidad: {
      ref: 'catalogo_entidades',
      type: Schema.Types.String,
    },
    rol: {
      ref: 'catalogo_rol',
      type: Schema.Types.String,
    },
    nombre: String,
  },
  schemaOptions,
);
