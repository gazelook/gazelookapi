import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: {
    createdAt: 'fechaCreacion',
    updatedAt: 'fechaActualizacionSistema',
  },
};

export const DocumentoUsuario = new Schema(
  {
    usuario: {
      ref: 'usuario',
      type: Schema.Types.ObjectId,
    },
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    tipo: {
      ref: 'catalogo_tipo_documento_usuario',
      type: Schema.Types.String,
    },
    archivo: {
      ref: 'archivo',
      type: Schema.Types.ObjectId,
    },
    fechaActualizacion: Date,
  },
  schemaOptions,
);
