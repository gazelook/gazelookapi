import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const MensajeModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    traducciones: [
      {
        ref: 'traduccion_mensajes',
        type: Schema.Types.ObjectId,
      },
    ],
    tipo: {
      ref: 'catalogo_tipo_mensaje',
      type: Schema.Types.String,
    },
    estatus: {
      ref: 'catalogo_mensaje',
      type: Schema.Types.String,
    },
    importante: {
      type: Boolean,
      default: false,
    },
    adjuntos: [
      {
        ref: 'media',
        type: Schema.Types.ObjectId,
      },
    ],
    conversacion: {
      ref: 'conversacion',
      type: Schema.Types.ObjectId,
    },
    propietario: {
      ref: 'participante_asociacion',
      type: Schema.Types.ObjectId,
    },
    entregadoA: [
      {
        ref: 'perfil',
        type: Schema.Types.ObjectId,
      },
    ],
    leidoPor: [
      {
        ref: 'perfil',
        type: Schema.Types.ObjectId,
      },
    ],
    eliminadoPor: [
      {
        ref: 'perfil',
        type: Schema.Types.ObjectId,
      },
    ],
    identificadorTemporal: {
      type: Schema.Types.Number,
    },
    referenciaEntidadCompartida: {
      type: Schema.Types.ObjectId,
    },
    entidadCompartida: {
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
MensajeModelo.plugin(mongoosePaginate);
