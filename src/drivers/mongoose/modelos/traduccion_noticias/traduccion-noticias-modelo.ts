import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const traduccionNoticiaSchema = new Schema(
  {
    tituloCorto: String,
    titulo: String,
    descripcion: String,
    tags: [],
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    original: Boolean,
    referencia: {
      ref: 'noticia',
      type: Schema.Types.ObjectId,
    },
  },
  schemaOptions,
);
traduccionNoticiaSchema.plugin(mongoosePaginate);
