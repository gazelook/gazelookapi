import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const CatalogoMetodoPagoModelo = new Schema(
  {
    codigo: String,
    // nombre: String,
    // descripcion: String,
    traducciones: [
      {
        ref: 'traduccion_catalogo_metodo_pago',
        type: Schema.Types.ObjectId,
      },
    ],
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    icono: {
      ref: 'media',
      type: Schema.Types.ObjectId,
    },
  },
  schemaOptions,
);

//const UsuarioModelo = mongoose.model<mongoose.Document>('usuario', campos, 'usuario')

//export default UsuarioModelo
