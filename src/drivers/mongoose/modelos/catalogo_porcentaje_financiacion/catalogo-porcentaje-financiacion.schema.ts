import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const CatalogoPorcentajeFinanciacionModelo = new Schema(
  {
    codigo: String,
    porcentaje: Number,
    prioridad: Number,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
