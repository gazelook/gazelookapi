import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: {
    createdAt: 'fechaCreacion',
    updatedAt: 'fechaActualizacionSistema',
  },
};

export const ComentarioModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    adjuntos: [
      {
        ref: 'media',
        type: Schema.Types.ObjectId,
      },
    ],
    coautor: {
      ref: 'participante_proyecto',
      type: Schema.Types.ObjectId,
    },
    importante: Boolean,
    traducciones: [
      {
        ref: 'traduccion_comentario',
        type: Schema.Types.ObjectId,
      },
    ],
    tipo: {
      ref: 'catalogo_tipo_comentario',
      type: Schema.Types.String,
    },
    proyecto: {
      ref: 'proyecto',
      type: Schema.Types.ObjectId,
    },
    idPerfilRespuesta: {
      ref: 'perfil',
      type: Schema.Types.ObjectId,
    },
    fechaActualizacion: Date,
    fechaCreacionFirebase: String,
  },
  schemaOptions,
);
ComentarioModelo.plugin(mongoosePaginate);
