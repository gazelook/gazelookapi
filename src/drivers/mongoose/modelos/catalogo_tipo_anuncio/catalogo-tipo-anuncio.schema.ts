import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const CatalogoTipoAnuncioModelo = new Schema(
  {
    codigo: String,
    nombre: String,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
