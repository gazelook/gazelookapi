import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TraduccionIntercambioComentarioModelo = new Schema(
  {
    texto: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    referencia: {
      ref: 'comentarios_intercambio',
      type: Schema.Types.ObjectId,
    },
  },
  schemaOptions,
);
