import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

//var collectionName=''
export const TraduccionCatalogoTipoProyectoModelo = new Schema(
  {
    referencia: {
      ref: 'catalogo_tipo_proyecto',
      type: Schema.Types.String,
    },
    nombre: String,
    descripcion: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
