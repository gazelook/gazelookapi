import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TraduccionComentarioModelo = new Schema(
  {
    texto: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    referencia: {
      ref: 'comentarios',
      type: Schema.Types.ObjectId,
    },
  },
  schemaOptions,
);
