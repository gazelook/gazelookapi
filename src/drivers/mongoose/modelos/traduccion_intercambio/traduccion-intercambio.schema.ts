import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TraduccionIntercambioModelo = new Schema(
  {
    titulo: String,
    tituloCorto: String,
    descripcion: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    intercambio: {
      ref: 'intercambio',
      type: Schema.Types.ObjectId,
    },
  },
  schemaOptions,
);

TraduccionIntercambioModelo.plugin(mongoosePaginate);
