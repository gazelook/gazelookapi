import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const CatalogoTipoIntercambioModelo = new Schema(
  {
    codigo: String,
    traducciones: [
      {
        ref: 'traduccion_catalogo_tipo_intercambio',
        type: Schema.Types.ObjectId,
      },
    ],
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
