import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const CatalogoEventoNotificacionModelo = new Schema(
  {
    codigo: String,
    nombre: String,
    descricion: String,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
