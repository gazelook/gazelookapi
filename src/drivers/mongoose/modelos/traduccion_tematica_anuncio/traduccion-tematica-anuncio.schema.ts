import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TraduccionTematicaAnuncioModelo = new Schema(
  {
    titulo: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    anuncio: {
      ref: 'anuncio',
      type: Schema.Types.ObjectId,
    }, //referencia del id del proyecto
  },
  schemaOptions,
);
TraduccionTematicaAnuncioModelo.plugin(mongoosePaginate);
