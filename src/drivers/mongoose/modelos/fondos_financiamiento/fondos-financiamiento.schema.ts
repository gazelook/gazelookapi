import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const FondosFinanciamientoModelo = new Schema(
  {
    catalogoPorcentajeFinanciacion: {
      ref: 'catalogo_porcentaje_financiacion',
      type: Schema.Types.String,
    },
    monto: Number,
    fondosTipoProyecto: {
      ref: 'fondos_tipo_proyecto',
      type: Schema.Types.ObjectId,
    },
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
