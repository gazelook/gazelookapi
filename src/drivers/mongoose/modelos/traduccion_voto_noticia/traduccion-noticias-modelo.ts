import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const traduccionVotoNoticiaSchema = new Schema(
  {
    descripcion: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    original: Boolean,
    referencia: {
      ref: 'voto_noticia',
      type: Schema.Types.ObjectId,
    },
  },
  schemaOptions,
);

// const PensamientoModelo = mongoose.model<mongoose.Document>('pensamiento', campos)

// export default PensamientoModelo
