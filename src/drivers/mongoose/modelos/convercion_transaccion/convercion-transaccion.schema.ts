import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const ConversionTransaccionModelo = new Schema(
  {
    moneda: {
      ref: 'catalogo_tipo_moneda',
      type: Schema.Types.String,
    },
    monto: Number,
    principal: {
      type: Boolean,
      default: false,
    },
    transaccion: {
      ref: 'transaccion',
      type: Schema.Types.ObjectId,
    },
    comisionTransferencia: Number,
    totalRecibido: Number,
  },
  schemaOptions,
);
