import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const BonificacionModelo = new Schema(
  {
    proyecto: {
      ref: 'proyecto',
      type: Schema.Types.ObjectId,
    },
    montoCubierto: Number,
    montoFaltante: Number,
    fondosFinanciamiento: {
      ref: 'fondos_financiamiento',
      type: Schema.Types.ObjectId,
    },
    fondosReservados: {
      ref: 'transaccion',
      type: Schema.Types.ObjectId,
    },
    esperaFondos: {
      ref: 'financiamiento_espera_fondos',
      type: Schema.Types.ObjectId,
    },
    transacciones: [
      {
        ref: 'transaccion',
        type: Schema.Types.ObjectId,
      },
    ],
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
