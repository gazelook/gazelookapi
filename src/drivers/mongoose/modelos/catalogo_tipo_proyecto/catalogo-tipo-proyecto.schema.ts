import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const CatalogoTipoProyectoModelo = new Schema(
  {
    codigo: String,
    traducciones: [
      {
        ref: 'traduccion_catalogo_tipo_proyecto',
        type: Schema.Types.ObjectId,
      },
    ],
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
