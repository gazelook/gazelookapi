import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';
const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const ConversacionModelo = new Schema(
  {
    asociacion: {
      ref: 'asociacion',
      type: Schema.Types.ObjectId,
    },
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    ultimoMensaje: {
      ref: 'mensaje',
      type: Schema.Types.ObjectId,
    },
    listaUltimoMensaje: [
      {
        ref: 'mensaje',
        type: Schema.Types.ObjectId,
      },
    ],
  },
  schemaOptions,
);
ConversacionModelo.plugin(mongoosePaginate);
