import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const votoNoticiaSchema = new Schema(
  {
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    perfil: {
      ref: 'perfil',
      type: Schema.Types.String,
    },
    noticia: {
      ref: 'noticia',
      type: Schema.Types.String,
    },
    traducciones: [
      {
        ref: 'traduccion_voto_noticias',
        type: Schema.Types.ObjectId,
      },
    ],
  },
  schemaOptions,
);
