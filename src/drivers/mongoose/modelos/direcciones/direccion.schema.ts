import { Schema } from 'mongoose';
const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const DireccionModelo = new Schema(
  {
    localidad: {
      ref: 'catalogo_localidad',
      type: Schema.Types.String,
    },
    pais: {
      ref: 'catalogo_pais',
      type: Schema.Types.String,
    },
    latitud: Number,
    longitud: Number,
    traducciones: [
      {
        ref: 'traduccion_direccion',
        type: Schema.Types.ObjectId,
      },
    ],
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
