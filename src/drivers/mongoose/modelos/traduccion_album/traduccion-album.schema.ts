import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

//var collectionName=''
export const TraduccionAlbum = new Schema(
  {
    nombre: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
  },
  schemaOptions,
);

// export const CatalogoTraduccionTipoPerfilModelo = mongoose.model<mongoose.Document>('traduccion_catalogo_tipo_perfil', traduccionTipoPerfilModelo, 'traduccion_catalogo_tipo_perfil')
