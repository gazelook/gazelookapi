import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const ConfiguracionPersonalizada = new Schema(
  {
    configuracion: {
      ref: 'configuracion',
      type: Schema.Types.ObjectId,
    },
  },
  schemaOptions,
);
