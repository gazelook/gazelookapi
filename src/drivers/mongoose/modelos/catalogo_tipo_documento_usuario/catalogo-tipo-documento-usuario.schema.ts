import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const CatalogoTipoDocumentoUsuario = new Schema(
  {
    codigo: String,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    nombre: String,
  },
  schemaOptions,
);
