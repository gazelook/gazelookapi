import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const ConfiguracionEstiloAnunciosModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    estilos: [
      {
        ref: 'estilos_anuncio',
        type: Schema.Types.ObjectId,
      },
    ],
    tipo: {
      ref: 'catalogo_configuracion',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
