import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const TraduccionCatalogoTipoIntercambioModelo = new Schema(
  {
    catalogoTipoIntercambio: {
      ref: 'catalogo_tipo_intercambio',
      type: Schema.Types.String,
    },
    nombre: String,
    descripcion: String,
    idioma: {
      ref: 'catalogo_idiomas',
      type: Schema.Types.String,
    },
    original: Boolean,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
