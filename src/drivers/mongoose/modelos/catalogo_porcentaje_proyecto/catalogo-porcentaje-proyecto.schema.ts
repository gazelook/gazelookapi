import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const CatalogoPorcentajeProyectoModelo = new Schema(
  {
    codigo: String,
    tipo: {
      ref: 'catalogo_tipo_proyecto',
      type: Schema.Types.String,
    },
    porcentaje: Number,
    prioridad: Number,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
