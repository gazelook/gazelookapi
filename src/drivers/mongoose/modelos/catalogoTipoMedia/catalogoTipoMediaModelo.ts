import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const CatalogoTipoMediaModelo = new Schema(
  {
    codigo: String,
    nombre: String,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
