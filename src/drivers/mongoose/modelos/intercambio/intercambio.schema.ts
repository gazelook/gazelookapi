import { Schema } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
  timestamps: {
    createdAt: 'fechaCreacion',
    updatedAt: 'fechaActualizacionSistema',
  },
};

export const IntercambioModelo = new Schema(
  {
    perfil: {
      ref: 'perfil',
      type: Schema.Types.ObjectId,
    },
    tipo: {
      ref: 'catalogo_tipo_intercambio',
      type: Schema.Types.String,
    },
    tipoIntercambiar: {
      ref: 'catalogo_tipo_intercambio',
      type: Schema.Types.String,
    },
    direccion: {
      ref: 'direccion',
      type: Schema.Types.ObjectId,
    },
    adjuntos: [
      {
        ref: 'album',
        type: Schema.Types.ObjectId,
      },
    ],

    traducciones: [
      {
        ref: 'traduccion_intercambio',
        type: Schema.Types.ObjectId,
      },
    ],
    participantes: [
      {
        ref: 'participante_intercambio',
        type: Schema.Types.ObjectId,
      },
    ], //lista de participantes del proyecto
    comentarios: [
      {
        ref: 'comentarios_intercambio',
        type: Schema.Types.ObjectId,
      },
    ],
    status: {
      ref: 'catalogo_estatus_intercambio',
      type: Schema.Types.String,
    },
    email: String,
    estado: {
      ref: 'catalogo_estado',
      type: Schema.Types.String,
    },
    fechaActualizacion: Date,
  },
  schemaOptions,
);

IntercambioModelo.plugin(mongoosePaginate);
