import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};
export const catalogoEstatusIntercambioModelo = new Schema(
  {
    codigo: String,
    nombre: String,
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);

//autoIncrement.initialize(mongoose.connection)

//campos.plugin(autoIncrement.plugin, {model:'catalogo_estado',field:'codigo'});

// const CatalogoEstadoModelo = mongoose.model<mongoose.Document>('catalogo_estados', campos)

// export default CatalogoEstadoModelo
