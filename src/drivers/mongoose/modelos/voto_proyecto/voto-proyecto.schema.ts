import { Schema } from 'mongoose';

const schemaOptions = {
  timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
};

export const VotoProyectoModelo = new Schema(
  {
    estado: {
      ref: 'catalogo_estados',
      type: Schema.Types.String,
    },
    perfil: {
      ref: 'perfil',
      type: Schema.Types.ObjectId,
    },
    proyecto: {
      ref: 'proyecto',
      type: Schema.Types.ObjectId,
    },
    traducciones: [
      {
        ref: 'traduccion_voto_proyecto',
        type: Schema.Types.ObjectId,
      },
    ],
    tipo: {
      ref: 'catalogo_tipo_voto',
      type: Schema.Types.String,
    },
    numeroVoto: Number,
    configuracion: {
      ref: 'configuracion_evento',
      type: Schema.Types.String,
    },
  },
  schemaOptions,
);
