import { Module } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { ConverCoinMarketCapController } from './controllers/conver-coinmarketcap.controller';
import { ConverCoinMarketCapService } from './services/conver.coinmarketcap.service';
// import { ConverCoinMarketCapService } from './services/conver-coinmarketcap.service';

@Module({
  providers: [
    ConverCoinMarketCapService, 
    Funcion],
  exports: [
    ConverCoinMarketCapService
  ],
  controllers: [ConverCoinMarketCapController],
})
export class CoinMarketCapModule {}
