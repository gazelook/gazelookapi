import { Injectable } from '@nestjs/common';
import { ConverCoinMarketCapDto } from '../dtos/conver-dto';
import { ConfigService } from '../../../config/config.service';
const rp = require('request-promise');


@Injectable()
export class ConverCoinMarketCapService {
    COINMARKETCAP_KEY: string;

    constructor(private config: ConfigService
    ) {
        this.COINMARKETCAP_KEY = config.get<string>('COINMARKETCAP_KEY');
    }


    async conversionCoinmarket(
        transactionDto: ConverCoinMarketCapDto
    ): Promise<any> {

        try {
            //Configuracion del api de conversion
            const requestOptions = {
                method: 'GET',
                uri: 'https://pro-api.coinmarketcap.com/v1/tools/price-conversion',
                qs: {
                    'amount': transactionDto.amount,
                    'convert_id': transactionDto.convert_id,
                    'symbol': transactionDto.symbol
                },
                headers: {
                    'X-CMC_PRO_API_KEY': this.COINMARKETCAP_KEY,
                    'CMC_PRO_API_KEY': this.COINMARKETCAP_KEY
                },
                json: true,
                gzip: true
            };

            const moneda = transactionDto.convert_id.toString()
            const conver = rp(requestOptions).then(body => {
                
                const conversion = body.data.quote[moneda].price
                return conversion

            }).catch((error) => {
                console.log('API call error coinMarketCap:', error.message);
                throw error
            });

            return conver
        } catch (error) {
            throw error
        }
    }

}