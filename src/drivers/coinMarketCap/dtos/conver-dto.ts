import { ApiProperty } from "@nestjs/swagger";

export class ConverCoinMarketCapDto {
  @ApiProperty({type:Number})
  amount : number;

  @ApiProperty({type:Number})
  convert_id: number;

  @ApiProperty({type:String})
  symbol: string;
  
}
