import { Body, Controller, HttpStatus, Module, Post } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { ConverCoinMarketCapDto } from '../dtos/conver-dto';
import { ConverCoinMarketCapService } from '../services/conver.coinmarketcap.service';

@Controller('api/conversion-coinmarketcap')
export class ConverCoinMarketCapController {
  constructor(
    private readonly converCoinMarketCapService: ConverCoinMarketCapService,
    private readonly funcion: Funcion,
    ) {}

  @Post('/')
  async convertirMonedas(
    @Body() transactionDto: ConverCoinMarketCapDto,
  ): Promise<any> {

    try {
      const conversion = await this.converCoinMarketCapService.conversionCoinmarket(transactionDto)
      
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: conversion,
        });
     
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message,
      });
      
    }
    
  }
}

@Module({
  controllers: [ConverCoinMarketCapController],
  providers: [
    ConverCoinMarketCapService, 
    Funcion],
})
export class CoinpaymentsTransactionModule {}
