import { Injectable } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
@Injectable()
export class AuthService {
  private apiKeys: string[];

  constructor(config: ConfigService) {
    // KEYS
    this.apiKeys = [config.get<string>('API_KEY')];
  }
  validateApiKey(apiKey: string) {
    return this.apiKeys.find(apiK => apiKey === apiK);
  }
}
