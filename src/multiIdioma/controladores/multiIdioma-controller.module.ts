import { Module } from '@nestjs/common';
import { TraduccionEstaticaController } from './traduccion-estatica-controller';

@Module({
  imports: [],
  providers: [],
  exports: [],
  controllers: [TraduccionEstaticaController],
})
export class MultiIdiomaControllerModule {}
