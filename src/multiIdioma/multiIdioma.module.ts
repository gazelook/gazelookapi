import { Module } from '@nestjs/common';
import { MultiIdiomaControllerModule } from './controladores/multiIdioma-controller.module';

@Module({
  imports: [MultiIdiomaControllerModule],
  providers: [],
  controllers: [],
})
export class MultiIdiomaModule {}
