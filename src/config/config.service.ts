// src/config/config.service.ts
import { Injectable } from '@nestjs/common';
import { ConfigManager } from '@nestjsplus/config';
import * as Joi from '@hapi/joi';

@Injectable()
export class ConfigService extends ConfigManager {
  provideConfigSpec() {
    return {
      API_KEY: {
        validate: Joi.string(),
        required: true,
        default: process.env.API_KEY,
      },
      S3_ACCESS_KEY: {
        validate: Joi.string(),
        required: true,
        default: process.env.S3_ACCESS_KEY,
      },
      S3_SECTRET_ACCESS_KEY: {
        validate: Joi.string(),
        required: true,
        default: process.env.S3_SECTRET_ACCESS_KEY,
      },
      S3_BUCKET: {
        validate: Joi.string(),
        required: true,
        default: process.env.S3_BUCKET,
      },
      S3_FOLDER: {
        validate: Joi.string(),
        required: true,
        default: process.env.S3_FOLDER,
      },
      S3_FOLDER_FILE_DEFAULT: {
        validate: Joi.string(),
        required: true,
        default: process.env.S3_FOLDER_FILE_DEFAULT,
      },
      S3_BUCKET_DOCUMENTOS_USUARIO: {
        validate: Joi.string(),
        required: true,
        default: process.env.S3_BUCKET_DOCUMENTOS_USUARIO,
      },
      S3_FOLDER_DOCUMENTOS: {
        validate: Joi.string(),
        required: true,
        default: process.env.S3_FOLDER_DOCUMENTOS,
      },
      URL_S3_DOCUMENTOS_USUARIO: {
        validate: Joi.string(),
        required: true,
        default: process.env.URL_S3_DOCUMENTOS_USUARIO,
      },
      PORT: {
        validate: Joi.number()
          .min(4000)
          .max(65535),
        required: true,
        default: process.env.PORT,
      },
      HOST: {
        validate: Joi.string(),
        required: true,
        default: process.env.HOST,
      },
      MONGO_URI: {
        validate: Joi.string(),
        required: true,
      },
      PAYPAL_MODE: {
        validate: Joi.string(),
        required: true,
      },
      PAYPAL_CLIEN_ID: {
        validate: Joi.string(),
        required: true,
      },
      PAYPAL_CLIENT_SECRET: {
        validate: Joi.string(),
        required: true,
      },
      PAYPAL_RETURN_URL: {
        validate: Joi.string(),
        required: true,
      },
      PAYPAL_CANCEL_URL: {
        validate: Joi.string(),
        required: true,
      },
      FIREBASE_PROJECT_ID: {
        validate: Joi.string(),
        required: true,
        default: process.env.FIREBASE_PROJECT_ID,
      },
      FIREBASE_PRIVATE_KEY: {
        validate: Joi.string(),
        required: true,
        default: process.env.FIREBASE_PRIVATE_KEY,
      },
      FIREBASE_CLIENT_EMAIL: {
        validate: Joi.string(),
        required: true,
        default: process.env.FIREBASE_CLIENT_EMAIL,
      },
      DATABASE_URL: {
        validate: Joi.string(),
        required: true,
        default: process.env.DATABASE_URL,
      },
      URL_CLOUDFRONT: {
        validate: Joi.string(),
        required: true,
        default: process.env.URL_CLOUDFRONT,
      },
      STRIPE_KEY: {
        validate: Joi.string(),
        required: true,
        default: process.env.STRIPE_KEY,
      },
      JWT_SECRET: {
        validate: Joi.string(),
        required: true,
        default: process.env.JWT_SECRET,
      },
      URL_SERVER_HISTORICO: {
        validate: Joi.string(),
        required: true,
        default: process.env.URL_SERVER_HISTORICO,
      },
      COINPAYMENTEZ_SECRET: {
        validate: Joi.string(),
        required: true,
        default: process.env.COINPAYMENTEZ_SECRET,
      },
      COINPAYMENTEZ_KEY: {
        validate: Joi.string(),
        required: true,
        default: process.env.COINPAYMENTEZ_KEY,
      },
      COINMARKETCAP_KEY: {
        validate: Joi.string(),
        required: true,
        default: process.env.COINMARKETCAP_KEY,
      },
    };
  }
}
